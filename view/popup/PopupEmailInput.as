/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.FormValidator;
	
	import data.Infos;
	
	import utils.Colors;
	import utils.TextFormats;
	import utils.TextUtils;
	
	
	
	public class PopupEmailInput extends PopupSimpleInput
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var errorField : TextField
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PopupEmailInput( inputLabel : String, titleKey:String , descKey:String = null, autoFill:Boolean = true ) 
		{
			super( inputLabel, titleKey, descKey );
			
			if( autoFill && Infos.session.email ) inputField.text = Infos.session.email; 
			
			// 
			var nextPosY:Number = inputField.y + inputField.height + 5;
			errorField = TextUtils.createLabelWithTextFormat("", TextFormats.ASAP_BOLD(12,Colors.RED_FLASH, TextFormatAlign.CENTER),description.x,nextPosY,description.width);
			view.addChild(errorField)
			nextPosY += errorField.height + 5;
			okButton.y = cancelButton.y = nextPosY;
			nextPosY += okButton.height + 10;
			bg.height = nextPosY + 20;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		protected override function okHandler(e:Event=null):void
		{
			inputField.showError(false);
			errorField.text = "";
			
			// check infos in form
			if(FormValidator.isEmailFieldValid(inputField.text, 3, inputField.waterMark) != FormValidator.FIELD_VALID){
				inputField.showError();
				errorField.text = ResourcesManager.getString("form.error.ERROR_MAIL_NOT_VALID");
				return;
			}
			
			super.okHandler(e);
		}

		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
	}
}