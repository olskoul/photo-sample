/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import com.bit101.components.HBox;
	import com.bit101.components.PushButton;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import comp.button.SimpleButton;
	
	import library.close.btn;
	
	import mcs.popup.defaultPopup;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.TextFormats;
	
	import view.AbstractScreen;
	import view.popup.PopupAbstract;

	public class PopupDefault extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var skin : defaultPopup = new defaultPopup();
		private var closeButton : Sprite;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PopupDefault(title:String, description:String, hasCancelButton:Boolean = true, hasCloseButton:Boolean = false, buttonLabelList:Array = null, buttonFunctionList:Array = null, centerButtons:Boolean = true, buttonLabelAsString : Boolean = false) 
		{ 
			addChild(skin);
			
			skin.title.text = title;
			skin.description.autoSize = "center";
			
			
			skin.description.htmlText = description;

			skin.description.x = 20;
			skin.description.width = skin.bg.width - 40;
			var tf:TextFormat = TextFormats.ASAP_REGULAR(14,Colors.BLUE,TextFormatAlign.CENTER);
			skin.description.setTextFormat(tf);
			skin.description.defaultTextFormat = tf;
			
			/*
			registerOkButton(skin.okButton);
			registerCancelButton(skin.cancelButton);
			*/
			if(buttonLabelList && buttonLabelList.length>0)
			{
				// remove existing button
				if(skin.okButton.parent)skin.okButton.parent.removeChild(skin.okButton);
				if(skin.cancelButton.parent)skin.cancelButton.parent.removeChild(skin.cancelButton);
				
				// create new button box
				var buttonBox : HBox = createButtonBox(buttonLabelList, buttonFunctionList, 150,15,buttonLabelAsString);
				addChild(buttonBox);
				buttonBox.y = skin.description.y + skin.description.height + 20;
				buttonBox.x = 20;
				
				// update background
				// case buttons are bigger than text
				if(buttonBox.width +40 > skin.bg.width){
					skin.bg.width = buttonBox.width + 40;
					// modify size of txt and descrpition too then
					skin.title.width =skin.description.width= buttonBox.width;
				}
				// case text is bigger than buttons
				else
				{
					// case extend buttons
					if(!centerButtons ) buttonBox.spacing = Math.round((skin.bg.width - 40 - buttonBox.width) / (buttonLabelList.length-1));
					// case center buttons
					else buttonBox.x = (skin.bg.width - buttonBox.width )*.5;
				}
				skin.bg.height = buttonBox.y + buttonBox.height + 20;	
			}
			else
			{
				okButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
				replaceButton(skin.okButton, okButton, true);
				
				cancelButton = SimpleButton.createPopupCancelButton("common.cancel",cancelHandler);
				replaceButton(skin.cancelButton, cancelButton);
				
				(cancelButton as SimpleButton).forcedWidth = 150;
				(okButton as SimpleButton).forcedWidth = 150;
				(cancelButton as SimpleButton).x = skin.bg.width *.5 - 160;
				(okButton as SimpleButton).x = skin.bg.width *.5 +10;
	
				
				cancelButton.y = okButton.y = skin.description.y + skin.description.height + 20;
				skin.bg.height = okButton.y + okButton.height + 20;
				
				
				if(!hasCancelButton) {
					cancelButton.visible = false;
					okButton.x = skin.width*.5 - okButton.width*.5;
				}
			}

			if(hasCloseButton){
				closeButton = ButtonUtils.makeButton(new library.close.btn(), cancelHandler);
				closeButton.x = skin.bg.width - closeButton.width - 7;
				closeButton.y = 3;
				addChild(closeButton);
			}
			

			
			super();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		protected function createButtonBox( buttonLabelList:Array, buttonFunctionList:Array, forcedWidth:Number = -1, spacing:Number =NaN, buttonLabelAsString:Boolean = false):HBox
		{
			var buttonBox : HBox = new HBox();
			if(!isNaN(spacing)) buttonBox.spacing = spacing;
			for (var i:int = 0; i < buttonLabelList.length; i++) 
			{
				var btn : Sprite = SimpleButton.createPopupOkButton(buttonLabelList[i], cancelHandler, buttonLabelAsString);//new PushButton(buttonBox,0,0,buttonLabelList[i]);
				if(forcedWidth != -1) (btn as SimpleButton).forcedWidth = forcedWidth;
				buttonBox.addChild(btn);
				if(buttonFunctionList && buttonFunctionList[i] != null){
					btn.addEventListener(MouseEvent.CLICK, buttonFunctionList[i]);
				}
				else if (i==0) btn.addEventListener(MouseEvent.CLICK, okHandler);
				else btn.addEventListener(MouseEvent.CLICK, cancelHandler);
			}
			
			//okButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
			return buttonBox;
		}
		
	}
}