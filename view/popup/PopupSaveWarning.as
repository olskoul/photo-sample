/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	
	import comp.button.SimpleButton;
	
	import data.Infos;
	
	import mcs.popup.defaultPopup;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.TextFormats;

	public class PopupSaveWarning extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public const DONOT_SAVE : Signal = new Signal();
		
		// const
		private const margin : int = 20;
		private const listItemHeight:int = 25;
		
		// ui
		private var view : defaultPopup = new defaultPopup();
		private var title : TextField;
		private var description : TextField;
		private var donotSaveButton : SimpleButton; 
		private var bg : MovieClip;
		

		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function PopupSaveWarning() 
		{ 
			// map
			addChild(view);
			title = view.title;
			description = view.description;
			bg = view.bg;
			
			//bg.width = 600;
			//bg.height = 600;
			
			
			// title
			title.x = description.x = margin;
			//title.y = margin;
			title.width = description.width = bg.width -2*margin;
			title.autoSize = "left";
			ResourcesManager.setText(title, "popup.project.save.warning.title");
			
			// description
			//description.setTextFormat(TextFormats.ASAP_REGULAR(15, Colors.BLUE));
			description.autoSize = "left";
			ResourcesManager.setText(description, "popup.project.save.warning.description");
			
			
			// buttons
			okButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
			replaceButton(view.okButton, okButton, true);
			
			cancelButton = SimpleButton.createPopupCancelButton("common.cancel",cancelHandler);
			replaceButton(view.cancelButton, cancelButton);
			
			donotSaveButton = SimpleButton.createBlueButton("popup.project.save.warning.donotsave.button",doNotSaveHandler);
			donotSaveButton.forcedWidth = 140;
			
			(cancelButton as SimpleButton).forcedWidth = 140;
			(okButton as SimpleButton).forcedWidth = 140;
			(cancelButton as SimpleButton).x = view.bg.width *.5 - 170;
			(okButton as SimpleButton).x = view.bg.width *.5 +20;
			
			
			super();
		}
		
		public override function open():void
		{
			// positions
			description.y = view.titleBkg.y + view.titleBkg.height + 10;
			
			// buttons position
			okButton.y = description.y + description.height + 20 ;
			cancelButton.y = okButton.y;
			donotSaveButton.y = okButton.y;
			bg.height = okButton.y + okButton.height +20;
			
			donotSaveButton.forcedWidth = 110;
			(cancelButton as SimpleButton).forcedWidth = 110;
			(okButton as SimpleButton).forcedWidth = 110;
			
			var margin : Number = 10;
			var posX : Number = ( bg.width - (okButton.width + cancelButton.width + donotSaveButton.width + margin*2)) /2;
			view.addChild(donotSaveButton);
			cancelButton.x = posX;
			donotSaveButton.x = cancelButton.x + cancelButton.width + margin;
			okButton.x = donotSaveButton.x + donotSaveButton.width + margin;
			
			super.open();
		}
		
		
		/**
		 * ------------------------------------ DO NOT SAVE HANDLER -------------------------------------
		 */
		
		private function doNotSaveHandler():void
		{
			DONOT_SAVE.dispatch(this);
			close();
		}
	}
}
		
	
