/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	
	import comp.FormInput;
	import comp.button.SimpleButton;
	
	import mcs.popup.simpleInputPopup;
	
	import utils.Colors;
	import utils.TextFormats;
	
	import view.popup.PopupAbstract;

	public class PopupSimpleInput extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		protected var view : simpleInputPopup = new simpleInputPopup();
		protected var bg:MovieClip;
		protected var title : TextField;
		protected var titleBkg : Sprite;
		protected var description : TextField;
		protected var donotSaveButton : SimpleButton;
		protected var inputField:FormInput;
		protected var defaultLabel : String;
		protected var doNotSaveHandler : Function;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PopupSimpleInput( inputLabel : String, titleKey:String , descKey:String = null, doNotSaveHandler:Function = null, maxChars:int = 50 ) 
		{
			this.doNotSaveHandler = doNotSaveHandler;
			defaultLabel = inputLabel;
			
			// view
			addChild(view);
			bg = view.bg;
			title = view.title;
			titleBkg = view.titleBkg;
			description = view.description;
			
			// POSITIONATE AND FILL
			title.autoSize = description.autoSize = "left";
			title.multiline = title.wordWrap = description.multiline = description.wordWrap =  true;
			
			title.text = ResourcesManager.getString(titleKey);
			var nextPosY : Number = titleBkg.y + titleBkg.height + 20;
			
			// description
			if(descKey){
				description.htmlText = ResourcesManager.getString(descKey);
				description.y = nextPosY;
				nextPosY = description.y + description.height + 20;
			} else {
				description.visible = false;
			}
			
			// input field
			inputField = new FormInput(view.folderField, inputLabel,false,maxChars,NaN,null,false);
			inputField.y = nextPosY;
			nextPosY = inputField.y + inputField.height + 20;
			
			
			// BUTTONS
			okButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
			replaceButton(view.okButton, okButton, true);
			cancelButton = SimpleButton.createPopupCancelButton("common.cancel",cancelHandler);
			replaceButton(view.cancelButton, cancelButton);
			donotSaveButton = SimpleButton.createBlueButton("popup.project.save.warning.donotsave.button",doNotSaveClicked);
			
			okButton.y = cancelButton.y = donotSaveButton.y = nextPosY;
			nextPosY += okButton.height + 5;
			
			if( doNotSaveHandler != null ){
				view.addChild(donotSaveButton);
				donotSaveButton.forcedWidth = 110;
				(cancelButton as SimpleButton).forcedWidth = 110;
				(okButton as SimpleButton).forcedWidth = 110;
				var margin : Number = 10;
				var posX : Number = ( bg.width - (okButton.width + cancelButton.width + donotSaveButton.width + margin*2)) /2;
				cancelButton.x = posX;
				donotSaveButton.x = cancelButton.x + cancelButton.width + margin;
				okButton.x = donotSaveButton.x + donotSaveButton.width + margin;
			}
			else
			{
				donotSaveButton.visible = false;
				(cancelButton as SimpleButton).forcedWidth = 140;
				(okButton as SimpleButton).forcedWidth = 140;
				
				margin = 10;
				posX  = ( bg.width - (okButton.width + cancelButton.width + margin)) /2;
				cancelButton.x = posX;
				okButton.x = cancelButton.x + cancelButton.width + margin;
			}
			
			bg.height=nextPosY + 20;
			
			super();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public function get fieldValue():String
		{
			if(inputField.text == "") return defaultLabel;
			return inputField.text;
		}
		
		/**
		 * close current popup
		 */
		public function doNotSaveClicked(e:MouseEvent = null):void
		{
			doNotSaveHandler.call();
			super.close();
		}
		
		
		
		/**
		 * close current popup
		 */
		override public function close():void
		{
			doNotSaveHandler = null;
			super.close();
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
	}
}