/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.form.Watermark;
	import be.antho.utils.FormValidator;
	
	import comp.button.SimpleButton;
	
	import data.Infos;
	
	import flashx.textLayout.formats.TextAlign;
	
	import mcs.popup.defaultPopup;
	import mcs.popup.errorPopup;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.TextUtils;
	
	import view.popup.PopupAbstract;

	public class PopupError extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var view : errorPopup = new mcs.popup.errorPopup();
		private var ticketID: String;
		
		private var emailInputField : TextField; // offline only
		private var feedbackWatermark : String="";
		private var emailWatermark:String="";
		private var feedback:TextField;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PopupError( ticketId:String ) 
		{ 
			addChild(view);
			feedback = view.feedback;
			
			this.ticketID = ticketId;
			
			if(ResourcesManager.ready)
			{
				view.title.htmlText = ResourcesManager.getString("popup.error.title");
				view.feedbackTitle.htmlText = ResourcesManager.getString("popup.error.title.feedback.title");
				view.feedbackDescription.htmlText = ResourcesManager.getString("popup.error.title.feedback");
				view.description.htmlText = ResourcesManager.getString("popup.error.description");
				view.version.htmlText =  ResourcesManager.getString("popup.error.version") + Infos.buildString;
			}
			else
			{
				view.title.htmlText = "Oops !? An error occured..";
				view.feedbackTitle.htmlText = "Please, tell us what happened! ";
				view.feedbackDescription.htmlText = "What was your last action on the app, give us as much details as you want. it will help us to make the app better. Thank you!";
				view.description.htmlText = "We are sorry but the application did just crash and a report has been sent to the support team.<br/><br />We will now try to reload the previous state of your project."
				view.version.htmlText = "Editor version : " + Infos.buildString;
			}
			
			(view.ticket as TextField).multiline = true;
			(view.ticket as TextField).wordWrap = true;
			(view.ticket as TextField).autoSize = "left";
			(view.ticket as TextField).y -=7;
			view.ticket.htmlText =  "Your Ticket Reference : <br />" + ticketId;
		
			registerOkButton(view.okButton);
			
			okButton = SimpleButton.createPopupOkButton("popup.error.button",sendFeedback);
			(okButton as SimpleButton).forcedWidth = 200;
			replaceButton(view.okButton, okButton, true);
			
			//make sure it's empty (condition to send feedback or not)
			feedback.text = "";
			
			
			// FOR DESKTOP APP, ASK FOR EMAIL
			if ( Infos.IS_DESKTOP )
			{
				// create input field
				emailInputField = TextUtils.createInputField("",TextField(feedback).defaultTextFormat,feedback.x,feedback.y,200,20);
				emailInputField.autoSize = TextFieldAutoSize.NONE;
				emailInputField.width = feedback.width;
				emailInputField.border = true;
				view.addChild(emailInputField);
				
				// resources
				if(ResourcesManager.ready)
				{
					emailWatermark = ResourcesManager.getString("popup.error.email.watermark");
					feedbackWatermark = ResourcesManager.getString("popup.error.feedback.watermark");
				}
				else
				{
					emailWatermark = "Enter your Email here so we can contact you.";
					feedbackWatermark = "Enter the description of your problem here.";
				}
				
				var ewm : Watermark = new Watermark(emailInputField, emailWatermark );
				var fwm : Watermark = new Watermark(feedback, feedbackWatermark );
				
				// auto fill with email if exists
				if(Infos.session.email != null) emailInputField.text = Infos.session.email;
				else emailInputField.text = emailWatermark;
					
				// layout
				feedback.height -=25;
				feedback.y +=25;
				feedback.text = feedbackWatermark;
				
				emailInputField.backgroundColor = Colors.WHITE;
				emailInputField.borderColor = Colors.GREY_LIGHT;
				/*
				inputLabel.x = view.title.x;
				inputLabel.y = inputField.y = feedbackDescription.y + feedbackDescription.height + 15;
				*/
				
			}
			
			feedback.backgroundColor = Colors.WHITE;
			feedback.borderColor = Colors.GREY_LIGHT;
			
			
			super();
			cancelOnModalClick = false;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		private function sendFeedback():void
		{
			okButton.mouseEnabled = okButton.mouseChildren = false;
			okButton.alpha = 0.2;
			
			var sendFlag : Boolean = false;
			
			// save email
			if(emailInputField)
			{
				if( emailInputField.text != "" && emailInputField.text != emailWatermark && FormValidator.isEmailFieldValid(emailInputField.text) == FormValidator.FIELD_VALID )
				{
					Infos.session.email = emailInputField.text;
					SharedObjectManager.instance.write("userEmail", Infos.session.email); // save email
					sendFlag = true;
				}
			}
			
			
			if(feedback.text != feedbackWatermark && feedback.text != "")
			{
				sendFlag = true;
			}
			
			if(sendFlag) Debug.sendDevMail(feedback.text, false, true, sendFeedBackSuccessHandler);
			else okHandler();
		}
		
		private function sendFeedBackSuccessHandler(e:*):void
		{
			okHandler();
		}
	}
}