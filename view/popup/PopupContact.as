/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup 
{
	import com.bit101.components.InputText;
	import com.bit101.components.List;
	import com.bit101.components.ListItem;
	import com.bit101.components.Text;
	import com.bit101.components.TextArea;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import mx.resources.ResourceManager;
	
	import be.antho.data.DateUtil;
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.form.Watermark;
	import be.antho.utils.FormValidator;
	
	import comp.button.InfosButton;
	import comp.button.SimpleButton;
	import comp.checkbox.CheckboxTitac;
	import comp.combobox.ComboBoxTitTac;
	import comp.inputtext.InputTextTicTac;
	import comp.label.LabelTictac;
	import comp.textarea.TextAreaTictac;
	
	import data.Infos;
	import data.ProjectVo;
	
	import manager.ProjectManager;
	import manager.SessionManager;
	import manager.UserActionManager;
	
	import mcs.popup.defaultPopup;
	
	import offline.data.OfflineProjectManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.popup.PopupAbstract;

	public class PopupContact extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		/*public static const TOPIC_BUG:String = "bug";
		public static const TOPIC_QUESTION:String = "question";
		public static const TOPIC_SUGGESTION:String = "suggestion";*/
		
		// const
		private const margin : int = 20;
		
		// ui
		private var view : defaultPopup = new defaultPopup();
		private var title : TextField;
		private var titleBkg : Sprite;
		private var description : TextField;
		private var topicComboBox : ComboBoxTitTac;
		private var emailInput : InputTextTicTac;	
		private var messageInput : TextAreaTictac;	
		private var bg : MovieClip

		
		/**
		 * ------------------------------------ STATIC METHODS -------------------------------------
		 */
		public static function OPEN():void
		{
			if(Infos.IS_DESKTOP && !Infos.INTERNET)
				PopupAbstract.Alert(ResourcesManager.getString("popup.no.internet.title"),ResourcesManager.getString("popup.no.internet.desc"),false);
			else
			{
				var popupContact:PopupContact = new PopupContact();					
				popupContact.open();
			}
		}
		

		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function PopupContact() 
		{ 
			// map
			addChild(view);
			title = view.title;
			description = view.description;
			titleBkg = view.titleBkg;
			
			//bg
			bg = view.bg;
			bg.width = 600;
			//bg.height = 360;
			
			//title
			title.autoSize = "center";
			title.x = description.x = margin;
			title.width = description.width = bg.width -2*margin;
			titleBkg.width = bg.width;
			title.text = ResourcesManager.getString("btn.contact.us");
			
			//remove description, not needed 
			description.parent.removeChild(description);

			//topic label
			var topicLabel:LabelTictac = new LabelTictac(view, margin, titleBkg.y + titleBkg.height + margin, ResourcesManager.getString("popup.contact.choose.topic"));
			topicLabel.textFormatUpdate(TextFormats.ASAP_BOLD(14,Colors.BLUE,"left"));
			
			//topic combo
			topicComboBox = new ComboBoxTitTac(this);
			var topicComboWidht:Number = titleBkg.width - topicLabel.x - topicLabel.width - 10 - 20;
			topicComboBox.setSize(topicComboWidht,30);
			topicComboBox.y = topicLabel.y - 5;
			topicComboBox.x = topicLabel.x + topicLabel.width + 10;
			
			//topics choices
			var selectedIndex:int = 0;
			//var topicsArray:Array  = [TOPIC_QUESTION,TOPIC_BUG,TOPIC_SUGGESTION];
			var topicsItems:Array = [];
			for (var i:int = 0; i < Infos.config.contactTopics.length; i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("popup.contact.topic."+Infos.config.contactTopics[i].id);
				item.id = String(Infos.config.contactTopics[i].id);
				topicsItems.push(item);
			}
			topicComboBox.items = topicsItems;
			topicComboBox.draw();
			topicComboBox.selectedIndex = 0;
			
			// input Email
			emailInput = new InputTextTicTac(this, 20, topicLabel.y + topicLabel.height + margin);
			emailInput.addWatermark(ResourcesManager.getString("popup.contact.topic.email.watermark"));
			emailInput.setSize(bg.width - 40,30);
			emailInput.setTabIndex(1);
			if(Infos.session.email && Infos.session.email != "")
				emailInput.text = Infos.session.email;
			
			// input Message
			messageInput = new TextAreaTictac(this,20,emailInput.y + emailInput.height + margin/2);
			messageInput.width = bg.width - 40;
			messageInput.height = 180;
			messageInput.addWatermark(ResourcesManager.getString("popup.contact.topic.message.watermark"));
			messageInput.setTabIndex(2);
			
			//Buttons
			okButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
			replaceButton(view.okButton, okButton, true);
			cancelButton = SimpleButton.createPopupCancelButton("common.cancel",cancelHandler);
			replaceButton(view.cancelButton, cancelButton);
			(cancelButton as SimpleButton).forcedWidth = 140;
			(okButton as SimpleButton).forcedWidth = 140;
			(cancelButton as SimpleButton).x = view.bg.width *.5 - cancelButton.width - 10;
			(okButton as SimpleButton).x = view.bg.width *.5 +10;
			cancelButton.y = messageInput.y + messageInput.height + margin;
			okButton.y = messageInput.y + messageInput.height + margin;
			
			//adjust BG
			bg.height = okButton.y + okButton.height + margin;
			
			
			
			

						
			super();
		}
		
		
		/**
		 * ------------------------------------ Getter/Setter -------------------------------------
		 */
		
		
		
		
		
		/**
		 * ----------------------------------- OPEN POPUP -------------------------------------
		 */
		
		
		
		protected override function introCompleted():void
		{
			super.introCompleted();
			//messageInput.watermark.reset();
		}
		
		
		
		
			
		/**
		 * ----------------------------------- CLOSE AND DISPOSE -------------------------------------
		 */
		
		public override function close():void
		{
			super.close();
		}
		
		/**
		 * destroy view
		 */
		override public function destroy():void
		{
			stageRef.removeEventListener(Event.RESIZE, onStageResizeHandler);
			if(modal) modal.removeEventListener(MouseEvent.CLICK, cancelHandler);
			if(parent) parent.removeChild(this);
			
			// remove signals
			CANCEL.removeAll();
			OK.removeAll();
		}
		
		/**
		 * ----------------------------------- OK HANDLER -------------------------------------
		 */
		protected override function okHandler(e:Event = null):void
		{
			//disable ui
			activateButtons(false);
			
			//Send function
			SendMessage();
			
		}
		private function okHandlerContinue():void
		{
			super.okHandler(null);
		}
		
		
		/**
		 * Send message to server
		 * Sucess and error handling
		 */
		private function SendMessage():void
		{
			Debug.log("POPUP CONTACT: Send message");
			// check and save email
			if( emailInput.text != "" && emailInput.text != emailInput.watermark.value && FormValidator.isEmailFieldValid(emailInput.text) == FormValidator.FIELD_VALID )
			{
				Infos.session.email = emailInput.text;
				SharedObjectManager.instance.write("userEmail", Infos.session.email); // save email
			}
			else 
			{
				PopupAbstract.Alert(ResourcesManager.getString("popup.userfeedback.error.title"), ResourcesManager.getString("popup.userfeedback.email.error"), false);
				activateButtons(true);
				return;
			}
			
			// check feedback is valid
			if( messageInput.text != "" && messageInput.text != messageInput.watermark.value )
			{
				//Send a copy to us
				ServiceManager.instance.sendContactMail(messageInput.text, Infos.session.email, topicComboBox.selectedItem.id);
				//Send content to support system flow
				ServiceManager.instance.sendContactToSupport(messageInput.text, Infos.session.email, topicComboBox.selectedItem.id, sendSuccessHandler,onFail );
				
			}
			else 
			{
				PopupAbstract.Alert(ResourcesManager.getString("popup.userfeedback.error.title"), ResourcesManager.getString("popup.userfeedback.feedback.error"), false);
				activateButtons(true);
				return;
			}
		}
		
		
		private function sendSuccessHandler(e:* = null):void
		{
			if(e == "success")
			{
				okHandlerContinue();
				PopupAbstract.Alert(ResourcesManager.getString("popup.userfeedback.thanks.title"), ResourcesManager.getString("popup.userfeedback.thanks.description"), false);
			}
			else
				onFail();
			
		}
		
		private function onFail(e:* = null):void
		{
			activateButtons(true);
			PopupAbstract.Alert(ResourcesManager.getString("connectioncheck.down.title"), ResourcesManager.getString("connectionCheck.down.desc"), false);
		}

		
		
		/**
		 * ----------------------------------- UI HANDLERS -------------------------------------
		 */
		
		
		/**
		 *
		 */
		private function onTextChange(e:Event):void
		{
			
		}
		
		
		/**
		 * ----------------------------------- HELPERS -------------------------------------
		 */
		private function activateButtons(flag:Boolean):void
		{
			okButton.mouseEnabled = 
				okButton.mouseChildren =
				cancelButton.mouseEnabled = 
				cancelButton.mouseChildren = flag;
			okButton.alpha = cancelButton.alpha = (flag)?1:0.2;
		}
		
			
		
	}
}