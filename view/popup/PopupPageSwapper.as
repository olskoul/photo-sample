package view.popup
{
	import comp.button.SimpleButton;
	
	import data.Infos;
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	import library.popup.page.swapper;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;

	public class PopupPageSwapper extends PopupAbstract
	{
		
		private var view : MovieClip = new library.popup.page.swapper();
		private var swapFrom:TextField;
		private var swapTo:TextField;
		private var moveFrom:TextField;
		private var moveTo:TextField;
		private var after:TextField;
		private var swap:SimpleButton;
		private var move:SimpleButton;
		public var SWAP:Signal = new Signal();
		public var MOVE:Signal = new Signal();
		
		public function PopupPageSwapper()
		{
			super();
			
			addChild(view);
			swapFrom = view.swapFrom;
			swapTo = view.swapTo;
			moveFrom = view.moveFrom;
			moveTo = view.moveTo;
			after = view.after;
			swap = new SimpleButton("Swap!",Colors.BLUE,swapHandler);
			move = new SimpleButton("Move!",Colors.BLUE,moveHandler);
			swap.x = width - swap.width - 20;
			move.x = width - move.width - 20;
			swap.y = 30;
			move.y = height - move.height - 30;
			addChild(move);
			addChild(swap);
		}
		
		private function moveHandler():void
		{
			trace("move!");
		}
		
		private function swapHandler():void
		{
			var from:int = int(swapFrom.text);
			var to:int = int(swapTo.text);
			
			//check
			if(from<0 || from>Infos.project.pageList.length-1)
			{
				PopupAbstract.Alert("La page "+from+" n'existe pas","Veuillez changer l'index de la page.",false);
				return;
			}
			
			if(to<0 || to>Infos.project.pageList.length-1)
			{
				PopupAbstract.Alert("La page "+to+" n'existe pas","Veuillez changer l'index de la page.",false);
				return;
			}
			
			//SWAP'em!!
			SWAP.dispatch(from,to);
			close();
		}
		
		/**
		 * destroy view
		 */
		override public function destroy():void
		{
			super.destroy();
			SWAP.removeAll();
			MOVE.removeAll();
		}
	}
}