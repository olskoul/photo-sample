package view.popup
{
	import com.bit101.components.Label;
	import com.bit101.components.ListItem;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import data.TooltipVo;
	
	import mcs.icons.thumbDeleteIcon;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.TextFormats;
	
	public class ProjectListItem extends ListItem
	{
		public static const DELETE:Signal = new Signal(ProjectListItem);
		public static var isSavePopup : Boolean = false; 
		
		private var _deleteIcon : thumbDeleteIcon = new thumbDeleteIcon();
		private var _dateLabel : Label;
		private var _classLabel : Label;
		
		
		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function ProjectListItem(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, data:Object=null)
		{
			super(parent, xpos, ypos, data);
		}
		
		/**
		 * Initilizes the component.
		 */
		protected override function init() : void
		{
			super.init();
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			setSize(100, 30);
		}
		
		protected override function addChildren() : void
		{
			super.addChildren();
			_label = new Label(this, 5, 0);
			_label.width = 240;
			_label.draw();
			
			_classLabel = new Label(this, 260, 0);
			_classLabel.multiline = false;
			_classLabel.draw();
			
			_dateLabel = new Label(this, 525, 0);
			_dateLabel.draw();
			
			// delete icon (online only)
			CONFIG::offline
			{
				addChild(_deleteIcon);
				ButtonUtils.makeButton(_deleteIcon, deleteThumbHandler, new TooltipVo("tooltip.deleteProject.info",TooltipVo.TYPE_SIMPLE, null, Colors.RED_FLASH)); 
				_deleteIcon.scaleX = _deleteIcon.scaleY = 0.75;
			}
		}
		
		
		/**
		 * ------------------------------------ DRAW -------------------------------------
		 */
		
		/**
		 * Draws the visual ui of the component.
		 */
		public override function draw() : void
		{
			//super.draw();
			graphics.clear();
			graphics.lineStyle(1,0xededed);
			
			// delete icon
			_deleteIcon.visible = (!isSavePopup && (_mouseOver || _selected));
			_deleteIcon.x = _width - _deleteIcon.width - 20;
			_deleteIcon.y = Math.round(height - _deleteIcon.height >> 1);
				
			if(isSavePopup)
			{
				graphics.beginFill(Colors.WHITE);
			}
			else if(_selected)
			{
				graphics.beginFill(Colors.BLUE_LIGHT, 1);
			}
			else if(_mouseOver)
			{
				graphics.beginFill(Colors.BLUE_LIGHT,0.5);
			}
			else
			{
				graphics.beginFill(0xffffff);
			}
			graphics.drawRect(0, 0, width-1, height);
			graphics.endFill();
			
			if(_data == null || _data == "") return;
			
			_label.text = _data.label;
			_dateLabel.text = _data.date;
			_classLabel.text = _data.type;
			
			_label.y = _dateLabel.y = _classLabel.y = Math.round(height - _label.height >> 1);
			_label.x = Math.round(_label.x);
			
			var textColor : Number = (!isSavePopup && _selected)? Colors.BLUE : Colors.GREY_DARK;
			var tf:TextFormat = (_data.isCurrent)? TextFormats.ASAP_BOLD(12,textColor) : TextFormats.ASAP_REGULAR(12,textColor);
			
			// apply
			_label.textField.defaultTextFormat = 
				_dateLabel.textField.defaultTextFormat = 
				_classLabel.textField.defaultTextFormat = tf;	

		}
		
		
		/**
		 *
		 */
		private function deleteThumbHandler(e:Event = null):void
		{
			DELETE.dispatch(this);
		}
	}
}
