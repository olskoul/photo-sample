/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import com.adobe.utils.CRC32;
	import com.google.analytics.debug.Info;
	
	import flash.display.BitmapData;
	import flash.display.JPEGEncoderOptions;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLLoaderDataFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.ByteArray;
	
	import mx.validators.EmailValidator;
	
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.form.Watermark;
	import be.antho.utils.FormValidator;
	
	import comp.button.SimpleButton;
	import comp.inputtext.InputTextTicTac;
	import comp.textarea.TextAreaTictac;
	
	import data.Infos;
	
	import deng.fzip.FZip;
	
	import manager.SessionManager;
	
	import mcs.popup.errorPopup;
	
	import ru.inspirit.net.MultipartURLLoader;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextUtils;
	import utils.TimeStamp;
	
	public class PopupSendLogs extends PopupAbstract
	{
		
		/**
		 * ------------------------------------ PROPERTIES -------------------------------------
		 */		
		
		private var view : errorPopup = new mcs.popup.errorPopup();
		private var ticketID: String;
		
		private var emailInputField : InputTextTicTac; // offline only
		private var feedbackWatermark : String="";
		private var emailWatermark:String="";
		
		
		

		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function PopupSendLogs() 
		{ 
			addChild(view);
			
			// hide unused items
			//view.title.visible = false;
			view.description.visible = false;
			(view.ticket as TextField).visible = false;
			view.feedback.visible = false;
			
			// update bg
			var bg : MovieClip = (view as MovieClip).getChildAt(0) as MovieClip;
			
			// update content
			view.feedbackDescription.autoSize = "left";
			// view.feedbackTitle.htmlText = ResourcesManager.getString("popup.userlog.title");
			view.title.htmlText = ResourcesManager.getString("popup.userlog.title");
			view.feedbackDescription.htmlText = ResourcesManager.getString("popup.userlog.description");
			view.version.htmlText =  ResourcesManager.getString("popup.error.version") + Infos.buildString;
			
			view.feedbackTitle.y = 15;
			view.feedbackDescription.y = view.feedbackTitle.y + view.feedbackTitle.height + 20;
			//view.feedback.y =  view.feedbackDescription.y + view.feedbackDescription.height + 30;
			

			emailWatermark = ResourcesManager.getString("popup.userfeedback.email.watermark");
			//feedbackWatermark = ResourcesManager.getString("popup.userfeedback.feedback.watermark");

			
			//make sure it's empty (condition to send feedback or not)
			view.feedback.text = "";
			
			
			// create input field
			//emailInputField = TextUtils.createInputField("",TextField(view.feedback).defaultTextFormat,view.feedbackDescription.x,view.feedbackDescription.y +view.feedbackDescription.height + 20,200,30);
			emailInputField = new InputTextTicTac(view);
			emailInputField.setSize(view.feedback.width, 30);
			emailInputField.addWatermark(emailWatermark);
			emailInputField.x = 20;
			emailInputField.y = view.feedbackDescription.y + view.feedbackDescription.height + 20;
			view.version.y = emailInputField.y + emailInputField.height + 2;
			
			// auto fill with email if exists
			var mail:String = Infos.session.email;
			if(Infos.session.email != null) emailInputField.text = Infos.session.email;
			else emailInputField.watermark.reset();
			
			// hide ticket id
			registerOkButton(view.okButton);
			
			// OK BUTTON
			okButton = SimpleButton.createPopupOkButton("common.ok",sendFeedback);
			(okButton as SimpleButton).forcedWidth = 200;
			replaceButton(view.okButton, okButton, true);
			okButton.y = emailInputField.y + emailInputField.height + 30;
			
			// CANCEL BUTTON
			cancelButton = SimpleButton.createPopupCancelButton("common.cancel",close);
			(cancelButton as SimpleButton).forcedWidth = 200;
			addChild(cancelButton);
			cancelButton.y = okButton.y;
			
			cancelButton.x = bg.width *.5 -cancelButton.width - 10;
			okButton.x = bg.width*.5 + 10;
			
			//BG
			bg.height = okButton.y + okButton.height + 20;
			
			// activate buttons
			activateButtons(true);
			
			super();
			cancelOnModalClick = false;
			
			
			
			
		}
		private function activateButtons(flag:Boolean):void
		{
			okButton.mouseEnabled = 
				okButton.mouseChildren =
				cancelButton.mouseEnabled = 
				cancelButton.mouseChildren = flag;
			okButton.alpha = cancelButton.alpha = (flag)?1:0.2;
		}
		
		
		/**
		 * ------------------------------------ ON SEND -------------------------------------
		 */
		
		private function sendFeedback():void
		{
			activateButtons(false);
			
			// check and save email
			if(emailInputField)
			{
				if( emailInputField.text != "" && emailInputField.text != emailWatermark && FormValidator.isEmailFieldValid(emailInputField.text) == FormValidator.FIELD_VALID )
				{
					Infos.session.email = emailInputField.text;
					SharedObjectManager.instance.write("userEmail", Infos.session.email); // save email
				}
				else 
				{
					PopupAbstract.Alert(ResourcesManager.getString("popup.userfeedback.error.title"), ResourcesManager.getString("popup.userfeedback.email.error"), false);
					activateButtons(true);
					return;
				}
			}
			
			// check feedback is valid
			//Update: No more message in this, user use the contact panel to send messages
			//if(view.feedback.text != feedbackWatermark && view.feedback.text != "")
			//{
				CONFIG::offline
				{
					// for offline, first send user files
					Debug.sendUserFiles(false, view.feedback.text,
						// on success
						sendFeedbackMail, 
						// on error
						onFail)
				}
				CONFIG::online
				{
					sendFeedbackMail();
				}
			/*}
			else 
			{
				PopupAbstract.Alert(ResourcesManager.getString("popup.userfeedback.error.title"), ResourcesManager.getString("popup.userfeedback.feedback.error"), false);
				activateButtons(true);
				return
			}*/
		}
		
		private function sendFeedBackSuccessHandler(e:* = null):void
		{
			okHandler();
			PopupAbstract.Alert(ResourcesManager.getString("popup.userfeedback.thanks.title"), ResourcesManager.getString("popup.userfeedback.thanks.description"), false);
		}
		
		private function sendFeedbackMail():void
		{
			Debug.sendDevMail(view.feedback.text, false, true, sendFeedBackSuccessHandler, sendFeedBackSuccessHandler);
		}
		
		private function onFail(e:*):void
		{
			activateButtons(true);
			PopupAbstract.Alert(ResourcesManager.getString("connectioncheck.down.title"), ResourcesManager.getString("connectionCheck.down.desc"), false);
		}
		
		
		
	}
}