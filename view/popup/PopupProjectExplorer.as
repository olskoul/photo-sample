/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import com.adobe.utils.StringUtil;
	import com.bit101.components.InputText;
	import com.bit101.components.List;
	import com.bit101.components.ListItem;
	import com.bit101.components.Text;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import mx.resources.ResourceManager;
	
	import be.antho.data.DateUtil;
	import be.antho.data.ResourcesManager;
	
	import comp.button.InfosButton;
	import comp.button.SimpleButton;
	import comp.checkbox.CheckboxTitac;
	import comp.inputtext.InputTextTicTac;
	
	import data.Infos;
	import data.ProjectVo;
	
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import mcs.popup.defaultPopup;
	
	import offline.data.OfflineProjectManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.Filters;
	import utils.TextFormats;
	
	import view.popup.PopupAbstract;

	public class PopupProjectExplorer extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// Signals
		public const PROJECT_OPEN:Signal = new Signal();
		public const PROJECT_RENAME:Signal = new Signal();
		public const PROJECT_DUPPLICATE:Signal = new Signal();
		public const PROJECT_DELETE:Signal = new Signal( String ); // project name
		
		// const
		public static const TYPE_SAVE : String 			= "TYPE_SAVE";			// Simple save
		public static const TYPE_SAVE_WARNING : String 	= "TYPE_SAVE_WARNING";	// Save warning popup when doing an action that would need a save ( mostly when leaving )
		public static const TYPE_OPEN : String 			= "TYPE_OPEN";			// Simple open project view
		
		// data
		public var isSaveAs : Boolean = false;
		private var type : String = TYPE_OPEN; 
		private var isBrowserLeave:Boolean;
		private var projectsXML:XML;
		
		// const
		private const margin : int = 20;
		private const listItemHeight:int = 25;
		
		// ui
		private var view : defaultPopup = new defaultPopup();
		private var title : TextField;
		private var titleBkg : Sprite;
		private var description : TextField;
		private var nameInput : InputText;
		private var donotSaveButton : SimpleButton; 
		private var projList : List;
		private var bg : MovieClip
		private var storageTypeCbx:CheckboxTitac;
		private var storageTypeCbxInfo:InfosButton;
		
		
		
		

		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function PopupProjectExplorer() 
		{ 
			// map
			addChild(view);
			title = view.title;
			description = view.description;
			titleBkg = view.titleBkg;
			
			bg = view.bg;
			bg.width = 750;
			bg.height = 600;
				
			title.autoSize = "center";
			title.x = description.x = margin;
			title.width = description.width = bg.width -2*margin;
			titleBkg.width = bg.width;
			
			description.setTextFormat(TextFormats.ASAP_BOLD(15, Colors.BLUE));
			description.autoSize = "left";
			
			
			// register buttons
			//registerOkButton(view.okButton);
			//registerCancelButton(view.cancelButton);
			
			okButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
			replaceButton(view.okButton, okButton, true);
			
			cancelButton = SimpleButton.createPopupCancelButton("common.cancel",cancelHandler);
			replaceButton(view.cancelButton, cancelButton);
			
			donotSaveButton = SimpleButton.createBlueButton("popup.project.save.warning.donotsave.button",doNotSaveHandler);
			donotSaveButton.forcedWidth = 140;
			
			(cancelButton as SimpleButton).forcedWidth = 140;
			(okButton as SimpleButton).forcedWidth = 140;
			(cancelButton as SimpleButton).x = view.bg.width *.5 - 170;
			(okButton as SimpleButton).x = view.bg.width *.5 +20;
			
			
			// project list
			projList = new List(this,margin);
			projList.backgroundAlpha = 0;
			projList.listItemClass = ProjectListItem;
			projList.alternateRows = true;
			projList.listItemHeight = listItemHeight;
			
			projList.showScrollButtons = false;
			projList.width = bg.width - 2*margin;
			
			// listeners
			projList.addEventListener(Event.SELECT, onListSelect);
			projList.addEventListener(MouseEvent.DOUBLE_CLICK, onListSelectAndContinue);
			ProjectListItem.DELETE.add(deleteProject);
			
			// input
			nameInput = new InputText(this,20);
			nameInput.restrict = "A-Z a-z_0-9\\-"; // restrict to letters, numbers, spaces, uppercase
			
			/*
			public static function sanitize( fileName:String ):String
			{
				var p:RegExp = /[:\/\\\*\?"<>\|%]/g;
				return fileName.replace( p, "" );
			}
			*/
			
			nameInput.shadow = false;
			//nameInput.shadowDist=3;
			nameInput.textFormat = TextFormats.ASAP_BOLD(15,Colors.BLUE);
			nameInput.leftMargin = 10;
			nameInput.backgroundColor = Colors.WHITE;
			nameInput.borderThickness = 1;
			nameInput.borderAlpha = .8;
			nameInput.borderColor = Colors.GREY_LIGHT;
			nameInput.filters = [Filters.INPUT_INNER_SHADOW];
			
			nameInput.addEventListener(Event.CHANGE, onTextChange);
			nameInput.width = bg.width - 40;
			nameInput.height = 30;
			nameInput.maxChars = 35;
			
			// offline storage type checkbox
			storageTypeCbx = new CheckboxTitac(this,margin,0, ResourcesManager.getString("popup.project.savetype.checkbox"), onStorageCheckBoxHandler,true,Colors.RED); 
			storageTypeCbx.texFormat = TextFormats.ASAP_BOLD(13,Colors.BLUE);
			storageTypeCbxInfo = new InfosButton("popup.project.savetype.info",this,0,0);
						
			super();
		}
		
		
		/**
		 * ------------------------------------ Getter/Setter -------------------------------------
		 */
		
		
		public function get nameLabel():String
		{
			return nameInput.text;
		}
		
		/**
		 * return list selected item
		 */
		public function get selectedItem():Object
		{
			return projList.selectedItem;
		}
		
		
		
		/**
		 * ----------------------------------- OPEN POPUP -------------------------------------
		 */
		
		
		/**
		 * is opening function but we need to retrieve list before
		 * Call this instead of open
		 */
		public function loadAndOpen(  _type:String = TYPE_OPEN, _isBrowserLeave : Boolean = false ):void
		{
			type = _type;
			isBrowserLeave = _isBrowserLeave
			
			// if warning, first open save warning
			if(type == TYPE_SAVE_WARNING)
			{
				openSaveWarning();
				return;
			}
			
			
			// First we retreive the current project list
			CONFIG::online
			{
				ServiceManager.instance.getProjectList(Infos.session.classname, isBrowserLeave, onProjectListLoadSuccess);
			}
				
			CONFIG::offline
			{
				OfflineProjectManager.instance.getProjectList(onProjectListLoadSuccess, onProjectListLoadError);
			}
		}
		private function onProjectListLoadError(e:ErrorEvent):void
		{
			Debug.warnAndSendMail("PopupProjectExplorer.onProjectListLoadError: "+e.toString());
			PopupAbstract.Alert(ResourcesManager.getString("popup.project.list.load.error.title"),ResourcesManager.getString("popup.project.list.load.error.description"),false);
		}
		private function onProjectListLoadSuccess(result:String):void
		{
			// retrieve project list xml
			projectsXML = new XML(result);
			
			// retrieve current project name
			var currentProject : String = (Infos.project)? Infos.project.name : null;
			
			// update popup depending on project list
			update(currentProject);
			
			// CASE if there is no current project
			if(type == TYPE_OPEN && projectsXML.project.length() < 1 )
			{
				if(type == TYPE_OPEN)
				{
					cancelHandler();
					PopupAbstract.Alert(ResourcesManager.getString("popup.project.open.noProject.title"),ResourcesManager.getString("popup.project.open.noProject.description"),false);
				}
				else
				{
					// openFieldOnlySavePopup(); // DO NOT USE THIS ANYMORE
				}
			}
			
			// open/display popup
			else open();
		}
		protected override function introCompleted():void
		{
			super.introCompleted();
			if(nameInput.visible) stage.focus = nameInput.textField;
		}
		
		
		
		/**
		 * ----------------------------------- UPDATE VIEW DEPENDING ON POPUP TYPE -------------------------------------
		 */
		
		
		private function update(currentProject : String ):void
		{
			// update layout
			if( type == TYPE_OPEN )
			{ 
				// we are in the open project popup
				isSaveAs = false;
				description.autoSize = "left";
				nameInput.visible = storageTypeCbx.visible = storageTypeCbxInfo.visible = false;
				projList.allowItemSelection = true;
				description.width = bg.width - 2*margin;// full width description
				ResourcesManager.setText(title, "popup.project.open.title");
				ResourcesManager.setText(description, "popup.project.open.description");
				description.y = titleBkg.y + titleBkg.height + margin;
				SimpleButton(okButton).setLabel("common.open");
			} 
			else // TYPE SAVE
			{ 
				// we are in the save project popup
				nameInput.visible = true;
				projList.allowItemSelection = false;
				description.autoSize = "right";
				SimpleButton(okButton).setLabel("common.save");
				if( !isSaveAs )
				{
					ResourcesManager.setText(title, "popup.project.save.title");
					ResourcesManager.setText(description, "popup.project.save.description");
				}
				else
				{
					ResourcesManager.setText(title, "popup.project.saveas.title"); 
					ResourcesManager.setText(description, "popup.project.saveas.description"); 
				}
				// description
				description.width = (bg.width - 2*margin)*.4;
				description.y = titleBkg.y + titleBkg.height + margin + 10;
				// name input
				nameInput.y = description.y - 5;
				nameInput.x = description.x + description.textWidth + 20;
				nameInput.width = bg.width - margin - nameInput.x;
				nameInput.text = (!isSaveAs)? ""+ currentProject : currentProject + " " + ResourcesManager.getString("common.copy.fileSuffix");
				// storage type checkbox
				if(Infos.IS_DESKTOP)
				{
					storageTypeCbx.visible = storageTypeCbxInfo.visible = true;
					storageTypeCbx.selected = (Infos.project.imageStorageSystem == ProjectVo.STORAGE_TYPE_COPY);
					storageTypeCbx.enabled = !Infos.project.isSaved;
					storageTypeCbx.x = nameInput.x;
					storageTypeCbxInfo.x = storageTypeCbx.x + storageTypeCbx.width + 15;
					storageTypeCbx.y = storageTypeCbxInfo.y = nameInput.y + nameInput.height + 10;
				}
				else
				{
					storageTypeCbx.visible = storageTypeCbxInfo.visible = false;
				}
			} 
		
			
			// PROJECT LIST
			if(projList) projList.removeAll(); 
			ProjectListItem.isSavePopup = (type != TYPE_OPEN );
			
			var item:ListItem;
			var node : XML;
			var selected : int = -1;
			var projDate : Date;
			var dateString:String;
			var detailString:String;
			var projectName:String
			for (var i:int = 0; i < projectsXML.project.length(); i++) 
			{
				node =  projectsXML.project[i];
				
				CONFIG::offline
				{
					projDate = new Date(new Date().setTime(Date.parse(node.last_modified.text())));
					dateString = DateUtil.toSimpleDateString(projDate);
					detailString = "" + node.detail.text();
					if(detailString == "") detailString = ResourcesManager.getString("class."+node.classname.text());
				}
				CONFIG::online
				{
					detailString = ResourcesManager.getString("class."+node.classname.text());
					dateString = "" + node.last_modified.text();
					dateString = dateString.substr(0,19);
				}
				
				projectName = ""+unescape(node.name.text());
				projList.addItem({	label:projectName,
					type:detailString,
					date:ResourcesManager.getString("projectList.modified.label") + " " +dateString, 
					name:projectName,
					id:""+node.id.text(), 
					classname:""+node.classname.text(), 
					isCurrent:false});
				if(currentProject){
					if(	Infos.project.id == null && ""+node.name.text() == Infos.project.name // search on name first (security)
						|| ""+node.id.text() == Infos.project.id ) // search on id 
					{ 
						selected = i;
						projList.items[i].isCurrent = true;
					}
				}
			}
				
			// case no item is selected in the list
			if(selected == -1)
			{
				if(type == TYPE_OPEN) {
					selected = 0; // if we open a project, force project at index 0
				}
				if(Infos.project && Infos.project.id) Debug.warn("PopupProjectExplorer.update : no project found with same id : "+ Infos.project.id);
			} 
			
			
			// update projList height
			var projListHeight : int = projList.items.length * listItemHeight;
			if(projListHeight > listItemHeight*6) projListHeight = listItemHeight*6;
			else if (projListHeight < listItemHeight*4) projListHeight = listItemHeight*4;
			projList.height = projListHeight;
			projList.draw();
			projList.selectedIndex = selected;
			projList.autoHideScrollBar = true ; 
			
			
			// project list position
			if(storageTypeCbx.visible){
				projList.y = storageTypeCbx.y + storageTypeCbx.height + 25;	
			}
			else if(nameInput.visible){
				projList.y = nameInput.y + nameInput.height + 25;	
			}
			else{
				projList.y = description.y + description.height + 25;
			}
			
			// buttons position
			okButton.y = projList.y + projList.height + 30 ;
			cancelButton.y = okButton.y;
			donotSaveButton.y = okButton.y;
			bg.height = okButton.y + okButton.height +20;
			
				
			// button x position
			donotSaveButton.forcedWidth = 140;
			(cancelButton as SimpleButton).forcedWidth = 140;
			(okButton as SimpleButton).forcedWidth = 140;
			
			if(donotSaveButton.parent) donotSaveButton.parent.removeChild(donotSaveButton);
			var posX:int  = ( bg.width - (okButton.width + cancelButton.width + margin)) /2;
			cancelButton.x = posX;
			okButton.x = cancelButton.x + cancelButton.width + margin;
		}
		
		
		
		/**
		 * ----------------------------------- OPEN PROJECT -------------------------------------
		 */
		
		private function openProject():void
		{}
		private function renameProject():void
		{}
		private function dupplicateProject():void
		{}
		
		
		/**
		 * ----------------------------------- DELETE PROJECT (offline) -------------------------------------
		 */
		
		
		private function deleteProject( projItem : ProjectListItem):void
		{
			Debug.log("PopupProjectExplorer.deleteProject : "+projItem.data.name);
			var onDeleteConfirm:Function = function( p:PopupAbstract) : void
			{
				close();
				PROJECT_DELETE.dispatch( ""+projItem.data.name );
			}
			PopupAbstract.Alert(ResourcesManager.getString("popup.delete.warning.title"),ResourcesManager.getString("popup.delete.warning.description1") +"\n"+projItem.data.name+"\n"+ResourcesManager.getString("popup.delete.warning.description2"), true, onDeleteConfirm); 
			 
		}
		
		

		
		
			
		/**
		 * ----------------------------------- CLOSE AND DISPOSE -------------------------------------
		 */
		
		public override function close():void
		{
			super.close();
			ProjectManager.PROJECT_DO_NOT_SAVE.removeAll();
		}
		
		/**
		 * do not save handler
		 */
		private function doNotSaveHandler(e:Event = null):void
		{
			UserActionManager.instance.actionsSinceLastSave = 0 // be sure to not enter in a loop of javascript popup, we just set the last action save to 0
			ProjectManager.PROJECT_DO_NOT_SAVE.dispatch();
			close();
		}
		
		
		/**
		 * ----------------------------------- OK HANDLER -------------------------------------
		 */
		protected override function okHandler(e:Event = null):void
		{
			// case open
			if (type == TYPE_OPEN) okHandlerContinue();
			
			// case save
			else
			{
				// check project name
				var newProjectName:String = nameInput.text;
				
				// trim possible spaces at start or end
				newProjectName = StringUtil.trim(newProjectName);
				
				// check if we already have a project named like this
				if( checkIfProjectNameExists(newProjectName) ){
					PopupAbstract.Alert(ResourcesManager.getString("popup.project.save.nameExists.title"), ResourcesManager.getString("popup.project.save.nameExists.description"), false );
					return;
				}
				
				// change project name
				if(newProjectName != "") Infos.project.name = newProjectName;
				
				// change image storage system
				if(Infos.IS_DESKTOP && !Infos.project.isSaved) Infos.project.imageStorageSystem = (storageTypeCbx.selected)?ProjectVo.STORAGE_TYPE_COPY : ProjectVo.STORAGE_TYPE_REFERENCE;
				
				// continue
				okHandlerContinue();
			}
			
		}
		private function okHandlerContinue():void
		{
			super.okHandler(null);
		}
		
		
		
		/**
		 * ----------------------------------- INTERMEDIATE SAVE WARNING POPUP -------------------------------------
		 */
		
		private function openSaveWarning( p : PopupAbstract = null):void
		{
			// listeners
			var onSaveContinue:Function = function(p:PopupAbstract):void
			{
				clearWarningPopup();
				loadAndOpen(TYPE_SAVE,isBrowserLeave);
			}
			var onDoNotSave:Function = function(p:PopupAbstract):void
			{
				clearWarningPopup();
				doNotSaveHandler();
			}
			var onSaveCancel:Function = function(p:PopupAbstract):void
			{
				clearWarningPopup();
				saveCancel(p);
			}
			var clearWarningPopup:Function = function():void
			{
				warningPopup.OK.removeAll();
				warningPopup.DONOT_SAVE.removeAll();
				warningPopup.CANCEL.removeAll();
			}
			
			// popup
			var warningPopup : PopupSaveWarning = new PopupSaveWarning();
			warningPopup.OK.add(onSaveContinue);
			warningPopup.DONOT_SAVE.add(onDoNotSave);
			warningPopup.CANCEL.add(onSaveCancel);
			warningPopup.open();	
		}
		
		

		/**
		 * ----------------------------------- EXTERNAL SAVE ACTION -------------------------------------
		 */
		
		
		
		private function openFieldOnlySavePopup( p : PopupAbstract = null):void
		{
			var savePopup : PopupSimpleInput = new PopupSimpleInput(Infos.project.name,"popup.project.save.title","popup.project.save.description");
			savePopup.open();
			savePopup.OK.add(saveNameSelected);
			savePopup.CANCEL.add(saveCancel);
		}
		private function saveNameSelected(p:PopupAbstract):void
		{
			var newProjectName:String = (p as PopupSimpleInput).fieldValue;
			
			if( checkIfProjectNameExists(newProjectName) ){
				PopupAbstract.Alert(ResourcesManager.getString("popup.project.save.nameExists.title"), ResourcesManager.getString("popup.project.save.nameExists.description"), false, openFieldOnlySavePopup );
				return;
			}
			
			// Check if name already exist or not and if so, it must have the same id ( = override of same project), otherwise we must popup a warning
			if(newProjectName != "")Infos.project.name = newProjectName;
			okHandler();
		}
		private function saveCancel(p:PopupAbstract):void
		{
			ProjectManager.PROJECT_SAVE_CANCEL.dispatch();
			cancelHandler();
		}
		
		
		
		/**
		 * ----------------------------------- UI HANDLERS -------------------------------------
		 */
		
		private function onStorageCheckBoxHandler(e:Event):void
		{
			// TODO
		}
		
		
		
		/**
		 * Double click handler...
		 */
		private function onListSelectAndContinue(e:Event):void
		{
			if( type == TYPE_OPEN ) okHandler(null);
		}
		
		
		/**
		 *
		 */
		private function onListSelect(e:Event):void
		{
			
		}
		
		/**
		 *
		 */
		private function onTextChange(e:Event):void
		{
			
		}
		
		
		/**
		 * ----------------------------------- HELPERS -------------------------------------
		 */
		
		
		private function checkIfProjectNameExists( newProjectName : String ):Boolean
		{
			var node : XML;
			var currentProjectId : String = (Infos.project)? Infos.project.id : null ;
			newProjectName = newProjectName.toLowerCase();
			for (var i:int = 0; i < projectsXML.project.length(); i++) 
			{
				node =  projectsXML.project[i];
				var projName : String = node.name.text();
				projName = projName.toLowerCase();
				
				// if project name exists 
				if( projName == newProjectName)
				{
					// if this is a save as (copy) return true
					if(isSaveAs)  return true;
						//  project ids are different, return true
					else if(currentProjectId != node.id.text()) return true;
				}
			}
			return false;
		}
			
		
	}
}