﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/
package view.popup 
{
	import com.bit101.components.PushButton;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Strong;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	
	import data.Infos;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.Debug;

	public class PopupAbstract extends Sprite
	{
		// static const
		//public static const INPUT_INNER_SHADOW : DropShadowFilter = new DropShadowFilter(2,45,0,.3,9,9,1,1,true);
		public static const INPUT_INNER_SHADOW : GlowFilter = new GlowFilter(0,.3,6,6,1,1,true);
		
		// static vars
		public static var stageRef : Stage;
		public static var popupsOpen :int = 0;
		public static var popupList : Vector.<PopupAbstract>; // should be used to register popups
		
		// signals
		public var OK:Signal = new Signal(PopupAbstract);
		public var CANCEL:Signal = new Signal(PopupAbstract);
		
		public var cancelOnModalClick : Boolean = true;
		
		// views
		protected var buttonContainer : Sprite;
		protected var okButton : Sprite;
		protected var cancelButton : Sprite;
		protected var modal : Sprite;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PopupAbstract() 
		{
			this.visible = false;
		}
		
		////////////////////////////////////////////////////////////////
		//	Static methods
		////////////////////////////////////////////////////////////////
		
		public static function Alert(title:String, description:String, hasCancelButton:Boolean = true, okHandler:Function = null, cancelHandler:Function = null, hasCloseButton:Boolean = false, buttonLabelList:Array = null, buttonFunctionList:Array = null, buttonLabelAsString:Boolean = false):void
		{
			var alertPopup : PopupDefault = new PopupDefault(title, description,hasCancelButton,hasCloseButton,buttonLabelList,buttonFunctionList,true,buttonLabelAsString);
			if(okHandler != null) alertPopup.OK.add(okHandler);
			if(hasCancelButton && cancelHandler != null){
				alertPopup.CANCEL.add(cancelHandler);
			}
			alertPopup.cancelOnModalClick = false;
			alertPopup.open();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * this can be overriden to give the specific open point in Y for the popup
		 * by default, it opens on the center
		 */
		protected function get openY(): Number 
		{
			var posY : Number = Math.round(stageRef.stageHeight*.5 - height*.5);
			return posY;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *	Open a popup centered on stage
		 */
		public function open():void
		{
			if(!stageRef) throw new Error("Popup has no reference to stage, please use Poupup.stageRef = this.stage on root class");
			if(!modal){
				modal = new Sprite();
				modal.graphics.beginFill(0x000000,.4);
				modal.graphics.drawRect(0,0,10,10);
				modal.graphics.endFill();
			}
			if( cancelOnModalClick) modal.addEventListener(MouseEvent.CLICK, cancelHandler);
			
			
			this.visible = false;
			Infos.blockMouse(true);
			
			// show modal
			modal.width = stageRef.stageWidth;
			modal.height = stageRef.stageHeight;
			stageRef.addChild(modal);
			
			// add popup
			stageRef.addChild(this);
			
			var initScale : Number = .1;
			var posX : Number = Math.round(stageRef.stageWidth*.5 - width*.5);
			var posY : Number = openY
			TweenMax.to(this, .3, {delay:.1, autoAlpha:1, y:posY,x:posX, scaleX:1, scaleY:1, ease:Back.easeOut, onComplete:introCompleted, startAt:{y:stageRef.stageHeight*.5, x:stageRef.stageWidth*.5, scaleX : initScale, scaleY:initScale, autoAlpha:0}});
			
			
			stageRef.addEventListener(Event.RESIZE, onStageResizeHandler);
			popupsOpen ++;
		}
			
		
		
		/**
		 * close current popup
		 */
		public function close():void
		{
			popupsOpen --;
			if(modal && modal.parent) modal.parent.removeChild(modal);
			
			this.visible = false;
			destroy();
		}
		
		
		/**
		 * destroy view
		 */
		public function destroy():void
		{
			stageRef.removeEventListener(Event.RESIZE, onStageResizeHandler);
			if(modal) modal.removeEventListener(MouseEvent.CLICK, cancelHandler);
			if(parent) parent.removeChild(this);

			// remove signals
			CANCEL.removeAll();
			OK.removeAll();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 *
		 */
		protected function introCompleted():void
		{
			Infos.blockMouse(false);
		}
		
		/**
		 *
		 */
		protected function registerOkButton(btn : Sprite):void
		{
			okButton = btn;
			ButtonUtils.makeButton(btn, okHandler);
		}
		
		/**
		 *
		 */
		protected function registerCancelButton(btn : Sprite):void
		{
			cancelButton = btn;
			ButtonUtils.makeButton(btn, cancelHandler);
		}
		
		/**
		 *
		 */
		protected function okHandler(e:Event = null):void
		{
			OK.dispatch(this);
			close();
		}
		
		
		/**
		 * replace button by new one
		 */
		protected function replaceButton( buttonToReplace : Sprite, newButton:Sprite, alignOnRight : Boolean = false ):void
		{
			newButton.y = buttonToReplace.y;
			
			if(alignOnRight) newButton.x = buttonToReplace.x + buttonToReplace.width - newButton.width;
			else newButton.x = buttonToReplace.x;
			
			if(buttonToReplace.parent){
				buttonToReplace.parent.addChildAt(newButton, buttonToReplace.parent.getChildIndex(buttonToReplace));
				buttonToReplace.parent.removeChild(buttonToReplace);
			}
		}
		
		/**
		 *
		 */
		protected function cancelHandler(e:Event = null):void
		{
			CANCEL.dispatch(this);
			close();
		}
		
		
		/**
		 *
		 */
		protected function onStageResizeHandler(e:Event):void
		{
			var posX : Number = Math.round(stageRef.stageWidth*.5 - width*.5);
			var posY : Number = Math.round(stageRef.stageHeight*.5 - height*.5);
			modal.width = stageRef.stageWidth;
			modal.height = stageRef.stageHeight;
			TweenMax.to(this, 0, {y:posY,x:posX, ease:Back.easeOut});
		}
		
			
	}
	
}