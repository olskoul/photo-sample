/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.utils.FormValidator;
	
	import com.greensock.TweenMax;
	
	import comp.DefaultTooltip;
	import comp.FormInput;
	import comp.button.SimpleButton;
	
	import data.TooltipVo;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import library.popup.create.account;
	
	import mcs.popup.loginPopup;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	public class PopupCreateAccount extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var view : MovieClip = new library.popup.create.account();
		private var loginField : FormInput;
		private var passField : FormInput;
		private var passCheckField : FormInput;
		private var tooltipVo:TooltipVo;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		
		public function PopupCreateAccount() 
		{ 
			addChild(view);
			
			ResourcesManager.setText(view.title,"popup.create.account.title");
			
			loginField = new FormInput(view.login, "");
			passField = new FormInput(view.pass, "", true);
			passCheckField = new FormInput(view.passCheck, "", true);
			
			ResourcesManager.setText(view.login_label,"popup.create.account.watermark.login");
			ResourcesManager.setText(view.pass_label,"popup.create.account.watermark.pass");
			ResourcesManager.setText(view.passcheck_label,"popup.create.account.watermark.pass.check");
			
			var ok:SimpleButton = SimpleButton.createPopupOkButton("common.ok",okHandler);
			ok.y = passCheckField.y + passCheckField.height + 20;
			ok.x = width - ok.width - 20;
			view.addChild(ok);
			
			var cancel:SimpleButton = SimpleButton.createPopupCancelButton("common.cancel",cancelHandler);
			cancel.y = ok.y;
			cancel.x = ok.x - cancel.width - 20;
			view.addChild(cancel);
			
			
			super();
			cancelOnModalClick = false;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		

		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		protected override function okHandler(e:Event = null):void
		{
			loginField.showError(false);
			passField.showError(false);
			passCheckField.showError(false);
			
			// check infos in form
			var result:String = FormValidator.isEmailFieldValid(loginField.text, 3, loginField.waterMark);
			if( result != FormValidator.FIELD_VALID){
				loginField.showError();
				showErrorTooltip(result,loginField);
				return;
			}
			result = FormValidator.isFieldValid(passField.text, 3, passField.waterMark)
			if(result != FormValidator.FIELD_VALID){
				passField.showError();
				showErrorTooltip(result,passField);
				return;
			}
			
			if(passCheckField.text != passField.text)
			{
				passCheckField.showError();
				showErrorTooltip("PASSWORD_MISMATCH", passCheckField);
				return;
			}
			
			// check infos on server
			ServiceManager.instance.register(loginField.text, passField.text, passCheckField.text, onLoginSuccess);
		}
		
		private function showErrorTooltip(result:String, displayObject:DisplayObject):void
		{
			if(!tooltipVo)
			tooltipVo = new TooltipVo("form.error."+result,TooltipVo.TYPE_SIMPLE,null,Colors.RED, Colors.WHITE, TooltipVo.ARROW_TYPE_VERTICAL);
			else
				tooltipVo.labelKey = "form.error."+result;
			
			DefaultTooltip.tooltipOnClip(displayObject , tooltipVo, true);
			TweenMax.delayedCall(5,killToolTip);
		}		
		
		private function killToolTip():void
		{
			DefaultTooltip.killAllTooltips();
		}
		
		/**
		 *
		 */
		private function onLoginSuccess(result:Object):void
		{
			var sessionXML:XML = new XML(result);
			var userId : String = ""+sessionXML.session.user_id.text(); 
			if(userId !="" && userId != "0")
			{
				
				OK.dispatch(this);
				close();
				
			} else {
				PopupAbstract.Alert("Error", String(result).split("*ERROR: ")[1],false);
			}
		}
		
		
		
	}
}