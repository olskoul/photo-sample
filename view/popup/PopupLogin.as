/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.popup
{
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.utils.FormValidator;
	
	import comp.FormInput;
	import comp.button.SimpleButton;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import mcs.popup.loginPopup;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	public class PopupLogin extends PopupAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var view : loginPopup = new loginPopup();
		private var loginField : FormInput;
		private var passField : FormInput;
		private var forget : TextField;
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PopupLogin( isConnectionCheck : Boolean ) 
		{
			addChild(view);
			//registerOkButton(view.okButton);
			//registerCancelButton(view.cancelButton);
			
			ResourcesManager.setText(view.title,"popup.login.title");
			var tf:TextFormat = TextFormats.DEFAULT(12,Colors.GREY_DARK,TextFormatAlign.LEFT);
			tf.underline = true;
			ResourcesManager.setText(view.forget,"popup.login.forget",tf);
			ButtonUtils.makeHyperLink(view.forget,"http://editor.tictacphoto.com/wizard/login.php?fp");

			loginField = new FormInput(view.login, "popup.login.watermark.login");
			if(Debug.SHOW_PASSWORD) passField = new FormInput(view.pass, "popup.login.watermark.pass", false);
			else passField = new FormInput(view.pass, "popup.login.watermark.pass", true);
			
			var createAccount:SimpleButton = new SimpleButton("popup.login.account.create.btn",0x0067a0,function():void{openPageCreateAccountPopup();},Colors.WHITE, Colors.GREY_DARK, Colors.WHITE);
			createAccount.y = view.okButton.y;
			createAccount.x = 20;
			if(!isConnectionCheck) view.addChild(createAccount);
			
			//saved Password
			var savedLoginPass:Array = String(SharedObjectManager.instance.read("login")).split("::");
			if(savedLoginPass.length>1)
			{
				loginField.text = savedLoginPass[0];
				passField.text = savedLoginPass[1];
			}
			
			// replace ok button
			okButton = SimpleButton.createPopupOkButton("popup.login.ok.button", okHandler);
			(okButton as SimpleButton).forcedWidth = 120;
			replaceButton(view.okButton, okButton,true)
			
			view.cancelButton.visible = false;
			
			super();
			cancelOnModalClick = false;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Open account create popup
		 * popup for creatign an account
		 */
		private function openPageCreateAccountPopup():void
		{
			var popup:PopupCreateAccount = new PopupCreateAccount();
			popup.open();
			popup.OK.addOnce(accountCreated);
			popup.CANCEL.addOnce(accountCanceled);
			visible = false;
		}
		
		/**
		 *
		 */
		protected override function okHandler(e:Event = null):void
		{
			loginField.showError(false);
			passField.showError(false);
			
			// check infos in form
			if(FormValidator.isEmailFieldValid(loginField.text, 3, loginField.waterMark) != FormValidator.FIELD_VALID){
				loginField.showError();
				return;
			}
			else if(FormValidator.isFieldValid(passField.text, 3, passField.waterMark) != FormValidator.FIELD_VALID){
				passField.showError();
				return;
			}
			
			// check infos on server
			ServiceManager.instance.login(loginField.text, passField.text, onLoginSuccess);
		}
		
		/**
		 *
		 */
		private function accountCreated(p:PopupAbstract):void
		{
			OK.dispatch(this);
			close();
		}
		
		/**
		 *
		 */
		private function accountCanceled(p:PopupAbstract):void
		{
			visible = true;
		}
		
		
		/**
		 *
		 */
		private function onLoginSuccess(result:Object):void
		{
			var sessionXML:XML = new XML(result);
			var userId : String = ""+sessionXML.session.user_id.text(); 
			if(userId !="" && userId != "0")
			{
				//if(Debug.SAVE_PASSWORD)
				savePassword();
				
				OK.dispatch(this);
				close();
				
			} else {
				loginField.showError();
				passField.showError();
			}
		}
		
		private function savePassword():void
		{
			SharedObjectManager.instance.write("login",loginField.text+"::"+passField.text);
		}
		
		private function getSavedPassword():String
		{
			return SharedObjectManager.instance.read("login") as String;
		}
		
	}
}