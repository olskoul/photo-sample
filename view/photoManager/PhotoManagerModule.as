﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.photoManager
{
	import com.bit101.components.Component;
	import com.bit101.components.HBox;
	import com.bit101.components.PushButton;
	import com.bit101.components.ScrollPane;
	import com.bit101.components.Text;
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.sampler.NewObjectSample;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	import comp.ManagerThumbItem;
	import comp.PhotoItem;
	import comp.button.SimpleThinButton;
	import comp.label.LabelTictac;
	
	import data.AlbumVo;
	import data.Infos;
	import data.PhotoVo;
	import data.PreferencesVo;
	import data.ProjectVo;
	import data.TooltipVo;
	
	import library.photomanager.eye.icon;
	import library.photomanager.folder.line;
	import library.photomanager.folder.separation.text;
	
	import manager.FolderManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import mcs.defaultPanel;
	import mcs.photoManager.folderItem;
	
	import net.hires.utils.SpriteUtils;
	
	import photoManager.folder.activateall.icon;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.TextUtils;
	import utils.UIUtils;
	
	import view.edition.imageEditor.ImageEditorModule;
	import view.popup.PopupAbstract;
	import view.popup.PopupSimpleInput;
	import view.uploadManager.PhotoUploadManager;
	
	/**
	 * The photoManager view is the mondule to manage project albums and photos
	 * It also contains the photo uploader
	 */
	public class PhotoManagerModule extends PopupAbstract
	{
		// constants
		
		// view
		private var view : mcs.defaultPanel;
		private var bg : MovieClip;
		private var title : TextField;
		private var titleBkg : Sprite;
		private var closeButton : MovieClip;
		private var boxL : MovieClip;
		private var boxR : MovieClip;
				
		
		// @ view
		private var folderList:Sprite;
		private var activateAllBtn:HBox;
		private var activateAllLabel:LabelTictac;
		private var activateAllIcon:MovieClip;
		private var folderArea:ScrollPane;
		private var imageArea : ScrollPane;
		private var renameFolderButton : SimpleThinButton;
		private var newFolderButton : SimpleThinButton;
		private var deleteFolderButton : SimpleThinButton;
		private var insideUploadButton : SimpleThinButton;
		private var modifyButton : SimpleThinButton;
		private var uploadButton : SimpleThinButton;
		private var imageToDelete:ManagerThumbItem;
		private var thingsToClean:Array = [];
		
		// @ datas
		private var currentFolderIndex : int = 0;
		private var thumbList : Vector.<ManagerThumbItem>;
		private var folderItemList : Vector.<FolderListItem>;
		private var selectedAlbumVo : AlbumVo;
		private var tempFolderName : String;	// temp value to store new folder name when creation or renaming a folder
		private var needFolderUpdate : Boolean = false; // flag to know if we need an update for the folders in left menu
		
		private var allFolderAreActivated:Boolean;
		private var _isEditing:Boolean;
		
		
		
		private const WIDTH : int = 800;
		private const HEIGHT : int = 550;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		public function PhotoManagerModule(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("PhotoManagerModule is a singleton");
			
			// Construct view
			construct();	
		}
		
		////////////////////////////////////////////////////////////////
		// Instance
		////////////////////////////////////////////////////////////////
		
		private static var _instance : PhotoManagerModule;
		public static function get instance():PhotoManagerModule
		{
			if(!_instance) _instance = new PhotoManagerModule(new SingletonEnforcer());
			return _instance;
		}
		
		////////////////////////////////////////////////////////////////
		// GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		public function get isEditing():Boolean
		{
			return _isEditing;
		}
		public function set isEditing( value : Boolean):void
		{
			_isEditing = value;
		}
		
		protected override function get openY(): Number 
		{
			var posY : Number = Math.round(stageRef.stageHeight*.5 - bg.height*.5);
			return posY;
		}
		
		
		////////////////////////////////////////////////////////////////
		// GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		private function get albumList():Vector.<AlbumVo>
		{
			return SessionManager.instance.albumList;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHOD
		////////////////////////////////////////////////////////////////
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public override function open():void
		{	
			Debug.log("PhotoManager.open");
			dispose(); // security to be sure everything is ok
			
			// prepare view
			renderFolderList();
			selectFolder( 0 ); // force opening on current project folder
			
			//selectFolder(currentFolderIndex);
			
			super.open();
			
			// listeners
			SessionManager.IMAGE_LIST_UPDATE.add(updateComplete);
			ProjectManager.PHOTO_VO_MATCH_COMPLETE.add(updateUsedList);
		}
		
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public override function close():void
		{	
			Debug.log("PhotoManager > close");
			if(needFolderUpdate) updateAlbumAfterUploads();
			
			// be sure to remove temp update listener
			removeEventListener(Event.ENTER_FRAME, tempThumbUpdate);
			
			dispose();
			super.close();
		}
		
		/**
		 * on cancel handler
		 */
		private function cancel(e:Event):void
		{
			close();
		}
		
		
		/**
		 * Remove thumb list
		 * Remove folder list
		 * Remove listeners and enter frame
		 */
		private function dispose():void
		{
			clearFolders();
			clearThumbs();
			// listeners
			SessionManager.IMAGE_LIST_UPDATE.remove(updateComplete);
			ProjectManager.PHOTO_VO_MATCH_COMPLETE.remove(updateUsedList);
		}
		
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public override function destroy():void
		{		
			super.destroy();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function construct():void
		{
			//  background
			view = new defaultPanel();
			addChild(view);
			//ref
			title = view.title;
			titleBkg = view.titleBkg;
			bg = view.bg;
			closeButton = view.closeButton;
			boxL = view.boxL;
			boxR = view.boxR;
			
			// positionate
			bg.width = WIDTH;
			bg.height = HEIGHT;
			var margin : Number = 15;
			//title.y = margin;
			
			closeButton.x = bg.width - closeButton.width - margin;
			closeButton.y = 10;
			title.x = margin;
			title.width = closeButton.x - title.x - margin;
			titleBkg.width = WIDTH-2;
			
			boxL.y = boxR.y = titleBkg.y + titleBkg.height + margin;
			boxL.height = boxR.height = bg.height - boxL.y - 50;
			boxL.width = 210;
			
			boxR.width = bg.width - boxL.width - margin*3;
			boxL.x = margin;
			boxR.x = bg.width-boxR.width-margin;
			
			// title & close
			title.text = ResourcesManager.getString("photomanager.panel.title");
			ButtonUtils.makeButton(closeButton, function(e:Event):void{close()});
			
			
			
			// activate all folder button
			activateAllBtn = new HBox(this, boxL.x + 9, boxL.y + 10);
			activateAllBtn.alignment = HBox.MIDDLE;
			activateAllIcon = new photoManager.folder.activateall.icon();
			TweenMax.to(activateAllIcon,0,{tint:Colors.GREY_DARK});
			activateAllBtn.addChild(activateAllIcon);
			activateAllLabel = new LabelTictac(activateAllBtn,0,0,ResourcesManager.getString("photomanager.panel.btn.activateall"),11,Colors.GREY_DARK);
			activateAllLabel.textFormatUpdate(TextFormats.ASAP_BOLD(11,Colors.GREY_DARK));
			ButtonUtils.makeButton(activateAllBtn,toggleAllFolderActivation);
			
			//line
			var line:Sprite = new library.photomanager.folder.line();
			line.x = activateAllBtn.x;
			line.y = activateAllBtn.y + activateAllBtn.height + 10;
			addChild(line);
			
			
			allFolderAreActivated = ( Infos.prefs.foldersUsedList && Infos.prefs.foldersUsedList.length == Infos.project.albumList.length );
			toggleAllFolderBtnState();
			
			//
			folderArea = new ScrollPane(this, boxL.x,boxL.y+40);
			folderArea.shadow = false;
			folderArea.backgroundAlpha = 0;
			folderArea.autoHideScrollBar = true;
			folderArea.showScrollButtons(false);
			folderArea.mouseWheelEnabled = true;
			folderArea.setSize(boxL.width + 2 + 10,boxL.height-50);
			
			newFolderButton = new SimpleThinButton(	"photomanager.panel.btn.newfolder", // label
				Colors.BLUE, //over color
				createNewFolder, //handler
				Colors.GREY, //off color
				Colors.WHITE, //off color label
				Colors.WHITE); // over color label
			newFolderButton.x = boxL.x;
			newFolderButton.y = HEIGHT-40;
			addChild(newFolderButton);
			
			renameFolderButton = new SimpleThinButton(	"photomanager.panel.btn.renamefolder", // label
				Colors.BLUE, //over color
				renameFolder, //handler
				Colors.GREY, //off color
				Colors.WHITE, //off color label
				Colors.WHITE); // over color label
			renameFolderButton.x = newFolderButton.x + newFolderButton.width + 10;
			renameFolderButton.y = newFolderButton.y;
			addChild(renameFolderButton);
			
			
			deleteFolderButton = new SimpleThinButton(	"photomanager.panel.btn.deletefolder", // label
				Colors.RED, //over color
				deleteCurrentFolder, //handler
				Colors.GREY, //off color
				Colors.WHITE, //off color label
				Colors.WHITE); // over color label
			deleteFolderButton.x = renameFolderButton.x + renameFolderButton.width + 10;
			deleteFolderButton.y = newFolderButton.y;
			addChild(deleteFolderButton);
			
			
			// CLOSE BUTTON
			cancelButton = new SimpleThinButton( "common.close", // label
				Colors.GREY, //over color
				close, //handler
				Colors.GREEN, //off color
				Colors.WHITE, //off color label
				Colors.WHITE); // over color label
			cancelButton.x = boxR.x + boxR.width - cancelButton.width;
			cancelButton.y = newFolderButton.y;
			addChild(cancelButton);
			
			
			// UPLOAD BUTTON
			uploadButton = new SimpleThinButton( "photomanager.panel.btn.upload.newphotos", // label
				Colors.BLUE, //over color
				openPhotoUploader, //handler
				Colors.YELLOW, //off color
				Colors.BLACK, //off color label
				Colors.WHITE); // over color label
			uploadButton.x = cancelButton.x - uploadButton.width - 15;
			uploadButton.y = cancelButton.y;
			addChild(uploadButton);
			
			
			imageArea = new ScrollPane(this, boxR.x, boxR.y+10);
			imageArea.addEventListener(Event.CHANGE, updateThumbsVisibility);
			imageArea.SCROLLING.add(function():void{updateThumbsVisibility()});
			imageArea.shadow = false;
			imageArea.autoHideScrollBar = true;
			imageArea.mouseWheelEnabled = true;
			imageArea.backgroundAlpha = 0;
			imageArea.showScrollButtons(false);
			
			//insideUploadButton = new PushButton(this, 430, 245, "Upload photos", openPhotoUploader);
			insideUploadButton = new SimpleThinButton(	"photomanager.panel.btn.upload.newphotos", // label
				Colors.BLUE, //over color
				openPhotoUploader, //handler
				Colors.YELLOW, //off color
				Colors.BLACK, //off color label
				Colors.WHITE); // over color label
			insideUploadButton.x = boxR.x + (boxR.width - insideUploadButton.width)*.5;
			insideUploadButton.y = boxR.y + (boxR.height - insideUploadButton.height)*.5;
			addChild(insideUploadButton);
			
			insideUploadButton.visible = false;
			
			//
			/*
			renderFolderList();
			selectFolder(0);
			*/
		}
		
		/**
		 * Toggle all folder activation 
		 * */
		private function toggleAllFolderActivation(e:MouseEvent):void
		{
			allFolderAreActivated = !allFolderAreActivated;
			toggleAllFolderBtnState();
			for (var i:int = 0; i < folderItemList.length; i++) 
			{
				var folderItem:FolderListItem = folderItemList[i];
				folderItem.activated = allFolderAreActivated;
			}
			updateFolderActivationList();
	
		}		
		
		/**
		 * Toggle all folder btn state 
		 * */
		private function toggleAllFolderBtnState():void
		{
			activateAllLabel.text = (allFolderAreActivated)?ResourcesManager.getString("photomanager.panel.btn.desactivateall"):ResourcesManager.getString("photomanager.panel.btn.activateall");
			activateAllIcon.alpha = (allFolderAreActivated)?1:.2;
		}
		
		
		/**
		 * update image list after image upload (when upload view is closed)
		 */
		private function updateAlbumAfterUploads():void
		{
			SessionManager.instance.updateImageList();
			needFolderUpdate = false;
		}
		private function updateComplete():void
		{
			clearFolders();
			renderFolderList();
			selectFolder(currentFolderIndex);
		}
		
		
		
		/**
		 * render the left folder list
		 */
		private function renderFolderList():void
		{
			var folderItem : FolderListItem;
			folderItemList = new Vector.<FolderListItem>;
			var nextPos : Number = 10;
			var oldFolderPriority:int = 0;
			var currentFolderPriority:int = 0;
			for (var i:int = 0; i < albumList.length; i++) 
			{
				currentFolderPriority = FolderManager.instance.getFolderOrderPriority(albumList[i]);
				folderItem = new FolderListItem(albumList[i].name, albumList[i].photoList.length, i,currentFolderPriority);
				
				if(currentFolderPriority != oldFolderPriority){
					if(i != 0){
						addFolderSeparator( currentFolderPriority, nextPos+20 );
						nextPos +=40;	
					} else {
						addFolderSeparator( currentFolderPriority, nextPos );
						nextPos +=20;
					}
				}
				oldFolderPriority = currentFolderPriority;
				
				folderItem.y = nextPos;
				folderItem.x = 10;
				folderItem.CLICKED.add(selectFolder);
				folderItem.ACTIVATE.add(updateFolderActivationList);
				
				folderArea.addChild(folderItem);
				folderItemList.push(folderItem);
				folderItem.activated = FolderManager.instance.checkIfFolderIsUsed(folderItem.folderName);
				
				// prepare next pos
				nextPos += 35;
			}
			
			if(albumList.length ==0){
				renameFolderButton.enabled = false;
				deleteFolderButton.enabled = false;	
				insideUploadButton.visible = true;
				activateAllBtn.enabled = false;
			}
			
			// update folder area scroll // TODO :check if this works
			folderArea.update();
		}
		
		/**
		 *
		 */
		private function addFolderSeparator( priority : int, posY:Number):void
		{
			var separation:MovieClip = new library.photomanager.folder.separation.text();
			TextField(separation.title).autoSize = "left";
			TextField(separation.title).background = true;
			TextField(separation.title).backgroundColor = Colors.GREY_LIGHT;
			var labelKey:String = (priority == FolderManager.TYPE_FOLDER_OTHER_PROJECT)?"photomanager.folder.separation.otherProject.label":"photomanager.folder.separation.customProject.label";
			TextField(separation.title).text = ResourcesManager.getString(labelKey);
			separation.y = posY;
			separation.x = 10;
			folderArea.addChild(separation);
			thingsToClean.push(separation);
		}
		
		
		/**
		 * check if a folder is activated (from project preference cookie)
		 */
		private function updateFolderActivationList():void
		{
			// save folder open list
			var folderActivatedList : Array = new Array();
			for (var i:int = 0; i < folderItemList.length; i++) 
			{
				var item : FolderListItem = folderItemList[i];
				if(item.activated) folderActivatedList.push(item.folderName);
			}
			Infos.prefs.foldersUsedList = folderActivatedList;
			
			needFolderUpdate = true;
		}
		
		/**
		 * update used photos
		 */
		private function updateUsedList():void
		{
			var photoItem : ManagerThumbItem;
			if(thumbList)
			{
				for (var i:int = 0; i < thumbList.length; i++) 
				{
					photoItem = thumbList[i] as ManagerThumbItem;
					photoItem.updateUsedIcon(); 	// TODO : here photovo doesn't seem to be right.. I must investigate..
				}
			}
		}
		
		
		/**
		 * select a specific folder and display image of it
		 */
		private function updateFolderSelection(folderIndex:int):void
		{
			var f:FolderListItem;
			for (var i:int = 0; i < folderItemList.length ; i++) 
			{
				f = folderItemList[i];
				if(i == folderIndex) f.selected = true;
				else f.selected = false;
			}
		}
		
		/**
		 * create a new folder
		 */
		private function createNewFolder(e:Event = null):void
		{
			Debug.log("PhotoManager > createNewFolder");
			var folderName:String = "";
			var date : Date = new Date();
			var newFolderName:String = ResourcesManager.getString("popup.photomanager.new.folder.newfolder") + date.fullYear + date.month + date.day + date.hours + date.minutes;
			
			var newFolderPopup : PopupSimpleInput = new PopupSimpleInput(newFolderName,"popup.photomanager.new.folder.title");
			newFolderPopup.open();
			newFolderPopup.OK.add(folderNameSelected);
		}
		private function folderNameSelected(p:PopupAbstract):void
		{
			Debug.log("PhotoManager > folderNameSelected");
			var folderName:String = (p as PopupSimpleInput).fieldValue;
			if(folderName == ""){
				var date : Date = new Date();
				folderName = "" + date.fullYear + date.month + date.day + date.hours + date.minutes;
			}
			Debug.log("PhotoManager > folderNameSelected > new name : "+folderName);
			tempFolderName = folderName;
			ServiceManager.instance.createNewFolder(tempFolderName,onFolderCreationComplete);
		}
		private function onFolderCreationComplete(result:Object):void
		{
			var newAlbumVo:AlbumVo = new AlbumVo();
			newAlbumVo.name = tempFolderName;
			newAlbumVo.photoList = new Vector.<PhotoVo>;

			// add new album to the list
			var indexToSelect:int = pushNewCustomFolderIntoAlbumList(newAlbumVo);
			
			// render folders
			clearFolders();
			renderFolderList();
			
			// activate folder and save folder activation state
			folderItemList[indexToSelect].activated = true;
			updateFolderActivationList();

			// select folder
			if(albumList.length>0) selectFolder(indexToSelect);
		}
		
		/**
		 * Push new folder at right place 
		 * returns the new folder index in the album list
		 */
		private function pushNewCustomFolderIntoAlbumList($albumVo:AlbumVo):int
		{
			for (var i:int = 0; i < albumList.length; i++) 
			{
				var albumVo:AlbumVo = albumList[i];
				
				// add new custom folder where other custom folder starts
				if(FolderManager.instance.getFolderOrderPriority(albumVo) == FolderManager.TYPE_FOLDER_CUSTOM)
				{
					albumList.splice(i,0,$albumVo);
					return i;
				}
			}	
			
			// if no cutom folder yet, add this new folder at the end of the list
			albumList.push($albumVo);
			return albumList.length -1;
			
		}
		
		/**
		 * rename a folder
		 */
		private function renameFolder(e:Event= null):void
		{
			Debug.log("PhotoManager > renameFolder");
			var currentFolderName : String = selectedAlbumVo.name+"(copy)";
			var newFolderPopup : PopupSimpleInput = new PopupSimpleInput(currentFolderName,"popup.photomanager.new.folder.title");
			newFolderPopup.open();
			newFolderPopup.OK.add(folderRenameDone);
		}
		private function folderRenameDone(p:PopupAbstract):void
		{
			var folderName:String = (p as PopupSimpleInput).fieldValue;
			if(folderName == ""){
				var date : Date = new Date();
				folderName = "" + date.fullYear + date.month + date.day + date.hours + date.minutes;
			}
			tempFolderName = folderName;
			ServiceManager.instance.renameFolder(selectedAlbumVo.name, tempFolderName, onFolderRenameComplete);
		}
		private function onFolderRenameComplete(result:Object):void
		{
			selectedAlbumVo.name = tempFolderName;
			
			var f:FolderListItem;
			for (var i:int = 0; i < folderItemList.length ; i++) 
			{
				f = folderItemList[i];
				if(f.selected) f.rename(tempFolderName);
			}
			needFolderUpdate = true;
		}
		
		
		
		/**
		 * delete the currently selected folder
		 */
		private function deleteCurrentFolder(e:Event= null):void
		{
			Debug.log("PhotoManager > deleteFolder");
			Debug.log(" >> folder name is "+ selectedAlbumVo.name);
			PopupAbstract.Alert(ResourcesManager.getString("popup.deleteFolder.title"),ResourcesManager.getString("popup.deleteFolder.desc1") + selectedAlbumVo.name + ResourcesManager.getString("popup.deleteFolder.desc2"), true, doDeleteFolder);
		}
		private function doDeleteFolder(p:PopupAbstract):void
		{
			ServiceManager.instance.deleteFolder(selectedAlbumVo.name,onDeleteComplete);
		}
		private function onDeleteComplete(result:Object):void
		{
			// clear current views folders
			clearFolders();
			clearThumbs();
			
			// update album structure
			for (var i:int = 0, l:int =albumList.length; i < l; i++) 
			{
				if(albumList[i] == selectedAlbumVo){
					albumList.splice(i, 1);
					i=l;
				}
				// TODO : check here if we do not need to destroy vo objects also (bitmap especially) while deleting an album
			}
			
			// render new folder list
			renderFolderList();
			selectFolder(0);
			needFolderUpdate = true;
		}
		
		
		/**
		 * delete the currently selected folder
		 */
		private function deleteImage(item:ManagerThumbItem):void
		{
			Debug.log("PhotoManager > deleteImage");
			Debug.log(" >> image id is "+ item.thumbVo.id);
			imageToDelete = item;
			//PopupAbstract.Alert("Warning", "This will delete the image '"+item.thumbVo.id+"' completely. Are you sure you want to continue ?", true, doDeleteImage);
			PopupAbstract.Alert(ResourcesManager.getString("popup.deleteImage.title"),ResourcesManager.getString("popup.deleteImage.desc1") + item.thumbVo.id + ResourcesManager.getString("popup.deleteImage.desc2"), true, doDeleteImage);
		}
		private function doDeleteImage(p:PopupAbstract):void
		{
			ServiceManager.instance.deleteImage(imageToDelete.thumbVo.id,onDeleteImageComplete);
		}
		private function onDeleteImageComplete(result:Object):void
		{
			// TODO here check if we reload all the images or we just remove the current deleted image
			
			// update album structure
			var photoList:Vector.<PhotoVo> = selectedAlbumVo.photoList;
			for (var i:int = 0, l:int = photoList.length; i < l; i++) 
			{
				if( photoList[i].id == imageToDelete.thumbVo.id ){
					photoList.splice(i, 1);
					i=l;
				}
			}
			
			// clear current views folders
			clearThumbs();
			
			// render new folder list
			selectFolder(currentFolderIndex);
			needFolderUpdate = true;
			imageToDelete = null;
		}
		
		
		/**
		 * edit current photo
		 */
		private function editImage(item:ManagerThumbItem):void
		{
			Debug.log("PhotoManager > editImage");
			Debug.log(" >> image id is "+ item.thumbVo.id);
			//frame.edit();
			//ImageEditorModule.EDITION_COMPLETE.add(onImageEditSuccess)
			//ImageEditorModule.EDITION_CANCEL.add(onImageEditCancel);
			ImageEditorModule.instance.openAndEditPhoto(item.thumbVo as PhotoVo);
		}
		
		
		
		/**
		 * Clear and destroy current folders
		 */
		private function clearFolders():void
		{
			Debug.log("PhotoManager.clearFolders");

			if(!folderItemList) return;
			
			for (var i:int = 0; i < thingsToClean.length; i++) 
			{
				if(thingsToClean[i].parent)
					thingsToClean[i].parent.removeChild(thingsToClean[i]);
				
				thingsToClean[i] = null;
			}
			thingsToClean = [];
			
			var folderItem : FolderListItem;
			for (i = 0; i < folderItemList.length; i++) 
			{
				folderItem = folderItemList[i];
				folderItem.destroy();
			}
			folderItemList = null;
			
			
		}
		
		/**
		 * Clear and destroy current thumbList
		 */
		private function clearThumbs():void
		{
			Debug.log("PhotoManager > Clear clearThumbs");
			if(!thumbList) return;
			
			var thumb:ManagerThumbItem;
			for (var i:int = 0; i < thumbList.length; i++) 
			{
				thumb = thumbList[i];
				thumb.destroy();
			}
			thumbList = null;
		}
		
		
		/**
		 * select a specific folder and display image of it
		 */
		private function selectFolder(folderIndex:int):void
		{
			Debug.log("PhotoManagerModule.selectFolder : "+folderIndex);
			if(folderItemList.length < 1) return;
			if(!albumList) return;
			if(folderIndex > albumList.length-1)
			{
				Debug.warn( "Folder index is bigger than album list : "+folderIndex+ " / " + (albumList.length-1) + " > reset folder index to 0");
				folderIndex= 0;
			}
			
			var thumb : ManagerThumbItem;
			var photoVo : PhotoVo;
			
			// destroy existing list
			if(thumbList) clearThumbs();
			
			selectedAlbumVo = ProjectManager.instance.selectedAlbumVo = albumList[folderIndex];
			updateFolderSelection(folderIndex);
			currentFolderIndex = folderIndex;
			
			// create new list
			thumbList = new Vector.<ManagerThumbItem>;
			var numCols : int = 5;
			for (var i:int = 0, l:int = selectedAlbumVo.photoList.length; i < l; i++) 
			{
				thumb = new ManagerThumbItem(selectedAlbumVo.photoList[i]);
				photoVo = selectedAlbumVo.photoList[i];
				if(!photoVo) Debug.warn("PhotoManagerModule.selectFolder : photoVo is null");
				thumb.draggable = false;
				thumb.x = 15 + i%numCols * 105;
				thumb.y = 15 + Math.floor(i/numCols) * 100;
				imageArea.addChild(thumb);
				//thumb.intro();
				if(photoVo.id && !photoVo.temp) thumb.tooltip = TooltipVo.TOOLTIP_IMAGE_PREVIEW(photoVo);
				thumb.DELETE.add(deleteImage);
				thumb.EDIT.add(editImage);
				thumb.DOUBLE_CLICK.add(editImage);
				thumbList.push(thumb);
			}
			
			imageArea.setSize(boxR.width,boxR.height-20);
			
			if( FolderManager.instance.canFolderBeEdited(selectedAlbumVo.name) )
			{
				renameFolderButton.enabled = true;
				deleteFolderButton.enabled = true;
			}
			else 
			{
				renameFolderButton.enabled = false;
				deleteFolderButton.enabled = false;
			}
			
			// activate button only for current project and custom project (not for other projects folder)
			uploadButton.enabled = (FolderManager.instance.getFolderOrderPriority(selectedAlbumVo) != FolderManager.TYPE_FOLDER_OTHER_PROJECT)? true : false;
				
			insideUploadButton.visible = (thumbList.length>0)?false:true;
			
			// update thumb visibility
			updateThumbsVisibility();
			
			
			// check if we need to update temp photos
			removeEventListener(Event.ENTER_FRAME, tempThumbUpdate);
			if( PhotoUploadManager.instance.numUploadLeft > 0 ){
				addEventListener(Event.ENTER_FRAME, tempThumbUpdate);
			}
			else{
				removeEventListener(Event.ENTER_FRAME, tempThumbUpdate);
				tempThumbUpdate(); // do it one last time
			}
		}
		
		
		/**
		 *
		 */
		private function updateThumbsVisibility(e:Event = null):void
		{
			if( !imageArea || !imageArea.content) return; // security
			
			var thumb : ManagerThumbItem;
			var posY : Number = imageArea.content.y;
			if(thumbList)
			{
				for (var i:int = 0; i < thumbList.length; i++) 
				{
					thumb = thumbList[i] as ManagerThumbItem;
					if(posY + thumb.y + thumb.height < 0 || posY + thumb.y > 500) { //TODO: change 800
						thumb.outro();
					}
					else {
						thumb.intro();
					}
				}	
			}
		}
		
		
		/**
		 * open image uploader view
		 */
		private function openPhotoUploader(e:Event = null):void
		{
			Debug.log("PhotoManager.openPhotoUploader");
			var folderName : String = (Infos.selectedAlbumVo)? Infos.selectedAlbumVo.name : null;
			PhotoUploadManager.instance.browse(null, folderName);
		}
		
		/**
		 * while there are thumbs being uploaded, we need to refresh the left list
		 * For this we use an onEnterFrame function doing the job. Seems better than using enterFrame in each photoItem.
		 * Once all images are uploaded (no more file in upload list) we can stop the enterframe
		 */
		private function tempThumbUpdate( e:Event = null ):void
		{
			// destroy thumb items
			var photoItem : PhotoItem;
			var thumbsToUpdate:int = 0;
			if(thumbList)
			{
				for (var i:int = 0; i < thumbList.length; i++) 
				{
					photoItem = thumbList[i] as PhotoItem;
					if( (photoItem.thumbVo as PhotoVo).tempID )
					{
						// update thumb
						thumbsToUpdate ++;
						photoItem.updateTempPhotoItem();
					}
					else photoItem.removeUploadingIcon();
				}	
			}
		}
		
		
	}
	
}


internal class SingletonEnforcer{}