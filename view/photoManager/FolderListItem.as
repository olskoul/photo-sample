/***************************************************************
PROJECT 		: Tic Tac
COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
YEAR			: 2013
****************************************************************/
package view.photoManager
{
	import be.antho.data.ResourcesManager;
	
	import com.bit101.components.PushButton;
	import com.greensock.TweenMax;
	
	import comp.DefaultTooltip;
	
	import data.TooltipVo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import manager.FolderManager;
	import manager.ProjectManager;
	
	import mcs.photoManager.folderItem;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.Colors;

	public class FolderListItem extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public const CLICKED : Signal = new Signal(uint);
		public const ACTIVATE : Signal = new Signal();
		
		// ui
		public var view : MovieClip = new folderItem();
		public var title : TextField;
		public var infos : MovieClip;
		public var bg:MovieClip;
		public var icon : MovieClip;
		
		public var folderName : String;
		public var folderIndex : uint;
		
		
		public var activateButton : MovieClip;
		
		public var desactivable : Boolean;

		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function FolderListItem(folderName:String, numItemfolderIndex:uint, folderIndex:uint, folderType:int) 
		{
			addChild(view);
			
			switch(folderType)
			{
				case FolderManager.TYPE_FOLDER_CURRENT_PROJECT:
					view.gotoAndStop(1);
					view.icon.gotoAndStop(2);
					break;
				case FolderManager.TYPE_FOLDER_OTHER_PROJECT:
					view.gotoAndStop(2);
					break;
				case FolderManager.TYPE_FOLDER_CUSTOM:
					view.gotoAndStop(3);
					break;
			}
			
			title = view.title;
			bg = view.bg;
			icon = view.icon;
			infos = view.infos;
			activateButton = view.activatedClip;
			
			this.folderName = folderName;
			this.folderIndex = folderIndex;
			
			title.text = FolderManager.instance.getFolderDisplayName( folderName );
			infos.txt.text = numItemfolderIndex;
			
			mouseChildren = true;
			title.mouseEnabled = title.mouseWheelEnabled = false;
			infos.mouseChildren = infos.mouseEnabled = false;
			icon.mouseEnabled = icon.mouseChildren = false;
			bg.addEventListener(MouseEvent.CLICK, function():void{
				CLICKED.dispatch(folderIndex);
			});
			bg.addEventListener(MouseEvent.ROLL_OVER, function():void{
				alpha = .8
			});
			bg.addEventListener(MouseEvent.ROLL_OUT, function():void{
				alpha = 1
			});
			
			
			// activate button construct
			desactivable = FolderManager.instance.canFolderBeDesactivated( folderName )? true : false;
			activateButton.mouseChildren = false;
			activateButton.buttonMode = true;
			
			if(desactivable)
			{
				activateButton.addEventListener(MouseEvent.ROLL_OVER, function():void{
					var frameTo : Number = (activated)? 2 : 1;
					activateButton.gotoAndStop(frameTo);
					DefaultTooltip.tooltipOnClip(activateButton,new TooltipVo("tooltip.photomanager.activate.folder"));
				});
				activateButton.addEventListener(MouseEvent.ROLL_OUT, function():void{
					var frameTo : Number = (activated)? 1 : 2;
					activateButton.gotoAndStop(frameTo);
					DefaultTooltip.killAllTooltips();
				});
				activateButton.addEventListener(MouseEvent.CLICK, eyeClickHandler);
			}
			else
			{
				activateButton.visible =false;
			}
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * SELECT
		 */
		private var _selected:Boolean = false;

		

		public function set selected(value:Boolean):void
		{
			_selected = value;
			//bg.mouseEnabled = (!_selected && activated);
			bg.mouseEnabled = true;
			
			if(selected){
				TweenMax.to(bg, 0, {tint : 0x0068A0});
				TweenMax.to(title, 0, {tint : 0xffffff});
				TweenMax.to(infos, 0, {tint : 0xffffff});	
			}
			else{
				TweenMax.to(bg, 0, {tint : null});
				TweenMax.to(title, 0, {tint : null});
				TweenMax.to(infos, 0, {tint : null});
			}	
		}
		public function get selected():Boolean
		{
			return _selected;
		}
		
		
		
		/**
		 * ACTIVATE
		 */
		private var _activated:Boolean = true;
		public function set activated(value:Boolean):void
		{
			if(!value && !desactivable) return;
			_activated = value;
			if(activated){
				//bg.mouseEnabled = true;
				activateButton.gotoAndStop(1);
				activateButton.alpha = 1;
				bg.alpha = 1;
				title.alpha = 1;
				//icon.visible = true;
			}
			else
			{
				selected = false; // if we desactivate, we deselect !
				//bg.mouseEnabled = false;
				activateButton.gotoAndStop(2);
				//activateButton.alpha = .2;
				title.alpha = .5;
				bg.alpha = .5;
				//icon.visible = false;
			}
		}
		public function get activated():Boolean
		{
			return _activated;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public function rename(newName:String):void
		{
			title.text = newName;
		}
		
		/**
		 *
		 */
		public function updateItems(numItems:int):void
		{
			infos.txt.text = numItems;
		}
		
		/**
		 *
		 */
		public function destroy():void
		{
			CLICKED.removeAll();
			ACTIVATE.removeAll();
			TweenMax.killTweensOf(bg);
			TweenMax.killTweensOf(title);
			TweenMax.killTweensOf(infos);
			this.parent.removeChild(this);
		}
		
		
		/**
		 * activate / desactivate folder
		 */
		public function eyeClickHandler(e:MouseEvent):void
		{
			toggleActivation();
		}
		
		/**
		 * activate / desactivate folder
		 */
		public function toggleActivation():void
		{
			activated = !_activated;
			ACTIVATE.dispatch();
		}
		
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
	}
}