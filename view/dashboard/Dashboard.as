/***************************************************************
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2014
 ****************************************************************/
package view.dashboard
{
	import com.bit101.components.HBox;
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.System;
	
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	import comp.button.SimpleButton;
	import comp.label.LabelTictac;
	
	import data.Infos;
	import data.ProductsCatalogue;
	
	import library.offline.wizard.online.product.textile;
	import library.offline.wizard.online.product.visual;
	
	import manager.ProjectManager;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	
	
	/**
	 * Dashboard manager
	 */
	public class Dashboard
	{
		//Signals
		public static const DASHBOARD_RETURN_TO_HOME:Signal = new Signal();
		public static const CLASSNAME_CHOSEN:Signal = new Signal();
		public static const DASHBOARD_CHOICE_MADE:Signal = new Signal();
		public static const DASHBOARD_CANCELED:Signal = new Signal();
		public static const UPDATE_LAYOUT:Signal = new Signal();
		public static const STATE_CHOICE:String = "stateChoice";
		public static const STATE_CONFIGURATOR:String = "stateConfigurator";
		
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		/**
		 * instance
		 */
		private static var _instance:Dashboard;
		private var stageRef:Stage;
		private var wrapper:Sprite;
		private var bkg:Sprite;
		
		private var albumsBtn:DashboardClassnameBtn;
		private var calendarsBtn:DashboardClassnameBtn;
		private var cardsBtn:DashboardClassnameBtn;
		private var canvasBtn:DashboardClassnameBtn;
		private var btns:Vector.<DashboardClassnameBtn>;
		
		private var cancelBtn:SimpleButton;
		private var openProjectBtn:SimpleButton;
		private var backBtn:SimpleButton;
		
		
		private var title:LabelTictac;
		private var titleOnlineProdcut:LabelTictac;
		private var imgOnlineProduct:Bitmap;
		
		private var btnWrapper:HBox;
		
		private var visualOnlineProduct:HBox;
		
		private var visualTextile:Sprite;
		
		private var _isOpen : Boolean;
		private var visualFun:Sprite;
		private var state:String = STATE_CHOICE;
		private var currentClassName:String;
		private var openingClassName:String;
		
		
		/**
		 * ------------------------------------ SINGLETON -------------------------------------
		 */
		
		public function Dashboard(sf:SingletonEnforcer) { if(!sf) throw new Error("Dashboard is a singleton, use instance"); }
		public static function get instance():Dashboard
		{
			if (!_instance) _instance = new Dashboard(new SingletonEnforcer())
			return _instance;
		}
		
		/**
		 * ------------------------------------ GETTER/SETTER -------------------------------------
		 */
		
		public function get isOpen():Boolean
		{
			return _isOpen;
		}
		
		
		/**
		 * ------------------------------------ PUBLIC INIT/DESTROY/OPEN -------------------------------------
		 */
		
		/**
		 * init manager
		 * 
		 */
		public function init($stageRef:Stage):void
		{
			//stageRef
			stageRef = $stageRef;
		}
		
		/**
		 * destroy manager
		 * 
		 */
		public function destroy():void
		{
			dispose();
			stageRef = null;
			_instance = null;
		}
		
		/**
		 * Start dasboard
		 * */
		public function open():void
		{
			Debug.log("Dashboard.start"); 
			_isOpen = true;
			openingClassName = Infos.session.classname;
			build();
			show();
			addListerners();
		}
		
		/**
		 * FINISH Btn click handler
		 * */
		public function close():void
		{
			_isOpen = false;
			hide(dispose);
		}
		
				
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Add signals listeners
		 */
		private function addListerners():void
		{
			CLASSNAME_CHOSEN.add(classnameChosen);	
			UPDATE_LAYOUT.add(centerConfigurator);
		}
		
		/**
		 * Classname chosen
		 * set currentclassname
		 * updatelayout to show configurator
		 */
		private function classnameChosen(classname:String):void
		{
			Debug.log("Dashboard.classnameChosen: "+classname);
			currentClassName = classname;
			setState(STATE_CONFIGURATOR);
		}
		
		/**
		 * Center current configurator after setting new state
		 */
		private function centerConfigurator(item:DashboardClassnameBtn):void
		{
			Debug.log("Dashboard.centerConfigurator");
			updateLayout();
		}
		
		/**
		 * setState
		 * change state value and updatelayout
		 */
		private function setState(newState:String):void
		{
			state = newState;
			updateLayout();
		}
		
		/**
		 * show current step
		 * */
		private function show():void
		{
			stageRef.addChild(wrapper);
			wrapper.y = stageRef.stageHeight;
						
			var timeline:TimelineMax = new TimelineMax();
			timeline.stop();
			
			//timeline.append( TweenMax.to(wrapper, 1, {alpha:.6}) );
			timeline.append( TweenMax.to(wrapper, .5, {y:0,ease:Strong.easeOut}) );
			timeline.play();
		}
		
		/**
		 * hide current step
		 * */
		private function hide($onComplete:Function = null):void
		{
			// security
			if(!wrapper) 
			{
				if($onComplete != null) $onComplete();
				return;
			}
			
			var timeline:TimelineMax = new TimelineMax({onComplete:$onComplete});
			timeline.stop();
			
			timeline.append( TweenMax.to(wrapper, .5, {autoAlpha:0 }) );			
			timeline.play();	
		}
		
		/**
		 * Build current step
		 * */
		private function build():void
		{
			//resize
			stageRef.addEventListener(Event.RESIZE, resizeEventHandler);
			
			//kill eventual project tooltip (avoid blinking effect)
			DefaultTooltip.killTooltipById("infoproject"); 
			
			//Create wrapper
			wrapper = new Sprite();	
			
			//Create darkness (Sounds good huh!? :) )
			bkg = new Sprite();			
			
			//darkness.alpha = 0;
			wrapper.addChild(bkg);
			
			//Classname Btns
			albumsBtn = new DashboardClassnameBtn(ProductsCatalogue.CLASS_ALBUM);
			calendarsBtn = new DashboardClassnameBtn(ProductsCatalogue.CLASS_CALENDARS);
			cardsBtn = new DashboardClassnameBtn(ProductsCatalogue.CLASS_CARDS);
			canvasBtn = new DashboardClassnameBtn(ProductsCatalogue.CLASS_CANVAS);
			
			btns = new Vector.<DashboardClassnameBtn>();
			addClassnameBtn(albumsBtn);
			addClassnameBtn(calendarsBtn);
			addClassnameBtn(cardsBtn);
			addClassnameBtn(canvasBtn);
			
			//CancelBtn
			cancelBtn = SimpleButton.createBlueButton("common.cancel", cancelBtnClickedHandler);
			wrapper.addChild(cancelBtn);
			
			
			//BackBtn
			backBtn = SimpleButton.createBlueButton("common.back", backBtnClickedHandler);
			wrapper.addChild(backBtn);
			
			// open project btn
			openProjectBtn = SimpleButton.createBlueButton("topbar.project.open.btn.label", openBtnClickedHandler);
			wrapper.addChild(openProjectBtn);
			
			// VISIBILITY
			//cancelBtn.visible = isUserAction;
			cancelBtn.visible = (Infos.project != null); // if we have a project loaded, allow cancel
			//openProjectBtn.visible = !cancelBtn.visible; // otherwise show open button
			backBtn.visible = (state == STATE_CONFIGURATOR);
			
			//title
			var textColor : Number = (Infos.config.isRebranding)? Colors.WHITE : Colors.BLUE;
			title = new LabelTictac(wrapper,0,0,ResourcesManager.getString('wizard.choose.classname'),36,textColor,"center");
			/*
			var shadow:DropShadowFilter = new DropShadowFilter(1,90,Colors.WHITE,.6,1,1,1,1);
			title.filters = [shadow];
			*/
			
			//title online product
			titleOnlineProdcut = new LabelTictac(wrapper,0,0,ResourcesManager.getString('wizard.discover.online.prodcuts'),16,textColor,"center");
			
			//Visual online product
			visualOnlineProduct = new HBox();
			visualOnlineProduct.spacing = 12;
			visualTextile = new Sprite();
			visualTextile.addChild(new Bitmap(new library.offline.wizard.online.product.textile()));
			visualFun = new Sprite();
			visualFun.addChild(new Bitmap(new library.offline.wizard.online.product.fun()));
			visualOnlineProduct.addChild(visualTextile);
			visualOnlineProduct.addChild(visualFun);
			visualOnlineProduct.buttonMode = true;
			visualTextile.addEventListener(MouseEvent.CLICK, onlineProductClicked);
			visualFun.addEventListener(MouseEvent.CLICK, onlineProductClicked);
			wrapper.addChild(visualOnlineProduct);
			
			
			//update layout
			updateLayout(false);

		}
		
		protected function resizeEventHandler(event:Event):void
		{
			updateLayout();
		}
		
		/**
		 * Visit online product
		 * Naigate to url
		 * */
		protected function onlineProductClicked(event:MouseEvent):void
		{
			var target:Sprite = event.currentTarget as Sprite;
			var url:String = (target == visualTextile)?"VisitOnlineProductsTextiles":"VisitOnlineProductsFun";
			navigateToURL(new URLRequest(Infos.config.getSettingsValue(url)),"_blank");
		}		
		
		/**
		 * destroy function
		 * */
		private function dispose():void
		{
			stageRef.removeEventListener(Event.RESIZE, resizeEventHandler);
			CLASSNAME_CHOSEN.removeAll();
			state = STATE_CHOICE;
			btns = null;
			
			if(bkg)
			{
				if(bkg.parent)
				bkg.parent.removeChild(bkg);
				bkg = null;
			}
			
			if(title)
			{
				if(title.parent)
					title.parent.removeChild(title);
				title = null;
			}
			
			if(albumsBtn)
			{
				if(albumsBtn.parent)
					albumsBtn.parent.removeChild(albumsBtn);
				albumsBtn.destroy();
				albumsBtn = null;
			}
			if(calendarsBtn)
			{
				if(calendarsBtn.parent)
					calendarsBtn.parent.removeChild(calendarsBtn);
				calendarsBtn.destroy();
				calendarsBtn = null;
			}
			if(cardsBtn)
			{
				if(cardsBtn.parent)
					cardsBtn.parent.removeChild(cardsBtn);
				cardsBtn.destroy();
				cardsBtn = null;
			}
			if(canvasBtn)
			{
				if(canvasBtn.parent)
					canvasBtn.parent.removeChild(canvasBtn);
				canvasBtn.destroy();
				canvasBtn = null;
			}
			if(cancelBtn)
			{
				if(cancelBtn.parent)
					cancelBtn.parent.removeChild(cancelBtn);
				cancelBtn = null;
			}
			if(openProjectBtn)
			{
				if(openProjectBtn.parent)
					openProjectBtn.parent.removeChild(openProjectBtn);
				openProjectBtn = null;
			}
			if(backBtn)
			{
				if(backBtn.parent)
					backBtn.parent.removeChild(backBtn);
				backBtn = null;
			}
			
						
			if(visualTextile)
			{
				visualTextile.removeEventListener(MouseEvent.CLICK, onlineProductClicked);
				visualFun.removeEventListener(MouseEvent.CLICK, onlineProductClicked);
				
				if(visualTextile.parent)
					visualTextile.parent.removeChild(visualTextile);
				visualTextile = null;
				if(visualFun.parent)
					visualFun.parent.removeChild(visualFun);
				visualFun = null;
			}
			if(visualOnlineProduct)
			{
				if(visualOnlineProduct.parent)
					visualOnlineProduct.parent.removeChild(visualOnlineProduct);
				visualOnlineProduct = null;
			}
			if(wrapper)
			{
				if(wrapper.parent)
					wrapper.parent.removeChild(wrapper);
				wrapper = null;
			}
			
		}
		
		/**
		 * Update layout based on state value
		 */
		protected function updateLayout(transition:Boolean = true):void
		{
			//background
			var color:Number = (!Infos.config.isRebranding ) ? Colors.BLUE_LIGHT : Infos.config.rebrandingMainColor;
			bkg.graphics.clear();
			bkg.graphics.beginFill(color,1);
			bkg.graphics.drawRect(0,0,stageRef.stageWidth, stageRef.stageHeight);
			bkg.graphics.endFill();	
			
			backBtn.visible = (state == STATE_CONFIGURATOR);
			switch(state)
			{
				case STATE_CHOICE:
					showChoices(transition);
					break;
				case STATE_CONFIGURATOR:
					showConfigurator();
					break;
			}
			
			try
			{
				System.gc();
			} 
			catch(error:Error) 
			{
				Debug.warn("Dashboard call garbage collector failed");
			}
			
		}
		
		
		/**
		 * Show state Choice
		 * display all 4 btns and online options
		 * update title
		 */
		private function showChoices(transition:Boolean = true):void
		{
			Debug.log("Dashboard.showChoices");
			var timming:Number = (transition)?0.5:0;
			var gap:int = 15;
			var totalBtns:int = btns.length;
			var posY:Number = (stageRef.stageHeight - btns[0].initHeight)*.5;
			var leftPosX:Number = (stageRef.stageWidth - ((totalBtns * btns[0].initWidth) + ((totalBtns-1) * gap))) * .5;
			var btn:DashboardClassnameBtn;
			var posX:Number;
			for (var i:int = 0; i < totalBtns; i++) 
			{
				btn = btns[i];
				btn.setState(DashboardClassnameBtn.STATE_BTN);
				posX = leftPosX + (btn.width + gap) * i;
				TweenMax.killTweensOf(btn);
				TweenMax.to(btn,timming,{autoAlpha:1,y:posY,x:posX,ease:Strong.easeOut});				
			}
			
			//cancelBtn
			//cancelBtn.visible = true;
			cancelBtn.x = stageRef.stageWidth - cancelBtn.width - 20;
			openProjectBtn.x = (cancelBtn && cancelBtn.visible)?cancelBtn.x - openProjectBtn.width - 10:stageRef.stageWidth - openProjectBtn.width - 20;
			cancelBtn.y = openProjectBtn.y = 20;
			
			//back btn
			backBtn.x = 20;
			backBtn.y = 20;
			
			//TITLE
			title.updateText(ResourcesManager.getString('wizard.choose.classname'));
			TweenMax.killTweensOf(title);
			TweenMax.to(title,timming,{autoAlpha:1,
				x:stageRef.stageWidth - title.width >> 1,
				y:posY - 80
			});
			
			//TITLE ON LINE PRODUCT
			var titleOnlinePosY:Number = posY + btns[0].height + 60;
			TweenMax.killTweensOf(titleOnlineProdcut);
			TweenMax.to(titleOnlineProdcut,timming,{autoAlpha:1,
				x:stageRef.stageWidth - titleOnlineProdcut.width >> 1,
				y:titleOnlinePosY
			});
			
			//BTN ONLINE PRDUCT
			TweenMax.killTweensOf(visualOnlineProduct);
			TweenMax.to(visualOnlineProduct,timming,{autoAlpha:1,
												x:stageRef.stageWidth - visualOnlineProduct.width >> 1,
												y:titleOnlinePosY + titleOnlineProdcut.height + 15
			});
						
		}
		
		/**
		 * Show state Configurator
		 * Hide 3 of all 4 btns and online options
		 * update title
		 */
		private function showConfigurator():void
		{
			Debug.log("Dashboard.showConfigurator");
			var totalBtns:int = btns.length;
			var btn:DashboardClassnameBtn;
			var posX:Number;
			var posY:Number;
			for (var i:int = 0; i < totalBtns; i++) 
			{
				btn = btns[i];
				TweenMax.killTweensOf(btn);
				if(btn.classname != currentClassName)
				{
					
					TweenMax.to(btn,.5,{autoAlpha:0,ease:Strong.easeOut});		
				}
				else
				{
					posY = (stageRef.stageHeight - btn.height)*.5;
					posX =  (stageRef.stageWidth - btn.width) *.5;
					TweenMax.to(btn,.5,{autoAlpha:1,
						y:posY,
						x:posX,
						ease:Strong.easeOut, 
						onComplete:btn.setState,
						onCompleteParams:[DashboardClassnameBtn.STATE_CONFIGURATOR]});	
				}
				
						
			}
			
			//cancelBtn
			//cancelBtn.visible = false;
			cancelBtn.x = stageRef.stageWidth - cancelBtn.width - 20;
			cancelBtn.y = 20;
			
			//backBtn
			backBtn.x = 20;
			backBtn.y = 20;
			
			//TITLE
			title.updateText(ResourcesManager.getString('wizard.choose.classname.options'));
			TweenMax.killTweensOf(title);
			TweenMax.to(title,.5,{autoAlpha:1,
				x:stageRef.stageWidth - title.width >> 1,
				y:posY - 80
			});
			
			//TITLE ON LINE PRODUCT
			TweenMax.killTweensOf(titleOnlineProdcut);
			TweenMax.to(titleOnlineProdcut,.5,{autoAlpha:0
			});
			
			//BTN ONLINE PRDUCT
			TweenMax.killTweensOf(visualOnlineProduct);
			TweenMax.to(visualOnlineProduct,.5,{autoAlpha:0
			});
			
		}
		
		/**
		 * CANCEL BTN CLICK HANDLER
		 * */
		private function cancelBtnClickedHandler():void
		{
			if(Infos.session.classname != openingClassName)
				Dashboard.DASHBOARD_CHOICE_MADE.dispatch(openingClassName);
			DASHBOARD_CANCELED.dispatch();
			Dashboard.instance.close(); // close it
		}
		
		/**
		 * BACK BTN CLICK HANDLER
		 * */
		private function backBtnClickedHandler():void
		{
			setState(STATE_CHOICE);
		}
		
		/**
		 * open project popup
		 * */
		private function openBtnClickedHandler():void
		{
			ProjectManager.PROJECT_OPEN_CONTINUE.remove(close);
			ProjectManager.PROJECT_OPEN_CONTINUE.add(close);
			ProjectManager.instance.openProjectExplorer();
		}

		
		
		/**
		 * Add btn to wrapper and to array
		 */
		private function addClassnameBtn(btn:DashboardClassnameBtn):void
		{
			wrapper.addChild(btn);
			btns.push(btn);
		}	
		
		
		
		

	}
}

class SingletonEnforcer{}