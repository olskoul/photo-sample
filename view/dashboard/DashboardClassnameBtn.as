/***************************************************************
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2014
 ****************************************************************/

package view.dashboard
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.MathUtils2;
	
	import comp.accordion.AccordionItemContent;
	import comp.button.SimpleButton;
	
	import data.ProductsCatalogue;
	
	import library.offline.wizard.classname.item.view;
	
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.menu.lefttabs.project.album.ProjectAlbumConfigurator;
	import view.menu.lefttabs.project.calendar.ProjectCalendarConfigurator;
	import view.menu.lefttabs.project.canvas.ProjectCanvasConfigurator;
	import view.menu.lefttabs.project.cards.ProjectCardsConfigurator;
	
	public class DashboardClassnameBtn extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		public static const STATE_BTN:String = "button";
		public static const STATE_CONFIGURATOR:String = "configurator";

		private var view:MovieClip;
		private var btn:SimpleButton;
		private var state:String = STATE_BTN;
		private var configurator:Sprite;

		public var initWidth:Number;
		public var initHeight:Number;
		public var classname:String;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////

		public function DashboardClassnameBtn(classname:String)
		{
			super();
			this.classname = classname;
			view = new library.offline.wizard.classname.item.view();
			init(classname);
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		public function setState(newState:String, forceState:Boolean = false):void
		{
			//security
			if(state == newState && forceState == false) return;
			//ref new state
			state = newState;
			//update layout
			updateLayout();
			
		}
		
		public function destroy():void
		{
			doDestroy();
		}

		////////////////////////////////////////////////////////////////
		//	PRIVATES
		////////////////////////////////////////////////////////////////
		
		private function init(classname:String):void
		{
			//title
			view.title.text = ResourcesManager.getString("class."+classname);
			view.title.mouseEnabled = false;	
			TweenMax.to(view.titleBg,0,{tint:Colors.BLUE});
			TweenMax.to(view.title,0,{tint:Colors.WHITE});
			
			//init Dimensions
			initWidth = view.bg.width;
			initHeight = view.bg.height;
			
			//preview image
			view.image.gotoAndStop(classname);
			
			//btn
			btn = SimpleButton.createGreenAndBlueButton("common.start.new.project", btnClickHandler);
			btn.forcedWidth = 200;
			
			addChild(view);
			addChild(btn);
			
			//setState
			setState(STATE_BTN, true);
		}
		
		private function btnClickHandler(e:Event = null):void
		{
			Debug.log("DashboardClassnameBtn.btnClickHandler");
			ProjectManager.PROJECT_NO_PROJECT_SELECTED.addOnce(function():void{ Dashboard.CLASSNAME_CHOSEN.dispatch(classname); });
			Dashboard.DASHBOARD_CHOICE_MADE.dispatch(classname);
		}
		
		
		
		private function updateLayout():void
		{
			//first remove all listener
			reset();
			
			switch(state)
			{
				case STATE_BTN:
					showMeAsBtn();
					break;
				case STATE_CONFIGURATOR:
					showMeAsOptions();
					break;
				
			}
		}
		
		private function showMeAsBtn():void
		{
			TweenMax.killTweensOf(view.image);
			TweenMax.killTweensOf(btn);
			
			//listeners
			view.image.mouseEnabled = false;
			view.buttonMode = true;
			view.bg.addEventListener(MouseEvent.CLICK,btnClickHandler);
			
			//props
			//image
			TweenMax.to(view.image,0.2,{autoAlpha:1});
			//btn
			var btnPosX:Number = view.width - btn.width >> 1;
			TweenMax.to(btn,0.2,{autoAlpha:1,
								x:btnPosX,
								y:view.height - btn.height - btnPosX	
			});
			
			
		}
		
		private function showMeAsOptions():void
		{
			var randomW:Number = MathUtils2.randomRange(500,800);
			var randomH:Number = MathUtils2.randomRange(250,450);
			var margin:int = 15;

			//Add configurator based on className
			configurator = getConfiguratorByClassName();
			/*
			configurator = new Sprite();
			configurator.graphics.clear();
			configurator.graphics.beginFill(Colors.RED,1);
			configurator.graphics.drawRect(0,0,randomW, randomH);
			configurator.graphics.endFill();
			*/
			
			configurator.x = 0;
			configurator.y = view.titleBg.y + view.titleBg.height + margin;
			addChild(configurator);
			
			//Resize bg and title
			TweenMax.killTweensOf(view.bg);
			TweenMax.killTweensOf(view.titleBg);
			TweenMax.killTweensOf(view.title);

			TweenMax.to(view.bg,0,{width:configurator.width + margin*2,
									height:	configurator.height + (margin*2) + view.titleBg.height,
									ease:Strong.easeOut
			});
			TweenMax.to(view.titleBg,0,{width:configurator.width + margin*2 - view.titleBg.x//,
									//ease:Strong.easeOut, onComplete:showConfigurator
			});
			
			view.title.x = view.titleBg.width - view.title.width >> 1;
			
			showConfigurator();
			
		}
		
		private function showConfigurator():void
		{
			Dashboard.UPDATE_LAYOUT.dispatch(this);
			/*TweenMax.to(configurator,.1,{alpha:1,
				ease:Strong.easeOut
			});*/
			TweenMax.to(this,.5,{alpha:1,
				delay:2,
				ease:Strong.easeOut
			});
		}
		
		private function getConfiguratorByClassName():Sprite
		{
			var xml:XML = SessionManager.instance.menusXML[classname];
			var nodeProject:XMLList = xml.menu.item.(@id == "project");
			switch(classname)
			{
				case ProductsCatalogue.CLASS_ALBUM:
					return new ProjectAlbumConfigurator(nodeProject[0],true);
					break;
				case ProductsCatalogue.CLASS_CALENDARS:
					return new ProjectCalendarConfigurator(nodeProject[0],true);
					break;
				case ProductsCatalogue.CLASS_CARDS:
					return new ProjectCardsConfigurator(nodeProject[0],true);
					break;
				case ProductsCatalogue.CLASS_CANVAS:
					return new ProjectCanvasConfigurator(nodeProject[0],true);
					break;
			}
			return null;
		}
		
		private function reset():void
		{
			//first remove all listener
			removeAllListener();
			
			
			if(configurator)
			{
				TweenMax.killTweensOf(configurator);
				TweenMax.to(configurator,0,{autoAlpha:0});
				if(configurator.parent)
					configurator.parent.removeChild(configurator);
				AccordionItemContent(configurator).destroy();
				configurator = null;
			}
			
			if(view)
			{
				TweenMax.killTweensOf(view);
				view.bg.width = initWidth;
				view.bg.height = initHeight;
				view.titleBg.width = initWidth - view.titleBg.x*2;
				view.title.x = view.titleBg.width - view.title.width >> 1;
				//props
				//image
				TweenMax.to(view.image,0,{autoAlpha:0});
			}
			

			if(btn)
			{
				TweenMax.killTweensOf(btn);
				TweenMax.to(btn,0,{autoAlpha:0});
			}
		}
		
		private function doDestroy():void
		{
			//first remove all listener
			removeAllListener();
			
			TweenMax.killTweensOf(view.image);
			TweenMax.killTweensOf(btn);
			
			if(view)
			{
				if(view.parent)
					view.parent.removeChild(view);
				view = null;
			}
			
			if(configurator)
			{
				TweenMax.killTweensOf(configurator);
				TweenMax.to(configurator,0,{autoAlpha:0});
				if(configurator.parent)
					configurator.parent.removeChild(configurator);
				AccordionItemContent(configurator).destroy();
				configurator = null;
			}
			
			if(btn)
			{
				if(btn.parent)
					btn.parent.removeChild(btn);
				btn = null;
			}
		}
		
		
		
		/**
		 * Remove all listener
		 */
		private function removeAllListener():void
		{
			if(view)
			{
				view.buttonMode = false;
				view.bg.removeEventListener(MouseEvent.CLICK,btnClickHandler);
			}
			
		}
		
	}
}