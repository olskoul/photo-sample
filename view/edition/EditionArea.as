﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition
{
	import com.bit101.components.HBox;
	import com.bit101.components.HSlider;
	import com.bit101.components.Label;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	import flash.ui.Mouse;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.GarbageUtil;
	
	import comp.AssetItem;
	import comp.BackgroundCustomItem;
	import comp.BackgroundItem;
	import comp.ClipartItem;
	import comp.DraggableItem;
	import comp.DropTargetArea;
	import comp.LayoutItem;
	import comp.OverlayerItem;
	import comp.PhotoItem;
	import comp.PhotoItemOffline;
	import comp.button.SimpleButton;
	import comp.button.SimpleThinButton;
	import comp.button.ToolbarButton;
	import comp.label.LabelTictac;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	import data.ClipartVo;
	import data.FrameVo;
	import data.Infos;
	import data.OverlayerVo;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.PhotoVo;
	import data.ProductsCatalogue;
	import data.ProjectVo;
	import data.SessionVo;
	import data.TooltipVo;
	import data.TutorialStepVo;
	
	import library.button.toobarButton;
	import library.close.btn;
	import library.edition.zoomSliderBox;
	import library.edition.pagearea.highlight.fill.bmp;
	
	import manager.CalendarManager;
	import manager.CanvasManager;
	import manager.CardsManager;
	import manager.DragDropManager;
	import manager.LayoutManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.PhotoSwapManager;
	import manager.ProjectManager;
	import manager.TutorialManager;
	import manager.UserActionManager;
	
	import offline.data.OfflineProjectManager;
	import offline.manager.PriceManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.editionArea.FlyleafColorBox;
	import view.edition.imageEditor.EditionZoomOverview;
	import view.edition.pageNavigator.PageNavigator;
	import view.edition.pageNavigator.PageNavigatorGroupCTA;
	import view.edition.pageNavigator.PageNavigatorItem;
	import view.edition.toolbar.EditionToolBar;
	import view.menu.lefttabs.LeftTabs;
	import view.popup.PopupAbstract;
	import view.popup.PopupPageSwapper;
	
	CONFIG::offline
		{
			import offline.manager.OfflineAssetsManager;
		}


	/**
	 * The edition area class correspond to the right part of the screen
	 * This is where all the editing is done
	 * It contains album, calendar, etc layouts + frames and editing tools
	 * Can be of course changed and improved
	 */
	public class EditionArea extends Sprite
	{
		// statics
		public static const STATE_NORMAL : String = "STATE_NORMAL";
		public static const STATE_PREVIEW : String = "STATE_PREVIEW";
		public static var editorScale:Number;
		
		// signals
		public static const PAGE_DRAW_COMPLETE:Signal = new Signal();
		public static const EDITOR_RESIZE:Signal = new Signal();
		//public static const PAGE_SELECTED:Signal = new Signal(PageVo);
		
		// instance
		private static var _instance:EditionArea;
		
		// view
		private var transformTool:TransformTool ;
		private var pageNavigator:PageNavigator;
		private var pageContainer : Sprite;
		private var pageContainerWrapper : Sprite;
		private var buttonBar:HBox;
		private var pageToolbar:HBox;
		private var lastPageButton : SimpleButton;
		private var nextPageButton : SimpleButton;
		private var previousPageButton : SimpleButton;
		private var firstPageButton : SimpleButton;
		private var pageIndexField : TextField;
		private var zoomBox : MovieClip;
		private var zoomSlider : HSlider;
		private var editorMask : Shape ;
		private var zoomOverview : EditionZoomOverview;
		private var fullScreenModal : Sprite; // fullscreen background
		private var fullScreenContainer : Sprite;// fullscreen container to center
		private var editionToolbar : EditionToolBar;
		private var hasFlyLeafPreview:Boolean; // fullscreen fake page added to page container for the page 1
		private var closeFullScreenBtn:Sprite;
		private var pageGradient:Shape = new Shape();
		private var expandPageNavigatorBtn:ToolbarButton;
		private var previewBtn:ToolbarButton;
		// canvas only could be for all project but later
		private var canvasDetailBox : Sprite;
		private var canvasDetailLabel : Label;
		private var addMorePage:SimpleThinButton;
		private var flyleafTitle:LabelTictac;
		private var flyleafSelectedLabel:LabelTictac;
		
		
		// datas
		private var ready:Boolean = false; // value indicating when view is ready for modifications (all children are created, etc.. )
		private var currentState:String = STATE_NORMAL;
		private var currentPagesVect:Vector.<PageArea>; // contains the pages currently drawn
		private var pageList:Vector.<PageVo>;
		private var _pageWidth : Number;
		private var _pageHeight : Number;
		private var isOut : Boolean;
		private var _backgroundClickEnabled : Boolean = true; // use background click for some actions (unstick)
		
		
		//current edited page highlight
		private var pageHighLight:Sprite;
		private var pageHighLightBmd:BitmapData;
		private var pageHighLightShadow:DropShadowFilter;
		private var pageHighLightBlur:BlurFilter;
		
		//grid
		private var _showGrid:Boolean;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		private var flyfleafColorChoices:Sprite;

		

		
		public function EditionArea():void
		{		
			_instance = this; 
			
			// listen to drop event
			DragDropManager.instance.ITEM_DROPPED.add(onItemDropped);
			
			// listen to page click event from page navigator
			PageNavigator.PAGE_CLICKED.add(drawPageFromPageVo);
			
			//listen to cover change handler
			ProjectManager.COVER_CHANGED.add(coverChangedHandler);
			
			//listen to modification on any PageVo
			PageVo.PAGEVO_UPDATED.add(pageVoUpdateHandler);		
			
			PhotoSwapManager.SWAP_IMAGE_MODE.add(swapImageModeHandler);
			
			// opaque background for perfs
			//opaqueBackground = Colors.ORANGE; // goes out of bounds... can't do that
			
			
			// allow background click
			addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent):void{
				isOut = true;
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, outClick);
				stage.addEventListener(MouseEvent.MOUSE_DOWN, outClick);
			});
			addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void{
				isOut = false;
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, outClick);
			});
			
		}
		
		////////////////////////////////////////////////////////////////
		//	INIT
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * construct is the function used to construct the view, add children, prepare animations, everything before the show method.
		 * This function should be only called once after the creation of the view
		 */
		public function init():void
		{
			//retreive page list
			pageList = Infos.project.pageList
			
			// construct children if not already done
			if(! pageContainerWrapper )
			{
				// create page container
				pageContainerWrapper = new Sprite();
				addChild(pageContainerWrapper);
				pageContainer = new Sprite();
				pageContainerWrapper.addChild(pageContainer);
				
				// for perfs ?
				pageContainerWrapper.opaqueBackground = Colors.BACKGROUND_COLOR;
				pageContainer.opaqueBackground = Colors.BACKGROUND_COLOR;//0xe5e5e5;	
				
				// create transform tool
				if(!transformTool)
				transformTool = new TransformTool(this);		
				
				// add navigator
				pageNavigator = new PageNavigator(this, 0,550);
				
				//Button Bar (Swap popup, zoom etc...)
				buttonBar = new HBox(this);
				buttonBar.spacing = 2;
				expandPageNavigatorBtn = new ToolbarButton(ToolbarButton.TYPE_EXPAND, ResourcesManager.getString("tooltip.toolbar.button.expand.navigator"), expandPageNavigator)
				previewBtn = new ToolbarButton(ToolbarButton.TYPE_PREVIEW, ResourcesManager.getString("tooltip.toolbar.button.preview"), goFullScreen)
				
				
				// canvas detail box
				if(Infos.isCanvas)
				{
					canvasDetailBox = new Sprite();
					canvasDetailBox.opaqueBackground = 0xffffff;
					canvasDetailLabel  = new Label(canvasDetailBox, 0,0,"coucou", TextFormats.ASAP_REGULAR(12));
					addChild(canvasDetailBox);
					updateCanvasDetails();
				}
				
				
				
				// zoom box
				zoomBox = new library.edition.zoomSliderBox();
				zoomSlider = new HSlider(zoomBox, 25, 8, onZoomChange);
				zoomSlider.width = 58;
				zoomSlider.height = 8;
				zoomSlider.minimum = 1;
				zoomSlider.maximum = 2;
				zoomSlider.value = 1;
				
				zoomBox.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void
				{
					if(zoomSlider.value > 1) {
						addChild(zoomOverview);
						zoomOverview.show();
					} else if (zoomOverview && zoomOverview.parent){
						zoomOverview.parent.removeChild(zoomOverview);
					}
				});
				
				// create zoom button
				ButtonUtils.makeButton(zoomBox.plus, onZoomPlusHandler);
				ButtonUtils.makeButton(zoomBox.minus, onZoomMinusHandler);
				
				buttonBar.addChild(expandPageNavigatorBtn);
				buttonBar.addChild(previewBtn);
				//buttonBar.addChild(swapperBtn);
				//buttonBar.addChild(zoomBox);
				addChild(zoomBox);
				
				zoomOverview = new EditionZoomOverview();
				zoomOverview.START_DRAG.add(function():void{
					transformTool.unstick();
				});
				
				
				
				// 
				pageToolbar = new HBox();
				pageToolbar.alignment = HBox.MIDDLE;
				nextPageButton = new SimpleButton("edition.toolbar.page.next", Colors.BLUE, nextPageHandler, Colors.GREY, Colors.WHITE);
				previousPageButton = new SimpleButton("edition.toolbar.page.previous", Colors.BLUE, previousPageHandler, Colors.GREY, Colors.WHITE);
				firstPageButton = new SimpleButton("edition.toolbar.page.first", Colors.BLUE, firstPageHandler, Colors.GREY, Colors.WHITE);
				lastPageButton = new SimpleButton("edition.toolbar.page.last", Colors.BLUE, lastPageHandler, Colors.GREY, Colors.WHITE);
				
				nextPageButton.forcedWidth = previousPageButton.forcedWidth = firstPageButton.forcedWidth = lastPageButton.forcedWidth = 100;
				
				pageIndexField = new TextField();
				pageIndexField.embedFonts = true;
				pageIndexField.antiAliasType = AntiAliasType.ADVANCED;
				pageIndexField.multiline = false;
				pageIndexField.mouseEnabled = pageIndexField.mouseWheelEnabled = false;
				pageIndexField.defaultTextFormat = TextFormats.ASAP_REGULAR(12, Colors.GREY_DARK,TextFormatAlign.CENTER);
				pageIndexField.width = 150;
				pageIndexField.height = 15;
				//pageIndexField.y = 7;
				
				pageToolbar.addChild(firstPageButton);
				pageToolbar.addChild(previousPageButton);
				pageToolbar.addChild(pageIndexField);
				pageToolbar.addChild(nextPageButton);
				pageToolbar.addChild(lastPageButton);
				addChild(pageToolbar);
				
				
				// add edition toolbar
				editionToolbar= new EditionToolBar();
				editionToolbar.y = 10;
				addChild(editionToolbar);
				
				// LISTENERS
				// init action manager only when project is ready
				UserActionManager.instance.init(this, editionToolbar);
				
				
				//Listen for an update
				ProjectManager.PROJECT_UPDATED.add(projectUpdatedHandler);
				
				// TUTORIAL
				//Add this to tutorial flow
				var tutorialVo:TutorialStepVo = new TutorialStepVo(
					buttonBar,
					TutorialManager.KEY_FULLSCREEN_BTN,
					TutorialManager.ARROW_LEFT,
					new Point(buttonBar.width+20, buttonBar.height/2)
				);
				TutorialManager.instance.addStep(tutorialVo);
				
				//Add this to tutorial flow
				var tutorialVo:TutorialStepVo = new TutorialStepVo(
					editionToolbar,
					TutorialManager.KEY_EDITION_TOOLBAR,
					TutorialManager.ARROW_UP,
					new Point(buttonBar.width/2, buttonBar.height+20)
				);
				TutorialManager.instance.addStep(tutorialVo);
				
				//Add this to tutorial flow
				var tutorialVo:TutorialStepVo = new TutorialStepVo(
					pageToolbar,
					TutorialManager.KEY_PAGE_TOOLBAR_BTN,
					TutorialManager.ARROW_DOWN,
					new Point(pageToolbar.width/2, pageToolbar.y - 20)
				);
				TutorialManager.instance.addStep(tutorialVo);
				
				//Add this to tutorial flow
				var tutorialVo:TutorialStepVo = new TutorialStepVo(
					pageContainerWrapper,
					TutorialManager.KEY_EDITION_AREA,
					TutorialManager.ARROW_RIGHT,
					new Point(300, 300)
				);
				TutorialManager.instance.addStep(tutorialVo);
			}
			
			//create vector referencing the pages if it does not exist already
			if(!currentPagesVect) currentPagesVect = new Vector.<PageArea>();
			
			
			// draw first pages
			//drawPages(getPageToDraw(pageList[0]));
			drawPageFromPageVo(pageList[0],true);
			
			// update page navigator content
			pageNavigator.updateContent();
			
			// update layout
			updateLayout();
			
			ready = true;
		}
		
		/**
		 * Handle project change
		 * Add elements related to the current classname
		 * clear pages
		 * or init if not ready
		 */
		public function desktopProjectChange():void
		{
			/*//-----There is no projectVo
			
			//Case New project configurator (means no projectVo)
			if(!ProjectManager.instance.ready)
			{
				//Listen to PORJECT READY and re-run this function
				ProjectManager.PROJECT_READY.addOnce(desktopProjectChange);
				//Clear pages (nothing should be shown when configurator is in new project state)
				if(ready)
				{
					clearPages();
					pageNavigator.destroy();
					updatePageToolbar();
					removeHightlight();
				}
				//stop function
				return;
			}

			//-----There is a projectVo
			
			if(!ready)
			{
				//If new project coming from configurator 
				init();
			}
			else
			{
				//clear page
				clearPages();
				
				//retreive page list
				pageList = Infos.project.pageList
				
				//show/hide options
				displayOptions();
				
				// draw first pages
				//drawPages(getPageToDraw(pageList[0]));
				drawPageFromPageVo(pageList[0],true);
				
				// update page navigator content
				pageNavigator.updateContent();
				
				// update layout
				updateLayout();
			}*/
			
			//Listen to PORJECT READY
			//ProjectManager.PROJECT_READY.addOnce(init);
			//dispose();
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	SHORTCUTS (this is not super clean but it's the best way I found to avoid creating unecessory managers)
		////////////////////////////////////////////////////////////////

		/**
		 * does project need page navigator
		 */
		public function get hasPageNavigator():Boolean
		{
			if( Infos.isCanvas ) return false;
			else return (Infos.project.pageList.length>1);
		}

		
		/**
		 * return amount of pixel between two canvas pages
		 */ 
		public function get gapBetweenPages():int
		{
			if(Infos.isCalendar) return 20;
			if(Infos.isCards) 
			{
				if(CardsManager.instance.getNumCardsPagePerDraw() == 2)
					return 20;
				else if(CardsManager.instance.getNumCardsPagePerDraw() == 4)
					return 1;
				
				return 20;
			}
			if(Infos.isCanvas) return CanvasManager.instance.gapBetweenPages;
			return 0;
		}

		
		
		/**
		 * retrieve currently visible frame by it's frameVo
		 */
		public static function getVisibleFrameByFrameVo( frameVo : FrameVo ): Frame 
		{
			var currentPageList : Vector.<PageArea> = _instance.currentPagesVect;
			// check in rendred pages
			for (var i:int = 0; i < currentPageList.length; i++) 
			{
				var pageArea:PageArea = currentPageList[i];
				var frame : Frame = pageArea.getFrameByFrameVo(frameVo);
				if(frame) return frame;
			}
			
			return null;
		}
		
		
		/**
		 * retrieve currently visible frame(s) using this photovo
		 */
		public static function getVisibleFramesByPhotoVo( photoVo : PhotoVo ): Vector.<FramePhoto> 
		{
			var currentPageList : Vector.<PageArea> = _instance.currentPagesVect;
			var currentFrameList : Vector.<FramePhoto> = new Vector.<FramePhoto>;
			
			// check in rendred pages
			for (var i:int = 0; i < currentPageList.length; i++) 
			{
				var pageArea:PageArea = currentPageList[i];
				var pageFrameList : Vector.<FramePhoto> = pageArea.getFramesByPhotoVo(photoVo);
				for each ( var f:FramePhoto in pageFrameList )
				{
					currentFrameList.push(f);
				}
			}
			
			return currentFrameList;
		}
		
		
		
		
		/**
		 * page area for framevo
		 */
		public static function getPageAreaForFrameVo( frameVo : FrameVo ): PageArea 
		{
			var currentPageList : Vector.<PageArea> = _instance.currentPagesVect;
			
			// check in rendred pages
			for (var i:int = 0; i < currentPageList.length; i++) 
			{
				var pageArea:PageArea = currentPageList[i];
				var frame : Frame = pageArea.getFrameByFrameVo(frameVo);
				if(frame) return pageArea;
			}
			return null;
		}
		
		
		
		/**
		 * find nearest difference between two frame points
		 * > frame is the ref frame to check
		 * > snapMagnet is the size in pixel of the minimum frame diff to display line and make a snap
		 * 
		 * > returns an object with : 
		 * obj.diff = point // difference of horizontal and vertical with ref values
		 * obj.pos = point // position of the line to be drawn 
		 * obj.fy = frameVo // aligned frame vo horizontally
		 * obj.fx = frameVo // aligned frame vo vertically
		 */
		public static function findNearestSnapDiff( frame:Frame, snapMagnetSize:int ): Object 
		{
			var returnObj : Object = new Object();
			var snapDiff : Point = new Point(snapMagnetSize, snapMagnetSize);
			var snapPoint : Point = new Point();
			returnObj.diff = snapDiff;
			returnObj.pos = snapPoint;
			
			// if frame is rotated, we do not check values
			if(frame.frameVo.rotation != 0 ) return returnObj;
			
			// store 6 values to check ( 3 verticals and 3 horizontals ) for ref frame
			var refValues : Vector.<Number> = new Vector.<Number>(6);
			var i : int ;
			var j:int
			var k:int;
			var rf : Rectangle = frame.frameVo.boundRect;
			for (i=0; i < 3; i++) 
			{
				refValues[i] = frame.parent.localToGlobal(new Point(frame.frameVo.x + (i-1)*rf.width*.5)).x; // horizontal values
				refValues[i+3] = frame.parent.localToGlobal(new Point(0,frame.frameVo.y + (i-1)*rf.height*.5)).y; // vertical values
			}	
			
			// check in rendred pages
			var currentPageList : Vector.<PageArea> = _instance.currentPagesVect;
			var pageArea:PageArea;
			var frameList : Vector.<FrameVo>;
			var frameVo:FrameVo;
			var diffX : Number;
			var diffY : Number;
			var pos:Number;
			for (i = 0; i < currentPageList.length; i++) 
			{
				pageArea = currentPageList[i];
				frameList = pageArea.pageVo.layoutVo.frameList;
				for (j = 0; j < frameList.length ; j++) 
				{
					frameVo	= frameList[j];
					// conditions to check snap
					if(frameVo != frame.frameVo && !frameVo.dateAction && frameVo.rotation == 0 && !frameVo.uniqFrameLinkageID)
					{
						rf = frameVo.boundRect;
						// check coordinates
						for (k=0; k < 3; k++) 
						{
							// check horizontal diff
							pos = pageArea.localToGlobal(new Point(frameVo.x + (k-1)*rf.width*.5)).x;
							if( k == 1 ){ // check middle
								diffX = pos - refValues[1];
								if( Math.abs(diffX) < Math.abs(snapDiff.x)) { snapDiff.x = diffX; snapPoint.x = pos }; 
							}
							else {
								diffX = pos - refValues[2]; 
								if(Math.abs(diffX) < Math.abs(snapDiff.x)) { snapDiff.x = diffX; snapPoint.x = pos };
								diffX = pos - refValues[0]; 
								if(Math.abs(diffX) < Math.abs(snapDiff.x)) { snapDiff.x = diffX; snapPoint.x = pos };	
							}
							
							
							// check vertical diff
							pos = pageArea.localToGlobal(new Point(0,frameVo.y + (k-1)*rf.height*.5)).y;
							if( k == 1 ){ // check middle
								diffY = pos - refValues[4];
								if(Math.abs(diffY) < Math.abs(snapDiff.y)) { snapDiff.y = diffY; snapPoint.y = pos };// check middle
							}else{
								diffY = pos - refValues[5]; 
								if(Math.abs(diffY) < Math.abs(snapDiff.y)) { snapDiff.y = diffY; snapPoint.y = pos }; 
								diffY = pos - refValues[3]; 
								if(Math.abs(diffY) < Math.abs(snapDiff.y)) { snapDiff.y = diffY; snapPoint.y = pos };	
							}	
						}
					}
				}
			}
			
			return returnObj;
		}
		
		
		
		/**
		 * retrieve current page area from it's page vo (only in actual visible pages on screen
		 */
		public static function getCurrentPageAreaFromPageVo( pageVo : PageVo ): PageArea
		{
			if(!_instance){
				Debug.warn("EditionArea > getCurrentPageAreaFromPageVo : instance not yet ready");
				return null;
			}
			
			for (var i:int = 0; i < _instance.currentPagesVect.length; i++) 
			{
				if( _instance.currentPagesVect[i].pageVo == pageVo) return  _instance.currentPagesVect[i];
			}
			
			Debug.warn("EditionArea > getCurrentPageAreaFromPageVo : no page area found for the pageVo");
			return null;
		}
		
		/**
		 * Expand pageNavigator
		 */
		public function expandPageNavigator(e:Event = null):void 
		{
			if(!Infos.project)
				return;
			
			expandPageNavigatorBtn.changeIcon(ToolbarButton.TYPE_COLLAPSE);
			expandPageNavigatorBtn.changeTooltipLabel(ResourcesManager.getString("tooltip.toolbar.button.collapse.navigator"));
			expandPageNavigatorBtn.changeHandler(collapsePageNavigator);
			pageNavigator.expand(Infos.stage.stageHeight - this.y - 40,Infos.stage.stageWidth - this.x);
			pageToolbar.visible = false;
			pageContainerWrapper.visible = false;
			zoomBox.visible = false;
			previewBtn.visible = false;
			editionToolbar.visible = false;
			updateLayout();
		}
		
		/**
		 * Collapse pageNavigator
		 */
		public function collapsePageNavigator(e:Event = null):void 
		{
			if(!Infos.project)
				return;
			
			if(pageNavigator)
			{
				expandPageNavigatorBtn.changeIcon(ToolbarButton.TYPE_EXPAND);
				expandPageNavigatorBtn.changeTooltipLabel(ResourcesManager.getString("tooltip.toolbar.button.expand.navigator"));
				expandPageNavigatorBtn.changeHandler(expandPageNavigator);
				pageNavigator.collapse();
				pageToolbar.visible = true;
				pageContainerWrapper.visible = true;
				zoomBox.visible = true;
				previewBtn.visible = true;
				editionToolbar.visible = true;
				updateLayout();	
			}
			
		}
		
		/**
		 * Refresh currently displayed pages
		 */
		public static function Refresh():void 
		{
			if(_instance) {
				TweenMax.killDelayedCallsTo(_instance.refresh);
				TweenMax.delayedCall(0.2, _instance.refresh); // refresh after 0.2sec // so views and loads are correctly completed
			}
		}
		private function refresh():void
		{
			Debug.log("EditionArea.refresh");
			var currentPageVo:PageVo = currentPagesVect[0].pageVo;
			drawPageFromPageVo(currentPageVo,true);
			PageNavigator.RedrawPage(currentPageVo);
		}
		
		
		
		/**
		 * Go fullscreen
		 */
		public static function goFullScreen(e:Event = null):void 
		{
			if(!Infos.project)
				return;
			
			Debug.log("EditionArea > goFullScreen");
			try{
				Infos.stage.displayState = StageDisplayState.FULL_SCREEN;
			}catch(e:Error)
			{
				Debug.warn("EditionArea.goFullScreen error : " + e.toString());
				//PopupAbstract.Alert(ResourcesManager.getString("popup.fullscreenNotAllowed.title"),ResourcesManager.getString("popup.fullscreenNotAllowed.description"),false);
			}
		}
		
		/**
		 * is preview shortcut
		 */
		public static function isPreview():Boolean 
		{
			if(_instance.currentState == STATE_PREVIEW) return true;
			return false;
		}
		
		/**
		 * show grid 
		 */
		public static function showGrid():Boolean 
		{
			return _instance._showGrid;
		}
		
		
		/**
		 * background click enabled
		 */
		public static function set backgroundClickEnabled( value : Boolean ):void 
		{
			_instance._backgroundClickEnabled = value;
		}
		public static function get backgroundClickEnabled():Boolean 
		{
			return _instance._backgroundClickEnabled;
		}
		
		
		/**
		 * give the current number of photos on screen
		 */
		public static function get numPhotoOnScreen():Number
		{
			var currentPageList : Vector.<PageArea> = _instance.currentPagesVect;
			if(!currentPageList) return 0;
			
			var numPhotos : Number = 0;
			for (var i:int = 0; i < currentPageList.length; i++) 
			{
				var pageVo:PageVo = currentPageList[i].pageVo;
				for (var j:int = 0; j < pageVo.layoutVo.frameList.length; j++) 
				{
					var frame : FrameVo = pageVo.layoutVo.frameList[j];
					if(frame.type == FrameVo.TYPE_PHOTO && !frame.isEmpty) numPhotos ++;
				}
				
			}
			
			return numPhotos;
		}
		
		
		
		/**
		 * re-render page navigator (update current page display)
		 */
		public static function updatePageNavigator():void 
		{
			if(_instance.pageNavigator) _instance.pageNavigator.updateAllPages();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		public function get pageContainerHeight():Number
		{	
			if(currentPagesVect.length<1)
			{
				Debug.warn("EditionArea.get pageContainerHeight: no page shown, return 0");
				return 0;
			}
			
			if(Infos.isAlbum)
			{
				//Handle cover classic exception
				if(currentPagesVect[0] is PageCoverClassicArea)
					return pageContainer.getBounds(pageContainer).height;
			}
			
			if(Infos.isCalendar)
			{
				//on preview mode, pages are show vertically
				if(isPreview()){
					//Calculate the height based on the number of page currently shown
					var returnHeight:Number = 0;
					for (var i:int = 0; i < currentPagesVect.length; i++) 
					{
						var pageVo:PageVo = currentPagesVect[i].pageVo;
						returnHeight += pageVo.bounds.height;
						returnHeight += gapBetweenPages*i;
					}
					returnHeight = returnHeight*editorScale;
					
					return returnHeight	
				}
			}
			
			if(Infos.isCanvas)
			{
				//Calculate the height based on the number of page currently shown
				var numRows : Number = CanvasManager.instance.multipleRowsCount;
				returnHeight = (Infos.project.getPageBounds().height + CanvasManager.instance.edgeSize*2) * numRows;
				returnHeight += gapBetweenPages*(numRows-1);
				returnHeight *= editorScale;
				
				return returnHeight	
			}
			
			
			// by default we return the page bounds height * editorScale value
			return (currentPagesVect[0].pageVo.bounds.height)*editorScale;
		}

		public function get pageContainerWidth():Number
		{
			var returnWidth:Number = 0;
			var i:int = 0;
			var pageVo:PageVo;
			
			if(currentPagesVect.length<1)
			{
				Debug.warn("EditionArea.get pageContainerWidth: no page shown, return 0");
				return 0;
			}
			
			// ALBUM EXCEPTIONS
			if(Infos.isAlbum)
			{
				//Handle cover classic exception
				if(currentPagesVect[0] is PageCoverClassicArea)
					return pageContainer.getBounds(pageContainer).width;
				
				//Add exception for 1 page after front cover and last page before back cover
				if(hasFlyLeafPreview)
					returnWidth += currentPagesVect[0].pageVo.bounds.width;
			}
			
			// CALENDAR EXCEPTIONS
			if(Infos.isCalendar)
			{
				if(isPreview())	
					return (currentPagesVect[0].pageVo.bounds.width)*editorScale;
			}
			
			// CANVAS EXCEPTIONS
			if(Infos.isCanvas)
			{
				//Calculate the height based on the number of page currently shown
				var numCols : Number = CanvasManager.instance.multipleColsCount;
				returnWidth = (Infos.project.getPageBounds().width + CanvasManager.instance.edgeSize*2) * numCols;
				returnWidth += gapBetweenPages*(numCols-1);
				returnWidth *= editorScale;
				
				return returnWidth	
			}
			
			
			// DEFAULT
			//Calculate the width based on the number of page currently shown
			for (i = 0; i < currentPagesVect.length; i++) 
			{
				pageVo = currentPagesVect[i].pageVo;
				returnWidth += pageVo.bounds.width;
				returnWidth += gapBetweenPages*i;
			}
			returnWidth = returnWidth*editorScale;
			return returnWidth;	
		}
		
		/**
		 *
		 */
		public function get selectedFrame():Frame
		{
			if(transformTool) return transformTool.frame;
			return null;
		}

		/**
		*
		*/
		public function get currentPages():Vector.<PageArea>
		{			
			return currentPagesVect;
		}
		
		/**
		 *	return global (stage) page bounds rectangle
		 */
		public function getPagesBounds():Rectangle
		{
			var bounds : Rectangle = pageContainer.getRect(stage);
			return bounds;
		}
		
		
		/**
		 *	block frame mouse (while using transform tool for example)
		 */
		public function blockFrameMouse( flag : Boolean = true):void
		{
			pageContainer.mouseChildren = pageContainer.mouseEnabled = !flag;
		}
		
		
		/**
		 *
		 */
		public function get toolBarPosition():Rectangle
		{			
			return transformTool.toolBarPosition;
		}
		
				
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * intro is the show method, when adding view on display list, we should directly call the intro method
		 */
		public function intro():void
		{			
			
			
		}
		
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public function outro():void
		{					
		}
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public function destroy():void
		{		
			if(parent) parent.removeChild(this);
		}
		
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Dispose view
		 * 
		 */
		public function dispose():void
		{		
			clearPages();
			
			pageList = null
			
			if(pageContainerWrapper)
			{
				if(pageContainerWrapper.parent)
					pageContainerWrapper.parent.removeChild(pageContainerWrapper);
				pageContainerWrapper.removeChildren();
				pageContainerWrapper = null;
			}
			
			if(pageContainer)
			{
				if(pageContainer.parent)
					pageContainer.parent.removeChild(pageContainer);
				pageContainer.removeChildren();
				pageContainer = null;
			}
			
			if(transformTool)	
			{
				//leave transform tool
				transformTool.unstick(null,true);
			}
				
			if(pageNavigator)
			{
				if(pageNavigator.parent)
					pageNavigator.parent.removeChild(pageNavigator);
				pageNavigator.destroy();
				pageNavigator = null;
			}
			
			if(buttonBar)
			{
				if(buttonBar.parent)
					buttonBar.parent.removeChild(buttonBar);
				buttonBar = null;
			}
			
			if(expandPageNavigatorBtn)
			{
				if(expandPageNavigatorBtn.parent)
					expandPageNavigatorBtn.parent.removeChild(expandPageNavigatorBtn);
				expandPageNavigatorBtn = null;
			}
			
			if(previewBtn)
			{
				if(previewBtn.parent)
					previewBtn.parent.removeChild(previewBtn);
				previewBtn = null;
			}
			
			if(canvasDetailBox)
			{
				if(canvasDetailBox.parent)
					canvasDetailBox.parent.removeChild(canvasDetailBox);
				canvasDetailBox.removeChildren();
				canvasDetailBox = null;
			}
			
			if(zoomBox)
			{
				if(zoomBox.parent)
					zoomBox.parent.removeChild(zoomBox);
				zoomBox.removeChildren();
				zoomBox = null;
			}

			if(zoomOverview)
			{
				zoomOverview.START_DRAG.removeAll();
				zoomOverview.destroy();
			}
			
			
			if(pageToolbar)
			{
				if(pageToolbar.parent)
					pageToolbar.parent.removeChild(pageToolbar);
				pageToolbar.removeChildren();
				pageToolbar = null;
			}
				
			if(editionToolbar)
				editionToolbar.destroy();
				
			ProjectManager.PROJECT_UPDATED.remove(projectUpdatedHandler);
			
			ready = false;
		
		}
		
		
		/**
		 * Cover has changed, we need to update if the cover is currently displayed
		 */
		private function coverChangedHandler():void
		{
			var currentPageVo:PageVo = currentPagesVect[0].pageVo;
			if(currentPageVo.index == 0)
			{
				drawPageFromPageVo(currentPageVo,true);
				PageNavigator.RedrawPage(currentPageVo);
			}
		}
		
		
		/**
		 * a PageVo have been updated, if it belong to one of the current drown pages, redraw them!
		 */
		private function pageVoUpdateHandler(pageVo:PageVo):void
		{
			for (var i:int = 0; i < currentPagesVect.length; i++) 
			{
				var page:PageArea = currentPagesVect[i];
				if(page.pageVo == pageVo)
				{
					drawPageFromPageVo(pageVo, true, false);
				}
			}
		}
		
		
		/**
		 * update layout = render	 
		 */
		public function updateLayout(e:Event = null):void
		{
			if(!ready || !Infos.project) return; // do nothing if view isn't ready
			if(currentPagesVect.length < 1) return; 
			
			// check if state is different than display state
			var newState : String = (stage.displayState == StageDisplayState.NORMAL)? STATE_NORMAL : STATE_PREVIEW;
			if( currentState != newState ) setState(newState);
			
			//if event is not null, its a stage resize, so we collapse pageNavigator
			if(e != null)
				collapsePageNavigator();
			
			// fixed y position
			y = 50;// 84; // = top bar height
			
			// get real working edition width
			var editionWidth :Number = (!isPreview())?Infos.stage.stageWidth - this.x:Infos.stage.stageWidth;
			var editionHeight :Number = (!isPreview())?Infos.stage.stageHeight - this.y:Infos.stage.stageHeight;
			
			//
			// If we are in canvas mode + multiple layout, we need to change page position depending on state
			if(Infos.isCanvas)// && CanvasManager.instance.isMultiple)
			{ 
				var nextPageAreaPosPoint:Point = new Point() ;
				var project : ProjectVo = Infos.project ;
				var pageArea : PageArea;
				for (var i:int = 0; i < currentPagesVect.length; i++) 
				{
					pageArea = currentPagesVect[i];
					pageArea.x = 0 + nextPageAreaPosPoint.x; //positionate 
					pageArea.y = 0 + nextPageAreaPosPoint.y; //positionate 
					nextPageAreaPosPoint = CanvasManager.instance.getPagePosition(i, isPreview());
					if(isPreview()) pageArea.filters = [new DropShadowFilter(10,45,0,.2,10,10)];
					else pageArea.filters = [];
				}
			}
			
			
			//if is One page project hide things we don't need (ex: magnet calendars)
			if(!hasPageNavigator)
			{
				if(expandPageNavigatorBtn && expandPageNavigatorBtn.parent)
				{
					expandPageNavigatorBtn.parent.removeChild(expandPageNavigatorBtn);
					buttonBar.draw();
				}
				
				if(pageNavigator)
					pageNavigator.visible = false;
				
				if(pageToolbar)
					pageToolbar.visible = false;
			}
			else
			{
				if(expandPageNavigatorBtn)
				{
					buttonBar.addChild(expandPageNavigatorBtn);
					buttonBar.draw();
				}
				
				if(pageNavigator)
					pageNavigator.visible = true;
				
				if(pageToolbar)
					pageToolbar.visible = true;
			}
			
			// positionate pageNavigator
			var tabOpen : Boolean = (this.x < 200 )? false : true// left tab open or not
			if(pageNavigator){
				pageNavigator.x = (tabOpen)? -25 :-12;
				pageNavigator.y = (hasPageNavigator)?editionHeight - pageNavigator.height:editionHeight;
				pageNavigator.setSize(editionWidth+25,pageNavigator.height);
				
				if(!isPreview() && hasPageNavigator)
				editionHeight -= pageNavigator.height;
			}
			
			// Button Bar
			if(buttonBar){
				buttonBar.x = (tabOpen)? -13 : -12;
				buttonBar.y = pageNavigator.y - buttonBar.height - 2;	
			}
			
			if(canvasDetailBox){
				if(buttonBar){
					canvasDetailBox.y = buttonBar.y +buttonBar.height - canvasDetailBox.height-1;
					canvasDetailBox.x = buttonBar.x + buttonBar.width + 5;
				}
			}
			
			//zoomBOX
			if(zoomBox)
			{
				zoomBox.x = editionWidth - zoomBox.width - 2;
				zoomBox.y = pageNavigator.y - zoomBox.height - 2;	
			}
			
			// Page toolbar
			if(pageToolbar){
				pageToolbar.x = (editionWidth - pageToolbar.width) *.5;
				pageToolbar.y = (!isPreview())? pageNavigator.y  - 40:editionHeight - 70;	
				
				if(hasPageNavigator)
					editionHeight -= 45;
			}
			
			// editor vertical and horizontal margin
			var hMargin : Number = 35; 
			var vMargin : Number = 35;
			
			// check zoom scale from slider
			var zoomScale : Number = zoomSlider.value;
			/*
			if( zoomScale != 1 && editorMask){
				editorMask.cacheAsBitmap = false;
				editorMask.x = hMargin;
				editorMask.y = vMargin;
				editorMask.width = editionWidth-2*hMargin;
				editorMask.height = editionHeight-2*vMargin;
				pageContainer.mask = editorMask;
				editorMask.cacheAsBitmap = true;
				addChild(editorMask);
			} else {
				pageContainer.mask = null;
				if(editorMask && editorMask.parent) removeChild(editorMask);
			}
			*/
			pageContainerWrapper.scrollRect = new Rectangle(0,0,editionWidth, editionHeight);
			//pageContainer.filters = [new DropShadowFilter(3,90,0,0.15,10,10)];
			
			//pageContainer.o
			
			// reset editor scale (for calculation of pageWidth)
			editorScale = 1;
			
			// check fit scale = scale needed to fit in edition width and height
			var fullWidth : Number = pageContainerWidth;
			var fullHeight : Number = pageContainerHeight;
			var fitScaleWidth : Number = (editionWidth-hMargin*2) / (pageContainerWidth + 20); // scale to allow the
			var fitScaleHeight:Number = (editionHeight-vMargin*2) / (pageContainerHeight + 20); // scale to allow the
			
			editorScale = (fitScaleWidth<=fitScaleHeight)?fitScaleWidth:fitScaleHeight;
			
			if(!EditionArea.isPreview())
			{
				if(editorScale > 1) editorScale =1;
			}
			editorScale *= zoomScale;
			
			//scale pageContainer
			pageContainer.scaleX = pageContainer.scaleY = editorScale;
			if(currentPagesVect[0] is PageCoverClassicArea)
			{
				pageContainer.x = Math.round( editionWidth - pageContainerWidth*editorScale) * .5;
				pageContainer.y = Math.round( editionHeight- pageContainerHeight*editorScale)* .5 + editionToolbar.height;
			}
			/*
			else if(Infos.isCanvas) {
				pageContainer.x = Math.round(( editionWidth - pageContainerWidth) * .5 - CanvasManager.instance.edgeSize);
				pageContainer.y = Math.round(( editionHeight - pageContainerHeight)* .5 + editionToolbar.height + 15 - CanvasManager.instance.edgeSize); // no navigator and a edge, we need to change the y positionning a bit to make it "joli"
			}
			*/
			else
			{
				pageContainer.x = Math.round( editionWidth - pageContainerWidth) * .5;// - 20 why is this "- 20" was there??
				pageContainer.y = Math.round( editionHeight - pageContainerHeight)* .5 + editionToolbar.height - 20;
			}
			
			
			
			// unstick transform tool
			transformTool.unstick();
			
			// update zoom overview
			if(zoomScale > 1) {
				zoomOverview.WIDTH = buttonBar.width*1.5;
				zoomOverview.update(pageContainer, fullWidth, fullHeight, pageContainerWrapper.scrollRect);
				zoomOverview.x = zoomBox.x +zoomBox.width- zoomOverview.width-5;
				zoomOverview.y = zoomBox.y - zoomOverview.height-5;
				addChild(zoomOverview);
			} else if (zoomOverview && zoomOverview.parent){
				zoomOverview.parent.removeChild(zoomOverview);
			}
			
			//resize item in flyleaf
			if(hasFlyLeafPreview)
			{
				if(currentPagesVect && currentPagesVect.length > 0)
				{
					var fakeBounds:Rectangle = currentPagesVect[0].pageVo.bounds;
					var pagePosX:Number = currentPagesVect[0].pageVo.index != 1?0:currentPagesVect[0].pageVo.bounds.width;
					var fakePosX:Number = currentPagesVect[0].pageVo.index != 1?currentPagesVect[0].pageVo.bounds.width:0;
					
					//flyleaf title
					if(flyfleafColorChoices)
					{
						TweenMax.killTweensOf(flyfleafColorChoices);
						TweenMax.to(flyfleafColorChoices,.5,{delay:.5, alpha:1});
						flyfleafColorChoices.x = fakePosX + 15;
						flyfleafColorChoices.y = 15;						
						flyfleafColorChoices.scaleX = flyfleafColorChoices.scaleY = 1/editorScale;						
					}
					
					//add more pages
					if(addMorePage)
					{
						addMorePage.scaleX = addMorePage.scaleY = 1/editorScale;
						addMorePage.x = fakePosX + (fakeBounds.width - addMorePage.width)*0.5;
						addMorePage.y = (fakeBounds.height - addMorePage.height)*0.5;
					}
					
				}
				
			}
			
			
			// notify resize
			EDITOR_RESIZE.dispatch(editorScale);
			
			/*graphics.clear();
			graphics.beginFill(0xFF0000);
			graphics.drawRect(0,0,width-10,height-10);
			graphics.endFill();*/
		
		}	
		
		
		
		
		/**
		 * Draw selected page on the edition AREA
		 * 
		 */
		private function drawPages(pageIndexes:Array, withAnim:Boolean = true):void
		{
			//Fade effect
			pageContainer.alpha = 0;
						
			//If the page to draw is aclassic cover, let's use a custom construc function for it
			var firstPageVo:PageVo =  Infos.project.pageList[pageIndexes[0]];
			if(firstPageVo is PageCoverClassicVo)
			{
				drawClassicCover(firstPageVo as PageCoverClassicVo);
				updatePageToolbar();
				pageContainer.graphics.clear();
				if(isPreview())
					setFullScreenShadow(false)
				
				if(stage)
					updateLayout();
				return;
			}
			else if( isPreview() ) setFullScreenShadow(true)
				
			
			
			//Draw a basic pageArea with its pageVo
			/*var nextPageAreaPosX:Number = 0;
			var nextPageAreaPosY:Number = 0;*/
			var nextPageAreaPosPoint:Point = new Point();
			var project : ProjectVo = Infos.project;
			for (var i:int = 0; i < pageIndexes.length; i++) 
			{
				var index:int = pageIndexes[i];
				if(index<project.pageList.length)
				{
					var pageVo:PageVo = project.pageList[index] as PageVo; //get pageVo from projec
					var pageArea:PageArea = new PageArea(pageVo); // create the page's view
					pageArea.FRAME_CREATED.add(registerFrame);
					pageArea.FRAME_DELETED.add(frameDeleted);
					pageArea.x = 0 + nextPageAreaPosPoint.x; //positionate 
					pageArea.y = 0 + nextPageAreaPosPoint.y; //positionate 
					
					// EDIT : we do not use page area as drop target as we now have always a background
					//DragDropManager.instance.addDropTarget(pageArea, [DragDropManager.TYPE_MENU_BKG, DragDropManager.TYPE_MENU_PHOTO,DragDropManager.TYPE_MENU_LAYOUT, DragDropManager.TYPE_MENU_CLIPART]);//, DragDropManager.TYPE_MENU_OVERLAYER ]); //Reference the pageArea as a dropTarget
					
					currentPagesVect.push(pageArea); // Add the pageArea to the currently drawn pageAreas vector
					if(Infos.isCanvas) nextPageAreaPosPoint = CanvasManager.instance.getPagePosition(i);
					else nextPageAreaPosPoint = getNextPagePosition(nextPageAreaPosPoint,pageVo); // get the nextpage position
					
					pageArea.init(); //init it!
					pageContainer.addChild(pageArea); //add pageArea to EditionArea
					
					// draw edge if we are in canvas
					if(Infos.isCanvas) {
						pageArea.drawCanvasEdge();
						if(Infos.project.canvasType == CanvasManager.TYPE_KADAPAK){
							pageArea.drawKadapakBorder();
						}
					}
					
					
					//If no current edited page, select the cover
					if(!PagesManager.instance.currentEditedPage && i==0)
					PagesManager.instance.currentEditedPage = pageVo;
						
				}
				else
					Debug.warn("Edtion Area warn: Page index requested doesn't exist");//App.showError("Edtion Area Error","","Page index requested doesn't exist");
			}	
				
			//handle fake page after and vefore cover
			drawFlyLeaf();
			
			trace("hasFakePagePreview: "+hasFlyLeafPreview);
			// update page toolbar and index
			updatePageToolbar();
			
			//Intro animation
			TweenMax.killDelayedCallsTo(updateLayout);
			TweenMax.delayedCall(.3,updateLayout); //if(stage) // EDIT : was .3, see difference
			
			
			//Avoid transparency bug
			removeHightlight();
			
			//Between page gradient
			showPageGradient();
			
			//fade effect
			var duration:Number = (withAnim)? .5 : 0;
			var delay : Number = (withAnim)? .3 : 0;
			
			CONFIG::online
			{
				TweenMax.killTweensOf(pageContainer);
				TweenMax.to(pageContainer, duration, {alpha:1, delay:delay, onComplete:pageDrawComplete});
			}
			
			//remove animation for offline, avoid having tween at the same time as big image loading
			CONFIG::offline
			{
				pageContainer.alpha = 1;
				TweenMax.killDelayedCallsTo(pageDrawComplete);
				TweenMax.delayedCall(0,pageDrawComplete);
				//pageDrawComplete();
			}
			
			
			
			// update toolbar
			UserActionManager.instance.updateToolbar();
			
			//drawgrid if needed
			drawGrid();
			
		}
		
		private function removeHightlight():void
		{
			if(pageHighLight)
			{
				TweenMax.killTweensOf(pageHighLight);
				pageHighLight.alpha = 0;
				pageHighLight.visible = false;
			}
		}
		
		/**
		 * Calculate next page position based on the project type (ALBUMS, CALENDARD) and the total number of pages to show
		 * Othe conditions could be added later on (is preview or not for example)
		 * */
		private function getNextPagePosition(nextPageAreaPosPoint:Point, pageVo:PageVo):Point
		{
			if(Infos.isAlbum)
			{
				nextPageAreaPosPoint.x = nextPageAreaPosPoint.x + pageVo.bounds.width;
				nextPageAreaPosPoint.y = nextPageAreaPosPoint.y;
				return nextPageAreaPosPoint;
			}
			
			if(Infos.isCalendar)
			{
				
				if(isPreview())
				{
					//as for now, in previews, calendars that has more than 1 page to show, are shown 1 page under the other (top/bottom)
					nextPageAreaPosPoint.x = nextPageAreaPosPoint.x;
					nextPageAreaPosPoint.y = nextPageAreaPosPoint.y +  pageVo.bounds.height + gapBetweenPages;
				}
				else
				{
					nextPageAreaPosPoint.x = nextPageAreaPosPoint.x + pageVo.bounds.width + gapBetweenPages;
					nextPageAreaPosPoint.y = nextPageAreaPosPoint.y;
				}
				
				return nextPageAreaPosPoint;
			}
			
			if(Infos.isCards)
			{
				nextPageAreaPosPoint.x = nextPageAreaPosPoint.x + pageVo.bounds.width + gapBetweenPages;
				nextPageAreaPosPoint.y = nextPageAreaPosPoint.y;
				return nextPageAreaPosPoint;
			}
			
			Debug.warn("EDITION AREA > DRAW PAGES > get next page position > Product class is not know or invalid: "+Infos.project.classname);
			return null;
		}
		
		/**
		 * Draw a gradient to enhance "BOOK PAGE" effect
		 * Can be use on CALENDARS as well, but has to be horizontal sometime
		 * This function handle where,when and how this gradient show up or not
		 * */
		private function showPageGradient():void
		{
			var matrix: Matrix = new Matrix();
			var colors: Array = [Colors.GREY_DARK,Colors.BLACK,Colors.GREY_DARK];
			var alphas: Array = [0,.2,0];
			var ratios: Array=[45,128,173];
						//clear pageGradient
			pageGradient.graphics.clear();
			
			//Hande gradient for albums
			if(Infos.isAlbum)
			{
				//no gradient if it's cover
				if(!currentPagesVect[0].pageVo.isCover)
				{
					pageGradient.x = currentPagesVect[0].pageVo.bounds.width - 45;
					pageGradient.y = 0;
					matrix.createGradientBox(90,currentPagesVect[0].pageVo.bounds.height);
					pageGradient.graphics.beginGradientFill(GradientType.LINEAR,colors,alphas,ratios,matrix);
					pageGradient.graphics.drawRect(0,0,90,currentPagesVect[0].pageVo.bounds.height+1);
					pageGradient.graphics.endFill();
					pageContainer.addChild(pageGradient);
				}
			}
			
			//Handle gradient for Calendars
			if(Infos.isCalendar)
			{
				//no gradient if it's cover
				/*if(!currentPagesVect[0].pageVo.isCover)
				{
					pageGradient.x = currentPagesVect[0].pageVo.bounds.width - 45;
					pageGradient.y = 0;
					matrix.createGradientBox(90,currentPagesVect[0].pageVo.bounds.height);
					pageGradient.graphics.beginGradientFill(GradientType.LINEAR,colors,alphas,ratios,matrix);
					pageGradient.graphics.drawRect(0,0,90,currentPagesVect[0].pageVo.bounds.height+1);
					pageGradient.graphics.endFill();
					pageContainer.addChild(pageGradient);
				}*/
			}
		}
		
		/**
		 * Draw a fake page when on fullscreen to understand where is the cover 
		 * */
		private function handleCalendarPageDisplay():Boolean
		{
			// Calendars requires to redraw page if there is more than 1 page display
			// Because 2 calendar pages in editor are shown horizontally, but vertically in preview 
			if(Infos.isCalendar && currentPagesVect.length > 1)
			{
				drawPageFromPageVo(currentPagesVect[0].pageVo,true,true);
				return true;
			}
			
			return false;
		}
		
		/**
		 * Draw flyleaf
		 * */
		private function clearFlyLeaf():void
		{
			//clear btn 
			if(addMorePage)
			{
				if(addMorePage.parent)
					addMorePage.parent.removeChild(addMorePage);
			}
			
			//clear btn 
			if(flyfleafColorChoices)
			{
				if(flyfleafColorChoices.parent)
					flyfleafColorChoices.parent.removeChild(flyfleafColorChoices);
			}
		}
		private function drawFlyLeaf():void
		{
			//clear first
			clearFlyLeaf();
			
			if( currentPagesVect.length==1  && !(currentPagesVect[0].pageVo.isCover) && Infos.project.classname == ProductsCatalogue.CLASS_ALBUM)
			{
				//draw flyleaf page 
				var fakeBounds:Rectangle = currentPagesVect[0].pageVo.bounds;
				var pagePosX:Number = currentPagesVect[0].pageVo.index != 1?0:currentPagesVect[0].pageVo.bounds.width;
				var fakePosX:Number = currentPagesVect[0].pageVo.index != 1?currentPagesVect[0].pageVo.bounds.width:0;
				pageContainer.graphics.clear();
				var color:Number = PagesManager.instance.getFlyLeafColorByID(Infos.project.flyleafColor);
				pageContainer.graphics.lineStyle(1,color, .5);
				pageContainer.graphics.beginFill(color, 1);
				pageContainer.graphics.drawRect(fakePosX,0,fakeBounds.width,fakeBounds.height);
				currentPagesVect[0].x = pagePosX;//currentPagesVect[0].pageVo.bounds.width;
				hasFlyLeafPreview = true;
				
				//Add flyleaf options
				if(!(isPreview()) && Infos.project.type != ProductsCatalogue.ALBUM_CASUAL)
				{
					//color choices wrapper
					flyfleafColorChoices = new Sprite();
					pageContainer.addChild(flyfleafColorChoices);
					flyfleafColorChoices.alpha= 0;
					//title
					var price:String = String(PriceManager.instance.getFlyleafCost(Infos.project.docType,Infos.project.flyleafColor));
					var titleStr:String = ResourcesManager.getString("albums.flyleaf.title");
					var selectedLabelStr:String = "("+ResourcesManager.getString("album.flyleaf.label."+Infos.project.flyleafColor)+ ", "+price+"€)";
					flyleafTitle = new LabelTictac(flyfleafColorChoices, 0,0,titleStr ); //
					flyleafTitle.textFormatUpdate(TextFormats.ASAP_BOLD(15,Colors.GREY));
					flyleafSelectedLabel = new LabelTictac(flyfleafColorChoices, 0,flyleafTitle.y + flyleafTitle.height + 5,selectedLabelStr ); //
					flyleafSelectedLabel.textFormatUpdate(TextFormats.ASAP_BOLD(13,Colors.GREY));
					//boxes
					var boxesContainer:HBox = new HBox(flyfleafColorChoices, 5, flyleafSelectedLabel.y + flyleafSelectedLabel.height + 15);
					boxesContainer.alignment = HBox.MIDDLE;
					
						
					for (var i:int = 0; i < PagesManager.instance.flyleafColorsAvailable.length; i++) 
					{
						//create color boxes
						var colorBox : FlyleafColorBox = new FlyleafColorBox();
						colorBox.color = PagesManager.instance.flyleafColorsAvailable[i].color;
						colorBox.id = PagesManager.instance.flyleafColorsAvailable[i].id;
						colorBox.tooltipVo = new TooltipVo("album.flyleaf.box.tooltip."+colorBox.id);
						colorBox.init();
						//Set selection
						colorBox.selected(Infos.project.flyleafColor == colorBox.id);
						//click
						colorBox.addEventListener(MouseEvent.CLICK,function(e:MouseEvent):void{
							Infos.project.flyleafColor = e.currentTarget.id;
							//update project price
							ProjectManager.instance.updateProjecPrice();
							// add user action for undo process
							UserActionManager.instance.addAction(UserActionManager.TYPE_PROJECT_UPDATE);
							EditionArea.Refresh();
							
						});

						colorBox.buttonMode = true;
						
						boxesContainer.addChild(colorBox);	
					}
					//set scaling
					flyfleafColorChoices.scaleX = flyfleafColorChoices.scaleY = 1/editorScale;
					
				}
				
				//add more page btn if necessary 
				var maxPages:int = ProjectManager.instance.getMaxPages();
				var currentPagesNumber:int = Infos.project.flooredNumPages;
				if(currentPagesNumber && !(isPreview()) && currentPagesVect[0].pageVo.index != 1)
				{
					if( maxPages >= currentPagesNumber + 10)
					{
						Debug.log("EditionArea.handleFakePageAfterAndBeforeCover : add more pages btn");
						if(!addMorePage)
							addMorePage = new SimpleThinButton("btn.add.more.pages.label",Colors.BLUE,addMorePageToProject,Colors.YELLOW, Colors.BLACK, Colors.WHITE,1,false,false);  //SimpleButton.createBlueButton("add 10 pages",addMorePageToProject);
						
						addMorePage.scaleX = addMorePage.scaleY = 1/editorScale;
						
						addMorePage.x = fakePosX + (fakeBounds.width - addMorePage.width)*0.5;
						addMorePage.y = (fakeBounds.height - addMorePage.height)*0.5;
						pageContainer.addChild(addMorePage);
					}
				}
			}
			else
			{
				
				pageContainer.graphics.clear();
				currentPagesVect[0].x = 0;
				hasFlyLeafPreview = false;
			}
		}
		
		private function addMorePageToProject():void
		{
			var newProjectTypeCode:String = Infos.project.docPrefix + String(Infos.project.flooredNumPages + 10);
			var newProjectDocType:String = ProjectManager.instance.getDocTypeIDByCode(newProjectTypeCode);
			if(newProjectDocType != null)
			{
				//fromMorePage = true;
				ProjectManager.instance.updateCurrentAlbumProject(newProjectDocType, Infos.project.type, Infos.project.coverType);
				UserActionManager.instance.addAction(UserActionManager.TYPE_PROJECT_UPGRADE);
			}
			else
			{
				Debug.warnAndSendMail("EditionArea.addMorePage: failed");
				PopupAbstract.Alert("popup.error.title","popup.warn.support.desc",false,null,null,false);
			}
		}
		
		private function pageDrawComplete( highlightPage:Boolean = true ):void
		{
			PAGE_DRAW_COMPLETE.dispatch();
			if(highlightPage)
			highLightEditedPage();
		}
		
		/**
		 * Show which page is currently edited
		 **/
		public function highLightEditedPage():void
		{
			if(PagesManager.instance.currentEditedPage is PageCoverClassicVo || currentState == STATE_PREVIEW ){
				
				if(pageHighLight &&  pageHighLight.parent)
				{
					pageHighLight.parent.removeChild(pageHighLight);
				}
				
				return;
			}
			
			var pageVo:PageVo = PagesManager.instance.currentEditedPage;
			if(pageVo == null)
			{
				Debug.warn("Edition Area > highLightEditedPage > Trying to highlight current Edited page: But currentEditedPage is null");
				return;
			}
			
			if(!pageHighLight)
			pageHighLight = new Sprite();
			
			if(!pageHighLightBmd)
			pageHighLightBmd = new library.edition.pagearea.highlight.fill.bmp();
			
			if(!pageHighLightShadow)
			pageHighLightShadow = new DropShadowFilter(3,90,0,.15,10,10,1,BitmapFilterQuality.HIGH);
			
			if(!pageHighLightBlur)
				pageHighLightBlur = new BlurFilter(1,1,BitmapFilterQuality.HIGH);
			
			pageHighLight.graphics.clear();
			pageHighLight.graphics.beginBitmapFill(pageHighLightBmd,null, true);
			pageHighLight.graphics.drawRect(-10,-10,pageVo.bounds.width + 20, pageVo.bounds.height + 20);
			pageHighLight.graphics.endFill();
			pageHighLight.cacheAsBitmap = true;
			pageHighLight.filters = [pageHighLightBlur];
			//pageHighLight.filters = [pageHighLightShadow];
			
			var pageArea:PageArea = getCurrentPageAreaFromPageVo(pageVo);
			if(pageArea == null)
			{
				Debug.warn("Edition Area > highLightEditedPage > Trying to highlight current Edited page: But pagearea is null");
				return;
			}
			
			if(pageHighLight.x == pageArea.x) {
				// we are at right place; do nothing
				return;
			}
			pageHighLight.x = pageArea.x;
			pageHighLight.y = pageArea.y;
			pageHighLight.alpha = 0;
			pageHighLight.visible = false;
			
			pageContainer.addChildAt(pageHighLight,0);
			//pageContainer.addChild(pageHighLight);
			
			showHighLight();
		}
		
		private function showHighLight():void
		{
			//do not show hightlights for canvas
			if(Infos.isCanvas)
				return;
			
			//Avoid transparency bug
			if(pageHighLight)
			{
				pageHighLight.visible = false;
				pageHighLight.alpha = 0;
				TweenMax.killTweensOf(pageHighLight);
				TweenMax.to(pageHighLight, .5, {autoAlpha:.4, tint:Colors.GREEN, delay:0});
			}
		}
		
		
		/**
		 * on image swap mode start or stop
		 */
		private function swapImageModeHandler( flag : Boolean ):void
		{
			transformTool.unstick(); // unstick transform tool when switch swap mode
			if(pageNavigator) pageNavigator.enabled = !flag;
			buttonBar.enabled = !flag;
			pageToolbar.enabled = !flag;
		
			// reset out click to avoid outzone click bug
			if(!flag) {
				isOut = false;
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, outClick);
			}
			
		}
		
		
		
		
		/**
		 * Construc the Classic cover page and its customization options
		 **/
		private function drawClassicCover(pageVo:PageCoverClassicVo):void
		{
			hasFlyLeafPreview = false; 
			clearFlyLeaf();
			
			var pageArea:PageCoverClassicArea = new PageCoverClassicArea(pageVo);
			pageArea.init();
			pageContainer.addChild(pageArea); //add pageArea to EditionArea
			
			//If no current edited page, select the cover
			//if(!PagesManager.instance.currentEditedPage)
			PagesManager.instance.currentEditedPage = pageVo;
			
			currentPagesVect.push(pageArea);
			
			//fade effect
			TweenMax.killTweensOf(pageContainer);
			TweenMax.to(pageContainer, .5, {alpha:1, onComplete:pageDrawComplete, onCompleteParams:[false]});
			//remove highLight
			if(pageHighLight && pageHighLight.parent)
			pageHighLight.parent.removeChild(pageHighLight);
			// update toolbar
			UserActionManager.instance.updateToolbar();
		}		
		
		/**
		 * show specific page from a pageVo
		 * 1-> signal comes from pageNavigator
		 * 
		 * 2-> signal comes from a pageVo that have been updated 
		 * Just using this function to destroy the updated page, a recreate it based on the updated data
		 * Could have deleted everything and recreate frame by frame... but this seems quicker at this point
		 */
		private function drawPageFromPageVo(vo:PageVo, forceRedraw:Boolean = false, withAnim:Boolean = true):void
		{
			Debug.log("EditionArea.drawPageFromPageVo : "+vo);
			if(!vo){
				Debug.warn(" >> no page vo, do not continue");
				return;
			}
			//
			var pagesToDraw : Array = getPageToDraw(vo);
			
			var redrawPage:Boolean = true;
			for (var i:int = 0; i < currentPagesVect.length; i++) 
			{
				if(currentPagesVect[i].pageVo == vo) redrawPage = false;
			}
			
			if(redrawPage || forceRedraw){
				clearPages();
				drawPages(pagesToDraw, withAnim);	
			}
			
			
			//set the current edited page
			PagesManager.instance.currentEditedPage = vo;
			
			//no redraw, but the current edited page may have change, so showHighlight
			if(!redrawPage && !forceRedraw)
				highLightEditedPage();
		}
		
		/**
		 * Return an array of page indexes to draw
		 * based on the the project class and the selected index
		 **/
		private function getPageToDraw(vo:PageVo):Array
		{
			Debug.log("Edition Area.GetPageToDraw : "+vo);
			if(!vo) {
				Debug.warn(" >> no page vo, we do not continue" );
				return null;
			}
			
			
			var pagesToDraw:Array;
			var projectClass:String = ProjectManager.instance.project.classname;
			
			//security when removing pages from project
			if(vo.index > pageList.length-1)
				vo = pageList[pageList.length-1];
			
			switch(projectClass)
			{
				case ProductsCatalogue.CLASS_ALBUM:
					
					if(vo.index == pageList.length-1) return[vo.index];
					if(vo.index == 0 || vo.index == 1) pagesToDraw = [vo.index]
					else if(vo.index%2 ==0) pagesToDraw = [vo.index, vo.index + 1];
					else pagesToDraw = [vo.index-1, vo.index];
					break;
				
				case ProductsCatalogue.CLASS_CALENDARS:					
					if(Infos.project.docCode == "WCAL2" || Infos.project.docCode == "WCAL3") //show 2 pages for those projects
					{
						if(vo.index == 0) pagesToDraw = [vo.index]
						else if(vo.index%2 != 0) pagesToDraw = [vo.index, vo.index + 1];
						else pagesToDraw = [vo.index-1, vo.index];
						
						return pagesToDraw;
					}
					
					if(!isPreview())pagesToDraw = [vo.index];
					else pagesToDraw = [vo.index];
					
					break;
				
				case ProductsCatalogue.CLASS_CARDS:
					pagesToDraw = getCardPagesToDraw(vo);
					break;
				
				case ProductsCatalogue.CLASS_CANVAS:
					pagesToDraw = getCanvasPagesToDraw(vo);
					break;
			}
			
			return pagesToDraw;
		}		
		// get CARDS pages to draw
		private function getCardPagesToDraw(vo:PageVo):Array
		{	
			var pagesToDraw:Array = [];
			var pagePerDraw:int = CardsManager.instance.getNumCardsPagePerDraw();
			var rest:int = vo.index%pagePerDraw;
			var firstPageIndex:int = vo.index - rest;
			
			// Display settings for 2 Pages cards
			if(pagePerDraw==2) 
			{
				for (var i:int = 0; i < pagePerDraw; i++) 
				{
					pagesToDraw.push(firstPageIndex+i);
				}
			}
			// Display settings for 4 pages cards
			// Show first and last page alone
			// Show inside pages together
			else if(pagePerDraw == 4)
			{
				//First page
				if(vo.index == firstPageIndex)
					return [vo.index];
				//lastpage
				if(vo.index - 3 == firstPageIndex)
					return [vo.index];
				//inside page 1
				if(vo.index - firstPageIndex == 1)
					return [vo.index, vo.index+1];
				//inside page 2
				if(vo.index - firstPageIndex == 2)
					return [vo.index-1, vo.index];
				
			}

			return pagesToDraw;
		}
		// get CANVAS pages to draw
		// we display always all pages for canvas
		private function getCanvasPagesToDraw(vo:PageVo):Array
		{
			var pagesToDraw:Array = [];
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				pagesToDraw.push(i);
			}
			
			return pagesToDraw;
		}
		
		
		
		/**
		 * Clean things when a frame is deleted
		 */
		private function frameDeleted(frame:Frame):void
		{
			transformTool.unstick();
		}
		
		/**
		 * when clicking outside edition area
		 */
		private function outClick(e:MouseEvent):void
		{
			//Debug.log("EditionArea.outClick");
			if( backgroundClickEnabled ) transformTool.unstick();
		}
		
		
		/**
		 * Adds listeners to a frame
		 */
		private function registerFrame(frame:Frame):void
		{
			frame.SELECT.add(frameSelected);
			frame.DESELECT.add(frameDeSelected);	
		}
		
		/**
		 * update page toolbar
		 */
		private function updatePageToolbar():void
		{
			if(!Infos.project)
			{
				pageToolbar.visible = false;
				return;
			}
			
			var fieldString : String = getPageIndexFieldString();//"";		
			
			pageIndexField.text = fieldString;
			pageToolbar.draw();
			
			previousPageButton.enabled = (currentPagesVect[0].pageVo.index != 0)? true : false;
			nextPageButton.enabled = (currentPagesVect[currentPagesVect.length-1].pageVo.index != Infos.project.pageList.length-1)? true : false;
			firstPageButton.enabled = (currentPagesVect[0].pageVo.index > 0)? true : false;
			lastPageButton.enabled = (currentPagesVect[currentPagesVect.length-1].pageVo.index < Infos.project.pageList.length-1)? true : false;
			
		}
		
		/**
		 * Return the string between the next and previous page button
		 */
		private function getPageIndexFieldString():String
		{
			var pageString : String = ResourcesManager.getString("common.page");
			var fieldString : String = "";
			var pageVo : PageVo 
			switch(Infos.project.classname)
			{
				case ProductsCatalogue.CLASS_CALENDARS:
					for (i = 0; i < currentPagesVect.length; i++) 
					{
						pageVo = currentPagesVect[i].pageVo;
						
						if(pageVo.index == 0 && !pageVo.isCalendar) fieldString += ResourcesManager.getString("common.cover");
						else if (pageVo.index == 0 && pageVo.isCalendar) fieldString += CalendarManager.instance.getMonthWithOffSetYear(pageVo.monthIndex,false);
						else  
						{
							if (pageVo.isCalendar) fieldString += CalendarManager.instance.getMonthWithOffSetYear(pageVo.monthIndex,false);
						}
					}
					return fieldString;
					break;
				
				
				case ProductsCatalogue.CLASS_CARDS:
					
					var pagePerDraw:int =  CardsManager.instance.getNumCardsPagePerDraw();
					
					for (var i:int = 0; i < currentPagesVect.length; i++) 
					{
						pageVo = currentPagesVect[i].pageVo;
						var rest:int = pageVo.index%pagePerDraw;
						var firstPageIndex:int = pageVo.index - rest;
						var modulo:int =  i % pagePerDraw;
						
						if(pagePerDraw == 2)
						{
							if(i>0) fieldString += " - ";
							if( i<1 || modulo == 0) fieldString += ResourcesManager.getString("common.front");
							else if(modulo > 0 && modulo<pagePerDraw-1) fieldString += ResourcesManager.getString("common.inside");
							else if(modulo == pagePerDraw-1) fieldString += ResourcesManager.getString("common.cover.back");
						}
						
						else if(pagePerDraw == 4)
						{
							if(i>0) fieldString += " - ";
							//First page
							if(pageVo.index == firstPageIndex)
								fieldString += ResourcesManager.getString("common.front");
							//lastpage
							if(pageVo.index - 3 == firstPageIndex)
								fieldString += ResourcesManager.getString("common.cover.back");
							//inside page 1
							if(pageVo.index - firstPageIndex == 1)
								fieldString += ResourcesManager.getString("common.inside");
							//inside page 2
							if(pageVo.index - firstPageIndex == 2)
								fieldString += ResourcesManager.getString("common.inside");
							
						}
					}
					return fieldString;
					break;
				
				
				default:
					for (i = 0; i < currentPagesVect.length; i++) 
					{
						pageVo = currentPagesVect[i].pageVo;
						
						if(pageVo.index == 0) 
						{
							if(pageVo.coverType == ProductsCatalogue.COVER_LEATHER)
							fieldString += ResourcesManager.getString("common.front");
							else
								fieldString += ResourcesManager.getString("common.cover.back") + " - " +ResourcesManager.getString("common.front");
						}
						else  
						{
							if(pageVo.index == 1) fieldString += ResourcesManager.getString("common.front") + " - ";
							if(i>0) fieldString += " - ";
							fieldString += pageString + " "+ pageVo.index;
							if(pageVo.index == Infos.project.pageList.length-1) fieldString += " - " + ResourcesManager.getString("common.cover.back");
						}
					}
					return fieldString;
			}
			return "";
		}
			
		
		
		/**
		 * When item is dropped 
		 */
		private function onItemDropped( item:DraggableItem, target:DropTargetArea ):void
		{		
			var page : PageVo;
			var pageArea : PageArea;
			
			// PHOTO DROP
			if(item is PhotoItem)
			{
				// offline photo item need to be first saved on project folder
				// if photovo is still a temp photo vo (not yet saved in project folder)
				CONFIG::offline
				{
					var photoVo : PhotoVo = (item as PhotoItemOffline).photoVo;
					if(!photoVo) // do nothing but warn
					{
						Debug.warn("EditionArea.onItemDropped > no photoVo on photoItem ?");
						return;
					}
					if(photoVo.temp)
					{
						// ON IMAGE SAVE SUCCESS
						var onSaveImageSuccess:Function = function():void
						{
							onItemDropped(item, target); // relaunch this function as photo is now imported to folder
						}
						// ON IMAGE ERROR
						var onSaveImageError:Function = function(e:ErrorEvent):void
						{
							Debug.warn("EditionArea.onItemDropped > error while saving image : "+e.toString());
						}
						// SAVE IMAGE LOCALY
						OfflineProjectManager.instance.SaveImage( photoVo, (item as PhotoItemOffline).thumbBitmapData, onSaveImageSuccess, onSaveImageError );
						
						// do not continue until image is saved
						return;
					}
				}
				
				
				
				//Check if the target is a Page
				/* KEEP : add a new photo item should only be done with double click now
				if(target is PageArea)
				{
					//Add frame to the pageArea
					PageArea(target).createFrameFromPhotoVo((item as PhotoItem).thumbVo as PhotoVo);
					frameCreatedActions();
					/*UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
					
					// update used/not used items
					ProjectManager.instance.remapPhotoVoList();* /
				}
				else 
				*/
				
				if(target is FrameBackground)
				{
					// if we are in canvas and multiple frame, inject photo on multiple frames
					if( Infos.isCanvas && CanvasManager.instance.isMultiple )
					{
						// if not yet created
						if( CanvasManager.instance.isMultipleFirstAdd )
						{
							CanvasManager.instance.updateMultipleFramePhoto( (item as PhotoItem).thumbVo as PhotoVo );
							// history
							UserActionManager.instance.addAction(UserActionManager.TYPE_ALL_BACKGROUND_CHANGED);
							// update used/not used items
							ProjectManager.instance.remapPhotoVoList();
						} 
						else
						{
							// open alert popup
							PopupAbstract.Alert(ResourcesManager.getString("canvas.popup.multipleFramePhotoDrop.title"),ResourcesManager.getString("canvas.popup.multipleFramePhotoDrop.description"),false,null, null,true, ["canvas.popup.btn.replaceall","canvas.popup.btn.replacesimple","canvas.popup.btn.add"], [
							function(e:MouseEvent):void{ // case update all
								CanvasManager.instance.updateMultipleFramePhoto( (item as PhotoItem).thumbVo as PhotoVo );
								// history
								UserActionManager.instance.addAction(UserActionManager.TYPE_ALL_BACKGROUND_CHANGED);
								// update used/not used items
								ProjectManager.instance.remapPhotoVoList();
							}, 
							function(e:MouseEvent):void{ // case update single
								FrameBackground(target).update((item as PhotoItem).thumbVo as PhotoVo);
								FrameBackground(target).frameVo.isMultiple = false;
								page = PagesManager.instance.getPageVoByFrameVo(FramePhoto(target).frameVo);
								// notify history
								frameCreatedActions();
							}, 
							function(e:MouseEvent):void{ // case add
								page = PagesManager.instance.getPageVoByFrameVo(FramePhoto(target).frameVo);
								PagesManager.instance.addPhotoFramePage( (item as PhotoItem).thumbVo as PhotoVo, page );
							}
							]);
						}
					}
					
					//Update Frame background with photoVo
					else {
						
						if(item is PhotoItem)
						FrameBackground(target).update((item as PhotoItem).thumbVo as PhotoVo);
						/*else if (item is BackgroundCustomItem)
						FrameBackground(target).update((item as BackgroundCustomItem).thumbVo as PhotoVo);*/
						
						page = PagesManager.instance.getPageVoByFrameVo(FramePhoto(target).frameVo);
						PageNavigator.RedrawPage(page);
						PagesManager.BACKGROUND_UPDATED.dispatch();
						// notify history
						frameCreatedActions();
					}
					
				}
				
				
				
				// drop photo item on frame overlayer
				else if(target is FrameOverlayer)
				{
					//Update Frame with new photoVo
					var linkedFrame : FramePhoto = FrameOverlayer(target).linkedFrame;
					linkedFrame.update((item as PhotoItem).thumbVo as PhotoVo); // update linked frame with new photo
					FrameOverlayer(target).update(null); // re adapt frame to fit overlayer content zone
					
					// update page
					page  = PagesManager.instance.getPageVoByFrameVo(FramePhoto(target).frameVo);
					PageNavigator.RedrawPage(page);
					frameCreatedActions();
				}
				
				else if(target is FramePhoto)
				{
					// remove possible popart effect
					FramePhoto(target).frameVo.isPopart = false;
					
					//Update Frame with new photoVo
					FramePhoto(target).update((item as PhotoItem).thumbVo as PhotoVo);
					page = PagesManager.instance.getPageVoByFrameVo(FramePhoto(target).frameVo);
					PageNavigator.RedrawPage(page);
					// actions for new frame created
					frameCreatedActions();
				}
				else if(target is FrameCalendar)
				{
					if(FrameCalendar(target).frameVo.editable && FrameCalendar(target).frameVo.dateAction == CalendarManager.DATEACTION_DAYDATE) 
					{
						//Add FramePhot to the pageArea
						PageArea(target.parent.parent).addFramePhotoToCalendarFrame((target as FrameCalendar),(item as PhotoItem).thumbVo as PhotoVo);
						// actions for new frame created
						frameCreatedActions();
					}
					else
					Debug.warn("You can not drop a photoItem onto a calendar frame that is not editable")
				}
				else if (target is PageNavigatorItem)
				{
					var navigatorItem : PageNavigatorItem =  target as PageNavigatorItem;
					var photoVo : PhotoVo = (item as PhotoItem).photoVo;
					
					// do we have an existing frame as target
					if( navigatorItem.lastFrameDropTarget ){
						FrameVo.injectPhotoVoInFrameVo( navigatorItem.lastFrameDropTarget.frameVo, photoVo );
					}
					// otherwise create a new one
					else navigatorItem.pageVo.createNewFrameFromPhotoVo( photoVo );
					
					// update current page if needed
					pageVoUpdateHandler( navigatorItem.pageVo );
					
					// actions for new frame created
					frameCreatedActions();
					
					// update navigaotr
					PageNavigator.RedrawPage( navigatorItem.pageVo );
				}
				
				
			}
			
			// LAYOUT DROP
			else if(item is LayoutItem)
			{
				// first we check if the current page can receive layout update
				
				
				//Check if the target is a Page
				if( target is FrameBackground )// PageArea)
				{
					pageArea = EditionArea.getPageAreaForFrameVo( (target as FrameBackground).frameVo );
					//the page is not a calendar
					if( ! pageArea.pageVo.layoutVo.isCalendar )
					{
						//replace layoutvo of the page
						pageArea.pageVo.updateLayout(LayoutManager.instance.getLayoutById((item as LayoutItem).layoutId,pageArea.pageVo.isCover,(item as LayoutItem).isCustomLayout));
						UserActionManager.instance.addAction(UserActionManager.TYPE_PAGE_LAYOUT_UPDATE);
					}
					//the page is a calendar
					if( pageArea.pageVo.layoutVo.isCalendar && (item as LayoutItem).isCalendar)
					{
						//replace layoutvo of the page
						pageArea.pageVo.updateLayout(LayoutManager.instance.getLayoutById((item as LayoutItem).layoutId,pageArea.pageVo.isCover));
						UserActionManager.instance.addAction(UserActionManager.TYPE_PAGE_LAYOUT_UPDATE);
					}
					
					//redraw page
					PageNavigator.RedrawPage(pageArea.pageVo);
				}
				else if(target is Frame)
				{
					//replace layoutvo of the page containing the frame
					pageVo = PagesManager.instance.getPageVoByFrameVo(Frame(target).frameVo);
					if(!pageVo.layoutVo.isCalendar)
					{
						pageVo.updateLayout(LayoutManager.instance.getLayoutById((item as LayoutItem).layoutId));
						UserActionManager.instance.addAction(UserActionManager.TYPE_PAGE_LAYOUT_UPDATE);
					}
					PageNavigator.RedrawPage(pageVo);
				}	
			}
			
			// CLIPART DROP
			else if(item is ClipartItem)
			{
				//Check if the target is a Page
				if(target is FrameBackground )// PageArea)
				{
					pageArea = EditionArea.getPageAreaForFrameVo( (target as FrameBackground).frameVo );
					
					//Add Clipart to the pageArea
					pageArea.createFrameFromPhotoVo((item as ClipartItem).thumbVo as ClipartVo);
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
					PageNavigator.RedrawPage(pageArea.pageVo);
					
					CONFIG::offline
						{
							AssetItem(item).startDownloadAsset();
						}
				}
				
			}
			
			// BACKGROUND DROP
			else if(item is BackgroundItem)
			{
				if(target is Frame)
				{
					var pageVo:PageVo = PagesManager.instance.getPageVoByFrameVo(Frame(target).frameVo);
					PagesManager.instance.updatePageBackground( PagesManager.instance.getPageVoByFrameVo(Frame(target).frameVo), BackgroundItem(item).thumbVo as BackgroundVo );
					CONFIG::offline
						{
							AssetItem(item).startDownloadAsset();
						}
				}
				
			}
			
			// BACKGROUND CUSTOM DROP
			else if(item is BackgroundCustomItem)
			{
				if(target is Frame)
				{
					pageVo = PagesManager.instance.getPageVoByFrameVo(Frame(target).frameVo);
					PagesManager.instance.updateCurrentPageBackgroundWithCustom(BackgroundCustomItem(item).thumbVo as BackgroundCustomVo, pageVo);
				}
				
			}
			
			// OVERLAYER DROP
			else if(item is OverlayerItem)
			{
				var vo : OverlayerVo = (item as OverlayerItem).thumbVo as OverlayerVo;
				/*
				// add overlayer frame on the page (on mouse pos)
				if(target is PageArea)
				{
					//Add overlayer to the pageArea
					PageArea(target).createFrameFromOverlayerVo(vo);
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
				}
				
				// change overlayer of existing overlayer frame
				else
				*/
				if(target is FrameOverlayer)
				{
					FrameOverlayer(target).update(vo,true); // update overlayer image
					//FrameOverlayer(target).adaptLinkedFrameToOverlayer(); // readapt linked frame to new overlayer bounds
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
					PageNavigator.RedrawPageByFrame( FrameOverlayer(target).frameVo );
					CONFIG::offline
						{
							AssetItem(item).startDownloadAsset();
						}
				}
				
				// add overlayer frame on a photo frame (group them together)
				else if(target is FramePhoto)
				{
					if(! FramePhoto(target).frameVo.isEmpty){// we do not allow to drop overlayer on empty frame
						page = PagesManager.instance.getPageVoByFrameVo(FramePhoto(target).frameVo);
						pageArea = getCurrentPageAreaFromPageVo(page);
						pageArea.addOverlayerToFrame( target as FramePhoto, vo)
						UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
						PageNavigator.RedrawPage( page );
						CONFIG::offline
							{
								AssetItem(item).startDownloadAsset();
							}
					}
				}
			}
			
			
			// PAGE NAVIGATOR DROP
			else if(item is PageNavigatorItem)
			{
				if(target is PageNavigatorItem)
				{
					// simple page swap
					swapPage(PageNavigatorItem(item).pageVo.index, PageNavigatorItem(target).pageVo.index);
				}
				else
				{
					// simple page move
					pageNavigator.onPageGroupDropped( item as PageNavigatorItem );
				}
			}
			
			// GROUP PAGE NAVIGATOR DROP
			else if(item is PageNavigatorGroupCTA)
			{
				// double page move
				pageNavigator.onPageGroupDropped();
			}
			
			
			
			//Remove Highlight of the target
			//target.alpha = 1;//target.highLight(false);
		}
		
		/**
		 * List of things to do when a frame is created
		 * */
		private function frameCreatedActions():void
		{
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			// update used/not used items
			ProjectManager.instance.remapPhotoVoList();
		}
		
		
		
		/**
		 * swap two pages by index
		 */
		private function swapPage(fromIndex:int, toIndex:int):void
		{
			Debug.log("EditionArea.swapPage : " + fromIndex + " to " +toIndex );
			var from:PageVo = Infos.project.getPageVoByIndex(fromIndex);
			var to:PageVo = Infos.project.getPageVoByIndex(toIndex);
			to.index = fromIndex;
			from.index = toIndex;
			
			// notify 
			PageNavigator.PAGE_SWAPPED.dispatch();
			
			//check if we need to redraw the current displayed pages
			for (var i:int = 0; i < currentPagesVect.length; i++) 
			{
				if(currentPagesVect[i].pageVo == to)
					drawPageFromPageVo(to,true)
				else if(currentPagesVect[i].pageVo == from)
					drawPageFromPageVo(from,true)
			}
		}
		
		/**
		 *	Change content state
		 */
		private function setState( newState : String ):void
		{
			
			if(!Infos.project)
				return;
			
			Debug.log("EditionArea > setState : " + newState );
						
			
			// do nothing if current state = new state
			if(currentState == newState) return;
			currentState = newState;
			
			if(currentState == STATE_PREVIEW)
			{
				//hide grid
				hideGrid();
				
				// be sure state is ok
				if(stage.displayState != StageDisplayState.FULL_SCREEN) 
				{
					try{
						stage.displayState = StageDisplayState.FULL_SCREEN;
					}catch(e:Error)
					{
						//PopupAbstract.Alert(ResourcesManager.getString("popup.fullscreenNotAllowed.title"),ResourcesManager.getString("popup.fullscreenNotAllowed.description"),false);
					}
				}
				
				TweenMax.to(pageIndexField,0,{tint:Colors.GREY_LIGHT});
				
				//handle fake page after cover
				drawFlyLeaf();
				
				// be sure to remove possible transform tool
				if(transformTool) transformTool.unstick();
				
				// be sure to reset zoom to 1
				zoomSlider.value = 1;
				updateLayout();
				
				// create modal
				if(!fullScreenModal){
					fullScreenModal = new Sprite();
					fullScreenModal.graphics.beginFill(Colors.FULLSCREEN_COLOR,1);
					fullScreenModal.graphics.drawRect(0,0,10,10);
				}
				fullScreenModal.width = stage.stageWidth;
				fullScreenModal.height = stage.stageHeight;
				stage.addChild(fullScreenModal);
				
				pageContainerWrapper.opaqueBackground = Colors.FULLSCREEN_COLOR;
				pageContainer.opaqueBackground = Colors.FULLSCREEN_COLOR;
				
				if(!fullScreenContainer){
					fullScreenContainer = new Sprite();
				}
				stage.addChild(fullScreenContainer);
				
				// add view on top and desactive it
				fullScreenContainer.addChild(pageContainerWrapper);
				pageContainerWrapper.mouseEnabled = pageContainerWrapper.mouseChildren = false;
				
				fullScreenContainer.addChild(pageToolbar);
				
				// Close btn
				closeFullScreenBtn = (!closeFullScreenBtn)?ButtonUtils.makeButton(new library.close.btn(), closeFullScreenHandler):closeFullScreenBtn;
				closeFullScreenBtn.x = stage.stageWidth - 30;
				closeFullScreenBtn.y = 30;
				stage.addChild(closeFullScreenBtn);
				
				// center fullscreen container
				if(Infos.isCanvas){
					var edgeSize : Number = CanvasManager.instance.edgeSize;
					fullScreenContainer.x = (stage.stageWidth - pageContainerWrapper.scrollRect.width)*.5 + edgeSize;
					fullScreenContainer.y = (stage.stageHeight - pageContainerWrapper.scrollRect.height)*.4 + edgeSize;	
				}else{
					fullScreenContainer.x = (stage.stageWidth - pageContainerWrapper.scrollRect.width)*.5;
					fullScreenContainer.y = (stage.stageHeight - pageContainerWrapper.scrollRect.height)*.4;	
				}
				
				
				var firstPageVo:PageVo =  currentPages[0].pageVo;
				if(firstPageVo is PageCoverClassicVo)
					setFullScreenShadow(false); // reset shadow when scrollrect is removed in fullscreen
				else
					setFullScreenShadow(true); 	
				
				// update page areas
				updatePageFullscreenState();
				
				// Update page disposition for calendars
				handleCalendarPageDisplay();

			}
			else
			{
				//reset grid like it was before fullscreen
				PageArea.GRID_ALPHA = .2;
				drawGrid();
				
				//remove single fake page preview if needed
				drawFlyLeaf()
				
				// Update page disposition for calendars is needed, else just update layout
				if(!handleCalendarPageDisplay())
				updateLayout();
				
				//removefullscreenBtn
				if(closeFullScreenBtn && closeFullScreenBtn.parent)
					closeFullScreenBtn.parent.removeChild(closeFullScreenBtn);
				
				
				// remove modal
				if(fullScreenModal && fullScreenModal.parent) fullScreenModal.parent.removeChild(fullScreenModal);
				
				TweenMax.to(pageIndexField,0,{tint:null});
				
				// put back view at correct place
				addChildAt(pageContainerWrapper,0);
				pageContainerWrapper.mouseEnabled = pageContainerWrapper.mouseChildren = true;
				
				pageContainerWrapper.opaqueBackground = Colors.BACKGROUND_COLOR;
				pageContainer.opaqueBackground = Colors.BACKGROUND_COLOR;//0xe5e5e5;
				
				setFullScreenShadow(false);
				
				addChild(pageToolbar);
				
				// show empty frames
				updatePageFullscreenState();
				
				
			}
			
			//pageContainerWrapper.opaqueBackground = Colors.BACKGROUND_COLOR;
			//pageContainer.opaqueBackground = Colors.BACKGROUND_COLOR;//0xe5e5e5;
			
			highLightEditedPage();
		}
		
		private function closeFullScreenHandler(e:MouseEvent):void
		{
			stage.displayState = StageDisplayState.NORMAL;
			setState(STATE_NORMAL);
			if(closeFullScreenBtn.parent)
				closeFullScreenBtn.parent.removeChild(closeFullScreenBtn);
		}
		
		/**
		 * resize event while fullscreen mode
		 */
		private function setFullScreenShadow(flag:Boolean):void
		{
			if(!Infos.isCanvas) pageContainer.filters = (flag)?[new DropShadowFilter(15,90,0,0.20,10,10)]:[]; // for now we do not add the shadow on canvas project, we should add a special perspective effect for edge
		}
		
				
		private function updatePageFullscreenState( flag : Boolean = true):void
		{
			for (var i:int = 0; i < currentPagesVect.length; i++) 
			{
				currentPagesVect[i].updateFullScreenState();
			}
			
		}
		
		
		
		/**
		 * Show next grid
		 * */
		public function nextGrid():void
		{
			Debug.log("Edition Area > NextGrid")
			PageArea.CURRENT_GRID_INDEX++;
			if(PageArea.CURRENT_GRID_INDEX > PageArea.GRID_COLS_NUMBER.length-1)
				PageArea.CURRENT_GRID_INDEX = 0;
			drawGrid();
			
		}	
		
		/**
		 * Hide grid directly
		 * */
		public function hideGrid():void
		{
			Debug.log("Edition Area > Hide Grid")
			PageArea.GRID_ALPHA = 0;
			drawGrid();
		}
		
		private function drawGrid():void
		{			
			Debug.log("Edition Area > DrawGrid")
			for (var i:int = 0; i < currentPagesVect.length; i++) 
			{
				var page:PageArea = currentPagesVect[i];
				//Cover classic
				if(!page.pageVo.isCover)
					page.drawGrid();
				else if(page.pageVo.coverType != ProductsCatalogue.COVER_LEATHER)
					page.drawGrid();	
			}
		}
		
		/**
		 * Project update handler
		 * 
		 * Let's update the view
		 * 
		 * */
		private function projectUpdatedHandler():void
		{
			drawPageFromPageVo(PagesManager.instance.currentEditedPage,true);
			collapsePageNavigator();
			
			if(Infos.isCanvas){
				updateCanvasDetails();
			}
		}
		
		/**
		 * update canvas details
		 */
		private function updateCanvasDetails():void
		{
			var	detailString:String = ResourcesManager.getString("canvas.type."+Infos.project.canvasType);
			detailString += " - " + ResourcesManager.getString("canvas.orientation."+Infos.project.type) ;
			var w : Number = Math.round(MeasureManager.PFLToCM(Infos.project.width));
			var h : Number = Math.round(MeasureManager.PFLToCM(Infos.project.height));
			detailString += " - " + w + "cm X " + h + "cm";
			if(CanvasManager.instance.isMultiple) {
				var totalW : Number = CanvasManager.instance.multipleColsCount * w;
				var totalH : Number = CanvasManager.instance.multipleRowsCount * h;
				detailString += " - " +ResourcesManager.getString("canvas.detail.multipleTotalSize") + " : " + totalW + "cm X " + totalH + "cm";
			}
				
			canvasDetailLabel.text = detailString; 
		}
		
		private function nextPageHandler(e:MouseEvent = null):void
		{
			Debug.log("EditionArea > nextPageHandler" );
			var currentPageVo : PageVo = currentPagesVect[currentPagesVect.length-1].pageVo;
			var nextPageIndex : Number = currentPageVo.index +1;
			var newPageVo:PageVo = Infos.project.getPageVoByIndex(nextPageIndex);
			drawPageFromPageVo(newPageVo);
			
		}
		private function previousPageHandler(e:MouseEvent = null):void
		{
			Debug.log("EditionArea > previousPageHandler" );
			var currentPageVo : PageVo = currentPagesVect[0].pageVo;
			var nextPageIndex : Number = currentPageVo.index -1;
			var newPageVo:PageVo = Infos.project.getPageVoByIndex(nextPageIndex);
			drawPageFromPageVo(newPageVo);
		}
		
		private function lastPageHandler():void
		{
			Debug.log("EditionArea > lastPageHandler" );
			var newPageVo:PageVo = pageList[pageList.length-1];
			drawPageFromPageVo(newPageVo);
		}
		
		private function firstPageHandler():void
		{
			Debug.log("EditionArea > firstPageHandler" );
			var newPageVo:PageVo = Infos.project.getPageVoByIndex(0);
			drawPageFromPageVo(newPageVo);
		}
		
		
		/**
		 * Open a popup 
		 * Swapper popup = swap pages
		 */
		private function openPageSwapperPopup(e:MouseEvent):void
		{
			var popup:PopupPageSwapper = new PopupPageSwapper();
			popup.open();
			popup.SWAP.addOnce(swapPage);
		}
		
		
		
		/**
		* Removes And Kills the current pages displayed
		*/
		private function clearPages():void
		{
			if(!currentPagesVect)
				return;
			
			for (var i:int = 0; i < currentPagesVect.length; i++) 
			{
				var pageArea:PageArea = currentPagesVect[i];
				pageArea.destroy();
				if(pageArea.parent)
					pageArea.parent.removeChild(pageArea);
				//DragDropManager.instance.removeDropTarget(pageArea); // now we do not use page area as drop target, so we do not need to remove it
				pageArea = null;
			}
			
			currentPagesVect.splice(0,currentPagesVect.length);
			pageGradient.graphics.clear();
			GarbageUtil.callGC();
		}
		
			
		
		/**
		 * on frame selected, add transform tool to it
		 */
		private function frameSelected( frame : Frame ):void
		{
			if(!frame){
				Debug.warn("EditionArea > FrameSelected : no Frame to select !");	
				return;
			}
			else Debug.log("EditionArea > FrameSelected : "+ frame.toString());
			
			
			if( PhotoSwapManager.instance.swapActive && frame is FramePhoto){ // if we are in swap mode, select frame adds it to the swap frames
				PhotoSwapManager.instance.addFrame(frame as FramePhoto);	
			}
			else if(transformTool.frame != frame && frame.frameVo.editable) // normal stick to frame on select if the frame is not already selected
				transformTool.stickToFrame(frame);
			
			/*
			else if(frame is FrameCalendar && (frame as FrameCalendar).linkedFrame is FrameText) // force ouside editor opening on frame calendar with linked frame text
				transformTool.stickToFrame(frame);
			*/
			
			else if(frame.frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION || frame.frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) // force ouside editor opening for spine and editor
				transformTool.stickToFrame(frame);
			
			else if(transformTool.frame == frame && frame.frameVo.editable && frame.allowMove) // if frame allow move and is editable and is selected, start move on second select
				transformTool.startMove();
			
		}
		
		/**
		 *
		 */
		private function onZoomChange ( s:HSlider ):void
		{
			updateLayout();	
		}

		/**
		 * Zoom +
		 */
		private function onZoomPlusHandler(e:MouseEvent):void
		{
			if(!Infos.project)
				return;
			
			Debug.log("EditionArea > onZoomPlusHandler" );
			zoomSlider.value += .1;
			updateLayout();	
		}
		
		/**
		 * Zoom -
		 */
		private function onZoomMinusHandler(e:MouseEvent):void
		{
			if(!Infos.project)
				return;
			
			Debug.log("EditionArea > onZoomMinusHandler" );
			zoomSlider.value -= .1;
			updateLayout();	
		}
		
		private function frameDeSelected( frame : Frame ):void
		{		
			if(transformTool)
			{
				if(transformTool.frame == frame) Debug.log("EditionArea.frameDeSelected" );
				transformTool.unstick();
			}
		}
	}
	
}