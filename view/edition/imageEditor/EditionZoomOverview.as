/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.imageEditor
{
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.CursorManager;
	
	import view.edition.EditionArea;

	public class EditionZoomOverview extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// signals
		public const START_DRAG : Signal = new Signal();
		
		public var WIDTH : Number = 170;
		
		// ui
		private var bg : Shape;
		private var visibleRect : Sprite;
		private var editionData:BitmapData;
		private var editionBitmap : Bitmap;
		
		// props
		private var isDragging : Boolean = false;
		private var pageContainer : Sprite;
		private var miniScale : Number;
		private var centerPos:Point = new Point();
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function EditionZoomOverview() 
		{ 
			//
			bg = new Shape();
			bg.graphics.beginFill(0xffffff, 1);
			bg.graphics.drawRect(0,0,10,10);
			addChild(bg);
			
			//
			editionBitmap = new Bitmap();
			addChild(editionBitmap);
			
			visibleRect = new Sprite();
			addChild(visibleRect);
			
			mouseChildren = false;
			addEventListener(MouseEvent.MOUSE_DOWN, onDrag);
			addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void{
				CursorManager.instance.showCursorAs(CursorManager.CURSOR_HAND);
			});
			addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent):void{
				if(!isDragging) CursorManager.instance.reset();
			});
			
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		public function destroy():void
		{
			if(parent)
				parent.removeChild(this);
			
			removeEventListener(MouseEvent.MOUSE_DOWN, onDrag);
			removeChildren();
		}
		
		/**
		 * page container is the content to draw
		 * fullwidth is the content width (with no scale)
		 * fullheight is the content height (with no scale)
		 * visible area is the scroll rect of container wrapper
		 */
		public function update(pageContainer : Sprite, fullWidth:Number, fullHeight:Number, visibleArea:Rectangle ):void
		{
			if(isDragging) return;
			
			this.pageContainer = pageContainer;
			
			alpha = 1;
			visible = true ;
			bg.width = 0;
			bg.height = 0;
			bg.x = 0;
			bg.y = 0;
			
			miniScale = WIDTH/fullWidth;
			if(editionData) editionData.dispose();
			editionData = new BitmapData(fullWidth*miniScale, fullHeight*miniScale, false, 0xff0000);
			var m : Matrix = new Matrix();
			m.scale(miniScale, miniScale);
			editionData.draw(pageContainer, m);
			
			editionBitmap.bitmapData = editionData;
			
			// visible rect
			visibleRect.graphics.clear();
			visibleRect.graphics.beginFill(Colors.YELLOW, .2);
			visibleRect.graphics.lineStyle(2, Colors.YELLOW,1);
			
			// use scroll rect
			//visibleRect.graphics.drawRect(visibleArea.x*miniScale/pageContainer.scaleX,visibleArea.y*miniScale/pageContainer.scaleX,visibleArea.width*miniScale/pageContainer.scaleX,visibleArea.height*miniScale/pageContainer.scaleY);
			
			// use pageContainer width
			var tempRect : Rectangle = new Rectangle(-pageContainer.x*miniScale/pageContainer.scaleX,-pageContainer.y*miniScale/pageContainer.scaleX,visibleArea.width*miniScale/pageContainer.scaleX,visibleArea.height*miniScale/pageContainer.scaleY);
			// transform temprect in coordinates
			tempRect = new Rectangle(tempRect.x, tempRect.y, tempRect.x + tempRect.width, tempRect.y + tempRect.height);
			if(tempRect.x <0) tempRect.x = 0;
			if(tempRect.y <0) tempRect.y = 0;
			if(tempRect.width > editionBitmap.width) tempRect.width = editionBitmap.width;
			if(tempRect.height > editionBitmap.height) tempRect.height = editionBitmap.height;
			// retransform to normal rect
			//tempRect = new Rectangle(tempRect.x, tempRect.y, tempRect.width-tempRect.x, tempRect.height-tempRect.y);
			visibleRect.graphics.drawRect(0, 0, tempRect.width-tempRect.x, tempRect.height-tempRect.y);
			visibleRect.x = tempRect.x;
			visibleRect.y = tempRect.y;
			
			bg.width = width+2;
			bg.height = height+2;
			bg.x = -1;
			bg.y = -1;
			
			// store pageContainer center position
			centerPos = new Point(pageContainer.x + pageContainer.width*.5, pageContainer.y + pageContainer.height*.5);
			
			show();
			
		}
		
		/**
		 *
		 */
		public function show():void
		{
			TweenMax.killDelayedCallsTo(autoHide);
			TweenMax.killTweensOf(this);
			TweenMax.delayedCall(2, autoHide);
			TweenMax.to(this, .5, {autoAlpha : 1});
		}
		
		/**
		 *
		 */
		public function hide():void
		{
			TweenMax.killDelayedCallsTo(autoHide);
			TweenMax.killTweensOf(this);
			this.visible = false;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function onDrag(e:MouseEvent):void
		{
			START_DRAG.dispatch();
			TweenMax.killDelayedCallsTo(autoHide);
			CursorManager.instance.showCursorAs(CursorManager.CURSOR_HAND);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onDrag);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMoveUpdate);
			stage.mouseChildren = false;
			stage.addEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			visibleRect.startDrag(false, new Rectangle(-10, -10, editionData.width-visibleRect.width + 20, editionData.height-visibleRect.height + 20));
			visibleRect.buttonMode = true;
			isDragging = true;
		}
		
		/**
		 *
		 */
		private function onStopDrag(e:MouseEvent):void
		{
			CursorManager.instance.reset();
			visibleRect.stopDrag();
			visibleRect.buttonMode = false;
			isDragging = false;
			stage.mouseChildren = true;
			stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMoveUpdate);
			TweenMax.killDelayedCallsTo(autoHide);
			TweenMax.delayedCall(2, autoHide);
		}
		
		/**
		 * when dragging, update pagecontainer position
		 */
		private function onMoveUpdate(e:MouseEvent):void
		{
			pageContainer.x = centerPos.x-(visibleRect.x+visibleRect.width*.5)/miniScale*pageContainer.scaleX;
			pageContainer.y = centerPos.y-(visibleRect.y+visibleRect.height*.5)/miniScale*pageContainer.scaleY
			/*
			pageContainer.x = Math.round( editionWidth - pageContainerWidth) * .5;// - 20 why is this "- 20" was there??
			pageContainer.y = Math.round( editionHeight - pageContainerHeight)* .5 + editionToolbar.height - 20;
			*/
		}
		
		/**
		 *
		 */
		private function autoHide():void
		{
			visible = false;
			if(parent) parent.removeChild(this);
		}
		
	}
}