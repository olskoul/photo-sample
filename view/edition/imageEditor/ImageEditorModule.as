﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.imageEditor
{
	import com.bit101.components.HBox;
	import com.bit101.components.HSlider;
	import com.bit101.components.Label;
	import com.bit101.components.ProgressBar;
	import com.bit101.components.PushButton;
	import com.bit101.components.Slider;
	import com.bit101.components.Text;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	import com.leeburrows.encoders.AsyncJPGEncoder;
	import com.leeburrows.encoders.supportClasses.AsyncImageEncoderEvent;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.JPEGEncoderOptions;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoaderDataFormat;
	import flash.sampler.NewObjectSample;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	import mx.effects.Tween;
	
	import be.antho.bitmap.JPEGEncoder;
	import be.antho.data.ResourcesManager;
	import be.antho.utils.DataLoadingManager;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import comp.button.SimpleThinButton;
	
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	
	import manager.PagesManager;
	import manager.SessionManager;
	import manager.UserActionManager;
	
	import mcs.dataLoadingClip;
	import mcs.defaultPanel;
	
	import offline.data.OfflineProjectManager;
	import offline.manager.LocalStorageManager;
	
	import org.osflash.signals.Signal;
	
	import ru.inspirit.net.MultipartURLLoader;
	
	import utils.BitmapUtils;
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.FramePhoto;
	import view.edition.pageNavigator.PageNavigator;
	import view.popup.PopupAbstract;
	import view.popup.PopupSimpleInput;
	
	/**
	 * The photoManager view is the mondule to manage project albums and photos
	 * It also contains the photo uploader
	 */
	public class ImageEditorModule extends PopupAbstract
	{
		// constants
		public static const EDITION_COMPLETE:Signal = new Signal(FramePhoto, BitmapData, String); // new image bitmapdata, new image name
		public static const EDITION_CANCEL:Signal = new Signal();
		
		public static const IMAGE_DIMENSION_LIMIT:int = 3100;
		
		// view
		private var view : mcs.defaultPanel;
		private var bg : MovieClip;
		private var title : TextField;
		private var titleBkg : Sprite;
		private var closeButton : MovieClip;
		private var boxL : MovieClip;
		private var boxR : MovieClip;
		
		private var leftArea : VBox;
		private var rightArea : Sprite;
		
		// sliders
		private var brightnessLabel : Label;
		private var brightnessSlider : HSlider;
		private var contrastLabel : Label;
		private var contrastSlider : HSlider;
		private var saturationLabel : Label;
		private var saturationSlider : HSlider;
		private var hueLabel : Label;
		private var hueSlider : HSlider;
		private var sepiaLabel : Label;
		private var sepiaAmountSlider : HSlider;
		
		// rotation
		private var insideRotationLabel : Label;
		private var rotatePlus : SimpleThinButton;
		private var rotateMinus : SimpleThinButton;
		
		// buttons
		private var resetBtn : SimpleThinButton;
		private var OkBtn : SimpleThinButton;
		private var cancelBtn : SimpleThinButton;
		private var blackWhiteBtn : SimpleThinButton;
		private var sepiaBtn : SimpleThinButton;
		private var mirrorBtn : SimpleThinButton;
		private var mirrorVBtn : SimpleThinButton;
		private var rotationPlusBtn : PushButton;
		private var rotationLessBtn : PushButton;
		private var redEyeBtn : PushButton;
		private var _sepia:Boolean = false;
		
		// image
		private var imageLoader : BulkLoader;
		private var imageContainer : Sprite;
		private var image : Bitmap;
		private var imageData : BitmapData;
		private var modifiedImageData : BitmapData;
		private var loadingBar : ProgressBar;
		
		// datas
		private var currentFrame : FramePhoto;
		private var photoVo : PhotoVo;
		private var imageColor : ColorTransform;
		private var imageUploader : MultipartURLLoader;
		private var newImageName : String;
		private var asyncEncoder : AsyncJPGEncoder
		
		private const WIDTH : Number = 800;
		private const HEIGHT : Number = 550;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		public function ImageEditorModule(sf : SingletonEnforcer) 
		{
			if(!sf) throw new Error("ImageEditorModule is a singleton");
			
			// Construct view
			construct();	
		}
		
		////////////////////////////////////////////////////////////////
		// Instance
		////////////////////////////////////////////////////////////////
		
		private static var _instance : ImageEditorModule;

		
		public function set sepia(value:Boolean):void
		{
			_sepia = value;
			sepiaBtn.selected = value;
			sepiaAmountSlider.enabled = value;
		}

		public static function get instance():ImageEditorModule
		{
			if(!_instance) _instance = new ImageEditorModule(new SingletonEnforcer());
			return _instance;
		}
		
		////////////////////////////////////////////////////////////////
		// GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		protected override function get openY(): Number 
		{
			var posY : Number = Math.round(stageRef.stageHeight*.5 - bg.height*.5);
			return posY;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHOD
		////////////////////////////////////////////////////////////////
		
		/**
		 * Open photo editor and pass it the photovo we want to edit
		 */
		public function init( stageRef : Stage ):void
		{	
			stageRef = stageRef
		}
		
		/**
		 * Open photo editor and pass it the photovo we want to edit
		 */
		public function openAndEditPhoto( _photoVo : PhotoVo ):void
		{	
			// check photo vo
			if(!_photoVo){
				Debug.warn("ImageEditorModule > No photovo given to edit");
				return;
			}
			
			// log edition start
			Debug.log("ImageEditorModule > Start edition of photoVo with id : "+ _photoVo.id);
			photoVo = _photoVo;
			super.open();
			
			// 
			
			// load photo
			loadFullSizePhoto();
			
			// reset sliders
			resetImage();
		}
		
		/**
		 * Open photo editor and pass it the photovo we want to edit
		 */
		public function openAndEditFromFramePhoto( framePhoto : FramePhoto ):void
		{	
			// check photo vo
			if(!framePhoto){
				Debug.warn("ImageEditorModule > No frame photo given to edit");
				return;
			}
			
			currentFrame = framePhoto;
			openAndEditPhoto( framePhoto.frameVo.photoVo );
		}
		
			
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public override function destroy():void
		{		
			// dispose bitmaps
			if(imageData) {
				imageData.dispose();
				imageData = null;
			}
			if(modifiedImageData){
				modifiedImageData.dispose();
				modifiedImageData = null;
			}
			
			// be sure to remove image list update
			SessionManager.IMAGE_LIST_UPDATE.remove(photoListUpdated)
			
			
			// dispose encoder
			disposeEncoder();
			
			// dispose loader
			disposeLoader();
			
			// dispose uploader
			disposeUploader()
			
			currentFrame = null;
			
			super.destroy();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * construct ui
		 */
		private function construct():void
		{
			// create background
			view = new defaultPanel();
			addChild(view);
			//ref
			title = view.title;
			titleBkg = view.titleBkg;
			bg = view.bg;
			closeButton = view.closeButton;
			boxL = view.boxL;
			boxR = view.boxR;
			
			// positionate
			bg.width = WIDTH;
			bg.height = HEIGHT;
			var margin : Number = 15;
			
			closeButton.x = bg.width - closeButton.width - margin;
			closeButton.y = 10;
			title.x = margin;
			title.width = closeButton.x - title.x - margin;
			titleBkg.width = WIDTH-2;
			
			boxL.y = boxR.y = titleBkg.y + titleBkg.height + margin;
			boxL.height = boxR.height = bg.height - boxL.y - 50;
			boxL.width = 120;
			
			boxR.width = bg.width - boxL.width - margin*3;
			boxL.x = margin;
			boxR.x = bg.width-boxR.width-margin;
			
			// title & close
			title.text = ResourcesManager.getString("imageeditor.panel.title");
			ButtonUtils.makeButton(closeButton, cancelHandler);
		
			// left area
			leftArea = new VBox();
			leftArea.y = boxL.y+10;
			leftArea.x = boxL.x+10;
			addChild(leftArea);
			
			// sliders
			brightnessLabel = new Label(leftArea, 0,0, ResourcesManager.getString("photomanager.panel.effect.brightness")); 
			brightnessSlider = new HSlider(leftArea, 0,0, onSliderChange);
			brightnessSlider.minimum = -2;
			brightnessSlider.maximum = 4;
			brightnessSlider.value = 1;
			
			contrastLabel = new Label(leftArea, 0,0, ResourcesManager.getString("photomanager.panel.effect.contrast"));
			contrastSlider = new HSlider(leftArea, 0,0, onSliderChange);
			contrastSlider.minimum = 0;
			contrastSlider.maximum = 2;
			contrastSlider.value = 1;
			
			saturationLabel = new Label(leftArea, 0,0, ResourcesManager.getString("photomanager.panel.effect.saturation"));
			saturationSlider = new HSlider(leftArea, 0,0, onSliderChange);
			saturationSlider.minimum = 0;
			saturationSlider.maximum = 2;
			saturationSlider.value = 1;
			
			hueLabel = new Label(leftArea, 0,0, ResourcesManager.getString("photomanager.panel.effect.hue"));
			hueSlider = new HSlider(leftArea, 0,0, onSliderChange);
			hueSlider.minimum = -180;
			hueSlider.maximum = 180;
			hueSlider.value = 1;
			
			
			
			
			
			
			
			insideRotationLabel = new Label(leftArea, 0,0, ResourcesManager.getString("photomanager.panel.effect.rotate"));
			var rotationBox : HBox = new HBox(leftArea);
			
			/*rotateMinus = new PushButton(rotationBox, 0,0 , "-", rotateMinusHandler);
			rotateMinus.setSize(45,20);*/
			rotateMinus = new SimpleThinButton(	"", // label
				Colors.BLUE, //over color
				rotateMinusHandler, //handler
				Colors.WHITE, //off color
				Colors.GREY_DARK, //off color label
				Colors.WHITE); // over color label
			rotateMinus.setLabel("-",false);
			rotateMinus.forcedWidth = 42;
			rotationBox.addChild(rotateMinus);
			
			/*rotatePlus = new PushButton(rotationBox, 0,0 , "+", rotatePlusHandler);
			rotatePlus.setSize(45,20);*/
			rotatePlus = new SimpleThinButton(	"upload.panel.uploadButton", // label
				Colors.BLUE, //over color
				rotatePlusHandler, //handler
				Colors.WHITE, //off color
				Colors.GREY_DARK, //off color label
				Colors.WHITE); // over color label
			rotatePlus.setLabel("+",false);
			rotatePlus.forcedWidth = 42;
			rotationBox.addChild(rotatePlus);
			
			// spacer
			var spacer : Label = new Label(leftArea, 0,0, " ");
			
			//Sepia
			sepiaBtn = new SimpleThinButton(	"photomanager.panel.effect.sepia", // label
				Colors.BLUE, //over color
				doSepia, //handler
				Colors.WHITE, //off color
				Colors.GREY_DARK, //off color label
				Colors.WHITE);// over color label
			//sepiaBtn.y = 50;
			sepiaBtn.forcedWidth = 100;
			leftArea.addChild(sepiaBtn);
			
			sepiaLabel = new Label(leftArea, 0,0, ResourcesManager.getString("photomanager.panel.effect.sepia.amount"));
			sepiaAmountSlider = new HSlider(leftArea, 0,0, onSliderChange);
			sepiaAmountSlider.minimum = 0;
			sepiaAmountSlider.maximum = 2;
			sepiaAmountSlider.value = 1;
			
			// spacer
			spacer = new Label(leftArea, 0,0, " ");
			
			//
			//mirrorBtn = new PushButton(leftArea, 0,50 , ResourcesManager.getString("photomanager.panel.effect.mirror"), doMirror);
			mirrorBtn = new SimpleThinButton(	"photomanager.panel.effect.mirror", // label
				Colors.BLUE, //over color
				doMirror, //handler
				Colors.WHITE, //off color
				Colors.GREY_DARK, //off color label
				Colors.WHITE);  // over color label
			mirrorBtn.y = 50;
			mirrorBtn.forcedWidth = 100;
			leftArea.addChild(mirrorBtn);
			
			
			mirrorVBtn = new SimpleThinButton(	"photomanager.panel.effect.mirrorV", // label
				Colors.BLUE, //over color
				doMirrorV, //handler
				Colors.WHITE, //off color
				Colors.GREY_DARK, //off color label
				Colors.WHITE);  // over color label
			//mirrorBtn.y = 50;
			mirrorVBtn.forcedWidth = 100;
			leftArea.addChild(mirrorVBtn);
			
			
			//blackWhiteBtn = new PushButton(leftArea, 0,50 , ResourcesManager.getString("photomanager.panel.effect.blackwhite"), doBlackAndWhite);
			blackWhiteBtn = new SimpleThinButton(	"photomanager.panel.effect.blackwhite", // label
				Colors.BLUE, //over color
				doBlackAndWhite, //handler
				Colors.WHITE, //off color
				Colors.GREY_DARK, //off color label
				Colors.WHITE);// over color label
			blackWhiteBtn.y = 50;
			blackWhiteBtn.forcedWidth = 100;
			leftArea.addChild(blackWhiteBtn);
			
			
			
			// right area
			rightArea = new Sprite();
			rightArea.y = boxR.y;
			rightArea.x = boxR.x;
			addChild(rightArea);
			
			imageContainer = new Sprite();
			imageContainer.x = boxR.width*.5;
			imageContainer.y = boxR.height*.5;
			image = new Bitmap();
			
			imageContainer.addChild(image);
			rightArea.addChild(imageContainer);
			
			// bottom buttons
			//OkBtn = new PushButton(this, width-120,height-40 , ResourcesManager.getString("common.apply"), applyImageModifications);
			OkBtn = new SimpleThinButton(	"common.apply", // label
				Colors.BLUE, //over color
				applyImageModifications, //handler
				Colors.GREY, //off color
				Colors.WHITE, //off color label
				Colors.WHITE);// over color label
			OkBtn.x = width-115;
			OkBtn.y = boxR.y + boxR.height+10;
			OkBtn.forcedWidth = 100;
			addChild(OkBtn);
			
			//cancelBtn = new PushButton(this, width-240,height-40 , ResourcesManager.getString("common.cancel"), cancelHandler);
			cancelBtn = new SimpleThinButton(	"common.cancel", // label
				Colors.BLUE, //over color
				cancelHandler, //handler
				Colors.GREY, //off color
				Colors.WHITE, //off color label
				Colors.WHITE);// over color label
			cancelBtn.x = OkBtn.x - OkBtn.width - 10;
			cancelBtn.y = OkBtn.y;
			cancelBtn.forcedWidth = 100;
			addChild(cancelBtn);
			
			//resetBtn = new PushButton(this, 20,height-40 , ResourcesManager.getString("photomanager.panel.btn.reset"), resetImage);
			resetBtn = new SimpleThinButton(	"photomanager.panel.btn.reset", // label
				Colors.BLUE, //over color
				resetImage, //handler
				Colors.GREY, //off color
				Colors.WHITE, //off color label
				Colors.WHITE);// over color label
			resetBtn.x = 15;
			resetBtn.y = OkBtn.y;
			resetBtn.forcedWidth = 100;
			addChild(resetBtn);
			
			// loading bar
			loadingBar = new ProgressBar(this);
			loadingBar.x = boxR.x +boxR.width*.5-loadingBar.width*.5;
			loadingBar.y = boxR.y +boxR.height*.5-loadingBar.height*.5;
			addChild(loadingBar);
			loadingBar.visible = false;
			
			/*
			imageArea = new ScrollPane(this, 180, 40);
			imageArea.autoHideScrollBar = true;
			insideUploadButton = new PushButton(this, 430, 245, "Upload photos", openImageUploader);
			insideUploadButton.visible = false;
			*/
			
		}
		
		
		/**
		 * ------------------------------------ LOAD IMAGE TO EDIT -------------------------------------
		 */
		
		private function loadFullSizePhoto():void
		{
			Debug.log("ImageEditorModule > load photo full size : "+ photoVo.highUrl);
			
			var photoUrl : String = photoVo.highUrl;
			
			/*
			CONFIG::online
			{
			
			}
			*/
				disposeLoader();
				imageLoader = new BulkLoader("imageEditorLoader");
				imageLoader.addEventListener(BulkProgressEvent.COMPLETE, onImageLoadSuccess);
				imageLoader.addEventListener(BulkLoader.ERROR, onImageLoadFail);
				imageLoader.addEventListener(BulkProgressEvent.PROGRESS, onImageLoadProgress);
				imageLoader.add(photoUrl, { id:"photo" ,type:"image"});
				imageLoader.start();
				loadingBar.visible = true;
				/*
			}
			
			CONFIG::offline
			{
				LocalStorageManager.instance.loadLocalImage( photoUrl, onLocalImageSuccess, onLocalImageError );
			}
				*/
		}
		private function onImageLoadFail(e:ErrorEvent):void
		{
			loadingBar.visible = false;
			Debug.warn("ImageEditorModule > Error while loading : "+photoVo.highUrl);
			abordEdition(" > load error : " + e.text);
		}
		private function onImageLoadProgress(e:BulkProgressEvent):void
		{	
			loadingBar.value = e.percentLoaded;
		}
		private function onImageLoadSuccess(e:Event):void
		{
			Debug.log("ImageEditorModule > load photo full size : completed");
			
			// recover
			imageData = imageLoader.getBitmapData("photo", true);
			if(photoVo.exifRot != 0) 
				imageData = BitmapUtils.transformBitmapData(imageData, photoVo.exifRot, 1, true);
			modifiedImageData = imageData.clone();
			
			showImage();
			disposeLoader();
			loadingBar.visible = false;
		}
		/*
		private function onLocalImageError(error:ErrorEvent):void
		{
			Debug.warn("ImageEditorModule.onLocalImageError : ");
			abordEdition(" > load error : " + error.toString());
		}
		private function onLocalImageSuccess( loadedData : BitmapData ):void
		{
			Debug.log("ImageEditorModule.onLocalImageSuccess");
			imageData = loadedData;
			modifiedImageData = imageData.clone();
			showImage();
		}
		*/
		
		
		/**
		 * ------------------------------------ SHOW IMAGE -------------------------------------
		 */
		
		private function showImage():void
		{
			image.bitmapData = modifiedImageData;
			image.visible = true;
			image.smoothing = true;
			// set image at correct size
			var maxWidth : Number = boxR.width;
			var maxHeight : Number = boxR.height;
			var canvasRatio : Number = maxWidth/maxHeight;
			var imageRatio : Number = modifiedImageData.width/modifiedImageData.height;
			
			if(imageRatio > canvasRatio) image.scaleX = image.scaleY = maxWidth / modifiedImageData.width;
			else image.scaleX = image.scaleY = maxHeight / modifiedImageData.height;
			
			positionateImage();
			
			// scrollrect for right area
			rightArea.scrollRect = new Rectangle((boxR.width-image.width)*.5,(boxR.height-image.height)*.5,image.width, image.height);
			rightArea.x = boxR.x + (boxR.width-image.width)*.5;
			rightArea.y = boxR.y + (boxR.height-image.height)*.5
		}
		
		
		/**
		 * ------------------------------------ IMAGE EDITION -------------------------------------
		 */
		
		/**
		 * reset image
		 */
		private function resetImage(e:Event = null):void
		{
			imageContainer.scaleX = imageContainer.scaleY = 1;
			imageContainer.rotation = 0;
			brightnessSlider.value = 1;
			contrastSlider.value = 1;
			saturationSlider.value = 1;
			hueSlider.value = 1;
			sepia = false;
			onSliderChange(null);
			if(image.scaleX<0) image.scaleX = -image.scaleX;
			if(image.scaleY<0) image.scaleY = -image.scaleY;
			
			
			positionateImage();
		}
		
		/**
		 * dispose current loader
		 */
		private function disposeLoader():void
		{
			if(!imageLoader) return;
			imageLoader.removeEventListener(BulkProgressEvent.COMPLETE, onImageLoadSuccess);
			imageLoader.removeEventListener(BulkLoader.ERROR, onImageLoadFail);
			imageLoader.removeEventListener(BulkProgressEvent.PROGRESS, onImageLoadProgress);
			imageLoader.clear(); 
			imageLoader = null;
		}
		
		/**
		 * dispose current loader
		 */
		private function onSliderChange(s : Slider = null):void
		{
			var sepiaColor:Number = 0xfcc268;
			var sepiaAmount:Number = (_sepia)?sepiaAmountSlider.value:0;
			
			TweenMax.killTweensOf(image);
			TweenMax.to(image, 0, {colorMatrixFilter:{colorize:sepiaColor, amount:sepiaAmount, saturation : saturationSlider.value,brightness : brightnessSlider.value ,contrast:contrastSlider.value ,hue:hueSlider.value }})
			/*TweenMax.killTweensOf(image);
			TweenMax.to(image, 0, {colorMatrixFilter:{ saturation : saturationSlider.value,brightness : brightnessSlider.value ,contrast:contrastSlider.value ,hue:hueSlider.value }})*/
		}
		
		/**
		 * do mirror image
		 */
		private function doMirror(e:MouseEvent = null):void
		{
			image.scaleX = -image.scaleX;
			
			positionateImage();
			
			/*
			var flipHorizontalMatrix:Matrix = new Matrix();
			flipHorizontalMatrix.scale(-1,1)
			flipHorizontalMatrix.translate(modifiedImageData.width,0)
			
			var tempBitmap : BitmapData = new BitmapData(modifiedImageData.width,modifiedImageData.height,false,0xFFCC00);
			tempBitmap.draw(image, flipHorizontalMatrix);
			modifiedImageData.dispose();
			modifiedImageData = tempBitmap;
			image.bitmapData = modifiedImageData;
			*/
		}
		
		/**
		 * do mirror image
		 */
		private function doMirrorV(e:MouseEvent = null):void
		{
			image.scaleY = -image.scaleY;
			
			positionateImage();
		}
		
		/**
		 * re positionate image
		 */
		private function positionateImage():void
		{
			if(image.scaleX > 0) image.x = -image.width*.5;
			else image.x = image.width*.5;
			
			if(image.scaleY > 0) image.y = -image.height *.5;
			else image.y = image.height *.5;
			
		}
		
		
		/**
		 * rotate image
		 */
		private function rotateMinusHandler(e:Event = null):void
		{
			imageContainer.rotation -=1;
			updateImageContainerZoom();
			
		}
		
		/**
		 * rotate image
		 */
		private function rotatePlusHandler(e:Event = null):void
		{
			imageContainer.rotation +=1;
			updateImageContainerZoom();
		}
			
		/**
		 * update image zoom to fit rotation 
		 */
		private function updateImageContainerZoom():void
		{
			var newBounds : Rectangle = imageContainer.getBounds(imageContainer.parent);
			var newScaleX : Number = newBounds.width / image.width / imageContainer.scaleX ;
			var newScaleY : Number = newBounds.height / image.height / imageContainer.scaleY;
			imageContainer.scaleX = imageContainer.scaleY = (newScaleX > newScaleY)? newScaleX : newScaleY;
		}
		
		
		
		/**
		 * do black and white transformation
		 */
		private function doBlackAndWhite(e:MouseEvent = null):void
		{
			Debug.log("ImageEditorModule > DoBlackAndWhite");
			saturationSlider.value = 0;
			sepia = false;
			onSliderChange();
		}
		
		/**
		 * do Sepia transformation
		 */
		private function doSepia(e:MouseEvent = null):void
		{
			Debug.log("ImageEditorModule > doSepia");
			
			sepia = !_sepia;
			sepiaBtn.selected = _sepia;
			
			brightnessSlider.value = 1;
			contrastSlider.value = 1;
			saturationSlider.value = 0;
			hueSlider.value = 1;
			
			onSliderChange();
			
		}
		
		/**
		 * ------------------------------------ APPLY AND CHOOSE NEW NAME -------------------------------------
		 */
		
		
		/**
		 * apply on bitmapdata
		 */
		private function applyImageModifications(e:MouseEvent= null):void
		{
			Debug.log("ImageEditorModule > Apply");
			chooseNewImageName(null);
		}
		
		/**
		 * create a new folder
		 */
		private function chooseNewImageName(e:Event):void
		{
			var newPhotoName:String = "";
			var addValue : String = "(1)";
			
			if(photoVo.name.indexOf(".jpg") != -1)
				newPhotoName = photoVo.name.split(".jpg")[0] + addValue ;
			else if (photoVo.name.indexOf(".JPG") != -1)
				newPhotoName = photoVo.name.split(".JPG")[0] + addValue ;
			else if (photoVo.name.indexOf(".JPEG") != -1)
				newPhotoName = photoVo.name.split(".JPEG")[0] + addValue ;
			else if (photoVo.name.indexOf(".jpeg") != -1)
				newPhotoName = photoVo.name.split(".jpeg")[0] + addValue ;
			else if (photoVo.name.indexOf(".png") != -1)
				newPhotoName = photoVo.name.split(".png")[0] + addValue ;
			else
				newPhotoName = newPhotoName + addValue;
				
			
			var newFolderPopup : PopupSimpleInput = new PopupSimpleInput(newPhotoName , "popup.imageeditor.new.image.title");
			newFolderPopup.open();
			newFolderPopup.OK.add(newNameSelected);
		}
		private function newNameSelected(p:PopupAbstract):void
		{
			DataLoadingManager.instance.showLoading();
			
			// prepare load
			newImageName = (p as PopupSimpleInput).fieldValue;
			
			Debug.log("ImageEditorModule > New name selected : "+ newImageName);
			startEncoding();
		}
		
		
		
		/**
		 * ------------------------------------ ABORD EDITION -------------------------------------
		 */
		
		
		/**
		 * general handler for edition error
		 * > load image error
		 * > upload image error
		 * > encoding image error
		 */
		private function abordEdition ( message : String = "default abord on image editor" ):void
		{
			DataLoadingManager.instance.hideLoading();
			Debug.warnAndSendMail("ImageEditorModule > abordEdition : "+message);
			
			// dispose
			disposeLoader();
			disposeEncoder();
			disposeUploader();
			
			// close
			close();
			
			// display error popup for user
			PopupAbstract.Alert(ResourcesManager.getString("popup.imageEditor.error.title"),ResourcesManager.getString("popup.imageEditor.error.desc"),false,null, null, true);
		}
		
		
		/**
		 * dispose image encoder
		 */
		private function disposeEncoder():void
		{
			if(asyncEncoder)
			{
				asyncEncoder.removeEventListener(AsyncImageEncoderEvent.PROGRESS, encodeProgressHandler);
				asyncEncoder.removeEventListener(AsyncImageEncoderEvent.COMPLETE, encodeCompleteHandler);
				asyncEncoder.dispose();
				asyncEncoder = null;
			}
		}
		
		/**
		 * dispose image uploader
		 */
		private function disposeUploader():void
		{
			try
			{
				if(imageUploader)
				{
					imageUploader.removeEventListener(Event.COMPLETE, imageEditionComplete);
					imageUploader.removeEventListener(ProgressEvent.PROGRESS, sendProgress);
					imageUploader.removeEventListener(IOErrorEvent.IO_ERROR, imageEditionError);
					imageUploader.clearFiles();
					imageUploader.close();
					imageUploader = null;
				}
			}
			catch(e:Error)
			{
				Debug.warnAndSendMail("ImageEditorModule > diposeUploader error : "+e.message);
			}
		}
		
		
		
		
		
		/**
		 * ------------------------------------ ENCODE -------------------------------------
		 */
		
		/**
		 * convert bitmap data to bytearray
		 */
		private function startEncoding():void
		{
			Debug.log("ImageEditorModule > Start image encoding");
			
			// dispose previous image data
			if(imageData) imageData.dispose(); 
			
			// check if there is modified image data
			if(!modifiedImageData)
			{
				abordEdition("ImageEditorModule > startEncoding > no modifiedImageData");
				return;
			}
			
			//Limit encoded image dimensions
			var encodingWidth:Number = modifiedImageData.width;
			var encodingHeight:Number = modifiedImageData.height;
			var ratio:Number = 1;
			
			if(encodingWidth > encodingHeight)
			{
				if(encodingWidth > IMAGE_DIMENSION_LIMIT)
				{
					ratio = IMAGE_DIMENSION_LIMIT / encodingWidth;
					encodingWidth = IMAGE_DIMENSION_LIMIT;
					encodingHeight = encodingHeight * ratio;
				}
			}
			else
			{
				if(encodingHeight > IMAGE_DIMENSION_LIMIT)
				{
					ratio = IMAGE_DIMENSION_LIMIT / encodingHeight;
					encodingHeight = IMAGE_DIMENSION_LIMIT;
					encodingWidth = encodingWidth * ratio;
				}
			}
			
			// copy modified image
			imageData = new BitmapData(encodingWidth,encodingHeight,false,0x000000);
			var m:Matrix = new Matrix();
			
			var scaleHack : Number = (image.scaleY < 0)? 1.01/(-image.scaleY)*ratio : 1.01/image.scaleY*ratio;
			m.scale(scaleHack, scaleHack) ; // 1.01 to avoid strange artifact on the right of the photo 
			
			imageData.draw(rightArea, m);
			
			//
			try
			{
				// online we use old encoder system ( new doesn't seem to work in flash player.. )
				CONFIG::online{
					asyncEncoder = new AsyncJPGEncoder(80);
					asyncEncoder.addEventListener(AsyncImageEncoderEvent.PROGRESS, encodeProgressHandler);
					asyncEncoder.addEventListener(AsyncImageEncoderEvent.COMPLETE, encodeCompleteHandler);
					//start encoding for 100 milliseconds per frame
					asyncEncoder.start(imageData, 100);
				}
				
				// OFFLINE SAVE	
				CONFIG::offline
				{
					// listeners
					var onOfflineSaveComplete:Function = function( newPhotoVo : PhotoVo ):void
					{
						// update frame
						if(currentFrame) currentFrame.update(newPhotoVo);
						// redraw navigator
						PageNavigator.RedrawPageByFrame(currentFrame.frameVo);
						// add action in history
						UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
						// close
						close();
						// hide loading
						DataLoadingManager.instance.hideLoading();
					}
					// save
					OfflineProjectManager.instance.SaveEditedImage(photoVo, newImageName, imageData, onOfflineSaveComplete, imageEditionError);
				}
				
			}
			catch(e:Error)
			{
				abordEdition("Image Editor encoding error : "+e.message);
			}
				
		}
		private function encodeProgressHandler(e:AsyncImageEncoderEvent):void
		{
			var pct : Number = Math.floor(e.percentComplete);
			DataLoadingManager.instance.updateText( ResourcesManager.getString("imageEdit.encoding.part1") + pct + ResourcesManager.getString("imageEdit.encoding.part2"));
			//Debug.log("asyncEncoder progress : " + Math.floor(e.percentComplete)+"% complete");
		}
		private function encodeCompleteHandler(e:AsyncImageEncoderEvent):void
		{
			Debug.log("ImageEditorModule > Send image to server (name ='"+newImageName+"')(category='"+photoVo.folderName+"')");
			
			if(asyncEncoder == null) {
				abordEdition("ImageEditorModule > encodeCompleteHandler > asyncEncoder is null");
				return;
			}
			
			if(asyncEncoder.encodedBytes == null) {
				abordEdition("ImageEditorModule > encodeCompleteHandler > asyncEncoder.encodedBytes is null");
				return;
			}
			
			// send image
			var imageBytes : ByteArray = asyncEncoder.encodedBytes;
			sendImage( imageBytes );
		}
		
		
		/**
		 * ------------------------------------ SEND IMAGE TO SERVER -------------------------------------
		 */
		
		/**
		 * send image to server OR locally for offline
		 */
		private function sendImage( imageBytes : ByteArray ):void
		{
			DataLoadingManager.instance.updateText(ResourcesManager.getString("imageEdit.encoding.send"));
			
			// dispose encoder
			disposeEncoder();
			
			// dispose uploading 
			disposeUploader();
			
			// start upload
			imageUploader = new MultipartURLLoader();
			imageUploader.dataFormat = URLLoaderDataFormat.TEXT;
			imageUploader.addEventListener(Event.COMPLETE, imageEditionComplete);
			imageUploader.addEventListener(ProgressEvent.PROGRESS, sendProgress);
			imageUploader.addEventListener(IOErrorEvent.IO_ERROR, imageEditionError);
			imageUploader.addVariable("cat", photoVo.folderName);
			imageUploader.addVariable("Filename", newImageName);
			imageUploader.addFile(imageBytes, newImageName, "uploadfile");
			imageUploader.load(Infos.config.serverNormalUrl + "/" + "flex/img_upload.php", false);
		}
		private function sendProgress(e:ProgressEvent):void
		{
			Debug.log("ImageEditorModule.sendProgress");
		}
		private function imageEditionError(e:* = null):void
		{
			Debug.warn("ImageEditorModule.ImageEditionError : "+e );
			abordEdition(e.text);
		}
		private function imageEditionComplete(e:Event = null):void
		{
			Debug.log("ImageEditorModule.ImageEditionComplete");
			SessionManager.IMAGE_LIST_UPDATE.addOnce(photoListUpdated)
			SessionManager.instance.updateImageList();
		}
		


		/**
		 * ------------------------------------ END -------------------------------------
		 */
		
		
		/**
		 * when photo is uploaded and photo list is updated, we can close module
		 */
		private function photoListUpdated():void
		{
			//SessionManager.IMAGE_LIST_UPDATE.remove(photoListUpdated)
			if(imageData) EDITION_COMPLETE.dispatch(currentFrame, imageData.clone(), newImageName);
		
			// close
			close();
			DataLoadingManager.instance.hideLoading();
			
		}
		
		
	}
	
}

internal class SingletonEnforcer{}