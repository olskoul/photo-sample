/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import be.antho.bitmap.snapshot.SnapShot;
	import be.antho.data.ResourcesManager;
	
	import data.FrameVo;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import flashx.textLayout.formats.VerticalAlign;
	
	import org.qrcode.QRCode;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;

	/**
	 */
	public class PageNavigatorFrameQR extends PageNavigatorFrame
	{
		
		// ui
		private var bitmap:Bitmap;
		private var content : Sprite;
		private var bitmapCache : Bitmap;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var qrObj:QRCode;
		
		public function PageNavigatorFrameQR( frameVo : FrameVo, scaleRatio:Number) 
		{ 
			super( frameVo , scaleRatio);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			//create content
			if(!content){
				content = new Sprite();
				/*textContainer = new Sprite();
				content.addChild(textContainer);*/
			}
			addChild(content);	
			
			if(frameVo && frameVo.QRLink != "") //(false) && 
			{
				if(qrObj && qrObj.bitmapData)
					qrObj.bitmapData.dispose();
				
				qrObj = new QRCode();
				qrObj.encode(frameVo.QRLink);
								
				bitmap = new Bitmap(qrObj.bitmapData);
				
				var drawRatio : Number = (frameVo.width/frameVo.zoom)/bitmap.width;
				var bmd : BitmapData = new BitmapData(frameVo.width/frameVo.zoom, frameVo.height/frameVo.zoom, true, 0x000000);
				var m:Matrix = new Matrix();
				m.scale(drawRatio, drawRatio);
				bmd.draw(bitmap, m, null, null, null, true);
				bitmap.bitmapData.dispose();
				bitmap = new Bitmap(bmd,"auto",true);
			}
			else
			{
				var background:Sprite = new Sprite();
				background.alpha = 1;
				background.graphics.clear();
				
				var tintColor :Number = Colors.RED;
				background.graphics.beginFill( tintColor,1 );
				background.graphics.drawRect(0,0,frameVo.width/frameVo.zoom,frameVo.height/frameVo.zoom); // it will be scaled later
				background.graphics.endFill();
				
				bitmap = new Bitmap(SnapShot.snap(background), "auto", true);
				
				background = null;
			}
			
			/*bitmap.width = frameVo.width/frameVo.zoom;
			bitmap.height = frameVo.height/frameVo.zoom;*/
			//bitmap.scaleX = bitmap.scaleY = frameVo.zoom;
			
			
		
			
			content.addChild(bitmap);
			
			//draw frame
			renderFrame();
		}
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			if(bitmap)
			{
				if(bitmap.parent)
				bitmap.parent.removeChild(bitmap);
				bitmap.bitmapData.dispose();
				bitmap = null;
			}
			if(content && contains(content))
			{
				removeChild(content);
				content = null;
			}
			
			if(bitmapCache && bitmapCache.bitmapData){
				bitmapCache.bitmapData.dispose();
				bitmapCache.bitmapData = null;
			}
			
			super.destroy();
		}
		

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		private function renderFrame():void
		{
			if(!frameVo.isEmpty)
			{
				bitmap.scaleX = bitmap.scaleY = frameVo.zoom * scaleRatio;
				bitmap.x = -(bitmap.width/2);
				bitmap.y = -(bitmap.height/2);
			}
			
			// apply position after center content
			x = frameVo.x * scaleRatio;
			y = frameVo.y * scaleRatio;
			rotation = frameVo.rotation;
			
			// draw content
			if(bitmapCache) Debug.warn("PageNavigatorFrameQR > renderFrame : Bitmapcache should not exist... re-check process");
			bitmapCache = new Bitmap();
			
			try{
				var bmd : BitmapData = new BitmapData(content.width, content.height,true, 0xff0000);
				var m:Matrix = new Matrix();
				m.translate(-bitmap.x, -bitmap.y);
				bmd.draw(content, m, null,null, null, true);
				bitmapCache.bitmapData = bmd;
				bitmapCache.x = bitmap.x;
				bitmapCache.y = bitmap.y;
				content.addChild(bitmapCache);
				content.removeChild(bitmap);
				bitmap.bitmapData.dispose();
				
				
			}
			catch (e:Error)
			{
				Debug.warn("PageNavigatorFrameQR > error when trying to generate bitmap");
				if(bmd) bmd.dispose();
				if(bitmapCache) bitmapCache = null;
			}
			
			
		}
	}
	
}