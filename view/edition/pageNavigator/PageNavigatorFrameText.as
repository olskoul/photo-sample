﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import be.antho.data.ResourcesManager;
	
	import data.FrameVo;
	import data.Infos;
	
	import flashx.textLayout.formats.VerticalAlign;
	
	import utils.Debug;
	import utils.TextFormats;

	/**
	 */
	public class PageNavigatorFrameText extends PageNavigatorFrame
	{
		
		// ui
		private var textfield : TextField;
		private var content : Sprite;
		private var bitmapCache : Bitmap;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PageNavigatorFrameText( frameVo : FrameVo, scaleRatio:Number) 
		{ 
			super( frameVo , scaleRatio);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			//create content
			if(!content){
				content = new Sprite();
				/*textContainer = new Sprite();
				content.addChild(textContainer);*/
			}
			addChild(content);	
			
			//create textfield
			textfield = new TextField();
			textfield.width = frameVo.width / frameVo.zoom;
			//textfield.height = frameVo.height / frameVo.zoom;
			textfield.scaleX = textfield.scaleY = frameVo.zoom;
			textfield.autoSize = "left";
			textfield.wordWrap = true;
			textfield.selectable = true;
			textfield.mouseWheelEnabled = false;
			textfield.type = TextFieldType.DYNAMIC;
			textfield.embedFonts = true;
			textfield.multiline = true;
			textfield.backgroundColor = 0xff00ff;
			textfield.background = false;
			textfield.htmlText = (!frameVo.isEmpty) ? frameVo.text : ResourcesManager.getString("edition.frame.text.watermark");
			
			if(frameVo.isEmpty){
				textfield.setTextFormat(TextFormats.DEFAULT_FRAME_TEXT(frameVo.fontSize));
			}
			
			/*
			frameVo.textFormat = (frameVo.textFormat != null)?frameVo.textFormat:TextFormats.DEFAULT_FRAME_TEXT(frameVo.fontSize);
			var fontValidated:String = TextFormats.validateFont(String(frameVo.textFormat.font));
			if(fontValidated != frameVo.textFormat.font && fontValidated != "null")
				frameVo.textFormat.font = fontValidated;
			textfield.setTextFormat(frameVo.textFormat);
			*/
			
			content.addChild(textfield);
			
			//draw frame
			renderFrame();
		}
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			if(textfield)
			{
				textfield.parent.removeChild(textfield);
				textfield = null;				
			}
			if(content && contains(content))
			{
				removeChild(content);
				content = null;
			}
			
			if(bitmapCache && bitmapCache.bitmapData){
				bitmapCache.bitmapData.dispose();
				bitmapCache.bitmapData = null;
			}
			
			super.destroy();
		}
		

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		private function renderFrame():void
		{
			if(!frameVo.isEmpty)
			{
				var textFieldWidth : Number = frameVo.width / frameVo.zoom;
				var textFieldHeight : Number = frameVo.height / frameVo.zoom;
				
				// set textfield size
				textfield.width = textFieldWidth;
				
				// place textfield on y
				if(frameVo.vAlign == VerticalAlign.TOP) textfield.y = (-frameVo.height/frameVo.zoom)*.5;
				else if(frameVo.vAlign == VerticalAlign.BOTTOM) textfield.y = (0 + (frameVo.height/frameVo.zoom)*.5) - (textfield.height/ frameVo.zoom);
				else textfield.y = (-textfield.height/ frameVo.zoom)*.5;
				
				textfield.y = textfield.y * scaleRatio;
				
				//scale texfield
				textfield.scaleX = textfield.scaleY = frameVo.zoom * scaleRatio;
				
				// place textfield x
				textfield.x = -textfield.width >> 1;//(textContainer.width-frameVo.width) >>1 ;
			}
			
			// apply position after center content 
			if(Infos.isCalendar) //Cal lux modification : posXAdjustement is always 0 except in cal Lux 
				x = (frameVo.x + frameVo.posXAdjustement) * scaleRatio;
			else
				x = frameVo.x * scaleRatio;
			y = frameVo.y * scaleRatio;
			rotation = frameVo.rotation;
			
			// draw content
			if(bitmapCache) Debug.warn("PageNavigatorFrameText > renderFrame : Bitmapcache should not exist... re-check process");
			bitmapCache = new Bitmap();
			
			try{
				var bmd : BitmapData = new BitmapData(content.width, content.height,true, 0xff0000);
				var m:Matrix = new Matrix();
				m.translate(-textfield.x, -textfield.y);
				bmd.draw(content, m, null,null, null, true);
				bitmapCache.bitmapData = bmd;
				bitmapCache.x = textfield.x;
				bitmapCache.y = textfield.y;
				content.addChild(bitmapCache);
				content.removeChild(textfield);
				textfield = null;	
				
				// render background
				if(frameVo.tb){
					content.graphics.beginFill( frameVo.fillColor, frameVo.fillAlpha );
					content.graphics.drawRect(-frameVo.width*scaleRatio*.5,-frameVo.height*scaleRatio*.5, frameVo.width*scaleRatio , frameVo.height*scaleRatio );
					content.graphics.endFill();
				}
			}
			catch (e:Error)
			{
				Debug.warn("PageNavigatorFrameText > error when trying to generate bitmap");
				if(bmd) bmd.dispose();
				if(bitmapCache) bitmapCache = null;
			}
			
			
		}
	}
	
}