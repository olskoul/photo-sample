/***************************************************************
 PROJECT 		: Tic Tac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import com.bit101.components.ScrollPane;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	
	import be.antho.data.ResourcesManager;
	
	import comp.DraggableItem;
	import comp.DropTargetArea;
	import comp.ScrollView;
	import comp.button.SimpleButton;
	import comp.button.SimpleThinButton;
	import comp.label.LabelTictac;
	
	import data.FrameVo;
	import data.Infos;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectVo;
	import data.TutorialStepVo;
	
	import manager.CalendarManager;
	import manager.CardsManager;
	import manager.DragDropManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.TutorialManager;
	import manager.UserActionManager;
	
	import mcs.navigator.groupCTA;
	import mcs.navigator.lib_groupDropGfx;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.TextUtils;
	import utils.VectorUtils;
	
	import view.edition.EditionArea;
	import view.edition.PageArea;
	import view.popup.PopupAbstract;

	public class PageNavigator extends ScrollPane
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// signals
		public static const PAGE_SWAPPED : Signal = new Signal();
		public static const PAGE_CLICKED : Signal = new Signal(PageVo);
		
		private static const REDRAW : Signal = new Signal(PageVo);
		private static const REDRAW_LABELS : Signal = new Signal();
		private var expandedWidth:Number = -1;
		
		// private
		private var itemContainer : DropTargetArea;
		private var pageItems:Vector.<PageNavigatorItem>;
		private var pageGroups:Vector.<PageGroup>;
		private var pageList : Vector.<PageVo> = Infos.project.pageList ; // reference to pageList in memory
		
		private var groupDragCTA : PageNavigatorGroupCTA;	// 
		private var groupDropGfx : lib_groupDropGfx = new lib_groupDropGfx();
		private var hasCover:Boolean = true;
		private var backCover:Sprite;
		private var addMorePage:SimpleThinButton;
		private var backCoverLabel:TextField;
		private var COLLAPSE_HEIGHT:Number = 126;
		private var spacer:Sprite;
		private var fromMorePage:Boolean = false;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		

		public function PageNavigator(_parent:DisplayObjectContainer,_x:Number, _y:Number) 
		{	
			// setup scroll
			scrollbarBackColor = Colors.WHITE;
			scrollbarBackAlpha = 1;
			
			super(_parent, _x, _y );
			_background.visible = true;			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	shortcuts
		////////////////////////////////////////////////////////////////
		
		public static function RedrawPageByFrame( frameVo : FrameVo ):void
		{
			REDRAW.dispatch( PagesManager.instance.getPageVoByFrameVo(frameVo) );
		}
		
		public static function RedrawPage( pageVo : PageVo ):void
		{
			REDRAW.dispatch( pageVo );
		}
		
		public static function RedrawLabels():void
		{
			REDRAW_LABELS.dispatch();
		}
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * select specific page by page vo
		 */
		public function selectPage(pageVo : PageVo):void
		{
			var pageItem : PageNavigatorItem;
			for (var i:int = 0; i < pageItems.length; i++) 
			{
				pageItem = pageItems[i] as PageNavigatorItem;
				if(pageItem.pageVo == pageVo){
					pageItem.selected = true;
					//itemContainer.addChild(pageItem) ; // put it back in front
				} else pageItem.selected = false;
			}
		}
		
		
		/**
		 * redraw all pages
		 */
		public function updateAllPages():void
		{
			var pageItem : PageNavigatorItem;
			for (var i:int = 0; i < pageItems.length; i++) 
			{
				pageItem = pageItems[i] as PageNavigatorItem;
				pageItem.update();
			}
		}
		
		
		
		/**
		 * on page group dropped success
		 * > if item is not null, it's a page move 
		 * > else it's a double page move
		 */
		public function onPageGroupDropped( item:PageNavigatorItem = null):void
		{
			// 
			var indexToMove : Number = (item)? item.pageVo.index : groupDragCTA.items[0].pageVo.index;
			var numPagesToMove : Number = (item)? 1 : groupDragCTA.items.length;
			
			// move pages and update page list					
			ProjectManager.instance.helper.movePages(indexToMove, numPagesToMove, groupDragCTA.dropIndex);
			
			// re render view
			drawItems();
			
			// go to specific page (if not already on it)
			if(item) itemClicked( item );
			else itemClicked( groupDragCTA.items[0] );
			
			// remove CTA and Separator
			killDropGfx();
		}
		
		
		
		/**
		 * EXPAND
		 */
		public function expand(_height:Number, _width:Number):void
		{
			expandedWidth = _width;
			super.vScrollbarEnabled = true;
			height = _height;//126
			drawItems();
			mouseWheelEnabled = true;
		}
		
		/**
		 * COLLAPSE
		 */
		public function collapse():void
		{
			expandedWidth = -1
			super.vScrollbarEnabled = true;
			height = COLLAPSE_HEIGHT;
			drawItems();
			mouseWheelEnabled = false;
			TweenMax.killDelayedCallsTo(scrollToEditedPageItem);
			TweenMax.delayedCall(.5,scrollToEditedPageItem);
		}
		
		
		/**
		 * create navigator content
		 * > kill previous content and recreate
		 */
		public function updateContent():void
		{
			destroy();
			
			addEventListener(MouseEvent.ROLL_OVER, allowGroupOver);
			addEventListener(MouseEvent.ROLL_OUT, disableGroupOver);
			
			pageList = Infos.project.pageList;
			pageItems = new Vector.<PageNavigatorItem>;
			var pageItem:PageNavigatorItem;
			for (var i:int = 0; i < pageList.length; i++) 
			{
				pageItem = new PageNavigatorItem(pageList[i]);
				pageItem.CLICKED.add(itemClicked);
				pageItems.push(pageItem);
				//pageItem.dropType = DragDropManager.TYPE_NAVIGATOR_SIMPLE;
				
				//add to dropTarget list
				addItemAsDropTarget(pageItem, i);
			}
			
			//Add to diplay list
			drawItems();	
			
			// create group drag CTA
			if(!groupDragCTA && pageItem) groupDragCTA = new PageNavigatorGroupCTA( pageItem.width,pageItem.height );
			
		}
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Initializes this component.
		 */
		override protected function init():void
		{
			hasCover = (ProjectManager.instance.project.classname != ProductsCatalogue.CLASS_CARDS);
			
			// create container
			itemContainer = new DropTargetArea(true);
			itemContainer.opaqueBackground = 0xbcbcbc; //0xbcbcbc;
			itemContainer.usePixelCheck = false;
			
			// Signal listener
			REDRAW.add(updatePage);
			REDRAW_LABELS.add(updateLabels);
			PAGE_SWAPPED.add(pageSwappedHandler);
			ProjectManager.COVER_CHANGED.add(coverChangedHandler);
			ProjectManager.PROJECT_UPDATED.add(updateContent);
			PagesManager.CURRENT_EDITED_PAGE_CHANGED.add(currentEditedPageChangedHandler);
			
			// init scrollpane
			super.init();
			super.vScrollbarEnabled = true;
			
			// set page navigator as drop target
			DragDropManager.instance.addDropTarget(itemContainer, [DragDropManager.TYPE_NAVIGATOR_SIMPLE, DragDropManager.TYPE_NAVIGATOR_DOUBLE]);
			
			// set a custom check in this class for the drop validity
			itemContainer.customCheckHandler = onGroupDropCheckHandler;
			itemContainer.customHighLight = onGroupCustomHighlight;
			
			// set size
			width = 1000;
			height = COLLAPSE_HEIGHT;
			
			
			//Add to tutorial flow
			var tutorialVo:TutorialStepVo = new TutorialStepVo(
				this,
				TutorialManager.KEY_BOTTOM_PANEL,
				TutorialManager.ARROW_DOWN,
				new Point(200, -20)
			);
			TutorialManager.instance.addStep(tutorialVo);
			
		}
		
		private function addItemAsDropTarget(pageItem:PageNavigatorItem, index:int):void
		{
			//ALBUMS cover
			if(Infos.isAlbum && index == 0)
			{
				pageItem.dragEnabled = false;
				return;
			}
			//CALENDARS cover
			if(Infos.isCalendar && index == 0)
			{
				pageItem.dragEnabled = false;
				return;
			}
			
			//CALENDARS page calendar are not swapable
			if(pageItem.dropType == DragDropManager.TYPE_NAVIGATOR_SIMPLE_CALENDAR)
				return;
			
			//CARDS page calendar are not swapable
			if(pageItem.dropType == DragDropManager.TYPE_NAVIGATOR_CARDS_POSTCARD_BACK)
			{
				pageItem.dragEnabled = false;
				return;
			}
			
			//CARDS (Maybe all project type later on) page with layout not editable are not swapable
			if(pageItem.dropType == DragDropManager.TYPE_NAVIGATOR_LAYOUT_NOT_EDITABLE)
			{
				pageItem.dragEnabled = false;
				return;
			}
			
			// allow item to be a drop target for navigator items, menu photos
			DragDropManager.instance.addDropTarget(pageItem, [DragDropManager.TYPE_NAVIGATOR_SIMPLE, DragDropManager.TYPE_MENU_PHOTO]);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			/*
			-> What the super does
			
			_vScrollbar = new VScrollBar(null, width - 10, 0, onScroll);
			_hScrollbar = new HScrollBar(null, 0, height - 10, onScroll);
			addRawChild(_vScrollbar);
			addRawChild(_hScrollbar);
			_corner = new Shape();
			_corner.graphics.beginFill(Style.BUTTON_FACE);
			_corner.graphics.drawRect(0, 0, 10, 10);
			_corner.graphics.endFill();
			
			addRawChild(_corner);*/
			
			
			content.addChild(itemContainer);
			
			//skinning
			color = 0xbcbcbc;
			shadow = false;
			borderAlpha =0;
			showScrollButtons(false);
			autoHideScrollBar = true;
			
			
		}
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override public function draw():void
		{
			super.draw();
			_mask.y = 18;
			_hScrollbar.y = 0;

		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
/**
 * 
 *
 * DRAW ITEMS
 * ADD ITEMS TO THE VIEW based on the project type (ALBUMS, CALENDARS..)
 * 
 * 
 * 
 * 
 */
		/**
		 * 
		 * General call for drawing items
		 * Detect whath kind of product we're in, and call the apropriate drawing method
		 * 
		 * */
		private function drawItems():void
		{
			Debug.log("PageNavigator.drawItems");
			if(expandedWidth == -1 && height != COLLAPSE_HEIGHT)
				height = COLLAPSE_HEIGHT;
			
			//type albums
			if(Infos.isAlbum)
				drawAlbumsItems();
			
			//type albums
			if(Infos.isCalendar)
				drawCalendarsItems();
			
			//type albums
			if(Infos.isCards)
				drawCardsItems();
			
			//spacer (left hand side of screen if collapsed or bottom is expanded)
			if(!spacer)
				spacer = new Sprite();
			spacer.graphics.clear();
			spacer.graphics.beginFill(Colors.GREY_DARK,0)
			spacer.graphics.drawRect(0,0,30,30);
			spacer.y = (expandedWidth == -1)?10:itemContainer.height +20;
			spacer.x = (expandedWidth == -1)?itemContainer.width + 20:10;
			itemContainer.addChild(spacer);
			
			//if from more page click, scroll to right end side 
			if(fromMorePage)
			{
				fromMorePage = false;
				TweenMax.delayedCall(.5,scrollHorizontallyTo,[20000]);
			}
			
		}
		
		/**
		 * 
		 * Drawing method for ALBUMS
		 * 
		 * */
		private function drawAlbumsItems():void
		{
			Debug.log("PageNavigator.drawAlbumsItems : pageList length is : ");
			if(!pageList){
				Debug.warn( " >> no page list, abord" );
				return;
			} else Debug.log("> " + pageList.length);
			
			var pageItem:PageNavigatorItem;
			var posX : Number = 35;
			var posY : Number = 28;
			var currentPageGroup : PageGroup;
			var isNewGroup : Boolean;
			
			pageGroups = new Vector.<PageGroup>; // reset page group
			Debug.log("PageNavigator.drawAlbumsItems : draw page item ");
			for (var i:int = 0; i < pageList.length; i++) 
			{
				pageItem = getPageItemByVo(pageList[i]);
				if(pageItem)
				{
					pageItem.x = posX; 
					pageItem.y = posY;
					posX = posX + pageItem.WIDTH;
					
					// add page to current group if there is one
					if( currentPageGroup ){
						currentPageGroup.pages.push(pageItem);
					}
					
					// check if we need to create a new page group
					isNewGroup = checkIfPageIsNewPageGroup( i );
					
					//handle expanded state
					if(expandedWidth != -1)
					{
						if( posX+15+(pageItem.WIDTH*2) > expandedWidth && isNewGroup)
						{	
							posX = 35;
							posY += PageNavigatorItem.HEIGHT + 25;
						}
					}
					
					if(isNewGroup){
						
						posX = (i==0 && !hasCover)?posX:posX + 15;
						
						// if there is a previous page gropu and the page group has more than 1 page, we add it to page group list
						if(currentPageGroup && currentPageGroup.pages.length>1) pageGroups.push(currentPageGroup);
						
						currentPageGroup = new PageGroup();
						// drag rect is between two pages
						currentPageGroup.dragRect = new Rectangle( posX + pageItem.WIDTH - 25, posY, 50, PageNavigatorItem.HEIGHT );
						// drop rect is between two page groups
						currentPageGroup.dropRect = new Rectangle( posX - 35, posY, 70, 100 );						
					} 
					
					// add page item to display list
					itemContainer.addChild(pageItem);
					//pageItem.setLabel();
					//Override label if needed
					getLabel(pageItem,i);
				}
			}
			
			
			
			// BACK COVER + Add page BTN
			if( Infos.project.classname == ProductsCatalogue.CLASS_ALBUM ) {
				
				if(pageItem)
				{
					Debug.log("PageNavigator.drawAlbumsItems : draw back cover");
					//add back cover
					if(!backCover)
						backCover = new Sprite();
					backCover.graphics.clear();
					backCover.graphics.beginFill(Colors.GREY_DARK,1)
					backCover.graphics.drawRect(0,0,pageItem.WIDTH,PageNavigatorItem.HEIGHT);
					backCover.y = pageItem.y;
					backCover.x = pageItem.x + pageItem.width;
					itemContainer.addChild(backCover);
				}
				
				
				//add back cover label
				if(backCover && !backCoverLabel)
				{
					Debug.log("PageNavigator.drawAlbumsItems : draw back cover label");
					backCoverLabel = TextUtils.createLabelWithTextFormat(ResourcesManager.getString("common.cover.back"), TextFormats.ASAP_BOLD(12,Colors.WHITE, TextFormatAlign.CENTER),0,PageNavigatorItem.HEIGHT+1,pageItem.WIDTH);
					backCover.addChild(backCoverLabel);
				}
				
				
				//add more page btn if necessary 
				var maxPages:int = ProjectManager.instance.getMaxPages();
				var currentPagesNumber:int = Infos.project.flooredNumPages
				if(currentPagesNumber)
				{
					if( maxPages >= currentPagesNumber + 10)
					{
						Debug.log("PageNavigator.drawAlbumsItems : draw more pages");
						if(!addMorePage)
							addMorePage = new SimpleThinButton("btn.add.more.pages.label",Colors.BLUE,addMorePageToProject,Colors.YELLOW, Colors.BLACK, Colors.WHITE,1,false,false);  //SimpleButton.createBlueButton("add 10 pages",addMorePageToProject);
						
						
						if(backCover)
						{
							addMorePage.x = backCover.x + backCover.width + 15;
							addMorePage.y = backCover.y + (backCover.height - backCoverLabel.height - addMorePage.height)*.5 ;
							itemContainer.addChild(addMorePage);
						}
						
					}
				}
				
			}
			
			
			
			// again, if there is a previous page group and the page group has more than 1 page, we add it to page group list
			if(currentPageGroup && currentPageGroup.pages.length>1) pageGroups.push(currentPageGroup);
			
		}
		
		private function addMorePageToProject():void
		{
			var newProjectTypeCode:String = Infos.project.docPrefix + String(Infos.project.flooredNumPages + 10);
			var newProjectDocType:String = ProjectManager.instance.getDocTypeIDByCode(newProjectTypeCode);
			if(newProjectDocType != null)
			{
				fromMorePage = true;
				ProjectManager.instance.updateCurrentAlbumProject(newProjectDocType, Infos.project.type, Infos.project.coverType);
				UserActionManager.instance.addAction(UserActionManager.TYPE_PROJECT_UPGRADE);
			}
			else
			{
				Debug.warnAndSendMail("PageNavigator.addMorePage: failed");
				PopupAbstract.Alert("popup.error.title","popup.warn.support.desc",false,null,null,false);
			}
		}
		
		/**
		 * 
		 * Drawing method for CALENDARS
		 * There is 2 types of drawing for calendars: 
		 * Calendars XXL (WCAL3) and XL (WCAL2) are called 2 pages calendars
		 * All others are 1 page calendars
		 * 
		 * */
		private function drawCalendarsItems():void
		{
			var pageItem:PageNavigatorItem;
			var posX : Number = 35;
			var posY : Number = 28;
			//var currentPageGroup : PageGroup;
			var isNewGroup : Boolean;
			var newLine:Boolean;
			var gap:int;
			
			//pageGroups = new Vector.<PageGroup>; // reset page group
			for (var i:int = 0; i < pageList.length; i++) 
			{
				pageItem = getPageItemByVo(pageList[i]);
				if(pageItem)
				{
					pageItem.x = posX; 
					pageItem.y = posY;
					posX = posX + pageItem.WIDTH;
					
					// add page to current group if there is one
					/*if( currentPageGroup ){
						currentPageGroup.pages.push(pageItem);
					}*/
					
					// check if we need to create a new page group
					isNewGroup = checkIfPageIsNewPageGroup( i );
					//handle expanded state
					if(expandedWidth != -1)
					{
						if( posX+15+(pageItem.WIDTH*2) > expandedWidth && isNewGroup)
						{	
							posX = 35;
							posY += PageNavigatorItem.HEIGHT + 25;
							newLine = true;
						}
						else
							newLine = false;
					}
					
					if(isNewGroup)
					{
						gap = (newLine)?0:15;
						posX = (i==0 && !hasCover)?posX:posX + gap;
						
						// if there is a previous page gropu and the page group has more than 1 page, we add it to page group list
						/*if(currentPageGroup && currentPageGroup.pages.length>1) pageGroups.push(currentPageGroup);
						
						currentPageGroup = new PageGroup();
						// drag rect is between two pages
						currentPageGroup.dragRect = new Rectangle( posX + pageItem.WIDTH - 25, posY, 50, PageNavigatorItem.HEIGHT );
						// drop rect is between two page groups
						currentPageGroup.dropRect = new Rectangle( posX - 35, posY, 70, 100 );		*/				
					} 
					
					// add page item to display list
					itemContainer.addChild(pageItem);
					//pageItem.setLabel();
					//Override label if needed
					getLabel(pageItem,i);
				}
			}
			
		}
		
		/**
		 * 
		 * Drawing method for CARDS
		 * 
		 * */
		private function drawCardsItems():void
		{
			var pageItem:PageNavigatorItem;
			var posX : Number = 35;
			var posY : Number = 28;
			//var currentPageGroup : PageGroup;
			var isNewGroup : Boolean;
			
			//pageGroups = new Vector.<PageGroup>; // reset page group
			for (var i:int = 0; i < pageList.length; i++) 
			{
				pageItem = getPageItemByVo(pageList[i]);
				if(pageItem)
				{
					pageItem.x = posX; 
					pageItem.y = posY;
					posX = posX + pageItem.WIDTH;
					
					// check if we need to create a space
					isNewGroup = checkIfPageIsNewPageGroup( i );
					//handle expanded state
					if(expandedWidth != -1)
					{
						if( posX+15+(pageItem.WIDTH*2) > expandedWidth && isNewGroup)
						{	
							posX = 35;
							posY += PageNavigatorItem.HEIGHT + 25;
						}
					}
					
					if(isNewGroup)
					{
						posX = (i==0 && !hasCover)?posX+5:posX + 15;
					}
					else
						posX = posX+5;
					
					// add page item to display list
					itemContainer.addChild(pageItem);
					//Override label if needed
					getLabel(pageItem,i);
				}
			}
			
		}
		/**
		 * 
		 * END DRAWING METHODS
		 * 
		 */		
		
		
		
		
		/**
		 * Current edited page has changed
		 * Scroll to the page if needed
		 * */
		private function currentEditedPageChangedHandler():void
		{
			//scroll to the edited item
			scrollToEditedPageItem();
			
		}
		
		/**
		 * Scroll to the pageitem currently edited
		 * */
		private function scrollToEditedPageItem():void
		{
			if(! PagesManager.instance.currentEditedPage ) return;
			var currentEditedPageItem:PageNavigatorItem = getPageItemByVo(PagesManager.instance.currentEditedPage);
			if(!currentEditedPageItem) return; //if null do nothing
			
			var itemBounds:Rectangle = currentEditedPageItem.getBounds(this);
			var visibleArea:Rectangle = new Rectangle(getHPosition(),0,width+getHPosition(),height);
			
			if(itemBounds.right > width)
				scrollHorizontallyTo(getHPosition() + (itemBounds.right -width) + 20);
			
			if(itemBounds.left < 0)
				scrollHorizontallyTo(getHPosition() + itemBounds.left - 20);
			
		}
		
			
		
		/**
		 * Destroy items content
		 * delete views
		 * removeListeners
		 * */
		
		public function destroy():void
		{
			removeEventListener(MouseEvent.ROLL_OVER, allowGroupOver);
			removeEventListener(MouseEvent.ROLL_OUT, disableGroupOver);
			
			if(pageItems){
				var pageItem:PageNavigatorItem;
				for (var i:int = 0; i < pageItems.length; i++) 
				{
					pageItem = pageItems[i];
					pageItem.CLICKED.remove(itemClicked);
					pageItem.destroy();
					//remove to dropTarget list
					if(i>0) DragDropManager.instance.removeDropTarget(pageItem);
				}
				pageItems = null;
			}
			
			//destroy spacer
			if(spacer)
			{
				if(spacer.parent)
					spacer.parent.removeChild(spacer);
				spacer.graphics.clear();
				spacer = null;
			}
			
			//destroy addMorePage
			if(addMorePage)
			{
				if(addMorePage.parent)
					addMorePage.parent.removeChild(addMorePage);
				addMorePage = null;
			}
		}
		
		
		/**
		 * Cover has changed, remove first page item ans create a new one
		 */
		private function coverChangedHandler():void
		{
			pageItems[0].CLICKED.removeAll();
			pageItems[0].destroy();
			var pageItem:PageNavigatorItem = new PageNavigatorItem(pageList[0]);
			pageItem.CLICKED.add(itemClicked);
			pageItems[0]=pageItem;
			drawItems();
		}
		
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		private function pageSwappedHandler():void
		{
			Debug.log("PageNavigator.pageSwappedHandler");
			//VectorUtils.sortOnNestedProps(pageItems, PageNavigatorItem,"pageVo.index", Array.NUMERIC);
			VectorUtils.sortOn(pageList,"index",Array.NUMERIC);
			drawItems();
		}
		
		
		/**
		 * Overriden the default label of the item is the project wants it or not
		 * */
		private function getLabel(pageItem:PageNavigatorItem, i:int):void
		{
			var pageVo:PageVo = pageItem.pageVo;
			
			switch(Infos.project.classname)
			{
				case ProductsCatalogue.CLASS_CALENDARS:
					if(pageVo.isCalendar) pageItem.setLabel(CalendarManager.instance.getMonthWithOffSetYear(pageVo.monthIndex,false));
					if(pageVo.index == 0 && !pageVo.isCalendar) pageItem.setLabel(ResourcesManager.getString("common.cover"));
					if(pageVo.index > 0 && !pageVo.isCalendar)pageItem.setLabel(" ");
					break;
				
				case ProductsCatalogue.CLASS_CARDS:
					var pagePerDraw:int =  CardsManager.instance.getNumCardsPagePerDraw();
					var modulo:int =  i % pagePerDraw;
					if( i<1 || modulo == 0) pageItem.setLabel(ResourcesManager.getString("common.front"));
					else if(modulo > 0 && modulo<pagePerDraw-1) pageItem.setLabel(ResourcesManager.getString("common.inside"));
					else if(modulo == pagePerDraw-1) pageItem.setLabel(ResourcesManager.getString("common.backCover"));
					break;
				
				default:
					if(pageVo.index == 0) pageItem.setLabel(ResourcesManager.getString("common.cover"));
					else pageItem.setLabel(""+ pageVo.index);
					
			}
		}		
		
		/**
		 * check if the page index is a new page group
		 */
		private function checkIfPageIsNewPageGroup( pageIndex : int ):Boolean
		{
			var pagePerDraw:int
			switch(ProjectManager.instance.project.classname)
			{
				case ProductsCatalogue.CLASS_ALBUM:
					if( pageIndex<2 || pageIndex%2==1 ) return true;
					break;
				
				case ProductsCatalogue.CLASS_CALENDARS:
					pagePerDraw =  ProjectManager.instance.helper.getNumCalendarsPagePerDraw();
					var condition:int = (pagePerDraw == 2)?0:pagePerDraw-1;
					if( pageIndex<1  || pageIndex % pagePerDraw == condition) return true;
					break;
				
				case ProductsCatalogue.CLASS_CARDS:
					pagePerDraw =  CardsManager.instance.getNumCardsPagePerDraw();
					if( pageIndex<1 || pageIndex % pagePerDraw == pagePerDraw-1) return true;
					break;
			}
			
			return false;
		}
		
		
		/**
		 * check if mouse is currently over a page group grap point
		 * -> if yes we display the call to action to start dragging a group of page
		 */
		private function checkPageGroupsOver(e:MouseEvent):void
		{
			if(!pageGroups) return;
			if( Infos.project.classname == ProductsCatalogue.CLASS_CARDS) return; // TODO : modify this
			
			var isOver :Boolean = false;
			var dragRect : Rectangle;
			var pageItem : PageNavigatorItem;
			for (var i:int = 0; i < pageGroups.length; i++) 
			{
				if(pageGroups[i].dragRect.contains(itemContainer.mouseX, itemContainer.mouseY)){
					if(groupDragCTA && groupDragCTA.parent) return; // it's already shown
					isOver = true;
					pageItem = pageGroups[i].pages[0];
					dragRect = pageGroups[i].dragRect;
					
					// place callto action
					groupDragCTA.visible = false;
					groupDragCTA.x = pageItem.x ;
					groupDragCTA.y = pageItem.y ;
					addChild(groupDragCTA);
					
					// update datas
					if(!groupDragCTA.isDragging) {
						groupDragCTA.items = pageGroups[i].pages;
						//trace("PAGE DRAG : "+pageGroups[i].pages[0].pageVo.index);
					}
					
					
					// show CTA
					TweenMax.killTweensOf(groupDragCTA);
					TweenMax.to( groupDragCTA, .7, {ease:Strong.easeOut, autoAlpha : 1, startAt:{autoAlpha:0}});
				}
			}
			
			if(!isOver) killGroupCTA();
		}
		private function allowGroupOver(e:MouseEvent):void
		{
			removeEventListener(MouseEvent.MOUSE_MOVE, checkPageGroupsOver);
			addEventListener(MouseEvent.MOUSE_MOVE, checkPageGroupsOver);
		}
		private function disableGroupOver(e:MouseEvent):void
		{
			removeEventListener(MouseEvent.MOUSE_MOVE, checkPageGroupsOver);
		}
		private function killGroupCTA():void
		{	
			if(groupDragCTA)
			{
				TweenMax.killTweensOf(groupDragCTA);
				groupDragCTA.dispose();
			}
		}
		
		
		/**
		 * custom check on group drop
		 */
		private function onGroupDropCheckHandler( dragItem : DraggableItem ):Boolean
		{	
			if(!pageGroups) return false;
			// check if pos is between page groups
			var dropRect : Rectangle;
			var pageGroup : PageGroup;
			for (var i:int = 0; i < pageGroups.length; i++) 
			{
				pageGroup = pageGroups[i];
				if(pageGroup.dropRect.contains(itemContainer.mouseX, itemContainer.mouseY))
				{
					// add separator
					groupDropGfx.mouseEnabled = groupDropGfx.mouseChildren = false ;
					groupDropGfx.y = pageGroup.dropRect.y - 10 ;
					groupDropGfx.height = pageGroup.pages[0].height+20
					groupDropGfx.x = pageGroup.dropRect.x + pageGroup.dropRect.width*.5 - groupDropGfx.width*.5 - 5 // 5 is gap /2;
					addChild( groupDropGfx );
					groupDragCTA.dropIndex = pageGroup.pages[0].pageVo.index;
					return true;
				}
			}
			
			// TODO : amélioration : animate pages
			
			// remove gfx if nothing found
			groupDragCTA.dropIndex = -1;
			killDropGfx();
			return false;
		}
		
		/**
		 * custom stop highlight for draggable area
		 */
		private function onGroupCustomHighlight(flag:Boolean, dragItem : DraggableItem):void
		{
			if(!flag) killDropGfx();
		}
		
		/**
		 *
		 */
		private function killDropGfx():void
		{
			if(groupDropGfx.parent) groupDropGfx.parent.removeChild(groupDropGfx);
		}
		
		
		
		
		/**
		 * return a page item by it's page vo
		 */ 
		private function getPageItemByVo(pageVo:PageVo):PageNavigatorItem
		{
			if(!pageItems) return null;
			for (var i:int = 0; i < pageItems.length; i++) 
			{
				if(pageItems[i].pageVo == pageVo)
					return pageItems[i];
			}
			
			return null;
		}
		
		/**
		 *
		 */
		private function itemClicked(item:PageNavigatorItem):void
		{
			selectPage(item.pageVo);
			PAGE_CLICKED.dispatch(item.pageVo);
		}
		
		/**
		 * update (redraw) simple page
		 */
		private function updatePage(pageVo:PageVo):void
		{
			var pageItem : PageNavigatorItem;
			if(!pageItems) return;
			for (var i:int = 0; i < pageItems.length; i++) 
			{
				pageItem = pageItems[i] as PageNavigatorItem;
				if(pageItem.pageVo == pageVo) pageItem.update();
			}
		}
		
		/**
		 * update (redraw) labels
		 */
		private function updateLabels():void
		{
			if(!pageItems) return;
			var pageItem : PageNavigatorItem;
			for (var i:int = 0; i < pageItems.length; i++) 
			{
				pageItem = pageItems[i] as PageNavigatorItem;
				getLabel(pageItem,i);
			}
		}
	}
}
import flash.display.Sprite;
import flash.geom.Rectangle;

import view.edition.pageNavigator.PageNavigatorItem;

internal class PageGroup
{
	public var pages : Vector.<PageNavigatorItem> = new Vector.<PageNavigatorItem>();
	public var dragRect : Rectangle; // areas where the drag zone is active
	public var dropRect : Rectangle; // areas where the drop zone is active
}
