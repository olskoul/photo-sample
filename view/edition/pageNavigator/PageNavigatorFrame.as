﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import data.FrameVo;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	/**
	 */
	public class PageNavigatorFrame extends Sprite
	{
		public var scaleRatio : Number // ratio between real page size and navigator page
		
		// datas
		public var frameVo : FrameVo; // ... rotation, filters, size, dpi, etc...
		public var boundRect:Rectangle = new Rectangle(); // boudaries of the current frame
		public var ratio:Number; // ratio is w/h
			
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PageNavigatorFrame( frameVo : FrameVo, scaleRatio:Number) 
		{ 
			this.frameVo = frameVo ;
			this.scaleRatio = scaleRatio;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		public function init():void
		{
			// must be overridden
		}
		

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public function destroy():void
		{		
			// must be overridden			
			frameVo = null;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

	}
}