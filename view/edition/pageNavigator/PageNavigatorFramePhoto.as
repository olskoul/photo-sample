﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import com.greensock.easing.Back;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import be.antho.utils.TintMovieClip;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import data.BackgroundVo;
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	
	import manager.MeasureManager;
	import manager.PagesManager;
	
	import offline.manager.LocalStorageManager;
	
	import ordering.FrameExporter;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.Frame;
	import view.edition.FramePhoto;
	import view.uploadManager.PhotoUploadManager;



	/**
	 * Frame is alayer inside the edition Area
	 * It can contain an image, a clipart, a text or something that can be editable
	 * It can be moved and rotated and the inside element can be transformed using the transform tool (zoom, directio, t
	 */
	public class PageNavigatorFramePhoto extends PageNavigatorFrame
	{
		// view
		private var content:Sprite; // it the image container on which we apply transformations
		private var image:Sprite; // is the image we are working with, it contains the working bitmap redrawn depending on the quality needed
		private var workingBitmap:Bitmap; // is the bitmap of the photo we are working on, this is redrawn depending on the quality needed
		
		// the content , here its a photo (Are we going to use a frame for text as well??)
		private var photo : Bitmap;// = new testPhoto() as Bitmap;
		private var photoLoader : BulkLoader; // loader of image
		private var masker : Shape;
		private var postCardBack:Sprite
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PageNavigatorFramePhoto( frameVo : FrameVo, scaleRatio:Number) 
		{ 
			super( frameVo, scaleRatio );
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			if(frameVo.photoVo)	loadPhoto();
			else drawEmpty();
			//opaqueBackground = 0xffffff;
		}
		

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			// kill loader
			destroyPhotoLoader();
			
			//
			if(photo){
				if(frameVo.photoVo) photo.bitmapData.dispose(); // otherwise, it's the emptyBitmapdata and we don't want it to be dispose
				photo.bitmapData = null;
				photo = null;	
			}
			
			if(image){
				workingBitmap.bitmapData.dispose();
				workingBitmap.bitmapData = null;
				image = null;
			}
			
			if(content){
				removeChild(content)
				content = null;	
			}
			
			if(postCardBack)
			{
				if(postCardBack.parent)
					postCardBack.parent.removeChild(postCardBack);
				postCardBack = null;
			}
			
			super.destroy();
		}
		
				
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * destroy current loader
		 */
		private function destroyPhotoLoader():void
		{
			if(photoLoader){
				photoLoader.removeEventListener(BulkLoader.ERROR, onPhotoLoadError);
				photoLoader.removeEventListener(BulkProgressEvent.COMPLETE, onPhotoLoaded);
				photoLoader.clear();
				photoLoader = null;
			}
		}
		
		/**
		 * load a photo
		 */
		private function loadPhoto():void
		{
			// security
			if(!frameVo || !frameVo.photoVo || !frameVo.photoVo.id  ){
				Debug.warn("PageNavigatorFrame.loadPhoto : we do not render frame as there is a problem with frameVo : " + frameVo);
				return;
			}
			
			// clean loader
			destroyPhotoLoader();
			
			if( frameVo.photoVo.temp )
			{
				usePhotoBitmap( PhotoUploadManager.instance.getTempThumbDataClone( frameVo.photoVo.id ));
			}
			else
			{
				var photoUrl : String = frameVo.photoVo.thumbUrl
					
				// ONLINE CONTENT
				if( !frameVo.IS_LOCAL_CONTENT ){
					photoLoader = new BulkLoader();
					photoLoader.addEventListener(BulkLoader.ERROR, onPhotoLoadError);
					photoLoader.addEventListener(BulkProgressEvent.COMPLETE, onPhotoLoaded);
					photoLoader.add(photoUrl, {id:"thumb", type:BulkLoader.TYPE_IMAGE});
					photoLoader.start();
				}
				
				// OFFLINE CONTENT (TODO:: review this system, we want to use a bulk loader for offline AND online content, no more localStorageManager
				else
				{
					LocalStorageManager.instance.loadLocalImage( photoUrl, onLocalPhotoLoaded, onLocalPhotoError );
				}
			}
		}
		private function onPhotoLoadError(e:ErrorEvent):void
		{
			Debug.warn("PageNavigatorFramePhoto > Error while loading image : " + e.text + " in PageNavigatorFrame");
			// handle error 
			drawError();
		}
		private function onPhotoLoaded(e:Event):void
		{
			usePhotoBitmap(photoLoader.getBitmapData("thumb", true));
		}
		private function onLocalPhotoError(e:ErrorEvent):void
		{
			Debug.warn("PageNavigatorFramePhoto.onLocalPhotoError :" + e.toString());
			// handle error 
			drawError();
		}
		private function onLocalPhotoLoaded( bmd : BitmapData):void
		{
			// added the check if framevo as during local load time, this frame could have been destroyed, meaning this frame do not exist anymore..
			if(frameVo) usePhotoBitmap( bmd );
			else if (bmd) bmd.dispose();
		}
		
		
		
		/**
		 * use photo loaded locally or from online
		 */
		private function usePhotoBitmap( bmd : BitmapData ) : void
		{
			if(photo && photo.bitmapData) photo.bitmapData.dispose();
			
			if(!bmd){
				destroy();
				return;
			}
			
			
			photo = null;
			//photo = (e.currentTarget as LoaderInfo).content as Bitmap;
			photo = new Bitmap( bmd, "auto", true);//photoLoader.getBitmap("thumb", true);
			destroyPhotoLoader();
			
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// HACK : CHECK if photo size has the same ratio as it should be // TODO : ask keith for best option with background ratio issue
			// > this is a bad hack but as we do not receive correct data from background xml (image ratio), let's keep it like this
			if(frameVo.photoVo && frameVo.photoVo is BackgroundVo)
			{
				// modify if needed
				PagesManager.instance.verifyBackgroundVoRatio( photo.bitmapData, frameVo.photoVo as BackgroundVo )
				// reinject photovo in frame
				FrameVo.injectPhotoVoInFrameVo(frameVo, frameVo.photoVo);
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			construct();
		}
		
		
		
		
		
		/**
		 * create view
		 */
		private function construct():void
		{
			if(!content){
				content = new Sprite();
				image = new Sprite();
				workingBitmap = new Bitmap();
				image.addChild(workingBitmap);
			}
			
			// generate minature
			generateMiniPhoto();
			
			//add image into content
			content.addChild(image);

			// render frame based on frame vo
			renderFrame();
			
			addChild(content);
			
			// add quality indicator if needed
			var printQuality:Number = frameVo.printQuality;
			if( printQuality < 2){ // TODO : change this to 1 if we do not want the green warnings
				var qualityIndicator : Sprite = new Sprite();
				var color : Number;
				if(printQuality == 1) color = Colors.GREEN_FLASH;
				else if (printQuality == 0) color = Colors.ORANGE;
				else color = Colors.RED_FLASH;
				qualityIndicator.graphics.beginFill(color);
				qualityIndicator.graphics.lineStyle(1,Colors.WHITE);
				qualityIndicator.graphics.drawRect(-6,-6,6,6);
				qualityIndicator.graphics.endFill();
				qualityIndicator.x = frameVo.width * scaleRatio *.5;
				qualityIndicator.y = frameVo.height * scaleRatio *.5;
				addChild(qualityIndicator);
			}
			
			// TODO (OPTIMIZE) : To optimize perf, we should probably store the full bitmapdata after the construct.
			// Or at leaste, be sure only pages displayed are rendered (or we risk to have lots of masks running at same time)
			
			//
			if(frameVo && frameVo.photoVo && frameVo.photoVo.error) drawError();
		}
		
		
		/**
		 * Draw empty photo frame
		 */
		private function drawEmpty():void
		{
			destroyPhotoLoader();
			
			// is empty
			var frameWidth : Number = frameVo.width * scaleRatio;
			var frameHeight : Number = frameVo.height * scaleRatio;
			graphics.clear();
			if(frameVo.type == FrameVo.TYPE_TEXT) { 
				graphics.lineStyle(1, Colors.BLUE_LIGHT);
				graphics.beginFill(Colors.BLUE_LIGHT, .3);
			}
			else if(frameVo.type == FrameVo.TYPE_BKG)
			{
				if(frameVo.isPostCardBackground)
				{
					postCardBack = PagesManager.instance.drawPostCardBack();
					postCardBack.scaleX = postCardBack.scaleY = scaleRatio;
					postCardBack.x = -postCardBack.width/2;
					postCardBack.y = -postCardBack.height/2;
					addChild(postCardBack);
				}
				else if(frameVo.fillColor != -1)
					graphics.beginFill(frameVo.fillColor, 1);
				
			}
			else {
				graphics.lineStyle(1, Colors.GREY);
				//frameWidth-=1;
				//frameHeight-=1;
				graphics.beginFill(0xcccccc, .4);
			}
			
			graphics.drawRect(-frameWidth*.5, -frameHeight*.5, frameWidth-1, frameHeight-1);
			graphics.endFill();
			
			x = frameVo.x * scaleRatio;
			y = frameVo.y * scaleRatio;
			rotation = frameVo.rotation;
			cacheAsBitmap = true;
		}
		
		/**
		 * Draw empty photo frame
		 */
		private function drawError():void
		{
			cacheAsBitmap = false;
			
			// if image and error, keep image visible with red background
			if(image) image.alpha = 0.5;
			
			destroyPhotoLoader();
			
			// security to be sure to not render if no framevo available
			if(!frameVo) return;
			
			// is empty
			var frameWidth : Number = frameVo.width * scaleRatio;
			var frameHeight : Number = frameVo.height * scaleRatio;
			graphics.clear();
			if(frameVo.type == FrameVo.TYPE_TEXT) { 
				graphics.lineStyle(1, Colors.RED_FLASH);
				graphics.beginFill(Colors.RED_FLASH, .3);
			}
			else if(frameVo.type == FrameVo.TYPE_BKG)
			{
				if(frameVo.isPostCardBackground)
				{
					postCardBack = PagesManager.instance.drawPostCardBack();
					postCardBack.scaleX = postCardBack.scaleY = scaleRatio;
					postCardBack.x = -postCardBack.width/2;
					postCardBack.y = -postCardBack.height/2;
					addChild(postCardBack);
				}
				else if(frameVo.fillColor != -1)
					graphics.beginFill(frameVo.fillColor, 1);
				
			}
			else {
				graphics.lineStyle(1, Colors.RED_FLASH);
				//frameWidth-=1;
				//frameHeight-=1;
				graphics.beginFill(Colors.RED_FLASH, .4);
			}
			
			graphics.drawRect(-frameWidth*.5, -frameHeight*.5, frameWidth-1, frameHeight-1);
			graphics.endFill();
			
			x = frameVo.x * scaleRatio;
			y = frameVo.y * scaleRatio;
			rotation = frameVo.rotation;
			cacheAsBitmap = true;
		}
		
		
		
		
		
		
		/**
		 * frame offset is the amount of pixel between the frame bounds and the image bounds
		 * if no border nor shadow the frame offset is always 0
		 * but if there is a border or a shadow, the photo is not exactly the size of the frame, the difference is the offset (size of the border or shadow)
		 */
		private function get frameOffset():Number
		{
			if(frameVo) return MeasureManager.millimeterToPixel(frameVo.border) * scaleRatio;
			return 0;
		}
		
		
		
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		private function renderFrame():void
		{
			try
			{	
					
				content.scaleX = content.scaleY = 1;
				image.scaleX = image.scaleY = frameVo.zoom * scaleRatio;
				
				var frameWidth : Number = frameVo.width * scaleRatio;
				var frameHeight : Number = frameVo.height * scaleRatio;
				var imageWidth : Number = image.width;
				var imageHeight : Number = image.height;
				
				
				// APPLY MASK / CROP
				if( frameVo.mask != FrameVo.MASK_TYPE_NONE || (imageWidth != frameVo.width || imageHeight != frameVo.height || frameOffset != 0) )
				{
					masker = new Shape();
					masker.graphics.beginFill(0xff0000, 1);
					var w : Number = frameWidth - frameOffset*2;
					var h : Number = frameHeight - frameOffset*2;
					var cPoint : Point = new Point(-w*.5,-h*.5);
					if(frameVo && frameVo.mask != FrameVo.MASK_TYPE_NONE)
					{
						if (frameVo.mask == FrameVo.MASK_TYPE_CIRCLE) masker.graphics.drawEllipse(cPoint.x,cPoint.y,w,h);
						else if (frameVo.mask == FrameVo.MASK_TYPE_CORNER_RADIUS)
						{
							var mmCr : Number = MeasureManager.millimeterToPixel(frameVo.cr) * scaleRatio;
							masker.graphics.drawRoundRect(cPoint.x,cPoint.y,w,h,mmCr, mmCr);
						}
					}
					else masker.graphics.drawRect(cPoint.x,cPoint.y,w,h);
					masker.graphics.endFill();
					content.mask = masker;
					addChild(masker);
					
					image.x = (imageWidth-frameWidth)*.5 - frameVo.cLeft * scaleRatio;
					image.y = (imageHeight-frameHeight)*.5 - frameVo.cTop * scaleRatio;
				}
					
				// apply border 
				if(frameVo.border >0 ){
					var borderShape:Shape = new Shape();
					borderShape.graphics.beginFill(frameVo.fillColor, 1);
					w = frameWidth ;
					h = frameHeight; 
					cPoint = new Point(-w*.5,-h*.5);
					if(frameVo && frameVo.mask != FrameVo.MASK_TYPE_NONE)
					{
						if (frameVo.mask == FrameVo.MASK_TYPE_CIRCLE) borderShape.graphics.drawEllipse(cPoint.x, cPoint.y,w,h);
						else if (frameVo.mask == FrameVo.MASK_TYPE_CORNER_RADIUS)
						{
							mmCr = MeasureManager.millimeterToPixel(frameVo.cr)*scaleRatio;
							borderShape.graphics.drawRoundRect(cPoint.x, cPoint.y,w,h,mmCr, mmCr);
						}
					}
					else borderShape.graphics.drawRect(cPoint.x, cPoint.y,w,h);
					borderShape.graphics.endFill();
					addChildAt(borderShape,0);
				}
				
				// apply shadow
				if(frameVo.shadow)
				{
					var bitmapdata:BitmapData = FrameExporter.generateFrameShadow( frameVo.mask, false );
					var shadowSprite:Bitmap = new Bitmap( bitmapdata, "auto", true );
					
					shadowSprite.x = -frameVo.width*.5 * scaleRatio;
					shadowSprite.y = -frameVo.height*.5 * scaleRatio;
					shadowSprite.width = (frameVo.width + frameVo.getShadowSize()) * scaleRatio;//*FrameVo.SHADOW_SCALE
					shadowSprite.height = (frameVo.height + frameVo.getShadowSize()) * scaleRatio;//*FrameVo.SHADOW_SCALE;
					addChildAt(shadowSprite,0);
					
					TintMovieClip.tint(shadowSprite, frameVo.sCol, 1);
				}
				
				
				// apply position after center content
				x = frameVo.x * scaleRatio;
				y = frameVo.y * scaleRatio;
				rotation = frameVo.rotation;
				
				// cache as bitmap
				cacheAsBitmap = true;
			} 
			catch(error:Error) 
			{
				Debug.warn("PageNavigatorFramePhoto.renderframe:"+error.message);
			}
		}
		

		
		
		
		/**
		 * generate mini bitmap of photo vo to be used in preview
		 */		
		private function generateMiniPhoto():void
		{
			// select quality wanted width // use static quality for minatures
			var wantedWidth:Number = 100;
			
			// generate bitmap with wanted quality width
			var scaleRatio : Number = wantedWidth/photo.width;
			var bmd : BitmapData = new BitmapData(photo.width*scaleRatio, photo.height*scaleRatio, true, 0x000000); // TODO (OPTIMIZE): check if we can implement in the current tic tac photo tansparent images (png transparency)
			var m:Matrix = new Matrix();
			m.scale(scaleRatio, scaleRatio);
			bmd.draw(photo, m, null, null, null, true);
			
			if(frameVo.isPopart) bmd = Frame.popartBitmap(bmd, frameVo);
			
			if(workingBitmap.bitmapData) {
				workingBitmap.bitmapData.dispose();
				workingBitmap.bitmapData = null;
			}
			workingBitmap.bitmapData = bmd;
			workingBitmap.smoothing = true;
			
			
			// scale bitmap container to fit working default size
			scaleRatio = frameVo.photoVo.width/wantedWidth;
			workingBitmap.scaleX = workingBitmap.scaleY = scaleRatio;
			//trace("working bitmap. width : "+workingBitmap.width);
			
			// center this image will allow to use always centered system
			workingBitmap.x = -workingBitmap.width*.5;
			workingBitmap.y = -workingBitmap.height*.5;
		}		
			
		
	}
	
}