/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import be.antho.data.ResourcesManager;
	
	import comp.DraggableItem;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	import manager.DragDropManager;
	
	import mcs.navigator.groupCTA;

	public class PageNavigatorGroupCTA extends DraggableItem
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public var items : Vector.<PageNavigatorItem>; // items being moved (after a start drag)
		public var dropIndex : int = -1;	// current drop index (if on a dropable zone)
		
		private var view : groupCTA = new groupCTA();
		private var bg : Bitmap = new Bitmap();
		 
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PageNavigatorGroupCTA( pageItemWidth : Number, pageItemHeight : Number ) 
		{
			super(DragDropManager.TYPE_NAVIGATOR_DOUBLE,true);
			
			var shadowMargin : Number = 3;
			
			// construct
			view.labelBg.width = pageItemWidth * 2 - shadowMargin*2;
			view.labelBg.x = -pageItemWidth + shadowMargin ;
			view.labelBg.y = pageItemHeight *.5 - 13;
			view.label.width = view.labelBg.width;
			view.label.x = view.labelBg.x;
			view.label.y = view.labelBg.y + 2;
			view.label.htmlText = ResourcesManager.getString("edition.navigator.groupCTA");
			view.cacheAsBitmap = true;
			addChild(view);
			
			view.x = view.width*.5;
			view.y = view.height*.5-4;
			
			
			mouseChildren = false;
			buttonMode = true;
			
			// activate draggable functionnality
			dragEnabled = true;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 *
		 */
		public function dispose():void
		{
			if(parent) parent.removeChild(this);
		}
		
		/**
		 *
		 */
		public override function destroy():void
		{
			dispose();
			super.destroy();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		protected override function mouseDownHandler(e:MouseEvent):void
		{
			// draw bitmaps of pageItems
			var bmd : BitmapData = new BitmapData(items[0].width*2, items[0].height,true,0);
			bmd.draw(items[0]);//, null, null, null, new Rectangle(items[0].x, items[0].y, items[0].width*2, items[0].height));
			var m:Matrix = new Matrix();
			m.translate(items[0].WIDTH, 0);
			bmd.draw(items[1],m);
			
			if(bg.bitmapData) bg.bitmapData.dispose();
			bg.bitmapData = bmd;
			if(!bg.parent) addChildAt(bg,0);
			
			// start drag process
			super.mouseDownHandler(e);
			
			// remove bitmap of page items
			bg.bitmapData.dispose();
			if(bg.parent) bg.parent.removeChild(bg);
		}
		
		
	}
}