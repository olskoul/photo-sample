/***************************************************************
 PROJECT 		: tictac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.pageNavigator
{
	import be.antho.data.ResourcesManager;
	import com.greensock.TweenMax;
	
	import comp.DefaultTooltip;
	import comp.DraggableItem;
	
	import data.FrameVo;
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectVo;
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.LineScaleMode;
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	
	import library.pagenavigation.selection.view;
	
	import manager.DragDropManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	
	import mcs.navigator.navigatorPageItem;
	
	import mx.skins.Border;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.TextUtils;
	
	
	public class PageNavigatorItem extends DraggableItem
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// const
		private var tooltip:TooltipVo = new TooltipVo("tooltip.pagenavigator.item",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
		public var WIDTH : Number = NaN; 
		public static const HEIGHT : Number = 75;
		public var scaleRatio : Number = 1;
		
		// signals
		public const CLICKED : Signal = new Signal(PageNavigatorItem);
		public var pageVo : PageVo;
	
		// views
		//private var skin : navigatorPageItem = new navigatorPageItem();
		private var label:TextField;
		private var framesContainer:Sprite = new Sprite();
		private var selectedBorder : Sprite;
		private var dropHighLight : Sprite;
		
		// datas
		private var _selected : Boolean = false;
		private var frames:Vector.<PageNavigatorFrame> ;
		
		private var _draggable:Boolean = true;
		private var _lastFrameTarget : PageNavigatorFrame;
		
		private var coverPreview:Sprite;

		private var voCasted:PageCoverClassicVo;

		private var loader:Loader;
		private var preview:Bitmap = new Bitmap();


		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		
		public function PageNavigatorItem(_pageVo:PageVo) 
		{
			super(DragDropManager.instance.getDropTypeFromPageVo(_pageVo));
			isCustomDropArea = true; // page navigatorItem is a custom dropTargetArea
			usePixelCheck = false; // do not use pixel check for this
			
			pageVo = _pageVo;
			//addChild(skin);
			
			
			/*var pageBounds:Rectangle = (pageVo.isCover && pageVo.coverType==ProductsCatalogue.COVER_CUSTOM)?Infos.project.getCoverBounds():pageVo.bounds;
			
			if(!pageBounds || pageBounds.width == 0){
				Debug.warn("PageNavigatorItem constructor : page bounds is not set");
				return;	
			}
			
			scaleRatio = HEIGHT/pageBounds.height;
			//WIDTH = Math.round(pageBounds.width * scaleRatio);	
			WIDTH = pageBounds.width * scaleRatio;	

			if(	isNaN(WIDTH)
				|| isNaN(HEIGHT)
				|| WIDTH<0
				|| HEIGHT<0){
				Debug.warn("PageNavigatorItem constructor : Problem with width or height : ("+WIDTH+"x"+HEIGHT+")");
				return;	
			}*/

			setWIDTH();
			
			framesContainer.filters =[new DropShadowFilter(2,90,0,0.3)];
			framesContainer.graphics.beginFill(Colors.WHITE, 1);
			framesContainer.graphics.lineStyle(1,Colors.GREY_LIGHT);
			framesContainer.graphics.drawRect(0,0,WIDTH, HEIGHT);
			addChild(framesContainer);
			
			/*
			var theMask:Sprite =  new Sprite();
			theMask.graphics.beginFill(Colors.ORANGE, .3);
			theMask.graphics.lineStyle(0,Colors.GREY_LIGHT);
			theMask.graphics.drawRect(0,0,WIDTH, HEIGHT);
			addChild(theMask);
			framesContainer.mask = theMask;
			*/
			
			framesContainer.scrollRect = new Rectangle(0,0,WIDTH,HEIGHT); 
			
			if(pageVo is PageCoverClassicVo)
			{
				voCasted = pageVo as PageCoverClassicVo;
				//setupLoader
				loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onloadCoverPreviewHandler);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onloadError);
				loadCoverPreview();
			}			
			
			// create label
			label = TextUtils.createLabelWithTextFormat("", TextFormats.ASAP_BOLD(12,Colors.WHITE, TextFormatAlign.CENTER),0,HEIGHT+1,WIDTH);
			label.autoSize = TextFieldAutoSize.CENTER;
			setLabel();
			addChild(label);
			
			// selected border
			/*
			selectedBorder = new library.pagenavigation.selection.view();
			selectedBorder.width = WIDTH+4;
			selectedBorder.height = HEIGHT+4;
			*/
			
			selectedBorder = new Sprite();
			selectedBorder.graphics.lineStyle(3,0,1,true);
			selectedBorder.graphics.drawRect(0,0,WIDTH+2, HEIGHT+2);
			selectedBorder.x = -1;
			selectedBorder.y = -1;
			addChild(selectedBorder);
			selectedBorder.visible = false;
			//selectedBorder.alpha = 0;
			
			
			// listeners 
			buttonMode = true;
			mouseChildren = false;
			addEventListener(MouseEvent.ROLL_OVER, function():void{
				selectedBorder.visible = true;
				selectedBorder.alpha = .8;
				TweenMax.killTweensOf(selectedBorder);
				TweenMax.to(selectedBorder, 0,{tint:Colors.YELLOW});
				if(shouldIShowToolTip())
				showTooltip();
			}
				
			);
			addEventListener(MouseEvent.ROLL_OUT, function():void{
				TweenMax.killTweensOf(selectedBorder);
				TweenMax.to(selectedBorder, 0,{tint:Colors.GREEN});
				selectedBorder.alpha = 1;
				selectedBorder.visible = _selected;
				if(shouldIShowToolTip())
				hideTooltip();
			});
			
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			
			addEventListener(MouseEvent.CLICK, onClickHandler);
		
			PagesManager.CURRENT_EDITED_PAGE_CHANGED.add(checkSelection);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			// custom drag&drop
			customCheckHandler = onCustomDropCheck;
			customHighLight = onCustomDropHighlight;
			
			
			// construct preview
			construct();		
			
		}

		/**
		 * ------------------------------------ GETTER/SETTER -------------------------------------
		 */
		
		/**
		 * the frame selected on drag/Drop images directly to this page navigator
		 */ 
		public function get lastFrameDropTarget():PageNavigatorFrame
		{
			return _lastFrameTarget;
		}

		public function get draggable():Boolean
		{
			if(pageVo.isCalendar)
				return false;
			
			return _draggable;
		}
		
		public function set draggable(value:Boolean):void
		{
			_draggable = value;
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE
		////////////////////////////////////////////////////////////////
		/**
		 * Set WIDHT value
		 */
		private function setWIDTH():void
		{
			if(pageVo.isCover)
			{
				var test:PageVo = pageVo;
			}
			
			var pageBounds:Rectangle = (pageVo.isCover && pageVo.coverType==ProductsCatalogue.COVER_CUSTOM)?Infos.project.getCoverBounds():Infos.project.getPageBounds();//pageVo.bounds;
			
			if(!pageBounds || pageBounds.width == 0){
				Debug.warn("PageNavigatorItem constructor : page bounds is not set");
				return;	
			}
			
			scaleRatio = HEIGHT/pageBounds.height;
			//WIDTH = Math.round(pageBounds.width * scaleRatio);	
			WIDTH = pageBounds.width * scaleRatio;	
			
			if(	isNaN(WIDTH)
				|| isNaN(HEIGHT)
				|| WIDTH<0
				|| HEIGHT<0){
				Debug.warn("PageNavigatorItem constructor : Problem with width or height : ("+WIDTH+"x"+HEIGHT+")");
				return;	
			}
		}
		
		/**
		 * Condition to allow show tooltip
		 */
		private function shouldIShowToolTip():Boolean
		{
			return dragEnabled;
		}
		
		/**
		 * display/hide DRAG INFO tooltip
		 */
		protected function showTooltip( autoHide : Boolean = true):void
		{
			hideTooltip();
			DefaultTooltip.tooltipOnClip(this,tooltip, true, 150)
			if(autoHide) TweenMax.delayedCall(2, hideTooltip);
		}
		protected function hideTooltip():void
		{
			TweenMax.killDelayedCallsTo(hideTooltip);
			DefaultTooltip.killAllTooltips();
		}
		
		
		protected function loadCoverPreview():void
		{
			var vars:URLVariables = new URLVariables();
			vars.line1 = voCasted.line1;
			vars.line2 = voCasted.line2;
			vars.spine = voCasted.spine;
			vars.edition = voCasted.edition;
			vars.cover = voCasted.cover;
			vars.corners = voCasted.corners;
			vars.color = voCasted.color;
			
			var url : String = Infos.config.serverNormalUrl + "/" + "wizard/images/preview-cover.php?"+vars.toString();
			loader.load(new URLRequest(url));
		}
		
		private function onloadCoverPreviewHandler(e:Event):void
		{
			if(preview.bitmapData)
				preview.bitmapData.dispose();

			// make bitmap data at correctsize
			var loadedBmd : BitmapData = Bitmap(loader.content as Bitmap).bitmapData;
			var bmd : BitmapData = new BitmapData(WIDTH, HEIGHT, false, 0);
			var m:Matrix = new Matrix();
			m.translate(-96,-14);
			m.scale(WIDTH/409, HEIGHT/323);
			var r:Rectangle = new Rectangle(0,0,409,323);
			bmd.draw(loadedBmd,m,null,null,r,true);
			loadedBmd.dispose();
			
			preview.bitmapData = bmd;
			preview.width = WIDTH;
			preview.scaleY = preview.scaleX;
			preview.y = HEIGHT - preview.height >> 1;
			preview.alpha = 0;
			addChild(preview);
			addChild(selectedBorder);
			TweenMax.killTweensOf(preview);
			TweenMax.to(preview,1,{alpha:1});
			
		}
		
		protected function onloadError(event:ErrorEvent):void
		{
			Debug.warn("PageNavigatorItem.onLoadError : " + event.toString());
		}
		
		
		
		protected function addedToStageHandler(event:Event):void
		{
			
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			//Just to put it on top of the display list
			selected = false;
			checkSelection();
		}
		
		
		override public function mouseUpHandler(e:MouseEvent = null):void
		{
			checkSelection();
		}
		
		override protected function mouseDownHandler(e:MouseEvent):void
		{
			TweenMax.delayedCall(.2,doDrag);
		}
			
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Set text label
		 */
		public function setLabel(_label:String = ""):void
		{
			if(_label == "")
			{
				if(pageVo.index == 0) label.text = "cover";
				else label.text = ""+ pageVo.index;
			}
			else
				label.text = _label;
		}
		
		/**
		 * refresh view
		 */
		public function update():void
		{
			dispose();
			construct();
		}
		
		/**
		 * set selected or not
		 */
		public function set selected(value : Boolean):void
		{
			
			//Set value
			_selected = selectedBorder.visible = value;

			if(_selected && parent)
			{
				parent.addChild(this);
				TweenMax.killTweensOf(selectedBorder);
				TweenMax.to(selectedBorder,0,{tint:Colors.GREEN});
			}
				
		}
		
		/**
		 * destroy view, clean everything
		 */
		public override  function destroy():void
		{
			dispose();
			if(loader)
			{
				loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onloadCoverPreviewHandler);
				loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onloadError);
			}
			
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			removeEventListener(MouseEvent.CLICK, onClickHandler);
			PagesManager.CURRENT_EDITED_PAGE_CHANGED.remove(checkSelection);	
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			if(parent)
				parent.removeChild(this);
			
			super.destroy();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Start drag and drop process
		 */
		private function doDrag():void
		{
			selectedBorder.visible = true;
			TweenMax.killTweensOf(selectedBorder);
			TweenMax.to(selectedBorder,0,{tint:Colors.YELLOW});
			
			if(draggable)
				super.mouseDownHandler(null);
		}
		/**
		 * Construction of the view
		 * Diplay the area
		 * Checking for existing frames/texts/cliparts/backgrounds (empty or not)
		 */
		private function construct():void
		{
			//cacheAsBitmap = false;
			setWIDTH();
			
			frames = new Vector.<PageNavigatorFrame>;
			//Frames
			var frame : PageNavigatorFrame;
			for (var i:int = 0; i < pageVo.layoutVo.frameList.length; i++) 
			{
				var frameVo:FrameVo = pageVo.layoutVo.frameList[i] as FrameVo;
				/*
				if( frameVo.type == FrameVo.TYPE_BKG || !frameVo.isEmpty ){
				*/
				
				//Postcard Back => force special background Vo, so its always up to date
				if(frameVo.type == FrameVo.TYPE_BKG && pageVo.isPostCardBack)
					frameVo.isPostCardBackground = true;
				
					if(frameVo.type == FrameVo.TYPE_TEXT && !frameVo.isEmpty) frame = new PageNavigatorFrameText( frameVo, scaleRatio );
					else if(frameVo.type == FrameVo.TYPE_QR) frame = new PageNavigatorFrameQR( frameVo, scaleRatio );
					else frame = new PageNavigatorFramePhoto(frameVo, scaleRatio);
					
					//init
					frame.init();
					
					//Add frame
					framesContainer.addChild(frame);
					//Add frame to the list
					frames.push(frame);
					/*
				}
					*/
			}
			
			// set cache as bitmap
			//cacheAsBitmap = true;
			
			if(voCasted)
				loadCoverPreview();
			
		}
		
		/**
		 * Signal handler 
		 * Chek if the current edited page has the same PageVo
		 */
		private function checkSelection():void
		{
			selected = (PagesManager.instance.currentEditedPage == pageVo);
		}
		
		
		/**
		 * clean method
		 * removes all frames (destroy)
		 */
		private function dispose():void
		{
			//Frames
			for (var i:int = 0; i < frames.length; i++) 
			{	
				var frame:PageNavigatorFrame = frames[i] as PageNavigatorFrame;
				frame.destroy();
				frame.parent.removeChild(frame);
			}
			frames = null;
			
			if(coverPreview && contains(coverPreview))
			{
				removeChild(coverPreview);
				coverPreview = null;
			}
			
		}
		
		
		/**
		 * on click handler
		 */
		private function onClickHandler(e:MouseEvent):void
		{
			TweenMax.killDelayedCallsTo(doDrag);
			CLICKED.dispatch(this);
		}
		
		
		
		////////////////////////////////////////////////////////////////////////
		/// DRAG & DROP
		////////////////////////////////////////////////////////////////////////
		
		/**
		 * custom check on group drop
		 */
		private function onCustomDropCheck( dragItem : DraggableItem ):Boolean
		{	
			// reset last frame target found on drag/drop
			_lastFrameTarget = null;
			
			
			// if drop item is navigator item, we are not a custom anymore
			if( dragItem is PageNavigatorItem ) {
				showDropHighLight(new Point(framesContainer.width*.5,framesContainer.height*.5), framesContainer.width, framesContainer.height, 0);
				return true;
			}
			
			// check if mousex mousey is on a frame
			else{
				var frame:PageNavigatorFrame;
				var frameBounds: Rectangle;
				for (var i:int = frames.length-1; i >= 0; i--)  // start by the most in front !
				{
					frame = frames[i]; 
					//trace(frame.);
					frameBounds = new Rectangle(- frame.frameVo.width*scaleRatio*.5,-frame.frameVo.height*scaleRatio*.5, frame.frameVo.width*scaleRatio, frame.frameVo.height*scaleRatio);
					if((frame.frameVo.type == FrameVo.TYPE_PHOTO || frame.frameVo.type == FrameVo.TYPE_BKG) && frameBounds.contains(frame.mouseX, frame.mouseY) ) {//frame.hitTestPoint(frame.mouseX, frame.mouseY,true)){
						showDropHighLight(new Point(frame.x, frame.y), frameBounds.width, frameBounds.height, frame.frameVo.rotation);
						_lastFrameTarget = frame;
						return true;
					}
				}
				
				
			}
			
			return false;
		}
		
		/**
		 * custom stop highlight for draggable area
		 */
		private function onCustomDropHighlight(flag:Boolean, dragItem:DraggableItem):void
		{
			if(!flag) killDropHighlight();
		}
		
		/**
		 * remove dropHighlight
		 */
		private function showDropHighLight(pos:Point, w:Number, h:Number, r:Number = 0):void
		{
			if(!dropHighLight){
				dropHighLight = new Sprite();
				dropHighLight.graphics.beginFill(Colors.YELLOW, 0.5);
				dropHighLight.graphics.lineStyle(1,Colors.YELLOW,1,false,LineScaleMode.NONE);
				dropHighLight.graphics.drawRect(-10,-10,20, 20);
				dropHighLight.graphics.endFill();
				addChild(dropHighLight);
			}
			
			dropHighLight.width = w;
			dropHighLight.height = h;
			dropHighLight.rotation = r;
			dropHighLight.x = pos.x;
			dropHighLight.y = pos.y;
			dropHighLight.visible = true;
		}
		
		
		/**
		 * remove dropHighlight
		 */
		private function killDropHighlight():void
		{
			if(dropHighLight && dropHighLight.visible) dropHighLight.visible = false;
		}
		
		
		
	}
}