package view.edition
{
	import be.antho.bitmap.snapshot.SnapShot;
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	import comp.label.LabelTictac;
	
	import data.FrameVo;
	import data.Infos;
	import data.OverlayerVo;
	import data.PhotoVo;
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import manager.MeasureManager;
	import manager.PagesManager;
	
	import ordering.FrameExporter;
	
	import org.qrcode.QRCode;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	
	public class FrameQR extends FramePhoto
	{
		//
		private static const FULL_WIDTH_QR:Number = 1000;
		public var showToolTipError:Boolean = false;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRuCTOR
		////////////////////////////////////////////////////////////////

		private var qrObj:QRCode;
		private var resourceKey:String;

		
		public function FrameQR(frameVo:FrameVo)
		{
			super(frameVo);
			minSize = MeasureManager.CMToPixel(2.5);
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 * + remove linked frame reference
		 */
		override public function destroy():void
		{	
			super.destroy();
		}
		
		
		/**
		 * 2. Update frame with a new QR code (not used yet)
		 * -> Redrow frame with new generated QR Code 
		 */
		public override function update(photoVo:PhotoVo, keepPhotoRatio:Boolean = false):void
		{
			
		}
		
		
		/**
		 * Delete overlayer
		 * -> it also
		 */
		public override function deleteMe( withAnim : Boolean = false):void
		{
			DELETE.dispatch(this, withAnim);
		}
		
		
		/**
		 * inside zoom for frame overlayer does it on its linked frame
		 */
		public override function zoom ( amount:Number ):void
		{
			//no zoom for QR frame
		}
		/**
		 * when zoom is over
		 */
		public override function endZoom():void
		{
			//no zoom for QR frame
		}
		
		/**
		 * Move content inside cropped area
		 */
		public override function insideMove():void
		{	
			//no Inside move
		}
		
		/**
		 * Stop inside move
		 * > reset and clean movePreviewBitmap
		 */
		public override function stopInsideMove():void
		{
			//no Inside move
		}

		////////////////////////////////////////////////////////////////
		//	protected
		////////////////////////////////////////////////////////////////
		/**
		 * 1. simple init frame
		 * After creation in page area
		 */
		override public function init():void
		{
			// update state depending on photo vo
			updateState();
			
			renderFrame();
			
			/*if(frameVo.isEmpty) {
				saveTransformToolAllowance();
				frameReady();
			}
			else loadPhoto();*/
			loadPhoto();
			
			setListeners();
		}
		
		/**
		 * switch to correct state
		 * which can be : 
		 * 	1. empty -> no photo > we remove content sprite
		 *  2. with photo -> we remove cta and add content
		 *  3. with error -> we draw an error box
		 */
		override protected function updateState():void
		{	
			// create image mask
			drawFrameMask();
			
			// create content of not already done
			if(!content){
				content = new Sprite();
				image = new Sprite();
				workingBitmap = new Bitmap();
				image.addChild(workingBitmap);
				content.addChild(image);
			}
			
			// add content
			addChild(content);
			
			// create quality indicator
			if(!qualityIndicator) createQualityIndicator();
			
			
			// remove cta if still there
			if(cta && cta.parent) cta.parent.removeChild(cta);
	
			
		}
		
		/**
		 * load a photo // overriden flow from FramePhoto
		 * -> Generate QRCode bitmapdata and set it as WorkingBitmap
		 * -> So it act like a frame photo
		 */
		override protected function loadPhoto():void
		{
			generateQRCode();
			/*try{
				generateQRCode();
			}
			catch (err:Error) {
				Debug.log("FrameQr.generateQRCode Failed : " + err.toString());
			}*/
		}
		
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 * + render linked frame if existing
		 */
		override protected function renderFrame():void
		{
			// be sure no mask is set for this frame
			content.mask = null;
			if(masker && masker.parent) masker.parent.removeChild(masker);
			
			// content
			content.scaleX = content.scaleY = 1;
			image.scaleX = image.scaleY = frameVo.zoom;
			
			// apply position
			x = frameVo.x;
			y = frameVo.y;
			rotation = frameVo.rotation;
			
			// be sure no border nor shadow is set for this frame
			if(borderShape && borderShape.parent) borderShape.parent.removeChild(borderShape);
			
			// quality indicator
			super.updateQualityIndicator();

		}
		
		/**
		* check if quality used for image correspond to quality needed
		*/
		override protected function checkAndUpdateImageQuality():void
		{
			var displayWidth : Number = frameVo.width;
			var newResolution:String = "low";
			if (displayWidth > 450) newResolution = "medium";
			else if (displayWidth > 600) newResolution = "high";
			newResolution = "high";// TODO remove, use to check max quality, for now keep it like this, it's best for UX
			
			if(newResolution != workResolution) generatePhotoWithResolution(newResolution);
		}
		
		/**
		 * generate the photo with High, Medium or low resolution
		 * only working in closed system inside the image sprite
		 * we just update the working bitmap value, it doesnt affect anything in layout, zoom, crop
		 */
		override protected function generatePhotoWithResolution(newResolution:String):void
		{
			// select quality wanted width
			var wantedWidth:Number;
			if(newResolution == "high") wantedWidth = 720;
			else if (newResolution == "medium") wantedWidth = 500;
			else wantedWidth = 300;
			
			// generate bitmap with wanted quality width
			// QRCode generate bmd of 1000pixels
			var fullsizeWidth : int = frameVo.width/frameVo.zoom ;
			var fullsizeHeight : int =  frameVo.height/frameVo.zoom ;
			var loadedWidth:Number = basePhoto.width;
			var loadedHeight:Number = basePhoto.height;
			
			//draw with correct ratio
			var drawRatio : Number = wantedWidth/loadedWidth;
			var bmd : BitmapData = new BitmapData(loadedWidth*drawRatio, loadedHeight*drawRatio, true, 0x000000); //TODO: wathc out perf, when transparency set to true// TODO : check if we can implement in the current tic tac photo tansparent images (png transparency)
			var m:Matrix = new Matrix();
			m.scale(drawRatio, drawRatio);
			if(basePhoto) {
				bmd.draw(basePhoto, m, null, null, null, true);
				// checks if popart
				// if(frameVo.isPopart) bmd = Frame.popartBitmap(bmd, frameVo);
			}
						
			
			// kill current working bitmap
			if(workingBitmap.bitmapData) workingBitmap.bitmapData.dispose();
			workingBitmap.bitmapData = bmd;
			//workingBitmap.smoothing = true;
			
			// scale bitmap container to fit working fullSize
			var scaleRatio:Number = fullsizeWidth/wantedWidth;
			workingBitmap.scaleX = workingBitmap.scaleY = scaleRatio;
			
			// center this image will allow to use always centered system
			workingBitmap.x = -workingBitmap.width*.5;
			workingBitmap.y = -workingBitmap.height*.5;
			
			workResolution = newResolution;
		}	
		
		////////////////////////////////////////////////////////////////
		//	Privates
		////////////////////////////////////////////////////////////////
		
		/**
		 * Render a RED frame if there is an error
		 */
		private function renderError():void
		{
			
		}
		
		/**
		 * Generate A QRCODE bmd from a QRLink
		 * Set the workingBitmap
		 */
		private function generateQRCode():void
		{
			// security
			if(frameVo && frameVo.QRLink != "")
			{
				showToolTipError = false;
				// clean possible base photo
				clearBasePhoto();
				if(qrObj && qrObj.bitmapData)
					qrObj.bitmapData.dispose();
				
				qrObj = new QRCode();
				qrObj.encode(frameVo.QRLink);
				
				// create base photo
				basePhoto = new Bitmap(qrObj.bitmapData);
				
				//Make it a full size image
				var drawRatio : Number = FULL_WIDTH_QR/basePhoto.width;
				var bmd : BitmapData = new BitmapData(FULL_WIDTH_QR, FULL_WIDTH_QR, false, 0xffffff);
				var m:Matrix = new Matrix();
				m.scale(drawRatio, drawRatio);
				bmd.draw(basePhoto, m, null, null, null, true);
				basePhoto.bitmapData.dispose();
				basePhoto = new Bitmap(bmd);
				//Infos.stage.addChild(basePhoto);
			}
			else
			{
				qrError("FramePhoto > onPhotoLoaded > no Framevo or no QRlink > we show a red frame");
				showToolTipError = true;
				clearBasePhoto();
				
				var background:Sprite = new Sprite();
				background.alpha = 1;
				background.graphics.clear();
				
				var tintColor :Number = Colors.RED;
				background.graphics.beginFill( tintColor,1 );
				background.graphics.drawRect(0,0,400,400); // it will be scaled later
				background.graphics.endFill();
				
				basePhoto = new Bitmap(SnapShot.snap(background), "auto", true);
				
				background = null;
			}
			
			
			workResolution = null; 	// reset resolution info to force generation
			checkAndUpdateImageQuality(); 
			
			// render the frame
			renderFrame();
			
			// remove loader
			if(loadingBar && loadingBar.parent) loadingBar.parent.removeChild(loadingBar);
			
						
			// tell the world we are ready
			frameReady();
			
		}
		
		/**
		 * generate a bitmap data of current content
		 */
		override public function getContentBitmapData(transparency:Boolean = true, bkgColor:Number = 0x000000):BitmapData
		{
			/*var qualityScale : Number = 3;
			var MAX_SIZE : Number = 1000;
			var frameWidth : Number = frameVo.width;
			var frameHeight : Number = frameVo.height;
			if(frameWidth *qualityScale > MAX_SIZE) qualityScale = MAX_SIZE/frameWidth;
			if(frameHeight *qualityScale > MAX_SIZE) qualityScale = MAX_SIZE/frameHeight;
			
			var bmd : BitmapData = new BitmapData(basePhoto.width*qualityScale, basePhoto.height*qualityScale,transparency, bkgColor);
			var m:Matrix = new Matrix();
			//m.translate(content.width*.5, content.height*.5);
			m.scale(qualityScale,qualityScale);
			bmd.draw(basePhoto, m, null,null, null, true);*/
			var bmd:BitmapData = basePhoto.bitmapData.clone();
			return bmd;
		}
		
		
		/**
		 * QR Error function hanlder
		 * Delete the image is there is something wrong
		 */
		private function qrError(msg:String):void
		{
			Debug.warn("FrameQR > qrError : " + msg);
			deleteMe(false);
		}
		
	}
}