﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition 
{
	import com.bit101.components.Calendar;
	import com.bit101.components.ColorChooser;
	import com.bit101.components.HSlider;
	import com.bit101.components.ScrollPane;
	import com.bit101.components.Slider;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Orientation3D;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	import comp.FontSelector;
	import comp.button.SimpleButton;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	import data.FrameVo;
	import data.Infos;
	import data.TooltipVo;
	
	import flashx.textLayout.formats.VerticalAlign;
	
	import manager.CanvasManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	import manager.UserActionManager;
	
	import mcs.transformToolbar;
	import mcs.toolbar.btn.view;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.KeyboardManager;
	import utils.TextFormats;
	
	import view.edition.imageEditor.ImageEditorModule;
	import view.edition.pageNavigator.PageNavigator;
	import view.popup.PopupAbstract;



	/**
	 * TransformToolBar is the toolbar associated with the transform tool
	 * This toolbar can have multiple states depending on what we are editing
	 * Actions : 
	 * > zoom in/out image
	 * > flip image
	 * > move up/down image depth
	 * > rotate from 45° 
	 * 
	 * WARNING : For performance choice, it should only have one instance of this component.
	 * 
	 * 
	 * Button id:
	 * 
	 * 1- rotate
	 * 2- Flip
	 * 3- Depth up
	 * 4- Depth Down
	 * 5- Edit (Image)
	 * 6- Text
	 * 7- Border
	 * 8- Shadow
	 * 9- Poparts
	 * 10- masks
	 * 11- Text Background
	 * 
	 * t1- Increase text size
	 * t2- Reduce text size
	 * t3
	 */
	public class TransformToolBar extends Sprite
	{
		// signals
		public static const UPDATE_TRANSFORM_TOOLBAR_TEXTBTN_STATE : Signal = new Signal();
		public const POPART_CLICKED : Signal = new Signal();
		public const BORDER_CLICKED : Signal = new Signal();
		public const SHADOW_CLICKED : Signal = new Signal();
		public const MASKS_CLICKED : Signal = new Signal();
		public const ROTATION_STEP_CLICKED : Signal = new Signal();
		public const TEXT_BACKGROUND_CLICKED : Signal = new Signal();
		
		
		
		// alignement refs
		public static const HALIGNMENTS:Array = [{type:TextFormatAlign.LEFT,frame:9}, {type:TextFormatAlign.CENTER,frame:10},{type:TextFormatAlign.RIGHT,frame:8},{type:TextFormatAlign.JUSTIFY,frame:7}];
		public static const VALIGNMENTS:Array = [{type:VerticalAlign.TOP,frame:11},{type:VerticalAlign.MIDDLE,frame:12},{type:VerticalAlign.BOTTOM,frame:13}];
		
		// static
		private static var instance : TransformToolBar;
			
		// view
		private var skin:transformToolbar; // the skin retrieved from flash swc
		private var zoomSlider : Slider;
		private var deleteBtn : MovieClip;
		private var editionArea:EditionArea; // locale reference to the edition Area view
		private var buttons:Object = {};
		private var buttonsText:Object = {};
		private var buttonsFonts:Object = {};
		// the edited content
		private var frame:Frame;
		
		// data
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var zoom:MovieClip;

		private var bg:Sprite;

		private var textTool:MovieClip;

		private var fontSelected:Sprite;

		private var fontListContainer:ScrollPane;

		private var colorChooser:ColorChooser;

		private var fontSelector:FontSelector;

		private var fontSizeTxt:TextField;

		private var testQRCode:SimpleButton;
		
		public function TransformToolBar(editionArea:EditionArea) 
		{
			// check for instance
			if(instance) Debug.error("TransformToolBar must be instantiate only once");
			instance = this;
			
			//Reference to the edition area
			this.editionArea = editionArea;
			
			// create and init skin and ui
			skin = new mcs.transformToolbar();
			addChild(skin);
			
			//bg
			bg = skin.bg;
			//TweenMax.to(bg,0,{tint:Colors.YELLOW});
			
			// create slider
			zoom = skin.zoom;
			zoomSlider = new HSlider( zoom, 40,15, onZoomUpdate);	
			zoomSlider.setSize(70,8);
			zoomSlider.tick = 0.001;
			zoomSlider.CHANGE_OVER.add(onZoomEnd);
			
			//tooltip
			var tooltipVo:TooltipVo;
			var tooltipResourceKey:String = "tooltip.transformtool.";
			var resourceTooltipKeys:Array = [
			"rotate",
			"reflect",
			"depth.plus",
			"depth.minus",
			"edit.image",
			"text",
			"border",
			"shadow",
			"popart",
			"masks",
			"textBackground",
			"infoImage",
			"calendarFrameDragDrop"
			];
			
			
			
			// create zoom button
			tooltipVo = new TooltipVo(tooltipResourceKey+"zoom.plus",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
			ButtonUtils.makeButton(zoom.plus, onZoomPlusHandler,tooltipVo,Colors.GREEN);
			tooltipVo = new TooltipVo(tooltipResourceKey+"zoom.minus",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
			ButtonUtils.makeButton(zoom.minus, onZoomMinusHandler,tooltipVo,Colors.GREEN);
			
			
			// prepare buttons
			var btn:MovieClip;
			
			for (var i:int = 0; i < 13; i++) 
			{
				btn = new mcs.toolbar.btn.view();//skin.getChildByName('btn_'+(i+1)) as MovieClip;
				btn.icon.gotoAndStop((i+1));
				btn.id = (i+1);
				tooltipVo = new TooltipVo(tooltipResourceKey+resourceTooltipKeys[i],TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
				ButtonUtils.makeButton(btn, onButtonClick, tooltipVo);
				buttons[btn.id] = btn;
				if(i == 12)
					btn.enabled = false;
			}
			
			//Delete btn
			deleteBtn = skin.btn_delete;
			tooltipVo = new TooltipVo(tooltipResourceKey+"delete",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
			ButtonUtils.makeButton(deleteBtn,deleteFrame,tooltipVo);
			
			//Prepare text tool bar
			textTool = skin.textToolBar;
			skin.removeChild(textTool);
			
			//resource key (text tool bar)
			resourceTooltipKeys = [
				"text.font.size.plus",
				"text.font.size.minus",
				"",
				"text.font.bold",
				"text.font.italic",
				"text.font.underline",
				"text.align",
				"text.valign"
			];
			
			for (i = 0; i < 8; i++) 
			{
				btn = textTool.getChildByName('btn_'+(i+1)) as MovieClip;
				if(btn)
				{
				btn.icon.gotoAndStop((i+1));
				btn.id = String("t"+(i+1));
				tooltipVo = new TooltipVo(tooltipResourceKey+resourceTooltipKeys[i],TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
				ButtonUtils.makeButton(btn, onTextButtonClick, tooltipVo);
				buttonsText[btn.id] = btn;
				}
				//buttons[btn.id] = btn;
			}
			
			//Create font seletor
			fontSelector = new FontSelector(TextFormats.AVAILABLE_FONTS);
			fontSelector.x = 10;
			fontSelector.y = 8;
			textTool.addChild(fontSelector);
			fontSelector.FONT_CHOSEN.add(fontChosen);
						
			//color picker
			colorChooser = new ColorChooser(null,0,0,0x000000,colorChosen);
			colorChooser.OPEN.add(function(colorChoose:ColorChooser):void{EditionArea.backgroundClickEnabled = false});
			colorChooser.CLOSE.add(function():void{EditionArea.backgroundClickEnabled = true});
			colorChooser.x = textTool.getChildByName('btn_8').x + textTool.getChildByName('btn_8').width + 2;
			colorChooser.y = textTool.getChildByName('btn_8').y ;
			colorChooser.usePopup = true;
			colorChooser.popupAlign = ColorChooser.TOP;
			colorChooser.model = new Bitmap(Colors.PALETTE);
			textTool.addChild(colorChooser);
			
			//fontSize Input
			fontSizeTxt = textTool.fontSizeTxt;
			fontSizeTxt.restrict = "0-9";
			fontSizeTxt.maxChars = 2;
			fontSizeTxt.addEventListener(Event.CHANGE,fontSizeTxtChangeHandler);
			
			//QR Test link button
			testQRCode = SimpleButton.createGreyButton("transform.toolbar.qr.test.btn.label", onQRcodeTest);
			
			//Listener
			UPDATE_TRANSFORM_TOOLBAR_TEXTBTN_STATE.add(checkTextBtnState);
			
		}
		
		private function fontChosen(fontName:String):void
		{
			Debug.log("TransformToolbar > fontChosen");
			if(frame && frame is FrameText){
				FrameText(frame).setFont(fontName);
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
			}
		}
		
		private function colorChosen(e:Event):void
		{
			Debug.log("TransformToolbar > colorChosen");
			if(frame && frame is FrameText){
				FrameText(frame).setColor(colorChooser.value);
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
			//EditionArea.backgroundClickEnabled = true; // re-enable background clic
			}
		}
		
		private function fontSizeTxtChangeHandler(e:Event):void
		{
			Debug.log("TransformToolbar > fontSizeTxtChangeHandler");
			if(frame && frame is FrameText){
				FrameText(frame).setFontSize(int(fontSizeTxt.text)/frame.frameVo.zoom,false);
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);	
			}
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	ALIGNMENT HELPERS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * retrieve halign button frame corresponding to halign string 
		 */
		private function getHAlignButtonFrame( halign : String ):int
		{
			for (var i:int = 0; i < HALIGNMENTS.length; i++) 
			{
				if(HALIGNMENTS[i].type == halign)
					return HALIGNMENTS[i].frame;
			}
			
			return HALIGNMENTS[1].frame ;//center by default
		}
		/**
		 * retrieve halign index corresponding to halign string 
		 */
		private function getHAlignButtonIndex( halign : String ):int
		{
			for (var i:int = 0; i < HALIGNMENTS.length; i++) 
			{
				if(HALIGNMENTS[i].type == halign) return i;
			}
			
			return 1 ;//center by default
		}
		
		
		/**
		 * retrieve valign button frame corresponding to valign string 
		 */
		public static function getVAlignButtonFrame( valign : String ):int
		{
			for (var i:int = 0; i < VALIGNMENTS.length; i++) 
			{
				if(VALIGNMENTS[i].type == valign)
					return VALIGNMENTS[i].frame;
			}
			
			return VALIGNMENTS[0].frame ;//top by default
		}
		
		/**
		 * retrieve valign button index corresponding to valign string 
		 */
		public static function getVAlignButtonIndex( valign : String ):int
		{
			for (var i:int = 0; i < VALIGNMENTS.length; i++) 
			{
				if(VALIGNMENTS[i].type == valign) return i;
			}
			
			return 0
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/*
		* 
		*/
		public function stickToFrame( newFrame : Frame, noDefaultSkin : Boolean = false):void
		{	
			if(frame) unstick();
		
			skin.visible = !noDefaultSkin;
			
			// ref to current frame
			frame = newFrame;
			
			//switch state based on frame type
			var display:Boolean = setState();
			if(!display) return;
			
			//add child this on top of the edition area display list
			editionArea.addChild(this);
			
			// keyboard event
			KeyboardManager.DELETE.add(deleteFrame);
				
			
			// positionate
			positionate();
			
			// update slider values
			zoomSlider.setSliderParams(.1, 1.9, 1 ); // TODO : do not hardcode values here)
			
			// init listeners
			setListeners();
		}
		
		
		/**
		 * positionate toolbar just below the frame
		 */
		public function positionate(e:Event = null):void
		{
			//var frameBounds : Rectangle = frame.getRect(frame.parent);
			var barWidth : Number = bg.width;
			var barHeight : Number = bg.height * 3;//height; // HACK : textToolbar combobox can be really big and it's corrupting calculation
			var frameBounds : Rectangle = TransformTool.frameBounds;//frame.boundRectScaled; // TODO : take care of rotation here !
			if(!frameBounds) return;
			//var point:Point = new Point(frame.x,frame.y);
			
			
			//Global point
			/*
			point = frame.parent.localToGlobal(point);
			point = editionArea.globalToLocal(point);
			this.x = Math.round(point.x - barWidth *.5);
			this.y = Math.round(point.y + frameBounds.height/2 + 50 );
			
			//handle exception based on frametype
			switch(frame.frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					this.y = Math.round(point.y + frameBounds.height/2);
					//this.x = Math.round(point.x + frameBounds.width/2 - barWidth);
					break;
				
				case FrameVo.TYPE_TEXT:
					if(frame.frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
					{
						this.y = Math.round(point.y + frameBounds.width/2 + 30);
					}
					break;
			}
			*/
			
			this.x = Math.round(frameBounds.x +frameBounds.width*.5 - barWidth*.5);
			this.y = Math.round(frameBounds.y +frameBounds.height + 10);
			
			
			//check screen limits
			
			//right
			if( x+barWidth+editionArea.x > Infos.stage.stageWidth)
			{
				x = Infos.stage.stageWidth - editionArea.x - barWidth - 10;
			}
			//bottom
			if(y+barHeight > Infos.stage.stageHeight)
			{
				y = Infos.stage.stageHeight - barHeight - 50;
				
			}
			//left
			if(x < 0)
			{
				x = 0;
			}
			
			// positionate textToolbar
			if( textTool && textTool.parent ){
				textTool.y = (skin.visible)? bg.height : 0;
				textTool.x = 0;
				if( x + textTool.width + editionArea.x > Infos.stage.stageWidth-50 && skin.visible){
					textTool.x = Infos.stage.stageWidth - editionArea.x - textTool.width - x-50;
				}
			}
			
			
				
		}
		
		
		
		/**
		 * unstick the transform tool from a frame
		 */
		public function unstick(e:Event = null):void
		{
			if(parent) parent.removeChild(this);
			showTextTool(false);
			frame = null;
			KeyboardManager.DELETE.add(deleteFrame);
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Test link of generated QR code
		 */
		private function onQRcodeTest():void
		{
			if(frame && frame is FrameQR)
			{
				if(frame.frameVo.QRLink != "")
				{
					navigateToURL(new URLRequest(frame.frameVo.QRLink),"_blank");
				}
				else
				{
					PopupAbstract.Alert("QR Code",ResourcesManager.getString("edition.frame.qr.error"),false);
				}
			}
		}
		
		/**
		 * Set the tollbar state based on the frame type
		 * Hide or show tools
		 * return true if there is at least one action to do, otherwise we should not display the toolbar
		 */
		private function setState():Boolean
		{
			// by default, remove text tools
			showTextTool(false);
			var display:Boolean = true;
			
			switch (frame.frameVo.type)
			{
				case FrameVo.TYPE_PHOTO:
					if(frame.frameVo.isEmpty) {
						// for canvas projects, non movable frames should not be deletable so we remove the delete button
						if(Infos.isCanvas && frame.frameVo.fixed ) {
							setButtons([],false,false);
							display = false;
						}
						else setButtons([],false,true); //setButtons([1,2,3,4,5],true,true);
					}
					else if(!frame.frameVo.showBorder) setButtons([3,4,1,5,10,9,12],true,true); 
					else setButtons([3,4,1,5,7,10,8,9,12],true,true); //setButtons([1,2,3,4,5],true,true);
					break;
				
				case FrameVo.TYPE_TEXT:
					var frameCasted:FrameText = frame as FrameText;
					if(frameCasted.isEmpty)setButtons([1,3,4,6],false,true);
					else
					{
						if(!frameCasted.frameVo.isPageNumber)
						setButtons([1,3,4,6,11],false,true);
						else
							setButtons([1,3,4,6,11],false,false);
						
						if(frameCasted.isOutsideFrame) showTextTool(true);
					}
					checkTextBtnState();
					positionate();
					break;
				
				case FrameVo.TYPE_CLIPART:
					setButtons([3,4],false,true);
					break;
				
				case FrameVo.TYPE_BKG:
					if(frame.frameVo.photoVo != null){
						if(frame.frameVo.photoVo is BackgroundCustomVo) setButtons([5,9,12],true,true);  // there is a photo in the background
						else if(frame.frameVo.photoVo is BackgroundVo) setButtons([],false,true); 
						else setButtons([5,9,12],true,true);  // there is a photo in the background
					} 
					else {
						// if no photo, we can't do anything
						setButtons([],false,false);
						display = false;
					}
					
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					setButtons([3,4],true,true); //setButtons([1,2,3,4,5],true,true);
					break;
				
				case FrameVo.TYPE_CALENDAR:
					var cFrame : FrameCalendar = frame as FrameCalendar;
					
					// calendar has a photo frame linked to it
					if( cFrame.linkedPhotoFrame ) {
						setButtons([6,5,9],true,true);
					}	 
					
					// calendar has a text frame linked to it, we can edit it by double clicking or click on text button in toolbar
					else if(cFrame.linkedTextFrame) { // this means there is a text linked to calendar frame, we can edit it by double clicking or click on text button in toolbar
						setButtons([6,11,13,],false,true);
					}
					
					// no linked frames, be we may want to add some text as frame is editable
					else if(cFrame.frameVo.editable) setButtons([6,13],false,false); 
					
					// no edition possible for this frame
					else {
						setButtons([],false,false);
						display = false;
					}
					break;
				
				case FrameVo.TYPE_QR:
						setButtons([3,4],false,true,true);
					break;
			}
			
			return display;
		}
		
		/**
		 * set action listeners
		 */
		private function setButtons(buttonsToShow:Array, showZoom:Boolean = true, showDelete:Boolean = true, showQRTestBtn:Boolean = false):void
		{
			skin.removeChildren(0,skin.numChildren-1);
			var gap:Number = 7;
			var posx:Number = 0;
			
			//bg
			skin.addChild(bg);
			
			//zoom
			if(showZoom)
			{
				skin.addChild(zoom);
				zoom.x = gap + posx;
				posx = zoom.x + zoom.width;
			}
			
			//showQRTestBtn
			if(showQRTestBtn)
			{
				skin.addChild(testQRCode);
				testQRCode.x = gap + posx;
				testQRCode.y = gap + 3;
				posx = testQRCode.x + testQRCode.width;
			}
				
			//btns
			for (var i:int = 0; i < buttonsToShow.length; i++) 
			{
				var btn:MovieClip = buttons[buttonsToShow[i]];
				btn.x = (i==0)? gap+posx : 1+posx;
				btn.y = gap;
				posx = btn.x + btn.width;
				skin.addChild(btn);
				
				//infos image check
				if(buttonsToShow[i] == 12)
				{
					//Change tooltip content
					updateImageInfoContent(btn);
				}
				
				// popart check
				if(buttonsToShow[i] == 9){
					if(frame.frameVo.isPopart){
						btn.gotoAndStop(3); // 3 is selected yellow background
						// put a delay to be sure this is well positinated
						TweenMax.delayedCall(.1, function():void{POPART_CLICKED.dispatch();});// open automatically 
					}else{
						btn.gotoAndStop(1);
					}
				}
				
			}
			
			//delete
			if(showDelete)
			{
				skin.addChild(deleteBtn);
				deleteBtn.x = gap + posx;
				deleteBtn.y = gap;
				posx = deleteBtn.x + deleteBtn.width;
			}
			
			
			
			//adjustBg
			bg.width = posx + gap;
			bg.height = gap + deleteBtn.height + gap;
			
			
		}	
		
		
		/**
		 * Check if the button has to be activated or not
		 */
		private function updateImageInfoContent($btn:MovieClip):void
		{
			if(frame && frame.frameVo && frame.frameVo.photoVo)
			{
				var btn:MovieClip = new mcs.toolbar.btn.view();//skin.getChildByName('btn_'+(i+1)) as MovieClip;
				btn.icon.gotoAndStop(12);
				btn.id = 12;
				var tooltipVo:TooltipVo = TooltipVo.TOOLTIP_IMAGE_PREVIEW(frame.frameVo.photoVo,true);
				ButtonUtils.makeButton(btn, onButtonClick, tooltipVo);
				buttons[btn.id] = btn;
				btn.x = $btn.x;
				btn.y = $btn.y;
				if($btn.parent)
				{
					$btn.parent.addChild(btn);
					$btn.parent.removeChild($btn);
					$btn = null;
				}
			}
			
			
		}
		
		/**
		 * Check if the button has to be activated or not
		 */
		private function checkTextBtnState():void
		{
			var frameCasted:FrameText;
			
			if(frame is FrameText) frameCasted = frame as FrameText;
			else if (frame is FrameCalendar) frameCasted = (frame as FrameCalendar).linkedTextFrame;
			else return;
			
			// do we continue
			if(! frameCasted || frameCasted.isEmpty) return;
			
			var selection:Object = frameCasted.getSelectedText();
			var textFormat:TextFormat = frameCasted.getTextFormat(selection.begin,selection.end);
			
			for (var btnId:String in buttonsText) 
			{
				var btn:MovieClip = buttonsText[btnId];
				switch (btn.id)
				{
					case "t1" :
						break;
					
					case "t2" :
						break;
					
					case "t4" :
						btn.gotoAndStop((textFormat.bold)?2:1);
						break;
					
					case "t5" :
						btn.gotoAndStop((textFormat.italic)?2:1);
						break;
					
					case "t6" :
						btn.gotoAndStop((textFormat.underline)?2:1);
						break;
					
					case "t7" :
						btn.icon.gotoAndStop(getHAlignButtonFrame(textFormat.align));
						break;
					
					case "t8" :
						btn.icon.gotoAndStop(getVAlignButtonFrame(frame.frameVo.vAlign));
						break;
					
				}
			}
			
			//size 
			updateFontSizeText(selection);
			
			//color:
			colorChooser.value = textFormat.color as Number;
			
			//font
			if(textFormat.font)
			{
				//trace("Transformetoolbar> "+textFormat.font);
				fontSelector.setFont(textFormat.font);
			}
			
		}
		
		
		
		/**
		 * set action listeners
		 */
		private function setListeners():void
		{
			// listen to zoom slider
			//zoomSlider.addEventListener(
		}	
		
		/**
		 * on edit button clicked
		 */
		private function onButtonClick(e:MouseEvent):void
		{
			Debug.log("TransformToolbar > onButtonClick : ");
			var buttonId : int = (e.currentTarget as MovieClip).id;
			Debug.log(" >> "+buttonId);
			switch (buttonId){
				
				// MAKE INSIDE ROTATION (clockwise)
				case 1 :
					if(!frame) return;
					if((frame is FramePhoto))
					{
						(frame as FramePhoto).insideRotation();
						PageNavigator.RedrawPageByFrame(frame.frameVo);
						UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
					}
					else if((frame is FrameText))
					{
						ROTATION_STEP_CLICKED.dispatch();
					}
					
					break;
				
				// MAKE INSIDE FLIP
				case 2 :
					//frame.insideFlip();
					break;
				
				// DEPTH UP
				case 3 :
					frame.depthUp();
					break;
				
				// DEPTH DOWN
				case 4 :
					frame.depthDown();
					break;
				
				// EDIT IMAGE
				case 5 :
					Debug.log("TransformToolbar > edit Frame");
					//frame.edit();
					var refFrame : FramePhoto = (frame is FrameCalendar)? (frame as FrameCalendar).linkedPhotoFrame : frame as FramePhoto;
					
					// securities
					if(!refFrame || !refFrame.frameVo || !refFrame.frameVo.photoVo) return;
					
					// if image is temp image, we do not allow edition (for now)
					if(refFrame.frameVo.photoVo.tempID) PopupAbstract.Alert(ResourcesManager.getString("popup.imageEditor.tempImage.title"),ResourcesManager.getString("popup.imageEditor.tempImage.desc"),false);
					
					// otherwise we open image editor
					else {
						ImageEditorModule.EDITION_COMPLETE.add(onImageEditSuccess)
						ImageEditorModule.EDITION_CANCEL.add(onImageEditCancel);
						ImageEditorModule.instance.openAndEditFromFramePhoto( refFrame );}
					break;
				
				// START TEXT EDITION
				case 6 :
					//toggleTextToolBar();
					if(frame is FrameCalendar) {
						(frame as FrameCalendar).EDIT_TEXT.dispatch(frame);
						return;
					}
					else editText();
					//frame.DOUBLE_CLICK.dispatch(frame);
					break;
				
				// EDIT BORDER
				case 7 :
					BORDER_CLICKED.dispatch(); //(frame as FramePhoto).applyBorder();
					break;
				
				// MODIFY SHADOW
				case 8 :
					SHADOW_CLICKED.dispatch(); // (frame as FramePhoto).toggleShadow();
					break;
				
				// USE POPART EFFECT
				case 9 :
					POPART_CLICKED.dispatch();
					break;
				
				// USE MASK EFFECT
				case 10 :
					MASKS_CLICKED.dispatch();
					break;
				
				// USE FRAME TEXT BACKGROUND
				case 11 :
					TEXT_BACKGROUND_CLICKED.dispatch();
					break;
					
			}
			//trace("button clicked id = "+(e.currentTarget as MovieClip).id);
		}	
		
		/**
		 * on text edit button clicked (has static function for quick shortcut)
		 */
		public static function updateFontSizeText(selection:Object = null):void
		{
			instance.updateFontSizeText();
		}
		public function updateFontSizeText(selection:Object = null):void
		{
			if(!frame || !(frame is FrameText)) return;
			if(!selection){
				selection = getCurrentTextFrameSelection();
				if(!selection) return;
			}
			if(!fontSizeTxt) return;
			
			var textFormat:TextFormat = FrameText(frame).getTextFormat(selection.begin,selection.end);
			var textToSet:String = "" + Math.round(Number(textFormat.size) * frame.frameVo.zoom);
			fontSizeTxt.text = (textToSet != "null")?textToSet:"-";
			
			/* TODO update font size depending on frame zoom*/
		}
		private function getCurrentTextFrameSelection():Object
		{
			var frameCasted:FrameText;
			if(frame is FrameText) frameCasted = frame as FrameText;
			else if (frame is FrameCalendar) frameCasted = (frame as FrameCalendar).linkedTextFrame as FrameText;
			else return null;
			
			if(!frameCasted || frameCasted.isEmpty) return null;
			
			return frameCasted.getSelectedText();
		}
		
		/**
		 * on text edit button clicked
		 */
		private function onTextButtonClick(e:MouseEvent):void
		{
			var button : MovieClip = e.currentTarget as MovieClip
			var buttonId : String = button.id;
			switch (buttonId)
			{
				case "t1" :
					FrameText(frame).increaseText();
					//updateFontSizeText();
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
					break;
				
				case "t2" :
					FrameText(frame).decreaseText();
					//updateFontSizeText();
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
					break;
				
				case "t4" :
					FrameText(frame).boldText( textTool.getChildByName('btn_4') as MovieClip);
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
					break;
				
				case "t5" :
					FrameText(frame).italicText( textTool.getChildByName('btn_5') as MovieClip);
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
					break;
				
				case "t6" :
					FrameText(frame).underlineText( textTool.getChildByName('btn_6') as MovieClip);
					UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
					break;
				
				case "t7" :
					changeTextHAlign( button );
					break;
				
				case "t8" :
					changeTextVAlign( button )
					break;
				
			}
			//FrameText(frame).isEdited = true;
			checkTextBtnState();
		}	
		
		
		
		/**
		 *	next text hAlign
		 */
		private function changeTextHAlign( btn : MovieClip ):void
		{
			if(!frame is FrameText) {Debug.warn("TransformToolBar > changeTextHAlign can only be done on Frame Text");return; }
			var frameCasted : FrameText = frame as FrameText;
			var selection:Object = frameCasted.getSelectedText();
			var textFormat:TextFormat = frameCasted.getTextFormat(selection.begin, selection.end);
			var currentIndex : int = getHAlignButtonIndex( textFormat.align );
			
			if(currentIndex + 1 < HALIGNMENTS.length) currentIndex += 1;
			else currentIndex = 0;
			
			textFormat.align = HALIGNMENTS[currentIndex].type;
			btn.icon.gotoAndStop( currentIndex );
			
			frameCasted.applyTextFormat ( textFormat, selection.begin, selection.end );
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
		}
		
		/**
		 *	next text hAlign
		 */
		private function changeTextVAlign( btn : MovieClip ):void
		{
			if(!frame is FrameText) {Debug.warn("TransformToolBar > changeTextVAlign can only be done on Frame Text");return; }
			var frameCasted : FrameText = frame as FrameText;
			var currentIndex : int = getVAlignButtonIndex( frameCasted.frameVo.vAlign );
			
			if(currentIndex + 1 < VALIGNMENTS.length) currentIndex += 1;
			else currentIndex = 0;
			
			frameCasted.changeVAlign( VALIGNMENTS[currentIndex].type);
			btn.icon.gotoAndStop( currentIndex );
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_TEXT_UPDATE);
		}
		
		
		
		
		/**
		 * 
		 */
		private function editText():void
		{
			// open in outside editor
			TransformTool.editTextOutsideFrame();
			// display text tools
			//showTextTool(true);
		}
		
		/**
		 * Toogle text toolbar visibility
		 */
		private function toggleTextToolBar():void
		{
			if(frame is FrameCalendar) {
				(frame as FrameCalendar).EDIT_TEXT.dispatch(frame);
				return;
			}
			else if( frame is FrameText){
				if(frame.isEmpty) {
					(frame as FrameText).setFocus(true, true);
					showTextTool(true);
					return;
				} else if (!textTool.parent){
					(frame as FrameText).setFocus(true);
					showTextTool(true);
					return;
				}
			}
				
			if(textTool.parent) showTextTool(false);
			else showTextTool(true);
		}	
		
		/**
		 *	show/hide text tool
		 */
		public function showTextTool( flag:Boolean = true ):void
		{
			var textBtn : MovieClip =  buttons[6];
			// show text tool
			if( flag ){ 
				addChild(textTool);
				positionate();
				textBtn.gotoAndStop(2);
			// hide text tool
			}else{
				if(fontSelector) fontSelector.close();
				if(colorChooser) colorChooser.close();
				if(textTool.parent) textTool.parent.removeChild(textTool);
				textBtn.gotoAndStop(1);
			}
		}
		
		
		/**
		 * set action listeners
		 */
		private function deleteFrame(e:MouseEvent = null):void
		{
			if(!frame){
				Debug.warn("TransformToolbar > delete frame > no frame selected");
				return;
			}
			
			// clear image content first if frame is photo and not empty
			if( frame.frameVo.type == FrameVo.TYPE_BKG || 
				frame.frameVo.type == FrameVo.TYPE_CALENDAR || 
				( frame.frameVo.type == FrameVo.TYPE_PHOTO && !frame.frameVo.isEmpty ) || 
				( frame is FrameText && !frame.isEmpty )) 
			{
				if( frame.frameVo.isMultiple ) CanvasManager.instance.clearMultipleFrame(); // for canvas multiple frames
				else frame.clearMe();
			}
			
			// if frame is empty, or clipart, or overlayer, we delete it completely
			else frame.deleteMe();
		}	
		
		
		/**
		 * set zoom in/out handler
		 */
		private function onZoomUpdate(value:Slider):void
		{
			if(frame is FramePhoto) FramePhoto(frame).zoom(zoomSlider.value);
			else if( frame is FrameCalendar) FrameCalendar(frame).zoom(zoomSlider.value); // we zoom on the linked image
		}
		
		/**
		 * set zoom in/out handler
		 */
		private function onZoomEnd(value:Slider):void
		{
			if(frame is FramePhoto) FramePhoto(frame).endZoom();
			else if( frame is FrameCalendar) FrameCalendar(frame).endZoom(); // we zoom on the linked image
			
			zoomSlider.setSliderParams(.1, 1.9, 1 );
			PageNavigator.RedrawPageByFrame(frame.frameVo);
		}
		
		/**
		 * Zoom +
		 */
		private function onZoomPlusHandler(e:MouseEvent):void
		{
			if(frame is FramePhoto) FramePhoto(frame).zoom(zoomSlider.value + .1);
			else if(frame is FrameCalendar) FrameCalendar(frame).zoom(zoomSlider.value + .1);
			onZoomEnd(null);
		}
		

		
		/**
		 * Zoom -
		 */
		private function onZoomMinusHandler(e:MouseEvent):void
		{
			if(frame is FramePhoto) FramePhoto(frame).zoom(zoomSlider.value - .1);
			else if(frame is FrameCalendar) FrameCalendar(frame).zoom(zoomSlider.value - .1);
			onZoomEnd(null);
		}
		
		
		/**
		 * when image edit success
		 */
		private function onImageEditSuccess(frame:FramePhoto, newImageData : BitmapData, newImageName:String):void
		{
			Debug.log("TransformToolBar.onImageEditSuccess : frame is "+frame);
			if(frame && frame is FramePhoto) {
				frame.switchToModifiedPhotoVo(newImageName, newImageData);
				FrameVo.injectPhotoVoInFrameVo(frame.frameVo,frame.frameVo.photoVo);
				frame.update(null);
				// update used/not used items
				ProjectManager.instance.remapPhotoVoList();
			}
		}
		
		/**
		 * set zoom in/out handler
		 */
		private function onImageEditCancel():void
		{
			// does nothing 
		}
		
		
		
	}
	
}

