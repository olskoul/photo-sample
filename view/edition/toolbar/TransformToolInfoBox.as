package view.edition.toolbar
{
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	import library.tranform.tool.frame.infos.box;
	
	import utils.Colors;
	
	public class TransformToolInfoBox extends Sprite
	{
		private var view:MovieClip;
		private var label:TextField;
		private var bg:Sprite;
		private const MARGIN:int = 5;
		
		public function TransformToolInfoBox()
		{
			super();
			mouseEnabled = mouseChildren = false;
			view = new library.tranform.tool.frame.infos.box();
			label = view.label;
			bg = view.bg;
			label.autoSize = "left";
			label.multiline = false;
			addChild(view);
			
			TweenMax.to(bg,0,{alpha:.5, tint:Colors.GREEN});
		}
		
		/**
		 * 
		 * */
		public function update(value:String):void
		{
			label.text = value;
			label.x = MARGIN;
			bg.x = 0;
			bg.width = label.width + MARGIN*2;
		}
	}
}