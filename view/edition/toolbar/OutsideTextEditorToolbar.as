/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2013
 ****************************************************************/
package view.edition.toolbar
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	
	import be.antho.data.ResourcesManager;
	
	import comp.button.SimpleButton;
	
	import data.Infos;
	
	import library.close.btn;
	
	import manager.PagesManager;
	import manager.UserActionManager;
	
	import mcs.navigator.lib_groupDropGfx;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FrameCalendar;
	import view.edition.FrameText;
	import view.edition.TransformToolBar;
	import view.edition.pageNavigator.PageNavigator;
	
	public class OutsideTextEditorToolbar extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		public static const REFRESH_POSITION:Signal = new Signal();
		
		// view
		private var bg : Sprite;
		private var textBg : Sprite;
		private var okButton : SimpleButton;
		private var okAllButton : SimpleButton;
		private var deleteButton : SimpleButton;
		private var closeButton:Sprite;
		
		private var content:MovieClip;
		private var borders:Array = [];
		
		// data
		private var editionArea:EditionArea
		private var toolbar:TransformToolBar
		private var refFrame : FrameText;
		private var editFrame : FrameText;
		private var editFrameContainer : Sprite; // container of the frame to edit. It allows to scale this frame if it's too big to fit in screen !!
		private var calendarFrame : FrameCalendar;
		private var refFixedValue:Boolean = false; // value of frame "fixed" var before edition. It's needed to be true during edition but we need to reset it to original value at end of edition
		private const toolbarHeight : Number = 54;
		private const toolbarWidth : Number = 552;
		

		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		/**
		 * 
		 * @param $editionArea
		 * @param $transformToolbar
		 * 
		 */
		public function OutsideTextEditorToolbar( $editionArea : EditionArea, $transformToolbar:TransformToolBar ) 
		{
			editionArea = $editionArea;
			toolbar = $transformToolbar;
			
			// draw bg
			bg = new Sprite();
			bg.graphics.beginFill(Colors.BLUE_LIGHT, .8);
			bg.graphics.drawRect(0,0,10,10);
			bg.graphics.endFill();
			bg.mouseChildren = false;
			bg.mouseEnabled = true;
			
			// bg shadow
			var shadow : DropShadowFilter = new DropShadowFilter(3,90,0x000000,.3,10,10);
			bg.filters = [shadow];
			addChild(bg);
			
			// frame
			okButton = SimpleButton.createApplyButton( "common.apply", onApplyHandler)
			okAllButton = SimpleButton.createApplyToAllButton( "lefttab.backgrounds.applytoall", onApplyToAllHandler)
			deleteButton = SimpleButton.createPopupCancelButton( "common.delete", onDeleteHandler)
			closeButton = ButtonUtils.makeButton(new library.close.btn(), function(e:Event):void{onCloseHandler()});
			
			REFRESH_POSITION.add(refreshPosition);
			
			addChild(okButton);
			addChild(okAllButton);
			addChild(deleteButton);
			addChild(closeButton);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * add toolbar to a frame
		 */
		public function stickToFrame( newFrame : Frame ):void
		{	
		//	if(refFrame && refFrame == newFrame) return; // do nothing
			
			if(refFrame) unstick();
			
			// check calendar linked text frame
			if(newFrame is FrameCalendar && (newFrame as FrameCalendar).linkedTextFrame){
				calendarFrame = newFrame as FrameCalendar;
				refFrame = calendarFrame.linkedTextFrame;
				
			// check if frame is frame text
			} else if (newFrame is FrameText){
				refFrame = newFrame as FrameText;
			}
			else{
				Debug.warn( "OutsideTextEditorToolbar > stickToFrame > newFrame type not OK");
				return;
			}
			
			
			//make sure its visible
			visible = true;
			
			// set fixed layout
			refFixedValue = refFrame.frameVo.fixed;
			// refFrame.frameVo.fixed = true; // not sure this is needed // check if this is needed...
			
			if(!editFrameContainer){
				editFrameContainer = new Sprite;
				addChild(editFrameContainer);
			}
			
			//if(refFrame.frameVo.isEmpty) refFrame.frameVo.text = "this is a test";
			editFrame = new FrameText( refFrame.frameVo )// refFrame.frameVo.copy() );
			editFrame.isOutsideFrame = true;
			editFrame.frameVo.fixed = true;
			editFrame.init();
			if(refFrame.frameVo.text != "") editFrame.setHtmlText(refFrame.frameVo.text);
			editFrame.TEXT_CHANGE.add(textUpdated);
			editFrameContainer.addChild(editFrame);
			
			deleteButton.enabled = !refFrame.frameVo.isPageNumber;
			
			//add child this on top of the edition area display list
			editionArea.addChild(this);
			
			// positionate
			scaleToFitScreen();
			
			// set toolbar
			toolbar.stickToFrame(editFrame, true);
			
			//position toolbar
			positionateToolbar();
			
			refFrame.mouseEnabled = refFrame.mouseChildren = false;
			
			// focus on field!
			editFrame.setFocus(true,false,true);
		}
		
		/* *
		* positionate toolbar just below the frame
		*/
		public function positionateToolbar():void
		{
			toolbar.x = x + (bg.width - toolbarWidth)*.5;
			toolbar.y = y + bg.height;
		}
		
		/* *
		 * Scale to fit Screen
		 */
		public function scaleToFitScreen():void
		{
			// check if edition frame fits in screen
			var maxWidth : Number = editionArea.stage.stageWidth - editionArea.x - 200;
			var maxHeight : Number = editionArea.stage.stageHeight - editionArea.y - 250;
			var scaleValue : Number = 1; 
			
			// check framwe height
			if ( editFrame.frameVo.height > maxHeight ) scaleValue = maxHeight / editFrame.frameVo.height;
			if ( editFrame.frameVo.width*scaleValue > maxWidth ) scaleValue = maxWidth / editFrame.frameVo.width;
			editFrameContainer.scaleX = editFrameContainer.scaleY = scaleValue;
			
			//Update elements Position
			updateElementsPosition();
			
			// Center on screen
			centerOnScreen();
			
		}
		
		/* *
		*  Update elements Position
		*/
		public function updateElementsPosition():void
		{
			// update view background and content
			bg.width = editFrameContainer.width + 40;
			if(toolbarWidth > bg.width) bg.width = toolbarWidth;
			
			editFrameContainer.x = bg.width/2;
			editFrameContainer.y = 20 + editFrameContainer.height /2;
			
			//deleteButton.y = editFrameContainer.y + editFrameContainer.height*.5 + 15;
			deleteButton.y = editFrameContainer.y + editFrameContainer.height*.5 + 15;
			okButton.y = deleteButton.y;
			okAllButton.y = deleteButton.y;
			
			okAllButton.x = (bg.width - okAllButton.width)*.5;
			deleteButton.x = okAllButton.x - deleteButton.width - 10;
			okButton.x = okAllButton.x + okAllButton.width + 10;
			
			closeButton.x = bg.width-closeButton.width-3;
			closeButton.y = 3;
			
			bg.height = okButton.y + okButton.height + 20;
		}
		
		/* *
		*  Center on screen
		*/
		public function centerOnScreen():void
		{
			x = (stage.stageWidth - editionArea.x - bg.width)*.5;
			y = (stage.stageHeight - bg.height)*.5 - 100;
		}
		
		/**
		 * text edit update
		 */
		private function textUpdated( htmlText : String ):void
		{
			refFrame.frameVo.text = htmlText;
			refFrame.setHtmlText(htmlText);
		}
		
		
		/* *
		 * unstick the transform tool from a frame
		 */
		public function unstick(e:Event = null):void
		{
			
			// while unstick with calendar text frames check if the frame text edited is empty, if so, delete it
			//if ( calendarFrame && refFrame && refFrame.isEmpty ) calendarFrame.clearMe();
			if(refFrame) {
				refFrame.mouseEnabled = refFrame.mouseChildren = true;
				refFrame.frameVo.fixed = refFixedValue;
				
				if(!refFrame.isTextEdited())
					textUpdated("");
			}
			
			if(parent) parent.removeChild(this);
			if(editFrame){
				if(editFrame.parent) editFrame.parent.removeChild(editFrame);
				editFrame.destroy();
			}
			toolbar.unstick();
			editFrame = null;
			
			refFrame = null;
			calendarFrame = null;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		private function refreshPosition():void
		{
			if(editFrame || refFrame || calendarFrame)
			{
				updateElementsPosition();
				centerOnScreen();
				positionateToolbar();
			}
		}
		
		private function onApplyHandler(e:MouseEvent = null):void
		{
			Debug.log("OutsideTextEditor > onApplyHandler");
			if(!editFrame) { Debug.warn("OutsideTextEditor.onApplyHandler > no editFrame"); return; }
			if(!editFrame.frameVo) { Debug.warn("OutsideTextEditor.onApplyHandler > no editFrame.frameVo"); return; }
			
			if(editFrame.frameVo.text != "")textUpdated( editFrame.frameVo.text );
			
			
			var textFormatToApplyToAll:TextFormat = refFrame.getRegularTextFormat();
			if(textFormatToApplyToAll)
			{
				saveLastUsedFont(textFormatToApplyToAll);
			}
			
			// save for undo
			UserActionManager.instance.addAction( UserActionManager.TYPE_FRAME_TEXT_UPDATE );
			
			if(refFrame) refFrame.DESELECT.dispatch(refFrame);
				
		}
		
		private function onApplyToAllHandler(e:MouseEvent = null):void
		{
			Debug.log("OutsideTextEditor > onApplyToAllHandler");
			if(!editFrame) { Debug.warn("OutsideTextEditor.onApplyToAllHandler > no editFrame"); return; }
			if(!editFrame.frameVo) { Debug.warn("OutsideTextEditor.onApplyToAllHandler > no editFrame.frameVo"); return; }
				
			if(editFrame.frameVo.text != "")textUpdated( editFrame.frameVo.text );
			
			var isPageNumberFrame:Boolean = editFrame.frameVo.isPageNumber;
			var textFormatToApplyToAll:TextFormat = refFrame.getRegularTextFormat();
			
			if(textFormatToApplyToAll)
			{
				PagesManager.instance.applyTextFormatToAllText(textFormatToApplyToAll, refFrame.frameVo.vAlign, isPageNumberFrame);
				saveLastUsedFont(textFormatToApplyToAll, isPageNumberFrame);
			}
			
			
			
			if(refFrame) refFrame.DESELECT.dispatch(refFrame);
				
		}
		
		private function saveLastUsedFont($textFormat:TextFormat, isPageNumber:Boolean = false):void
		{
			if(isPageNumber)
			{
				TextFormats.LAST_PAGE_NUMBER_USED = $textFormat;
				return;
			}
			
			var obj:Object;
			obj = {};
			obj.font =$textFormat.font;
			obj.color = $textFormat.color;
			obj.italic =$textFormat.italic;
			obj.bold =$textFormat.bold;
			obj.underline =$textFormat.underline;
			
			Infos.project.lastUsedTextFormat = obj;
				
		}
		
		
		/**
		 * delete content
		 * if we are editing a text frame linked to calendar frame, we delete this frame and the linkage
		 */
		private function onDeleteHandler(e:MouseEvent = null):void
		{
			Debug.log("OutsideTextEditor > onDeleteHandler");
			// delete frame text
			if( calendarFrame ) {
				calendarFrame.clearMe();
				unstick();
			}
			else 
			{
				textUpdated( "" );
				if(refFrame) refFrame.DESELECT.dispatch(refFrame);
			}
			
			
		}
		private function onCloseHandler(e:MouseEvent = null):void
		{
			Debug.log("OutsideTextEditor > onCloseHandler");

			if(!refFrame.isTextEdited())
					textUpdated( "" );
				
			if(refFrame)refFrame.DESELECT.dispatch(refFrame);
			
			
		}
		
		

		
		
	}
}