
package view.edition.toolbar
{
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import be.antho.data.ResourcesManager;
	
	import comp.button.SimpleButton;
	
	import data.FrameVo;
	import data.TooltipVo;
	
	import library.toolbar.masks.content;
	
	import manager.PagesManager;
	import manager.UserActionManager;
	
	import mcs.toolbarBg;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FramePhoto;
	

	public class MaskToolBar extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// const
		private const CRSizeRef:Array = [0,5,10,15,20];	// corner radius in mm	
		
		// view
		private var bg : toolbarBg = new toolbarBg();
		private var container : HBox;
		private var view:MovieClip;
		private var maskNoneBtn:MovieClip;
		private var maskCircleBtn:MovieClip;
		private var maskCornerBtn:MovieClip;
		
		// data
		private var editionArea:EditionArea;
		private var frame:Frame;
		private var margin:Number;
		private var buttons : Vector.<MovieClip> = new Vector.<MovieClip>;

		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		
		public function MaskToolBar( $editionArea : EditionArea ) 
		{
			editionArea = $editionArea;
			var shadow : DropShadowFilter = new DropShadowFilter(3,90,0x000000,.3,10,10);
			bg.filters = [shadow];
			addChild(bg);
			
			// construct sliders
			margin = 10;
			
			// left area
			container = new HBox();
			container.spacing = 10;
			container.alignment = HBox.MIDDLE;
			container.x = margin/2;
			container.y = bg.y+margin/2;
			addChild(container);
			
			
			//btns
			var tooltipVo:TooltipVo;
			view = new library.toolbar.masks.content();
			maskNoneBtn = ButtonUtils.makeButton(view.maskNoneBtn,borderBtnClickHandler,new TooltipVo("toolbar.mask.noMask",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			maskCircleBtn = ButtonUtils.makeButton(view.maskCircleBtn,borderBtnClickHandler,new TooltipVo("toolbar.mask.circleMask",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			maskCornerBtn = ButtonUtils.makeButton(view.maskCornerBtn,borderBtnClickHandler,new TooltipVo("toolbar.mask.cornerRadiusMask",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			
			buttons.push(maskNoneBtn);
			buttons.push(maskCircleBtn);
			buttons.push(maskCornerBtn);
			
			setupBtn();
			container.addChild(view);
			
			//apply to all photo btn
			var applyToAllBtn:SimpleButton = SimpleButton.createApplyToAllButton( "lefttab.backgrounds.applytoall", applyMaskToAllFrame);
			container.addChild(applyToAllBtn);
			
			// applyBtn
			var applyBtn:SimpleButton = SimpleButton.createApplyButton("common.apply", apply); 
			container.addChild(applyBtn);
		
			upadteBG();
			TweenMax.delayedCall(1,container.draw);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * add toolbar to a frame
		 */
		public function stickToFrame( newFrame : Frame ):void
		{	
			if(frame) unstick();
			
			//make sure its visible
			visible = true;
			
			// ref to current frame
			frame = newFrame;
			
			//add child this on top of the edition area display list
			editionArea.addChild(this);
			
			// positionate
			positionate();
			
			// apply effect to frame
			if( !frame is FramePhoto) Debug.warn("Mask system can only be used on framePhoto types");
			
			checkBtnState();
		}
		
		
		/* *
		 * positionate toolbar just below the frame
		 */
		public function positionate(e:Event = null):void
		{
			var toolbarPoint:Rectangle = editionArea.toolBarPosition;
			var frameBounds : Rectangle = frame.boundRectScaled;
			var point:Point = new Point(frame.x - frame.frameVo.width*.5,frame.y);
			
			this.y = toolbarPoint.y+55;
			this.x = toolbarPoint.x + 200;
			
			if((this.x + this.width)>(stage.stageWidth-editionArea.x))
				this.x -= (this.x + this.width)-(stage.stageWidth-editionArea.x);
		}
		
		
		
		/* *
		 * unstick the transform tool from a frame
		 */
		public function unstick(e:Event = null):void
		{
			if(parent) parent.removeChild(this);
			frame = null;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Apply frame specification (border and shadow) to all frames
		 */
		private function applyMaskToAllFrame():void
		{
			PagesManager.instance.applyMaskToAllPhotos(frame.frameVo.mask, frame.frameVo.cr);
		}
		
		private function apply():void
		{
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			unstick();
		}
		
		/**
		 * setupBtn
		 */
		private function setupBtn():void
		{
			for (var i:int = 0; i < buttons.length; i++) 
			{
				var btn:MovieClip = buttons[i];
				btn.icon.gotoAndStop(i+1);
				btn.data = i;
			}
		}
		
		/**
		 * BTN border click handler
		 */
		private function borderBtnClickHandler(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			var maskType : Number = target.data;
			var cornerRadius : Number = (maskType == FrameVo.MASK_TYPE_CORNER_RADIUS)? 10 : NaN; 
			FramePhoto(frame).applyMask( maskType , cornerRadius );
			
			checkBtnState();
		}
		
		/**
		 * Active the right button when stick to a frame
		 */
		private function checkBtnState():void
		{
			//borders
			for (var i:int = 0; i < buttons.length; i++) 
			{
				var btn:MovieClip = buttons[i];
				TweenMax.killTweensOf(btn.bg);
				TweenMax.to(btn.bg,0,{tint:(btn.data == frame.frameVo.mask)?Colors.YELLOW:null});
			}
			
			// slider..
			
		}
		
		
		/**
		 * ADjust bg dimensions
		 */
		private function upadteBG():void
		{
			bg.width = container.width + margin;
			bg.height = container.height + margin;
		}
	}
}