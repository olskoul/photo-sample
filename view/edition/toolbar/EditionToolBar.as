/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.toolbar
{
	import com.bit101.components.HBox;
	import com.bit101.components.PushButton;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.flash_proxy;
	
	import be.antho.data.ResourcesManager;
	
	import comp.button.ToolbarButton;
	
	import data.FrameVo;
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.TooltipVo;
	
	import manager.PagesManager;
	import manager.PhotoSwapManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import org.qrcode.QRCode;
	
	import utils.Debug;
	import utils.VectorUtils;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FrameBackground;
	import view.edition.FrameCalendar;
	import view.edition.PageArea;
	import view.edition.PageCoverClassicArea;
	import view.popup.PopupAbstract;
	import view.popup.PopupDefault;
	import view.popup.PopupSimpleInput;

	public class EditionToolBar extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// refs
		private var editionArea : EditionArea;
		private var copiedFrames : Vector.<FrameVo>;
		
		// ui
		private var container : HBox;
		private var undoButton : ToolbarButton;
		private var redoButton : ToolbarButton;
		private var copyButton : ToolbarButton;
		private var cutButton : ToolbarButton;
		private var pasteButton : ToolbarButton;
		private var addTextButton : ToolbarButton;
		private var addPhotoButton : ToolbarButton;
		private var autoFillButton : ToolbarButton;
		private var calendarColorsButton : ToolbarButton;
		private var swapButton : ToolbarButton;
		private var gridBtn:ToolbarButton;
		private var QRCodeBtn:ToolbarButton;

		//toolbar
		private var calendarColors:CalendarColorToolbar;
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		
		public function EditionToolBar() 
		{
			container = new HBox(this);
			undoButton = new ToolbarButton(ToolbarButton.TYPE_UNDO, "tooltip.toolbar.button.undo", onUndoHandler);
			redoButton = new ToolbarButton(ToolbarButton.TYPE_REDO, "tooltip.toolbar.button.redo", onRedoHandler);
			copyButton = new ToolbarButton(ToolbarButton.TYPE_COPY, "tooltip.toolbar.button.copy", onCopyHandler);
			cutButton = new ToolbarButton(ToolbarButton.TYPE_CUT, "tooltip.toolbar.button.cut", onCutHandler);
			pasteButton = new ToolbarButton(ToolbarButton.TYPE_PASTE, "tooltip.toolbar.button.paste", onPasteHandler);
			addTextButton = new ToolbarButton(ToolbarButton.TYPE_ADD_TEXT, "tooltip.toolbar.button.addText", onAddTextHandler);
			addPhotoButton = new ToolbarButton(ToolbarButton.TYPE_ADD_PHOTO, "tooltip.toolbar.button.addPhoto", onAddPhotoHandler);
			autoFillButton = new ToolbarButton(ToolbarButton.TYPE_AUTO_FILL, "tooltip.toolbar.button.autoFill", onAutoFill);//,ResourcesManager.getString("edition.toolbar.btn.autofill.label"));
			calendarColorsButton = new ToolbarButton(ToolbarButton.CALENDAR_COLOR, "tooltip.toolbar.button.calendarColors", onCalendarColors);
			swapButton = new ToolbarButton(ToolbarButton.TYPE_SWAP, "tooltip.toolbar.button.swap", toggleSwap);
			gridBtn = new ToolbarButton(ToolbarButton.TYPE_GRID, "tooltip.toolbar.button.grid", nextGrid);
			QRCodeBtn = new ToolbarButton(ToolbarButton.TYPE_QR_CODE,null, onQRCode);
			QRCodeBtn.tooltip = TooltipVo.TOOLTIP_QR_CODE();
			
			container.addChild(undoButton);
			container.addChild(redoButton);
			container.addChild(copyButton);
			container.addChild(cutButton);
			container.addChild(pasteButton);
			container.addChild(swapButton);
			if(Infos.isCalendar)
			container.addChild(calendarColorsButton);
			container.addChild(gridBtn);
			container.addChild(addTextButton);
			container.addChild(addPhotoButton);
			container.addChild(autoFillButton);
			container.addChild(QRCodeBtn);
			
			
			PagesManager.CURRENT_EDITED_PAGE_CHANGED.add(update);
			PhotoSwapManager.SWAP_IMAGE_MODE.add(onSwapModeChange);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		public function destroy():void
		{
			editionArea = null;
			PagesManager.CURRENT_EDITED_PAGE_CHANGED.remove(update);
			PhotoSwapManager.SWAP_IMAGE_MODE.remove(onSwapModeChange);
			
			if(parent)
				parent.removeChild(this);
			removeChildren();
		}
		
		/**
		 *
		 */
		public function init($editionArea:EditionArea):void
		{
			editionArea = $editionArea;				
		}
		
		

		
		/**
		 * update toolbar depending on action manager
		 */
		public function update():void
		{
			if(PhotoSwapManager.instance.swapActive){
				copyButton.enabled = 
					cutButton.enabled =
					pasteButton.enabled = 
					addTextButton.enabled =
					redoButton.enabled = 
					undoButton.enabled = 
					autoFillButton.enabled = false;
				if(calendarColorsButton) calendarColorsButton.enabled = false;
				
			} else {
				// update buttons view
				if( editionArea && editionArea.selectedFrame 
					&& !( editionArea.selectedFrame is FrameBackground ) // we cannot copy/paste background frames for now.. 
					&& !( editionArea.selectedFrame is FrameCalendar ) // we cannot copy/paste calendar frame
					&& !( editionArea.selectedFrame.frameVo.uniqFrameLinkageID ) // we cannot copy/paste background frames with linkage for now
				){ 
					copyButton.enabled = true;
					cutButton.enabled = true;
				} else {
					copyButton.enabled = false;
					cutButton.enabled = false;
				}
				
				pasteButton.enabled = (copiedFrames != null)? true : false;
				addTextButton.enabled = !(PagesManager.instance.currentEditedPage is PageCoverClassicVo);
				undoButton.enabled = UserActionManager.instance.hasPreviousAction;
				redoButton.enabled = UserActionManager.instance.hasRedoAction;
				autoFillButton.enabled = true;

				if(calendarColorsButton) calendarColorsButton.enabled = lookForCalendarFrames();
				swapButton.enabled = PagesManager.instance.checkSwapPhotoState();
			}
		}
		
		private function lookForCalendarFrames():Boolean
		{
			if(PagesManager.instance.currentEditedPage)
			{
				if(PagesManager.instance.currentEditedPage.isCalendar)
					return true;
				else if(PagesManager.instance.currentEditedPage.hasCalendarFrame())
					return true;
				else
					return false;
			}
			return false;
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Show QR code popup
		 * */
		private function onQRCode(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > QR code BTN click handler");
			var qrCodePopup : PopupSimpleInput = new PopupSimpleInput("" , "popup.qr.code.title","popup.qr.code.desc", null, 150);
			qrCodePopup.open();
			qrCodePopup.OK.add(onQRCodeOK);
			
		}
		
		/**
		 * Popup QR code OK handler
		 * */
		private function onQRCodeOK(popup:PopupSimpleInput):void
		{
			Debug.log("EditionToolbar > QR code popup OK Handler");
			
			var link:String = popup.fieldValue;
			
			if(link == "")
				return;
			
			if(link.indexOf('://') == -1)
				link = 'http://' + link;
			
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			if(currentPage &&  currentPage.coverType != ProductsCatalogue.COVER_LEATHER)
			{
				PageArea.ADD_QR_TO_PAGE.dispatch(currentPage,link);
			}
		}
		
		/**
		 * Show / hide grid
		 * */
		private function nextGrid(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > toggle grid");
			editionArea.nextGrid();
			gridBtn.selected = (PageArea.GRID_COLS_NUMBER[PageArea.CURRENT_GRID_INDEX] != 0);
		}		
		
		/**
		 *
		 */
		private function onUndoHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > undo");
			UserActionManager.instance.undoAction();
		}
		
		/**
		 *
		 */
		private function onRedoHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > redo");
			UserActionManager.instance.redoAction();
		}
		
		/**
		 *
		 */
		private function onCopyHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > copy");
			copiedFrames = new Vector.<FrameVo>;
			var copiedFrame : FrameVo = editionArea.selectedFrame.frameVo;
			copiedFrames.push ( copiedFrame.copy() ) ; // copy frame selected
			
			// check if there are some linked frames
			if( copiedFrame.uniqFrameLinkageID )
			{
				var pageVo:PageVo = PagesManager.instance.getPageVoByFrameVo(copiedFrame);
				var linkedFrames : Vector.<FrameVo> = PagesManager.instance.findFramesVoByLinkage( copiedFrame.uniqFrameLinkageID, copiedFrame, pageVo );
				for (var i:int = 0; i < linkedFrames.length; i++) 
				{
					copiedFrames.push( linkedFrames[i].copy());
				}
			}
			
			// update
			update(); // add a create frame action here
		}
		
		/**
		 *
		 */
		private function onCutHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > cut");
			copiedFrames = new Vector.<FrameVo>;
			var copiedFrame : Frame = editionArea.selectedFrame;
			copiedFrames.push ( copiedFrame.frameVo.copy() ) ; // copy frame selected
			
			// check if there are some linked frames
			if( copiedFrame.frameVo.uniqFrameLinkageID )
			{
				var linkedFrames : Vector.<Frame> = PagesManager.instance.getFramesLinked( copiedFrame );
				for (var i:int = 0; i < linkedFrames.length; i++) 
				{
					copiedFrames.push( linkedFrames[i].frameVo.copy() );
					// delete linked frame from stage
					linkedFrames[i].deleteMe(); // TODO : beware this could remove linkage id, but as we work now with only two frames linked max together, this should not have any impact
				}
			}
			
			// delete from stage
			copiedFrame.deleteMe();
		}
		
		/**
		 *
		 */
		private function onPasteHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar.paste");
			var currentFrame : FrameVo;
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			if(currentPage && !(currentPage is PageCoverClassicVo))
			{
				copiedFrames.reverse(); // frame copied front to back, should be pasted back to front
				if( copiedFrames.length > 1 ) { // if multiple frames, generate new uniqID to link them together
					PagesManager.instance.linkFramesVoTogether(copiedFrames);
				}
				for (var i:int = 0; i < copiedFrames.length; i++) 
				{
					currentFrame = copiedFrames[i];
					// move it a bit so it's not perfectly in front of the other
					currentFrame.x +=15;
					currentFrame.y +=15;
					
					// check out of bounds !
					if(currentFrame.x > currentPage.bounds.width) currentFrame.x = currentPage.bounds.width*.5;
					if(currentFrame.y > currentPage.bounds.height) currentFrame.y = currentPage.bounds.height*.5;
					
					// add framevo to new page
					currentPage.layoutVo.frameList.push(currentFrame);
				}
			}
			
			// remove copied frame ref
			copiedFrames = null;	
			
			// notify update on page
			PageVo.PAGEVO_UPDATED.dispatch(currentPage);
			
			// save action in history (which updates toolbar also)
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_PASTE)//, currentFrame); // add a create frame action here
		}
		
		/**
		 * Add text frame to the currentEdited page
		 */
		private function onAddTextHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > add text");
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			if(currentPage &&  currentPage.coverType != ProductsCatalogue.COVER_LEATHER)
			{
				PageArea.ADD_TEXT_TO_PAGE.dispatch(currentPage);
			}
		}
		
		/**
		 * Add photo frame to the currentEdited page
		 */
		private function onAddPhotoHandler(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > add photo");
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			if(currentPage &&  currentPage.coverType != ProductsCatalogue.COVER_LEATHER)
			{
				PageArea.ADD_PHOTO_TO_PAGE.dispatch(currentPage);
			}
		}		
		
		/**
		 *
		 */
		private function onAutoFill(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > autoFill");
			PopupAbstract.Alert(ResourcesManager.getString("popup.autoFill.warning.title"),ResourcesManager.getString("popup.autoFill.warning.description"),true, autoFillContinueHandler);
		}
		private function autoFillContinueHandler(p:PopupAbstract):void
		{
			Debug.log("EditionToolbar > autoFill > continue");
			ProjectManager.instance.makeAutoFill();
		}
		
		
		/**
		 *
		 */
		private function toggleSwap(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > toggleSwap");
			PhotoSwapManager.instance.toggleSwapMode();
		}
		private function onSwapModeChange(flag:Boolean):void
		{
			swapButton.selected = flag;
			update();
		}
		
		
		
		/**
		 * Show Calendar color panel
		 */
		private function onCalendarColors(e:MouseEvent):void
		{
			Debug.log("EditionToolbar > onCalendarColors");
			
			if(!calendarColors)
			calendarColors = new CalendarColorToolbar(editionArea);

			
			if(calendarColors.parent)
			{
				calendarColors.close();
				return;
			}
			else
			{
				//positionate
				var point:Point = new Point();
				point.x = calendarColorsButton.x - 10;
				point.y = calendarColorsButton.y + calendarColorsButton.height;
				
				point = calendarColorsButton.parent.localToGlobal(point);
				point = stage.globalToLocal(point);
				//clipPos = globalToLocal(clipPos);

				if(lookForCalendarFrames())
				{
					calendarColors.currentPage = PagesManager.instance.currentEditedPage;
					calendarColors.open(point);
				}
				else
				{
					Debug.warn("Edition Toolbar > Calendars color is enabled when the current page is not a calendar -> fix this. Project is:"+Infos.project.docCode);
					if(calendarColors.parent)
					{
						calendarColors.close();
						return;
					}
				}
				
			}
		}		
		
	}
}