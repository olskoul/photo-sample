/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.toolbar
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.ColorChooser;
	import com.bit101.components.HBox;
	import com.bit101.components.HSlider;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	
	import be.antho.data.ResourcesManager;
	
	import comp.button.SimpleButton;
	import comp.checkbox.CheckboxTitac;
	import comp.label.LabelTictac;
	
	import manager.PagesManager;
	import manager.UserActionManager;
	
	import mcs.toolbarBg;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.EditionArea;
	import view.edition.FramePhoto;
	


	public class ShadowToolbar extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// const
		private const DEFAULT_DARK_COLOR:Number = 0x02a3e5;
		private const DEFAULT_LIGHT_COLOR:Number = 0x9dd6ef;
		private const DEFAULT_TRESHOLD:Number = 50;
		private const DEFAULT_BLACK_TRESHOLD:Number = 30;
		
		// view
		private var bg : toolbarBg = new toolbarBg();
		private var container : HBox;
		private var checkBox : CheckBox;
		private var checkLabel : LabelTictac;
		private var alphaSlider : HSlider;
		private var alphaLabel : LabelTictac;
		
		private var colorChooser : ColorChooser;
		private var colorChooserLabel : LabelTictac;
		private var applyToAllBtn:SimpleButton;
		
		// data
		private var editionArea:EditionArea;
		private var frame:FramePhoto;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var margin:Number;
		public function ShadowToolbar( $editionArea : EditionArea) 
		{
			editionArea = $editionArea;
			var shadow : DropShadowFilter = new DropShadowFilter(3,90,0x000000,.3,10,10);
			bg.filters = [shadow];
			addChild(bg);
			
			// construct sliders
			margin = 20;
			
			// left area
			container = new HBox();
			container.spacing = 15;
			container.alignment = HBox.MIDDLE;
			container.y = bg.y+margin/2;
			//container.x = bg.x+margin/2;*/
			addChild(container);
			
			
			//spacer //hack
			var spacer:Sprite = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,1,1);
			container.addChild(spacer);
			
			/*
			<key id="toolbar.textBackground.checkbox.label"><![CDATA[Use background]]></key>
				<key id="toolbar.textBackground.alpha.label"><![CDATA[Opacity]]></key>
				<key id="toolbar.textBackground.color.label"><![CDATA[Color]]></key>
			*/
			
			// add popart check
			checkBox = new CheckboxTitac(container,0,0, ResourcesManager.getString("toolbar.shadow.checkbox.label"), onToggle); 
			spacer = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,1,1);
			container.addChild(spacer);
			
			// alpha slider
			/*
			var optionContainer:VBox = new VBox(container);
			optionContainer.alignment = VBox.LEFT;
			alphaLabel = new LabelTictac(optionContainer, 0,0, ResourcesManager.getString("toolbar.textBackground.alpha.label"),11);
			alphaLabel.textFormatUpdate(TextFormats.ASAP_BOLD(12));
			alphaSlider = new HSlider(optionContainer, 0,0, onSliderChange);
			alphaSlider.minimum = 0;
			alphaSlider.maximum = 100;
			alphaSlider.value = 100;
			*/
			
			// color
			var colorContainer:HBox = new HBox(container);
			colorContainer.alignment = HBox.MIDDLE;
			colorChooserLabel = new LabelTictac(colorContainer, 0,0, ResourcesManager.getString("toolbar.shadow.color.label"),11);
			colorChooserLabel.textFormatUpdate(TextFormats.ASAP_BOLD(12));
			colorChooser = new ColorChooser(colorContainer, 0,0, Colors.BLACK, onPickerChange);
			colorChooser.popupAlign = ColorChooser.TOP;
			colorChooser.OPEN.add(function(colorChoose:ColorChooser):void{EditionArea.backgroundClickEnabled = false});
			colorChooser.CLOSE.add(function():void{EditionArea.backgroundClickEnabled = true});
			colorChooser.usePopup = true;
			colorChooser.model = new Bitmap(Colors.PALETTE);
			
			//apply to all photo btn
			applyToAllBtn = SimpleButton.createApplyToAllButton( "lefttab.backgrounds.applytoall", applyToAll);
			container.addChild(applyToAllBtn);
			var applyBtn:SimpleButton = SimpleButton.createApplyButton("common.apply", apply); 
			container.addChild(applyBtn);
			
			spacer = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,1,1);
			container.addChild(spacer);
			
			upadteBG();
			TweenMax.delayedCall(1,container.draw);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * add toolbar to a frame
		 */
		public function stickToFrame( newFrame : FramePhoto ):void
		{	
			if(frame) unstick();
			if(!newFrame)
			{
				unstick();
				Debug.warn("ShadowToolbar.stickToFrame : no frame...");
				return;
			}
		
			
			// TODO : check if this is needed for this background frame
			/*
			if(newFrame is FrameCalendar){
				newFrame = (newFrame as FrameCalendar).linkedFrame as FrameText;
				if(!newFrame) return;
			}
			*/
			
			
			//make sure its visible
			visible = true;
			
			// ref to current frame
			frame = newFrame;
			
			//add child this on top of the edition area display list
			editionArea.addChild(this);
			
			// positionate
			positionate();
			
			// apply effect to frame
			/*
			if( !frame is FrameText){
				Debug.warn("TextBackground toolbar can only be used on FrameText types");
				unstick();
				return;
			}
			*/
			
			if(frame.frameVo.shadow)
			{
				//alphaSlider.value = frame.frameVo.fillAlpha * 100;
				colorChooser.value = frame.frameVo.sCol;
			}
			frame.frameVo.shadow = true; // force shadow use directly
			checkBox.selected = frame.frameVo.shadow;
			onToggle();
		}
		
		
		/* *
		 * positionate toolbar just below the frame
		 */
		public function positionate(e:Event = null):void
		{
			var toolbarPoint:Rectangle = editionArea.toolBarPosition;
			
			// positionate
			this.y = toolbarPoint.y+55;
			
			var newX : Number = toolbarPoint.x; 
			if((newX + this.width)>(stage.stageWidth-editionArea.x)) newX -= (newX + this.width)-(stage.stageWidth-editionArea.x);
			this.x = newX;
			
		}
		
		
		
		/* *
		 * unstick the transform tool from a frame
		 */
		public function unstick(e:Event = null):void
		{
			if(parent) parent.removeChild(this);
			frame = null;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * ADjust bg dimensions
		 */
		private function upadteBG():void
		{
			bg.width = container.width;
			bg.height = 55;//container.height + margin*2;
		}
		
		/**
		 * Toggle background activation
		 */
		private function onToggle(e:Event = null):void
		{
			// modify 
			var flag : Boolean = checkBox.selected;
			//alphaLabel.enabled = 
			//alphaSlider.enabled = 
			colorChooserLabel.enabled = 
			//applyToAllBtn.enabled =
			colorChooser.enabled = flag;
			
			render();
		}
		
		/* *
		* 
		*/
		private function onSliderChange(slider : HSlider):void
		{	
			render();
		}
		
		/* *
		* 
		*/
		private function onPickerChange(e:Event):void
		{	
			render();
		}
		
		
		/**
		*  Update popart effect on frame
		*/
		private function render():void
		{	
			if(!frame){
				Debug.warn("TextBackgroundToolbar.render : No frame to edit");
				return;
			}
			
			var flag : Boolean = checkBox.selected;
			frame.frameVo.shadow = flag;
			if(flag){
				frame.frameVo.sCol = colorChooser.value;
				//frame.frameVo.fillAlpha = alphaSlider.value/100;
			}
			
			// update frame shadow
			frame.applyShadow();	
		}
		
		
		/**
		 * Apply frame specification (border and shadow) to all frames
		 */
		private function applyToAll():void
		{
			PagesManager.instance.applyShadowToAll(frame.frameVo.shadow, frame.frameVo.sCol );
		}
		
		private function apply():void
		{
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			unstick();
		}

		
		
	}
}