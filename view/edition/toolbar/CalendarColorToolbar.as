/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.toolbar
{
	
	
	import com.bit101.components.ColorChooser;
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import be.antho.data.ResourcesManager;
	
	import comp.PanelAbstract;
	import comp.button.SimpleButton;
	import comp.checkbox.CheckboxTitac;
	
	import data.Infos;
	import data.PageVo;
	
	import manager.CalendarManager;
	import manager.PagesManager;
	import manager.UserActionManager;
	
	import mcs.toolbar.calendarColor;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.EditionArea;

	public class CalendarColorToolbar extends PanelAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// const

		
		// view
		private var _view:MovieClip;
		private var margin:int = 10;
		private var container:VBox;
		private var colorChoosers:Vector.<ColorChooser> = new Vector.<ColorChooser>();
		private var transparentBgCheckBox:CheckboxTitac;
		
		// data
		private var editionArea:EditionArea
		private var initArray:Array;

		//public
		public var currentPage:PageVo
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var applyBtn:SimpleButton;
		private var applyToAllBtn:SimpleButton;

		private var separator:Sprite;

		private var closeButton:MovieClip;

		

		
		public function CalendarColorToolbar( $editionArea : EditionArea ) 
		{
			Debug.log("CalendarColorToolbar");
			//ref
			editionArea = $editionArea;
			
			//view
			Debug.log("CalendarColorToolbar > creating view");
			_view = new mcs.toolbar.calendarColor();
			addChild(_view);
			
			// container
			container = new VBox();
			/*container.y = _view.title.y + _view.title.height + margin;
			container.x = margin;*/
			_view.addChild(container);
			
			//title
			Debug.log("CalendarColorToolbar > set title autosize");
			_view.title.autoSize = "left";
			//setTitle();
						
			//Color Choosers
			initArray = ["month", "year", "border", "weekDay", "weekEndDay", "offSetDay", "dayBackground" ];
			
			
			for (var i:int = 0; i < initArray.length; i++) 
			{
				var id:String = initArray[i];
				
				//Add transparent checkbox before box
				if(id == "dayBackground")
				{
					separator = getSeparator();
					container.addChild(separator);
					
					transparentBgCheckBox = new CheckboxTitac(container,margin,0,ResourcesManager.getString("edition.toolbar.calendar.color.label.tranparentbg"),checkBoxHandler, true);
				}
				
				var box:HBox = new HBox(container, 0, 0);
				box.alignment = HBox.MIDDLE;
				var defaultColor:Number = Colors.BLACK;
				var colorChooser:ColorChooser = new ColorChooser(box,0,0,defaultColor,colorChosenHandler);
				colorChooser.id = id;
				colorChoosers.push(colorChooser);
				colorChooser.usePopup = true;
				colorChooser.popupAlign = ColorChooser.LEFT;
				colorChooser.model = new Bitmap(Colors.PALETTE);
				colorChooser.CLOSE.add(function():void{TweenMax.killDelayedCallsTo(unBlockClickOut);TweenMax.delayedCall(.3,unBlockClickOut)});
				colorChooser.OPEN.add(function(colorChoose:ColorChooser):void{blockOutClick = true;closeAllOtherColorChooser(colorChoose)});
				var label:Label = new Label(box,0,0,ResourcesManager.getString("edition.toolbar.calendar.color.label."+id));
			}
			
			Debug.log("CalendarColorToolbar > add separator");
			separator = getSeparator();
			container.addChild(separator);
			
			//closebtn
			Debug.log("CalendarColorToolbar > add close btn");
			closeButton = _view.closeBtn;
			ButtonUtils.makeButton(closeButton, close);
			
			
			//btn wrapper
			box = new HBox(container, 0, 0);
			box.alignment = HBox.MIDDLE;
			box.spacing = 5;
			
			applyToAllBtn = new SimpleButton("lefttab.backgrounds.applytoall", // label
				Colors.GREY_LIGHT, //over color
				applyToAllPages, //handler
				Colors.GREEN, //off color
				Colors.WHITE, //off color label
				Colors.GREEN); // over color label
			
			box.addChild(applyToAllBtn);
			box.addChild(new Sprite());
			
			applyBtn = new SimpleButton("lefttab.backgrounds.apply", // label
				Colors.GREY_LIGHT, //over color
				applyChanges, //handler
				Colors.GREEN, //off color
				Colors.WHITE, //off color label
				Colors.GREEN); // over color label
			
			
			box.addChild(applyBtn);
		}
		
		private function closeAllOtherColorChooser(colorChoose:ColorChooser):void
		{
			if(colorChoosers)
			{
				for (var i:int = 0; i < colorChoosers.length; i++) 
				{
					var cc:ColorChooser = colorChoosers[i];
					if(cc != colorChoose)
						cc.close(false);
				}
				
			}
		}
		
		private function getSeparator():Sprite
		{
			var sprite:Sprite = new Sprite();
			sprite.graphics.clear();
			sprite.graphics.beginFill(0,0);
			sprite.graphics.drawRect(0,0,10,20);
			sprite.graphics.lineStyle(1,Colors.GREY_DARK,.5);
			sprite.graphics.moveTo(0,10);
			sprite.graphics.lineTo(200,10);			
			return sprite;
		}
		
		private function applyToAllPages():void
		{
			//apply changes
			applyChanges(true);
			//apply then to all other pages
			PagesManager.instance.applyCalendarColorToAllPages(currentPage.customColors);
			//update page
			PageVo.PAGEVO_UPDATED.dispatch(currentPage);
			//close
			close();
		}
		
		private function unBlockClickOut():void
		{
			blockOutClick = false
		}
		
		private function setTitle():void
		{
			_view.title.text = ResourcesManager.getString("edition.toolbar.calendar.color.title") + " \n"+ getCurrentMonth();
		}	
		
		private function draw():void
		{
			container.y = _view.title.y + _view.title.height + margin;
			container.x = margin;
			container.draw();
			_view.bg.width = container.x + container.width+35;
			_view.bg.height =container.y + container.height+margin;
			//applyToAllBtn.x = margin;//_view.bg.width - applyToAllBtn.width >> 1;
			closeButton.x = _view.bg.width - closeButton.width - 10;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		/**
		 * Override open
		 */
		override public function open( $pos : Point ):void
		{
			super.open($pos);
			
			//update
			updatePanel();
			//listener
			//PagesManager.CURRENT_EDITED_PAGE_CHANGED.add(updatePanel);
		}
		
		
		/**
		 * Override Close 
		 */
		override public function close(e:Event = null):void
		{
			super.close(e);
			currentPage = null;
			
			//listener
			//PagesManager.CURRENT_EDITED_PAGE_CHANGED.remove(updatePanel);
		}
		////////////////////////////////////////////////////////////////
		//	PRIVATES 
		////////////////////////////////////////////////////////////////
		
		/**
		 * Update 
		 * Checks if current page , else close.
		 * Set title
		 * update colorchoosers 
		 * */
		private function updatePanel():void
		{
			if(!currentPage)
			{
				Debug.warn("CalendarColorToolBar > trying to update but current page is  "+currentPage+" -> fix this. Project is:"+Infos.project.docCode);
				close();
				return;
			}
			//set Title
			setTitle();
			
			//check boxes states
			transparentBgCheckBox.selected = (currentPage.customColors.dayBackground == -1);
			//getColorChooserById("dayBackground").enabled = !transparentBgCheckBox.selected;
			
			//update panel components
			updatePanelColors();
			//draw
			draw();
		}		
		
		/**
		 * update panel components must check the colors values from the currentPage and apply them to the related colorchoosers and checkboxes
		 * */
		private function updatePanelColors():void
		{			
			for (var i:int = 0; i < colorChoosers.length; i++) 
			{
				var colorChooser:ColorChooser = colorChoosers[i];
				var id:String = colorChooser.id;
				colorChooser.value = (currentPage.customColors[id] != -1)?currentPage.customColors[id]:Colors.GREY_LIGHT;
			}
		}
		
		/**
		 * Apply changes to pageVo and reload the page
		 * */
		private function applyChanges(isBeforeApplyToAll:Boolean = false):void
		{
			if(!currentPage){
				Debug.warn("CalendarColorToolbar.applyChanges : Currentpage is null, we abord");
				return;
			}
			
			try
			{
				//apply values from colors chooser to Vo
				for (var i:int = 0; i < colorChoosers.length; i++) 
				{
					var colorChooser:ColorChooser = colorChoosers[i];
					var id:String = colorChooser.id;
					if(id == "dayBackground" && transparentBgCheckBox.selected)
					currentPage.customColors[id] = -1;//colorChooser.value;
					else
					currentPage.customColors[id] = colorChooser.value;
				}
				
				if(!isBeforeApplyToAll)
				{
					//update page
					PageVo.PAGEVO_UPDATED.dispatch(currentPage);
					//undo redo
					UserActionManager.instance.addAction(UserActionManager.TYPE_COLOR_CHANGE);
					
					//close
					close();
				}
			}
			catch(e:Error)
			{
				Debug.warn("CalendarColorToolbar.applyChanges : "+e.message);
			}
		}
		
		
		private function colorChosenHandler(e:Event):void
		{	
			if(e.target == getColorChooserById("dayBackground"))
				transparentBgCheckBox.selected = false;
			
			//applyChanges();
		}
		
		private function checkBoxHandler(e:MouseEvent):void
		{	
			//applyChanges(true);
			
		}
		
		private function getColorChooserById(id:String):ColorChooser
		{
			for (var i:int = 0; i < colorChoosers.length; i++) 
			{
				var cc:ColorChooser = colorChoosers[i];
				if(cc.id == id)
					return cc;
			}
			return null;	
		}
		
		private function getCurrentMonth():String
		{
			if(currentPage.monthIndex != -1)
				return CalendarManager.instance.getMonthWithOffSetYear(currentPage.monthIndex,true);
			return ResourcesManager.getString("edition.toolbar.calendar.color.label.global");
		}	
		
		
	}
}