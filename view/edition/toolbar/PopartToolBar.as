/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition.toolbar
{
	import be.antho.data.ResourcesManager;
	
	import com.bit101.components.CheckBox;
	import com.bit101.components.ColorChooser;
	import com.bit101.components.HBox;
	import com.bit101.components.HSlider;
	import com.bit101.components.Knob;
	import com.bit101.components.Label;
	import com.bit101.components.Text;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	
	import comp.checkbox.CheckboxTitac;
	import comp.label.LabelTictac;
	
	import data.Infos;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mcs.toolbarBg;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FrameCalendar;
	import view.edition.FramePhoto;

	public class PopartToolBar extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// const
		private const DEFAULT_DARK_COLOR:Number = 0x02a3e5;
		private const DEFAULT_LIGHT_COLOR:Number = 0x9dd6ef;
		private const DEFAULT_TRESHOLD:Number = 50;
		private const DEFAULT_BLACK_TRESHOLD:Number = 30;
		
		// view
		private var bg : toolbarBg = new toolbarBg();
		private var title : Label;
		private var popartCheck : CheckBox;
		private var container : HBox;
		private var tresholdLabel : LabelTictac;
		private var tresholdSlider : HSlider;
		private var blackTresholdLabel : LabelTictac;
		private var blackTresholdSlider : HSlider;
		private var lightColorLabel  :LabelTictac;
		private var lightColorPicker : ColorChooser;
		private var darkColorLabel  :LabelTictac;
		private var darkColorPicker : ColorChooser;
		
		// data
		private var editionArea:EditionArea
		private var frame:Frame;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var margin:Number;
		public function PopartToolBar( $editionArea : EditionArea ) 
		{
			editionArea = $editionArea;
			var shadow : DropShadowFilter = new DropShadowFilter(3,90,0x000000,.3,10,10);
			bg.filters = [shadow];
			addChild(bg);
			
			// construct sliders
			margin = 20;
			
			// left area
			container = new HBox();
			container.spacing = 15;
			container.alignment = HBox.MIDDLE;
			container.y = bg.y+margin/2;
			//container.x = bg.x+margin/2;*/
			addChild(container);
			
			//spacer //hack
			var spacer:Sprite = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,1,1);
			container.addChild(spacer);
			
			// add popart check
			popartCheck = new CheckboxTitac(container,0,0, ResourcesManager.getString("toolbar.popart.checkbox.label"), onPopartToggle); 
			
			spacer = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,1,1);
			container.addChild(spacer);
			
			// sliders
			var optionContainer:VBox = new VBox(container);
			optionContainer.alignment = VBox.LEFT;
			tresholdLabel = new LabelTictac(optionContainer, 0,0, ResourcesManager.getString("toolbar.popart.treshold"),11);
			tresholdLabel.textFormatUpdate(TextFormats.ASAP_BOLD(12));
			tresholdSlider = new HSlider(optionContainer, 0,0, onSliderChange);
			tresholdSlider.minimum = 0;
			tresholdSlider.maximum = 100;
			tresholdSlider.value = DEFAULT_TRESHOLD
			
			optionContainer = new VBox(container);
			optionContainer.alignment = VBox.LEFT;
			blackTresholdLabel = new LabelTictac(optionContainer, 0,0, ResourcesManager.getString("toolbar.popart.blacktreshold"),11);
			blackTresholdLabel.textFormatUpdate(TextFormats.ASAP_BOLD(12));
			blackTresholdSlider = new HSlider(optionContainer, 0,0, onSliderChange);
			blackTresholdSlider.minimum = 0;
			blackTresholdSlider.maximum = 100;
			blackTresholdSlider.value = DEFAULT_BLACK_TRESHOLD
			
			var colorContainer:HBox = new HBox(container);
			colorContainer.alignment = HBox.MIDDLE;
			lightColorLabel = new LabelTictac(colorContainer, 0,0, ResourcesManager.getString("toolbar.popart.lightcolor"),11);
			lightColorLabel.textFormatUpdate(TextFormats.ASAP_BOLD(12));
			lightColorPicker = new ColorChooser(colorContainer, 0,0, DEFAULT_LIGHT_COLOR, onPickerChange);
			lightColorPicker.popupAlign = ColorChooser.TOP;
			lightColorPicker.OPEN.add(function(colorChoose:ColorChooser):void{EditionArea.backgroundClickEnabled = false});
			lightColorPicker.CLOSE.add(function():void{EditionArea.backgroundClickEnabled = true});
			lightColorPicker.usePopup = true;
			lightColorPicker.model = new Bitmap(Colors.PALETTE);
			
			colorContainer = new HBox(container);
			colorContainer.alignment = HBox.MIDDLE;
			darkColorLabel = new LabelTictac(colorContainer, 0,0, ResourcesManager.getString("toolbar.popart.darkcolor"),11);
			darkColorLabel.textFormatUpdate(TextFormats.ASAP_BOLD(12));
			darkColorPicker = new ColorChooser(colorContainer, 0,0, DEFAULT_DARK_COLOR, onPickerChange);
			darkColorPicker.popupAlign = ColorChooser.TOP;
			darkColorPicker.OPEN.add(function(colorChoose:ColorChooser):void{EditionArea.backgroundClickEnabled = false});
			darkColorPicker.CLOSE.add(function():void{EditionArea.backgroundClickEnabled = true});
			darkColorPicker.usePopup = true;
			darkColorPicker.model = new Bitmap(Colors.PALETTE);
			
			spacer = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,1,1);
			container.addChild(spacer);
			
			upadteBG();
			TweenMax.delayedCall(1,container.draw);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * add toolbar to a frame
		 */
		public function stickToFrame( newFrame : Frame ):void
		{	
			if(frame) unstick();
			
			// check if calendar frame and has a linked photoframe
			if(newFrame is FrameCalendar){
				newFrame = (newFrame as FrameCalendar).linkedPhotoFrame;
				if(!newFrame) return;
			}
			
			//make sure its visible
			visible = true;
			
			// ref to current frame
			frame = newFrame;
			
			//add child this on top of the edition area display list
			editionArea.addChild(this);
			
			// positionate
			positionate();
			
			// apply effect to frame
			if( !frame is FramePhoto) Debug.warn("Popart system can only be used on framePhoto types");
			
			if(frame.frameVo.isPopart)
			{
				tresholdSlider.value = frame.frameVo.PopArtTreshold;
				blackTresholdSlider.value = frame.frameVo.PopArtBlackTreshold;
				lightColorPicker.value = frame.frameVo.PopArtLightColor;
				darkColorPicker.value = frame.frameVo.PopArtDarkColor;
			}
			frame.frameVo.isPopart = true;
			popartCheck.selected = frame.frameVo.isPopart;
			onPopartToggle();
			// init listeners
			//setListeners();
		}
		
		
		/* *
		 * positionate toolbar just below the frame
		 */
		public function positionate(e:Event = null):void
		{
			var toolbarPoint:Rectangle = editionArea.toolBarPosition;
			//var frameBounds : Rectangle = frame.boundRectScaled;
			//var point:Point = new Point(frame.x - frame.frameVo.width*.5,frame.y);
			
			//Global point
			/*point = frame.parent.localToGlobal(point);
			point = editionArea.globalToLocal(point);
			this.x = Math.round(point.x - this.width);
			this.y = Math.round(point.y - this.height*.5 );*/
			
			this.y = toolbarPoint.y+55;
			this.x = toolbarPoint.x;
			
			if((this.x + this.width)>(stage.stageWidth-editionArea.x))
				this.x -= (this.x + this.width)-(stage.stageWidth-editionArea.x);
			
			/*
			var point:Point = new Point(frame.x,frame.y);
			
			//Global point
			point = frame.parent.localToGlobal(point);
			point = editionArea.globalToLocal(point);
			this.x = Math.round(point.x - this.width*.5);
			this.y = Math.round(point.y + frameBounds.height/2 + 50 );
			
			//check screen limits
			var bounds:Rectangle = this.getBounds(Infos.stage);
			//right
			if(bounds.right > Infos.stage.stageWidth)
			{
				var offset:Number = stage.stageWidth - bounds.right;
				x += offset;
			}
			//bottom
			if(bounds.bottom > Infos.stage.stageHeight)
			{
				y = point.y - frameBounds.height/2 - height;
			}
			//left
			if(bounds.left < 0)
			{
				x = -editionArea.x;
			}
			*/
		}
		
		
		
		/* *
		 * unstick the transform tool from a frame
		 */
		public function unstick(e:Event = null):void
		{
			if(parent) parent.removeChild(this);
			frame = null;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * ADjust bg dimensions
		 */
		private function upadteBG():void
		{
			bg.width = container.width;
			bg.height = 55;//container.height + margin*2;
		}
		
		/**
		 *
		 */
		private function onPopartToggle(e:Event = null):void
		{
			// modify 
			var flag : Boolean = popartCheck.selected;
			tresholdLabel.enabled = 
			tresholdSlider.enabled = 
			blackTresholdLabel.enabled = 
			blackTresholdSlider.enabled =
			lightColorLabel.enabled =
			lightColorPicker.enabled =
			darkColorLabel.enabled =
			darkColorPicker.enabled = flag;
			
			updatePopartEffect();
		}
		
		/* *
		* 
		*/
		private function onSliderChange(slider : HSlider):void
		{	
			updatePopartEffect();
		}
		
		/* *
		* 
		*/
		private function onPickerChange(e:Event):void
		{	
			updatePopartEffect();
		}
		
		
		/**
		*  Update popart effect on frame
		*/
		private function updatePopartEffect():void
		{	
			if(!frame){
				Debug.warn("PopartToolBar > updatePopartEffect > No frame to edit");
				return;
			}
			
			var flag : Boolean = popartCheck.selected;
			if(flag){
				frame.frameVo.PopArtTreshold = tresholdSlider.value;
				frame.frameVo.PopArtBlackTreshold = blackTresholdSlider.value;
				frame.frameVo.PopArtLightColor = lightColorPicker.value;
				frame.frameVo.PopArtDarkColor = darkColorPicker.value;
			}
			(frame as FramePhoto).applyPopArt(flag);	
		}
		
		
		

		
		
	}
}