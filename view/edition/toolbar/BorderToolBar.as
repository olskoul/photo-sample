/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2013
 ****************************************************************/
package view.edition.toolbar
{
	import be.antho.data.ResourcesManager;
	
	import com.bit101.components.CheckBox;
	import com.bit101.components.ColorChooser;
	import com.bit101.components.HBox;
	import com.bit101.components.HSlider;
	import com.bit101.components.Knob;
	import com.bit101.components.Label;
	import com.bit101.components.Text;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	
	import comp.button.SimpleButton;
	import comp.checkbox.CheckboxTitac;
	import comp.label.LabelTictac;
	
	import data.Infos;
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import library.toolbar.border.content;
	
	import manager.PagesManager;
	import manager.UserActionManager;
	
	import mcs.toolbarBg;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FramePhoto;

	public class BorderToolBar extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// const
		private const DEFAULT_DARK_COLOR:Number = 0x02a3e5;
		private const DEFAULT_LIGHT_COLOR:Number = 0x9dd6ef;
		private const DEFAULT_TRESHOLD:Number = 50;
		private const DEFAULT_BLACK_TRESHOLD:Number = 30;
		private const borderSizeRef:Array = [0,1,2,4];	// in mm	
		
		// view
		private var bg : toolbarBg = new toolbarBg();
		private var title : Label;
		private var container : HBox;
		private var colorChooserContainer : Sprite;
		private var colorChooser : ColorChooser;
		
		private var content:MovieClip;
		private var borders:Array = [];
		
		// data
		private var editionArea:EditionArea
		private var frame:Frame;
		private var margin:Number;

		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var borderNoneBtn:MovieClip;
		private var border2mmBtn:MovieClip;
		private var border4mmBtn:MovieClip;
		private var border6mmBtn:MovieClip;
		private var shadowBtn:MovieClip;

		public function BorderToolBar( $editionArea : EditionArea ) 
		{
			editionArea = $editionArea;
			var shadow : DropShadowFilter = new DropShadowFilter(3,90,0x000000,.3,10,10);
			bg.filters = [shadow];
			addChild(bg);
			
			// construct sliders
			margin = 10;
			
			// left area
			container = new HBox();
			container.spacing = 10;
			container.alignment = HBox.MIDDLE;
			container.x = margin/2;
			container.y = bg.y+margin/2;
			addChild(container);
			
			
			//btns
			var tooltipVo:TooltipVo;
			
			content = new library.toolbar.border.content();
			borderNoneBtn = ButtonUtils.makeButton(content.borderNoneBtn,borderBtnClickHandler,new TooltipVo("toolbar.border.noborder",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			border2mmBtn = ButtonUtils.makeButton(content.border2mmBtn,borderBtnClickHandler,new TooltipVo("toolbar.border.border.small",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			border4mmBtn = ButtonUtils.makeButton(content.border4mmBtn,borderBtnClickHandler,new TooltipVo("toolbar.border.border.medium",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			border6mmBtn = ButtonUtils.makeButton(content.border6mmBtn,borderBtnClickHandler,new TooltipVo("toolbar.border.border.large",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			shadowBtn = ButtonUtils.makeButton(content.shadowBtn,shadowBtnClickHandler,new TooltipVo("tooltip.transformtool.shadow",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK)) as MovieClip;
			
			// UPDATE : do not use shadow button in border toolbar
			if(shadowBtn && shadowBtn.parent) shadowBtn.parent.removeChild(shadowBtn);
			
			borders.push(borderNoneBtn);
			borders.push(border2mmBtn);
			borders.push(border4mmBtn);
			borders.push(border6mmBtn);
			setupBtn();
			container.addChild(content);
			
			
			// border color 
			colorChooserContainer = new Sprite();
			colorChooserContainer.graphics.beginFill(Colors.GREY, 0.5);
			colorChooserContainer.graphics.drawRect(0,0,40,40);
			colorChooserContainer.graphics.endFill();
			
			colorChooser = new ColorChooser(null,0,0,0x000000,colorChosen);
			colorChooser.OPEN.add(function(colorChoose:ColorChooser):void{EditionArea.backgroundClickEnabled = false});
			colorChooser.CLOSE.add(function():void{EditionArea.backgroundClickEnabled = true});
			colorChooser.usePopup = true;
			colorChooser.popupAlign = ColorChooser.TOP;
			colorChooser.model = new Bitmap(Colors.PALETTE);
			colorChooserContainer.addChild(colorChooser);
			container.addChild(colorChooserContainer);
			
			
			//apply to all photo btn
			var applyToAllBtn:SimpleButton = SimpleButton.createApplyToAllButton("lefttab.backgrounds.applytoall", applyBorderToAllFrame); 
			var applyBtn:SimpleButton = SimpleButton.createApplyButton("common.apply", apply); 
			container.addChild(applyToAllBtn);
			container.addChild(applyBtn);
		
			upadteBG();
			TweenMax.delayedCall(1,container.draw);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * add toolbar to a frame
		 */
		public function stickToFrame( newFrame : Frame ):void
		{	
			if(frame) unstick();
			
			//make sure its visible
			visible = true;
			
			// ref to current frame
			frame = newFrame;
			
			//add child this on top of the edition area display list
			editionArea.addChild(this);
			
			// positionate
			positionate();
			
			// reset slider and picker values 
			// TODO
			
			// apply effect to frame
			if( !frame is FramePhoto) Debug.warn("Popart system can only be used on framePhoto types");
			
			checkBtnState();
		}
		
		
		/* *
		 * positionate toolbar just below the frame
		 */
		public function positionate(e:Event = null):void
		{
			var toolbarPoint:Rectangle = editionArea.toolBarPosition;
			var frameBounds : Rectangle = frame.boundRectScaled;
			var point:Point = new Point(frame.x - frame.frameVo.width*.5,frame.y);
			
			this.y = toolbarPoint.y+55;
			this.x = toolbarPoint.x;
			
			if((this.x + this.width)>(stage.stageWidth-editionArea.x))
				this.x -= (this.x + this.width)-(stage.stageWidth-editionArea.x);
		}
		
		
		
		/* *
		 * unstick the transform tool from a frame
		 */
		public function unstick(e:Event = null):void
		{
			if(parent) parent.removeChild(this);
			if(colorChooser) colorChooser.close();
			frame = null;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		private function shadowBtnClickHandler(e:MouseEvent):void
		{
			FramePhoto(frame).applyShadow();
			checkBtnState();
		}
		
		/**
		 * Apply frame specification (border and shadow) to all frames
		 */
		private function applyBorderToAllFrame():void
		{
			PagesManager.instance.applyBorderToAllPhoto(frame.frameVo.border, frame.frameVo.shadow, frame.frameVo.fillColor);
		}
		private function apply():void
		{
			//UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE); // already done when color is chosen !
			unstick();
		}
		
		
		/**
		 * setupBtn
		 */
		private function setupBtn():void
		{
			
			for (var i:int = 0; i < borders.length; i++) 
			{
				var btn:MovieClip = borders[i];
				btn.icon.gotoAndStop(i+1);
				btn.borderSize = borderSizeRef[i];
			}
			shadowBtn.icon.gotoAndStop(5);
		}
		
		/**
		 * BTN border click handler
		 */
		private function borderBtnClickHandler(e:MouseEvent):void
		{
			var target:MovieClip = e.target as MovieClip;
			FramePhoto(frame).applyBorder(target.borderSize);
			
			checkBtnState();
		}
		
		/**
		 * Active the right button when stick to a frame
		 */
		private function checkBtnState():void
		{
			//borders
			for (var i:int = 0; i < borders.length; i++) 
			{
				var btn:MovieClip = borders[i];
				TweenMax.killTweensOf(btn.bg);
				TweenMax.to(btn.bg,0,{tint:(btn.borderSize == frame.frameVo.border)?Colors.YELLOW:null});
			}
			
			//shadow
			TweenMax.killTweensOf(shadowBtn.bg);
			TweenMax.to(shadowBtn.bg,0,{tint:(frame.frameVo.shadow)?Colors.YELLOW:null});
			
			//color
			colorChooser.value = (!isNaN(frame.frameVo.fillColor) && frame.frameVo.fillColor != -1)? frame.frameVo.fillColor : Colors.WHITE;
		}
		
		
		/**
		 * ADjust bg dimensions
		 */
		private function upadteBG():void
		{
			bg.width = container.width + margin;
			bg.height = container.height + margin;
		}
		
		/**
		 * when color has been chosen
		 */
		private function colorChosen(e:Event):void
		{
			Debug.log("BorderToolbar.colorChosen : "+colorChooser.value);
			if(frame && frame is FramePhoto && frame.frameVo){
				frame.frameVo.fillColor = colorChooser.value;
				FramePhoto(frame).applyBorder(frame.frameVo.border, frame.frameVo.fillColor);
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			}
		}
		
		/**
		 *
		 */
		/*private function onPopartToggle(e:Event = null):void
		{
			// modify 
			var flag : Boolean = popartCheck.selected;
			tresholdLabel.enabled = 
			tresholdSlider.enabled = 
			blackTresholdLabel.enabled = 
			blackTresholdSlider.enabled =
			lightColorLabel.enabled =
			lightColorPicker.enabled =
			darkColorLabel.enabled =
			darkColorPicker.enabled = flag;
			
			updatePopartEffect();
		}*/
		
		/**
		*  Update popart effect on frame
		*/
		/*private function updatePopartEffect():void
		{	
			var flag : Boolean = popartCheck.selected;
			if(flag){
				frame.frameVo.PopArtTreshold = tresholdSlider.value;
				frame.frameVo.PopArtBlackTreshold = blackTresholdSlider.value;
				frame.frameVo.PopArtLightColor = lightColorPicker.value;
				frame.frameVo.PopArtDarkColor = darkColorPicker.value;
			}
			(frame as FramePhoto).applyPopArt(flag);	
		}*/
		
		
		

		
		
	}
}