package view.edition
{
	import flash.display.CapsStyle;
	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import mx.core.mx_internal;
	
	import data.CalendarColors;
	import data.FrameVo;
	import data.Infos;
	import data.PageVo;
	
	import flashx.textLayout.formats.VerticalAlign;
	
	import manager.CalendarManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	public class FrameCalendar extends Frame
	{
		// signals
		public var EDIT_TEXT:Signal = new Signal(FrameCalendar);
		
		// data
		private var subContent:Sprite;
		private var pageVO:PageVo;
		private var colors:CalendarColors;
		//private var iAm29thFebOfBirthDayCal:Boolean;
		private var _linkedFrames : Vector.<Frame>; // a liked frame for calendar frame can be a text frame or a photo frame
		private var _linkedTextFrame : FrameText;	// shortcut to not have to recursive check in linked frames to find text frame
		private var _linkedPhotoFrame : FramePhoto;	// shortcut to not have to recursive check in linked frames to find photo frame
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var txt:TextField;
		
		public function FrameCalendar(frameVo:FrameVo)
		{
			super(frameVo);
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * The frame linked to this frame calendar
		 */
		public function get linkedFrames():Vector.<Frame>
		{
			updateLinkedFrames(); // each time we want the linked frame, we check to see if there was an update. It could be an overload here but like this we are sure it's always updated
			return _linkedFrames
		}
		
		/**
		 * The Photo frame linked to this frame calendar
		 */
		public function get linkedPhotoFrame():FramePhoto
		{
			updateLinkedFrames(); // each time we want the linked frame, we check to see if there was an update. It could be an overload here but like this we are sure it's always updated
			return _linkedPhotoFrame;
		}
		
		/**
		 * The Text frame linked to this frame calendar
		 */
		public function get linkedTextFrame():FrameText
		{
			updateLinkedFrames(); // each time we want the linked frame, we check to see if there was an update. It could be an overload here but like this we are sure it's always updated
			return _linkedTextFrame;
		}
		

		
		/*
		public function set behindFrame(value:Frame):void
		{
			_behindFrame = value;
			mouseChildren = mouseEnabled = (value==null);
			if(value!=null)
			value.DELETE.addOnce(behindFrameDeleted);
		}
		*/
		
		
		public function updateColors():void
		{
			renderFrame();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDENS
		////////////////////////////////////////////////////////////////

		override public function init():void
		{
			// construct
			construct();
			//render frame
			renderFrame();
			//Listeners
			setListeners();
			//ready!!
			frameReady();
		}
		
		override protected function setListeners():void
		{
			super.setListeners();
			content.doubleClickEnabled = true;
			content.mouseChildren = false;
			content.mouseEnabled = true;
			content.addEventListener(MouseEvent.DOUBLE_CLICK, onFrameDoubleClick);
		}
		
		override public function removeListeners():void
		{
			super.removeListeners();
			content.removeEventListener(MouseEvent.DOUBLE_CLICK, onFrameDoubleClick);
			content.doubleClickEnabled = false;
		}
		
		
		
		
		/**
		* create content and other view
		*/
		override protected function construct():void
		{
			//_isEmpty = false;
			//ref
			pageVO = PagesManager.instance.getPageVoByFrameVo(frameVo);
			pageVO.customColors = (pageVO.customColors)?pageVO.customColors:new CalendarColors();
			colors = pageVO.customColors;
			//colors.useGlobalColors = true;
			
			//create content
			if(!content) 
				content = new Sprite();
			addChild(content);	
			
			//create subContent
			if(!subContent)
				subContent = new Sprite();
			content.addChild(subContent);
			
			//Some calendar must be hidden and not printed, but still need to exsit (birthday calendar, luxCalendar)
			//frameVo.visible = shouldIBePrinted();
			
			//what to put in subContent
			createSubContent(); 
		}
		
		
		/**
		 * render a frame calendar 
		 * stroke or not, dat number etc
		 */
		override protected function renderFrame():void
		{
			// draw bg and stroke and
			drawStrokeAndBackground(frameVo.stroke, colors.dayBackground);
				
			//Zoom textfield (upgrades only)
			//txt.scaleX = txt.scaleY = frameVo.zoom;
			
			// center subcontent (text) in the frame
			subContent.x = -frameVo.width *.5; //-subContent.width >>1;
			subContent.y = -frameVo.height *.5; //-subContent.height >>1;
			

			// apply frame position and rotation
			x = frameVo.x + frameVo.posXAdjustement;
			y = frameVo.y;
			rotation = frameVo.rotation;
			
			if(frameVo.visible)
				subContent.visible = true;
			else
				subContent.visible = false;
		}
		
		
		/**
		 * clear linked frames
		 * > WARNING : when we clear a calendar frame that does have linked frames, we DELETE completely all linked frames !
		 */
		public override function clearMe():void
		{
			if(! linkedFrames ) return;
			
			// remove exising linkage
			PagesManager.instance.removeFramesLinkage( frameVo.uniqFrameLinkageID );
			
			// delete all linked frames
			for (var i:int = 0; i < _linkedFrames.length; i++) 
			{
				_linkedFrames[i].deleteMe();
			}
			
			// 
			_linkedFrames = null;
			
			// update background colors
			updateLinkedFrames(); // remove linkage
			updateColors();
			
			super.clearMe();
			
			
		}
		
		/**
		 * Save frame properties
		 * overriden: Because some calendars frame should not be saved and rely on the layout all the time
		 *
		 */
		override public function save():void
		{
			//Cal Lux dateName Frame properties should not be saved as they should never changed from the layout
			if(Infos.project.docPrefix == "WCAL5" || Infos.project.docPrefix == "WCAL6")
			{
				if(frameVo.dateAction == CalendarManager.DATEACTION_DATENAME)
				{
					return;
				}
			}
			
			super.save();
		}	
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{
			removeListeners();
			EDIT_TEXT.removeAll();
			super.destroy();
		}
		
		/**
		 * inside zoom for frame overlayer does it on its linked frame
		 */
		public function zoom ( amount:Number ):void
		{	
			if( _linkedPhotoFrame ){
				_linkedPhotoFrame.zoom( amount);
			}
		}
		/**
		 * when zoom is over
		 */
		public function endZoom():void
		{
			if( _linkedPhotoFrame ){
				_linkedPhotoFrame.endZoom();
			}
		}
		
		/**
		 * Move content inside cropped area
		 */
		public function insideMove():void
		{	
			if( _linkedPhotoFrame ){
				_linkedPhotoFrame.insideMove();
			}
		}
		
		/**
		 * Stop inside move
		 * > reset and clean movePreviewBitmap
		 */
		public function stopInsideMove():void
		{
			if( _linkedPhotoFrame ){
				_linkedPhotoFrame.stopInsideMove();
			}
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATES
		////////////////////////////////////////////////////////////////
		
		
		
		
		/**
		 * The linked frame has been deleted
		 * remove its reference
		 */
		/*
		private function behindFrameDeleted(frame:Frame):void
		{
			behindFrame = null;
		}
		*/
		/**
		 *
		 */
		private function onFrameDoubleClick(e:MouseEvent):void
		{
			if(! frameVo.editable ) return;
			//if( _linkedTextFrame ) return; // do not allow double click if there is already a frame text (double click force a frame text creation)
			EDIT_TEXT.dispatch(this);
		}
		
		
		
		/**
		 * render a frame calendar 
		 * stroke or not, dat number etc
		 * force render background when we set back the calendar colors to -1
		 */
		private function drawStrokeAndBackground(flagBorder:Boolean, backgroundColor:Number, forceRenderBackground:Boolean = false ):void
		{
			var strokeAlpha:Number = (flagBorder)?1:0;
			
			// if frame has a linkage, be sure to update frames before applying colors
			if(frameVo.uniqFrameLinkageID != null) updateLinkedFrames();
			
			// check for background color
			// if frame has a text, apply background to text as text is below calendar frame
			// if frame has an image, do not apply background as image is background
			var backgroundAlpha:Number = ( backgroundColor != -1 && !_linkedPhotoFrame )?1:0;
			backgroundColor = (backgroundColor != -1)? backgroundColor : 0; // always have a background color to allow selection
			backgroundAlpha = (frameVo.dateAction == CalendarManager.DATEACTION_DAYDATE)?backgroundAlpha:0;
			
			// set background to text instead of frame if there is a linked text frame
			if( _linkedTextFrame && (backgroundAlpha != 0 || forceRenderBackground) )
			{
				_linkedTextFrame.frameVo.tb = true;
				_linkedTextFrame.frameVo.fillAlpha = backgroundAlpha;
				_linkedTextFrame.frameVo.fillColor = backgroundColor;
				_linkedTextFrame.renderTextBackground();
				backgroundAlpha = 0; // if there is a text linked frame, always remove background so we can display it
			}
			
			// draw graphics
			subContent.graphics.clear();
			//subContent.graphics.lineStyle(MeasureManager.mmToPoint*1, colors.border,strokeAlpha,true,CapsStyle.SQUARE);
			subContent.graphics.lineStyle(1, colors.border,strokeAlpha,true,CapsStyle.SQUARE);
			subContent.graphics.beginFill(backgroundColor,backgroundAlpha);
			subContent.graphics.drawRect(0,0,frameVo.width, frameVo.height);
			subContent.graphics.endFill();
		}
		
		
		/**
		 * Based on the dateAction property, we change things to put in subContent
		 * Mostly its a textfield with a diffenet textFormet, sometimes other things..
		 */
		private function createSubContent():void
		{
			switch(frameVo.dateAction)
			{
				case CalendarManager.DATEACTION_MINICALENDAR_PREVIOUS:
					createMiniCalendar(-1);
					break;
				case CalendarManager.DATEACTION_MINICALENDAR_NEXT:
					createMiniCalendar(1);
					break;
				
				default:
					createNormalSubContent();
			}
		}
		
		/**
		 * MINI CALENDAR CONTENT
		 * ONly for XXL and XL
		 * */
		private function createMiniCalendar(monthIndexOffSet:int):void
		{
			var monthIndex:int = CalendarManager.instance.getFrameMonthIndex(frameVo);
			var txt:TextField;
			var textFormat:TextFormat = TextFormats.getCalendarTextFormatByDateAction(frameVo,"center");
			var posY:int = 0;
			textFormat.color = CalendarManager.instance.getColor(frameVo,frameVo.dateAction);
			
			//month year
			txt = new TextField();
			txt.width = 10;//frameVo.width;
			txt.multiline = false;
			txt.antiAliasType = AntiAliasType.ADVANCED;
			txt.autoSize = TextFieldAutoSize.LEFT;
			txt.selectable = false;
			txt.wordWrap = false;
			txt.embedFonts = true;
			txt.text = CalendarManager.instance.getMiniCalendarMonthWithOffSetYear(monthIndex,monthIndexOffSet,true);
			txt.setTextFormat(textFormat);
			txt.x = frameVo.width - txt.width >>1;
			subContent.addChild(txt);
			
			posY = Math.ceil(txt.textHeight) + MeasureManager.PFLToPixel(3);
			
			//vertical weeks
			for (var i:int = 0; i < 7; i++) 
			{
				txt = new TextField();
				txt.width = frameVo.width/7;
				txt.multiline = false;
				txt.antiAliasType = AntiAliasType.ADVANCED;
				txt.autoSize = TextFieldAutoSize.CENTER;
				txt.selectable = false;
				txt.wordWrap = false;
				txt.embedFonts = true;
				txt.x = (frameVo.width/7)*i;
				txt.y = posY;
				var smallerSize:Number = MeasureManager.getFontSize(Number(frameVo.calFontSizeRaw)-2,false);
				textFormat.size = smallerSize;//frameVo.fontSize - 2;
				var verticalWeekIndex:String = ""+int(monthIndex+1)+String(i+1);
				txt.text = CalendarManager.instance.getVerticalWeek(int(verticalWeekIndex),monthIndexOffSet,1,true);
				txt.setTextFormat(textFormat);
				subContent.addChild(txt);
			}
			
		}
		
		/**
		 * REGULAR CONTENT
		 * */
		private function createNormalSubContent():void
		{
			
			var monthIndex:int = CalendarManager.instance.getFrameMonthIndex(frameVo);
			txt = new TextField();
			var align:String = CalendarManager.instance.getFrameTextHAlign(frameVo);
			subContent.addChild(txt);
			txt.multiline = getMultiline(frameVo.dateAction);
			txt.antiAliasType = AntiAliasType.ADVANCED;
			txt.width = frameVo.width;
			txt.autoSize = getAutoSize(frameVo.dateAction);
			txt.selectable = false;
			txt.background = false;
			//txt.alpha = .5;
			txt.backgroundColor = 0xDDDDDD;
			txt.wordWrap = getWordWrap(frameVo.dateAction);
			
			//small hack for MINI CALENDARS on txt.width
			txt.width = (frameVo.dateAction != CalendarManager.DATEACTION_MINICALENDAR_WEEK)?txt.width:txt.width+3;
			
			//Textformat
			//txt.defaultTextFormat = TextFormats.getCalendarTextFormatByDateAction(frameVo.dateAction,frameVo.fontSize,frameVo.color,align);
			txt.defaultTextFormat = TextFormats.getCalendarTextFormatByDateAction(frameVo,align);
			//txt.text = (iAm29thFebOfBirthDayCal)?"00":CalendarManager.instance.getFrameText(frameVo);
			txt.text = CalendarManager.instance.getFrameText(frameVo);
			
			// COLOR 
			var tempTextFormat : TextFormat = txt.getTextFormat();
			tempTextFormat.color = CalendarManager.instance.getFrameTextColor(frameVo);
			txt.setTextFormat(tempTextFormat);
			
			//Valign
			var posY:Number = 0;
			switch (frameVo.vAlign)
			{
				case VerticalAlign.MIDDLE:
					posY = frameVo.height - txt.textHeight >> 1;
					break;
				
				case VerticalAlign.BOTTOM:
					posY = frameVo.height - txt.textHeight;
					break;
			}
			txt.y = Math.round(posY);
			
			
			var textOffset : Point = CalendarManager.instance.getFrameTextOffset(frameVo);
			txt.x += textOffset.x;
			txt.y += textOffset.y;
			
			
			//LIttle hack: todo: see if still needed when vAling is implemented
			if(ProjectManager.instance.project.docCode == "MCAL1" || ProjectManager.instance.project.docCode == "MCAL2" && frameVo.dateAction == CalendarManager.DATEACTION_MINICALENDAR_WEEK)
			{
				txt.height = frameVo.height*.8;
			}
		}
		
		
		
		/**
		 Helper: gives the autoSize based on the dateaction
		 */
		private function getAutoSize(dateAction:String):String
		{
			switch(dateAction)
			{
				case CalendarManager.DATEACTION_NOTES:
					return TextFieldAutoSize.NONE;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_WEEK:
					return TextFieldAutoSize.NONE;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_MONTH:
					return TextFieldAutoSize.CENTER;
					break;
				
			}
			return TextFieldAutoSize.CENTER;
		}
	
		
		/**
		 Helper: gives the halign based on the dateaction
		 */
		private function getMultiline(dateAction:String):Boolean
		{
			switch(dateAction)
			{
				case CalendarManager.DATEACTION_NOTES:
					return true;
					break;
			}
			return false;
		}
		
		/**
		 Helper: gives the halign based on the dateaction
		 */
		private function getWordWrap(dateAction:String):Boolean
		{
			switch(dateAction)
			{
				case CalendarManager.DATEACTION_MINICALENDAR_MONTH:
					return false;
					break;
			}
			return true;
		}
		
		/**
		 * notify everyone that frame is ready
		 * It adds listener, for every transform event and draggable event
		 */
		private function frameReady():void
		{
			CREATED.dispatch(this);	
		}
		
		
		/**
		 * update linked frame
		 */
		private function updateLinkedFrames():void
		{
			// reset linked frames
			_linkedFrames = null;
			_linkedPhotoFrame = null;
			_linkedTextFrame = null;
			
			// if linkage id, recover all frames linked
			if ( frameVo.uniqFrameLinkageID )
			{
				_linkedFrames = PagesManager.instance.getFramesLinked(this);
				if(_linkedFrames &&  _linkedFrames.length > 0)
				{
					for (var i:int = 0; i < _linkedFrames.length; i++) 
					{
						if( _linkedFrames[i] is FrameText ) _linkedTextFrame = _linkedFrames[i] as FrameText;
						else if( _linkedFrames[i] is FramePhoto ) _linkedPhotoFrame = _linkedFrames[i] as FramePhoto;
					}
				}
			}
			
			/*	TO DELETE OLD 2		
			var linkedFrames : Vector.<Frame> = PagesManager.instance.getFramesLinked(this);
			if(linkedFrames) return linkedFrames
			return null;
			
			// if no linked frame found, find it now !
			if ( frameVo.uniqFrameLinkageID && !_linkedFrame) _linkedFrame = findLinkedFrame();
			if(!_linkedFrame) return;
			*/
			
			/* TO DELETE OLD 1
			// update linked frame vo
			linkedFrame.frameVo.x = frameVo.x;
			linkedFrame.frameVo.y = frameVo.y;
			linkedFrame.frameVo.width = frameVo.width;
			linkedFrame.frameVo.height = frameVo.height;
			linkedFrame.frameVo.rotation = rotation;
			
			// be sure to remove possible shadows and borders
			linkedFrame.frameVo.border = 0;
			linkedFrame.frameVo.shadow = false;
			
			if( linkedFrame is FramePhoto ){
				var pFrame:FramePhoto = linkedFrame as FramePhoto;
				
				// constraint photo vo in new frameVo width
				if( pFrame.frameVo.photoVo) FrameVo.injectPhotoVoInFrameVo(linkedFrame.frameVo, linkedFrame.frameVo.photoVo);
				
				// render linked frame
				pFrame.update(null);
			}
			else if (linkedFrame is FrameText){
				// TODO handle linked text frame
			}
			*/
		}
	}
}