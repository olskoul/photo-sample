package view.edition.editionArea
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import comp.DefaultTooltip;
	
	import data.TooltipVo;
	
	import library.cover.option.selected.view;
	
	import utils.Colors;
	
	public class FlyleafColorBox extends Sprite
	{
		
		
		public var color:Number;
		public var id:String;
		public var tooltipVo:TooltipVo;
		private var selectedView:Sprite;
		
		public function FlyleafColorBox()
		{
			super();
			
			
			
		}
		
		public function init():void
		{
			//color box
			graphics.lineStyle(1,Colors.GREY_LIGHT,1,true);
			graphics.beginFill(color);
			graphics.drawRect(0,0,20,20);
			
			// create selected view
			selectedView = new library.cover.option.selected.view();
			selectedView.x = width - selectedView.width >> 1;
			selectedView.y = (color!=-1)?-8:-2;
			addChild(selectedView);
			
			//over
			addEventListener(MouseEvent.ROLL_OVER, function (e:MouseEvent):void
			{
				DefaultTooltip.tooltipOnClip(e.currentTarget as DisplayObject,tooltipVo);
			});
			//out
			addEventListener(MouseEvent.ROLL_OUT, function (e:MouseEvent):void
			{
				DefaultTooltip.killAllTooltips();
			});
		}
		
		public function selected(value:Boolean):void
		{
			selectedView.visible = value;
		}
		
		
	}
}