package view.edition
{
	import data.FrameVo;

	/**
	 * Frame multiple is the frame used in multiple canvas format.
	 * It's a virtual frame that will draw multiple sub frames
	 * It's behavior is the same as the frame photo but it redraws subframes based on current frame position, zoom etc...
	 */
	public class FrameMultiple extends FramePhoto
	{
		////////////////////////////////////////////////////////////////
		//	CONSTRuCTOR
		////////////////////////////////////////////////////////////////
		
		public function FrameMultiple( frameVo:FrameVo )
		{
			super(frameVo);
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
	}
}