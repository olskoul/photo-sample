package view.edition
{
	import data.FrameVo;
	import data.OverlayerVo;
	import data.PhotoVo;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import manager.PagesManager;
	
	
	public class FrameOverlayer extends FramePhoto
	{
		//
		private var _linkedFrame : FramePhoto;
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRuCTOR
		////////////////////////////////////////////////////////////////
		
		public function FrameOverlayer(frameVo:FrameVo)
		{
			super(frameVo);
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 * + remove linked frame reference
		 */
		override public function destroy():void
		{	
			super.destroy();
			_linkedFrame = null;
		}
		
		/**
		 * modify linked frame to fit with current overlayer area
		 * -> this is done when user add an overlayer frame to a photo frame
		 * -> this is done when we update an overlayer with new overlayer vo
		 */
		public function adaptLinkedFrameToOverlayer():void
		{
			if(! linkedFrame ) _linkedFrame = findLinkedFrame();
			updateLinkedFrame( true );
		}
		
		
		/**
		 * update current frame position
		 */
		public override function updatePosition(newX:Number, newY:Number):void
		{
			super.updatePosition(newX, newY);
			updateLinkedFrame();
		}
		
		/**
		 * update current frame rotation
		 */
		public override function updateRotation(newRotation:Number):void
		{
			super.updateRotation(newRotation);
			updateLinkedFrame();
		}
		
		
		/**
		 * drop bounds is used for highlight
		 */
		public override function get dropBounds():Rectangle
		{
			return this.getRect(this);
		}
		
		/**
		 * The frame linked to this overlayer
		 */
		public function get linkedFrame():FramePhoto
		{
			return _linkedFrame
		}
		
		
		
		/**
		 * 2. Update frame with a new overlayer vo
		 * -> update current overlayer with new overlayer frame 
		 * -> update current overlayer without new overlayer vo, then just render frame and adapt linked frame 
		 */
		public override function update(photoVo:PhotoVo, keepPhotoRatio:Boolean = false):void
		{
			if(photoVo) {
				_linkedFrameRect = null; // if new overlayer vo, reset frame rect to null so it's rendered again in renderframe
				
				// we first check the special item blank !
				if( photoVo.id == "blank"){
					deleteMe();
					return;
				}
			}
			
			// if no linked frame found, find it now !
			if ( frameVo.uniqFrameLinkageID && !_linkedFrame) _linkedFrame = findLinkedFrame();
			
			// update overlayer content and view
			super.update(photoVo, keepPhotoRatio);
			
			// adapt current linked frame to fit visible overlayer rect
			adaptLinkedFrameToOverlayer();
			
			// update bounds
			updateFrameBounds();
		}
		
		
		/**
		 * Delete overlayer
		 * -> it also
		 */
		public override function deleteMe( withAnim : Boolean = true):void
		{
			// when deleting an overlayer, we also remove linkage
			if(frameVo) {
				var deletedLinkage : int = PagesManager.instance.removeFramesLinkage(frameVo.uniqFrameLinkageID);
				trace("OVERLAYER KILLED : linkage removed : " + deletedLinkage);
			}
			
			DELETE.dispatch(this, withAnim);
		}
		
		
		/**
		 * inside zoom for frame overlayer does it on its linked frame
		 */
		public override function zoom ( amount:Number ):void
		{
			if(linkedFrame){
				linkedFrame.zoom( amount);
			}
		}
		/**
		 * when zoom is over
		 */
		public override function endZoom():void
		{
			if(linkedFrame){
				linkedFrame.endZoom();
			}
		}
		
		/**
		 * Move content inside cropped area
		 */
		public override function insideMove():void
		{	
			if(linkedFrame){
				linkedFrame.insideMove();
			}
		}
		
		/**
		 * Stop inside move
		 * > reset and clean movePreviewBitmap
		 */
		public override function stopInsideMove():void
		{
			if(linkedFrame){
				linkedFrame.stopInsideMove();
			}
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	protected
		////////////////////////////////////////////////////////////////
		
		/**
		 * rectangle corresponding of linked frame area to display
		 */
		private var _linkedFrameRect : Rectangle; 
		private function get linkedFrameRect():Rectangle
		{
			// mask rect is the area in wich linked frame should be viewed
			if(_linkedFrameRect) return _linkedFrameRect;
			if(!_linkedFrameRect && frameVo.photoVo){
				_linkedFrameRect = new Rectangle();
				var vo : OverlayerVo = frameVo.photoVo as OverlayerVo;
				_linkedFrameRect.x = (vo.bleft - vo.bwidth)*.5; 
				_linkedFrameRect.y = (vo.btop - vo.bheight)*.5;
				_linkedFrameRect.width = vo.width - vo.bleft - vo.bwidth;
				_linkedFrameRect.height = vo.height - vo.btop - vo.bheight;
				return _linkedFrameRect;
			}
			return null;
		}
		
		/**
		 * find frame linked with same uniq linke id
		 */
		private function findLinkedFrame():FramePhoto
		{
			var linkedFrames : Vector.<Frame> = PagesManager.instance.getFramesLinked(this);
			if(linkedFrames) {
				return linkedFrames[0] as FramePhoto;
			}
			return null;
		}
	
		
		
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 * + render linked frame if existing
		 */
		override protected function renderFrame():void
		{
			// be sure no mask is set for this frame
			content.mask = null;
			if(masker && masker.parent) masker.parent.removeChild(masker);
			
			// content
			content.scaleX = content.scaleY = 1;
			image.scaleX = image.scaleY = frameVo.zoom;
			
			// apply position
			x = frameVo.x;
			y = frameVo.y;
			rotation = frameVo.rotation;
			
			// be sure no border nor shadow is set for this frame
			if(borderShape && borderShape.parent) borderShape.parent.removeChild(borderShape);
			
			// quality indicator
			//super.updateQualityIndicator();
			
			// if no linked frame found, find it now !
			if ( frameVo.uniqFrameLinkageID && !_linkedFrame) _linkedFrame = findLinkedFrame();
			
			// update linked frame
			updateLinkedFrame();
		}
		
		/**
		 * returns the frame width of linked frame
		 */
		private function get linkedFrameWidth():Number
		{
			return linkedFrameRect.width * frameVo.zoom; 
		}
		
		/**
		 * returns the frame height of linked frame
		 */
		private function get linkedFrameHeight():Number
		{
			return linkedFrameRect.height * frameVo.zoom;
		}
		
		
		/**
		 * update linked frame
		 */
		private function updateLinkedFrame( adaptToOverlay : Boolean = false):void
		{
			if(!linkedFrame) return;
			
			// update linked frame vo
			var translationPoint : Point = frameVo.translateWithRotation(linkedFrameRect.x * frameVo.zoom, linkedFrameRect.y * frameVo.zoom);
			linkedFrame.frameVo.x = frameVo.x + translationPoint.x;//frameVo.translateWithRotation(linkedFrameRect.x * frameVo.zoom; // offset pos x of frame borders
			linkedFrame.frameVo.y = frameVo.y + translationPoint.y;//linkedFrameRect.y * frameVo.zoom; // offset pos y of frame borders
			var zoomDiff : Number =  linkedFrameWidth / linkedFrame.frameVo.width; // zoom scale since last update
			linkedFrame.frameVo.width = linkedFrameWidth;
			linkedFrame.frameVo.height = linkedFrameHeight;
			linkedFrame.frameVo.rotation = rotation;
			
			// be sure to remove possible shadows and borders
			linkedFrame.frameVo.border = 0;
			linkedFrame.frameVo.shadow = false;
			
			
			// constraint photo vo in new frameVo width
			if( linkedFrame.frameVo.photoVo && adaptToOverlay ) FrameVo.injectPhotoVoInFrameVo(linkedFrame.frameVo, linkedFrame.frameVo.photoVo);
			else {
				// update zoom and crop values
				linkedFrame.frameVo.zoom *= zoomDiff;
				linkedFrame.frameVo.cLeft *= zoomDiff;
				linkedFrame.frameVo.cTop *= zoomDiff;
			}
			
			// render linked frame
			linkedFrame.update(null);
			//linkedFrame.updateQualityIndicator(true);

		}
		
	}
}