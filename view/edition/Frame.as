﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition 
{
	import com.bit101.components.Calendar;
	import com.bit101.components.ProgressBar;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.CapsStyle;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.LineScaleMode;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import mx.core.IContainer;
	
	import be.antho.bitmap.snapshot.SnapShot;
	import be.antho.utils.MathUtils2;
	
	import comp.DropTargetArea;
	import comp.button.SimpleButton;
	
	import data.FrameVo;
	import data.PageVo;
	import data.PhotoVo;
	
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import ordering.FrameExporter;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.CursorManager;
	import utils.Debug;
	
	import view.edition.pageNavigator.PageNavigator;
	import view.uploadManager.PhotoUploadManager;


	/**
	 * Frame is alayer inside the edition Area
	 * It can contain an image, a clipart, a text or something that can be editable
	 * It can be moved and rotated and the inside element can be transformed using the transform tool (zoom, directio, t
	 */
	public class Frame extends DropTargetArea
	{
		public static const STATE_EMPTY:String = "FRAME_STATE_EMPTY";
		public static const STATE_USED:String = "FRAME_STATE_USED";
		
		// signals
		public var DELETE:Signal = new Signal(Frame, Boolean); // the frame to delete and with or not animation
		public var CREATED:Signal = new Signal(Frame);
		public var SELECT:Signal = new Signal(Frame);
		public var DESELECT:Signal = new Signal(Frame);
		public var BOUNDS_CHANGE:Signal = new Signal(Frame);
		public var DEPTH_UP:Signal = new Signal(Frame);
		public var DEPTH_DOWN:Signal = new Signal(Frame);
		public var MOVE_ME:Signal = new Signal(Frame);
		public var CLEAR:Signal = new Signal(Frame);
		
		
		// PUblic datas
		private var _frameVo : FrameVo; // ... rotation, filters, size, dpi, etc...
		public var frameDepth : int; // frame depth in page area, depth is not always display list depth as we can have frames group (overlayer, calendars)
		
		// private
		private var _boundRectScaled:Rectangle = new Rectangle();
		
		//ui & CTA
		protected var content:Sprite; // it the image container on which we apply transformations
		protected var cta:Sprite; // CTA = call to action when frame is empty
		protected var ctaBg:Sprite; // BG in cta sprite
		private var iconBtn:SimpleButton;
		private var iconHeight:Number = 70;
		private var _iconbmd:BitmapData;
		private var icon:Bitmap;
		private var iconContainer:Sprite;
		private var _labelCta:String = "no label";
		
		// If the transform tool is stick to this frame
		protected var _isSticked:Boolean = false; 
		protected var overColor : Number = Colors.GREEN;
		private var swapHighlightGfx : Sprite;
		
		// flag to indicate if there is an error with this frame
		protected var _hasError : Boolean = false;
		public function get hasError():Boolean {return _hasError};
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		public function get allowInsideMove():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					if(!frameVo.isEmpty && frameVo.photoVo != null) return true;
					return false;
					break;
				
				case FrameVo.TYPE_PHOTO:
					if(!frameVo.isEmpty && (this as FramePhoto).isCropped) return true;
					else return false;
					break;
				
				case FrameVo.TYPE_CLIPART:
					return false;
					break;
				
				case FrameVo.TYPE_TEXT:
					return false;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					return true;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					if( (this as FrameCalendar).linkedPhotoFrame ) return true; // if there is a frame photo linked to frame calendar
					return false;
					break;
			}
			
			return false;
		}
		
		
		/**
		 * GETTER SETTER FRAME VO
		 */
		public function get frameVo():FrameVo
		{
			return _frameVo;
		}
		public function set frameVo(value:FrameVo):void
		{
			_frameVo = value;
		}
		
		
		public function get allowRotation():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					return false;
					break;
				
				case FrameVo.TYPE_PHOTO:
					if(frameVo.fixed) return false;
					return true;
					break;
				
				case FrameVo.TYPE_CLIPART:
					return true;
					break;
				
				case FrameVo.TYPE_TEXT:
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE) return false;
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) return false;
					return true;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					return true;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					return false;
					break;
			}
			
			return false;
		}
		
		public function get allowBorderMove():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					return false;
					break;
				
				case FrameVo.TYPE_PHOTO:
					if(frameVo.fixed) return false;
					return true;
					break;
				
				case FrameVo.TYPE_CLIPART:
					return true;
					break;
				
				case FrameVo.TYPE_TEXT:
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE) return false;
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) return false;
					if(frameVo.isPageNumber) return false;
					return true;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					return true;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					return false;
					break;
				
				case FrameVo.TYPE_QR:
					return true;
					break;
				
			}
			
			return false;
		}
		
		public function get allowCrop():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					return false;
					break;
				
				case FrameVo.TYPE_PHOTO:
					if(frameVo.fixed) return false;
					return true; //if(!frameVo.isEmpty) return true;
					return false;
					break;
				
				case FrameVo.TYPE_CLIPART:
					return false;
					break;
				
				case FrameVo.TYPE_TEXT:
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE) return false;
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) return false;
					if(frameVo.isPageNumber) return false;
					return true;//if(!frameVo.isEmpty) return true;
					return false;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					return false;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					return false;
					break;
				
				case FrameVo.TYPE_QR:
					return false;
					break;
			}
			
			return false;
		}
		
		public function get allowResize():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					return false;
					break;
				
				case FrameVo.TYPE_PHOTO:
					if(frameVo.fixed) return false;
					return true ; // if(!frameVo.isEmpty) return true;
					return false;
					break;
				
				case FrameVo.TYPE_CLIPART:
					if(!frameVo.isEmpty) return true;
					else return false;
					break;
				
				case FrameVo.TYPE_TEXT:
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE) return false;
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) return false;
					if(frameVo.isPageNumber) return false;

					return true;//if(!frameVo.isEmpty) return true;
					return false;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					if(!frameVo.isEmpty) return true;
					else return false;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					return false;
					break;
				
				case FrameVo.TYPE_QR:
					return true;
					break;
			}
			
			return false;
		}

		public function get allowMove():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					return false
					break;
				
				case FrameVo.TYPE_PHOTO:
					if(frameVo.fixed) return false;
					return true;
					break;
				
				case FrameVo.TYPE_CLIPART:
					return true;
					break;
				
				case FrameVo.TYPE_TEXT:
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE) return false;
					if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) return false;
					var frame:FrameText = this as FrameText;
					if(frame.hasFocus) return false;
					return true;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					return true;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					return false;
					break;
				
				case FrameVo.TYPE_QR:
					return true;
					break;
			}
			
			return false;
		}
		
		
		public function get allowToolBar():Boolean
		{
			switch (frameVo.type)
			{
				case FrameVo.TYPE_BKG:
					if(isEmpty)return false;
					if(frameVo.photoVo || frameVo.fillColor != -1) return true;
					break;
				
				case FrameVo.TYPE_PHOTO:
					return true;
					break;
				
				case FrameVo.TYPE_CLIPART:
					return true;
					break;
				
				case FrameVo.TYPE_TEXT:
					return true;
					break;
				
				case FrameVo.TYPE_OVERLAYER:
					return true;
					break;
				
				case FrameVo.TYPE_CALENDAR:
					if(frameVo.editable) return true;
					else return false;
					break;
				
				case FrameVo.TYPE_QR:
					return true;
					break;
			}
			
			return false;
		}
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		public function Frame( frameVo : FrameVo) 
		{ 
			super();
			this.frameVo = frameVo ;
			
			overColor = getColorByFrameType();
		}
		
		private function getColorByFrameType():Number
		{
			switch(frameVo.type)
			{
				case FrameVo.TYPE_TEXT:
				return Colors.BLUE;
				break;
				
				case FrameVo.TYPE_PHOTO:
					return Colors.GREEN;
					break;
				
				case FrameVo.TYPE_BKG:
					return Colors.GREY_LIGHT;
					break;
				
			}
			
			return Colors.GREY_DARK;
		}		
		
		////////////////////////////////////////////////////////////////
		//	Static functions
		////////////////////////////////////////////////////////////////
		
		/**
		 * create popart effect bitmap clone
		 */
		public static function popartBitmap(bitmapdata:BitmapData, params : Object, scaleFactor:Number = 1):BitmapData 
		{	
			Debug.log("Frame > popartBitmap > width :"+bitmapdata.width);
			
			if(scaleFactor != 1){
				Debug.log("Frame > popartBitmap > scale factor : "+scaleFactor);
				var tempBmd : BitmapData = new BitmapData(bitmapdata.width*scaleFactor, bitmapdata.height*scaleFactor);
				var m:Matrix = new Matrix();
				m.scale(scaleFactor, scaleFactor);
				tempBmd.draw(bitmapdata, m);
				bitmapdata = null;
				bitmapdata = tempBmd;
				Debug.log("Frame > popartBitmap > new width : "+bitmapdata.width);
			}
			
			var graymatrix:Array = [0.3086,0.6094,0.0820,0,0,
				0.3086,0.6094,0.0820,0,0,
				0.3086,0.6094,0.0820,0,0,
				0,0,0,1,0];
			var grayfilter:ColorMatrixFilter = new ColorMatrixFilter(graymatrix);
			var popartdata:BitmapData = new BitmapData(bitmapdata.width, bitmapdata.height);
			popartdata.floodFill(0, 0, 0xFF000000);
			bitmapdata.applyFilter(bitmapdata, bitmapdata.rect, new Point(0, 0), grayfilter);
			bitmapdata.threshold(bitmapdata, bitmapdata.rect, new Point(0, 0), "<", ((params.PopArtBlackTreshold/100)*0xFFFFFF) + 0xFF000000, 0xFF000000, 0xFFFFFFFF, true);
			popartdata.threshold(bitmapdata, bitmapdata.rect, new Point(0, 0), ">", 0xFF000000, params.PopArtDarkColor | 0xFF000000, 0xFFFFFFFF, false);
			popartdata.threshold(bitmapdata, bitmapdata.rect, new Point(0, 0), ">=", ((params.PopArtTreshold/100)*0xFFFFFF) + 0xFF000000, params.PopArtLightColor | 0xFF000000, 0xFFFFFFFF, false);
			return popartdata;
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		//CTA
		public function get labelCta():String
		{
			return _labelCta;
		}

		public function get iconbmd():BitmapData
		{
			return _iconbmd;
		}

		//TRANSFORM tool
		public function get isSticked():Boolean
		{
			return _isSticked;
		}

		public function set isSticked(value:Boolean):void
		{
			_isSticked = value;
			if(value)
			{
				rollOut(true);
			}
		}

		/**
		 * 
		 * GET scaled bounds based on the editionArea scale factor (var editorscale) if different from 1
		 * 
		 * */
		
		public function get boundRectScaled():Rectangle
		{
			var rect:Rectangle;
			if(EditionArea.editorScale != 1)
				rect = new Rectangle(boundRect.x*EditionArea.editorScale,boundRect.y*EditionArea.editorScale,boundRect.width*EditionArea.editorScale,boundRect.height*EditionArea.editorScale);
			else rect = boundRect;
			return rect;
		}
		
		
		/**
		 * 
		 * GET scaled bounds based on the editionArea scale factor (var editorscale) if different from 1
		 * 
		 * */
		public function get frameRatio():Number
		{
			return frameVo.width / frameVo.height;
		}
		
		/**
		 * ist the current frame empty (Could be overriden)
		 */
		public function get isEmpty() : Boolean
		{
			return frameVo.isEmpty;
		}
		
		
		/**
		 * the bounds of a frame correspond to the bounds of it's frame vo
		 */
		public function get boundRect():Rectangle
		{
			return frameVo.boundRect;//new Rectangle(-frameVo.width*.5, -frameVo.height*.5, frameVo.width, frameVo.height);
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		public function init():void
		{
		}		
		
		/**
		 * frame content should always be centered
		 * center content in the middle of the frame
		 * returns the diff with the previous point
		 */
		/*public function centerContent():Point
		{	
			return null;
		}*/
		
		
		/**
		 * Resize content at specific point 
		 * 0 is Top Left
		 * 2 is Top Right
		 * 5 is Bottom Left
		 * 7 is Bottom Right
		 */
		public function resize( resizeType : int ):void
		{	
		}
		public function endResize():void
		{}
		
		/**
		 * Resize content at specific point 
		 * 1 is Top
		 * 3 is Left
		 * 4 is right
		 * 6 is Bottom
		 */
		public function crop( cropType : int ):void
		{	
			// update content scrollrect
			updateFrameBounds();			
		}
		public function endCrop():void
		{}

		
		/**
		 * Move frame one depth up in page
		 */
		public function depthUp():void
		{
			DEPTH_UP.dispatch(this);
			PageNavigator.RedrawPageByFrame(frameVo);
		}
		
		/**
		 * Move frame one depth up in page
		 */
		public function depthDown():void
		{
			DEPTH_DOWN.dispatch(this);
			PageNavigator.RedrawPageByFrame(frameVo);
		}
		
		/**
		 * Save frame properties
		 * > save frame propeties into photovo when there is a modification or when a frame is created (x, y, rotation, filters, bounds, cropping etc...)
		 * > call this method each time the frame is modified
		 * > Could have to be refined later on depending on how we will save those informtions in DB
		 * > Can be overriden in specific frame ex: CalendarFrame
		 */
		public function save():void
		{
			//Debug.log("Frame > save :");
			frameVo.x = x;
			frameVo.y = y;
			frameVo.rotation = rotation;
		}	
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public function destroy():void
		{		
			//Debug.log("Frame > destroy");
			//Deselect me of course
			DESELECT.dispatch(this);
			
			//then remove listeners!
			CREATED.removeAll();
			SELECT.removeAll();
			DESELECT.removeAll();
			BOUNDS_CHANGE.removeAll();
			DELETE.removeAll();
			
			DEPTH_UP.removeAll();
			DEPTH_DOWN.removeAll();
			//CLEAR.removeAll();
			
			EditionArea.EDITOR_RESIZE.remove(editorResizeHandler);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, onClick);
			this.removeEventListener(MouseEvent.ROLL_OVER, onOver);
			this.removeEventListener(MouseEvent.ROLL_OUT,onRollOut);
			
			frameVo = null;
		}
		
		
		/**
		 * check if frame contains point
		 */
		public function containsPoint( hitPoint : Point ):Boolean
		{
			if(!frameVo) return false;
			
			
			//var bounds:Rectangle = boundRect;
			//bounds.c
			
			return true;
		}
		
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public function deleteMe( withAnim : Boolean = true):void
		{
			DELETE.dispatch(this, withAnim);
		}
		
		
		/**
		 * clearMe do clear a frame and reset the CTA and empty frame state
		 * > it must be overriden
		 */
		public function clearMe():void
		{
			// page vo updated
			if(frameVo)	PageNavigator.RedrawPageByFrame(frameVo);
			// register delete frame
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			// update used/not used items
			ProjectManager.instance.remapPhotoVoList();
			// deselect from transform tool
			DESELECT.dispatch(this);
			
			//CLEAR.dispatch(this);
		}
		
		
		
		/**
		 * update frame rotation
		 */
		public function updateRotation( newRotation : Number ) :void
		{
			frameVo.rotation = newRotation;
			rotation = newRotation;
		}
		
		
		/**
		 * update frame rotation
		 */
		public function updatePosition( newX : Number, newY:Number ) :void
		{
			// security
			if(!frameVo){ Debug.warn("Frame > updatePosition : framevo is null..., we stop"); return; }
			
			frameVo.x = x = newX;
			frameVo.y = y = newY;
		}
		
		
		/**
		 * generate a bitmap data of current content
		 */
		public function getContentBitmapData(transparency:Boolean = true, bkgColor:Number = 0x000000):BitmapData
		{
			var qualityScale : Number = 3;
			var MAX_SIZE : Number = 2000;
			var frameWidth : Number = frameVo.width * FrameExporter.TEXT_FRAME_SECURITY_MARGIN;
			var frameHeight : Number = frameVo.height * FrameExporter.TEXT_FRAME_SECURITY_MARGIN;
			if(frameWidth *qualityScale > MAX_SIZE) qualityScale = MAX_SIZE/frameWidth;
			if(frameHeight *qualityScale > MAX_SIZE) qualityScale = MAX_SIZE/frameHeight;
			
			var bmd : BitmapData = new BitmapData(frameWidth*qualityScale, frameHeight*qualityScale,transparency, bkgColor);
			var m:Matrix = new Matrix();
			m.translate(content.width*.5, content.height*.5);
			m.scale(qualityScale,qualityScale);
			bmd.draw(content, m, null,null, null, true);
			return bmd;
		}
		
		
		/**
		 * show/hide swap highlight when swap mode is enabled
		 */ 
		public function swapHighlight(flag :Boolean) :void
		{
			if(flag){
				if(!swapHighlightGfx) swapHighlightGfx = new Sprite();
				addChild(swapHighlightGfx);
				var g:Graphics = swapHighlightGfx.graphics;
				g.clear();
				g.beginFill(Colors.YELLOW, .4);
				g.lineStyle(15, Colors.YELLOW, 1,true, LineScaleMode.NORMAL, CapsStyle.NONE);
				g.drawRect(-frameVo.width*.5, -frameVo.height*.5, frameVo.width, frameVo.height);
				g.endFill();
			} else
			{
				if(swapHighlightGfx && swapHighlightGfx.parent) swapHighlightGfx.parent.removeChild(swapHighlightGfx);
				swapHighlightGfx = null;
			}
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PROTECTED METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		protected function renderFrame():void
		{	
		}
		
		/**
		 * Overriden funciton by each extends of this class
		 * this is creating a call to action based on the type of the frame (photo, text, bkg..)
		 */
		protected function createCTA(alphaZone:Number = .6, colorZone:Number = Colors.GREY_LIGHT):void
		{
			//container
			cta = new Sprite();
			
			//semi-transparent zone to display frame bounds
			ctaBg =  new Sprite();
			var g:Graphics = ctaBg.graphics;
			g.lineStyle(1,colorZone,1,true, LineScaleMode.NONE);
			g.beginFill(colorZone,alphaZone);
			g.drawRect(-frameVo.width*.5,-frameVo.height*.5,frameVo.width, frameVo.height);
			g.endFill();
			cta.addChild(ctaBg);
			
			ctaBg.doubleClickEnabled = true;
			ctaBg.addEventListener(MouseEvent.DOUBLE_CLICK, ctaClickHandler);
			
			//container for icon and button
			iconContainer = new Sprite();
			iconContainer.name = "iconContainer";
			//iconContainer.opaqueBackground = 0xff00ff;
			
			//icon
			icon = new Bitmap(iconbmd);
			icon.smoothing = true;
			iconContainer.addChild(icon);
			//TweenMax.to(icon,0,{tint:overColor});
			if(frameVo.type == FrameVo.TYPE_TEXT)
				TweenMax.to(icon,0,{tint:Colors.BLUE_LIGHT});
			else
				TweenMax.to(icon,0,{tint:Colors.GREY_DARK});
			
			//Btn
			if(labelCta != "")
			{
				//add btn
				iconBtn = (frameVo.type == FrameVo.TYPE_TEXT)?SimpleButton.createFrameTextCTA(labelCta):SimpleButton.createFramePhotoCTA(labelCta);
				iconBtn.name = "btn";
				iconBtn.buttonMode = true;
				iconBtn.addEventListener(MouseEvent.CLICK, ctaClickHandler);
			}

			//if not editable, do not add anything else
			//if(frameVo.editable) //TODO PUT THAT BACK ANd handle old project
			//{
				cta.addChild(iconContainer);
				// set icon listener
				iconContainer.buttonMode = true;
				iconContainer.mouseChildren = false;
				iconContainer.addEventListener(MouseEvent.CLICK, ctaClickHandler);
				
			//}
			
			//addChild
			addChild(cta);

			//positionate
			updateCTA(EditionArea.editorScale);
		}
		
		/**
		 * Center CTA content in the middle of the frame
		 */
		protected function updateCTA(scaleFactor:Number):void
		{
			if(!scaleFactor) return; // do nothing
			if(!cta) return; // do nothing
			//if(!frameVo.editable) return; // do nothing  //TODO PUT THAT BACK ANd handle old project
			
			// update cta bg to frame size
			ctaBg.width = frameVo.width;
			ctaBg.height = frameVo.height;
			
			//show or hide the icon button
			var displayButton : Boolean = (iconBtn && (frameVo.height-10)*scaleFactor > icon.height + iconBtn.height + 10)? true : false;
			if(displayButton)
				displayButton = (iconBtn && (frameVo.width-10)*scaleFactor > iconBtn.width + 10)? true : false;
			
			if( displayButton ) {
				iconContainer.addChild(iconBtn);
				iconContainer.mouseChildren = true;
				iconContainer.buttonMode = false
				iconContainer.removeEventListener(MouseEvent.CLICK, ctaClickHandler);
			}
			else if(iconBtn && iconBtn.parent) {
				iconBtn.parent.removeChild(iconBtn);
				iconContainer.mouseChildren = false;
				iconContainer.buttonMode = true;
				iconContainer.addEventListener(MouseEvent.CLICK, ctaClickHandler);
			}
			
			
			//handle case when frame.height < icon.height
			icon.scaleX = icon.scaleY = 1;
			if(!displayButton)
			{
				if((frameVo.height- 10)*scaleFactor <= icon.height)
				{
					icon.height = (frameVo.height - 10)*scaleFactor;
					icon.scaleX = icon.scaleY;
				}
			}
			
			//scale icon container reversed to the editor scale, make it look like scale = 1;
			if(frameVo.type != FrameVo.TYPE_BKG)
				iconContainer.scaleX = iconContainer.scaleY = 1/scaleFactor;
			
			
			
			//positionate icon & button
			icon.x = -icon.width*.5;
			icon.y = -icon.height*.5;
			if(displayButton){
				iconBtn.x = -iconBtn.width*.5;
				icon.y = -( icon.height+iconBtn.height+10) *.5;
				iconBtn.y = icon.y + icon.height + 10;
			}
			
			// update listeners 
			//   (only icon can be clickabel to allow selection of the frame when empty -> REMOVED: if no room to display button, use full frame size as click handler
			// -> if enough room for button, button and icon are click handler
			/*cta.buttonMode = !displayButton;
			cta.removeEventListener(MouseEvent.CLICK, ctaClickHandler);
			iconContainer.removeEventListener(MouseEvent.CLICK, ctaClickHandler);
			if(!displayButton) 
			cta.addEventListener(MouseEvent.CLICK, ctaClickHandler);
			else
			iconContainer.addEventListener(MouseEvent.CLICK, ctaClickHandler);*/
		}
		
		
		
		/**
		 * Overriden funciton by each extends of this class
		 * this what the frame should do when the CTA is clicked
		 */
		protected function ctaClickHandler(e:MouseEvent = null):void
		{
			// This must be overriden
		}
		
		
		
		protected function setListeners():void
		{
			removeListeners();
			rollOut();
			//if(!frameVo.editable)return;//TODO put that back with condition for old project do nothing
			
			EditionArea.EDITOR_RESIZE.add(editorResizeHandler);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onClick);
			this.addEventListener(MouseEvent.ROLL_OVER, onOver);
			this.addEventListener(MouseEvent.ROLL_OUT,onRollOut);
		}
		
		public function removeListeners():void
		{
			EditionArea.EDITOR_RESIZE.remove(editorResizeHandler);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, onClick);
			this.removeEventListener(MouseEvent.ROLL_OVER, onOver);
			this.removeEventListener(MouseEvent.ROLL_OUT,onRollOut);
		}
		
		/**
		 * Overriden funciton by each extends of this class
		 * Editor scale has resized, let update what needs to be updated (icon)
		 */
		protected function editorResizeHandler(scaleFactor:Number):void
		{
			if(cta)
				updateCTA(scaleFactor);
		}			
		
		/**
		 * create view
		 */
		protected function construct():void
		{
			//Cronstruction finish, tell everyone!
			CREATED.dispatch(this);			
		}
		
		/**
		 * Update frame boundaries value
		 * > after a resize (or zoom)
		 * > after a crop
		 */
		public function updateFrameBounds():void
		{
			// say it to everyone
			BOUNDS_CHANGE.dispatch(this);
		}
		
		
		/**
		 * When image is clicked
		 * + send signal to start edition
		 */
		protected function onClick(e:MouseEvent):void
		{
			if(hasError) return;
			SELECT.dispatch(this);
		}
		
		
		
		
		
		/**
		 * ------------------------------------ ROLL OVER / ROLL OUT -------------------------------------
		 */
		
		
		
		// ROLL OVER
		protected function onOver(e:MouseEvent):void
		{
			if(TransformTool.currentAction == null)
				over();
		}
		protected function over(force:Boolean = false):void
		{
			if(hasError) return;
			
			showOverEffect(true);
			
			if(!frameVo.isEmpty)
			{
				/* TINT EFFECT
				TweenMax.killTweensOf(this);
				if(this is FrameBackground) TweenMax.to(this, .0, {colorTransform:{tint:overColor, tintAmount:0.2}});
				else TweenMax.to(this, .0, {colorTransform:{tint:overColor, tintAmount:0.2}});
				*/
				
				// border effect
				//showOverEffect();
				
				if(isSticked && allowMove)
					CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
			/*
			else
			{
				if(cta){
					TweenMax.killTweensOf(cta.getChildAt(0));
					TweenMax.to(cta.getChildAt(0), .0, {colorTransform:{tint:overColor, tintAmount:0.5}});
				}
			}
			*/
		}
		
		
		/**
		 * on roll out
		 */
		protected function onRollOut(e:MouseEvent):void
		{
			rollOut();
		}
		protected function rollOut(force :Boolean = false):void
		{
			//if(hasError) return ;
			showOverEffect(false);
			
			
			//highLight(false);
			if(!frameVo.isEmpty)
			{
				//showOverEffect(false);
				/*
				TweenMax.killTweensOf(this);	
				TweenMax.to(this, .0, {colorTransform:{tint:overColor, tintAmount:0}});
				*/
			}
			/*
			else if(cta)
			{
				TweenMax.killTweensOf(cta.getChildAt(0));
				TweenMax.to(cta.getChildAt(0), .0, {colorTransform:{tint:overColor, tintAmount:0}});
			}
			*/
			
			CursorManager.instance.reset();
		}
		
		// show over GFX
		private var overGFX : Sprite;
		private function showOverEffect(flag :Boolean = true) :void
		{
			if(flag){
				if(!overGFX) overGFX = new Sprite();
				addChild(overGFX);
				var g:Graphics = overGFX.graphics;
				g.clear();
				//g.beginFill(Colors.YELLOW, .1);
				g.lineStyle(3, Colors.YELLOW, 1,true, LineScaleMode.NONE, CapsStyle.SQUARE);
				g.drawRect(-frameVo.width*.5, -frameVo.height*.5, frameVo.width, frameVo.height);
				g.endFill();
			} else
			{
				if(overGFX && overGFX.parent) overGFX.parent.removeChild(overGFX);
				overGFX = null;
			}
		}
	}
}