package view.edition
{
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import be.antho.data.ResourcesManager;
	import be.antho.form.Watermark;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import comp.CoverClassicOptionBtn;
	import comp.FontSelector;
	import comp.button.SimpleButton;
	
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.TooltipVo;
	
	import library.cover.classic.editor;
	import library.cover.classic.corner.icon;
	
	import manager.CoverManager;
	import manager.ProjectManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.pageNavigator.PageNavigator;

	public class PageCoverClassicArea extends PageArea
	{
		private static const GOLD:Number = 0xEBCA41;
		private static const SILVER:Number = 0xDBDBD9;
		
		private static const STATE_CTA_ADD:String = "STATE_CTA_ADD";
		private static const STATE_CTA_EDIT:String = "STATE_CTA_EDIT";
		
		
		private var editor:MovieClip;
		private var panelOptions:MovieClip;
		private var loader:Loader;
		private var variables:URLVariables;
		private var panelText:MovieClip;
		private var voCasted:PageCoverClassicVo;
		private var fontSelector:FontSelector;
		private var line1W:Watermark;
		private var line2W:Watermark;
		private var spineW:Watermark;
		private var editionW:Watermark;

		private var showTextPanelBtn:SimpleButton;

		private var preview:Bitmap = new Bitmap();

		private var coverBtnViews:Vector.<CoverClassicOptionBtn>;

		private var cornerBtnViews:Vector.<CoverClassicOptionBtn>;
		private var spineShowTextOnPanelBtn:SimpleButton;
		private var editorPosX:Number;
		private var visibilitySaver:Object = {};

		public function PageCoverClassicArea(typeVo:PageCoverClassicVo)
		{
			super(typeVo);
			voCasted = typeVo;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHOD
		////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHOD
		////////////////////////////////////////////////////////////////
		/**
		 * destroy view
		 * clean memory
		 */
		override public function destroy():void
		{
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onloadCoverPreviewHandler);
			loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onloadError);
			loader = null;
			CoverClassicOptionBtn.COVER_OPTION_CLICK.removeAll();
		}
		
		/**
		 * Construction of the view
		 * Diplay the editor, localize it, set it up
		 * 
		 */
		override protected function construct():void
		{
			editor = new library.cover.classic.editor();
			addChild(editor);
			
			//ref
			panelText = editor.textPanel;
			panelOptions = editor.options;
			
			//Localisation
			localize();
			
			//SetUp options
			setupOptions();
			
			//SetUp Text options
			setupTextOptions();
			
			//setupLoader
			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onloadCoverPreviewHandler);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onloadError);
			
			//overide bounds (not using the real cover bounds, it's just an editor)
			//pageVo.bounds = new Rectangle(0,0,editor.width, editor.height);
			
			//Default params for cover //TODO: When saved, fill this params with the saved ones
			editor.preview.defaultPreview.alpha = .1;
			variables = new URLVariables();
			variables.line1 = "";
			variables.line2 = "";
			variables.spine = "";
			variables.edition = "";
			variables.font = "Arial";
			variables.color = voCasted.color;
			variables.corners = voCasted.corners;
			variables.cover = voCasted.cover;
			
			//Add text panel show button
			showTextPanelBtn = new SimpleButton("edition.cover.classic.editor.btn.showtext.title",Colors.BLUE_LIGHT,showTextPanel,Colors.BLUE_LIGHT, Colors.WHITE,Colors.YELLOW,.2,false);
			editor.addChild(showTextPanelBtn);
			
			spineShowTextOnPanelBtn = new SimpleButton("edition.cover.classic.editor.btn.showtext.title",Colors.BLUE_LIGHT,showTextPanel,Colors.BLUE_LIGHT, Colors.WHITE,Colors.YELLOW,.2,false);
			spineShowTextOnPanelBtn.rotation = -90;
			
			
			//add text button label
			showCtaText();
			
			//close Btn
			panelText.close.buttonMode = true;
			panelText.close.addEventListener(MouseEvent.CLICK, hideTextPanel);
			
			//preview
			editorPosX = editor.preview.x;
			loadCoverPreview();
			
			//Cover infos
			updateTitle();
			
			// show/hide empty frames if fullscreen
			updateFullScreenState();
			
		}
		
		
		/**
		 * update page content depending on state fullscreen (preview) or normal
		 * > show/hide empty frames
		 */
		override public function updateFullScreenState():void
		{
			var isPreview : Boolean = EditionArea.isPreview();
			
			panelText.visible = (isPreview)?false:visibilitySaver.panelText;
			panelOptions.visible = (isPreview)?false:visibilitySaver.panelOptions;
			
			if(!isPreview)
				hideTextPanel()
			else
			showCtaText(false);
			
			editor.x = (isPreview)?editorPosX + 150:editorPosX;
			
			opaqueBackground = (isPreview)?Colors.FULLSCREEN_COLOR:Colors.BACKGROUND_COLOR; 
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHOD
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Clear text from cover
		 * Reset Text Panel
		 */
		private function clearAllTexts():void
		{
			line1W.reset();
			line2W.reset();
			spineW.reset();
			editionW.reset();
			
			editor.line1.text = voCasted.line1 = "";
			editor.line2.text = voCasted.line2 = "";
			editor.spine.text = voCasted.spine = "";
			editor.edition.text = voCasted.edition = "";

			
		}
		
		/**
		 * show text panel visible
		 */
		private function showCtaText(flag:Boolean = true):void
		{
			if(!flag)
			{
				showTextPanelBtn.visible = false;
				if(spineShowTextOnPanelBtn.parent)
				editor.removeChild(spineShowTextOnPanelBtn);
				return;
			}
			
			showTextPanelBtn.visible = true;
			if(voCasted.line1 != "" || voCasted.line2 != "" || voCasted.spine != "" || voCasted.edition != "")
			{
				showTextPanelBtn.setLabel("edition.cover.classic.editor.btn.edittext.title");
				showTextPanelBtn.y = editor.preview.height  - showTextPanelBtn.height - 65;
				if(spineShowTextOnPanelBtn.parent)
				editor.removeChild(spineShowTextOnPanelBtn);
			}
			else
			{
				showTextPanelBtn.setLabel("edition.cover.classic.editor.btn.showtext.title");
				showTextPanelBtn.y = (editor.preview.height  - showTextPanelBtn.height >> 1);
				editor.addChild(spineShowTextOnPanelBtn);
				spineShowTextOnPanelBtn.x = 22;
				spineShowTextOnPanelBtn.y = editor.preview.height - ((editor.preview.height - spineShowTextOnPanelBtn.height)/2);
			}
			
			showTextPanelBtn.x = (editor.preview.width - showTextPanelBtn.width >>1) + 50;
		}
		
		/**
		 * show text panel visible
		 */
		private function showTextPanel(e:Event = null):void
		{
			if(panelText.alpha != 1)
			{
				showCtaText(false);
				panelText.alpha = 1;
				panelText.visible = true;
				panelText.x = panelText.posX;
				TweenMax.killTweensOf(panelOptions);
				TweenMax.killTweensOf(panelText);
				TweenMax.from(panelText,.5,{autoAlpha:0, x:panelText.posX+20});
				TweenMax.to(panelOptions,.5,{autoAlpha:0});
			}
		}
		
		/**
		 * hide text panel visible
		 */
		private function hideTextPanel(e:Event = null):void
		{
			showCtaText();
			TweenMax.killTweensOf(panelOptions);
			TweenMax.killTweensOf(panelText);
			TweenMax.to(panelText,.5,{autoAlpha:0, x:panelText.posX});
			TweenMax.to(panelOptions,.5,{autoAlpha:1});
			
		}
		
		
		/**
		 * an option has benn clicked
		 * check type of option and update cover
		 */
		private function optionClickHandler(btn:CoverClassicOptionBtn):void
		{
			switch(btn.type)
			{
				case ProductsCatalogue.COVER_FABRIC_LEATHER:
					variables.cover = voCasted.cover = btn.id;
					voCasted.coverLabelName = btn.labelName;
					voCasted.coverFabric = ProductsCatalogue.COVER_FABRIC_LEATHER;
					break;
				
				case ProductsCatalogue.COVER_FABRIC_LINEN:
					variables.cover =  voCasted.cover = btn.id;
					voCasted.coverLabelName = btn.labelName;
					voCasted.coverFabric = ProductsCatalogue.COVER_FABRIC_LINEN;
					break;
				
				case "corner":
					variables.corners = voCasted.corners = btn.id;
					ProjectManager.instance.updateProjecPrice();
					break;
			}
			updateTitle();			
			loadCoverPreview();
			PageNavigator.RedrawPage(pageVo);
		}
		
		private function updateTitle():void
		{
			panelOptions.chosen_cover.text = "( "+CoverManager.instance.getCoverLabelName()+" )";
		}		
		
		
		
		/**
		 * Load the cover preview with specific parameters
		 * Color, text, corners etc..
		 */
		private function loadCoverPreview():void
		{
			Debug.log("PageCoverClassicArea :");
			if(!variables || !loader) {
				Debug.warn("PageCoverClassicArea > loadCoverPreview > variables is : "+ variables + " and loader is : "+loader);
				return;
			}
			var pUrl : String = Infos.config.serverNormalUrl+"/wizard/images/preview-cover.php?"+variables.toString();
			Debug.log(" >> url : "+pUrl);
			loader.load(new URLRequest(pUrl));
			Debug.log("Load Cover preview variables: "+variables.toString());
			
		}	
		private function onloadCoverPreviewHandler(e:Event):void
		{
			Debug.log("PageCoverClassicArea > onloadCoverPreviewHandler");
			if(preview.bitmapData)
				preview.bitmapData.dispose();
			
			preview.bitmapData = Bitmap(loader.content as Bitmap).bitmapData;
			editor.preview.addChild(preview);
			preview.alpha = 0;
			TweenMax.killTweensOf(preview);
			TweenMax.to(preview,1,{alpha:1});
			
		}
		protected function onloadError(event:ErrorEvent):void
		{
			Debug.warn("PageCoverClassicArea > onloadError > we do not report this error, we let the user continue. ERROR : "+event.text);
			//Debug.error(event.text);
		}
		
		
		
		/**
		 * SET Text options up!
		 * Create font selector, retreive exisiting text if needed
		 * set colors or not etc..
		 */
		private function setupTextOptions():void
		{
			//Add text panel buttons
			var validateTextBtn:SimpleButton = new SimpleButton("common.validate",Colors.WHITE,validateTexts,Colors.GREEN,Colors.WHITE,Colors.GREEN);
			validateTextBtn.x = panelText.title.x + panelText.title.width - validateTextBtn.width;
			validateTextBtn.y = panelText.height - validateTextBtn.height -  panelText.title.x;
			panelText.addChild(validateTextBtn);
			
			//Add text panel buttons
			var deleteTextBtn:SimpleButton = new SimpleButton("edition.cover.classic.editor.panel.text.button.delete",Colors.WHITE,clearAllTexts,Colors.RED,Colors.WHITE,Colors.RED);
			deleteTextBtn.x = validateTextBtn.x - deleteTextBtn.width - 10;
			deleteTextBtn.y = validateTextBtn.y;
			panelText.addChild(deleteTextBtn);
			
			//Font Selector
			fontSelector = new FontSelector(TextFormats.AVAILABLE_COVER_FONTS,voCasted.fontName);
			fontSelector.height = panelText.gold.height;
			fontSelector.scaleX = fontSelector.scaleY;
			fontSelector.x = panelText.gold.x - fontSelector.width - 5;
			fontSelector.y = panelText.gold.y;
			panelText.addChild(fontSelector);
			fontSelector.FONT_CHOSEN.add(fontChosen);
			
			//setup text on cover
			TextField(panelText.line1).multiline = 
				TextField(panelText.line2).multiline = 
				TextField(panelText.spine).multiline = 
				TextField(panelText.edition).multiline = false;
			
			editor.line1.text = voCasted.line1;
			editor.line2.text = voCasted.line2;
			editor.spine.text = voCasted.spine;
			editor.edition.text = voCasted.edition;
			
			//setup input field
			if(voCasted.line1 != ""){panelText.line1.text = voCasted.line1;panelText.line1.alpha = 1};
			if(voCasted.line2 != ""){panelText.line2.text = voCasted.line2;panelText.line2.alpha = 1};
			if(voCasted.spine != ""){panelText.spine.text = voCasted.spine;panelText.spine.alpha = 1};
			if(voCasted.edition != ""){panelText.edition.text = voCasted.edition;panelText.edition.alpha = 1};
			
			
			//Update text on the fly
			panelText.line1.addEventListener(Event.CHANGE, textChangeHandler);
			panelText.line2.addEventListener(Event.CHANGE, textChangeHandler);
			panelText.spine.addEventListener(Event.CHANGE, textChangeHandler);
			panelText.edition.addEventListener(Event.CHANGE, textChangeHandler);
			
			//show text panel onclick
			editor.line1.addEventListener(MouseEvent.CLICK, showTextPanel);
			editor.line2.addEventListener(MouseEvent.CLICK, showTextPanel);
			editor.spine.addEventListener(MouseEvent.CLICK, showTextPanel);
			editor.edition.addEventListener(MouseEvent.CLICK, showTextPanel);
			
			//colors
			//panelText.gold.addEventListener(MouseEvent.CLICK, colorChangeHandler);
			//panelText.silver.addEventListener(MouseEvent.CLICK, colorChangeHandler);
			var tooltipColorGold:TooltipVo = new TooltipVo("cover.classic.text.color.gold");
			var tooltipColorSilver:TooltipVo = new TooltipVo("cover.classic.text.color.silver");
			ButtonUtils.makeButton(panelText.gold,colorChangeHandler,tooltipColorGold);
			ButtonUtils.makeButton(panelText.silver,colorChangeHandler,tooltipColorSilver);
			
			//set default Font
			fontChosen(voCasted.fontName);
			//set default Color
			colorChosen(voCasted.color);
			
			//max chars
			TextField(editor.line1).maxChars = panelText.line1.maxChars = (Infos.project.docPrefix == "S")?22:30;
			TextField(editor.line2).maxChars = panelText.line2.maxChars = (Infos.project.docPrefix == "S")?30:38;
			TextField(editor.spine).maxChars = panelText.spine.maxChars = (Infos.project.docPrefix == "S")?22:40;
			TextField(editor.edition).maxChars = panelText.edition.maxChars = 2;
			
			//tabIndex
			panelText.line1.tabIndex = 1;
			panelText.line2.tabIndex = 2;
			panelText.spine.tabIndex = 3;
			panelText.edition.tabIndex = 4;
			
			//Animation
			panelText.alpha = 0;
			panelText.visible = false;
			panelText.posX = panelText.x;
			panelText.posY = panelText.y;
			
		}
		
		
		private function fontChosen(fontName:String):void
		{
			setFont(editor.line1,fontName);
			setFont(editor.line2,fontName);
			setFont(editor.spine,fontName);
			setFont(editor.edition,fontName);
			
			voCasted.fontName = fontName;
		}
		
		private function validateTexts():void
		{
			hideTextPanel();
			ProjectManager.instance.updateProjecPrice();
			PageNavigator.RedrawPage(pageVo);
		}
		
		private function colorChangeHandler(e:MouseEvent):void
		{
			colorChosen(e.target.name);	
		}
		
		private function colorChosen(colorName:String):void
		{
			setColor(editor.line1,colorName);
			setColor(editor.line2,colorName);
			setColor(editor.spine,colorName);
			setColor(editor.edition,colorName);
			
			panelText.gold.alpha = (panelText.gold == panelText[colorName])?1:.5;
			panelText.silver.alpha = (panelText.silver == panelText[colorName])?1:.5;
			voCasted.color = colorName;
		}
		/**
		 * SET color to cover text
		 * save color in Vo
		 */
		private function setColor(tf:TextField,colorName:String):void
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.color = (colorName == "gold")?GOLD:SILVER;
			tf.setTextFormat(tformat);
			voCasted.color = colorName;
		}
		
		/**
		 * SET color to cover text
		 * save color in Vo
		 */
		private function setFont(tf:TextField,fontName:String):void
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = fontName;
			tf.setTextFormat(tformat);
			
		}
		
		private function textChangeHandler(e:Event):void
		{
			var tf:TextField = e.target as TextField;
			var targetTf:TextField = editor.getChildByName(tf.name) as TextField;
			targetTf.text = tf.text;
			voCasted[tf.name] = tf.text;
			
			setColor(targetTf,voCasted.color);
			setFont(targetTf,voCasted.fontName);
		}
		
		/**
		 * SET options up!
		 * Chekc available color for leather and linen fabric
		 */
		private function setupOptions():void
		{
			var optionXML:XML = ProjectManager.instance.coversClassicOptionsXML;
			//get the availalble options for this project type

			//var productList : XMLList = nodeList.(@product == ProjectManager.instance.project.docType);
			//var availableOptionsIDsList:XMLList = optionXML.cover_stock..children().(descendants("node").@product.contains(ProjectManager.instance.project.docType));
			//var availableOptionsIDsList:XMLList = optionXML.cover_stock..children().(descendants("node").@product.contains(ProjectManager.instance.project.docType));
			var availableOptionsIDsList:XMLList = optionXML.cover_stock.children().(descendants("node").@product.contains(ProjectManager.instance.project.docType));//(descendants("node").(@product == ProjectManager.instance.project.docType));//.(descendants("node").(@product == (ProjectManager.instance.project.docType)).length()>0);
			var leatherOptions:Vector.<XML> = new Vector.<XML>();
			var linenOptions:Vector.<XML> = new Vector.<XML>();
			var cornerOptions:Vector.<XML> = new Vector.<XML>();
			
			for (var i:int = 0; i < availableOptionsIDsList.length(); i++) 
			{
				var node:XML = availableOptionsIDsList[i];
				var stockList:XMLList = node.node.(@product == ProjectManager.instance.project.docType); // TODO : reset this!!! removed because not working --->  && @stock.contains("ok"));
				if(stockList.length() > 0)
				{
					switch(String(node.@type))
					{
						case "leather":
							leatherOptions.push(node);
							break;
						
						case "linen":
							linenOptions.push(node);
							break;
						
						case "corner":
							cornerOptions.push(node);
							break;
					}
				}
			}
			
			var container:MovieClip;
			var optionBtn:CoverClassicOptionBtn;
			var defaultCoverFound:Boolean = false; // make sure the cover in vo is still in stock else choose the first one.
			coverBtnViews = new Vector.<CoverClassicOptionBtn>();
			cornerBtnViews = new Vector.<CoverClassicOptionBtn>();
			
			//leather options
			container = panelOptions.leather;
			for (i = 0; i < leatherOptions.length; i++) 
			{
				optionBtn = new CoverClassicOptionBtn(ProductsCatalogue.COVER_FABRIC_LEATHER,String(leatherOptions[i].name()),leatherOptions[i].@color); //createBtnFromXML(leatherOptions[i]);
				optionBtn.y = 3;
				optionBtn.x = 3+(optionBtn.width+3)*i;
				container.addChild(optionBtn);
				coverBtnViews.push(optionBtn);
				
				optionBtn.selected = (optionBtn.id == voCasted.cover);
				if(optionBtn.selected)
					defaultCoverFound = true;
			}
			container.bg.width = container.width + 3;
			
			//linen options
			container = panelOptions.linen;
			for (i = 0; i < linenOptions.length; i++) 
			{
				optionBtn = new CoverClassicOptionBtn(ProductsCatalogue.COVER_FABRIC_LINEN,String(linenOptions[i].name()),linenOptions[i].@color);
				optionBtn.y = 3;
				optionBtn.x = 3 + (optionBtn.width+3)*i;
				container.addChild(optionBtn);
				coverBtnViews.push(optionBtn);
				
				optionBtn.selected = (optionBtn.id == voCasted.cover);
				if(optionBtn.selected)
					defaultCoverFound = true;
			}
			container.bg.width = container.width + 3;
			
			//check if default cover is still in stock else choose first available one
			Debug.log("defaultCoverFound: "+defaultCoverFound);
			if(!defaultCoverFound)
			{
				voCasted.cover = coverBtnViews[0].id;
				voCasted.coverLabelName = coverBtnViews[0].labelName;
				voCasted.coverFabric = coverBtnViews[0].type;
				coverBtnViews[0].selected = true;
			}
			Debug.log("voCasted.cover: "+voCasted.cover);
			Debug.log("voCasted.coverLabelName: "+voCasted.coverLabelName);
			Debug.log("voCasted.coverFabric: "+voCasted.coverFabric);
			//corner options
			for (i = 0; i < cornerOptions.length; i++) 
			{
				var id:String = String(cornerOptions[i].name());
				container = new MovieClip();
				container.x = panelOptions.linen.x;
				container.y = panelOptions.corner_price_label.y + panelOptions.corner_price_label.height + 25;
				panelOptions.addChild(container);
				
				optionBtn = new CoverClassicOptionBtn("corner",id);
				optionBtn.y = container.height - optionBtn.height >> 1;
				optionBtn.x = (optionBtn.width+10)*i;
				container.addChild(optionBtn);
				cornerBtnViews.push(optionBtn);
				optionBtn.selected = (optionBtn.id == voCasted.corners);
				
				if(i==cornerOptions.length-1)
				{
					var noneBtn:CoverClassicOptionBtn = new CoverClassicOptionBtn("corner","none");
					noneBtn.y = optionBtn.y;
					noneBtn.x = optionBtn.x + optionBtn.width+ 10;
					container.addChild(noneBtn);
					cornerBtnViews.push(noneBtn);
					noneBtn.selected = (noneBtn.id == voCasted.corners);
				}
			}
			//click Signal
			CoverClassicOptionBtn.COVER_OPTION_CLICK.add(optionClickHandler);
			
			//textfiels setup
			panelOptions.cover_label.autoSize = "left";
			panelOptions.cover_label.multiline = true;
		}		
		
		
		/**
		 * Set text to lable in the current locale
		 * 
		 */
		private function localize():void
		{
			ResourcesManager.setText(panelOptions.cover_label, "edition.cover.classic.editor.panel.options.title1");
			ResourcesManager.setText(panelOptions.title_corner, "edition.cover.classic.editor.panel.options.title2");
			ResourcesManager.setText(panelOptions.corner_price_label, "edition.cover.corner.price");
			ResourcesManager.setText(panelOptions.title_leather, "edition.cover.leather");
			ResourcesManager.setText(panelOptions.title_linen, "edition.cover.fabric");
			
			ResourcesManager.setText(panelText.price_label, "edition.cover.text.price");
			ResourcesManager.setText(panelText.title, "edition.cover.classic.editor.panel.text.title");
			
			line1W = new Watermark(panelText.line1,ResourcesManager.getString("edition.cover.classic.editor.panel.text.watermark.line1"));
			line2W = new Watermark(panelText.line2,ResourcesManager.getString("edition.cover.classic.editor.panel.text.watermark.line2"));
			spineW = new Watermark(panelText.spine,ResourcesManager.getString("edition.cover.classic.editor.panel.text.watermark.spine"));
			editionW = new Watermark(panelText.edition,ResourcesManager.getString("edition.cover.classic.editor.panel.text.watermark.edition"));
			
			panelText.gold.gotoAndStop("gold");
			panelText.silver.gotoAndStop("silver");
			
			panelOptions.chosen_cover.y = panelOptions.cover_label.y + panelOptions.cover_label.height;
			
		}
	}
}