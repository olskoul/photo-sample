﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.edition 
{
	import com.adobe.serialization.json.JSONEncoder;
	import com.bit101.components.Calendar;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	
	import be.antho.utils.MathUtils2;
	
	import comp.DefaultTooltip;
	
	import data.FrameVo;
	import data.Infos;
	
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import mcs.moveIcon;
	import mcs.resizeIcon;
	import mcs.rotateIcon;
	import mcs.transformToolbar;
	import mcs.icons.handIcon;
	
	import org.osflash.signals.Signal;
	import org.osflash.signals.natives.INativeDispatcher;
	
	import utils.Colors;
	import utils.CursorManager;
	import utils.Debug;
	
	import view.edition.pageNavigator.PageNavigator;
	import view.edition.toolbar.BorderToolBar;
	import view.edition.toolbar.MaskToolBar;
	import view.edition.toolbar.OutsideTextEditorToolbar;
	import view.edition.toolbar.PopartToolBar;
	import view.edition.toolbar.ShadowToolbar;
	import view.edition.toolbar.TextBackgroundToolbar;
	import view.edition.toolbar.TransformToolInfoBox;
	import view.popup.PopupAbstract;


	/**
	 * Transform tool is the container of all image editing tools
	 * It stick above a frame that need to be edited 
	 * > resize icon to resize the frame
	 * > move listener to move the frame
	 * > move content to move image inside the frame
	 * > rotation icon to rotate frame 
	 * > transform toolbar with other transform elements
	 * 
	 * WARNING : For performance choice, it should only have one instance of this component.
	 * 
	 */
	public class TransformTool extends Sprite
	{
		// Signals
		
		// edition types
		//private const EDIT_SET_FOCUS : String = "EDIT_SET_FOCUS";
		private const EDIT_MOVE : String = "EDIT_MOVE";
		private const EDIT_ROTATE : String = "EDIT_ROTATE";
		private const EDIT_RESIZE : String = "EDIT_RESIZE";
		private const EDIT_CROP : String = "EDIT_CROP";
		private const EDIT_INSIDE_MOVE : String = "EDIT_INSIDE_MOVE";
		
		
		public static var currentAction : String = null;
		
		// view
		private var frameBorder:Sprite; // the border of the selected area
		private var frameHit:Sprite; // the hit area of the frame
		private var rotateIcon:Sprite; // the icon for the rotation
		private var resizeIcon:Sprite; // the
		private var moveIcon:Sprite; // the inside frame move icon
		private var toolbar:TransformToolBar; // the toolbar
		private var popartToolBar:PopartToolBar;
		private var borderToolBar:BorderToolBar;
		private var masksToolbar:MaskToolBar;
		private var shadowToolbar:ShadowToolbar;
		private var textBackgroundToolbar:TextBackgroundToolbar;
		private var textEditorToolbar:OutsideTextEditorToolbar;
		private var editionArea:EditionArea; // locale reference to the edition Area view
		private var snapLinesContainer:Sprite;
		private var infoWidth:TransformToolInfoBox;
		private var infoHeight:TransformToolInfoBox;
		
		// the edited content
		private var _frame:Frame;
		
		// data
		private static var instance : TransformTool;
		private var currentButton : CornerButton = null;
		private var resizeButtons:Vector.<CornerButton> = new Vector.<CornerButton>;
		private var outsideClickZone : Sprite = new Sprite();
		private var currentPage : PageArea;
		private var isDragging:Boolean = false;
		private var isSticked:Boolean = false;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		
		
		public function TransformTool(editionArea:EditionArea) 
		{
			// check for instance
			if(instance) {
				Debug.warn("Transform tool should be instantiate only once");
				return;
			}
			
			instance = this;
			
			//Reference to the edition area
			this.editionArea = editionArea;
			
			// create and init ui
			rotateIcon = new mcs.rotateIcon();
			addChild(rotateIcon);
			
			//infos frame
			infoWidth = new TransformToolInfoBox();
			addChild(infoWidth);
			infoHeight = new TransformToolInfoBox();
			addChild(infoHeight);
			
			//create border and buttons
			createFrameBorder();
			
			// the centered move icon
			moveIcon = new mcs.moveIcon();
			addChild(moveIcon);
			
			
			toolbar = new TransformToolBar(editionArea);
			toolbar.POPART_CLICKED.add(togglePopArtToolbar);
			toolbar.BORDER_CLICKED.add(toggleBorderToolbar);
			toolbar.MASKS_CLICKED.add(toggleMasksToolbar);
			toolbar.TEXT_BACKGROUND_CLICKED.add(toggleTextBackgroundToolbar);
			toolbar.SHADOW_CLICKED.add(toggleShadowToolbar);
			toolbar.ROTATION_STEP_CLICKED.add(rotationStepClicked);
			
			// init listeners
			setListeners();
			
			
		}
		
		/*public function destroy():void
		{
			unstick();
			//Reference to the edition area
			this.editionArea = null;
			
			// create and init ui
			if(rotateIcon)
			{
				if(rotateIcon.parent)
					rotateIcon.parent.removeChild(rotateIcon);
				rotateIcon = null;
			}
			
			if(infoWidth)
			{
				if(infoWidth.parent)
					infoWidth.parent.removeChild(infoWidth);
				infoWidth = null;
			}
			if(infoHeight)
			{
				if(infoHeight.parent)
					infoHeight.parent.removeChild(infoHeight);
				infoHeight = null;
			}
			
			if(frameBorder)
			{
				if(frameBorder.parent)
					frameBorder.parent.removeChild(frameBorder);
				frameBorder = null;
			}
			
			if(resizeButtons && resizeButtons.length > 1)
			{
				for (var i:int = 0; i < resizeButtons.length; i++) 
				{
					var btn : CornerButton =resizeButtons[i];
					
					btn.removeEventListener(MouseEvent.MOUSE_DOWN, cornerButtonPressed);
					if(btn.parent)
						btn.parent.removeChild(btn);
					btn = null;
				}
				resizeButtons = new Vector.<CornerButton>;
			}
			
			
			if(moveIcon)
			{
				if(moveIcon.parent)
					moveIcon.parent.removeChild(moveIcon);
				moveIcon = null;
			}
			
			toolbar.POPART_CLICKED.removeAll();
			toolbar.BORDER_CLICKED.removeAll();
			toolbar.MASKS_CLICKED.removeAll();
			toolbar.TEXT_BACKGROUND_CLICKED.removeAll();
			toolbar.SHADOW_CLICKED.removeAll();
			toolbar.ROTATION_STEP_CLICKED.removeAll();
			
			toolbar.destroy();
			
		}*/
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *GET and SET Current frame
		 */
		public function get frame():Frame
		{
			return _frame;
		}

		public function set frame(value:Frame):void
		{
			if(value)
			{
				//save current edited pageVo
				PagesManager.instance.currentEditedPage = PagesManager.instance.getPageVoByFrameVo(value.frameVo);
			}
			
			//Set value
			_frame = value;
		}

		/**
		 * add transfrom tool to a frame
		 */
		public function stickToFrame( newFrame : Frame ):void
		{	
			Debug.log("TransformTool > stickToFrame : "+frame);
			if(frame) unstick();
			
			// security
			if(!newFrame){
				Debug.warn("TransformTool > stickToFrame > new Frame is null, unstick");
				unstick();
				return;
			}
			if(!newFrame.frameVo){
				Debug.warn("TransformTool > stickToFrame > new Frame has no frameVo, unstick");
				unstick();
				return;
			}
			
			//
			currentPage = PagesManager.instance.getPageAreaByFrameVo( newFrame.frameVo );
			
			// be sure there is no tint on this
			TweenMax.to(this, 0, {tint:null});
			 
			isSticked = true;
			
			// stick transform tool
			frame = newFrame;
			frame.isSticked = true;
			editionArea.addChild(this); 
			frame.BOUNDS_CHANGE.add(onFrameBoundsChange);
			PageNavigator.RedrawPageByFrame(frame.frameVo);
			
			// highlight current edited page
			editionArea.highLightEditedPage();
			
			// get frame bounds
			var fb:Rectangle = frame.boundRectScaled;
			updateIcons();
			
			
			// positionate tool on frame
			var point:Point = new Point(frame.x,frame.y);
			point =  frame.parent.localToGlobal(point);
			point = editionArea.globalToLocal(point);
			this.x = Math.round(point.x);
			this.y = Math.round(point.y);
			updateRotation(frame.rotation); 
			
			
			// create border and resize buttons
			updateBorder();
			
			
			// stick toolbar
			
			// if frame text linked to calendar frame and no frame photo are linked
			/*
			if(frame is FrameCalendar && (frame as FrameCalendar).linkedFrame is FrameText) {
				editTextOusideFrame();
			}
			*/
			
			// if frame is spine or edition
			if(frame.frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE || frame.frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION) {
				editTextOusideFrame();
			}
			else
			{
				if(frame.allowToolBar) toolbar.stickToFrame(frame);
			}
			
			
			// update toolbar
			UserActionManager.instance.updateToolbar();
			
		}
		
		
		
	
		
		/**
		 * Helper
		 * */
		/*
		private function drawPoint(point:Point, scope:*, color:Number):void
		{
			scope.graphics.clear();
			scope.graphics.beginFill(color,.3);
			//scope.graphics.(point.x,point.y,10);
			scope.graphics.drawCircle(point.x,point.y,10);
			scope.graphics.endFill();
		}
		*/
		
		
		/**
		 *
		 */
		public static function get frameBounds():Rectangle
		{
			if(!instance.frameBorder || !instance.editionArea) return null;
			return instance.frameBorder.getBounds(instance.editionArea);
		}
		
		
		/**
		 * unstick the transform tool from a frame
		 * with a static function for shortcut
		 * - This function should completely clean the transform tool and action process.
		 */
		public static function unstick():void
		{
			if(instance) instance.unstick();
		}
		public function unstick(e:Event = null, force:Boolean = false):void
		{
			//if(!isSticked && !force) return; // can't be done as this is also used for outsideTextEditor..
			
			//Debug.log("TransformTool > unstick > frame : "+frame);
			try{
				// clean frame
				if(frame){
					frame.BOUNDS_CHANGE.remove(onFrameBoundsChange);
					frame.isSticked = false;
					frame = null;
				}
				
				// clean action
				cleanAction();
				
				// clean clip
				if(parent) parent.removeChild(this);
				if(toolbar) toolbar.unstick();
				
				
				//unstick toolbars
				removeAllToolBar();
				
				// remove cursor
				CursorManager.instance.reset();
				
				// 
				UserActionManager.instance.updateToolbar();
				
				isSticked = false;
			}
			catch(e:Error)
			{
				Debug.warn("TransformTool.unstick > error in try catch : "+e.toString());
			}
		}
		
		/**
		 * Start the action MOVE
		 * This is called from outside the transfrom tool now
		 * -> Allow the frame to control if it can move or not (Used mostly for testFrame and the text selection issue)
		 */
		public function startMove():void
		{
			Debug.log("TransformTool > Start move > frame is : " + frame);
			if(frame) startAction(EDIT_MOVE);
		}
		
		/**
		 *
		 */
		public function get toolBarPosition():Rectangle
		{			
			return toolbar.getBounds(editionArea);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * set action listeners
		 */
		private function setListeners():void
		{
				
			frameBorder.addEventListener(MouseEvent.MOUSE_DOWN, function():void{
				startAction(EDIT_MOVE);
			});
			frameBorder.addEventListener(MouseEvent.ROLL_OVER, function():void{
				if(frame.allowBorderMove && frame.allowMove) 
					CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			});
			frameBorder.addEventListener(MouseEvent.ROLL_OUT, function():void{
				CursorManager.instance.reset();
			});
			
			
			// rotation icon
			rotateIcon.buttonMode = true;
			rotateIcon.mouseChildren = false;
			rotateIcon.addEventListener(MouseEvent.MOUSE_DOWN, function():void{
				startAction(EDIT_ROTATE);
			});
			rotateIcon.addEventListener(MouseEvent.ROLL_OVER, function():void{
				TweenMax.killTweensOf(rotateIcon);
				TweenMax.to(rotateIcon, .3, {colorTransform:{exposure:1.2},glowFilter:{color:Colors.GREEN_TRANSFORM_TOOL, alpha:1, blurX:10, blurY:10}, ease:Strong.easeOut});
			});
			rotateIcon.addEventListener(MouseEvent.ROLL_OUT, function():void{	
				TweenMax.killTweensOf(rotateIcon);
				TweenMax.to(rotateIcon, .3, {colorTransform:{exposure:1},glowFilter:{alpha:0, remove:true}});
			});
			
			
			// move Icon
			moveIcon.addEventListener(MouseEvent.MOUSE_DOWN, function():void{
				startAction(EDIT_INSIDE_MOVE);
			});
			
			moveIcon.addEventListener(MouseEvent.ROLL_OVER, function():void{
				//TweenMax.to(moveIcon, .5, {ease:Strong.easeOut, tint : 0xffffff});
				if(frame.allowInsideMove) 
				{
					moveIcon.alpha = 0;
					CursorManager.instance.showCursorAs(CursorManager.CURSOR_HAND);
				}
				
				
			});
			moveIcon.addEventListener(MouseEvent.ROLL_OUT, function():void{
				//TweenMax.to(moveIcon, .5, {ease:Strong.easeOut, tint : null});
				moveIcon.alpha = 1;
				CursorManager.instance.reset();
			});
			
			//Infos.stage.addEventListener(MouseEvent.CLICK, unstick);
		}
		
		/**
		 * when frame boundaries change
		 */
		private function onFrameBoundsChange(actionType : String, buttonRef:CornerButton = null ):void
		{
			var point:Point = new Point(frame.x,frame.y);
			point = frame.parent.localToGlobal(point);
			point = editionArea.globalToLocal(point);
			this.x = Math.round(point.x);
			this.y = Math.round(point.y);
			this.rotation = frame.frameVo.rotation;
			updateIcons();
			updateBorder();
			toolbar.positionate(null);
		}
		
		
		
		/**
		 * start an editing action
		 */
		private function startAction(actionType : String, buttonRef:CornerButton = null ):void
		{
			Debug.log("TransformTool > startAction > "+actionType);
			
			
			// CHECKS
			if(currentAction){
				//Debug.warn("TransformTool > startAction cannot start because there is already an action running, we stop this : "+currentAction);
				//unstick();
				return;
			}
			if(!frame){
				Debug.warn("TransformTool > startAction : Frame is null..., we do not continue");
				unstick();
				return;
			}
			if(!stage){
				Debug.warn("TransformTool > startAction : Stage is null..., we do not continue");
				unstick();
				return;
			}
			
			
			// Frame allow check
			if(!frame.allowMove && actionType == EDIT_MOVE) return;
			if(!frame.allowRotation && actionType == EDIT_ROTATE) return;
			if(!frame.allowCrop && actionType == EDIT_CROP) return;
			if(!frame.allowInsideMove && actionType == EDIT_INSIDE_MOVE) return;
			
			//Wrap action into a try... We could not re-produce the bug #1009
			try
			{
				// be sure to remove possible tooltip while starting action
				DefaultTooltip.killAllTooltips();
				
				currentAction = actionType;
				currentButton = buttonRef;
				
				//hide all toolbars
				hideAllToolBars();
				
				// be sure to remove focus if we make a frame modification
				if(frame is FrameText) FrameText(frame).setFocus(false);
				
				// MOVE
				if( currentAction == EDIT_MOVE ){
					if(frame is FramePhoto) this.visible = false;
					startDrag();
					isDragging = true;
					addEventListener(Event.ENTER_FRAME, updateImage);
					stage.addEventListener(MouseEvent.MOUSE_UP, stopAction);
				}
				
				// ROTATE
				else if( currentAction == EDIT_ROTATE ){
					addEventListener(Event.ENTER_FRAME, updateImage);
					if(frame is FramePhoto) this.visible = false;
					stage.addEventListener(MouseEvent.MOUSE_UP, stopAction);
				}
				
				// RESIZE
				else if( currentAction == EDIT_RESIZE ){
					addEventListener(Event.ENTER_FRAME, updateImage);
					if(frame is FramePhoto) this.visible = false;
					stage.addEventListener(MouseEvent.MOUSE_UP, stopAction);
				}
				
				// CROP
				else if( currentAction == EDIT_CROP ){
					addEventListener(Event.ENTER_FRAME, updateImage);
					if(frame is FramePhoto) this.visible = false;
					stage.addEventListener(MouseEvent.MOUSE_UP, stopAction);
				}
				
				// INSIDE MOVE
				else if( currentAction == EDIT_INSIDE_MOVE ){
					addEventListener(Event.ENTER_FRAME, updateImage);
					moveIcon.visible = false;
					if(frame is FramePhoto) this.visible = false;
					stage.addEventListener(MouseEvent.MOUSE_UP, stopAction);
				}	
			} 
			catch(e:Error) 
			{
				Debug.warn("TransformTool > startAction > catch error : "+e.message);
				
				if(frame && frame.frameVo)
				{
					var frameVoStringify:String = new JSONEncoder(frame.frameVo).getString();
					var r:RegExp = /,/g;
					var s:String = "\n";
					frameVoStringify = frameVoStringify.replace(r, s);
					Debug.warn(" > frameVo Stringify: "+frameVoStringify);
				}
				
				// force unstick and display error to user
				unstick();
				PopupAbstract.Alert("popup.framephoto.error.title","popup.framephoto.error.desc",false,null,null,true);
			}
		}
		
		/**
		 * Hide all toolBars
		 * */
		private function hideAllToolBars():void
		{
			if(toolbar) toolbar.visible = false;
			if(popartToolBar) popartToolBar.visible = false;
			if(borderToolBar) borderToolBar.visible = false;
			if(masksToolbar) masksToolbar.visible = false;
			if(shadowToolbar) shadowToolbar.visible = false;
			if(textBackgroundToolbar) textBackgroundToolbar.visible = false;
		}		
			
		
		/**
		 * Clean a previous or possible existing action
		 * -> remove listeners
		 * -> reactivate frame mouse
		 * -> positionate toolbars
		 * -> remove snapping lines
		 */
		private function cleanAction():void
		{
			this.visible = true;
			currentAction = null;
			
			// remove all listeners
			if(isDragging) stopDrag();
			isDragging = false;
			if(stage) stage.removeEventListener(MouseEvent.MOUSE_UP, stopAction);
			removeEventListener(Event.ENTER_FRAME, updateImage);
			
			// reset frame mouse
			if(editionArea){
				TweenMax.killDelayedCallsTo(editionArea.blockFrameMouse);
				editionArea.blockFrameMouse(false);	
			}
			
			// remove possible snapping lines
			updateSnappingLines(true);
			
			//Repositionate All toolbar
			repositionateAllToolBar();
		}
		
		
		/**
		 * stop current action 
		 * -> clean action
		 * -> save frame state
		 * -> redraw navigator page
		 * -> update user history
		 */
		private function stopAction(e:Event):void
		{
			Debug.log("TransformTool > stopAction > "+currentAction);
			
			// save current action info
			var previousAction : String = currentAction;
			cleanAction();

			// check no frame
			if(!frame) 
			{
				Debug.warn("TransformTool > stopAction > no frame.. break..");
				return;
			}
			
			// STOP MOVE
			if( previousAction == EDIT_MOVE ){
				//stopDrag();
				updateImage();
				
				// when moving image, check if the release was done outside the bounds, if so delete frame
				if(checkFrameOutOfBounds())
				{
					// ok frame is not on current page anymore, check if it's on another page
					var isOnOtherPage :PageArea = checkFrameOnAnotherPage();
					if( isOnOtherPage ){
						var newPos : Point = new Point(x, y);
						newPos = editionArea.localToGlobal(newPos);
						newPos = isOnOtherPage.globalToLocal(newPos);
						PagesManager.instance.switchFrameToPage ( frame, isOnOtherPage, newPos );
					}
					// else delete it
					else 
					{
						if(frame is FrameOverlayer && (frame as FrameOverlayer).linkedFrame) (frame as FrameOverlayer).linkedFrame.deleteMe(); // if frame overlayer, we delete linked frame
						else frame.deleteMe();
						
						// save current state in history
						//UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
					}
					
					// remove possible visible snapping lines
					updateSnappingLines(true);
										
					return;
				}
			}
			
			// STOP ROTATE
			else if( previousAction == EDIT_ROTATE ){}
			
			// STOP RESIZE
			else if( previousAction == EDIT_RESIZE ){
				frame.endResize();
				if(frame is FrameText) toolbar.updateFontSizeText();
			}
			
			// STOP CROP
			else if( previousAction == EDIT_CROP ){
				frame.endCrop();
			}
			
			// STOP INSIDE MOVE
			else if( previousAction == EDIT_INSIDE_MOVE )
			{
				moveIcon.visible = true;
				if(frame is FramePhoto)FramePhoto(frame).stopInsideMove();
				else if (frame is FrameCalendar) FrameCalendar(frame).stopInsideMove();	
			}
			
			//Tell the frame to save the modification
			frame.save();
			
			// update page navigator
			PageNavigator.RedrawPageByFrame(frame.frameVo);
			
			// remove possible visible snapping lines
			updateSnappingLines(true);
			
			// save current state in history
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
		}
		
		/**
		 * Repositionate all toolbars
		 */
		private function repositionateAllToolBar():void
		{
			toolbar.visible = true;
			toolbar.positionate(); // repositionate toolbar
			
			if(popartToolBar && popartToolBar.parent){
				popartToolBar.visible = true;
				popartToolBar.positionate();	
			}
			
			if(borderToolBar && borderToolBar.parent){
				borderToolBar.visible = true;
				borderToolBar.positionate();	
			}
			
			if(masksToolbar && masksToolbar.parent){
				masksToolbar.visible = true;
				masksToolbar.positionate();	
			}
			
			if(shadowToolbar && shadowToolbar.parent){
				shadowToolbar.visible = true;
				shadowToolbar.positionate();	
			}
			
			if( textBackgroundToolbar && textBackgroundToolbar.parent){
				textBackgroundToolbar.visible = true;
				textBackgroundToolbar.positionate();	
			}
			
		}
		
		/**
		* Rotation step (45° - 90° - 120°)
		* Update rotation frame et transform tool
		*/
		private function rotationStepClicked():void
		{
			if(!frame) return;
			
			Debug.log("TransformTool > rotation step click");
			
			var currentRot:Number = rotation;
			var newRot:Number = currentRot;
			var steps:Array = [45,90,135,180,225,270,315,360];
			
			var step:int;
			currentRot = (currentRot>-0.1)?currentRot:currentRot+360;
			step =	Math.floor(currentRot/45);
			newRot = steps[step];						
			
			frame.updateRotation(newRot);
			updateRotation(newRot); 
		}
		
		
		/**
		 * update image is a function that happen on enterframe while an action is started
		 * -> this action can be a resize, a rotation, a move, a content frame move, a crop..
		 */
		private function updateImage( e:Event = null ):void
		{
			// securities
			if(!frame){
				Debug.warn("TransformTool > updateImage : Frame is null..., we unstick");
				unstick();
				return;
			}
			// security to be sure frame exists and is in display list
			if(!frame.parent){ 
				Debug.warn("TransformTool > updateImage : Frame has no parent..., we unstick");
				unstick();
				return;
			}
			
			// security to be sure stage exists
			if(!stage){ 
				Debug.warn("TransformTool > updateImage : stage is null..., we unstick");
				unstick();
				return;
			}
			
			
			// Here seems to be a major place for Bugs and user Error log, we put all this in try/catch for now and unstick when something goes wrong
			try{
				
				// action handling
				if( currentAction == EDIT_MOVE )
				{
					var point:Point = new Point(x,y);
					point = editionArea.localToGlobal(point);
					point = frame.parent.globalToLocal(point);
					frame.updatePosition(point.x, point.y);
					
					// TODO : change bounds color when about to delete
					if(checkFrameOutOfBounds()){
						visible = true;
						TweenMax.killTweensOf(this);
						if(checkFrameOnAnotherPage()) TweenMax.to(this, 0, {tint:null});
						else TweenMax.to(this, 0, {tint:0xff0000});
					}
					else{
						TweenMax.to(this, 0, {tint:null});
						visible = false;
					}
					
					
					// TODO : check align with other frames
					updateSnappingLines();
					
				}
				else if( currentAction == EDIT_ROTATE ){
					var centerPoint:Point = new Point(0, 0);
					centerPoint = frame.localToGlobal(centerPoint);
					var mousePoint:Point = new Point(stage.mouseX, stage.mouseY);
					var rot:Number = MathUtils2.getAngleBetweenPoints(centerPoint, mousePoint); 
					rot = MathUtils2.toDegrees(rot);
					rot += 90;  
					rot += 360;// to be sure to stay in positive
					
					// stick rotation on normal angle
					var angleDiff : Number = rot%90;
					//Debug.log("rotation = "+rotation + " & angle diff = "+ angleDiff);
					if(angleDiff < 3) rot = rot-rot%90;
					else if(angleDiff > 87) rot = rot+ 90-rot%90;
					
					frame.updateRotation(rot);
					updateRotation(rot); 
					
					
					//MathUtils2.rotateAroundCenter(this, 1);
					//rotation+=1;
					//trace("rotation = "+rotation);
				}
				else if( currentAction == EDIT_RESIZE )
				{
					frame.resize(currentButton.ref);
				}
				else if( currentAction == EDIT_CROP )
				{
					frame.crop(currentButton.ref);
				}
				else if( currentAction == EDIT_INSIDE_MOVE )
				{
					if(frame is FramePhoto) FramePhoto(frame).insideMove();
					else if (frame is FrameCalendar) FrameCalendar(frame).insideMove();
				}
			
				// toolbar.positionate(); // not used anymore as we hide toolbar during action process
			
			} //# End of try
			
			catch(e:Error) 
			{
				Debug.warn("TransformTool > updateImage > catch error : " +e.toString());
				
				if(frame && frame.frameVo)
				{
					var frameVoStringify:String = new JSONEncoder(frame.frameVo).getString();
					var r:RegExp = /,/g;
					var s:String = "\n";
					frameVoStringify = frameVoStringify.replace(r, s);
					Debug.warn(" > frameVo Stringify: "+frameVoStringify);
				}
				
				// force unstick and display error to user
				unstick();
				PopupAbstract.Alert("popup.framephoto.error.title","popup.framephoto.error.desc",false,null,null,true);
			}
		}
		
		/**
		 * update icons to fit to the frame target
		 */

		private function updateIcons():void
		{
			var fb:Rectangle = frame.boundRectScaled;
			var centerPoint : Point = new Point(fb.x + fb.width *.5, fb.y + fb.height*.5);
			
			rotateIcon.x = centerPoint.x - rotateIcon.width*.5;
			rotateIcon.y = fb.y - 50;
			
			var $width:Number = MeasureManager.PixelToCM(frame.frameVo.width);
			infoWidth.update($width.toFixed(2) + " cm");
			infoWidth.x = centerPoint.x - infoWidth.width*.5;
			infoWidth.y = fb.y + 5;
			infoWidth.visible = (fb.height > infoWidth.height*2);
			
			var $height:Number = MeasureManager.PixelToCM(frame.frameVo.height);
			infoHeight.update($height.toFixed(2) + " cm");
			infoHeight.rotation = -90;
			infoHeight.x = fb.x + 5
			infoHeight.y = centerPoint.y + infoHeight.height*.5;
			infoHeight.visible = (fb.height > infoHeight.height*1.5);
				
			
			
			if(frame.allowInsideMove)
			{
				moveIcon.visible = true;
				moveIcon.x = centerPoint.x - moveIcon.width*.5;
				moveIcon.y = centerPoint.y - moveIcon.height*.5;
			}
			else
			{
				moveIcon.visible = false;
			}
			
			
			rotateIcon.visible = frame.allowRotation;
			frameBorder.mouseEnabled = frame.allowBorderMove;
		}
		
		/**
		 * update borders creates the visible boundaries of the frame
		 */
		private function updateRotation(newRot:Number):void
		{
			rotation = newRot;
			//moveIcon.rotation = -newRot; // TODO make hand icon not rotating + hand icon should replace mouse when over and clicked
		}
		
		private function createFrameBorder():void
		{
			// create if not existing
			if(!frameBorder) {
				frameBorder = new Sprite();
				/*frameBorder.mouseEnabled = false;
				frameBorder.mouseChildren = false;*/
				/*frameHit = new Sprite();
				addChildAt(frameHit, 0);*/
				addChildAt(frameBorder, 1);
				
				// create resize spots
				for (var i:int = 0; i < 8; i++) 
				{
					var btn : CornerButton = new CornerButton();
					btn.graphics.lineStyle(1, 0x70ae4b);
					btn.graphics.beginFill(0x70ae4b, .8);
					
					// circles
					//if (i == 0 || i == 2 || i == 5 || i == 7 ) btn.graphics.drawCircle( 0,0, 6);
					//else btn.graphics.drawCircle( 0,0, 4)
					
					// squares
					if (i == 0 || i == 2 || i == 5 || i == 7 ) btn.graphics.drawRect( -5,-5, 10,10);
					else btn.graphics.drawRect( -4,-4, 8,8);
					
					btn.graphics.endFill();
					btn.ref = i;
					//btn.mouseChildren = false;
					btn.addEventListener(MouseEvent.MOUSE_DOWN, cornerButtonPressed);
					btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void{e.currentTarget.scaleX = e.currentTarget.scaleY = 1.5});
					btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent):void{e.currentTarget.scaleX = e.currentTarget.scaleY = 1});
					addChild(btn);
					resizeButtons.push(btn);
				}
			}
		}
		
		/**
		 * update borders creates the visible boundaries of the frame
		 */
		private function updateBorder():void
		{
			// get frame bounds
			var frameBounds:Rectangle = frame.boundRectScaled; 
			var margin:int = 0; // 4 =  draw content with a 2 pixel margin
			var radius : int = 0; //5
			var color : Number = Colors.GREEN_TRANSFORM_TOOL;
			var tickness :Number = 2;
			
			if( frame is FrameText ){
				margin = 0;
				radius = 0;
				tickness = 2;
				color = Colors.BLUE_LIGHT;
			}
			else if( frame is FrameCalendar ){
				margin = 0;
				radius = 0;
				color = Colors.BLUE_LIGHT;
			}
			
			frameBorder.graphics.clear();
			frameBorder.graphics.lineStyle(tickness, color);
			frameBorder.graphics.drawRoundRect( frameBounds.x-margin, frameBounds.y-margin, frameBounds.width + 2*margin, frameBounds.height+2*margin, radius);	
			frameBorder.graphics.lineStyle(10, color,0);
			frameBorder.graphics.drawRoundRect( frameBounds.x-margin, frameBounds.y-margin, frameBounds.width + 2*margin, frameBounds.height+2*margin, radius);	
			
			/*frameHit.graphics.clear();
			frameHit.graphics.beginFill(0xffffff, .1);
			frameHit.graphics.drawRoundRect( frameBounds.x, frameBounds.y, frameBounds.width, frameBounds.height, 5);*/
			
			// update resize buttons
			updateCornerButtons(frameBounds, color, margin, radius);
			
		}
		
		
		
		
		
		/**
		 * Check if the frame is out of bounds
		 * the page should be deleted if the release mouse point is out of page area
		 * AND the center of the frame is out of bounds too
		 */
		//private var visualizer : Sprite;
		private function checkFrameOutOfBounds():Boolean
		{
			// frame is out of bounds if the mouse was out of page bounds while release
			if(!currentPage){
				Debug.warn("TransformTool > checkFrameOutOfBounds > no page area found for frame");
				return false;
			}
			
			//var pageBounds : Rectangle = editionArea.getPagesBounds(); // give both pages bounds
			var pageBounds : Rectangle = currentPage.getBounds(stage);
			var framePoint:Point = frame.localToGlobal(new Point(0,0));
			return ( !pageBounds.contains(stage.mouseX, stage.mouseY) && !pageBounds.contains(framePoint.x, framePoint.y) );
			
			
			/*
			var showLimits : Boolean = true;
			if(showLimits){
				if(!visualizer){
					visualizer = new Sprite();
					visualizer.mouseEnabled = visualizer.mouseEnabled = false;
				}
				stage.addChild(visualizer);	
			}
			
			
			// get frame bounds
			// draw circle on frame bounds lower 
			var frameVo : FrameVo = frame.frameVo;
			var framePoint : Point = new Point(frameVo.x, frameVo.y);
			framePoint = frame.parent.localToGlobal(framePoint);
			
			var frameRadius : Number = ( frameVo.width > frameVo.height )? frameVo.height*.5 : frameVo.width*.5;
			frameRadius *= EditionArea.editorScale;
			
			// get current page area
			var pagesBounds : Rectangle = editionArea.getPagesBounds();
			
			// condition if frame is inside page bounds
			var isInside : Boolean = true;
			if( framePoint.x - frameRadius > pagesBounds.x + pagesBounds.width ) isInside = false;
			else if( framePoint.x + frameRadius < pagesBounds.x ) isInside = false;
			else if( framePoint.y - frameRadius > pagesBounds.y + pagesBounds.height ) isInside = false;
			else if( framePoint.y + frameRadius < pagesBounds.y ) isInside = false;
			
			
			// draw intersections
			if(showLimits){
				var g : Graphics = visualizer.graphics;
				g.clear();
				
				// draw visible zone
				g.beginFill(0xff00ff, .3);
				g.lineStyle(1,0xff00ff);
				g.drawRect(pagesBounds.x, pagesBounds.y, pagesBounds.width, pagesBounds.height);
				
				// draw circle
				var circleColor : Number = (isInside)? Colors.GREEN : Colors.RED;
				g.beginFill(circleColor, .3);
				g.lineStyle(1,circleColor);
				g.drawCircle(framePoint.x, framePoint.y, frameRadius);
				g.endFill();
			}
			
			
			return isInside;
			*/
		}
		
		
		/**
		 * The frame is out of bounds
		 * We now check if frame is on another page and return this page
		 */
		private function checkFrameOnAnotherPage():PageArea
		{
			// check if frame;
			if(!frame){
				Debug.warn("TransformTool.checkFrameOnAnotherPage : no Frame");
				return null;
			}
			
			var page:PageArea;
			var pageBounds : Rectangle;
			var framePoint : Point = frame.localToGlobal( new Point (0,0 ));
			for (var i:int = 0; i < editionArea.currentPages.length; i++) 
			{
				page = editionArea.currentPages[i];
				pageBounds = page.getBounds( stage );
				if( page != currentPage && pageBounds.contains(stage.mouseX, stage.mouseY) ) return page;   // && !pageBounds.contains(framePoint.x, framePoint.y) );
			}
			
			return null;
		}
		
		
		
		
		/**
		 * UPDATE SNAPPING LINES 
		 * > check all drawn frames on screen
		 * > find corresponding matches (with 5pixel magnet snapping)
		 * > draw lines between alignes frames
		 */
		private function updateSnappingLines( remove:Boolean = false ):void
		{
			if(remove)
			{
				if(snapLinesContainer){
					if(snapLinesContainer.parent)snapLinesContainer.parent.removeChild(snapLinesContainer);
					snapLinesContainer = null;
				}
				return;
			}
			if(!frame) return;
			if(!stage) return;
			if(!snapLinesContainer)
			{
				snapLinesContainer = new Sprite();
				snapLinesContainer.mouseEnabled = false;
				snapLinesContainer.mouseChildren = false;
			}
			if(!snapLinesContainer.parent) stage.addChild(snapLinesContainer);
			snapLinesContainer.graphics.clear();
			
			// check all frames
			var magnetSize:Number = 2; // in pixel
			var obj:Object = EditionArea.findNearestSnapDiff(frame,magnetSize);
			if(!obj || !obj.diff) return; // security
			
			snapLinesContainer.graphics.lineStyle(2,Colors.YELLOW,1,false);
			
			// vertical
			if(Math.abs(obj.diff.x) < magnetSize)
			{
				frame.x += obj.diff.x;
				this.x +=obj.diff.x;
				snapLinesContainer.graphics.moveTo(obj.pos.x, -10);
				snapLinesContainer.graphics.lineTo(obj.pos.x, stage.stageHeight + 10);
			}
			
			// horizontal
			if(Math.abs(obj.diff.y) < magnetSize)
			{
				frame.y += obj.diff.y;
				this.y +=obj.diff.y;
				snapLinesContainer.graphics.moveTo(-10, obj.pos.y);
				snapLinesContainer.graphics.lineTo(stage.stageWidth + 10, obj.pos.y );
			}
		}
		
		
		/**
		 * positionate resize Buttons
		 * RESIZE : TL = 0 , TR = 2, BL = 5, BR = 7
		 * CROP : T = 1, R = 3, L = 4, B = 6
		 */
		private function updateCornerButtons(frameBounds:Rectangle, color:Number, margin:Number, radius:Number):void
		{
			var btn:CornerButton, btnIndex:int;
			var rowWidth:Number = frameBounds.width*.5 + margin;
			var rowHeight:Number = frameBounds.height*.5 + margin;
			
			
			for (var i:int = 0; i < 8; i++) 
			{
				btn = resizeButtons[i];
				TweenMax.to(btn, 0,{tint:color});
				btnIndex = (i>3)? i+1 : i; // do this because we do not have a button on the center
				btn.x = frameBounds.x-margin + btnIndex%3 * rowWidth;
				btn.y = frameBounds.y-margin + Math.floor(btnIndex/3) * rowHeight;
				
				if (i == 0 || i == 2 || i == 5 || i == 7 ){
					btn.visible = frame.allowResize;
				}
				else btn.visible = frame.allowCrop;
			}
		}
		
		
		/**
		 *  Toggle popart toolbar visibiltiy
		 */
		private function togglePopArtToolbar():void
		{
			if(!frame)return; // security
			if(!popartToolBar) {
				popartToolBar = new PopartToolBar(editionArea);
			} 
			// stick to frame
			if(!popartToolBar.parent){
				removeAllToolBar(); popartToolBar.stickToFrame(frame);
			}
			else popartToolBar.unstick();
		}
		/**
		 *  Toggle BORDER toolbar visibiltiy
		 */
		private function toggleBorderToolbar():void
		{
			if(!borderToolBar) {
				borderToolBar = new BorderToolBar(editionArea);
			} 
			// stick to frame
			if(!borderToolBar.parent){
				removeAllToolBar(); borderToolBar.stickToFrame(frame);
			}
			else borderToolBar.unstick();
		}
		/**
		 *  Toggle MASK toolbar visibiltiy
		 */
		private function toggleMasksToolbar():void
		{
			if(!masksToolbar) {
				masksToolbar = new MaskToolBar(editionArea);
			} 
			// stick to frame
			if(!masksToolbar.parent){
				removeAllToolBar(); masksToolbar.stickToFrame(frame);
			}
			else masksToolbar.unstick();
		}
		
		/**
		 *  Toggle SHADOW toolbar visibiltiy
		 */
		private function toggleShadowToolbar():void
		{
			if(!shadowToolbar) {
				shadowToolbar = new ShadowToolbar(editionArea);
			} 
			// stick to frame
			if(!shadowToolbar.parent){
				removeAllToolBar(); shadowToolbar.stickToFrame(frame as FramePhoto);
			}
			else shadowToolbar.unstick();
		}
		
		/**
		 *  Toggle TEXT BACKGROUND toolbar visibiltiy
		 */
		private function toggleTextBackgroundToolbar():void
		{
			if(!textBackgroundToolbar) {
				textBackgroundToolbar = new TextBackgroundToolbar(editionArea);
			} 
			// stick to frame
			if(!textBackgroundToolbar.parent){
				removeAllToolBar(); 
				if(frame is FrameText) textBackgroundToolbar.stickToFrame(frame as FrameText);
				else if (frame is FrameCalendar && (frame as FrameCalendar).linkedTextFrame != null) textBackgroundToolbar.stickToFrame((frame as FrameCalendar).linkedTextFrame as FrameText);
			}
			else textBackgroundToolbar.unstick();
		}
		
		
		
		
		/**
		 *  Toggle OUTSIDE TEXT EDITOR panel
		 */
		public static function editTextOutsideFrame(frameToEdit : Frame = null):void
		{
			instance.editTextOusideFrame(frameToEdit);
		}
		
		private function editTextOusideFrame(frameToEdit : Frame = null):void
		{
			Debug.log("TransformToolbar > editTextOusideFrame > frame : " +frameToEdit);
			
			var castedFrameText : FrameText ;
			if(frameToEdit) frame = frameToEdit;
			if(frame is FrameCalendar && (frame as FrameCalendar).linkedTextFrame ) castedFrameText = (frame as FrameCalendar).linkedTextFrame;
			else if (frame is FrameText) castedFrameText = frame as FrameText;
			else {
				Debug.warn("TransformToolbar > editTextOusideFrame > No frame text to edit");
				return;
			}
			
			if(!textEditorToolbar) {
				textEditorToolbar = new OutsideTextEditorToolbar(editionArea, toolbar);
			} 
			
			/*
			// stick to frame
			if(!borderToolBar.parent){
				removeAllToolBar(); textEditorToolbar.stickToFrame( frame );
			}
			else textEditorToolbar.unstick();
			*/
			var frameRef : Frame = frame;
			
			unstick(); // fisrt unstick of frame on edition
			//removeAllToolBar(); 
			frame = frameRef;
			textEditorToolbar.stickToFrame( frame ); // then stick to outside editor
		}
		
		
		
		
		
		/**
		 *  Toggle BORDER toolbar visibiltiy
		 */
		private function removeAllToolBar():void
		{
			// unstick popart toolbar if needed
			if(popartToolBar && popartToolBar.parent) popartToolBar.unstick();
			// unstick border toolbar if needed
			if(borderToolBar && borderToolBar.parent) borderToolBar.unstick();
			// unstick mask toolbar if needed
			if(masksToolbar && masksToolbar.parent) masksToolbar.unstick();
			// unstick shadow toolbar if needed
			if(shadowToolbar && shadowToolbar.parent) shadowToolbar.unstick();
			// unstick text background toolbar if needed
			if(textBackgroundToolbar && textBackgroundToolbar.parent) textBackgroundToolbar.unstick();
			
			
			// unstick outside text toolbar if needed
			if(textEditorToolbar && textEditorToolbar.parent) textEditorToolbar.unstick();
		}
		
		
		/**
		 *  when a corner button is pressed
		 */
		private function cornerButtonPressed(e:MouseEvent):void
		{
			currentButton = e.currentTarget as CornerButton;
			var ref:int = currentButton.ref;
			if (ref == 0 || ref == 2 || ref == 5 || ref == 7 ) startAction(EDIT_RESIZE, currentButton);
			else startAction(EDIT_CROP, currentButton);
		}
	}
}


import flash.display.Sprite;
internal class CornerButton extends Sprite
{
	public var ref:int;
	public function CornerButton():void{};
}
