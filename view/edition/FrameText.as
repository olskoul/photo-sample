package view.edition
{
	import com.bit101.components.TextArea;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.Font;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextLineMetrics;
	import flash.text.engine.FontWeight;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.MathUtils2;
	
	import comp.DefaultTooltip;
	import comp.button.SimpleButton;
	
	import data.FrameVo;
	import data.Infos;
	import data.PageVo;
	import data.TooltipVo;
	
	import flashx.textLayout.formats.VerticalAlign;
	
	import library.addText.icon.bmd;
	
	import manager.PagesManager;
	
	import mcs.transformToolbar;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.CursorManager;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.pageNavigator.PageNavigator;
	import view.edition.toolbar.OutsideTextEditorToolbar;
	
	public class FrameText extends Frame
	{
		// signal
		public var TEXT_CHANGE : Signal = new Signal(String);
		
		//Bitmapdata call to action for photo
		private static var ctaTextIconBmd:BitmapData; //-> we use a static variable so it's created only once for all empty frames of the application
		
		// public
		public var hasFocus : Boolean = false; // this must be true at beginning so we go once to setFocus(false) to setup focus behavior
		public var isEdited : Boolean; // when a edition has been  made on the texfield (font size, color, aligment etc...)
		public var isOutsideFrame : Boolean; // is edited frame text in outsideEditor toolbar. It's not registred the same way
		
		// ui
		private var contentBg : Shape
		private var textfield:TextField; 	// text on which we are working
		private var icon:Sprite;
		private var startMousePoint:Point;
		private var startCropRect:Rectangle;
		private var lastHtmlText : String;
		private var watermark:String;
		
		private var forceBackgroundRedraw:Boolean; // set this to true to force background redraw on next renderFrame()
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		

		
		public function FrameText(frameVo:FrameVo)
		{
			super(frameVo);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////
		/**
		 * get has focus state
		 */
		/*public function get hasFocus():Boolean
		{
			return _hasFocus;
		}*/
		
		/**
		 * Overide the label for the cta btn
		 */
		override public function get labelCta():String
		{
			return "edition.frame.text.cta";
		}
		
		
		/**
		 * Overide the icon bitmapdata: Return the icon for photo
		 */
		override public function get iconbmd():BitmapData
		{
			if(!ctaTextIconBmd) ctaTextIconBmd = new library.addText.icon.bmd();
			return ctaTextIconBmd;
		}
		
		/**
		 * is the current frame empty
		 */
		override public function get isEmpty() : Boolean
		{
			return (frameVo.isEmpty && textfield == null);
		}
		
		/**
		 * when frame is fixed, it will not resize in height or width, content will be constraint in frame width and height
		 */
		public function get isFixed() : Boolean
		{
			return (frameVo.fixed);
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * update of font size frome transform toolbar
		 */
		public function setFontSize(size:int, setFocus:Boolean = true):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
		//	textFormat.size = size/frameVo.zoom;
			textFormat.size = size; // TODO update font size depending on zoom
			applyTextFormat(textFormat,selection.begin, selection.end,setFocus);
		}
		
		public function setColor(color:Number):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			textFormat.color = color;
			applyTextFormat(textFormat,selection.begin, selection.end);
		}
		
		public function setFont(fontName:String):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			textFormat.font = fontName;
			applyTextFormat(textFormat,selection.begin, selection.end,false, true);
		}
		
		public function increaseText():void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			var size:int = (textFormat.size!=null)?int(textFormat.size):getFirstCharSize();
			textFormat.size = size+1;
			applyTextFormat(textFormat,selection.begin, selection.end);

		}
		
		public function decreaseText(decreaseTillItFits:Boolean = false):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			var size:int = (textFormat.size!=null)?int(textFormat.size):getFirstCharSize();
			textFormat.size = size-1;
			applyTextFormat(textFormat,selection.begin, selection.end,false,decreaseTillItFits);
		}
		
		/**
		 *
		 */
		private function getFirstCharSize():int
		{
			if( textfield && textfield.text.length > 0 ){
				var tf:TextFormat = getTextFormat(0,1);
				var size : Object = tf.size;
				if(size != null) return int(size);
				
				// else
				Debug.warn("FrameText > getFirstCharSize is null" );
			}
			return 12; // default
		}
		
		public function boldText(btn:MovieClip, forceValue:Boolean = false, value:Boolean = false):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			
			if(!forceValue)
			{
				textFormat.bold = !textFormat.bold;
				btn.gotoAndStop((textFormat.bold)?2:1);
			}
			else
			{
				textFormat.bold = value;
				if(btn)
					btn.gotoAndStop((textFormat.bold)?2:1);
			}
			
			applyTextFormat(textFormat,selection.begin, selection.end);
		}
		
		public function italicText(btn:MovieClip , forceValue:Boolean = false, value:Boolean = false):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			
			if(!forceValue)
			{
				textFormat.italic = !textFormat.italic;
				btn.gotoAndStop((textFormat.italic)?2:1);
			}
			else
			{
				textFormat.italic = value;
				if(btn)
					btn.gotoAndStop((textFormat.italic)?2:1);
			}
			
			
			applyTextFormat(textFormat,selection.begin, selection.end);
		}
		
		public function underlineText(btn:MovieClip , forceValue:Boolean = false, value:Boolean = false):void
		{
			var selection:Object = getSelectedText();
			var textFormat:TextFormat = getTextFormat(selection.begin, selection.end);
			
			if(!forceValue)
			{
				textFormat.underline = !textFormat.underline;
				btn.gotoAndStop((textFormat.underline)?2:1);
			}
			else
			{
				textFormat.underline = value;
				if(btn)
					btn.gotoAndStop((textFormat.underline)?2:1);
			}
						
			applyTextFormat(textFormat,selection.begin, selection.end);
		}
		
		
		/**
		 * change text vertical align
		 */ 
		public function changeVAlign( value:String ):void
		{
			frameVo.vAlign = value;
			renderFrame();
			checkAndSave();
		}
		
		
		/**
		 * Get selected text 
		 */
		public function getSelectedText():Object
		{
			if(!textfield) return null;
			var obj:Object = {};
			obj.begin = (textfield.selectionBeginIndex);
			obj.end = (textfield.selectionEndIndex);
			
			obj.begin = (obj.begin >= textfield.length)?-1:obj.begin;
			obj.end = (obj.end >= textfield.length+1)?-1:obj.end;
			
			return obj;
		}
		
		/**
		 * Apply textFormat
		 */
		public function applyTextFormat(textFormat:TextFormat,begin:int, end:int, focusOnTexfield:Boolean = true, decreaseTextSizeIfNeeded:Boolean = false):void
		{	
			// do nothing if no textfield
			if(!textfield || textfield.length == 0 ) return;
			
			//if(!hasFocus) setFocus(); //not sure that adding focus on field on each change is a good way to go
			
			lastHtmlText = textfield.htmlText;
			if(end-begin > 0)
				textfield.setTextFormat(textFormat, begin, end);
			else textfield.setTextFormat(textFormat);
			
			
			/*
			if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
				adjustSpineMetrics();
			if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
				adjustEditionMetrics();
			*/
			
			// make text update check and save
			renderFrame();
			checkAndSave(decreaseTextSizeIfNeeded);
			
			if(hasFocus && focusOnTexfield && !frameVo.isPageNumber)
				stage.focus = textfield;
			
		}
		
		/**
		 * update text background (actually it's just a simple render frame as framevo props have been changed before calling this function)
		 */
		public function renderTextBackground():void
		{
			forceBackgroundRedraw = true;
			renderFrame();
			PageNavigator.RedrawPageByFrame(frameVo); // force navigator update
		}
		
		/**
		 * Get selected textformat 
		 */
		public function getTextFormat(begin:int, end:int):TextFormat
		{
			if(textfield.length == 0 || (end-begin) < 1) return textfield.getTextFormat();
			if(begin == 0 && end == 0) end = 1; // check because throws exception, if index is 0 then use the textformat of index 1
			return textfield.getTextFormat(begin, end);
		}
		
		/**
		 * Get raw text content
		 */
		public function isTextEdited():Boolean
		{
			if(textfield)
				return !((textfield.text == watermark || textfield.text == ""))
			else
				return false;
					
			return true;
		}
		
		
		/**
		 * Set direct html text with corresponding formatt 
		 * used for now only in the outsideTextEditorToolbar
		 */
		public function setHtmlText(value:String):void
		{
			frameVo.text = value;
			updateState(); // the condition is a security to be sure text is not going to cta state when deleting selection
			renderFrame();
			setContentText( value );
			save();
			
			//redraw frame in page navigator
			PageNavigator.RedrawPageByFrame(frameVo);
			
		}
		
		/**
		 * Get regular textformat 
		 */
		public function getRegularTextFormat():TextFormat
		{
			if(textfield)
			return textfield.getTextFormat();
			else
			return null;
		}
		
		
		/**
		 * put html text in textfield with specific checks
		 */
		private function setContentText( value : String ):void
		{
			// Text is going on two lines each time we put htmlText back as there is a "</p>" at the end closing the paragraph.
			// To avoid this and let the paragraph open for edition we just check the last elements
			//EXAMPLE = '<P ALIGN="CENTER"><FONT FACE="ArialRounded" SIZE="24" COLOR="#7C7C7C" LETTERSPACING="0" KERNING="0">Type your text here</FONT></P>';
			if(value && value.length > 4) {
				var editedValue : String = value.slice(0,value.length-4); // this leaves the edition possible on the same paragraph
				textfield.htmlText = editedValue; // STILL PROBLEM WITH UNDERLINE SOMETIMES.. BUT NO IDEA..
				
			}
			else if(textfield)
			{
				textfield.htmlText = value;
			}
			
			
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			// set correct state
			updateState();
			
			// be sure the next render will create background fill
			forceBackgroundRedraw = true;
			
			//render frame
			renderFrame();
			
			// This is some check to be sure the textfield that the editor render fit inside the frame, otherwise we need to modify frame (not textfield as we do 
			if( !isFixed && textfield && textfield.height > frameVo.height ){
				//frameVo.height = textfield.height;
				checkAndSave();
			}
			
			//Listeners
			setListeners();
			//ready!!
			frameReady();
		}
		
		/**
		 *
		 */
		private function get textFormat():TextFormat
		{
			if(!textfield) return null;
			return textfield.getTextFormat();
		}
		
		
		/**
		 * create textfield and set his listeners
		 */
		override protected function construct():void
		{
			//_isEmpty = false;
			
			//create content
			if(!content){
				content = new Sprite();
				content.mouseChildren = hasFocus;
				contentBg = new Shape();
				/*
				if( Debug.SHOW_TEXT_BOUNDS ) contentBg.graphics.beginFill(0xff0000,1);
				else contentBg.graphics.beginFill(0xff0000,0);
				contentBg.graphics.drawRect(-10,-10,20,20);
				contentBg.graphics.endFill();
				*/
				content.addChild(contentBg);
			}
				
			
			addChild(content);	
						
			//watermark
			watermark = (frameVo.grouptype != FrameVo.GROUP_TYPE_EDITION)?ResourcesManager.getString("edition.frame.text.watermark"):"00";
			
			//create textfield
			textfield = new TextField();
			textfield.width = frameVo.width / frameVo.zoom;
			//textfield.height = frameVo.height / frameVo.zoom;
			textfield.scaleX = textfield.scaleY = frameVo.zoom;
			textfield.wordWrap = true;
			textfield.selectable = true;
			textfield.mouseWheelEnabled = false;
			textfield.type = TextFieldType.INPUT;
			textfield.embedFonts = true;
			textfield.multiline = true;
			textfield.antiAliasType = AntiAliasType.ADVANCED;
			textfield.backgroundColor = 0xff00ff;
			textfield.background = (Debug.SHOW_TEXT_BOUNDS)? true : false;
			textfield.autoSize = "left"; // TODO check later if this impacts the textformat
			
			// if its an empty frame, create a new textformat
			if( frameVo.isEmpty )
			{ 
				textfield.text = watermark;
				textfield.setTextFormat(TextFormats.LAST_USED(frameVo.fontSize));		
			}
			else // otherwise use htmltext formating
			{
				setContentText( frameVo.text );	
			}
			
			//Page Number frame
			if(frameVo.isPageNumber)
			{
				var pageVo:PageVo = PagesManager.instance.getPageVoByFrameVo(frameVo);
				var index:int = pageVo.index;
				
				//Make sure the index is good (page move fix)
				var tempTextFormat:TextFormat = textfield.getTextFormat();
				textfield.text = String(index);
				textfield.setTextFormat(tempTextFormat);
				tempTextFormat = null;
				
				//make sure the pageNumber is at the right place
				PagesManager.instance.positionPageNumber(pageVo,frameVo,index);
			}
			
			/*
			if(frameVo.textFormat == null)
				frameVo.textFormat = TextFormats.DEFAULT_FRAME_TEXT(frameVo.fontSize);
			*/
			//var newTextFormat:TextFormat = textFormat;
			
			//font check, if wrong font, apply default text format
			/*
			var fontValidated:String = TextFormats.validateFont(String(newTextFormat.font));
			if(fontValidated != newTextFormat.font && fontValidated != "null")	textfield.setTextFormat(TextFormats.DEFAULT_FRAME_TEXT(frameVo.fontSize));
			*/
			
			//Cover Spine modification
			/*
			if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
			{
				textfield.multiline = false;
				textfield.wordWrap = false;
				textfield.autoSize = TextFieldAutoSize.NONE;
				textfield.setTextFormat(newTextFormat);
			}
			//Cover edition modification
			if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
			{
				newTextFormat.align = TextFormatAlign.CENTER;
				textfield.maxChars = 2;
				textfield.multiline = false;
				textfield.wordWrap = false;
				textfield.autoSize =TextFieldAutoSize.NONE;
				textfield.setTextFormat(newTextFormat);
			}
			*/
			
			
			//apply text Format
			//textfield.setTextFormat(newTextFormat);
			
			//adjust text metrcis spine
			/*
			if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
			adjustSpineMetrics();
			//adjust text metrcis edition
			if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
			adjustEditionMetrics();
			*/
			
			//Add it to display list
			content.addChild(textfield);	
			
			// put content in it
			lastHtmlText = textfield.htmlText;
			
			// set default text format of textfield just created to the first character textformat
			//not sure anymore why this is only on the first character
			//damn!
			var defaultTextFormat:TextFormat = getTextFormat(0,1);
			defaultTextFormat.underline = false; //hack?
			textfield.defaultTextFormat = defaultTextFormat;//textfield.getTextFormat(0,1);
			
			//textfield listeners
			textfield.addEventListener(FocusEvent.FOCUS_OUT,focusOutHandler);
			textfield.addEventListener(FocusEvent.FOCUS_IN,focusInHandler);
			textfield.addEventListener(Event.CHANGE,changeHandler);
		}
			
		private function adjustSpineMetrics():void
		{
			
			var metrics:TextLineMetrics = textfield.getLineMetrics(0);
			var realLineHeight :Number = metrics.ascent + metrics.descent;
			var textfieldHeight :Number = textfield.height - 5;
			var offsetHeight: Number = textfieldHeight - realLineHeight;
			var idealHeight:int = Math.floor(textfieldHeight);	
			var decrement:int = 0;
			var textFormat:TextFormat = textfield.getTextFormat();
			if(offsetHeight < 0)
			{
				while (realLineHeight > textfieldHeight)
				{ 
					trace("TextFrame > Spine > real text height ("+realLineHeight+") is bigger than the textfield Height ("+textfieldHeight+")");
					idealHeight -= decrement;
					
					textFormat.size = idealHeight;
					textfield.setTextFormat(textFormat);
					
					metrics = textfield.getLineMetrics(0);
					realLineHeight = metrics.ascent + metrics.descent; // recalcul metrics
					
					decrement++;
				}
			}
			
		}
		
		private function adjustEditionMetrics():void
		{
			
			//var metrics:TextLineMetrics = textfield.getLineMetrics(0);
			var realLineWidth :Number = textfield.textWidth;
			var textfieldWidth :Number = textfield.width - 5;
			var offset: Number = textfieldWidth - realLineWidth;
			var actualFontSize:int = frameVo.fontSize;	
			var decrement:int = 0;
			var textFormat:TextFormat = textfield.getTextFormat();
			if(offset < 0)
			{
				while (realLineWidth > textfieldWidth)
				{ 
					trace("TextFrame > Edition > real text width ("+realLineWidth+") is bigger than the textfield width ("+textfieldWidth+")");
					actualFontSize -= decrement;
					
					textFormat.size = actualFontSize;
					textfield.setTextFormat(textFormat);
					
					realLineWidth = textfield.textWidth; // recalcul realwidth
					
					decrement++;
				}
			}
			
		}
		
		/**
		 * Overriden funciton by each extends of this class
		 * this what the frame should do when the CTA is clicked
		 */
		override protected function ctaClickHandler(e:MouseEvent = null):void
		{
			//Deselect is dispatched because a click on CTA also dispatch the SELECT, which stick the toolbar and set buttons (frame is still empty at this point)
			//Once the updateState is done, frame is not empty anymore.
			//The setFocus will call the SELECT an reset button correctly
			DESELECT.dispatch(this);
			updateState(true);
			renderFrame();
			setFocus(true,false, true);
		}
		
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		override protected function renderFrame():void
		{
			if(textfield)
			{
				var textFieldWidth : Number = frameVo.width / frameVo.zoom;
				//var textFieldHeight : Number = frameVo.height / frameVo.zoom;
				
				// set textfield size
				textfield.width = textFieldWidth;
				//textfield.height = textFieldHeight;
				textfield.scaleX = textfield.scaleY = frameVo.zoom;
				
				// place textfield on x
				textfield.x = -textfield.width >> 1;//(textContainer.width-frameVo.width) >>1 ;
				
				// place textfield on y
				if(frameVo.vAlign == VerticalAlign.TOP) textfield.y = -frameVo.height*.5;
				else if(frameVo.vAlign == VerticalAlign.BOTTOM) textfield.y = frameVo.height*.5 - textfield.height;
				else textfield.y = -textfield.height*.5;
				
				if(frameVo.isPageNumber)
					textfield.mouseEnabled = false;
			}
			else
			{
				if(cta && contains(cta))
				{
					updateCTA(EditionArea.editorScale);
				}
			}
			
			// apply position after center content, if frame is ouside editing frame, do not apply x, y and rotation
			if(!isOutsideFrame){
				if( x != frameVo.x ) x = frameVo.x;
				if( y != frameVo.y ) y = frameVo.y;
				if( rotation != frameVo.rotation ) rotation = frameVo.rotation;	
			}
			
			// update background to fit framevo height and width
			updateBackground();
			
			
		}
		
		/**
		 * on textfield change handler
		 */
		protected function changeHandler(event:Event):void
		{
			renderFrame();
			checkAndSave();
		}
		
		/**
		 * check text update modifications
		 * Save it
		 */
		protected function checkAndSave(decreaseTextSizeIfNeeded:Boolean = false):void
		{
			// remove possible tooltip on text
			DefaultTooltip.killTooltipById("textLimitTip");
			
			// put scrollv to 0 to be sure
			textfield.scrollV = 0;
			
			// check for fixed limits
			if(isFixed)
			{
				if( Math.ceil((textfield.textHeight + 4)*frameVo.zoom) > frameVo.height) 
				{ // TODO HERE WE MUST TAKE SCALE INTO ACCOUNT !!

					//If decreaseTextSizeIfNeeded is true, start auto decrease process else render frame.
					if(!decreaseTextSizeIfNeeded)
					{
						setContentText(lastHtmlText);
						// display info to warn user the frame is not big enough
						showTextLimitTooltip();
						//render frame
						renderFrame();
					}
					else
					decreaseTillItFits();
 					
					return;
				}
			}
			
			// be sure te thex
			TransformToolBar.updateFontSizeText();
			
			lastHtmlText = textfield.htmlText; // save previous html text to revert when outside bounds for fixed frames
			
			save();
			updateTextfieldBounds();
			updateBackground();
			
			if(isOutsideFrame) 
			{
				TEXT_CHANGE.dispatch( textfield.htmlText );
			}
		}
		
		/**
		 * show background of the frame
		 * mostly for spine
		 */
		private function decreaseTillItFits():void
		{
			decreaseText(true);
			OutsideTextEditorToolbar.REFRESH_POSITION.dispatch();
		}
		
		/**
		 * show background of the frame
		 * mostly for spine
		 */
		private function showBackground(flag:Boolean = true, color:Number = Colors.RED_FLASH):void
		{
			this.opaqueBackground = (flag)?color:false;	
			//alpha = .2;
		}
		
		/**
		 * show a tooltip when user last edition doesn't fit in frame. So it was not applied
		 */
		private function showTextLimitTooltip():void
		{
			var tooltipVo : TooltipVo = TooltipVo.TOOLTIP_TEXT_LIMIT();
			tooltipVo.autoHide = 3;
			tooltipVo.uid = "textLimitTip";
			DefaultTooltip.tooltipOnClip(textfield, tooltipVo, true, 400);
		}
		
		
		
		/**
		 * update background hit zone to fit the real frame size
		 * It allows to double click on the frame to focus
		 * We need it to have same size as the frame
		 */
		private function updateBackground():void
		{
			if(contentBg){
				if(forceBackgroundRedraw)
				{
					contentBg.graphics.clear();
					var bAlpha : Number = (frameVo.tb)? frameVo.fillAlpha : 0;
					contentBg.graphics.beginFill(frameVo.fillColor,bAlpha);
					contentBg.graphics.drawRect(-10,-10,20,20);
					contentBg.graphics.endFill();
				}
				
				if(contentBg.width != frameVo.width) contentBg.width = frameVo.width;
				if(contentBg.height != frameVo.height) contentBg.height = frameVo.height;	
			}
		}
		
		
		
		
		/**
		 * completedly destroy frame
		 */
		override public function destroy():void
		{
			dispose();
			if(content)	content.removeEventListener(MouseEvent.DOUBLE_CLICK, onTextDoubleClick);
			TEXT_CHANGE.removeAll();
			super.destroy();
		}
		
		
		/**
		 * clear frame content
		 */
		override public function clearMe():void
		{
			dispose();
			frameVo.text = "";
			rollOut();
			isEdited = false;
			updateState();
			renderFrame();
			updateFrameBounds();
			//setFocus(false);
			DESELECT.dispatch(this);
			
		}
		
		/**
		 * Resize content at specific point 
		 * 0 is Top Left
		 * 2 is Top Right
		 * 5 is Bottom Left
		 * 7 is Bottom Right
		 */
		override public function resize( resizeType : int ):void
		{
			var staticPoint : Point; // static point is opposite point as the one dragged (actually the point not moving)
			
			// when starting resize, set focus to false
			//setFocus(false); // done in transform tool
			
			// new frame properties
			var newWidth : Number;
			var newHeight : Number;
			var translateX : Number;
			var translateY : Number;
			var limitCheckBounds : Rectangle;
			
			// TOP LEFT
			if( resizeType == 0){ 
				staticPoint = new Point(frameVo.width*.5, frameVo.height*.5) 
				newWidth = staticPoint.x - mouseX;
				newHeight = newWidth / frameRatio;	
				limitCheckBounds = checkResizeLimit(newWidth, newHeight);
				newWidth = limitCheckBounds.width;
				newHeight = limitCheckBounds.height;
				translateX = (frameVo.width-newWidth)*.5;
				translateY = (frameVo.height-newHeight)*.5;
			}
				// BOTTOM LEFT
			else if( resizeType == 5){
				staticPoint = new Point(frameVo.width*.5, -frameVo.height*.5) 
				newWidth = staticPoint.x - mouseX;
				newHeight = newWidth / frameRatio;	
				limitCheckBounds = checkResizeLimit(newWidth, newHeight);
				newWidth = limitCheckBounds.width;
				newHeight = limitCheckBounds.height;
				translateX = (frameVo.width-newWidth)*.5;
				translateY = (newHeight-frameVo.height)*.5;
			}
				// TOP RIGHT
			else if( resizeType == 2){
				staticPoint = new Point(-frameVo.width*.5 , frameVo.height*.5) 
				newHeight = staticPoint.y - mouseY  ;
				newWidth = newHeight * frameRatio;	
				limitCheckBounds = checkResizeLimit(newWidth, newHeight);
				newWidth = limitCheckBounds.width;
				newHeight = limitCheckBounds.height;
				translateX = (newWidth-frameVo.width)*.5;
				translateY = (frameVo.height-newHeight)*.5;
			}
				// BOTTOM RIGHT
			else if( resizeType == 7){
				staticPoint = new Point(-frameVo.width*.5 , -frameVo.height*.5) 
				newWidth = mouseX - staticPoint.x ;
				newHeight = newWidth / frameRatio;	
				limitCheckBounds = checkResizeLimit(newWidth, newHeight);
				newWidth = limitCheckBounds.width;
				newHeight = limitCheckBounds.height;
				translateX = (newWidth-frameVo.width)*.5;
				translateY = (newHeight-frameVo.height)*.5;
			}
			
			
			// apply new values
			var frameScale : Number = newWidth/frameVo.width;
			
			// as the rotation can fake our pos calculation, we need to use a matrix to reset rotation first and be sure elements are at the right place
			var m : Matrix = transform.matrix.clone();
			m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(translateX, translateY); 		// translate to new position
			m.rotate(MathUtils2.toRadian(rotation)); 	// rotate to rotation needed
			
			frameVo.zoom = frameVo.zoom*frameScale;
			
			frameVo.width = newWidth;
			frameVo.height = newHeight;
			
			frameVo.x = m.tx;
			frameVo.y = m.ty;
	
			renderFrame();
			updateFrameBounds();
		}
		private function checkResizeLimit(w:Number, h:Number):Rectangle
		{
			var r:Rectangle = new Rectangle();
			var limit : Number = 20;
			if(frameRatio<1 && w < limit){
				h = limit/w * h;
				w = limit;
			}
			else if(frameRatio>1 && h < limit)
			{
				w = limit/h * w;
				h = limit;
			}
			r.width = w;
			r.height = h;
			return r;
		}
		override public function endResize():void
		{	
		}
		
		
		/**
		 * Resize content at specific point 
		 * 1 is Top
		 * 3 is Left
		 * 4 is right
		 * 6 is Bottom
		 */
		/**
		 * Resize content at specific point 
		 * 1 is Top
		 * 3 is Left
		 * 4 is right
		 * 6 is Bottom
		 */
		override public function crop( cropType : int ):void
		{	
			if(!startMousePoint){
				startMousePoint = new Point(parent.mouseX, parent.mouseY);
				startCropRect = new Rectangle(0,0, frameVo.width, frameVo.height);
				//setFocus(false); // set focus to false when starting crop, now done in transform tool
			}
			
			// amount of translation since last frame
			var translateX : Number = startMousePoint.x - parent.mouseX;
			var translateY : Number = startMousePoint.y - parent.mouseY;
			
			// current crop values
			var cLeft : Number = 0;
			var cTop : Number = 0;
			var cRight : Number = 0;
			var cBottom : Number = 0;
			
			// LEFT 
			var newCropValue : Number
			if( cropType == 3){
				newCropValue = cLeft-translateX;
				//if(newCropValue<0) newCropValue = 0; // limit
				
				translateX = (newCropValue - cLeft)*.5;
				translateY = 0;
				cLeft = newCropValue;
				if(Math.abs(translateX) < 1) return; // add an error margin to avoid flickering when staying at the same place // TODO : nice idea, keep in mind if other problems like this
			}
				// RIGHT 
			else if( cropType == 4){
				newCropValue = cRight+translateX;
				//if(newCropValue<0) newCropValue = 0; // limit
				
				translateX = -(newCropValue - cRight)*.5;
				translateY = 0;
				cRight = newCropValue;
				if(Math.abs(translateX) < 1) return; 
			}
				// TOP 
			else if( cropType == 1){
				newCropValue = cTop-translateY;
				//if(newCropValue<0) newCropValue = 0; // limit
				
				translateY = (newCropValue - cTop)*.5;
				translateX = 0;
				cTop = newCropValue;
				if(Math.abs(translateY) < 1) return; // add an error margin to avoid flickering when staying at the same place // TODO : nice idea, keep in mind if other problems like this
			}
				// BOTTOM 
			else if( cropType == 6){
				newCropValue = cBottom+translateY;
				//if(newCropValue<0) newCropValue = 0; // limit
				
				translateY = -(newCropValue - cBottom)*.5;
				translateX = 0;
				cBottom = newCropValue;
				if(Math.abs(translateY) < 1) return;  
			}
			
			// store previous pos
			startMousePoint = new Point(parent.mouseX, parent.mouseY);
			
			// new frame width
			var newWidth:Number = frameVo.width - cLeft - cRight;
			var newHeight:Number = frameVo.height - cTop - cBottom;
			
			// as the rotation modify the pos calculation, we need to use a matrix to reset rotation first and be sure elements are at the right place
			var m : Matrix = transform.matrix.clone();
			m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(translateX, translateY); 		// translate to new position
			m.rotate(MathUtils2.toRadian(rotation)); 	// rotate to rotation needed
			
			frameVo.width = (newWidth < 20)? 20 : newWidth;
			frameVo.height = (newHeight <20)? 20: newHeight;
			frameVo.x = m.tx;
			frameVo.y = m.ty;
			
			renderFrame();
			updateTextfieldBounds(cropType);
			
			// reset bounds to start crop rect if there is enough space
			if((cropType == 3 || cropType == 4) && frameVo.height > getTextHeight()+1 && frameVo.height > startCropRect.height){
				frameVo.height = startCropRect.height;
				renderFrame();
			}
		}
		
		
		public override function endCrop():void
		{
			startMousePoint= null;
			startCropRect = null;
		}
		
		
		/**
		 * Save frame properties
		 * > save frame propeties into photovo when there is a modification or when a frame is created (x, y, rotation, filters, bounds, cropping etc...)
		 * > call this method each time the frame is modified
		 * > Could have to be refined later on depending on how we will save those informtions in DB
		 */
		override public function save():void
		{
			if( !isOutsideFrame ) 
			{
				super.save();
			}

			if(textfield) frameVo.text = textfield.htmlText;
			/*
			if(!frameVo.isEmpty)
			{
				frameVo.text = textfield.htmlText;
				//frameVo.textFormat = textfield.getTextFormat();
			}
			*/
			
		}
		
		/**
		 * Update frame boundaries value
		 * > after a resize (or zoom)
		 * > after a crop
		 */
		override public function updateFrameBounds():void
		{
			super.updateFrameBounds();
		}
		
		
		/**
		 * setter for isSticked
		 * a frame is sticked when the focus of the transform tool is on it
		 */
		override public function set isSticked(value:Boolean):void
		{
			// set private value
			_isSticked = value;
			
			// for frame text, when setting focus to false, we must be sure to remove focus of text
			if(!value) setFocus(false);
			
			if(value) // the transform tool stick on this frame
			{
				rollOut(true);
				if(allowMove && !hasFocus) CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
			else // the transform tool is not editing this frame
			{
				//If no action has been made on the textfield, we clear it (put cta back up)
				/*if(textfield && textfield.text == watermark && !isEdited)
					clearMe();*/
				
				updateState();
				
				setListeners();
			}
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Update state based on frameVo
		 */ 
		private function updateState(fromCTA:Boolean = false):void
		{
			if(frameVo.text != "" || fromCTA || isOutsideFrame) // if frame has linkage, never go to CTA state, it's controlled by something else
			{
				// remove cta if still there
				if(cta && cta.parent) cta.parent.removeChild(cta);
				//Create textifled if needed
				if(!textfield)
				construct();
			}
			else
			{
				//If no action has been made on the textfield, we clear it (put cta back up)
				if(textfield && (textfield.text == watermark || textfield.text == ""))
					clearMe();
				
				//create the call to action if needed or adchild it
				if(!cta) createCTA(.3, Colors.BLUE_LIGHT);
				else if(!cta.parent) addChild(cta);
				updateCTA(EditionArea.editorScale);
			}	
		}
		
		private function frameReady():void
		{
			//Cronstruction finish, tell everyone!
			if(!isOutsideFrame) CREATED.dispatch(this);	 // we call ready only if the frame is not a linked frame
		}
			
		/**
		 * dispose content before going to CTA
		 */ 
		private function dispose():void
		{
			// remove possible edition toolbar tooltip
			DefaultTooltip.killTooltipById("textLimitTip");
			
			if(textfield)
			{
				textfield.removeEventListener(Event.CHANGE,changeHandler);
				textfield.removeEventListener(FocusEvent.FOCUS_OUT,focusOutHandler);
				textfield.removeEventListener(FocusEvent.FOCUS_IN,focusInHandler);
				textfield.parent.removeChild(textfield);
				textfield = null;				
			}
			if(content && contains(content))
			{
				removeChild(content);
				content = null;
			}			
		}
		
		/**
		 * set focus on textfield and start text edition
		 * flag : true or false to put focus on or off
		 * forceCreation : force textfield creation and state to go on editing
		 * selectAll : select all content on focus
		 */
		public function setFocus( flag : Boolean = true, forceCreation : Boolean = false, selectAll:Boolean = false ):void
		{
			if(!textfield){ // there is no textfield right now
				if(forceCreation) { 
					updateState(true);
					renderFrame();
					setFocus(true, false, selectAll); // reset focus after textField creation
				}
				else hasFocus = false; // no textfield and no forceCreation, we do nothing
				return;
			}
			//if(_hasFocus == flag) return;
			
			/*
			if(!isOutsideFrame && (frameVo.width < 50 || frameVo.height < 50)) {
				TransformTool.editTextOutsideFrame();
				return;
			}
			*/
			if(flag && !isOutsideFrame) {
				TransformTool.editTextOutsideFrame(this)
				return;
			}
			hasFocus = flag;
			
			
			content.removeEventListener(MouseEvent.DOUBLE_CLICK, onTextDoubleClick);
			content.mouseChildren = flag;
			content.doubleClickEnabled = !flag;
			//if(frameVo) frameVo.allowMove = !flag;
			
			// put focus on field
			if(flag)
			{
				CursorManager.instance.reset();
				if(!isOutsideFrame && !isSticked) SELECT.dispatch(this); // we call selection only if the frame is not a linked frame
				//SELECT.dispatch(this); 	
				
				if(!frameVo.isPageNumber)
				stage.focus = textfield;
				//textfield.background = true;
				content.opaqueBackground = (isOutsideFrame)? Colors.WHITE : Colors.BLUE_LIGHT; // (textfield.textColor == Colors.WHITE)? Colors.GREY : Colors.WHITE; // TODO : maybe find a way to get correct font color (but as we can use multiple colors now, lets use grey by default)
				//add listener to update toolbar based on the selected text (selection always finish with a mouse UP)
				content.removeEventListener(MouseEvent.MOUSE_UP, updateToolBar);
				content.addEventListener(MouseEvent.MOUSE_UP, updateToolBar);
				
			}
			else
			{
				content.opaqueBackground = (isOutsideFrame)? Colors.WHITE : null;
				//TweenMax.to( textfield, 0, {tint:null});
				content.addEventListener(MouseEvent.DOUBLE_CLICK, onTextDoubleClick);
				content.removeEventListener(MouseEvent.MOUSE_UP, updateToolBar);
				//if(frameVo) frameVo.allowMove = flag;
			}
			
			
			// update boudns
			updateTextfieldBounds();
			
			// selection
			if(flag && selectAll){
				textfield.setSelection(0, textfield.text.length);
			}
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PROTECTED METHODS
		////////////////////////////////////////////////////////////////
		
		/*protected function drawClickZone(_x:Number,_y:Number,_width:Number,_height:Number ):void
		{
			var g:Graphics = clickZone.graphics;
			g.clear();
			g.beginFill(0x000000,0);
			g.drawRect(_x,_y,_width,_height);
			g.endFill();
		}*/
		
		
		protected function updateTextfieldBounds(cropType:int = -1):void
		{
			if( isFixed ) return; // do not change bounds
			
			//if(cropType != 3 && cropType != 4 && allowCrop) //exceptions: right and left
			if(textfield && allowCrop) //exceptions: right and left
			{
				if( frameVo.height < getTextHeight() ) {
					frameVo.height =  getTextHeight();
					textfield.height = frameVo.height;
					renderFrame();
				}
				
				if( frameVo.width < 20 ) 
				{
					frameVo.width = 20;
					textfield.width = 20;
					textfield.x = - frameVo.width * .5;
					renderFrame();
				}
			}
			
			updateFrameBounds();
		}
		
		protected function getTextHeight():Number
		{
			if(textfield && frameVo ){
				return (textfield.textHeight+4) * frameVo.zoom;
			}
			return -1;
		}
		
		
		/**
		 * EVENT HANDLERS
		 * */
		
		/**
		 * on Image over
		 */
		override protected function over(force:Boolean = false):void
		{
			if(isOutsideFrame) return;
			if(!isSticked || force)
				super.over(force);
			else
			{
				if(allowMove && !hasFocus) CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
		}
		/**
		 * on frame over
		 */
		override protected function rollOut(force:Boolean = false):void
		{
			if(isOutsideFrame) return;
			if(!isSticked || force)
				super.rollOut(force);
			else
			{
				CursorManager.instance.reset();
			}
		}
		
		protected function focusOutHandler(event:FocusEvent):void
		{
			
		}
		
		protected function focusInHandler(event:FocusEvent):void
		{
			//removeListeners();
			//SELECT.dispatch(this);
			//trace("Facus in");
		}
		
		
		protected function onTextDoubleClick(event:MouseEvent):void
		{
			setFocus(true,false, true);
		}
		
		protected function updateToolBar(event:MouseEvent = null):void
		{
			//update tool bar
			if(isSticked)
				TransformToolBar.UPDATE_TRANSFORM_TOOLBAR_TEXTBTN_STATE.dispatch();
		}
		
	}
}