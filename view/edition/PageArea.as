/**
 * jonathan@nguyen.eu
 * 
 * PageArea is the view of a page
 * Extends dropTargetArea to accept drag and drop actions
 * 
 * It needs a PageVo to construct itself
 * It can contain frames, texts, backgrounds, clip arts etc...
 * 
 */
package view.edition
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import comp.DropTargetArea;
	import comp.Image;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	import data.FrameVo;
	import data.Infos;
	import data.OverlayerVo;
	import data.PageVo;
	import data.PhotoVo;
	import data.ProductsCatalogue;
	import data.QRVo;
	
	import library.spine.bmp;
	
	import manager.BackgroundsManager;
	import manager.CalendarManager;
	import manager.CanvasManager;
	import manager.DragDropManager;
	import manager.LayoutManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.PhotoSwapManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import mcs.transformToolbar;
	
	import org.osflash.signals.Signal;
	
	import utils.AssetUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.VectorUtils;
	
	import view.edition.pageNavigator.PageNavigator;
	
	public class PageArea extends DropTargetArea
	{
		////////////////////////////////////////////////////////////////
		//	PROP
		////////////////////////////////////////////////////////////////
		
		public static const ADD_TEXT_TO_PAGE : Signal = new Signal();
		public static const ADD_PHOTO_TO_PAGE : Signal = new Signal();
		public static const ADD_QR_TO_PAGE : Signal = new Signal();
		public static const GRID_COLS_NUMBER:Array = [0,15, 30]; 
		public static var CURRENT_GRID_INDEX:int = 0; 
		public static var GRID_ALPHA:Number = .2; 
		
		public var FRAME_CREATED:Signal = new Signal(Frame);
		public var FRAME_DELETED:Signal = new Signal(Frame);
		
		// public
		public var pageVo:PageVo;
		
		// private
		private var frames:Vector.<Frame> = new Vector.<Frame>();
		private var framesDepthInfo : Array;
		private var _pageCenter:Point;
		private var _hasFixedLayout : Boolean = false;
		private var _defautFrameWidth:Number;
		
		// ui
		private var frameContainer : Sprite;
		private var canvasEdge : Sprite; // edge ui for canvas
		private var kadapakBorder : Sprite; //
		private var grid:Sprite;
		private var spineZone:Sprite;
		private var spineBmp:Bitmap;
		////////////////////////////////////////////////////////////////
		//	CONST
		////////////////////////////////////////////////////////////////

		

		
		
		public function PageArea(typeVo:PageVo)
		{
			super();
			this.pageVo = typeVo; // rename the vo as it easier to understand which kind of Vo we're dealing with
			opaqueBackground = Colors.BACKGROUND_COLOR;
			
			// fixed layout or not?
			_hasFixedLayout = LayoutManager.instance.isLayoutFixed( pageVo.layoutId );
			trace("layout is fixed : "+_hasFixedLayout);
			
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////
		
		public function get defautFrameWidth():Number
		{
			return pageVo.bounds.width/2;
		}

		public function get pageCenter():Point
		{
			return new Point(pageVo.bounds.width/2,pageVo.bounds.height/2);
		}
		
		/**
		 * The page has a fixed layout and can't be updated
		 */
		public function get hasFixedLayout():Boolean
		{
			return _hasFixedLayout;
		}	
		
		
		
		/**
		 *	check if a frame exist with frameVo and return it 
		 */
		public function getBackgroundFrame():FrameBackground
		{
			var backgroundFrame : FrameBackground;
			if(frames && frames.length>=1)
				backgroundFrame = frames[0] as FrameBackground;
			if(!backgroundFrame) {
				Debug.warn("PageArea > updateBackgroundFrame > background frame not found");
				return null;
			}
			
			return backgroundFrame;
		}
		
		/**
		 *	check if a frame exist with frameVo and return it 
		 */
		public function getFrameByFrameVo( frameVo : FrameVo ):Frame
		{
			for (var i:int = 0; i < frames.length; i++) 
			{
				var f : Frame = frames[i];
				if(f.frameVo == frameVo) return f;
			}
			
			return null;
		}
		
		/**
		 *	check if a frame exist with frameVo and return it 
		 */
		public function getFramesByPhotoVo( photoVo : PhotoVo ):Vector.<FramePhoto>
		{
			var currentFrameList : Vector.<FramePhoto> = new Vector.<FramePhoto>;
			for (var i:int = 0; i < frames.length; i++) 
			{
				var f : Frame = frames[i];
				if(f is FramePhoto && f.frameVo.photoVo && f.frameVo.photoVo.id == photoVo.id) 
					currentFrameList.push(f); 
			}
			
			return currentFrameList;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		public function init():void
		{
			construct();
			ADD_TEXT_TO_PAGE.add(addTextToPageHandler);
			ADD_PHOTO_TO_PAGE.add(addPhotoToPageHandler);
			ADD_QR_TO_PAGE.add(addQrToPageHandler);
			PhotoSwapManager.SWAP_IMAGE_MODE.add(swapImageModeHandler);
		}

		
		
		/**
		 * If project is canvas, pageArea must also display the edge of the canvas (which can be edited)
		 * We draw this once a page is drawn and destroy it when it's destroyed
		 */
		public function drawCanvasEdge():void
		{
			canvasEdge = new Sprite();
			var edgeAlpha:Number = .5;
			if(pageVo.layoutVo.id == "908" || pageVo.layoutVo.id == "916" || pageVo.layoutVo.id == "926" ) edgeAlpha = 0; // this is hardcoded hack for now, we should maybe add some stuff in xml, let's see later
			canvasEdge.mouseEnabled = canvasEdge.mouseChildren = false;
			var g : Graphics = canvasEdge.graphics;
			var edgeSize : int = CanvasManager.instance.edgeSize;
			var w : Number = pageVo.bounds.width + edgeSize * 2;
			var h : Number = pageVo.bounds.height + edgeSize * 2;
			g.beginFill(Colors.WHITE, edgeAlpha);
			g.lineStyle(1,0x000000,.2,true);
			
			g.lineTo(w, 0);
			g.lineTo(w, h);
			g.lineTo(0,h);
			g.lineTo(0,0);
			
			w = pageVo.bounds.width;
			h = pageVo.bounds.height;
			g.moveTo(edgeSize, edgeSize);
			g.lineStyle(1,0x000000,.5,true);
			g.lineTo(edgeSize+w, edgeSize);
			g.lineTo(edgeSize+w,edgeSize+h);
			g.lineTo(edgeSize,edgeSize+h);
			g.lineTo(edgeSize, edgeSize);
			g.endFill();
			
			canvasEdge.x = -edgeSize;
			canvasEdge.y = -edgeSize;
			
			//parent.addChildAt(canvasEdge,parent.getChildIndex(this)+1);
			addChild(canvasEdge);
		}
		
		
		/**
		 * If project is canvas and kadapak, we need to draw kadapak border
		 */
		public function drawKadapakBorder():void
		{
			kadapakBorder = new Sprite();
			kadapakBorder.mouseEnabled = kadapakBorder.mouseChildren = false;
			var g : Graphics = kadapakBorder.graphics;
			var borderSize : int = Math.round(10 * MeasureManager.mmToPoint);// CanvasManager.instance.edgeSize;
			var innerSize : int = Math.round(2 * MeasureManager.mmToPoint);
			var w : Number = pageVo.bounds.width + borderSize * 2 - innerSize*2;
			var h : Number = pageVo.bounds.height + borderSize * 2 - innerSize*2;
			g.beginFill(CanvasManager.instance.getKadapakFrameColor(Infos.project.canvasFrameColor), 1);
			
			g.lineTo(w, 0);
			g.lineTo(w, h);
			g.lineTo(0,h);
			g.lineTo(0,0);
			
			w = pageVo.bounds.width;
			h = pageVo.bounds.height;
			g.moveTo(borderSize, borderSize);
			//g.lineStyle(1,0x000000,.5,true);
			g.lineTo(borderSize+w, borderSize);
			g.lineTo(borderSize+w,borderSize+h);
			g.lineTo(borderSize,borderSize+h);
			g.lineTo(borderSize, borderSize);
			g.endFill();
			
			kadapakBorder.x = -borderSize;
			kadapakBorder.y = -borderSize;
			
			//parent.addChildAt(canvasEdge,parent.getChildIndex(this)+1);
			addChild(kadapakBorder);
		}
		
		
		
		/**
		 * Destroy the pageArea
		 * Removes all the frames/texts/cliparts/background from memory (at least it should be)
		 * Removes skins 
		 * Bref, Removes everything
		 * I kinda like to comment everything now :)
		 */
		public function destroy():void
		{
			Debug.log("PageArea.destroy page ("+pageVo.index+") with "+frames.length+" frames");
			
			//Spine zone
			if(spineZone)
			{
				spineZone.graphics.clear();
				if(spineZone.parent)
					spineZone.parent.removeChild(spineZone);
				spineZone = null;
				
				if(spineBmp)
				{
					spineBmp.bitmapData.dispose();
					if(spineBmp.parent)
						spineBmp.parent.removeChild(spineBmp);
					spineBmp = null;
				}
			}
			
			// Canvas only, remove canvas edge
			if(canvasEdge){
				if(canvasEdge.parent) canvasEdge.parent.removeChild(canvasEdge);
				canvasEdge = null;
			}
			
			if(kadapakBorder){
				if(kadapakBorder.parent) kadapakBorder.parent.removeChild(kadapakBorder);
				kadapakBorder = null;
			}
			
			for (var i:int = 0; i < frames.length; i++) 
			{
				var frame:Frame = frames[i];
				destroyFrame(frame);
				i--;
			}
			frames = null;
			framesDepthInfo = null;
			graphics.clear();
			FRAME_CREATED.removeAll();
			FRAME_DELETED.removeAll();
			ADD_TEXT_TO_PAGE.remove(addTextToPageHandler);
			ADD_QR_TO_PAGE.remove(addQrToPageHandler);
			PhotoSwapManager.SWAP_IMAGE_MODE.remove(swapImageModeHandler);
			
			
		}
		
		/**
		 * add a Framevo to a page (without the need to refresh page, otherwise we can simply add it to layoutVo)
		 * > create a corresponding frame for the framevo
		 * > init frame and listener
		 * > add frame to page and page vo
		 * > redraw navigator
		 * 
		 */
		/* SHOULD WORK BUT NOT USED NOR TESTED
		public function addFrameVoToPage( frameVo : FrameVo ) : void
		{
			Debug.log("PageArea.addFrameVoToPage");
			
			//Create Frame
			var frame:Frame;
			switch (frameVo.type){
				case FrameVo.TYPE_CLIPART : 
					frame = new FramePhoto (frameVo);
					break;
				case FrameVo.TYPE_PHOTO : 
					frame = new FramePhoto (frameVo);
					break;
				case FrameVo.TYPE_TEXT : 
					frame = new FrameText (frameVo);
					break;
				case FrameVo.TYPE_OVERLAYER : 
					frame = new FrameOverlayer (frameVo);
					break;
			}
			
			// security
			if(!frame) return;
			
			//listener
			frame.CREATED.add(frameCreated);
			//init
			frame.init();
			// add this frame to the page
			addNewFrameToPage(frame);
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
		}
		*/
		
		
		
		/**
		 * Create and Add a frame to the page based on a calendar Frame (Drag and drop move)
		 */
		public function addFramePhotoToCalendarFrame(calendarFrame:FrameCalendar, photoVo:PhotoVo):void
		{
			// EDIT :: we do not clear frame anymore as we now can have multiple linked frames (text AND photo)
			//if(calendarFrame.linkedFrame) calendarFrame.clearMe();
			
			// first we check if there is already a frame photo linked
			var linkedFrames:Vector.<Frame> = calendarFrame.linkedFrames;
			var linkedPhotoFrame : FramePhoto = calendarFrame.linkedPhotoFrame;
			var linkedTextFrame : FrameText = calendarFrame.linkedTextFrame;
			
			// if we have a frame photo, just update it
			if( linkedPhotoFrame )
			{
				linkedPhotoFrame.update(photoVo);
			}
			
			// otherwise create it
			else 
			{
				var calendarFrameVo:FrameVo = calendarFrame.frameVo;
				
				//creating frame photo
				var frameVo:FrameVo = new FrameVo(photoVo);
				frameVo.x = calendarFrameVo.x;
				frameVo.y = calendarFrameVo.y;
				frameVo.width = calendarFrameVo.width;
				frameVo.height = calendarFrameVo.height;
				//set date Linkage id
				frameVo.dateLinkageID = CalendarManager.instance.getDateLinkageId(calendarFrameVo);
				
				var frame:FramePhoto = new FramePhoto(frameVo);
				//listener
				frame.CREATED.add(frameCreated);
				//init
				frame.init();
				frame.update(photoVo);
				
				// add this frame to the page, below the linked text frame if there is one
				if(linkedTextFrame) addNewFrameToPage(frame, calendarFrame.parent.getChildIndex(linkedTextFrame)); 
				// otherwise just below the calendar frame
				else addNewFrameToPage(frame, calendarFrame.parent.getChildIndex(calendarFrame)); 
				
				//Link the frame to the calendar frame
				var framesToLink : Vector.<FrameVo> = new Vector.<FrameVo>();
				framesToLink.push(calendarFrame.frameVo);
				framesToLink.push(frame.frameVo); // add new frame photo
				if(linkedTextFrame) framesToLink.push(linkedTextFrame.frameVo); // if we already have a text frame linked, we link it too
				PagesManager.instance.linkFramesVoTogether(framesToLink, calendarFrame.frameVo.uniqFrameLinkageID);
				
				// update possible background colors when adding new text frame to page
				if(pageVo.customColors && pageVo.customColors.dayBackground != -1)
				{
					calendarFrame.updateColors();
				}
			}
			
			// Update navigator
			PageNavigator.RedrawPage(pageVo);
			
			// update transform tool selection
			calendarFrame.SELECT.dispatch(calendarFrame);
		}
		
		/**
		 * Create and Add a frame to the page based on a calendar Frame (Drag and drop move)
		 */
		private function addOrEditFrameTextToCalendarFrame(calendarFrame:FrameCalendar):void
		{
			// EDIT :: we do not clear frame anymore as we now can have multiple linked frames (text AND photo)
			//if(calendarFrame.linkedFrame) calendarFrame.clearMe();
			
			// first we check if there is already a frame photo linked
			var linkedFrames:Vector.<Frame> = calendarFrame.linkedFrames;
			var linkedPhotoFrame : FramePhoto = calendarFrame.linkedPhotoFrame;
			var linkedTextFrame : FrameText = calendarFrame.linkedTextFrame;
			
			// if we do not already have a text frame, we create it
			if( !linkedTextFrame ) 
			{
				var calendarFrameVo:FrameVo = calendarFrame.frameVo;
				var frameVo:FrameVo = new FrameVo();
				frameVo.type = FrameVo.TYPE_TEXT;
				frameVo.x = calendarFrameVo.x;
				frameVo.y = calendarFrameVo.y;
				frameVo.width = calendarFrameVo.width;
				frameVo.height = calendarFrameVo.height;
				frameVo.dateLinkageID = CalendarManager.instance.getDateLinkageId(calendarFrameVo);
				
				frameVo.fontSize = MeasureManager.getFontSize(25);
				
				var frame:FrameText = new FrameText(frameVo);
				frame.CREATED.add(frameCreated);
				
				//init
				frame.init();
				
				// add this frame to the page
				addNewFrameToPage(frame, calendarFrame.parent.getChildIndex(calendarFrame)); // add it below calendar frame
				
				//Link the frame to the calendar frame
				var framesToLink : Vector.<FrameVo> = new Vector.<FrameVo>();
				framesToLink.push(calendarFrame.frameVo);
				framesToLink.push(frame.frameVo); // add new frame text
				if(linkedPhotoFrame) framesToLink.push(linkedPhotoFrame.frameVo); // if we already have a photo frame linked, we link it too
				PagesManager.instance.linkFramesVoTogether(framesToLink, calendarFrame.frameVo.uniqFrameLinkageID);
				
				// update possible background colors when adding new text frame to page
				if(pageVo.customColors && pageVo.customColors.dayBackground != -1)
				{
					calendarFrame.updateColors();
				}
				
				// page vo updated
				PageNavigator.RedrawPage(pageVo);
				
				// be sure to update transform tool with new content
				calendarFrame.SELECT.dispatch(calendarFrame);
			}
			
			// open outside text editor
			TransformTool.editTextOutsideFrame(calendarFrame);
		}
		
		
		
		/**
		 * update background frame
		 */
		public function updateBackgroundWithBackgroundVo( backgroundVo : BackgroundVo ):void
		{
			var backgroundFrame : FrameBackground = frames[0] as FrameBackground;
			if(!backgroundFrame) {
				Debug.warn("PageArea > updateBackgroundFrame > background frame not found");
				return;
			}
			
			backgroundFrame.update(backgroundVo);
			// redraw page
			//PageArea.REDRAW_PAGE_IN_NAVIGATOR.dispatch( pageVo );
		}
		
		/**
		 * update background frame with Fill color
		 */
		public function updateBackgroundWithColor( fillColor : Number ):void
		{
			var backgroundFrame : FrameBackground = frames[0] as FrameBackground;
			if(!backgroundFrame) {
				Debug.warn("PageArea > updateBackgroundFrame > background frame not found");
				return;
			}
			
			//Dispose any current bkg type (image or color)
			backgroundFrame.clearMe();
			//apply new color to the frameVo
			backgroundFrame.frameVo.fillColor = fillColor;
			//update the frame
			backgroundFrame.update(null);
			// redraw page
			//PageArea.REDRAW_PAGE_IN_NAVIGATOR.dispatch( pageVo );
		}
		
		/**
		 * update background frame with custom bkg
		 */
		public function updateBackgroundWithCustom( backgroundCustomVo : BackgroundCustomVo ):void
		{
			var backgroundFrame : FrameBackground = frames[0] as FrameBackground;
			if(!backgroundFrame) {
				Debug.warn("PageArea > updateBackgroundFrame > background frame not found");
				return;
			}
			
			backgroundFrame.updateWithCustom(backgroundCustomVo.frameVoStr);
		}
		
		
		
		/**
		 * Create and Add a frame to the page based on a photoVo (Drag and drop move)
		 */
		public function createFrameFromPhotoVo(photoVo:PhotoVo, atMousePos : Boolean = true):void
		{
			// position to create frame
			var framePos : Point = null;
			if(atMousePos){
				framePos = new Point();
				framePos.x = mouseX;
				framePos.y = mouseY;	
			}
			
			// first we create frameVO in pageVo
			var newFrameVo : FrameVo = pageVo.createNewFrameFromPhotoVo( photoVo, framePos );
			
			// then we add it to the view
			var newFrame : Frame = createFrameFromFrameVo( newFrameVo );
			
			// update navigator
			PageNavigator.RedrawPage(pageVo);
			
			// stick to frame
			newFrame.SELECT.dispatch(newFrame);
		}
		
		
		/**
		 * add overlayer to existing frame
		 */
		public function addOverlayerToFrame( currentFrame:FramePhoto, overlayerVo:OverlayerVo ):void
		{
			// we do nothing if overlayer id is blank
			if( overlayerVo.id == "blank"){
				return;
			}
			
			// get current frame bounds
			var frameVo:FrameVo = new FrameVo( overlayerVo );
			frameVo.x = currentFrame.x;
			frameVo.y = currentFrame.y;
			frameVo.rotation = currentFrame.rotation;
			
			var frameScaleWidth : Number = currentFrame.width / overlayerVo.width;
			var frameScaleHeight : Number = currentFrame.height / overlayerVo.height;
			var frameScale : Number = (frameScaleHeight < frameScaleWidth)? frameScaleHeight : frameScaleWidth;
			
			frameVo.width = overlayerVo.width * frameScale; 
			frameVo.height = overlayerVo.height * frameScale;
			
			
			// create new overlayer frame
			var frame:FrameOverlayer = new FrameOverlayer(frameVo);
			frame.CREATED.add(frameCreated);
			frame.init();
			frame.update(overlayerVo);
			
			// deselect current frame
			currentFrame.DESELECT.dispatch(currentFrame);
			
			// create linkage
			var linkedFrames:Vector.<FrameVo> = new Vector.<FrameVo>;
			linkedFrames.push(currentFrame.frameVo);
			linkedFrames.push(frameVo);
			PagesManager.instance.linkFramesVoTogether(linkedFrames);
			
			// add frame
			addNewFrameToPage(frame, currentFrame.parent.getChildIndex(currentFrame)+1);
			
			// update frame to fit inside overlayer 
			frame.adaptLinkedFrameToOverlayer();
			
			// TODO : block photo frame user actions
			
			
			// update navigator
			PageNavigator.RedrawPage(pageVo);
			
			// select overlayer frame
			frame.SELECT.dispatch(frame);
			
		}
		
		
		/**
		 * update page content depending on state fullscreen (preview) or normal
		 * > show/hide empty frames
		 */
		public function updateFullScreenState():void
		{
			var frame : Frame;
			var isPreview : Boolean = EditionArea.isPreview();
			for (var i:int = 0; i < frames.length; i++) 
			{
				frame = frames[i];
				frame.visible = (frame.frameVo.isEmpty && isPreview)? false : true;
			}
			
			// canvas full screen do not show the edge part
			if(Infos.isCanvas){
				var edgeSize : Number = CanvasManager.instance.edgeSize;
				if(isPreview){
					if(CanvasManager.instance.isKadapak) {
						var borderWidth : Number = 5*MeasureManager.mmToPoint + 2 // add two pixels for the scrollrect redraw system
						scrollRect = new Rectangle(-borderWidth,-borderWidth,pageVo.bounds.width + borderWidth*2 ,pageVo.bounds.height + borderWidth*2);
					}
					else scrollRect = new Rectangle(0,0,pageVo.bounds.width,pageVo.bounds.height);
					if(canvasEdge) canvasEdge.visible = false;
				} else {
					if(CanvasManager.instance.isKadapak) {
						borderWidth = 5*MeasureManager.mmToPoint + 2 // add two pixels for the scrollrect redraw system
						scrollRect = new Rectangle(-borderWidth,-borderWidth,pageVo.bounds.width + borderWidth*2 ,pageVo.bounds.height + borderWidth*2);
					}
					else scrollRect = new Rectangle(-edgeSize,-edgeSize,pageVo.bounds.width+edgeSize*2,pageVo.bounds.height+edgeSize*2);
					if(canvasEdge) canvasEdge.visible = true;
				}
			}
		}
		
		/**
		 * Draw a grid over everything to help user with alignment
		 * */
		public function drawGrid():void
		{
			if(GRID_COLS_NUMBER[CURRENT_GRID_INDEX] == 0)
			{
				grid.graphics.clear();
				grid.visible = false;
				alpha = 1;
				return;
			}

			grid.visible = true;
			var pagesBounds:Rectangle = pageVo.bounds;
			var col:int = GRID_COLS_NUMBER[CURRENT_GRID_INDEX];
			col = (this.pageVo.isCover)?col*2:col;
			var gridWidth:Number = pagesBounds.width/col;
			var row:int = pagesBounds.height/gridWidth;
			var gridHeight:Number = pagesBounds.height/row;
			var i:int;
			var startY:Number = pagesBounds.y;
			var startX:Number = pagesBounds.x;
			var g:Graphics = grid.graphics;
			
			g.clear();
			g.lineStyle(1,Colors.GRID_COLOR,GRID_ALPHA);
			
			
			//vertical lines
			for (i = 0; i < col; i++) 
			{
				g.moveTo(startX + (i*gridWidth),startY);
				g.lineTo(startX + (i*gridWidth),startY + row*gridHeight);
			}
			
			//horizontal lines
			for ( i = 0; i < row+1; i++) 
			{
				g.moveTo(startX, startY + (i*gridHeight));
				g.lineTo(startX + col*gridWidth, startY + (i*gridHeight));
			}
			addChild(grid);
		}			
		
		/**
		 * on image swap mode start or stop
		 */
		private function swapImageModeHandler( flag : Boolean ):void
		{
			var frame : Frame;
			var activated : Boolean = false;
			for (var i:int = 0; i < frames.length; i++) 
			{
				frame = frames[i];
				//if(flag) activated = ( frame.frameVo.type == FrameVo.TYPE_PHOTO && ! frame.isEmpty ) ? true : false;
				if(flag) activated = ( frame.frameVo.type == FrameVo.TYPE_PHOTO) ? true : false;
				else activated = true;
				frame.alpha = (activated)?1:.2;
				frame.mouseEnabled = frame.mouseChildren = activated;
				
			}
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Construction of the view
		 * Diplay the area
		 * Checking for existing frames/texts/cliparts/backgrounds (empty or not)
		 */
		protected function construct():void
		{
			Debug.log("PageArea.construct");
			
				
			//display area : TODO: Replace graphics by assets
			graphics.beginFill(0xffffff,1);
			graphics.lineStyle(1,0xa7a7a7);
			graphics.drawRect(pageVo.bounds.x,pageVo.bounds.y,pageVo.bounds.width,pageVo.bounds.height);
			
			
			
			
			// canvas content displays the edge on the screen, we must then change the scrollrect bounds
			if(Infos.isCanvas){
				if(CanvasManager.instance.isKadapak) {
					var borderWidth:Number = 5*MeasureManager.mmToPoint + 2 // add two pixels for the scrollrect redraw system
					scrollRect = new Rectangle(-borderWidth,-borderWidth,pageVo.bounds.width + borderWidth*2 ,pageVo.bounds.height + borderWidth*2);
				}
				else{
					var edgeSize : Number = CanvasManager.instance.edgeSize;
					scrollRect = new Rectangle(-edgeSize,-edgeSize,pageVo.bounds.width+edgeSize*2,pageVo.bounds.height+edgeSize*2);
				}
				 
			}else{
				scrollRect = new Rectangle(0,0,pageVo.bounds.width,pageVo.bounds.height); 	
			}
			
			
			
			// frame container
			frameContainer = new Sprite();
			addChild(frameContainer);
			
			//display page info index
			var text:TextField = new TextField();
			text.background = true;
			text.backgroundColor = Colors.WHITE;
			text.autoSize = "left"; text.multiline = true;
			text.text = String("docCode: "+Infos.project.docCode+" / index: "+pageVo.index+" / LayoutId: "+pageVo.layoutVo.id);
			text.appendText(String("\nisCover: "+pageVo.isCover+" / CoverType: "+pageVo.coverType+" / LayoutVo.isCalendar: "+pageVo.layoutVo.isCalendar));
			text.appendText(String("\nPage width: "+pageVo.bounds.width+" / Page height: "+pageVo.bounds.height));
			if(pageVo.isCalendar)
			text.appendText(String("\nMonth Index: "+pageVo.monthIndex));
			//text.scaleX = text.scaleY = 1+(1-EditionArea.editorScale);
			if(Debug.SHOW_PAGE_INFOS) addChild(text);
			Debug.log("PageArea > details : " + text.text);
			
			
			//create Frames
			var frameVo : FrameVo;
			for (var i:int = 0; i < pageVo.layoutVo.frameList.length; i++) 
			{
				frameVo = pageVo.layoutVo.frameList[i] as FrameVo;
				createFrameFromFrameVo(frameVo);
			}
			
			//Show spine zone
			if(Infos.isAlbum && pageVo.isCover && pageVo.coverType==ProductsCatalogue.COVER_CUSTOM)
			{
				spineZone = new Sprite();
				spineZone.mouseEnabled = false;
				var sg:Graphics = spineZone.graphics;
				var spineWidth:Number = Infos.project.getSpineWidth() + 2;
				sg.beginFill(Colors.GREY,0);
				sg.lineStyle(1,Colors.GREY);
				sg.drawRect(0,0,spineWidth,pageVo.bounds.height);
				spineZone.x = (pageVo.bounds.width - spineZone.width) /2 + 1;
				/*spineBmp = new Bitmap(new library.spine.bmp());
				spineBmp.width = spineZone.width;
				spineBmp.height = spineZone.height;
				spineZone.alpha = .7;
				spineZone.addChild(spineBmp);*/
				addChild(spineZone);
			}
			
			// update frameDepthInfos
			updateFramesDepthInfo();
			
			// show/hide empty frames if fullscreen
			updateFullScreenState();
			
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
			
			
			//addGrid
			grid = new Sprite();
			grid.mouseChildren = false;
			grid.mouseEnabled = false;
			addChild(grid);
			drawGrid();
		}
		
		
		
		/**
		 * add a new frame to this page
		 * > check 
		 */
		private function addNewFrameToPage( frame : Frame, index:int = -1) : void
		{
			if(!frames) {
				Debug.warn("PageArea.addNewFrameToPage : frames is null");
				return;
			}
			if(index == -1 ) 
			{
				if(pageVo.layoutVo.frameList[pageVo.layoutVo.frameList.length-1].dateAction == CalendarManager.DATEACTION_URL)
					index = frames.length-1;
				else
					index = frames.length;
			}
			
			// add to display list (DISPLAY LIST)
			frameContainer.addChildAt(frame, index);
			// add to frame list (VIEW LIST)
			frames.splice(index,0,frame); //frames.push(frame);
			// add to layout list (DATA)
			PagesManager.instance.addFrameVoToPage(pageVo,index,frame.frameVo);
			//pageVo.layoutVo.frameList.splice(index,0,frame.frameVo); //pageVo.layoutVo.frameList.push(frame.frameVo);  // TODO : this should be done using index param too
			
			// update depth
			updateFramesDepthInfo();
			
			/*// KEEP
			trace("////////////////////////////////////");
			trace("PageArea > Add new frame = "+frame.frameVo.type); 
			for (var i:int = 0; i < pageVo.layoutVo.frameList.length; i++) 
			{
				trace("["+i+"] > "+ pageVo.layoutVo.frameList[i].type);
				trace("..["+i+"] frames > "+ frames[i]);
				trace("..["+i+"] displaylist > "+ frameContainer.getChildAt(i));
			}
			trace("////////////////////////////////////");
			*/
			
		}
		
		
		
		/**
		 * Add a text frame to the pageArea
		 */
		private function addTextFrame():void
		{
			//creating frameVo
			var frameVo:FrameVo = new FrameVo();
			frameVo.type = FrameVo.TYPE_TEXT;
			frameVo.x = pageCenter.x;
			frameVo.y = pageCenter.y;
			frameVo.width = 350;
			frameVo.height = 200;
			//frameVo.fontSize = MeasureManager.getFontSize(25);
			//frameVo.fontSize = MeasureManager.getFontSize(25);
			
			var fontSize : Number = 25;
			if(Infos.isAlbum){
				/*if( Infos.project.docPrefix == "S" ) fontSize = 18;
				else if( Infos.project.docPrefix == "M" ) fontSize = 36;
				else if( Infos.project.docPrefix == "L" ) fontSize = 40;*/
				fontSize = LayoutManager.instance.getFontSizeForAlbum();
			}
			frameVo.fontSize = fontSize;
			
			frameVo.editable = true;
			
			//Create Frame
			var frame:FrameText = new FrameText(frameVo);
			//listener
			frame.CREATED.add(frameCreated);
			//init
			frame.init();

			// add this frame to the page
			addNewFrameToPage(frame);
			
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
		}	
		
		/**
		 * Add a photo frame to the pageArea
		 */
		private function addPhotoFrame():void
		{
			//creating frameVo
			var frameVo:FrameVo = new FrameVo();
			frameVo.type = FrameVo.TYPE_PHOTO;
			frameVo.x = pageCenter.x;
			frameVo.y = pageCenter.y;
			frameVo.width = 350;
			frameVo.height = 200;
			frameVo.editable = true;
			
			//Create Frame
			var frame:FramePhoto = new FramePhoto(frameVo); 
			//listener
			frame.CREATED.add(frameCreated);
			//init
			frame.init();

			// add this frame to the page
			addNewFrameToPage(frame);
			
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
		}
		
		/**
		 * Add a photo frame to the pageArea
		 */
		private function addQRFrame(link:String):void
		{
			Debug.log("PageArea > addQRFrame");
			//creating frameVo
			var frameVo:QRVo = new QRVo();
			frameVo.QRLink = link;
			frameVo.type = FrameVo.TYPE_QR;
			frameVo.x = pageCenter.x;
			frameVo.y = pageCenter.y;
			frameVo.width = 200;
			frameVo.height = 200;
			frameVo.editable = true;
			
			//Create Frame
			var frame:FrameQR = new FrameQR(frameVo); 
			//listener
			frame.CREATED.add(frameCreated);
			//init
			frame.init();

			// add this frame to the page
			addNewFrameToPage(frame);
			
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
		}		
		
		
		/**
		 * Add text to page handler
		 * chek if the page is able to do it
		 */
		private function addTextToPageHandler(targetPageVo:PageVo):void
		{
			if(pageVo == targetPageVo)
				addTextFrame();
		}
		
		/**
		 * Add photo to page handler
		 * chek if the page is able to do it
		 */
		private function addPhotoToPageHandler(targetPageVo:PageVo):void
		{
			if(pageVo == targetPageVo)
				addPhotoFrame();
		}
		
		/**
		 * Add photo to page handler
		 * chek if the page is able to do it
		 */
		private function addQrToPageHandler(targetPageVo:PageVo, link:String):void
		{
			if(pageVo == targetPageVo && link != "")
				addQRFrame(link);
		}
		
		
		
		
		/**
		 * update frame depth infos
		 * > creates an array with all frames groups to allow changing depth
		 * > should be done on start and each frame modifications (DELETE, ADD)
		 */
		private function updateFramesDepthInfo():void
		{
			framesDepthInfo = new Array();
			var lastFrameLinkage : String;
			var tempFrame : Frame;
			
			var frameList : Array = VectorUtils.toArray(frames);
			for (var i:int = 0; i < frameList.length; i++) 
			{
				tempFrame = frameList[i] as Frame; 
				if(Debug.TRACE_PAGE_FRAMES ) trace("Frame "+i+" linkage = "+tempFrame.frameVo.uniqFrameLinkageID);
				
				if(lastFrameLinkage && tempFrame.frameVo.uniqFrameLinkageID == lastFrameLinkage) 
				{
					// great we have a group, we add the temp frame to last depth group
					framesDepthInfo[framesDepthInfo.length-1].push(tempFrame);
				}
				else 
				{
					// we have another linkage, so new group
					// first check if previous group has more than 1 frame, otherwise throw a warn for info
					if(lastFrameLinkage && framesDepthInfo[framesDepthInfo.length-1].length < 2) Debug.warn("PageArea > updateFramesDepthInfo (page "+pageVo.index+") > we didn't find at least 2 following frame with linkage : "+lastFrameLinkage+"");					
					framesDepthInfo.push([tempFrame]);
				}
				
				if(tempFrame.frameVo.uniqFrameLinkageID) lastFrameLinkage = tempFrame.frameVo.uniqFrameLinkageID;
				else lastFrameLinkage = null;
				
				// store info in frame so it's easier to update depth later
				tempFrame.frameDepth = framesDepthInfo.length-1;
			}
			
			
			// Trace page frames hierarchy
			if(Debug.TRACE_PAGE_FRAMES )
			{
				trace("////////////////////////////////////");
				trace("PageArea > updateFramesDepthInfo = num depth : "+framesDepthInfo.length); 
				for (i = 0; i < framesDepthInfo.length; i++) 
				{
					trace("["+i+"] > "+ framesDepthInfo[i].toString() );
				}
				trace("////////////////////////////////////");
			}
			
		}
		
		
		/**
		 * Create and Add a frame to the page based on a frameVo
		 * > Construct content based on saved infos
		 */
		private function createFrameFromFrameVo(frameVo:FrameVo):Frame
		{
			if(!frames)
			{
				Debug.warn("PageArea.createFrameFromFrameVo : no frames..");
				return null;
			}
			if(!frameVo)
			{
				Debug.warn("PageArea.createFrameFromFrameVo : no frameVO..");
				return null;
			}
			
			
			//remove spine from casual
			if(Infos.project.type == ProductsCatalogue.ALBUM_CASUAL && frameVo.type == FrameVo.TYPE_TEXT)
			{
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE || frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
				return null;
			}
			
			//creating frame
			var frame:Frame;
			
			// check frame type
			if(frameVo.type) // Checking the type could result in a null object as we're still using old XML from previous app version (containing frames with no type..)
			{
				
				switch(frameVo.type)
				{
					case FrameVo.TYPE_PHOTO:
						frame = new FramePhoto(frameVo); 
						break;
					
					case FrameVo.TYPE_TEXT:
						frame = new FrameText(frameVo); 
						break;
					
					case FrameVo.TYPE_BKG:
						frame = new FrameBackground(frameVo); 
						break;
					
					case FrameVo.TYPE_CLIPART:
						frame = new FramePhoto(frameVo); 
						break;
					
					case FrameVo.TYPE_CALENDAR:
						frame = new FrameCalendar(frameVo);
						break;
					
					case FrameVo.TYPE_OVERLAYER:
						frame = new FrameOverlayer(frameVo);
						break;
					
					case FrameVo.TYPE_QR:
						frame = new FrameQR(frameVo);
						break;
				}
				
				//Postcard Back => force special background Vo, so its always up to date
				if(frameVo.type == FrameVo.TYPE_BKG && pageVo.isPostCardBack)
					frameVo.isPostCardBackground = true;
				
				
				//listener
				frame.CREATED.add(frameCreated);
				//init
				frame.init();
				//Add frame
				frameContainer.addChild(frame);
				//Add frame to the list
				frames.push(frame);	
				
				// update depth
				updateFramesDepthInfo();
				
				return frame;
			}
			
			return null;
		}
		
		
		/**
		 * Frame has finished construct itself
		 * + add it as drop target if needed
		 * + save it
		 * + add signals
		 * + Calendars depth swap if linkage
		 */
		private function frameCreated(frame:Frame):void
		{
			//Add frame as a dropable area 
			if( frame.frameVo.type == FrameVo.TYPE_PHOTO) {
				DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_PHOTO, DragDropManager.TYPE_MENU_OVERLAYER,DragDropManager.TYPE_MENU_PHOTO]);
			}
			else if( frame.frameVo.type == FrameVo.TYPE_CLIPART) {
				DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_CLIPART]);
			}
			else if ( frame.frameVo.type == FrameVo.TYPE_CALENDAR && frame.frameVo.editable && frame.frameVo.dateAction == CalendarManager.DATEACTION_DAYDATE )
			{
				DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_PHOTO]);
			}
			else if( frame.frameVo.type == FrameVo.TYPE_OVERLAYER) {
				DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_PHOTO, DragDropManager.TYPE_MENU_OVERLAYER]);
			}
			else if( frame.frameVo.type == FrameVo.TYPE_BKG) {
				if(hasFixedLayout)
					DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_CLIPART]);
				else if(Infos.isCanvas && !CanvasManager.instance.isMultiple){
					DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_LAYOUT, DragDropManager.TYPE_MENU_CLIPART]);
				}
				else if(pageVo.isCalendar)
					DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_BKG, DragDropManager.TYPE_MENU_PHOTO, DragDropManager.TYPE_MENU_LAYOUT_CALENDAR, DragDropManager.TYPE_MENU_CLIPART]);
				else
					DragDropManager.instance.addDropTarget(frame, [DragDropManager.TYPE_MENU_BKG, DragDropManager.TYPE_MENU_PHOTO, DragDropManager.TYPE_MENU_LAYOUT, DragDropManager.TYPE_MENU_CLIPART]);
			}
			
			
			frame.save(); // first save of the frame properties
			frame.DELETE.add(deleteFrame); // delete a frame
			frame.CREATED.removeAll();//remove signal reference
			FRAME_CREATED.dispatch(frame);
			//frame.CLEAR.add(clearFrame); // when background frame has been cleared
			frame.DEPTH_UP.add(depthFrameUp);
			frame.DEPTH_DOWN.add(depthFrameDown);
			
			
			if( frame is FrameCalendar ){
				(frame as FrameCalendar).EDIT_TEXT.add(addOrEditFrameTextToCalendarFrame);
			}
			
			
		}
		
		
		/**
		 * set frame at depth +1
		 */
		private function depthFrameUp(frame:Frame):void
		{
			var frameDepth : int = frame.frameDepth;
			
			if( frameDepth == framesDepthInfo.length-1 ) return; // do nothing
			if(pageVo.layoutVo.frameList[frameDepth+1].dateAction == CalendarManager.DATEACTION_URL) return; // If next frame is URL tictac, do nothing.
			changeFrameDepth(frame, frameDepth+1);
		}
		/**
		 * set frame at depth -1
		 */
		private function depthFrameDown(frame:Frame):void
		{
			var frameDepth : int = frame.frameDepth;
			if( frameDepth == 1 ) return; // do nothing (as 0 is the background frame, we can't go below index 1
			changeFrameDepth(frame, frameDepth-1);
		}
		
		
		/**
		 * change frame depth
		 */
		private function changeFrameDepth( frame:Frame, newDepth:int ):void
		{
			Debug.log("PageArea > changeFrameDepth ("+framesDepthInfo.length+" level): from "+ frame.frameDepth+" to "+newDepth);
			var moveUp : Boolean = (newDepth > frame.frameDepth)? true : false;
			var movedFrames : Array = framesDepthInfo.splice(frame.frameDepth,1)[0];
			
			// reinject elements at correct place
			if(moveUp) framesDepthInfo.splice(newDepth, 0, movedFrames);
			else framesDepthInfo.splice(newDepth, 0, movedFrames);
			
			
			// update view and data
			var frameGroup : Array;
			var framesList : Vector.<Frame> = new Vector.<Frame>;
			var framesVoList : Vector.<FrameVo> = new Vector.<FrameVo>;
			for (var i:int = 0; i < framesDepthInfo.length; i++) 
			{
				frameGroup = framesDepthInfo[i];
				for (var j:int = 0; j < frameGroup.length; j++) 
				{
					frame = frameGroup[j];
					frame.frameDepth = i;
					frameContainer.addChild(frame); // update display list
					framesList.push(frame);			// update frames list
					framesVoList.push(frame.frameVo); // update frame vo list
				}
			}
			
			frames = framesList;
			pageVo.layoutVo.frameList = framesVoList;
			
			// TODO (IMPORTANT)! 
			// Add user action for frame depth... it's not done and can be problematic!
		}
		
		/**
		 * update frames position relative to framevo positions
		 * > this allows to make modification on frameVO list and then update frames positions
		 */
		/*
		private function updateFramesDepth():void
		{
			var frameList : Vector.<FrameVo> = pageVo.layoutVo.frameList;
			var frame : Frame;
			for (var i:int = 0; i < frameList.length; i++) 
			{
				frame = getFrameByFrameVo(frameList[i]);
				if(!frame) Debug.warn("PageArea > updateFramesDepth > a frame was not found..");
				else frameContainer.addChild(frame);
			}
		}
		*/
		
		
		
		/**
		 * delete a frame and it's reference in pageVo
		 * = user action
		 **/
		private function deleteFrame(frame:Frame, withAnim:Boolean = true):void
		{
			// quick delete animation
			if(withAnim){
				// make image clone
				var clone:Image = AssetUtils.getImageClone(frame, false, true); 
				clone.scaleX = clone.scaleY = EditionArea.editorScale;
				var newPos : Point = new Point( frame.x, frame.y );
				newPos = localToGlobal(newPos);
				clone.x = newPos.x;
				clone.y = newPos.y;
				clone.rotation = frame.rotation;
					
				// add it to stage
				stage.addChild(clone);
				
				// animate
				TweenMax.to(clone, .4, {scale:0, autoAlpha:0, ease:Strong.easeOut, tint:Colors.RED, onComplete:function():void{
					clone.destroy();
					clone.parent.removeChild(clone);
				}});
				
			}
			
			// when deleting a frame linked to an overlayer frame, we need to delete the overlayer too !
			if( frame.frameVo.uniqFrameLinkageID && frame.frameVo.type != FrameVo.TYPE_OVERLAYER ){
				// find overlayer frame 
				var framesLinked : Vector.<Frame> = PagesManager.instance.getFramesLinked(frame);
				if(framesLinked && framesLinked.length > 0 && framesLinked[0] is FrameOverlayer){
					//remove its vo
					pageVo.layoutVo.frameList.splice(pageVo.layoutVo.frameList.lastIndexOf(framesLinked[0].frameVo),1);
					
					// destroy it
					destroyFrame(framesLinked[0]);
				}
			}
			
			
			//remove its vo
			pageVo.layoutVo.frameList.splice(pageVo.layoutVo.frameList.lastIndexOf(frame.frameVo),1);
			
			// destroy it
			destroyFrame(frame);
			
			// udpdate frame depth info
			updateFramesDepthInfo();
			
			//Frame deleted signal
			FRAME_DELETED.dispatch(frame);
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
			// register delete frame
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_DELETE);
			// update used/not used items
			ProjectManager.instance.remapPhotoVoList();
		}
		
		
		/**
		 * clear a frame
		 * > redraw navigator
		 * > add user action
		 * > remap photo list
		 * > deselect from transform tool
		 */
		/*
		private function clearFrame(frame:Frame):void
		{
			// page vo updated
			PageNavigator.RedrawPage(pageVo);
			// register delete frame
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			// update used/not used items
			ProjectManager.instance.remapPhotoVoList();
			// deselect from transform tool
			frame.DESELECT.dispatch(frame);
		}
		*/
		
		
		/**
		 *
		 */
		/*
		private function replaceExistingFrame( frameToReplace : Frame, byFrame : Frame) : void
		{
			// add new frame at same index as other
			
			// remove old frame
			
			// destroy old frame
			
			// update index in frame
			
			// inject new frame in frame list
			
		}
		*/
		
		
		/**
		 * destroy frame
		 */
		private function destroyFrame(frame:Frame):void
		{
			//remove it from display list
			if(frameContainer.contains(frame))
				frameContainer.removeChild(frame);
			
			//remove reference of the view
			frames.splice(frames.lastIndexOf(frame),1);
			
			//Remove frame as a dropable area
			DragDropManager.instance.removeDropTarget(frame);
			
			// destroy frame
			frame.destroy();
		}
		
		/*
		private function frameDestroyed(frame:Frame, andDelete:Boolean):void
		{
			//remove it from display list
			if(contains(frame))
				removeChild(frame);
			
			//remove reference of the view
			frames.splice(frames.lastIndexOf(frame),1);
			
			//Remove frame as a dropable area
			DragDropManager.instance.removeDropTarget(frame);
			
			//remove its vo if andDelete
			if(andDelete)
			pageVo.layoutVo.frameList.splice(pageVo.layoutVo.frameList.lastIndexOf(frame.frameVo),1);
		}
		*/
		
		
	}
}