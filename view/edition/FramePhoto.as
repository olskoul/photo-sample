package view.edition
{
	import com.adobe.serialization.json.JSONEncoder;
	import com.bit101.components.ProgressBar;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.LineScaleMode;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.MathUtils2;
	import be.antho.utils.TintMovieClip;
	
	import comp.DefaultTooltip;
	import comp.button.SimpleButton;
	
	import data.BackgroundVo;
	import data.ClipartVo;
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	import data.TooltipVo;
	
	import library.addPhoto.icon.bmd;
	
	import manager.CanvasManager;
	import manager.ConnectionManager;
	import manager.FolderManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.SessionManager;
	
	import mcs.photoWarningIcon;
	
	CONFIG::offline
		{
			import offline.manager.OfflineAssetsManager;

		}
	import offline.manager.LocalStorageManager;
	
	import ordering.FrameExporter;
	
	import utils.Colors;
	import utils.CursorManager;
	import utils.Debug;
	
	import view.edition.pageNavigator.PageNavigator;
	import view.uploadManager.PhotoUploadItem;
	import data.AssetVo;
	import view.uploadManager.PhotoUploadManager;
	import offline.data.OfflinePhotoLoader;
	import utils.BitmapUtils;
	import data.OverlayerVo;
	import flash.system.LoaderContext;
	import flash.system.ImageDecodingPolicy;
	
	public class FramePhoto extends Frame
	{
		/* 
		Hierarchy is : this > content > image > workingBitmap
			> this : rotation, x, y 
			> content : crop (scrollrect)
			> image : scaled to zoom
			> workingBitmap : bitmap of resolution low,medium,high > scaled to fullsize photo width
		*/
		
		// view
		protected var image:Sprite; // is the image we are working with, it contains the working bitmap redrawn depending on the quality needed
		protected var workingBitmap:Bitmap; // is the bitmap of the photo we are working on, this is redrawn depending on the quality needed
		protected var movePreviewBitmap:Bitmap; // is the preview of the full bitmap when moving a cropped area
		protected var loadingBar : ProgressBar;
		
		// inside move values
		protected var startMousePoint : Point; // point of reference of the mouse when the grab occur
		protected var startImageBounds : Rectangle; // rectanlge bounds of reference of the image when the grab occur
		protected var startCropValue : int; // initial crop value
				
		// zoom values
		protected var zoomStartScale:Number ; // initial scale value when starting zoom, reference
		protected var zoomStartPos:Point ; // initial position value when starting zoom, reference
		
		// the content , here its a photo (Are we going to use a frame for text as well??)
		protected var basePhoto : Bitmap;		// original bitmap asset (in full size) stored in memory
		protected var isThumbPreview : Boolean ; // when we load a frame, we first load thumb url to speed up display, we then load full thumb in background
		protected var photoLoader : Loader; 	// loader of image
		protected var workResolution: String ; //  is current image resolution for best performance in editor (low, medium, high)
		protected var qualityIndicator : MovieClip;
		protected var qualityIndicatorTooltip : TooltipVo;
		
		protected var masker:Shape ;
		
		// border and shadow
		protected var borderShape : Shape;
		protected var shadowSprite : Bitmap;
		protected var forceShadowUpdate : Boolean;
		
		//Bitmapdata call to action for photo
		private static var ctaPhotoIconBmd:BitmapData; //-> we use a static variable so it's created only once for all empty frames of the application
		//save current trasform tool flags
		private var transformToolAllowance:Object;
		
		// local load (temp image)
		private var localUploadItem : PhotoUploadItem;
		private var errorBox : ErrorBox;
		
		protected var minSize:Number = 30;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRuCTOR
		////////////////////////////////////////////////////////////////

		
		
		public function FramePhoto(frameVo:FrameVo)
		{
			super(frameVo);
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Overide the label for the cta btn
		 */
		override public function get labelCta():String
		{
			return "edition.frame.photo.cta";
		}
		
		/**
		 * Overide the icon bitmapdata: Return the icon for photo
		 */
		override public function get iconbmd():BitmapData
		{
			if(!ctaPhotoIconBmd) ctaPhotoIconBmd = new library.addPhoto.icon.bmd();
			return ctaPhotoIconBmd;
		}
		
		/**
		 * is the current frame content cropped
		 */
		public function get isCropped() : Boolean
		{
			return (content.mask != null);
		}
		
		/**
		 * Override isSticked
		 */
		override public function set isSticked(value:Boolean):void
		{
			_isSticked = value;
			if(value)
			{
				rollOut(true);
				if(allowMove) CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
		}
		
		override public function get dropBounds():Rectangle
		{
			if(!isEmpty && masker) return masker.getRect(this);
			else return this.getRect(this);			
		}
		
		
		/**
		 * Overriden funciton by each extends of this class
		 * Editor scale has resized, let update what needs to be updated (icon)
		 */
		override protected function editorResizeHandler(scaleFactor:Number):void
		{
			super.editorResizeHandler(scaleFactor);
			if(hasError) updateErrorBox(scaleFactor);
		}	
		
		
		////////////////////////////////////////////////////////////////
		//	INIT/UPDATE POSSIBILITIES
		////////////////////////////////////////////////////////////////
		
		/**
		 * 1. simple init frame
		 * After creation in page area
		 */
		override public function init():void
		{
			// update state depending on photo vo
			updateState();
			
			renderFrame();
			
			if(frameVo.isEmpty) {
				saveTransformToolAllowance();
				frameReady();
			}
			else loadPhoto();
			
			setListeners();
		}
		
		/**
		 * Save transform tool allow move, allow rotate etc...
		 * until the frame is not empty anymore, then set it back.
		 * */
		private function saveTransformToolAllowance():void
		{
		/*	transformToolAllowance = {};
			
			transformToolAllowance.allowBorderMove = frameVo.allowBorderMove;
			transformToolAllowance.allowCrop = frameVo.allowCrop;
			transformToolAllowance.allowInsideMove = frameVo.allowInsideMove;
			transformToolAllowance.allowResize = frameVo.allowResize;
			transformToolAllowance.allowRotation = frameVo.allowRotation;
			transformToolAllowance.allowMove = frameVo.allowMove;
			
			//set it all to false
			frameVo.allowBorderMove = true;
			frameVo.allowCrop = false;
			frameVo.allowInsideMove = false;
			frameVo.allowResize = false;
			frameVo.allowRotation = false;
			//frameVo.allowToolBar = true;
			//frameVo.allowMove = true;
			
			*/
		}
		
		/**
		 * Reset transform tool allow move, allow rotate etc...
		 * */
		private function resetTransformToolAllowance():void
		{
			/*if(transformToolAllowance)
			{
				frameVo.allowBorderMove = transformToolAllowance.allowBorderMove;
				frameVo.allowCrop = transformToolAllowance.allowCrop;
				frameVo.allowInsideMove = transformToolAllowance.allowInsideMove;
				frameVo.allowResize = transformToolAllowance.allowResize;
				frameVo.allowRotation = transformToolAllowance.allowRotation;
				//frameVo.allowMove = transformToolAllowance.allowMove;
				//frameVo.allowToolBar = true;
			}*/
		}
		
		/**
		 * 2. Update frame with a new PhotoVo
		 * + if no photovo, just render the frame to be sure view is updated
		 * When updating this existing frame with new framevo (mainly following a drag/drop)
		 */
		public function update(photoVo:PhotoVo, keepPhotoRatio:Boolean = false):void
		{
			// if we update a frame photo with a new photoVO but this frame has error, we need to redraw it
			if( photoVo && hasError )
			{
				// make photo fit in frame
				FrameVo.injectPhotoVoInFrameVo(frameVo, photoVo, keepPhotoRatio);
				EditionArea.Refresh();
				return;
			}
			
			
			// if photovo > update and load new photo
			if(photoVo)
			{
				frameVo.photoVo = photoVo;
				
				updateState();
				
				// make photo fit in frame
				FrameVo.injectPhotoVoInFrameVo(frameVo, photoVo, keepPhotoRatio);
				
				// load new photo
				loadPhoto();
			}
			// else it can be only a data update, then just render frame
			else
			{
				renderFrame();
			}
		}
		
		/**
		 * 2. Update frame with a new FrameVo
		 */
		public function updateWithCustom(newVoString:String):void
		{
			frameVo.fill(JSON.parse(newVoString));
		}
		
		
		/**
		 * refresh image, try to load photovo again and keep framevo properties
		 */
		public function refresh():void
		{
			Debug.log("FramePhoto.refresh");
			if(frameVo && frameVo.photoVo)
			{
				loadPhoto();
			}
		}
		
		
		
		/**
		 * 3. TEmp bitmap frame
		 * After freshly uploaded files 
		 */
		/* SHOULD NOT BE DONE ANYMORE
		public function createTempFrame( tempBd:BitmapData ):void
		{
			updateState();
			
			// adapt photovo to temp bitmap info
			var photoVo : PhotoVo = frameVo.photoVo;
			photoVo.width = tempBd.width; 	// new width 
			photoVo.height = tempBd.height; // new height
			
			// update framevo
			FrameVo.injectPhotoVoInFrameVo(frameVo, photoVo);
			
			// change base photo 
			basePhoto = new Bitmap(tempBd, "auto", true);
			
			workResolution = null ; // force generation
			checkAndUpdateImageQuality();
			
			renderFrame();
			
			//reset transform tool authorisation
			if(transformToolAllowance)
				resetTransformToolAllowance();
			
			frameReady();
		}
		*/
		
		
		/**
		 * 4. TEmp bitmap after image edition
		 * Switch with edited new photo infos
		 * > from imageEditionModule completed
		 * > find vo corresponding to new photo name
		 * > replace content of frame and vo link
		 */
		public function switchToModifiedPhotoVo(newName : String, newData:BitmapData):void
		{
			var newPhotoVo:PhotoVo = SessionManager.instance.getPhotoVoByName(newName);
			basePhoto.bitmapData.dispose();
			basePhoto.bitmapData = newData; // TODO : check if we need to dispose something or not
			
			if(!newPhotoVo){
				Debug.warn("FramePhoto > switch to modified photo > no matching photovo found");
				return;
			}
			frameVo.photoVo = newPhotoVo;
			
			
			
			generatePhotoWithResolution(workResolution);
			
			// 
			PageNavigator.RedrawPageByFrame(frameVo);
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Completely destroy frame from memory (view)
		 * > function is called when pageArea is removed from ui
		 * > clear possible memory items
		 */
		override public function destroy() : void
		{
			dispose();
			super.destroy();
		}
			
		

	
	
		
		/**
		 * clear the frame and reset it to CTA state
		 */
		override public function clearMe():void
		{
			// kill loader
			killPhotoLoader();
			
			// remove photoVO
			if(frameVo)
			{
				frameVo.photoVo = null;
				// kill possible border and shadow
				frameVo.border = 0;
				frameVo.shadow = false;
				// remove possible popart effect
				frameVo.isPopart = false;
			} 
			else
			{
				imageError("FramePhoto.clearMe > no framevo");
			}
			
			if(borderShape){
				if(borderShape.parent) borderShape.parent.removeChild(borderShape);
				borderShape = null;
			}
			if(shadowSprite) clearShadow();
			
			if(loadingBar && loadingBar.parent) loadingBar.parent.removeChild(loadingBar);
			
			
			// remove actions
			//saveTransformToolAllowance();
			
			// dispose base photo
			clearBasePhoto();
			
			// clear working bitmap
			clearWorkingImage();
			
			
			// update state to reset content
			try
			{
				updateState();
			}
			catch(e:Error) 
			{
				var frameVoStringify:String = "No frame vo";
				if(frameVo)
				{
					frameVoStringify = new JSONEncoder(frameVo).getString();
					var r:RegExp = /,/g;
					var s:String = "\n";
					frameVoStringify = frameVoStringify.replace(r, s);
				}
				

				Debug.warn("FRAME VO Stringify: "+frameVoStringify);
				Debug.warn("FramePhoto > clearMe > try updateState failed " + e.message );
				
				//PopupAbstract.Alert("popup.framephoto.error.title","popup.framephoto.error.desc",false,null,null,true);
			}
			
			// remove quality indicator
			if(qualityIndicator && qualityIndicator.parent) qualityIndicator.parent.removeChild(qualityIndicator);
			
			super.clearMe();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * switch to correct state
		 * which can be : 
		 * 	1. empty -> no photo > we remove content sprite
		 *  2. with photo -> we remove cta and add content
		 *  3. with error -> we draw an error box
		 */
		protected function updateState():void
		{	
			//
			// image has an error
			//
			if( hasError ) 
			{
				if( frameVo )Debug.warn( "FramePhoto.updateState with error : frameVo is "+ frameVo.toString() );
				else Debug.warn( "FramePhoto.updateState with error : frameVo is null");
				
				cleanFrameForError(); // clean frame content
				if(!frameVo) {
					deleteMe(false);
					return;
				}
				
				// check frameVo minimum requirement to display...
				if(!frameVo.width || isNaN(frameVo.width)) frameVo.width = 250;
				if(!frameVo.height || isNaN(frameVo.height)) frameVo.height = 250
				if(!frameVo.x || isNaN(frameVo.x)) frameVo.x = 200;
				if(!frameVo.y || isNaN(frameVo.y)) frameVo.y = 200;
				
				drawErrorBox(); // draw error box
			}
			
			
			//
			// IS NOT EMPTY
			//
			else if( !frameVo.isEmpty ) 
			{
				// create image mask
				drawFrameMask();
				
				// create content of not already done
				if(!content){
					content = new Sprite();
					image = new Sprite();
					workingBitmap = new Bitmap();
					image.addChild(workingBitmap);
					content.addChild(image);
				}
				
				// create loading bar
				if(!loadingBar){
					loadingBar = new ProgressBar(null, 0,0);
					loadingBar.x = -(loadingBar.width)*.5;
					loadingBar.y = -(loadingBar.height)*.5;
				}
				
				// add content
				addChild(content);
				
				// create quality indicator
				if(!qualityIndicator) createQualityIndicator();
				
				
				// remove cta if still there
				if(cta && cta.parent) cta.parent.removeChild(cta);
			} 
			
			//
			// IS EMPTY
			//
			else  
			{				
				//create the call to action
				if(!cta) createCTA(.1, Colors.GREY);
				else if(!cta.parent) addChild(cta);
				updateCTA(EditionArea.editorScale);
				
				if(content && content.parent) removeChild(content);
				if(masker && masker.parent) masker.parent.removeChild(masker);
			}
		}
		
		
		/**
		 * public method to change the mask of a framePhoto
		 * > change mask type in framevo
		 * > draw frame mask forced
		 * > renderFrame
		 */
		public function applyMask( maskType : int, cornerRadius:Number ):void
		{
			if(!frameVo) Debug.warn("FramePhoto.applyMask : No frameVo...");
			else
			{
				frameVo.mask = maskType;
				if(!isNaN(cornerRadius)) frameVo.cr = cornerRadius;
				//if(maskType == FrameVo.MASK_TYPE_CIRCLE && frameVo.shadow) frameVo.shadow = false;
				drawFrameMask( true );
				drawFrameBorder( true );
				forceShadowUpdate = true;
				renderFrame();
				PageNavigator.RedrawPageByFrame(frameVo);
			}
		}
		
		
		
		/**
		 * draw the frame mask
		 */
		protected function drawFrameMask(forceUpdate:Boolean = false):void
		{
			// create mask if not exists
			if(!masker){
				masker = new Shape();
				forceUpdate = true;
			}
			
			// if type is corner radius, we need tu update mask
			if(frameVo && frameVo.mask == FrameVo.MASK_TYPE_CORNER_RADIUS) forceUpdate = true;
			
			
			// redraw if needed
			if(forceUpdate)
			{
				masker.graphics.clear(); 
				masker.graphics.beginFill(0xff0000, 1);
				if(frameVo && frameVo.mask != FrameVo.MASK_TYPE_NONE)
				{
					if (frameVo.mask == FrameVo.MASK_TYPE_CIRCLE) masker.graphics.drawCircle(0,0,50);
					else if (frameVo.mask == FrameVo.MASK_TYPE_CORNER_RADIUS)
					{
						var w : Number = frameVo.width - frameOffset*2 ;
						var h : Number = frameVo.height - frameOffset*2 ; 
						var mmCr : Number = MeasureManager.millimeterToPixel(frameVo.cr);
						masker.graphics.drawRoundRect(-w/2,-h/2,w,h,mmCr, mmCr);
					}
				}
				else masker.graphics.drawRect(-50,-50,100,100);
				masker.graphics.endFill();
			}
		}
		
		
		
		/**
		 * draw the frame border
		 */
		private function drawFrameBorder(forceUpdate:Boolean = false):void
		{
			if( frameVo.border > 0 ){
				
				// be sure border color is set otherwise put white !
				if(isNaN(frameVo.fillColor)) frameVo.fillColor = Colors.WHITE;
				
				// create border if not existing
				if(!borderShape){
					borderShape = new Shape();
					forceUpdate = true;
				}
				
				// if type is corner radius, we need tu update border
				if(frameVo && frameVo.mask == FrameVo.MASK_TYPE_CORNER_RADIUS) forceUpdate = true;
					
				// redraw if needed
				if(forceUpdate){
					borderShape.graphics.clear();
					borderShape.graphics.beginFill(frameVo.fillColor, 1);
					if(frameVo && frameVo.mask != FrameVo.MASK_TYPE_NONE)
					{
						if (frameVo.mask == FrameVo.MASK_TYPE_CIRCLE) borderShape.graphics.drawCircle(0,0,50);
						else if (frameVo.mask == FrameVo.MASK_TYPE_CORNER_RADIUS)
						{
							var w : Number = frameVo.width ;
							var h : Number = frameVo.height ; 
							var mmCr : Number = MeasureManager.millimeterToPixel(frameVo.cr);
							borderShape.graphics.drawRoundRect(-w/2,-h/2,w,h,mmCr, mmCr);
						}
					}
					else borderShape.graphics.drawRect(-10,-10,20,20);
					borderShape.graphics.endFill();
				}
				
				// positionate
				var borderWidth:Number = frameVo.width; 
				var borderHeight:Number = frameVo.height;
				borderShape.width = borderWidth;
				borderShape.height = borderHeight;
				addChildAt(borderShape,0);
			} 
			else 
			{
				if(borderShape && borderShape.parent) borderShape.parent.removeChild(borderShape);
			}
		}
		
		
		/**
		 * clean frame children to have nothing in the view 
		 * This doesnt destroy elements but just remove it from display list
		 */
		private function cleanFrameForError() : void
		{
			//if(content && content.parent) removeChild(content); // remove content
			//if(masker && masker.parent) masker.parent.removeChild(masker); // remove mask
			if(cta && cta.parent) cta.parent.removeChild(cta);// remove cta
			if(loadingBar && loadingBar.parent) loadingBar.parent.removeChild( loadingBar );// remove loading
			if(qualityIndicator && qualityIndicator.parent ) qualityIndicator.parent.removeChild(qualityIndicator); // remove quality indicator
			if(errorBox && errorBox.parent) errorBox.parent.removeChild(errorBox) // remove possible errorbox
		}
		
		
		
		/**
		 * When frame has a loading error or other unknow error, we put the frame in the error state
		 * and we draw an visual error container so the user knows there is some issue with this frame
		 */
		private function drawErrorBox() : void
		{
			if(!errorBox) {
				errorBox = new ErrorBox( frameVo );
				errorBox.REMOVE_CLICKED.add( onErrorRemoveFrame );
				errorBox.REIMPORT_CLICKED.add( onErrorReimportFrame );
				errorBox.RETRY_CLICKED.add( onErrorRetryFrame );
			}
			
			// be sure to remove loading
			loadingBar.visible = false;
			
			//addChild to frame
			addChild(errorBox);
			updateErrorBox(EditionArea.editorScale);
		}
		/**
		 * UPDATE ERROR BOX
		 * like updateCTA, it changes depending the scale factor of the edition area
		 */
		protected function updateErrorBox(scaleFactor:Number):void
		{
			if(errorBox) errorBox.updateContent(scaleFactor, frameVo);
		}
		/**
		 * Retry to load frame (page)
		 */
		private function onErrorRetryFrame():void
		{
			EditionArea.Refresh();
		}
		/**
		 * Remove frame
		 */
		private function onErrorRemoveFrame():void
		{
			if(frameVo) frameVo.reset();
			EditionArea.Refresh();
		}
		/**
		 * reimport frame
		 */
		private function onErrorReimportFrame():void
		{
			// try to reimport in same folder as before.. if old folder was temp, add this to current project folder
			CONFIG::online
			{
				var folder : String = frameVo.photoVo.folderName;
				if(folder == FolderManager.instance.TempFolderName) folder = FolderManager.instance.currentProjectFolderName;
				PhotoUploadManager.instance.browse(frameVo,folder);
			}
			
			// OFFLINE IMPORT
			CONFIG::offline
			{
				Infos.lastUserBrowsePath = frameVo.photoVo.originUrl;
				OfflinePhotoLoader.BrowseForFramePhoto(frameVo);
			}
		}
		
		
		
		
		
		
		
		
		/**
		 * frame offset is the amount of pixel between the frame bounds and the image bounds
		 * if no border nor shadow the frame offset is always 0
		 * but if there is a border or a shadow, the photo is not exactly the size of the frame, the difference is the offset (size of the border or shadow)
		 */
		private function get frameOffset():Number
		{
			if(frameVo) return MeasureManager.millimeterToPixel(frameVo.border);
			return 0;
		}
		
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		override protected function renderFrame():void
		{
			// security
			if(hasError) return;
			
			
			if(!frameVo.isEmpty)
			{
				content.scaleX = content.scaleY = 1; // security but it should always be 1 so maybe not mandatory
				
				// image is scaled to the frame vo zoom
				image.scaleX = image.scaleY = frameVo.zoom;
					
				// draw frame mask
				drawFrameMask();
				
				// Crop values and mask
				if( image.width != frameVo.width || image.height != frameVo.height || frameOffset != 0){ 
					
					// setup the mask (which as the same size as the frame vo)
					masker.width = frameVo.width - frameOffset*2 ;
					masker.height = frameVo.height - frameOffset*2 ;
					content.mask = masker;
					if(!masker.parent) addChild(masker);
					
					image.x = (image.width-frameVo.width)*.5 - frameVo.cLeft;
					image.y = (image.height-frameVo.height)*.5 - frameVo.cTop;
				}
				else 
				{
					content.mask = null;
					if(masker.parent) masker.parent.removeChild(masker);
					
					image.x = (image.width-frameVo.width)*.5 ;
					image.y = (image.height-frameVo.height)*.5 ;
				}
			}
			else
			{
				if(cta && contains(cta))
				{
					updateCTA(EditionArea.editorScale);
				}
			}
			
			// apply position after center content
			x = frameVo.x;
			y = frameVo.y;
			rotation = frameVo.rotation;
			
			/**
			* APPLY BORDER
			*/
			drawFrameBorder();
			
			/**
			 * APPLY SHADOW
			 */
			if(frameVo.shadow){
				
				if(forceShadowUpdate) clearShadow();
				if(!shadowSprite){
					shadowSprite = new Bitmap( FrameExporter.generateFrameShadow( frameVo.mask, false ), "auto", true );
				}
				
				shadowSprite.x = -frameVo.width*.5;
				shadowSprite.y = -frameVo.height*.5;
				shadowSprite.width = frameVo.width + frameVo.getShadowSize();//*FrameVo.SHADOW_SCALE
				shadowSprite.height = frameVo.height + frameVo.getShadowSize();//*FrameVo.SHADOW_SCALE;
				addChildAt(shadowSprite,0);
				
				TintMovieClip.tint(shadowSprite, frameVo.sCol, 1);
			}
			else
			{
				if(shadowSprite && shadowSprite.parent) shadowSprite.parent.removeChild(shadowSprite);
			}
			
			// quality indicator
			// updateQualityIndicator(); // this is now done only on specific changes (resize and inside zoom)
		}
		
		
		/**
		 * load a photo
		 */
		protected function loadPhoto():void
		{
			// kill and dispose photoloader
			killPhotoLoader();
			
			// check photo url
			var photoUrl:String;
			if(!frameVo.photoVo.id) 
			{	
				// the photo must be a TEMP photo (from upload) and still has no id
				// we will try to find the id right now, otherwise we must stop and warn
				var refVo : PhotoVo = SessionManager.instance.getPhotoVoByName(frameVo.photoVo.name);
				if(refVo) frameVo.photoVo = refVo;
				else {
					imageError("FramePhoto.loadPhoto > missing photoVo id");
					return;
				}
			}
			
			// TEMP Photo handling
			if(frameVo.photoVo.temp)
			{
				displayLoading();
				loadLocalVersion();
			}
			// NORMAL PHOTO HANDLING
			else
			{

				if(!isThumbPreview){
					photoUrl = frameVo.photoVo.thumbUrl;
					isThumbPreview = true;
				}
				else 
				{
					photoUrl = frameVo.photoVo.normalUrl;
					isThumbPreview = false;
				}

				
				
				// Online photo load
				//if(!frameVo.IS_LOCAL_CONTENT) 
				//{
					// TODO (OPTIMIZE) : replace this loader by bulkloader to allow multiple retry !!!
					photoLoader = new Loader();
					photoLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onPhotoLoaded);
					photoLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onPhotoLoadProgress);
					photoLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onPhotoLoadError);
					
					var loaderContext:LoaderContext = new LoaderContext(); 
					//loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD; // TODO : find option here, this is causing memory leak when rapid page change
					
					photoLoader.load(new URLRequest(photoUrl),loaderContext);
				//}
				// DESKTOP Local photo load
				//else
				//{
				//	LocalStorageManager.instance.loadLocalImage( photoUrl, onLocalImageLoaded, onLocalLoadError );
				//}
				
				
				// show loading bar (only after at least 20millisecond of download, otherwise we do not dipsplay it)
				TweenMax.delayedCall(.2, displayLoading); 
			}
		}
		private function displayLoading():void
		{
			addChild(loadingBar);
		}
		private function onPhotoLoadProgress(e:ProgressEvent):void
		{
			loadingBar.value = e.bytesLoaded/e.bytesTotal;
		}
		private function onPhotoLoaded(e:Event):void
		{
			try{
				workingBitmapLoaded( ((e.currentTarget as LoaderInfo).content as Bitmap).bitmapData );
			}
			catch (err:Error) {
				imageError("FramePhoto.onPhotoLoaded : " + err.toString());
			}
		}
		
		/*
		private function onLocalImageLoaded( bmd : BitmapData ):void
		{
			try{
				if(frameVo && frameVo.photoVo) workingBitmapLoaded( bmd ); // be sure to do this only if the frame still exists
			}
			catch (err:Error) {
			 	imageError("FramePhoto.onLocalImageLoaded : " + err.toString());
			}
		}
		private function onLocalLoadError( e:ErrorEvent ):void
		{
			imageError( e.toString() );	
		}
		*/
		
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		// @ BEGIN LOCAL LOAD FOR A PHOTO ITEM
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Load local version of file for temp photovo
		 */
		private function loadLocalVersion():void
		{
			Debug.log("FramePhoto.loadLocalVersion");
			cleanLocalUploadItemRef();
			localUploadItem = PhotoUploadManager.instance.getPhotoUploadItemByPhotoId(frameVo.photoVo.id);
			if(localUploadItem)
			{	
				localUploadItem.TEMP_WORKING_IMAGE_ERROR.addOnce(localImageLoadError);
				localUploadItem.TEMP_WORKING_IMAGE_LOADED.addOnce(localImageLoadSuccess);
				localUploadItem.LoadTempWorkingImage();
			}
			else
			{
				var photoID:String = "";
				if(frameVo && frameVo.photoVo)
					photoID = frameVo.photoVo.id;
				
				Debug.log("FramePhoto.loadLocalVersion > Failed > localUploadItem is null with id: "+photoID);
				cleanLocalUploadItemRef();
				clearMe();
				renderFrame();
			}
		}
		private function localImageLoadError( msg : String ):void
		{
			cleanLocalUploadItemRef();
			imageError("FramePhoto.localImageLoadError : " + msg);
		}
		private function localImageLoadSuccess():void
		{
			Debug.log("FramePhoto.localImageLoadSuccess");
			// securities
			try{
				workingBitmapLoaded( localUploadItem.getWorkingBitmapDataClone() );	
			}
			catch(e:Error){
				imageError(" > local load Error : "+e.toString());
			}
			
			cleanLocalUploadItemRef();
		}
		private function cleanLocalUploadItemRef():void
		{
			if(localUploadItem)
			{
				localUploadItem.TEMP_WORKING_IMAGE_ERROR.remove( localImageLoadError);
				localUploadItem.TEMP_WORKING_IMAGE_LOADED.remove( localImageLoadSuccess);
				localUploadItem = null;
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////
		// @ END LOCAL LOAD FOR A PHOTO ITEM
		////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		/**
		 * when bitmap is loaded from server or from local (temp image) we retrieve bitmapdate and the pass it to this function
		 * > it handles the bitmapdate to be used as base photo
		 */
		private function workingBitmapLoaded( bmd : BitmapData ) :void
		{
			// security
			if(!frameVo){
				imageError("FramePhoto.workingBitmapLoaded : no Framevo > we do not continue");
				return;
			}
			if(!bmd){
				imageError("FramePhoto.workingBitmapLoaded : no bitmapdata > we do not continue");
				return;
			}
			
			// clean possible base photo
			clearBasePhoto();
			
			// create base photo
			basePhoto = new Bitmap( bmd, "auto", true);
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// HACK : CHECK if photo size has the same ratio as it should be // TODO : ask keith for best option with background ratio issue
			// > this is a bad hack but as we do not receive correct data from background xml (image ratio), let's keep it like this
			if(frameVo.photoVo && frameVo.photoVo is BackgroundVo)
			{
				// verify
				PagesManager.instance.verifyBackgroundVoRatio( basePhoto.bitmapData, frameVo.photoVo as BackgroundVo )
				// reinject photovo in frame
				FrameVo.injectPhotoVoInFrameVo(frameVo, frameVo.photoVo);
			}
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			killPhotoLoader();
			
			workResolution = null; 	// reset resolution info to force generation
			checkAndUpdateImageQuality(); 
			
			//reset transform tool authorisation
			//if(transformToolAllowance)
				//resetTransformToolAllowance();
			
			// render the frame
			renderFrame();
			
			// remove loader
			if(loadingBar.parent) loadingBar.parent.removeChild(loadingBar);
			
			// just to test perfs
			/*
			var obj : Object = BulkLoader._allLoaders; // TODO : check to see if temp loaders are correctly removed from memory
			trace(obj.toString());
			*/
			
			// tell the world we are ready
			frameReady();
			
			// now we can load working version
			if(isThumbPreview){
				// now that thumb is loaded, we show error if photovo is flaged with error
				if(frameVo && frameVo.photoVo && frameVo.photoVo.error)
				{
					imageError("PhotoVo has error flag");
				}
				else loadPhoto();
			}
		}
		
		
		
		
		/**
		 *	main image error handler
		 *  > if something goes wrong here we make the minimum clear to avoid crash and memory leaks
		 *  > we should try to never go inside this function
		 */
		private function imageError(msg : String):void
		{
			Debug.warn("FramePhoto.imageError : " + msg);
			_hasError = true;
			
			
			// dispose image to be sure
			//dispose();
			killPhotoLoader();
			cleanLocalUploadItemRef();
			
			
			// if no frame vo, this frame is kind of lost, we need to remove it or at least avoid the possibility of the user to edit it
			if(!frameVo)
			{
				//EditionArea.Refresh(); // BUG : check this, could have been deleted in between and this would lead to loop refresh...
				return;
			}
			
			// if there is a framevo but not photovo, we should clear the frame (reinitialize it) and then reflesh to avoid problems
			else if(!frameVo.photoVo)
			{
				frameVo.reset(); // reset the frame vo
				EditionArea.Refresh();
			}
			
			// if there is a framevo and a photovo, we must display an error on the frame so the user try to delete it.
			else 
			{
				
				// TODO (OPTIMIZE) : in the best of the worlds, we should here display something wrong on the frame and allow user to choose to try again or to remove frame, but for now, we just reset it.
				// No choice you fools !
				updateState();
				
				// dispatch the ready event so we can still drag&drop photos to update this frame
				frameReady();
			}
			
			// check connection so we display a warning if connection issue
			ConnectionManager.instance.checkConnection();
		}
		
		/**
		 * dispose possible memory items
		 */
		private function dispose():void
		{
			localUploadItem = null; // remove reference to local upload item
			killPhotoLoader();
			clearBasePhoto();
			clearShadow();
			clearWorkingImage();
			cleanLocalUploadItemRef();
		}
		
		
		/**
		 * clear possible shadow 
		 */
		private function clearShadow():void
		{
			if(shadowSprite)
			{
				if(shadowSprite.parent) shadowSprite.parent.removeChild(shadowSprite);
				shadowSprite.bitmapData = null; // remove ref
				shadowSprite = null;
			}
		}
		
		
		
		
		
				
		/**
		 * update crop values for new image x and y
		 * > positionning is done via image.x and image.y 
		 * > positionning can be done via crop values also
		 * > here is the conversion 
		 */
		private function updateCropValues(newImageX : Number, newImageY:Number ):void
		{
			// update crop values in frame vo based on new image position
			frameVo.cLeft = (image.width-frameVo.width)*.5 - newImageX;
			frameVo.cTop = (image.height-frameVo.height)*.5 - newImageY;
			
			//Debug.log("New Crop values : CLeft = "+ frameVo.cLeft +" & CTop = "+frameVo.cTop);
		}
		
		/**
		 * clear possible existing base photo
		 */
		protected function clearBasePhoto():void
		{
			if( basePhoto ) {
				if(basePhoto.parent) basePhoto.parent.removeChild(basePhoto); // not sure it is needed as base photo should never be added to display list
				if(basePhoto.bitmapData) basePhoto.bitmapData.dispose();
				basePhoto.bitmapData = null;
				basePhoto = null;
			}
		}
		
		
		/**
		 * clear working image
		 */
		protected function clearWorkingImage():void
		{
			if(image){
				if(workingBitmap && workingBitmap.bitmapData){
					workingBitmap.bitmapData.dispose();
					workingBitmap.bitmapData = null;	
				}
				//if(image.parent) image.parent.removeChild(image);
			}
		}
		
		
		
		/**
		 * ------------------------------------ IMPORT IN FRAME -------------------------------------
		 */
		
		
		/**
		 * Overriden funciton by each extends of this class
		 * this what the frame should do when the CTA is clicked
		 */
		override protected function ctaClickHandler(e:MouseEvent = null):void
		{
			// ONLINE IMPORT
			CONFIG::online
			{
				PhotoUploadManager.instance.browse(frameVo);
			}
			
			// OFFLINE IMPORT
			CONFIG::offline
			{
				OfflinePhotoLoader.BrowseForFramePhoto(frameVo);
			}
		}
		
		/**
		 * notify everyone that frame is ready
		 * It adds listener, for every transform event and draggable event
		 */
		protected function frameReady():void
		{	
			// update quality indicator when image is ready
			updateQualityIndicator();
			
			//frame created
			CREATED.dispatch(this);	
		}
		
		private function onPhotoLoadError(e:ErrorEvent):void
		{
			imageError("FramePhoto > Error while loading image: " + e.toString());
			//clearMe();
		}
		
		
		/**
		 * kill, unload and destroy image loader
		 */
		protected function killPhotoLoader():void
		{
			try{
				TweenMax.killDelayedCallsTo(displayLoading);
			}catch(e:Error){}
				
			if(!photoLoader) return;
			photoLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onPhotoLoaded);
			photoLoader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onPhotoLoadProgress);
			photoLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onPhotoLoadError);
			if(photoLoader.contentLoaderInfo.bytes) photoLoader.contentLoaderInfo.bytes.clear();
			try{
				photoLoader.unload();
				photoLoader.close();
			}
			catch(e:Error){}
			photoLoader = null;
		}
		
		/**
		 * check if quality used for image correspond to quality needed
		 */
		protected function checkAndUpdateImageQuality():void
		{
			var displayWidth : Number = frameVo.photoVo.width*frameVo.zoom;
			var newResolution:String = "low";
			if (displayWidth > 450) newResolution = "medium";
			else if (displayWidth > 600) newResolution = "high";
			
			
			newResolution = "high";// TODO remove, use to check max quality EDIT: for now keep it like this, it's best for UX
			
			if(newResolution != workResolution) generatePhotoWithResolution(newResolution);
		}
		
		/**
		 * helper to convert a photo wanted scale to corresponding frame scale
		 * as the content scale is relative to the defaultWidth of the frame
		 * when we want to convert the image wanted scale to real content scale, use this helper
		 */
		private function getZoomByPhotoWantedScale( wantedScale : Number ) : Number
		{
			var zoomValue:Number = wantedScale * ( frameVo.photoVo.width/frameVo.width ) ;
			return zoomValue;
		}
		
		/**
		 * generate the photo with High, Medium or low resolution
		 * only working in closed system inside the image sprite
		 * we just update the working bitmap value, it doesnt affect anything in layout, zoom, crop
		 */
		protected function generatePhotoWithResolution(newResolution:String):void
		{
			// security
			/*
			if(!basePhoto){
				Debug.warn("FramePhoto.generatePhotoWithResolution : no base photo, we do not continue");
				return;
			}
			*/
			
			var newBmd:BitmapData;
			var bitmapScale:Number;
			
			// get fullsize image information
			var fullsizeWidth : int = frameVo.photoVo.width ;
			var fullsizeHeight : int =  frameVo.photoVo.height ;
			
			// we reduce size only if we are not in the thumbpreview
			if(!isThumbPreview)
			{
				// select quality wanted width
				var wantedWidth:Number;
				if(newResolution == "high") wantedWidth = 720;
				else if (newResolution == "medium") wantedWidth = 500;
				else wantedWidth = 300;
				
				// check possible user/exif rotation (offline only now but make the system compatible for both)
				var loadedWidth:Number = (frameVo.photoVo.exifRot == 90 || frameVo.photoVo.exifRot == -90)? basePhoto.height : basePhoto.width;
				var loadedHeight:Number = (frameVo.photoVo.exifRot == 90 || frameVo.photoVo.exifRot == -90)? basePhoto.width : basePhoto.height;
				
				//draw with correct ratio
				var drawRatio : Number = wantedWidth/loadedWidth;
				
				// only draw if base photo exists
				if(basePhoto) {
					if(!frameVo.photoVo.exifRot) frameVo.photoVo.exifRot = 0; // security for retro-compatibility
					var forceTransparent:Boolean = (frameVo.photoVo is ClipartVo || frameVo.photoVo is OverlayerVo);
					newBmd = BitmapUtils.transformBitmapData( basePhoto.bitmapData, frameVo.photoVo.exifRot, drawRatio, false,forceTransparent);
					
					/*
					newBmd = new BitmapData(loadedWidth*drawRatio, loadedHeight*drawRatio, true, 0x000000); //TODO: wathc out perf, when transparency set to true// TODO : check if we can implement in the current tic tac photo tansparent images (png transparency)
					var m:Matrix = new Matrix();
					m.scale(drawRatio, drawRatio);
					newBmd.draw(basePhoto, m, null, null, null,false); // do not draw using smoothing for perfs
					// checks if popart
					*/
					
					if(frameVo.isPopart) newBmd = Frame.popartBitmap(newBmd, frameVo);
				}
				else
				{
					// if no base photo, there is an error somewhere, we drow a red background
					Debug.warn("FramePhoto.base photo is null"); 
					newBmd = new BitmapData(loadedWidth*drawRatio, loadedHeight*drawRatio, false, 0xff0000); 					
				}
				
				bitmapScale = fullsizeWidth/wantedWidth;
			}
			else
			{
				// if thumb preview, we just scale the thumb bitmapdata
				newBmd = basePhoto.bitmapData.clone();
				bitmapScale = fullsizeWidth/basePhoto.bitmapData.width;
			}
			
			
			// kill current working bitmap
			if(workingBitmap.bitmapData) workingBitmap.bitmapData.dispose();
			workingBitmap.bitmapData = newBmd;
			workingBitmap.smoothing = true;
			
			// scale bitmap container to fit working fullSize
			workingBitmap.scaleX = workingBitmap.scaleY = bitmapScale;
			
			// center this image will allow to use always centered system
			workingBitmap.x = -workingBitmap.width*.5;
			workingBitmap.y = -workingBitmap.height*.5;
			
			workResolution = newResolution;
		}	
		
		/**
		 * Creates the quality indicator
		 */
		protected function createQualityIndicator():void
		{
			qualityIndicator = new mcs.photoWarningIcon(); // TODO : maybe store it one static bitmap
			qualityIndicator.visible = false;
			qualityIndicator.buttonMode = true;
			qualityIndicator.cacheAsBitmap = true;
			qualityIndicator.mouseChildren = false;
			qualityIndicator.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void{
				showQualityIndicatorTooltip();
			});
			qualityIndicator.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent):void{
				hideQualityIndicatorTooltip()
			});
		}
		
		/**
		 * display/hide QualityIndicator tooltip
		 */
		protected function showQualityIndicatorTooltip( autoHide : Boolean = false):void
		{
			hideQualityIndicatorTooltip();
			DefaultTooltip.tooltipOnClip(qualityIndicator,qualityIndicatorTooltip, false)
			if(autoHide) TweenMax.delayedCall(2, hideQualityIndicatorTooltip);
		}
		protected function hideQualityIndicatorTooltip():void
		{
			TweenMax.killDelayedCallsTo(hideQualityIndicatorTooltip);
			DefaultTooltip.killAllTooltips();
		}
		
		
		
		/**
		 * update quality indicator when modifying image
		 */
		public function updateQualityIndicator( showTooltip : Boolean = false ):void
		{
			if(frameVo.type == FrameVo.TYPE_BKG && frameVo.photoVo is BackgroundVo) return; // no more qulity indicator on BKG
			if(!frameVo.photoVo){
				if(qualityIndicator && qualityIndicator.parent) qualityIndicator.parent.removeChild(qualityIndicator);
				hideQualityIndicatorTooltip();
				return; // no photo, no quality indicator
			}
			
			// check quality
			//var qualityChanged : Boolean = true;
			switch (MeasureManager.checkImageResolution(frameVo.zoom))
			{
				case 1 : 
					qualityIndicator.visible = false;
					break;
				case 0 :
					qualityIndicator.visible = true;
					if(!qualityIndicatorTooltip || qualityIndicatorTooltip.bgColor != Colors.YELLOW)
					{
						qualityIndicator.gotoAndStop(1);
						qualityIndicatorTooltip = new TooltipVo("tooltip.frame.quality.low", TooltipVo.TYPE_SIMPLE, null, Colors.YELLOW,Colors.BLACK);
						//qualityChanged = true;
					}
					break;
				case -1 : 
					qualityIndicator.visible = true;
					if(!qualityIndicatorTooltip || qualityIndicatorTooltip.bgColor != Colors.RED)
					{
						qualityIndicator.gotoAndStop(2);
						qualityIndicatorTooltip = new TooltipVo("tooltip.frame.quality.ko", TooltipVo.TYPE_SIMPLE, null, Colors.RED);
						//qualityChanged = true;
					}
					break;
			}
			
			// positionate
			if( qualityIndicator.visible )
			{
				qualityIndicator.scaleX = qualityIndicator.scaleY = 1/EditionArea.editorScale;
				//qualityIndicator.x = frameVo.width*.5 - 20;
				//qualityIndicator.y = frameVo.height*.5 - 20;
				
				var margin : Number = (frameVo.type == FrameVo.TYPE_BKG)? 30 : 10;
				if( Infos.isCanvas ) margin = CanvasManager.instance.edgeSize;
				qualityIndicator.x = frameVo.width*.5 - qualityIndicator.width -margin;
				qualityIndicator.y = frameVo.height*.5 - qualityIndicator.height- margin;
				addChild(qualityIndicator); // be sure this is added on top of all
				
				
				// auto show quality indicator !
				if(showTooltip) showQualityIndicatorTooltip(true);
			}
			else
			{
				if(qualityIndicator.parent) qualityIndicator.parent.removeChild(qualityIndicator);
				hideQualityIndicatorTooltip();
			}
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	IMAGE TRANSFORM METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Resize content at specific point 
		 * 0 is Top Left
		 * 2 is Top Right
		 * 5 is Bottom Left
		 * 7 is Bottom Right
		 */
		override public function resize( resizeType : int ):void
		{	
			var staticPoint : Point; // static point is opposite point as the one dragged (actually the point not moving)
			
			// new frame properties
			var newWidth : Number;
			var newHeight : Number;
			var translateX : Number;
			var translateY : Number;
			
			//minSize = 30; // minimum size for a frame
			
			// TOP LEFT
			if( resizeType == 0){ 
				staticPoint = new Point(frameVo.width*.5, frameVo.height*.5) 
				newWidth = staticPoint.x - mouseX;
				if(newWidth < minSize ) newWidth = minSize;
				newHeight = newWidth / frameRatio;				
				translateX = (frameVo.width-newWidth)*.5;
				translateY = (frameVo.height-newHeight)*.5;
			}
				// BOTTOM LEFT
			else if( resizeType == 5){
				staticPoint = new Point(frameVo.width*.5, -frameVo.height*.5) 
				newWidth = staticPoint.x - mouseX;
				if(newWidth < minSize ) newWidth = minSize;
				newHeight = newWidth / frameRatio;				
				translateX = (frameVo.width-newWidth)*.5;
				translateY = (newHeight-frameVo.height)*.5;
			}
				// TOP RIGHT
			else if( resizeType == 2){
				staticPoint = new Point(-frameVo.width*.5 , frameVo.height*.5) 
				newHeight = staticPoint.y - mouseY  ;
				if(newHeight < minSize ) newHeight = minSize;
				newWidth = newHeight * frameRatio;				
				translateX = (newWidth-frameVo.width)*.5;
				translateY = (frameVo.height-newHeight)*.5;
			}
				// BOTTOM RIGHT
			else if( resizeType == 7){
				staticPoint = new Point(-frameVo.width*.5 , -frameVo.height*.5) 
				newWidth = mouseX - staticPoint.x ;
				if(newWidth < minSize ) newWidth = minSize;
				newHeight = newWidth / frameRatio;				
				translateX = (newWidth-frameVo.width)*.5;
				translateY = (newHeight-frameVo.height)*.5;
			}
			
			// to avoid pixel shaking effect, do nothing if it's less than 1 pixel
			if(Math.abs(translateX)<1 && Math.abs(translateY)<1) return;
			
			// apply new values
			var frameScale : Number = newWidth/frameVo.width;
			frameVo.cLeft *= frameScale;
			frameVo.cTop *= frameScale;
			
			// as the rotation can fake our pos calculation, we need to use a matrix to reset rotation first and be sure elements are at the right place
			var m : Matrix = transform.matrix.clone();
			m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(translateX, translateY); 		// translate to new position
			m.rotate(MathUtils2.toRadian(rotation)); 	// rotate to rotation needed
			
			frameVo.zoom = frameVo.zoom*frameScale;
			frameVo.width = newWidth;
			frameVo.height = newHeight;
			frameVo.x = m.tx;
			frameVo.y = m.ty;
			
			renderFrame();
			
			if(frameVo.photoVo) checkAndUpdateImageQuality();
			updateFrameBounds();
			
			updateQualityIndicator( true );
			
		}
		
		/**
		 * apply border (size is in mm)
		 */
		public function applyBorder(size:int, color:Number = NaN):void
		{
			if(!isNaN(color)) frameVo.fillColor = color;
			frameVo.border = size;
			drawFrameBorder(true);
			renderFrame();
			PageNavigator.RedrawPageByFrame(frameVo);
		}
		
		/**
		 * toggle shadow
		 */
		public function applyShadow():void
		{
			//frameVo.shadow = !frameVo.shadow;
			//if(frameVo.mask == FrameVo.MASK_TYPE_CIRCLE) frameVo.shadow = false; // force no shadow if circle
			renderFrame();
			PageNavigator.RedrawPageByFrame(frameVo);
		}
		
		
		/**
		 * Apply popart
		 */
		public function applyPopArt(flag:Boolean):void
		{
			frameVo.isPopart = flag;
			workResolution = null;
			checkAndUpdateImageQuality();
			renderFrame();
			save();
		}
		
		
		
		
		
		/**
		 * Resize content at specific point 
		 * 1 is Top
		 * 3 is Left
		 * 4 is right
		 * 6 is Bottom
		 */
		override public function crop( cropType : int ):void
		{	
			// for empty frames
			if(frameVo.isEmpty){
				cropEmptyFrame(cropType);
				return;
			}
			
			// set start crop info
			if(!startMousePoint){
				// setup init points
				startMousePoint = new Point(mouseX, mouseY);
				startImageBounds = new Rectangle(frameVo.x, frameVo.y, frameVo.width, frameVo.height);
				
				// if we are dealing with an image (and not an empty frame, we need to store initial crop values
				if( cropType == 3) startCropValue = frameVo.cLeft; // LEFT
				else if( cropType == 4) startCropValue = image.width - frameVo.width - frameVo.cLeft; // RIGHT
				else if( cropType == 1) startCropValue = frameVo.cTop; // TOP 
				else if( cropType == 6) startCropValue = image.height - frameVo.height - frameVo.cTop; // BOTTOM	
			}
			
				
			// amount of translation since start (difference of mouse positions)
			var translateX : Number = (startMousePoint.x - mouseX + startImageBounds.x- frameVo.x) /// EditionArea.editorScale;
			var translateY : Number = (startMousePoint.y - mouseY + startImageBounds.y- frameVo.y) /// EditionArea.editorScale;
				
				
			// amount of translation since start
				/*
			var translateX : Number = startMousePoint.x - stage.mouseX;
			var translateY : Number = startMousePoint.y - stage.mouseY;
			*/
				
			// current crop values
			var cLeft : Number = frameVo.cLeft;
			var cTop : Number = frameVo.cTop;
			var cRight : Number = image.width - frameVo.width - cLeft;
			var cBottom : Number = image.height - frameVo.height - cTop;
			
			var minSize : Number = 50;
			
			// LEFT 
			var newCropValue : Number
			if( cropType == 3){
				
				if(translateX > startCropValue) translateX = startCropValue;
				newCropValue = startCropValue-translateX;
				translateX = startImageBounds.x-frameVo.x-translateX*.5;
				
				if(translateX > frameVo.width-minSize) return;
				
				translateY = 0;
				cLeft = newCropValue;
			}
				// RIGHT 
			else if( cropType == 4){
				if(translateX < -startCropValue) translateX = -startCropValue;
				newCropValue = startCropValue+translateX;		
				translateX = startImageBounds.x-frameVo.x-translateX*.5;
				
				if(translateX < -(frameVo.width-minSize)) return;
				
				translateY = 0;
				cRight = newCropValue;
			}
				// TOP 
			else if( cropType == 1){
				if(translateY > startCropValue) translateY = startCropValue;
				newCropValue = startCropValue-translateY;
				translateY = startImageBounds.y-frameVo.y-translateY*.5;
				
				if(translateY > frameVo.height-minSize) return;
				
				translateX = 0;
				cTop = newCropValue;
			}
				// BOTTOM 
			else if( cropType == 6){
				if(translateY < -startCropValue) translateY = -startCropValue;
				newCropValue = startCropValue+translateY;		
				translateY = startImageBounds.y-frameVo.y-translateY*.5;
				
				if(translateY < -(frameVo.height-minSize)) return;
				
				translateX = 0;
				cBottom = newCropValue; 
			}
			
			// store previous pos
			//startMousePoint = new Point(parent.mouseX, parent.mouseY);
			
			// new frame width
			var newWidth:Number = image.width - cLeft - cRight;
			var newHeight:Number = image.height - cTop - cBottom;
			
			// as the rotation modify the pos calculation, we need to use a matrix to reset rotation first and be sure elements are at the right place
			var m : Matrix = transform.matrix.clone();
			//m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(translateX, translateY); 		// translate to new position
			//m.rotate(MathUtils2.toRadian(rotation)); 	// rotate to rotation needed
			
			
			frameVo.cLeft = cLeft;
			frameVo.cTop = cTop;
			
			frameVo.width = newWidth;
			frameVo.height = newHeight;
			frameVo.x = m.tx;
			frameVo.y = m.ty;
						
			renderFrame();
			updateFrameBounds();	
			updateQualityIndicator(); // update quality indicator
		}
		/**
		 * when zoom is over
		 */
		public override function endCrop():void
		{
			startMousePoint = null;
			startCropValue =-1; 
			startImageBounds = null;
		}
		// crop empty frame
		private function cropEmptyFrame( cropType : int ):void
		{	
			// set start crop info
			if(!startMousePoint){
				// setup init points
				startMousePoint = new Point(mouseX, mouseY);
				startImageBounds = new Rectangle(frameVo.x, frameVo.y, frameVo.width, frameVo.height);
			}				
					
			// amount of translation since start (difference of mouse positions)
			var translateX : Number = (startMousePoint.x - mouseX + startImageBounds.x- frameVo.x) /// EditionArea.editorScale;
			var translateY : Number = (startMousePoint.y - mouseY + startImageBounds.y- frameVo.y) /// EditionArea.editorScale;
			
			if( cropType == 3 || cropType == 4 ) translateY = 0; // no translation on Y for those croptypes
			else translateX = 0; // no translation on X for other croptypes
			
			
			// as the rotation modify the pos calculation, we need to use a matrix to reset rotation first and be sure elements are at the right place
			var m : Matrix = new Matrix(startImageBounds.width, 0, 0, startImageBounds.height,0,0);
			m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(translateX, translateY); 		// translate to new position
			
			// be sure crop values are still to null
			frameVo.cLeft = 0;
			frameVo.cTop = 0;
			
			// the minimum size for an empty frame crop
			var minSize : Number = 50;
			
			// LEFT 
			var newCropValue : Number
			if( cropType == 3){
				if(m.tx < -(startImageBounds.width-minSize)) m.tx = -(startImageBounds.width - minSize);
				frameVo.width = startImageBounds.width + m.tx ;	
				//frameVo.x = Math.floor(startImageBounds.x - m.tx*.5);
			}
				// RIGHT 
			else if( cropType == 4){
				if(m.tx > startImageBounds.width-minSize) m.tx = startImageBounds.width - minSize;
				frameVo.width = startImageBounds.width - m.tx ;
				//frameVo.x = Math.floor(startImageBounds.x - m.tx*.5);
			}
				// TOP 
			else if( cropType == 1){
				if(m.ty < -(startImageBounds.height-minSize)) m.ty = -(startImageBounds.height - minSize);
				frameVo.height = startImageBounds.height + m.ty ;
				//frameVo.y = Math.floor(startImageBounds.y - m.ty*.5);
			}
				// BOTTOM 
			else if( cropType == 6){
				if(m.ty > startImageBounds.height-minSize) m.ty = startImageBounds.height - minSize;
				frameVo.height = startImageBounds.height - m.ty ;
				//frameVo.y = Math.floor(startImageBounds.y - m.ty*.5);
			}
			
			m.rotate(MathUtils2.toRadian(rotation)); 	// rotate to rotation needed
			
			
			// the width 
			//frameVo.width = startImageBounds.width - m.tx ;
			//frameVo.height = startImageBounds.height - m.ty ;
			frameVo.x = Math.floor(startImageBounds.x - m.tx*.5);
			frameVo.y = Math.floor(startImageBounds.y - m.ty*.5);
			
			renderFrame();
			updateFrameBounds();			
		}
		
		
		/**
		 * Move content inside cropped area
		 */
		public function insideMove():void
		{	
			var stageRef:Stage = Infos.stage;
			
			// if multiple frame
			if(frameVo.isMultiple){
				if(!CanvasManager.instance.multipleFrame){
					Debug.warn("FramePhoto is multiple but there is no virtual multipleframe");
					return;
				}
				CanvasManager.instance.multipleFrame.insideMove();
				CanvasManager.instance.updateMultipleFramesPosition();
				return;
			}
			
			
			if(!movePreviewBitmap){
				
				// create movePreviewBitmap
				var bd : BitmapData = workingBitmap.bitmapData.clone();
				movePreviewBitmap = new Bitmap(bd,"auto", true);
				movePreviewBitmap.alpha = .3;
				movePreviewBitmap.scaleX = frameVo.zoom * workingBitmap.scaleX;
				movePreviewBitmap.scaleY = frameVo.zoom * workingBitmap.scaleY;
				addChildAt(movePreviewBitmap,0);
				
				// store initial values
				startMousePoint = new Point(stageRef.mouseX/EditionArea.editorScale, stageRef.mouseY/EditionArea.editorScale);
				startImageBounds = new Rectangle(image.x, image.y, image.width, image.height);
			}
			
			// new Image pos
			var newImageX:Number = stageRef.mouseX/EditionArea.editorScale-startMousePoint.x + startImageBounds.x;
			var newImageY:Number = stageRef.mouseY/EditionArea.editorScale- startMousePoint.y + startImageBounds.y;
			
			// manage drag limits
			var maxOffsetX : Number = (image.width-frameVo.width)*.5;
			var maxOffsetY : Number = (image.height-frameVo.height)*.5;
			
			if(newImageX < -maxOffsetX ) newImageX = -maxOffsetX;
			else if(newImageX > maxOffsetX ) newImageX = maxOffsetX;
			if(newImageY < -maxOffsetY ) newImageY = -maxOffsetY;
			else if(newImageY > maxOffsetY ) newImageY = maxOffsetY;
			
			// update preview bitmap
			movePreviewBitmap.x = newImageX - movePreviewBitmap.width*.5;
			movePreviewBitmap.y = newImageY - movePreviewBitmap.height*.5;
			
			// update crop values
			updateCropValues(newImageX, newImageY);
			
			// render frame
			renderFrame();
		}
		
		/**
		 * Stop inside move
		 * > reset and clean movePreviewBitmap
		 */
		public function stopInsideMove():void
		{
			// canvas multiple handle
			if(frameVo.isMultiple){
				if(!CanvasManager.instance.multipleFrame){
					Debug.warn("FramePhoto > insideMove > frame is multiple but there is no multiple frame reference, abord action");
					return;
				}
				CanvasManager.instance.multipleFrame.stopInsideMove();
				CanvasManager.instance.updateMultipleFramesPosition();
				return;
			}
			
			
			// clear preview bitmap
			if(movePreviewBitmap){
				movePreviewBitmap.bitmapData.dispose();
				movePreviewBitmap.bitmapData = null;
				if(movePreviewBitmap.parent) movePreviewBitmap.parent.removeChild(movePreviewBitmap);
				movePreviewBitmap = null;
			}
			
			startMousePoint = null;
			startImageBounds = null;
		}
		
		
		/**
		 * make inside rotation of 45°
		 */
		public function insideRotation():void
		{
			// as we make a rotation of 90°
			var fw:Number = frameVo.width;
			frameVo.width = frameVo.height;
			frameVo.height = fw;
			
			frameVo.rotation += 90;
			FrameVo.injectPhotoVoInFrameVo(frameVo, frameVo.photoVo);
			
			renderFrame();
			updateFrameBounds();	
		}
		
		
		
		
		
		/**
		 * Set inside zoom
		 * > inside zoom correspond to a content scale but keeping the same crop area
		 * > we have to zoom the content and keep crop area how it was
		 * > choice has been made to reset zoom amount at 1 when a zoom is finished, so the user can always zoom in and out
		 */
		public function zoom ( amount:Number ):void
		{
			// if multiple frame
			if(frameVo.isMultiple){
				if(!CanvasManager.instance.multipleFrame){
					Debug.warn("FramePhoto > insideMove > frame is multiple but there is no multiple frame reference, abord action");
					return;
				}
				CanvasManager.instance.multipleFrame.zoom(amount);
				CanvasManager.instance.updateMultipleFramesPosition();
				return;
			}
			
			
			// store initial values
			if(isNaN(zoomStartScale)) {
				zoomStartScale = frameVo.zoom; 					// we start with current image scale
				zoomStartPos = new Point( image.x, image.y ); 	// and current image position
			}
			
			// update content zoom
			var newScale:Number = zoomStartScale*amount;
			
			// limit scale
			if(image.width/image.scaleX * newScale < frameVo.width) return;
			if(image.height/image.scaleY * newScale < frameVo.height) return;
			
			image.scaleX = image.scaleY = newScale;
			
			var newImageX : Number = zoomStartPos.x * amount;
			var newImageY : Number = zoomStartPos.y * amount;
			
			// limit x
			var limitX : Number = (image.width-frameVo.width)*.5;
			var limitY : Number = (image.height-frameVo.height)*.5;
			
			if(newImageX < -limitX) newImageX = -limitX;
			else if(newImageX > limitX) newImageX = limitX;
			if(newImageY < -limitY) newImageY = -limitY;
			else if(newImageY > limitY) newImageY = limitY;
			
			// save values
			frameVo.zoom =newScale;
			updateCropValues(newImageX, newImageY);
			
			// render
			renderFrame();
			
			//updateFrameBounds();
			checkAndUpdateImageQuality();
			updateQualityIndicator();
		}
		/**
		 * when zoom is over
		 */
		public function endZoom():void
		{
			// if multiple frame
			if(frameVo.isMultiple){
				if(!CanvasManager.instance.multipleFrame){
					Debug.warn("FramePhoto > insideMove > frame is multiple but there is no multiple frame reference, abord action");
					return;
				}
				CanvasManager.instance.multipleFrame.endZoom();
				CanvasManager.instance.updateMultipleFramesPosition();
				return;
			}
			zoomStartScale = NaN;
			zoomStartPos = null;
			//placeAtCenterPoint();
			updateQualityIndicator(true);
		}
		
		
		/**
		 * on Image over
		 */
		override protected function over(force:Boolean = false):void
		{
			if(!isSticked || force)
				super.over(force);
			else
			{
				if(allowMove) CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
		}
		
		/**
		 * on Image over
		 */
		override protected function rollOut(force:Boolean = false):void
		{
			if(!isSticked || force)
			super.rollOut(force);
			else
			{
				CursorManager.instance.reset();
			}
		}
				
		
		/**
		 * return zoom value with wanted frame width
		 */
		/*
		private function getZoomByPhotoWantedWidth( wantedWidth:Number ) : Number
		{
			var wantedScale : Number = wantedWidth / frameVo.photoVo.width;
			return getZoomByPhotoWantedScale(wantedScale);
		}
		*/
	}
}
import flash.display.Graphics;
import flash.display.LineScaleMode;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormatAlign;

import be.antho.data.ResourcesManager;

import comp.button.SimpleButton;
import comp.label.LabelTictac;

import data.FrameVo;
import data.Infos;

import org.osflash.signals.Signal;

import utils.Colors;


internal class ErrorBox extends Sprite
{
	
	// signals
	public var RETRY_CLICKED : Signal = new Signal();
	public var REMOVE_CLICKED : Signal = new Signal();
	public var REIMPORT_CLICKED : Signal = new Signal();
	
	public var content : Sprite;
	public var label : LabelTictac;
	public var retryBtn : Sprite;
	public var removeBtn : Sprite;
	
	// constructor
	public function ErrorBox( frameVo : FrameVo)
	{
		//semi-transparent zone to display frame bounds
		var errorColor :Number = Colors.RED_FLASH;
		var g:Graphics = this.graphics;
		g.lineStyle(1,errorColor,1,true, LineScaleMode.NONE);
		g.beginFill(errorColor,.5);
		g.drawRect(-frameVo.width*.5,-frameVo.height*.5,frameVo.width, frameVo.height);
		g.endFill();
		
		// content
		content =  new Sprite();
		addChild(content);
		
		// create error label
		var imageName : String = (frameVo && frameVo.photoVo)? " (" + frameVo.photoVo.name +")": "";
		label = new LabelTictac(content, 0,0,ResourcesManager.getString("edition.frame.load.error.label") +imageName,13,Colors.WHITE,TextFormatAlign.CENTER);
		label.textField.autoSize = "center";
		label.textField.multiline = true;
		label.textField.wordWrap = true;
		label.textField.width = frameVo.width;
		
		if(!Infos.IS_DESKTOP)
		{
			//add retry btn
			retryBtn = SimpleButton.createPhotoErrorCTA("edition.frame.load.error.retry");
			retryBtn.name = "retryBtn";
			retryBtn.buttonMode = true;
			retryBtn.addEventListener(MouseEvent.CLICK, retry);
			retryBtn.x = Math.round(-retryBtn.width * .5);
			content.addChild(retryBtn);
			
			// add a remove frame btn
			removeBtn = SimpleButton.createPhotoErrorCTA("edition.frame.load.error.delete");
			removeBtn.name = "removeBtn";
			removeBtn.buttonMode = true;
			removeBtn.addEventListener(MouseEvent.CLICK, remove);
			removeBtn.x = Math.round(-removeBtn.width * .5);
			content.addChild(removeBtn);
		}
		else
		{
			// add a remove frame btn
			removeBtn = SimpleButton.createPhotoErrorCTA("edition.frame.load.error.reimport");
			removeBtn.name = "removeBtn";
			removeBtn.buttonMode = true;
			removeBtn.addEventListener(MouseEvent.CLICK, reImport);
			removeBtn.x = Math.round(-removeBtn.width * .5);
			content.addChild(removeBtn);
		}
		
		
		
	}
	
	
	/**
	 * update error box by the scale factor
	 */
	public function updateContent(scaleFactor : Number, frameVo:FrameVo):void
	{
		if(!scaleFactor || !this.parent) return; // do nothing
		
		content.scaleX = content.scaleY = 1/scaleFactor;
		if(frameVo){
			label.textField.width = frameVo.width*scaleFactor;
			label.x = Math.round(- label.textField.width*.5) ;
			label.y = -10-label.textField.height;
			var posY:Number = 0;
			if(retryBtn){
				retryBtn.y = posY;
				posY = retryBtn.height + 10 + posY;
			}
			removeBtn.y = posY;
		}
			
	}
	
	/**
	 *
	 */
	private function retry(e:MouseEvent):void
	{
		RETRY_CLICKED.dispatch();
	}
	private function remove(e:MouseEvent):void
	{
		REMOVE_CLICKED.dispatch();
	}
	private function reImport(e:MouseEvent):void
	{
		REIMPORT_CLICKED.dispatch();
	}

}
