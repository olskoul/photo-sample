package view.edition
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mcs.tempNavigation.view;
	
	import org.osflash.signals.Signal;

//library



	public class TempNavigation extends Sprite
	{
		private var next:Sprite;
		private var previous:Sprite;
		
		// signals
		public var NEXT:Signal = new Signal();
		public var PREV:Signal = new Signal();
		
		public function TempNavigation()
		{
			var view:MovieClip = new mcs.tempNavigation.view();
			addChild(view);
			next = view.next;
			next.addEventListener(MouseEvent.CLICK, navBtnClickhandler);
			previous = view.previous;
			previous.addEventListener(MouseEvent.CLICK, navBtnClickhandler);
		}
		
		protected function navBtnClickhandler(event:MouseEvent):void
		{
			switch(event.target)
			{
				case next:
					NEXT.dispatch();
					break;
				case previous:
					PREV.dispatch();
					break;
			}
			
		}
	}
}