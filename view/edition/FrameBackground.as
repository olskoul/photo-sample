package view.edition
{
	import com.bit101.components.ProgressBar;
	
	import data.BackgroundVo;
	import data.FrameVo;

	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import library.background.icon.bmd;
	
	import manager.PagesManager;
	import utils.CursorManager;
	import utils.Debug;
	
	public class FrameBackground extends FramePhoto
	{
		
		//Bitmapdata call to action for Backgrounds
		private static var ctaBkgIconBmd:BitmapData; //-> we use a static variable so it's created only once for all empty frames of the application
		
		private var background : Sprite = new Sprite();
		private var postCardBackground : Sprite;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRuCTOR
		////////////////////////////////////////////////////////////////
		
		public function FrameBackground(frameVo:FrameVo)
		{
			super(frameVo);
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * Overide the label for the cta btn
		 */
		override public function get labelCta():String
		{
			return "";
		}
		/**
		 * Overide the icon bitmapdata: Return the icon for photo
		 */
		override public function get iconbmd():BitmapData
		{
			if(!ctaBkgIconBmd) ctaBkgIconBmd = new library.background.icon.bmd();
			return ctaBkgIconBmd;
		}
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function deleteMe( withAnim : Boolean = false):void
		{
			Debug.warn("FrameBackground > deleteMe > should never be called.., we do nothing here");
		}
		
		/**
		 * Clear background content
		 * > if image, removes it
		 * > if background removes it
		 * > if fill set it to alpha = 0
		 */
		public override function clearMe():void
		{
			Debug.log("FrameBackground.clearMe");
			if(frameVo) frameVo.fillColor = -1;
			super.clearMe();
			/*
			// kill loader
			killPhotoLoader();
			
			// remove photoVO
			frameVo.photoVo = null;
			//frameVo.allowToolBar = false;
			
			// update transform tool 
			//updateTransformToolButtons();
			
			//Clear fillcolor
			frameVo.fillColor = -1;
			
			// dispose base photo
			clearBasePhoto();
			
			// clear working bitmap
			clearWorkingImage();
			
			// update state to reset content
			updateState();
			//renderFrame();
			
			/*_isEmpty = true;
			CLEAR.dispatch(this);* /
			
			// page vo updated
			//PageArea.REDRAW_PAGE_IN_NAVIGATOR.dispatch(PagesManager.instance.getPageVoByFrameVo(frameVo));
			// register delete frame
			//UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			// update used/not used items
			//ProjectManager.instance.remapPhotoVoList();
			// deselect from transform tool
			DESELECT.dispatch(this);
			*/
		}
		
		
		override public function get dropBounds():Rectangle
		{
			if(frameVo.photoVo && masker) return masker.getRect(this);
			else return this.getRect(this);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * 1. simple init frame
		 * After creation in page area
		 */
		public override function init():void
		{
			// update state depending on photo vo
			updateState();
			
			renderFrame();
			
			if(frameVo.photoVo) loadPhoto();
			else frameReady();
			
			setListeners();
		}
		
		
		/**
		 * switch to correct state
		 * which can be : 
		 * 	1. empty -> no photo > we remove content sprite
		 *  2. with photo -> we remove cta and add content
		 */
		protected override function updateState():void
		{
			if( frameVo.isPostCardBackground)
				drawBackground();
			
			else if( !frameVo.isEmpty ) // IS NOT EMPTY
			{
				// CONTENT IS AN IMAGE	
				if(frameVo.photoVo) {
					// create image mask
					if(!masker){
						masker = new Shape();
						masker.graphics.beginFill(0xff0000, 1);
						masker.graphics.drawRect(-50,-50,100,100);
						masker.graphics.endFill();
					}
					
					// create content of not already done
					if(!content){
						content = new Sprite();
						image = new Sprite();
						workingBitmap = new Bitmap();
						image.addChild(workingBitmap);
						content.addChild(image);
					}
					
					// create loading bar
					if(!loadingBar){
						loadingBar = new ProgressBar(null, 0,0);
						loadingBar.x = -(loadingBar.width)*.5;
						loadingBar.y = -(loadingBar.height)*.5;
					}
					
					// create quality indicator
					if(!qualityIndicator && !(frameVo.photoVo is BackgroundVo))createQualityIndicator();
					
					// add content
					addChild(content);
					background.alpha = 0;
					//frameVo.allowToolBar = true;
				}
				
				// CONTENT IS A FILL
				else
				{
					drawBackground();
					//frameVo.allowToolBar = false;
				}
			} 
			
			// IS EMPTY
			else  
			{		
				drawBackground();
				//frameVo.allowToolBar = false;
				if(content && content.parent) removeChild(content);
				if(masker && masker.parent) masker.parent.removeChild(masker);
			}
			
			//
			updateTransformToolButtons();
		}
		
		
		
		/**
		 * draw the background
		 */ 
		private function drawBackground():void
		{
			background.alpha = 1;
			background.graphics.clear();
			
			var tintColor :Number = ( frameVo.fillColor != -1) ? frameVo.fillColor : 0xffffff;
			background.graphics.beginFill( tintColor,1 );
			background.graphics.drawRect(-10,-10,20,20); // it will be scaled later
			background.graphics.endFill();
			
			background.width = frameVo.width;
			background.height = frameVo.height;
			
			
			// add it if not already done
			if(!background.parent) addChildAt(background, 0);
			
			//PostCard Back draw
			if(frameVo.isPostCardBackground)
				drawPostCardBack();
			
			
			// remove quality indicator if not already done
			if(qualityIndicator && qualityIndicator.parent) qualityIndicator.parent.removeChild(qualityIndicator);
		}
		
		/**
		 * draw the postcard back (replacing old system that was loading a image and scaling it down)
		 * -> old image width was 876 pixels
		 * -> Line for adresses
		 * -> stamp area
		 * -> separation line
		 */ 
		private function drawPostCardBack():void
		{
			if(postCardBackground)
			{
				postCardBackground.graphics.clear();
				if(postCardBackground.parent) postCardBackground.parent.removeChild(postCardBackground);
				postCardBackground = null;
			}
			
			postCardBackground = PagesManager.instance.drawPostCardBack();
			//Center it
			postCardBackground.x = -postCardBackground.width/2;
			postCardBackground.y = -postCardBackground.height/2;
			
			//add child it
			if(!postCardBackground.parent) addChild(postCardBackground);
			
		}		
		
		/**
		 * render a frame photo based on the frame width & height, x&y, crop values and zoom
		 */
		protected override function renderFrame():void
		{
			//this is double check just in case we don't gro through the updateState function (Ex:Update bkg fillColor)			
			// check if there is a photo
			if(frameVo.photoVo)
			{
				content.scaleX = content.scaleY = 1;
				image.scaleX = image.scaleY = frameVo.zoom;
				
				// Apply crop
				if( image.width != frameVo.width || image.height != frameVo.height ){
					
					// setup the mask (which as the same size as the frame vo)
					masker.width = frameVo.width;
					masker.height = frameVo.height;
					content.mask = masker;
					if(!masker.parent) addChild(masker);
					
					image.x = (image.width-frameVo.width)*.5 - frameVo.cLeft;
					image.y = (image.height-frameVo.height)*.5 - frameVo.cTop;
				}
				else {
					content.mask = null;
					if(masker.parent) masker.parent.removeChild(masker);
				}
			}
			
			// if no f-photo, check if there is a fill
			else drawBackground();
			
			
			// apply position after center content
			x = frameVo.x;
			y = frameVo.y;
			rotation = frameVo.rotation;
			
			// quality indicator
			updateQualityIndicator();
		}

		/**
		 * Overriden funciton by each extends of this class
		 * this what the frame should do when the CTA is clicked
		 */
		override protected function ctaClickHandler(e:MouseEvent = null):void
		{
			// do nothing
		}

		/**
		 * on Image over
		 */
		override protected function over(force:Boolean = false):void
		{
			if(!isSticked || force)
				super.over(force);
			else
			{
				if(allowMove) CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
		}
		
		/**
		 * on Image over
		 */
		override protected function rollOut(force:Boolean=false):void
		{
			if(!isSticked || force)
			super.rollOut(force);
			else
			{
				CursorManager.instance.reset();
			}
		}
		
	
		/**
		 * update transform tool buttons depending on current frame vo properties
		 */
		protected function updateTransformToolButtons() : void
		{
			//frameVo.allowMove = false;
			/*if( frameVo.photoVo != null ){
				frameVo.allowInsideMove = true;
			}
			else 
			{
				frameVo.allowInsideMove = false;
			}*/
			
			//SELECT.dispatch( this ); // update toolbar by sticking it again
		}
		
		/*
		override public function set isSticked(value:Boolean):void
		{
			_isSticked = value;
			if(value)
			{
				//out(true);
				//CursorManager.instance.showCursorAs(CursorManager.CURSOR_MOVE);
			}
			
		}
		*/
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Create a bitmapdata of content
		 * Scale it down to match thumb size (150*110)
		 */
		public function createThumbBitmapData():BitmapData
		{
			Debug.log("FrameBackground.createThumbBitmapData");
			if(content != null)
			{
				var ratio:Number = (frameVo.width > frameVo.height)?80 / frameVo.width:70 / frameVo.height;
				var bmd:BitmapData = new BitmapData(frameVo.width*ratio, frameVo.height*ratio, false, 0x000000); 
				var matrix:Matrix = new Matrix();
				matrix.scale(ratio,ratio);
				matrix.translate((frameVo.width*ratio)/2, (frameVo.height*ratio)/2);

				//remove qualityIndicator
				var quliatyIndicatorVisibility:Boolean;
				if(qualityIndicator)
				{
					quliatyIndicatorVisibility = qualityIndicator.visible
					qualityIndicator.visible = false;
				}
				
				bmd.draw(this, matrix);
				
				if(qualityIndicator)
				{
					qualityIndicator.visible = quliatyIndicatorVisibility;
				}
				
				//Debug, see the thumb
				/*var bitmap:Bitmap = new Bitmap(bmd);
				stage.addChild(bitmap);*/
				
				return bmd;
			}
			else
				return null;
		}
		
		
		
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		
		
	}
}