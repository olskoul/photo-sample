﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.uploadManager
{
	import com.bit101.components.PushButton;
	import com.bit101.components.ScrollPane;
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.tabs.TabManager;
	
	import comp.PanelAbstract;
	import comp.button.SimpleThinButton;
	
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	CONFIG::offline
	{
		import flash.events.FileListEvent;
		import flash.filesystem.File;
	}
	import flash.events.MouseEvent;
	
	import flash.geom.Point;
	import flash.globalization.DateTimeFormatter;
	import flash.globalization.DateTimeStyle;
	import flash.globalization.LocaleID;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.FileReferenceList;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	
	import manager.FolderManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import mcs.uploadListItem;
	import mcs.uploadPanel;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FramePhoto;
	import view.edition.PageArea;
	import view.edition.TransformTool;
	import view.edition.pageNavigator.PageNavigator;
	import view.photoManager.PhotoManagerModule;
	import be.antho.utils.DataLoadingManager;
	import view.popup.PopupAbstract;
	
	/**
	 * The photoUploadManager view is the mondule to upload photos
	 */
	public class PhotoUploadManager extends PanelAbstract
	{
		// SIGNALS
		public static const QUEUE_UPDATE:Signal = new Signal(PhotoUploadItem); // send true when this is a list update done after an upload completion
		public static const TEMP_PHOTO_UPDATE:Signal = new Signal(PhotoUploadItem); // send when a new temp photo is available to use (should change the photo list)
		
		
		// STATIC Props
		public static var OPENING_POINT:Point ;
		//public static var tempUploadItems:Vector.<PhotoUploadItem> = new Vector.<PhotoUploadItem>; // temp storage for upload items that can be used already in app (but not yet uploaded)
		
		
		private const MAX_OFFLINE_ENCODING_ITEMS:int = 1;//3; // maximum amount of encoding images at the same time // TODO : set this to minimum
		
		
		// PRIVATE Props
		// view
		private var view : mcs.uploadPanel = new uploadPanel;
		private var title : TextField;
		private var box : Sprite;
		private var detailLabel : TextField;
		private var bg:Sprite;
		private var closeBtn:Sprite;
		private var uploadButton : SimpleThinButton;
		private var viewButton : SimpleThinButton;
		private var uploadContainer : ScrollPane;
		
		// datas
		private var fileRefList : FileReferenceList; // on multiple file upload we use this
		private var fileRef : FileReference; // on single file upload we use this
		private var abordedItems : Array = new Array(); // list of all aborded items
		
		CONFIG::offline
		{
			private var localFileBrowser : File; // file browser for offline editor
		}
		
		// 
		public var tempCompletedItemList : Vector.<PhotoUploadItem> = new Vector.<PhotoUploadItem>;	// list of all upload complete items done but no image list refresh has been done, so we need to keep those to show the full photo list
		protected var _uploadItemsList : Vector.<PhotoUploadItem> = new Vector.<PhotoUploadItem>;		// list of all upload items
		public function get uploadItemsList():Vector.<PhotoUploadItem>
		{
			return _uploadItemsList;
		}
		
		//
		private var importIndex : int = 0; // current index of importing
		private var uploadIndex : int = 0; // current index of uploading
		private var maxUploadingItems:int = 2; // the maximum items that are uploading at the same time (for offline, = the maximum amount of items written to disk at the same time)
		
		
		//private var isImporting : Boolean;
		//private var	isUploading : Boolean;
		private var waitingFrameVo:FrameVo; // currently waiting frameVo (if we upload image directly in frameVo)
		private var folderToUse : String; // the current folder/category in which image will be uploaded
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		public function PhotoUploadManager(sf : SingletonEnforcer) 
		{
			if(!sf) throw new Error("ImageEditorModule is a singleton"); 
		
			addChild(view);
			// refs
			title = view.title;
			title.multiline = false;
			title.wordWrap = false;
			title.autoSize = "left";
			detailLabel = view.summary;
			box = view.box;
			bg = view.bg;
			closeBtn = view.closeBtn;
			
			ButtonUtils.makeButton(closeBtn, function(e:Event):void{close()});
			
			// upload container
			uploadContainer = new ScrollPane(view, box.x+5, box.y + 5);
			uploadContainer.showScrollButtons(false);
			uploadContainer.backgroundAlpha = 0;
			uploadContainer.autoHideScrollBar = true;
			uploadContainer.hScrollbarEnabled = false;
			uploadContainer.mouseWheelEnabled = true;
			uploadContainer.setSize(box.width-10,box.height-5);
			uploadContainer.shadow = false;
			
			uploadButton = new SimpleThinButton(	"upload.panel.uploadButton", // label
													Colors.BLUE, //over color
													onUploadButtonClick, //handler
													Colors.GREY, //off color
													Colors.WHITE, //off color label
													Colors.WHITE); // over color label
			uploadButton.x = box.x + box.width - uploadButton.width;
			uploadButton.y = detailLabel.y;
			addChild(uploadButton);
			
			viewButton = new SimpleThinButton(	"upload.panel.viewButton", // label
													Colors.BLUE, //over color
													onViewButtonClick, //handler
													Colors.GREY, //off color
													Colors.WHITE, //off color label
													Colors.WHITE); // over color label
			viewButton.x = uploadButton.x - viewButton.width - 20;
			viewButton.y = detailLabel.y;
			addChild(viewButton);
			/*uploadButton = new PushButton(this, box.x + box.width - 100, detailLabel.y, ResourcesManager.getString("upload.panel.uploadButton"), onUploadButtonClick);
			viewButton = new PushButton(this, box.x + box.width - 220, detailLabel.y, ResourcesManager.getString("upload.panel.viewButton"), onViewButtonClick);*/
			
			maxUploadingItems = Infos.IS_DESKTOP? 1 : 2; 
		}
		
		/**
		 * ------------------------------------INSTANCE/CONSTRUCTOR -------------------------------------	
		 */
		
		private static var _instance : PhotoUploadManager;
		private var isBrownsingModalOpen:Boolean;

		public static function get instance():PhotoUploadManager
		{
			if(!_instance) _instance = new PhotoUploadManager(new SingletonEnforcer());
			return _instance;
		}
		
		

		
		/**
		 * ------------------------------------ GETTER/SETTER -------------------------------------	
		 */
		
		/**
		 * amount of total uploading items still active 
		 */
		public function get numUploadingItems():int{
			return _uploadItemsList.length;
		}
		
		/**
		 * retrieve ratio
		 */
		public function get numUploadLeft():int
		{
			// TODO : check & verify this
			return numUploadingItems - uploadIndex;
		}
		
		/**
		 * retrieve percent left
		 */
		public function get completionPercent():int
		{
			// TODO : check & verify this
			return uploadIndex/numUploadingItems*100;
		}
		
		/**
		 * retrieve existing upload item by it's thumb id
		 */
		public function getUploadItemByTempID( tempId:String ):PhotoUploadItem
		{
			for (var i:int = 0; i < uploadItemsList.length; i++) 
			{
				if (uploadItemsList[i].photoVo && uploadItemsList[i].photoVo.tempID == tempId) return uploadItemsList[i];
			}
			return null;
		}
		
		
		
		
		/**
		 * ------------------------------------ BROWSE USER COMPUTER -------------------------------------	
		 */
		
		public function browse( $toFrame : FrameVo = null, $folderToUse:String = null ):void
		{
			Debug.log("PhotoUploader.browse (inside a frame : "+$toFrame+")");
			
			if(isBrownsingModalOpen)
			{
				Debug.log("PhotoUploader > browse: Dialog box is already open -> Return");
				return;
			}
			
			DataLoadingManager.instance.showLoading();
			
			waitingFrameVo = null; 
			
			// folder destination
			folderToUse = ($folderToUse)? $folderToUse : FolderManager.instance.currentProjectFolderName;
			
			/*
			if($ folderToUse)
				else folderToUse = 
			{
				
				var df:DateTimeFormatter = new DateTimeFormatter(LocaleID.DEFAULT, DateTimeStyle.SHORT, DateTimeStyle.NONE);
				var currentDate:Date = new Date();
				folderToUse = df.format(currentDate);
				//folderToUse = "" + Infos.project.id; TODO : make this work, to do so, we need the project ID which arrives after the first save.. find a way to handle this problem..
				
			}
			*/
			
			//var fileFilter : Array = [ new FileFilter(ResourcesManager.getString("upload.file.filter"), "*.jpg;*.jpeg;*.JPG;*.JPEG;*.png;*.PNG") ];
			var fileFilter : Array = [ new FileFilter(ResourcesManager.getString("upload.file.filter"), "*.jpg;*.jpeg;*.JPG;*.JPEG") ];
			//private static const FILE_TYPES:Array = [new FileFilter(ResourcesManager.getString("upload.file.filter"), "*.jpg;*.jpeg;*.JPG;*.JPEG")];
			//fileRefList.browse(new Array( new FileFilter( "Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg;*.jpeg;*.gif;*.png" )));
			
			// start browse for photos
			if(!$toFrame) {
				// open multiple file upload window				
				isBrownsingModalOpen = true;
				try
				{
					CONFIG::online
					{
						fileRefList = new FileReferenceList();
						fileRefList.addEventListener(Event.SELECT, multipleFileListSelectHandler);	
						fileRefList.addEventListener(Event.CANCEL, fileCancelHandler);	
						fileRefList.browse( fileFilter );
					}
					
					CONFIG::offline
					{
						Debug.warn("PhotoUploadManager.browse > we whould never go here with offline editor");
						/*
						//lastUsedFolderPath = File.documentsDirectory.url; // test
						if(Infos.lastUserBrowsePath != "" && Infos.lastUserBrowsePath != null)
							localFileBrowser = new File(Infos.lastUserBrowsePath);
						else
							localFileBrowser = new File();
						
						localFileBrowser.addEventListener(FileListEvent.SELECT_MULTIPLE, offlineMultipleFileListSelectHandler);	
						localFileBrowser.addEventListener(Event.CANCEL, fileCancelHandler);
						localFileBrowser.browseForOpenMultiple(ResourcesManager.getString("upload.file.filter"), fileFilter);
						*/
					}

					
				}
				catch(e:Error)
				{
					isBrownsingModalOpen = false;
					Debug.warnAndSendMail("PhotoUploader > browse error, we do not continue.. : " + e.message); 
				}
			} 
			else
			{
				waitingFrameVo = $toFrame;
				// open one file upload window
				isBrownsingModalOpen = true;
				try //TODO: Remove try catches
				{
					CONFIG::online
					{
						fileRef = new FileReference();
						fileRef.addEventListener(Event.SELECT, singleFileSelectHandler);
						fileRef.addEventListener(Event.CANCEL, fileCancelHandler);
						fileRef.browse( fileFilter );
					}
					
					CONFIG::offline
					{
						if(Infos.lastUserBrowsePath != "" && Infos.lastUserBrowsePath != null)
							localFileBrowser = new File(Infos.lastUserBrowsePath);
						else
							localFileBrowser = new File();
						localFileBrowser.addEventListener(Event.SELECT, offlineSingleFileSelectHandler);	
						localFileBrowser.addEventListener(Event.CANCEL, fileCancelHandler);
						localFileBrowser.browse(fileFilter); 
					}
				}
				catch(e:Error)
				{
					isBrownsingModalOpen = false;
					fileRef.cancel();
					Debug.warnAndSendMail("PhotoUploader > browse error, we do not continue.. : " + e.message); 
				}
			}
			
		}
		protected function fileCancelHandler(event:Event):void
		{
			DataLoadingManager.instance.hideLoading();
			isBrownsingModalOpen = false;
			destroyFileRefList();
		}
		protected function destroyFileRefList():void
		{
			if(fileRefList) {
				fileRefList.removeEventListener(Event.SELECT, multipleFileListSelectHandler);	
				fileRefList.removeEventListener(Event.CANCEL, fileCancelHandler);	
				fileRefList = null;
			}
			
			/* do not use as fileref is kept in reference inside the upload item
			if(fileRef){
				fileRef.removeEventListener(Event.SELECT, fileSelectHandler);
				fileRef.removeEventListener(Event.CANCEL, fileCancelHandler);
				fileRef = null;
			}
			*/
			CONFIG::offline
			{
				if(localFileBrowser){
					localFileBrowser.removeEventListener(FileListEvent.SELECT_MULTIPLE, offlineMultipleFileListSelectHandler);	
					localFileBrowser.removeEventListener(Event.SELECT, singleFileSelectHandler);	
					localFileBrowser.removeEventListener(Event.CANCEL, fileCancelHandler);
					localFileBrowser.cancel();
					localFileBrowser = null;
				}
			}
			
		}
		
		/**
		 * on file list selected, create visual uploaders
		 */
		private function singleFileSelectHandler(e:Event):void
		{
			Debug.log("PhotoUploader > fileSelectHandler >> single file upload in frame start");
			isBrownsingModalOpen = false;
			
			var fileRef:FileReference = (e.currentTarget) as FileReference;
			// TODO : prepare frame (remove add button for example )
			
			createUploadItems( [ fileRef ] );
		}
		
		/**
		 * on file list selected, create visual uploaders
		 */
		CONFIG::offline
		{
			private function offlineSingleFileSelectHandler(e:Event):void
			{
				Debug.log("PhotoUploader > offlinefileSelectHandler >> single file upload in frame start");
				isBrownsingModalOpen = false;			
				createUploadItems( [ e.target ] );
			}
		}
		
		/**
		 * on file list selected, create visual uploaders
		 */
		private function multipleFileListSelectHandler(e:Event):void
		{
			Debug.log("PhotoUploader > fileListSelectHandler >> multiple file upload start");
			isBrownsingModalOpen = false;
			createUploadItems( fileRefList.fileList );
		}
		
		/**
		 * multiple file select of offline
		 */
		CONFIG::offline
		{
			private function offlineMultipleFileListSelectHandler(e:FileListEvent):void
			{
				Debug.log("PhotoUploader.offlineMultipleFileListSelectHandler");
				isBrownsingModalOpen = false;
				createUploadItems( e.files );
			}
		}
		
		
		
		/**
		 * ------------------------------------ POPUP BEHAVIOR OVERRIDE -------------------------------------	
		 */
		
		
		/**
		 * open view at specific point
		 */
		public override function open( $pos : Point):void
		{
			Debug.log("PhotoUploader > open");
			//if(!$pos) $pos = openPos; // previous pos
			
			// force opening point
			updateView();
			super.open(OPENING_POINT);
		}
		/**
		 * close view
		 */
		public override function close(e:Event = null):void
		{
			Debug.log("PhotoUploader > close");
			super.close();
		}
		
		
		/**
		 * ------------------------------------ GET TEMP THUMB DATA CLONE for temp photoVo -------------------------------------	
		 */
		
		/**
		 * Get TEMP thumb data from photoVo Id when photoVo is temp=true
		 */
		public function getTempThumbDataClone( photoId : String ):BitmapData
		{
			for (var i:int = 0; i < _uploadItemsList.length; i++) 
			{
				if(_uploadItemsList[i].photoVo.id == photoId){
					return _uploadItemsList[i].getThumbBitmapDataClone(); 
				}
			}
			return null;
		}
		
		
		/**
		 * ------------------------------------ UPDATE TEMP IMAGE FOLDER NAME IF IT WAS CHANGED AND PHOTO IS STILL TEMP -------------------------------------	
		 */
		
		/**
		 * Update possible items using temp folder to new project folder
		 */
		public function updateTempFolderUploadName( newFolderName : String ):void
		{
			Debug.log("PHotoUploadManager.updateTempFolderUploadName : "+newFolderName);
			if(! _uploadItemsList )  return;
			for (var i:int = 0; i < _uploadItemsList.length; i++) 
			{
				if(FolderManager.instance.isTempFolder( _uploadItemsList[i].photoVo.folderName ))
				{
					_uploadItemsList[i].photoVo.folderName = newFolderName;
				}
			}
		}
		
		
		/**
		 * ------------------------------------ RETRIEVE TEMP UPLOAD ITEM for photoID -------------------------------------	
		 */
		
		/**
		 * Get photo upload item by photo id
		 */
		public function getPhotoUploadItemByPhotoId( photoId : String ):PhotoUploadItem
		{
			for (var i:int = 0; i < _uploadItemsList.length; i++) 
			{
				if(_uploadItemsList[i].photoVo.id == photoId) return _uploadItemsList[i];
			}
			return null;
		}
		
		/*
		public function getTempFileRef( photoId : String ):FileReference
		{
			for (var i:int = 0; i < uploadItemsList.length; i++) 
			{
				if(uploadItemsList[i].photoVo.id == photoId) return uploadItemsList[i].fileRef;
			}
			return null;
		}
		*/
		
		
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function updateView():void
		{
			// TODO : maybe create tabs here to allow modify albums while upload
			// texts
			ResourcesManager.setText(title, "upload.panel.title");
			updateDetailLabel();
		}
		
		/**
		 *
		 */
		private function onUploadButtonClick(e:MouseEvent = null):void
		{
			Debug.log("PhotoUploader > onUploadButtonClick");
			browse();
		}
		
		
		/**
		 * update photos 
		 */
		private function onViewButtonClick(e:MouseEvent = null):void
		{
			Debug.log("PhotoUploader > onViewButtonClick");
			TabManager.instance.open(true); // force tab opening
			SessionManager.instance.updateImageList();
			close();
		}
		
		
		
		
		
		/**
		 *
		 */
		/*
		private function cleanView(e:MouseEvent = null):void
		{
			if(!uploadItemsList) return;
				
			var item:ImageUploadItem
			// remove all item from display list
			for (var i:int = 0, l:int= uploadItemsList.length; i <l ; i++) 
			{
				item = uploadItemsList[i];
				if(item.completed){
					TweenMax.killTweensOf(item); // be sure to kill possible animation on item
					item.destroy();
					item.parent.removeChild(item);
					uploadItemsList.splice(i,1);
					i--;
					l--;	
					completedFiles --;
					totalFiles --;
				}
			}
			
			updateList();
			updateGlobalBar();
		}
		*/
		
		
		
		/**
		 * ------------------------------------ CREATE UPLOAD ITEMS AND ADD THEM TO UPLOAD LIST -------------------------------------	
		 */
		
		
		/**
		 * create upload items with filereferences / files 
		 */
		private function createUploadItems ( fileList : Array ):void
		{
			Debug.log("PhotoUploader.createUploadItems");
			
			// security
			if(!fileList) {
				Debug.warn("PhotoUploader.createUploadItems : no file list");
				return;
			}
			
			//save last used folder
			CONFIG::offline
			{
				try
				{
					Infos.lastUserBrowsePath = fileList[0].parent.nativePath;
				}
				catch(e:Error)
				{
					Infos.lastUserBrowsePath = "";
					Debug.warnAndSendMail("PhotoUploadManager.createUploadItems trying to save last used folder.");
				}
			}
			
			Debug.log(">> num files : "+fileList.length);
			
			
			// create new items
			var uploadItem : PhotoUploadItem;
			var doublonsList:Array = new Array();
			try
			{
				var fileToUpload : FileReference;
				var filePath : String;
				var fileName : String;
				for (var i:int = 0, l:int = fileList.length; i < l; i++) 
				{
					Debug.log("PhotoUploader > new uploadItem "+i+"/"+l);// : "+ fileToUpload.size); // this "size" property can lead to 2038 issues...
					
					
					// FOR offline, check for doublons
					CONFIG::offline
					{
						filePath = (fileList[i] as File).nativePath;
						if(SessionManager.instance.imageAlreadyExists( filePath )) {
							fileName = (fileList[i] as File).name;
							
							// if we import in frame, we do replace frame photo by the one we already have 
							if(fileList.length == 1 && waitingFrameVo)
							{
								var existingPhotoVo : PhotoVo = SessionManager.instance.getExistingPhotoVoByOriginUrl( filePath );
								FrameVo.injectPhotoVoInFrameVo(waitingFrameVo,existingPhotoVo);
								DataLoadingManager.instance.hideLoading();
								EditionArea.Refresh();	
								return;
							}
							
							// else add to doublon list
							else doublonsList.push("" + fileName);
							continue ;
						}
					}
					
					// create new item
					fileToUpload = fileList[i];
					uploadItem = new PhotoUploadItem( fileToUpload, folderToUse, waitingFrameVo );
					uploadItem.queueIndex = _uploadItemsList.length;
					_uploadItemsList.push(uploadItem);
				}
			}
			catch(e:Error)
			{
				Debug.warnAndSendMail("PhotoUploadManager.createUploadItems error in loop : " + e.toString());
			}
			
			
			// show doublons warning
			if(doublonsList.length > 0)
			{
				var doublonTitle : String = ResourcesManager.getString("popup.doublons.warning.title"); //"Doublons";
				var doublonDesc : String = ResourcesManager.getString("popup.doublons.warning.description"); //"Some files already exist in the editor, we didn't reimport them.<br />"; 
				var doublonsListString:String = "";
				var l : int = (doublonsList.length >5)? 5 : doublonsList.length;
				for (var j:int = 0; j < l; j++) 
				{
					doublonsListString += "- " +doublonsList[j];
					if(j<l-1) doublonsListString += "<br />";
				}
				if (doublonsList.length >5)doublonsListString += "<br />...";
				
				PopupAbstract.Alert(doublonTitle,doublonDesc + "<br />" + doublonsListString,false, null);
			}
			
			
			DataLoadingManager.instance.hideLoading();
			
			//if(!isOpen) open(null); // TODO : check if it does not break
			
			// we refresh image list wit new temp files
			SessionManager.instance.refreshImageList();
			
			
			
			// process queue
			processQueue();
			
			destroyFileRefList();
			
			// notify list update
			QUEUE_UPDATE.dispatch(null);
		}
		
		
		
		/**
		 * ------------------------------------ PROCESS UPLOAD QUEUE -------------------------------------	
		 */
		
		
		/**
		 *  Process queue
		 *  > first we need to import items
		 *  > then we can upload items
		 */
		private function processQueue():void
		{
			Debug.log('PhotoUploadManager.processQueue : import ('+ importIndex +') upload ('+ uploadIndex +')');
			
			// import 1 item at a time
			if(importIndex < _uploadItemsList.length)
			{
				var item : PhotoUploadItem = _uploadItemsList[importIndex];
				if(item.currentState == PhotoUploadItem.STATE_READY){
					item.ABORD.addOnce(itemAbord);
					item.IMPORTED.addOnce(itemImportComplete);
					item.StartImport();	
				}
			}
			
			
			// upload multiple items
			if( uploadIndex < _uploadItemsList.length )
			{
				for (var i:int = 0; i < maxUploadingItems; i++) // 2 item upload max at the same time
				{
					if(uploadIndex+i < _uploadItemsList.length){
						item = _uploadItemsList[uploadIndex+i];
						
						// case item is imported, we can start upload
						if(item.currentState == PhotoUploadItem.STATE_IMPORTED){
							item.COMPLETE.addOnce(itemUploadCompleted);
							item.ABORD.addOnce(itemAbord);
							item.StartUpload();
						}
						
						// case item was aborded, we skip it and continue
						else if(item.currentState == PhotoUploadItem.STATE_ABORDED){
							uploadIndex ++;
							processQueue();
							return;
						}
					}
				}
			} 
			
			// queue completed
			else  
			{
				Debug.log("Upload Queue completed");
				
				// close view
				close();
				
				// show aborded itesm warning
				if(abordedItems.length >0) {
					Debug.warn("PhotoUploadManager queue over : Error while importing images ("+abordedItems.length+") : "+abordedItems);
					var pTitle : String = ResourcesManager.getString("popup.warning.import_complete.title"); //"Import error";
					var pDesc : String = ResourcesManager.getString("popup.warning.import_complete.description" ) ;//Some photos (@@num@@) were not correcly imported.";
					pDesc = pDesc.split("@@num@@").join(""+abordedItems.length);
					PopupAbstract.Alert(pTitle, pDesc, false );
				}
				
				// reset aborded items
				abordedItems = new Array();
				
				// THis is important to refresh to be sure tooltips are availeble for images at the end of all uploads
				if(Infos.IS_DESKTOP)
					SessionManager.instance.refreshImageList();
			}
			
		}
		
		/**
		 * on item import complete, increase import index and process queue
		 */
		private function itemImportComplete(item:PhotoUploadItem):void
		{
			Debug.log("PhotoUploadManager.itemImportComplete : "+item.fileName);
			importIndex ++;
				
			// continue to process queue
			processQueue();
			
		}
		/**
		 * on item upload completed, increase upload index and process queue
		 */
		private function itemUploadCompleted(item:PhotoUploadItem):void
		{
			Debug.log("PhotoUploadManager.itemUploadCompleted : "+item.fileName);
			if(tempCompletedItemList) tempCompletedItemList.push(item);
			uploadIndex ++;
			
			// update list position (put at end finished items
			//renderList();
			
			// start new load
			processQueue();
			
			// notify list update
			QUEUE_UPDATE.dispatch(item);
		}
		/**
		 * on item upload cancel
		 */
		private function itemAbord(obj:PhotoUploadItem):void
		{
			Debug.log("PhotoUploadManager > itemAbord : "+obj.fileName);
			
			if(obj.currentState < PhotoUploadItem.STATE_UPLOADING) importIndex ++;
			else uploadIndex ++;
			
			// set state for this item
			obj.currentState = PhotoUploadItem.STATE_ABORDED;
			
			// push to aborded item list
			abordedItems.push(obj.photoVo.name);
			
			// update list position
			//renderList();
			
			// continue queue
			processQueue();
			
			// notify list update
			QUEUE_UPDATE.dispatch(obj);
		}
		
		
		/**
		 *	Inject uploaded item inside waiting frame vo
		 */
		/*
		private function injectPhotoInFrameVo( obj : PhotoUploadItem ):void
		{
			var newPhotoVo : PhotoVo = obj.photoVo;
			
			// update frame view ! if frame is currently on screen, render it
			// We need to receive new photo id from uppload service to be sure this doesn't happen
			var visibleFrame : Frame = EditionArea.getVisibleFrameByFrameVo(obj.frameVo);
			if(visibleFrame) 
			{
				(visibleFrame as FramePhoto).update( newPhotoVo );
			} 
			else // frame is not on screen, we just update framevo 
			{ 
				FrameVo.injectPhotoVoInFrameVo( obj.frameVo, newPhotoVo );
			}
			
			
			// update list
			SessionManager.instance.updateImageList(); // update frame list (would be much easier if we had the id of this fucking frame...)
			PageNavigator.RedrawPageByFrame(obj.frameVo);
			
			// force unstick to be sure the item is selectable again
			TransformTool.unstick();
		}
		*/
		
		
		
		
		
		
		
		
			/*
		/**
		 * on item upload cancel
		 */
			/*
		private function addItem(obj:PhotoUploadItem):void
		{
			uploadContainer.update();
			
			// update list position
			renderList();
		}
			*/
		
		/**
		 * update global bar progress
		 */
		/*
		private function updateGlobalBar(obj:ImageUploadItem = null):void
		{
			completedFiles ++;
			globalProgressBar.value = completedFiles/totalFiles;
			globalProgressLabel.text = "" + completedFiles + "/" + totalFiles + " completed";
		}
		*/
		
		
		/**
		 * REMOVE ITEM FROM LISTS
		 */
		/*
		private function removeItemFromList(obj:PhotoUploadItem, isAbord:Boolean = false):void
		{
			Debug.log("PhotoUploadManager > removeItemFromList : " + obj.fileName);
			//removeItemFromImportQueue(obj);
			removeItemFromUploadQueue(obj);
			// remove item from display list
			for (var i:int = 0, l:int= uploadItemsList.length; i <l ; i++) 
			{
				// remove it
				/ *
				if(obj == uploadItemsList[i]){
					obj.destroy();
					uploadItemsList.splice(i,1);
					i--;
					l--;
				}
				* /
				//put it at end
				if(obj == uploadItemsList[i]){
					obj.setComplete(isAbord);
					uploadItemsList.splice(i,1);
					i--;
					l--;
				}
			}
			uploadItemsList.push(obj);
			
			
		}
		*/
	
	
		/*
		private function removeItemFromImportQueue(obj:PhotoUploadItem):void
		{
			for (var i:int = 0, l:int= importQueue.length; i <l ; i++) 
			{
				if(obj == importQueue[i]){
					importQueue.splice(i,1);
					i--;
					l--;
				}
			}
		}
		*/
		
		/*
		private function removeItemFromUploadQueue(obj:PhotoUploadItem):void
		{
			for (var i:int = 0, l:int= uploadQueue.length; i <l ; i++) 
			{
				if(obj == uploadQueue[i]){
					uploadQueue.splice(i,1);
					i--;
					l--;
				}
			}
		}
		*/
		
		
		
		/**
		 * update list position
		 */
		/*
		private function renderList():void
		{
			// remove item from display list
			var uploadItem : PhotoUploadItem ;
			var posY:int;
			for (var i:int = 0, l:int= uploadItemsList.length; i <l ; i++) 
			{
				uploadItem = uploadItemsList[i];
				posY = i* (uploadItem.height+5);
				
				if(uploadItem.y != posY){
					TweenMax.killTweensOf(uploadItem);
					TweenMax.to(uploadItem, .7, {y:posY});
				}
				
			}
			// update container
			TweenMax.delayedCall(1, updateComplete);
			updateDetailLabel();
		}
		*/
		
		/**
		 * Update detail label at bottom of upload panel
		 */
		private function updateDetailLabel():void
		{
			// update label
			var detailText:String;
			if( numUploadLeft>0 ) detailText = ""+ numUploadLeft + " " + ResourcesManager.getString("upload.panel.detail.filesInQueue") + " ("+completionPercent+"%)";
			else detailText = ResourcesManager.getString("upload.panel.detail.noFiles");
			
			ResourcesManager.setTextLiteral(detailLabel, detailText);
		}
		
		/**
		 *
		 */
		private function updateComplete():void
		{
			uploadContainer.update();
		}
		
		
	}
}

internal class SingletonEnforcer{}