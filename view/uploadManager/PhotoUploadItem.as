/***************************************************************
PROJECT 		: Tic Tac
COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
YEAR			: 2013
****************************************************************/
package view.uploadManager
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	
	import be.antho.utils.MathUtils2;
	
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	CONFIG::offline
	{
		import flash.filesystem.File;
	}
	import flash.geom.Matrix;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import be.antho.utils.MathUtils2;
	
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	
	import jp.shichiseki.exif.ExifInfo;
	import offline.manager.LocalStorageManager;
	import org.osflash.signals.Signal;
	
	import utils.Debug;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FramePhoto;
	import view.edition.TransformTool;
	import view.edition.pageNavigator.PageNavigator;
	import service.ServiceManager;
	import utils.BitmapUtils;
	//import offline.BitmapScaler;
	import be.antho.data.ResourcesManager;
	import flash.events.ErrorEvent;

	
	
	/**
	 * Photo upload item is a mix between a vo and a view.
	 * > It actually store the upload informations
	 * > it's also the view used 
	 */
	public class PhotoUploadItem
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// signals
		public const IMPORTED : Signal = new Signal(PhotoUploadItem); 	// is imported, bitmapdata created and created on db ( = quick process done and we can use it in app )
		public const COMPLETE : Signal = new Signal(PhotoUploadItem);	// is encoded in jpeg and sent to server ( = slow process, will update photo vo when it's done )
		public const ABORD : Signal = new Signal(PhotoUploadItem);		// item aborded (error)
		public const TEMP_WORKING_IMAGE_LOADED : Signal = new Signal()	// dispatch loaded event when temp working image is available to use
		public const TEMP_WORKING_IMAGE_ERROR : Signal = new Signal( String )	// dispatch when error while loading working image
		
		// private signals
		private const IMAGE_LOAD_ERROR : Signal = new Signal(String);
		private const IMAGE_LOAD_PROGRESS : Signal = new Signal(Number);
		private const IMAGE_LOAD_COMPLETE : Signal = new Signal();
			
		// State
		public static const STATE_READY : int = 0;
		public static const STATE_IMPORTING : int = 1;
		public static const STATE_IMPORTED : int = 2;
		public static const STATE_UPLOADING : int = 3;
		public static const STATE_UPLOADED : int = 4;
		public static const STATE_COMPLETED : int = 5;
		public static const STATE_ABORDED : int = 6;
		
		
		// public datas
		public var currentState:int = 0;
		public var fileName : String;
		public var queueIndex : int;
		
		private var fileRef : FileReference;	// the fileref use to import bitmap
		
		// bitmap datas
		private var thumbBitmapData : BitmapData; // storage during upload time of the thumb bitmap data
		private var workingBitmapData : BitmapData; // storage during upload time of the working bitmap data (only on request)
		private var fullSizeBitmapData : BitmapData; // temp storage of fullsize bitmap data to create working bitmap or thumb bitmap.
		
		// private const
		private const MAX_PHOTO_WIDTH : Number = 3100; // photo imported with higher size will be redrawn
		private const MAX_PHOTO_HEIGHT : Number = 3100; // photo imported with higher size will be redrawn
		private const ENCODING_QUALITY : Number = 79; // used in previous version, I suppose it was the best compression
		private static var tempCount:int = 0;
		
		private var retryCount:Number = 0;
		
		// data
		private var linkedFrameVo : FrameVo;	// frame vo linked to this upload item (if using import button in the frame )
		private var newPhotoVo : PhotoVo;		// the newly created photo vo ( temp photo until upload is completed )
		private var finalPhotoId : String;		// the final photo id that will be used for this image
		private var statusLabel : String;
		
		
		private var uploadUrl : String;							// url where file will be uploaded
		private var imageLoader : Loader = new Loader();		// the image importer (from local)
		private var errorMessage:String = "";
		
		private var isImporting : Boolean ; 		// the fileref is already used for import, import and upload cannot be done at the same time
		private var isUploading : Boolean ; 		// the fileref is used for upload
		private var waitingForUpload : Boolean ; 	// if upload must be done but fileref is importing, we wait untill import is done to continue upload
		
		private var exifRotation : Number; 			// rotation to apply to loaded image to fit real image rotation. 
		private var exifDate:Number; 				// Date photo taken from exif metadatas	
		
		/*
		CONFIG::offline
		{
			private static const bitmapScaler : BitmapScaler = new BitmapScaler();
		}
		*/
		
		
		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		public function PhotoUploadItem( $fileRef:FileReference, $destinationFolder:String, frameVoRef : FrameVo = null ) 
		{
			try
			{
				// datas
				linkedFrameVo = frameVoRef;
				fileRef = $fileRef;
				fileName = fileRef.name;
				
				// already create temp photovo
				newPhotoVo = new PhotoVo();
				newPhotoVo.tempID = "temp_"+(tempCount);
				newPhotoVo.name = fileName;
				newPhotoVo.folderName = $destinationFolder;

				tempCount ++;
			}
			catch(e:Error)
			{
				abordItem("PhotoUploadItem abord on constructor error : "+e.message);
			}
		}
		
		
		/**
		 * ------------------------------------ PUBLIC ACCESS -------------------------------------
		 */
		
		// start fileref import process
		public function StartImport():void { 
			startImport() 
		};
		// start fileref upload process (or local save process if offline)
		public function StartUpload():void { 
			startUpload() 
		};
		// retrieve temp working image if image has not finished the upload process
		public function LoadTempWorkingImage():void{ loadTempWorkingImage() };
		// destroy view and datas
		//public function Destroy():void{ destroy() };
		
		
		/**
		 * ------------------------------------ GETTER/SETTER -------------------------------------
		 */
		
		/**
		 * retrieve linked frame vo if there is one (upload direct in a frame)
		 */
		public function get frameVo():FrameVo
		{
			return linkedFrameVo;
		}
		
		/**
		 * retrieve temp photoVo created
		 */
		public function get photoVo():PhotoVo
		{
			return newPhotoVo;
		}
		
		/**
		 * send working bitmap data (if we have one, otherwise, send thumb data
		 */
		public function getWorkingBitmapDataClone():BitmapData
		{
			if( workingBitmapData ) return workingBitmapData.clone();
			else return getThumbBitmapDataClone();
		}
		
		/**
		 * send working bitmap data (if we have one, otherwise, send thumb data
		 */
		public function getThumbBitmapDataClone():BitmapData
		{
			if(thumbBitmapData) return thumbBitmapData.clone();
			return null;
		}
		
		
		/**
		 * ------------------------------------ IMPORT  -------------------------------------------------
		 * ------------------------------------ STEP1 : IMPORT IMAGE -------------------------------------		 
		 */
		
		private function startImport():void
		{
			Debug.log("PHotoUploadItem.StartImport :" + fileRef.name);
			
			// security
			if(currentState > STATE_READY){
				Debug.warn("PHotoUploadItem.StartImport : item can't be imported because state is : "+currentState);
				return;
			}
			
			// make file size check before
			try
			{
				if(fileRef.size > 29000000)
				{
					abordItem("File is too big");
					return;
				}
			}
			catch(e:Error)
			{
				Debug.warn("PhotoUploadItem.startImport : fileref size 2038 error catch");
			}
			
			// init
			currentState = STATE_IMPORTING;
			
			// reset retries
			retryCount = 0;
			
			// step 1 load thumb and verify image
			loadImageData();
		}
		private function loadImageData():void
		{
			Debug.log("PhotoUploadItem.loadImageData : "+ fileName);
			
			IMAGE_LOAD_COMPLETE.add(loadUserImageSuccess);
			IMAGE_LOAD_ERROR.add(loadUserImageError);
			IMAGE_LOAD_PROGRESS.add(loadUserImageProgress);
			
			// import image
			importFileRefImage();
			
			// update status
			statusLabel = ResourcesManager.getString("loading.upload.import") + "0%" ;
		}
		private function loadUserImageError( msg : String):void
		{
			disposeImageLoadSignals();
			abordItem( msg );
		}
		private function loadUserImageProgress( pct : Number):void
		{
			statusLabel = ResourcesManager.getString("loading.upload.import") + pct.toFixed(1) + "%"
		}
		private function loadUserImageSuccess( ):void
		{
			disposeImageLoadSignals();
			
			// security
			if(!fullSizeBitmapData){
				abordItem("PhotoUploadItem.loadImageSuccess error no fullsizeBitmapData");
				return;
			}
			
			// create thumb
			// OFFLINE USE WORKER FOR BITMAP SCALE
			CONFIG::offline
			{
				//
				onThumbCreatedHandler( BitmapUtils.createThumbData(fullSizeBitmapData) );
				//bitmapScaler.ScaleBitmap(fullSizeBitmapData, BitmapUtils.THUMB_MAX_WIDTH, BitmapUtils.THUMB_MAX_HEIGHT,0,onThumbCreatedHandler,abordItem);
			}
			
			// ONLINE USE SIMPLE FUNCTION FOR BITMAP GENERATION
			CONFIG::online
			{
				onThumbCreatedHandler( BitmapUtils.createThumbData(fullSizeBitmapData) );
			}
		}
		private function onThumbCreatedHandler( thumbData:BitmapData ):void
		{
			// store thumb data
			thumbBitmapData = thumbData;
			
			// save image size
			newPhotoVo.width = fullSizeBitmapData.width;
			newPhotoVo.height = fullSizeBitmapData.height;
			
			// photo is imported but not yet uploaded, make it available for use
			newPhotoVo.temp = true;
			
			// dispose fullsize (only for online, for offline we keep fullsizedata in memory to avoid having to load it twice in a really short time delay :import&upload )
			//if(!Infos.IS_DESKTOP) disposeFullSizeData(); // TODO reset this 
			disposeFullSizeData(); 
			
			
			// step 2 create place holder on server
			prepareServerImage();
			
		}
		private function disposeImageLoadSignals() : void
		{
			IMAGE_LOAD_COMPLETE.removeAll();
			IMAGE_LOAD_ERROR.removeAll();
			IMAGE_LOAD_PROGRESS.removeAll();
		}
		
		
		/**
		 * ------------------------------------ IMPORT STEP2 : PREPARE PLACE HOLDER ON SERVER -------------------------------------
		 * Create place holder for image on server
		 * > get new photo server name
		 */
		
		private function prepareServerImage():void
		{
			CONFIG::online
			{
				// before sending image we create it on db (serverside)
				statusLabel = ResourcesManager.getString("upload.file.server.creation");
				ServiceManager.instance.createNewPhotoOnDB(fileRef.name, newPhotoVo.folderName, serverImageCreatedHandler,serverImageCreateFail)	
			}
			
			/*
			CONFIG::offline
			{
				// before sending image we create it on db (local)
				statusLabel = ResourcesManager.getString("offline.local.image.creation");
				var f : File = (fileRef as File);
				if(!f) { abordItem("Item aborded as there is no file reference"); return };
				if(!exifDate)exifDate = fileRef.creationDate.time;
				LocalStorageManager.instance.createNewTempImage(fileRef.name, newPhotoVo.folderName, newPhotoVo.width, newPhotoVo.height, f.nativePath, exifDate, fileRef.modificationDate.time, serverImageCreatedHandler, serverImageCreateFail);
			}
			*/
				
		}
		private function serverImageCreateFail( errorMessage : String):void
		{
			Debug.warn("PhotoUploadItem > serverImageCreateFail "+ errorMessage );
			abordItem(errorMessage);
		}
		private function serverImageCreatedHandler($result : Object):void
		{
			try // xml result in a try catch in case of wrong xml format
			{
				var result:XML = new XML( $result);
				if(result && result.id != null)
				{
					finalPhotoId = ""+result.id; // do not put it in new photo vo yet as id is a condition to know that the import process is not yet done
					var w : Number = newPhotoVo.width;
					var h : Number = newPhotoVo.height;
					newPhotoVo.fillFromXML(result);
					newPhotoVo.width = w;
					newPhotoVo.height = h;
					
					// update label			
					statusLabel = ResourcesManager.getString("upload.file.imported");
					// update frame
					if(frameVo) injectInWaitingFrameVo();
					
					
					// notify import complete
					currentState = STATE_IMPORTED;
					IMPORTED.dispatch( this );
				}
				else
				{
					Debug.warn("Error while serverside image creation : " + $result);
					abordItem();
				}
			}
			catch (e:Error)
			{
				abordItem("Server xml error : "+e.toString());
			}
		}
		
		
		/**
		 * ------------------------------------ UPLOAD STATE -------------------------------------
		 * ------------------------------------ UPLOAD STEP1 : UPLOAD IMAGE -------------------------------------		 
		 */
		
		private function startUpload():void
		{
			Debug.log("PHotoUploadItem.startUpload :" + fileRef.name);
			
			// security
			if(currentState > STATE_IMPORTED){
				Debug.warn("PHotoUploadItem.StartUpload : item can't be uploaded because state is : "+currentState);
				return;
			}
			
			// init
			currentState = STATE_UPLOADING;
			retryCount = 0;
			
			
			if(!Infos.IS_DESKTOP){
				if(!Debug.DO_NOT_UPLOAD_PHOTOS) sendImage();
			}
			//else saveImageLocally(); // do not use photoUploadItem anymore in offline
		}
		
		
		
		/**
		 * ------------------------------------ ONLINE UPLOAD => TO SERVER -------------------------------------	
		 */
		
		
		private function sendImage():void
		{
			Debug.log("PhotoUploadItem.sendImage : ");
			Debug.log(" >> img = "+ newPhotoVo.id);
			
			// update status
			statusLabel = ResourcesManager.getString("upload.file.send");
			
			// state
			currentState = STATE_UPLOADING;
			
			// check if file is not too big, otherwise abord it with message
			try
			{
				if(fileRef.size > 29000000)
				{
					abordItem("File is too big");
					return;
				}
			}
			catch(e:Error)
			{
				Debug.warn("PhotoUploadItem.sendImage : fileref size 2038 error catch");
			}
			
			// create upload url
			uploadUrl = Infos.config.serverNormalUrl +"/"+ Infos.config.getSettingsValue("ImageUploadPage") + "?img="+ newPhotoVo.id + "&sid=" + Infos.session.id;
			Debug.log("PhotoUploadItem > send image to : " + uploadUrl);
			
			// security
			if(isImporting){ // if fileref is used for import, we wait
				waitingForUpload = true;
				return;
			} 
			
			waitingForUpload = false;
			isUploading = true;
			
			// start upload
			fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, sendComplete);
			fileRef.addEventListener(ProgressEvent.PROGRESS, sendProgress);
			fileRef.addEventListener(IOErrorEvent.IO_ERROR, sendError);
			
			// try upload
			tryUpload();
		}
		/**
		 * try upload (we put it in a try catch and with 3 retries)
		 */
		private function tryUpload():void
		{
			Debug.log("PhotoUploadItem.tryUpload");
			try	{
				fileRef.upload(new URLRequest(uploadUrl), "uploadfile",false);
			}
			catch(e:Error)
			{
				Debug.warnAndSendMail("PhotoUploadItem > error with fileRef.upload : " + e.message); 
				abordItem("upload error");
			}
		}
		private function sendError(e:IOErrorEvent):void
		{
			Debug.warn("PhotoUploaded > sendError ("+fileName+") >" + e.text);
			retryCount ++;
			if( retryCount == 3) abordItem(e.text);
			else {
				Debug.log("-> Retry send : "+retryCount);
				tryUpload();
			}
		}
		private function sendProgress(e:ProgressEvent):void
		{
			var pct : Number = e.bytesLoaded/e.bytesTotal;
			statusLabel = ResourcesManager.getString("upload.file.send") + " " + (pct*100).toFixed(1) + "%";
		}
		private function sendComplete(e:Event = null):void
		{
			Debug.log("PhotoUploadItem > sendComplete > Result is : ");
			isUploading = false;
			var result : String =  (e as DataEvent).text;
			Debug.log(" >> "+result);
			// check if image load was successful
			if(result.indexOf("*") != -1){
				abordItem("Photo uploaded error : "+ result);
				return;
			}
			
			// completed
			onUploadProcessComplete();
		}
		
		
		
		/**
		 * ------------------------------------ OFFLINE UPLOAD => TO USER FOLDER -------------------------------------	
		 */
		/*
		private function saveImageLocally():void
		{
			Debug.log("PHotoUploadItem.saveImageLocally :" + fileRef.name);
			
			// init
			currentState = STATE_UPLOADING;
			retryCount = 0;
			Debug.log(" >> img = "+ newPhotoVo.id);
			
			// update status
			statusLabel = ResourcesManager.getString("offline.local.thumb.creation");
			
			// security
			if(isImporting){ // if fileref is used for import, we wait
				currentState = STATE_IMPORTING;
				waitingForUpload = true;
				return;
			} 
			
			waitingForUpload = false;
			isUploading = true;
			
			// if fullsize bitmap data is not available, reload it
			if(!fullSizeBitmapData)
			{
				// load fileRef image int fullsize bitmap data (again)
				IMAGE_LOAD_COMPLETE.addOnce(saveImageLocally);
				IMAGE_LOAD_ERROR.addOnce(abordItem);
				
				// import image
				importFileRefImage()
				return;
			}
			
			// clean possible image import/load signals
			disposeWorkingImageSignals();
			
			// generate working image and save image files on user desktop
			createWorkingImage(function(bitmapData:BitmapData):void
			{
				workingBitmapData = bitmapData;
				
				/////////////// ERROR SIMULATION ////////////////
				if(Debug.SIMULATE_IMPORT_ERRORS)
				{
					if( Math.round(Math.random() *4) == 2 )
					{
						// simulate error
						abordItem("ABORD ITEM SIMULATION");
						return;
					}
				}
				/////////////// END ERROR SIMULATION ////////////////
				
				
				LocalStorageManager.instance.saveImageFiles( newPhotoVo.id, thumbBitmapData,workingBitmapData, fullSizeBitmapData, localImageSaveComplete, localImageSaveError );
			});
		}
		private function localImageSaveComplete():void
		{
			/*
			Debug.log("PhotoUploadItem.localImageSaveComplete");
			
			// now we save image in image list
			LocalStorageManager.instance.writeTempImageToImageList( finalPhotoId, 
				// ON SUCCESS
				function(result:String):void{
					onUploadProcessComplete();
				},
				// ON ERROR
				function(e:ErrorEvent):void{
					abordItem("PhotoUploadItem.localImageSaveComplet : error on write temp image : "+e.toString())
				});
			* /

		}
		*/
		private function localImageSaveError( error : ErrorEvent ):void
		{
			/*
			Debug.log("PhotoUploadItem.localImageSaveError");
			abordItem("=> LocalImage files saved error : "+ error.toString());
			*/
		}
		
		/**
		 * Upload has completed
		 */
		private function onUploadProcessComplete():void
		{
			Debug.log("PhotoUploadItem.onUploadProcessComplete");
			
			isUploading = false;
			
			if(Infos.IS_DESKTOP) statusLabel = ResourcesManager.getString("offline.local.thumb.completed");
			else statusLabel = ResourcesManager.getString("upload.file.completed");
			
			
			// change state
			currentState = STATE_UPLOADED;
			
			// remove photoVo temp values
			newPhotoVo.temp = false;
			newPhotoVo.tempID = null;
			
			// check if this photovo is on the current page, if so, redraw the frame photo
			var framesToRefresh : Vector.<FramePhoto> = EditionArea.getVisibleFramesByPhotoVo(newPhotoVo);
			for (var i:int = 0; i < framesToRefresh.length; i++) 
			{
				framesToRefresh[i].refresh();
			}
			
			// notify complete
			COMPLETE.dispatch(this);
			
			// dispose item
			dispose(); // TODO : check if this is ok here, it was never called :(
		}
		
		
		/**
		 * ------------------------------------ OFFLINE UPLOAD => TO USER FOLDER -------------------------------------	
		 */
		

		/**
		 * Import image from local computer to use as working image in editor
		 */
		private function loadTempWorkingImage():void
		{
			Debug.log("PhotoUploadItem > LoadTempWorkingImage");
			if(fileRef) Debug.log(" > file : "+fileRef.name); // security
			
			// if working image already loaded, just use it
			if(workingBitmapData){
				TEMP_WORKING_IMAGE_LOADED.dispatch();
				return;
			}
			
			// if fullsize image data is still available
			if(fullSizeBitmapData)
			{
				workingImageLoadSuccess();
				return;
			}
			
			// load fileRef image
			IMAGE_LOAD_COMPLETE.add(workingImageLoadSuccess);
			IMAGE_LOAD_ERROR.add(workingImageLoadError);
			IMAGE_LOAD_PROGRESS.add(workingImageLoadProgress);
			
			// import image
			importFileRefImage();
			
		}
		private function workingImageLoadError( msg : String):void
		{
			disposeWorkingImageSignals();
			
			// working image not available (probably due to currently uploading so impossible to load it from local), try to use thumbdata
			if(thumbBitmapData) TEMP_WORKING_IMAGE_LOADED.dispatch();
			else TEMP_WORKING_IMAGE_ERROR.dispatch(msg); 
			
			//abordItem( msg ); // TODO : what do we do if working image load fails
		}
		private function workingImageLoadProgress( pct : Number):void
		{
			statusLabel = ResourcesManager.getString("loading.upload.import") + pct.toFixed(1) + "%"
		}
		private function workingImageLoadSuccess( ):void
		{
			disposeWorkingImageSignals();
			
			// security
			if(!fullSizeBitmapData){
				abordItem("PhotoUploadItem> workingImageLoadSuccess > error no fullsizeBitmapData");
				return;
			}
			
			// create working image
			createWorkingImage( function(bitmapData:BitmapData):void
			{
				workingBitmapData = bitmapData;
				
				// dispose fullsize
				disposeFullSizeData();
				
				//notify
				TEMP_WORKING_IMAGE_LOADED.dispatch();
			});
		}
		private function disposeWorkingImageSignals() : void
		{
			IMAGE_LOAD_COMPLETE.remove(workingImageLoadSuccess);
			IMAGE_LOAD_ERROR.remove(workingImageLoadError);
			IMAGE_LOAD_PROGRESS.remove(workingImageLoadProgress);
		}
		private function createWorkingImage( onSuccessBitmapData:Function ):void
		{
			// working already exists
			if(workingBitmapData) {
				onSuccessBitmapData(workingBitmapData);
				return;
			}
			
			// no fullsize, return
			if( !fullSizeBitmapData ) return;
			
			// create working image based on fullsizeBitmapdata
			var fullWidth : Number = fullSizeBitmapData.width;
			var fullHeight : Number = fullSizeBitmapData.height;
			var maxWorkingWidth:int  = 720 ;
			var maxWorkingHeight:int  = 720 ;
			
			// OFFLINE USE WORKER FOR BITMAP SCALE
			CONFIG::offline
			{
				//bitmapScaler.ScaleBitmap(fullSizeBitmapData, maxWorkingWidth, maxWorkingHeight,0,onSuccessBitmapData,abordItem);
				onSuccessBitmapData( BitmapUtils.resizeBitmapData(fullSizeBitmapData, maxWorkingWidth, maxWorkingHeight) );
			}
			
			// ONLINE USE SIMPLE FUNCTION FOR BITMAP GENERATION
			CONFIG::online
			{
				onSuccessBitmapData( BitmapUtils.resizeBitmapData(fullSizeBitmapData, maxWorkingWidth, maxWorkingHeight) );
			}
			
		}
		
		
		
		/**
		 * ------------------------------------ IMPORT/LOAD IMAGE FROM DISK -------------------------------------	
		 */
		
		
		/**
		 * load local image into memory
		 * > import bytes from fileRef
		 * > load bytes as bitmap
		 * > notify when bitmapdate is ready
		 * > clean after action
		 */
		private function importFileRefImage():void
		{
			// load fileref content
			try{
				isImporting = true;
				
				// IF ONLINE, use filref.load to retrieve content
				CONFIG::online
				{
					fileRef.addEventListener(Event.COMPLETE, fileRefLoadSuccess);
					fileRef.addEventListener(IOErrorEvent.IO_ERROR, localImportError);
					fileRef.addEventListener(ProgressEvent.PROGRESS, localImportProgress);
					fileRef.load();
				}
				
				// IF OFFLINE, we use localStorage manager system to load image bytes async
				CONFIG::offline
				{
					LocalStorageManager.instance.loadLocalImageBytes( (fileRef as File).nativePath, imageBytesLoaded, localImportError );
				}
			}
			catch (e:Error) { IMAGE_LOAD_ERROR.dispatch("PhotoUploadItem.importFileRefImage error : " + e.toString()) }
		}
		private function localImportProgress(e:ProgressEvent):void
		{
			var pct : Number = (e.bytesLoaded/e.bytesTotal)*100;
			IMAGE_LOAD_PROGRESS.dispatch( pct );
		}
		private function localImportError(e:IOErrorEvent):void
		{
			Debug.warn("PhotoUploadItem > localImportError "+e.text );
			retryCount ++;
			if( retryCount == 3) {
				localImportDone();
				IMAGE_LOAD_ERROR.dispatch(e.text);
			}
			else {
				Debug.log("-> Retry import : "+retryCount);
				importFileRefImage();
			}
		}
		private function fileRefLoadSuccess(e:Event):void
		{
			Debug.log("PhotoUploadItem.fileRefLoadSuccess");
			var imageBytes:ByteArray = fileRef.data;
			if( imageBytes == null) { IMAGE_LOAD_ERROR.dispatch( "Bytes error..." ); return; }
			imageBytesLoaded( imageBytes );
		}
		private function imageBytesLoaded( imageBytes : ByteArray ):void
		{
			Debug.log("PhotoUploadItem.imageBytesLoaded : size = "+imageBytes.length);
			localImportDone();
			
			// TRY READ EXIF to see if we need to apply orientation update
			var exifInfo:ExifInfo ;
			var exifDetails : String = "";
			exifRotation = 0;
			try{
				exifInfo = new ExifInfo( imageBytes );
				/*
				1 = normal
				3 = rotated 180 degrees (upside down)
				6 = rotated 90 degrees CW
				8 = rotated 90 degrees CCW
				9 = unknown
				*/
				exifDetails += "image : " + fileRef.name ;
				//exifDetails += "exifInfo = " + exifInfo.ifds);
				exifDetails += " - orientation = " + exifInfo.ifds.primary["Orientation"];
				exifDetails += " - width = " + exifInfo.ifds.exif["PixelXDimension"];
				exifDetails += " - height = " + exifInfo.ifds.exif["PixelYDimension"];
				var r : Number = parseInt( exifInfo.ifds.primary["Orientation"] );
				if( !Debug.DO_NOT_USE_EXIF_ROTATION )
				{
					if(r == 3) exifRotation = 180;
					else if(r == 6) exifRotation = -90;
					else if(r == 8) exifRotation = 90;
				}
				Debug.log("Exif details : "+exifDetails);
				
				// try to get taken date from metadatas
				var exifDateString:String = ""+ exifInfo.ifds.exif["DateTimeOriginal"]; // 2014:04:13 16:18:11
				exifDateString = exifDateString.split(" ").join(":");
				var exifSplit : Array = exifDateString.split(":");
				exifDate = new Date(parseInt(exifSplit[0]),parseInt(exifSplit[1])-1,parseInt(exifSplit[2]),parseInt(exifSplit[3]),parseInt(exifSplit[4]),parseInt(exifSplit[5])).time;
				Debug.log("Exif date : "+exifDate);
			}
			catch(e:Error)
			{
				Debug.log("Exif details : "+exifDetails);
				Debug.warn("PhotoUploadItem.localImportSuccess : error while loading exif Info : "+e.toString());
			}
			
			
			imageLoader = new Loader();
			// transform data which is bytes in bitmap data
			try{
				imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, bytesLoadSuccess);	
				imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, bytesLoadError);
				imageLoader.loadBytes(imageBytes);
			}
			catch(e:Error){ IMAGE_LOAD_ERROR.dispatch( "PhotoUploadItem.imageBytesLoaded bytes load error : " + e.toString()) }
		}
		private function bytesLoadError(e:IOErrorEvent):void
		{	
			Debug.warn("PhotoUploadItem.bytesLoadError : "+e.text);
			IMAGE_LOAD_ERROR.dispatch( "Local load error" );
		}
		private function bytesLoadSuccess(e:Event):void
		{
			Debug.log("PhotoUploadItem.bytesLoadSuccess");
			var loaderInf : LoaderInfo = ( e.currentTarget ) as LoaderInfo;
			
			try
			{
				// recover bitmap
				var bm : Bitmap = (loaderInf.content as Bitmap);
				var bmd : BitmapData = (loaderInf.content as Bitmap).bitmapData;
				var newW : Number = bmd.width;
				var newH : Number = bmd.height;
				
				
				// OFFLINE BITMAP SYSTEM, BITMAP SCALE IF TOO BIG
				/*
				CONFIG::offline
				{
					bitmapScaler.ScaleBitmap(bmd,MAX_PHOTO_WIDTH, MAX_PHOTO_HEIGHT,exifRotation, fullsizeImageCreated,function(error:String):void{IMAGE_LOAD_ERROR.dispatch("BitmapScaleError : "+error)},true);
				}
				
				// ONLINE BITMAP SYSTEM, just import 
				CONFIG::online
				{
				*/
				
				// create new fullsize bitmapdata based on exif rotation
				fullSizeBitmapData = BitmapUtils.transformBitmapData( bmd, -exifRotation, 1, true );
				//succes continue
				fullsizeImageCreated(fullSizeBitmapData);
				
					// apply possible exif rotation fix
					/*
					if(exifRotation != 0)
					{
						/*
						// invert width/height if rotation is -90 or 90 
						if( exifRotation != 0 && exifRotation != 180 ){
							var tempW : int = newW;
							newW = newH;
							newH = tempW;
						}
						
						// rotation and scale matrix
						var rm : Matrix = new Matrix();
						rm.translate( -bmd.width*.5, -bmd.height*.5); // put it in center
						rm.rotate(MathUtils2.toRadian(-exifRotation)); // rotate it
						rm.translate( newW*.5, newH*.5); // put it back in center
						fullSizeBitmapData = new BitmapData( newW, newH );
						fullSizeBitmapData.draw( bmd, rm, null, null, null, true);
					}
					else fullSizeBitmapData = bmd.clone(); // otherwise use raw fullsize 
					bmd.dispose();
						*/
					//}
					
					//succes continue
					//fullsizeImageCreated(fullSizeBitmapData);
					
				//}
				
				
			}
			catch(e:Error) { 
				IMAGE_LOAD_ERROR.dispatch( "PhotoUploadItem.bytesLoadSuccess : error with loaded bitmap : "+e.toString());
				return;
			}
		}
		private function fullsizeImageCreated( bitmapData:BitmapData ):void
		{
			fullSizeBitmapData = bitmapData;
			
			// dispose data
			disposeImageLoader();
			if(fileRef.data) fileRef.data.clear();
			
			// notify ready
			IMAGE_LOAD_COMPLETE.dispatch();
			
		}
		
		
		/**
		 * when importing is over
		 */
		private function localImportDone():void
		{
			if(fileRef)
			{
				fileRef.removeEventListener(Event.COMPLETE, fileRefLoadSuccess);
				fileRef.removeEventListener(IOErrorEvent.IO_ERROR, localImportError);
				fileRef.removeEventListener(ProgressEvent.PROGRESS, localImportProgress);	
			}
			// continue upload if there was an upload in queue for this item
			isImporting = false;
			if(waitingForUpload) startUpload();
		}

		
		
		
		
		/**
		 * ------------------------------------ INJECT PHOTOVO IN CURRENT WAITING FRAME -------------------------------------	
		 */
		
		
		
		/**
		 * inject current uploaded photo inside frameVo
		 */
		private function injectInWaitingFrameVo():void
		{
			if( frameVo && newPhotoVo ) 
			{
				// update frame view ! if frame is currently on screen, render it
				// We need to receive new photo id from uppload service to be sure this doesn't happen
				var visibleFrame : Frame = EditionArea.getVisibleFrameByFrameVo(frameVo);
				
				// if frame is visible update it
				if(visibleFrame) 
				{
					// if frame has not error, just updat it
					if(!visibleFrame.hasError)
						(visibleFrame as FramePhoto).update( newPhotoVo );
					// if frame has error, recreate it
					else {
						FrameVo.injectPhotoVoInFrameVo( frameVo, newPhotoVo );
						EditionArea.Refresh();
					}
				} 
				else // frame is not on screen, we just update framevo 
				{ 
					FrameVo.injectPhotoVoInFrameVo( frameVo, newPhotoVo );
				}
				
				// redraw navigator
				PageNavigator.RedrawPageByFrame(frameVo);
				
				// force unstick to be sure the item is selectable again
				TransformTool.unstick();
			}
		}
		

		
		/**
		 * ------------------------------------ DESTROY ITEM  -------------------------------------
		 */
		
		/**
		 * Completely destroy item
		 * Actually destroy will never be called again in current system as we keep track of all uploaded files in the session.
		 * To free memory up, we call dispose when upload is finished
		 */
		private function destroy():void
		{
			Debug.log("PHotoUploadItem > destroy : " + fileName );
			dispose();
			
			// clean signals
			ABORD.removeAll();
			IMPORTED.removeAll();
			COMPLETE.removeAll();
			
			TEMP_WORKING_IMAGE_LOADED.removeAll();
			TEMP_WORKING_IMAGE_ERROR.removeAll();
		}
		
		
		
		/**
		 * ------------------------------------ ABORD ITEM  -------------------------------------
		 */
		
		
		/**
		 * abord item if error or user cancel
		 */
		private function abordItem(msg:String = "aborded"):void
		{
			if(msg.indexOf("*") != -1){
				msg = msg.substr(8);
			}
			
			//currentState = STATE_ABORDED;
			errorMessage = msg;
			
			// reset fileRef status values
			isUploading = false;
			isImporting = false;
			waitingForUpload = false;
			
			// remove temp reference
			newPhotoVo.temp = true; // keep temp to true to be sure it's still in the list of temp items to update (to show red error)
			//newPhotoVo.tempID = null;
			
			Debug.warn("PhotoUploadItem.abordItem > " + fileName + " : "+msg);
			dispose();
			
			// TODO : here, what do we do if item is aborded but was injected inside a frame
			if(frameVo){
				//
			}
			
			// dispose public events
			COMPLETE.removeAll();
			IMPORTED.removeAll();
			ABORD.dispatch(this);
		}
		
		
		
		
		/**
		 * ------------------------------------ DISPOSE  -------------------------------------
		 */
		
		
		
		/**
		 * dispose view/loader/file ref from memory
		 */
		private function dispose():void
		{
			// clean file reference
			if(fileRef){
				fileRef.removeEventListener(Event.COMPLETE, fileRefLoadSuccess);
				fileRef.removeEventListener(IOErrorEvent.IO_ERROR, localImportError);
				fileRef.removeEventListener(ProgressEvent.PROGRESS, localImportProgress);
				fileRef.removeEventListener(DataEvent.UPLOAD_COMPLETE_DATA, sendComplete);
				fileRef.removeEventListener(ProgressEvent.PROGRESS, sendProgress);
				fileRef.removeEventListener(IOErrorEvent.IO_ERROR, sendError);
				if(fileRef.data) {
					fileRef.data.clear();
				}
				fileRef.cancel();
				fileRef = null;
			}
			
			// clean image loader
			disposeImageLoader();
			
			// clean image datas
			disposeFullSizeData();
			disposeWorkingData();
			disposeThumbData();
			
			// dispose thumb signals
			disposeImageLoadSignals();
			
			//linkedFrameVo = null;
			//newPhotoVo = null; // we do not kill this as we need it as reference in the upload tooltip notification
		}
		
		
		/**
		 * dispose image loader (local load from user computer);
		 */
		private function disposeImageLoader():void
		{
			if(imageLoader)
			{
				if(imageLoader.contentLoaderInfo)
				{
					imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, bytesLoadSuccess);	
					imageLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, bytesLoadError);
					if(imageLoader.contentLoaderInfo.bytes) imageLoader.contentLoaderInfo.bytes.clear();
				}
				
				try{
					imageLoader.close();
				}
				catch(e:Error){}
				imageLoader = null;
			}
		}
		private function disposeThumbData():void
		{
			if(thumbBitmapData) {
				thumbBitmapData.dispose();
				thumbBitmapData = null;
			}
		}
		private function disposeFullSizeData():void
		{
			if(fullSizeBitmapData) {
				fullSizeBitmapData.dispose();
				fullSizeBitmapData = null;
			}
		}
		private function disposeWorkingData():void
		{
			if(workingBitmapData) {
				workingBitmapData.dispose();
				workingBitmapData = null;
			}
		}
	}
}