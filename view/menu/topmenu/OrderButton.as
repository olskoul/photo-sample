/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.menu.topmenu
{
	import be.antho.data.ResourcesManager;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mcs.OrderButton;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;

	public class OrderButton extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public const CLICK : Signal = new Signal();
		
		private var view : mcs.OrderButton = new mcs.OrderButton;
		private var label: String;
		private var _enabled: Boolean;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function OrderButton( $label : String )
		{
			addChild(view);
			view.label.autoSize = "left";
			setLabel($label);
			
			
			// listeners
			mouseChildren = false;
			addEventListener(MouseEvent.CLICK, function():void{ CLICK.dispatch()});
			addEventListener(MouseEvent.ROLL_OVER, function():void{
				
				TweenMax.killTweensOf(view.bg);
				TweenMax.killTweensOf(view.label);
				TweenMax.to(view.bg, 0, { ease:Strong.easeOut, tint: Colors.BLUE});
				TweenMax.to(view.label, 0, { ease:Strong.easeOut, tint: Colors.WHITE});
			});
			addEventListener(MouseEvent.ROLL_OUT, function():void{ 
				TweenMax.killTweensOf(view.bg);
				TweenMax.killTweensOf(view.label);
				TweenMax.to(view.bg, 0, { ease:Strong.easeOut, tint: null});
				TweenMax.to(view.label, 0, { ease:Strong.easeOut, tint: null});
			});
			addEventListener(MouseEvent.MOUSE_DOWN, function():void{
				
				TweenMax.killTweensOf(view.bg);
				TweenMax.killTweensOf(view.label);
				TweenMax.to(view.bg, 0, { ease:Strong.easeOut, tint: Colors.GREEN});
				TweenMax.to(view.label, 0, { ease:Strong.easeOut, tint: Colors.WHITE});
			});
			addEventListener(MouseEvent.MOUSE_UP, function():void{
				
				TweenMax.killTweensOf(view.bg);
				TweenMax.killTweensOf(view.label);
				TweenMax.to(view.bg, 0, { ease:Strong.easeOut, tint: null});
				TweenMax.to(view.label, 0, { ease:Strong.easeOut, tint: null});
			});
		}
		
			
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * getter setter for enabled
		 */
		public function set enabled(value : Boolean):void
		{
			_enabled = value;
			mouseEnabled = value;
			alpha = (value)?1:.5;
		}
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 *Set label 
		 */
		public function setLabel($label:String):void
		{
			ResourcesManager.setText(view.label,$label);
			view.icon.x = view.label.x + view.label.width + 5;
			view.bg.width = view.icon.x + view.icon.width + 5 - view.bg.x;
			view.shadow.width = view.bg.x + view.bg.width - 1;
		}	
		
		/**
		 *
		 */
		public function updatePrice( newValue : Number ):void
		{
			view.priceLabel.text = "" + newValue.toFixed(2) + " €";
			Debug.log("Orderbutton price updated : "+view.priceLabel.text);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
	}
}