package view.menu.topmenu
{
	import com.bit101.components.HBox;
	import com.greensock.TweenMax;
	import com.greensock.layout.AlignMode;
	
	import flash.compiler.embed.EmbeddedMovieClip;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	
	import mx.controls.ToolTip;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.tabs.TabManager;
	
	import bmp.logoBeta;
	
	import comp.DefaultTooltip;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	import comp.label.LabelTictac;
	
	import data.Infos;
	import data.ProductsCatalogue;
	import data.TooltipVo;
	import data.TutorialStepVo;
	
	import library.tutorial.icon;
	
	import manager.CanvasManager;
	import manager.CardsManager;
	import manager.CoverManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.PhotoSwapManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	import manager.TutorialManager;
	
	import mcs.icons.uploadIcon;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.KeyboardManager;
	import utils.TextFormats;
	
	import view.edition.EditionArea;
	import view.menu.lefttabs.LeftTabs;
	import view.order.OrderPanel;
	import view.photoManager.PhotoManagerModule;
	import view.uploadManager.PhotoUploadItem;
	import view.uploadManager.PhotoUploadManager;
	
	public class TopBar extends Sprite
	{
		public static const UPDATE_PROJECT:Signal = new Signal();
		public static const HEIGHT:Number = 50;
		
		private const AUTO_SAVE_DELAY : Number = 20; // in min
		private static var _instance : TopBar;
		
		// views
		private var buttonContainer: HBox;
		private var _enabled:Boolean;
		private var orderPanel : OrderPanel;
		private var threePoints:TextField;
		private var threePointsDetail:TextField;
		private var titleName:TextField;	// project name infos
		private var detailName:TextField; 	// project detail infos
		
		// buttons
		private var orderButton : OrderButton;
		private var uploadButton : uploadIcon = new uploadIcon();
		private var saveBtn:SimpleButton;
		private var saveAsBtn:SimpleButton;
		private var newBtn:SimpleButton;
		private var photoManagerBtn:SimpleButton ;
		private var openProjectBtn:SimpleButton;
		private var logoutBtn:SimpleButton;
		private var previewButton:SimpleButton;
		private var tutorialBtn:Sprite;
		private var settingsBtn:SimpleSquareButton;
		
		// autosave
		private var autoSaveTooltip : TooltipVo;
		private var autoSaveTimer : Timer;
		
		// app logo
		[Embed(source="/assets/images/newLogo.png")] 
		private var embedLogo:Class;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		public function TopBar()
		{
			super();
			
			_instance = this;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			ProjectManager.PRICE_UPDATED.add(updateOrderPrice);
			PhotoUploadManager.QUEUE_UPDATE.add(onPhotoUploadUpdate);
			PhotoSwapManager.SWAP_IMAGE_MODE.add(onPhotoSwapModeChange);
			UPDATE_PROJECT.add(updateTitle);
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTERS
		////////////////////////////////////////////////////////////////
		private function get enabled():Boolean
		{
			return _enabled;
		}

		private function set enabled(value:Boolean):void
		{
			_enabled = value;
			
			/*
			mouseEnabled = value;
			mouseChildren = value;
			buttonContainer.alpha = (value)?1:.5;
			*/
			
			// keyboard
			if(_enabled) KeyboardManager.CTRL_S.add(ProjectManager.instance.saveProject);
			else KeyboardManager.CTRL_S.remove(ProjectManager.instance.saveProject);
			
			
			saveBtn.enabled = value;
			saveAsBtn.enabled = (Infos.project && Infos.project.id)? value : false;
			orderButton.enabled = value;
			photoManagerBtn.enabled = value;
			if(!Infos.IS_DESKTOP)
			newBtn.enabled = value;
			
			/*
			if(!Infos.IS_DESKTOP && !Debug.HIDE_UPLOAD_BTN_FOR_WEB)
			uploadButton.visible = value;
			else
			*/
			uploadButton.visible = false; // do not show anymore the upload button
			
			
			
		}

		private function addedToStageHandler(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			constructChildren();
			setListeners();
		}
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * reset title, enabled and timer
		 * */
		public function update():void
		{
			//update btnLabels
			updateBtnLabels();
			resizeHandler(null);
			
			if(!ProjectManager.instance.ready)
			{
				enabled = false;
				//ProjectManager.PROJECT_SELECTED.addOnce(projectSelectedHandler)
				clearTitle();
				ProjectManager.PROJECT_READY.addOnce(updateTitle);
			}
			else
			{
				updateTitle();
				enabled = true;
				
				// start auto save timer
				resetAutoSaveTimer();
			}
			
			//test
			//throw new Error("test");
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Update label of btns
		 * Mostly when changing language
		 * */
		private function updateBtnLabels():void
		{
			saveBtn.setLabel("topbar.project.save.btn.label");
			saveAsBtn.setLabel("topbar.project.saveAs.btn.label");
			newBtn.setLabel("topbar.project.new.btn.label");
			photoManagerBtn.setLabel("topbar.photomanager.btn.label");
			openProjectBtn.setLabel("topbar.project.open.btn.label");
			
			orderButton.setLabel("topbar.order.btn.label");
			//settingsBtn.setLabel(Infos.lang,false);
			buttonContainer.draw();

		}		
		
		/**
		 * set listners
		 * */
		private function setListeners():void
		{
			Infos.stage.addEventListener(Event.RESIZE, resizeHandler);
		}
		
		/**
		 * Construct view
		 * */
		private function constructChildren():void
		{
			//logo container
			var logoContainer:Sprite = new Sprite();
			//logoContainer.buttonMode = true;
			//logoContainer.addEventListener(MouseEvent.CLICK,function():void{ProjectManager.instance.reloadWithNewProject()});
			addChild(logoContainer);
			
			//add default Logo
			if(!Infos.config.isRebranding) {
				//var logoBmp:Bitmap = new Bitmap(new bmp.logoBeta()); //library default logo
				var logoBmp:Bitmap = new embedLogo() as Bitmap; //library default logo
				logoContainer.addChild(logoBmp);
			}
			// or rebranded logo
			else 
			{
				try{
					var loader : Loader = new Loader();
					loader.alpha = 0;
					loader.load(new URLRequest(Infos.config.rebrandingLogoUrl));
					loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function():void{
						TweenMax.to(loader, 0.5, {alpha:1});
					});
					logoContainer.addChild(loader);	
				}
				catch(e:Error){Debug.warn("TopBar.constructChildren : "+e.toString())};
			}
							
			
			//addTitle type project
			threePoints = new TextField();
			threePoints.embedFonts = true;
			var textformat:TextFormat = TextFormats.ASAP_REGULAR(18,Colors.WHITE);
			textformat.align = TextFormatAlign.LEFT;
			threePoints.defaultTextFormat = textformat;
			threePoints.autoSize = "left";
			threePoints.y = 3;
			threePoints.visible = false;
			addChild(threePoints);
			
			 
			threePointsDetail = new TextField();
			threePointsDetail.embedFonts = true;
			var textformat:TextFormat = TextFormats.ASAP_REGULAR(16,Colors.WHITE);
			textformat.align = TextFormatAlign.LEFT;
			threePointsDetail.defaultTextFormat = textformat;
			threePointsDetail.autoSize = "left";
			threePointsDetail.y = 3;
			threePointsDetail.visible = false;
			addChild(threePointsDetail);
			
			//addTitle name project
			titleName = new TextField();
			titleName.embedFonts = true;
			textformat = TextFormats.ASAP_BOLD(18,Colors.WHITE);
			textformat.align = TextFormatAlign.LEFT;
			titleName.defaultTextFormat = textformat;
			titleName.selectable = false;
			titleName.multiline = false;
			titleName.autoSize = "left";
			titleName.y = 3;
			titleName.x = 100;
			titleName.text = "";
			
			titleName.addEventListener(MouseEvent.ROLL_OVER, function():void{
				DefaultTooltip.killTooltipById("titleTip");
				if(Infos.project)
				{
					var tooltipVo : TooltipVo = new TooltipVo(Infos.project.name + "\n" + detailName.text);
					tooltipVo.labelIsNotKey = true;
					tooltipVo.uid = "titleTip";
					DefaultTooltip.tooltipOnClip(titleName, tooltipVo, true, 400);
				}
			});
			titleName.addEventListener(MouseEvent.ROLL_OUT, function():void{
				if(Infos.project)
				DefaultTooltip.killTooltipById("titleTip");
			});
			
			//labelView.width = container.width			
			addChild(titleName);
			
			
			// detail name
			detailName = new TextField();
			detailName.embedFonts = true;
			detailName.antiAliasType = AntiAliasType.ADVANCED;
			textformat = TextFormats.ASAP_BOLD(12,Colors.WHITE);
			textformat.align = TextFormatAlign.LEFT;
			detailName.defaultTextFormat = textformat;
			detailName.selectable = false;
			detailName.multiline = false;
			//detailName.background = true;
			//detailName.backgroundColor = 0xff0000;
			detailName.y = 27;
			detailName.x = titleName.x;
			detailName.autoSize = "left";
			detailName.text = "";
			addChild(detailName);
			
			
			//Add Btns container
			buttonContainer = new HBox(this,0,0);
			buttonContainer.alignment = HBox.MIDDLE;
			addChild(buttonContainer);
			
			//AddButton
			saveBtn = SimpleButton.createTopBarButton("topbar.project.save.btn.label", function():void{ProjectManager.instance.saveProject()});
			saveAsBtn = SimpleButton.createTopBarButton("topbar.project.saveAs.btn.label", function():void{ProjectManager.instance.saveProjectAs()});
			newBtn = SimpleButton.createTopBarButton("topbar.project.new.btn.label",function():void{ProjectManager.instance.reloadWithNewProject()});
			photoManagerBtn = SimpleButton.createTopBarYellowButton("topbar.photomanager.btn.label",function():void{PhotoManagerModule.instance.open()});
			//previewButton = SimpleButton.createTopBarButton("topbar.project.preview.btn.label",function():void{EditionArea.goFullScreen()});
			openProjectBtn = SimpleButton.createTopBarButton("topbar.project.open.btn.label",function():void{ProjectManager.instance.openProjectExplorer()});
			logoutBtn = new SimpleSquareButton("", Colors.BLUE, function():void{ProjectManager.instance.logout()}, Colors.WHITE, Colors.GREY_DARK, Colors.WHITE, 1,false,4);
				//SimpleButton.createTopBarButton("topbar.logout.btn.label",function():void{ProjectManager.instance.logout()});
			
			logoutBtn.tooltip = new TooltipVo("tooltip.topbar.logout.button");
			newBtn.tooltip = new TooltipVo("tooltip.topbar.new.button");
			saveBtn.tooltip = new TooltipVo("tooltip.topbar.save.button");
			saveAsBtn.tooltip = new TooltipVo("tooltip.topbar.saveAs.button");
			photoManagerBtn.tooltip = new TooltipVo("tooltip.topbar.manager.button",TooltipVo.TYPE_SIMPLE,null,Colors.YELLOW,Colors.BLACK);
			openProjectBtn.tooltip = new TooltipVo("tooltip.topbar.open.button");
			//previewButton.tooltip = new TooltipVo(ResourcesManager.getString("tooltip.topbar.preview.button"));
				
			orderButton = new OrderButton("topbar.order.btn.label");
			orderButton.CLICK.add(onOrderProcessStart);

			//Settings
			
			settingsBtn = new SimpleSquareButton("", Colors.BLUE,settingsButtonClicked,Colors.WHITE,Colors.BLUE, Colors.WHITE,1,true,9);
			settingsBtn.tooltip = new TooltipVo("settings.btn.tooltip");

			//tutotial
			var tutorialIcon:icon = new library.tutorial.icon();
			tutorialBtn = ButtonUtils.makeButton(tutorialIcon,tutorialClickHandler,new TooltipVo("tooltip.topbar.tutorial.button"),Colors.BLUE);		
			
			//
			// add buttons to bar
			//
			//buttonContainer.addChild(uploadsBtn);
			if(!Debug.DO_NOT_SHOW_TUTORIAL) buttonContainer.addChild(tutorialBtn);
			if(!Infos.IS_DESKTOP) buttonContainer.addChild(photoManagerBtn);
			//buttonContainer.addChild(previewButton);
			buttonContainer.addChild(newBtn);
			buttonContainer.addChild(openProjectBtn);
			buttonContainer.addChild(saveBtn);
			buttonContainer.addChild(saveAsBtn);
			if( !Infos.IS_DESKTOP) buttonContainer.addChild(logoutBtn); // no logout button for desktop app
			buttonContainer.addChild(orderButton);
			buttonContainer.addChild(settingsBtn);
			buttonContainer.y = (HEIGHT-buttonContainer.height)*.5;
			buttonContainer.x = Infos.stage.stageWidth - buttonContainer.width - buttonContainer.y;
			
			
			// upload button
			addChild(uploadButton);
			ButtonUtils.makeButton(uploadButton, function(e:Event = null):void{
				PhotoUploadManager.instance.open(null);
			}, new TooltipVo("tooltip.topbar.upload.button"));
			//uploadButton.visible = false;
			uploadButton.alpha = 1;
			var numUploads : int = PhotoUploadManager.instance.numUploadLeft;
			uploadButton.pastille.label.text = numUploads;
			uploadButton.pastille.visible = false;
			
			
			
			//if project is not ready -> no project is selected
			if(!ProjectManager.instance.ready)
			{
				enabled = false;
				clearTitle();
				ProjectManager.PROJECT_READY.addOnce(updateTitle);
			}
			else
			{
				updateTitle();
				enabled = true;
				orderButton.updatePrice(Infos.project.price);
			
				// start auto save timer
				resetAutoSaveTimer();

			}
			
			
			//draw opaque bg
			drawBg();
			
			//Add this to tutorial flow
			var tutorialVo:TutorialStepVo = new TutorialStepVo(
				this,
				TutorialManager.KEY_TOP_BAR,
				TutorialManager.ARROW_UP,
				new Point(this.width/2, this.height + 20)
			);
			TutorialManager.instance.addStep(tutorialVo);
			
			//Add tutorial btn to tutorial flow
			tutorialVo = new TutorialStepVo(
				tutorialBtn,
				TutorialManager.KEY_TUTORIAL_BTN,
				TutorialManager.ARROW_UP,
				new Point(tutorialBtn.width/2, tutorialBtn.height + 20)
			);
			TutorialManager.instance.addStep(tutorialVo);
			
			//Add checkout to tutorial flow
			tutorialVo = new TutorialStepVo(
				orderButton,
				TutorialManager.KEY_CHECKOUT_BTN,
				TutorialManager.ARROW_UP,
				new Point(orderButton.width/2, orderButton.height + 20)
			);
			TutorialManager.instance.addStep(tutorialVo);
			
			//Add PhotoManager to tutorial flow
			tutorialVo = new TutorialStepVo(
				photoManagerBtn,
				TutorialManager.KEY_PHOTOMANAGER_BTN,
				TutorialManager.ARROW_UP,
				new Point(photoManagerBtn.width/2, photoManagerBtn.height + 20),
					TutorialStepVo.EDITOR_ONLINE
			);
			TutorialManager.instance.addStep(tutorialVo);
			
			//Add settings to tutorial flow
			tutorialVo = new TutorialStepVo(
				settingsBtn,
				TutorialManager.KEY_SETTINGS_BTN,
				TutorialManager.ARROW_UP,
				new Point(settingsBtn.width/2, settingsBtn.height + 20)
			);
			TutorialManager.instance.addStep(tutorialVo);
			
			
			
		}
		
		
		/**
		 * Settings button hanlder
		 * */
		private function settingsButtonClicked():void
		{
			var settingsTooltip:TooltipVo = TooltipVo.TOOLTIP_SETTINGS();
			DefaultTooltip.killAllTooltips();
			DefaultTooltip.killTooltipById(TooltipVo.TOOLTIP_SETTINGS_UID);
			DefaultTooltip.tooltipOnClip(settingsBtn,settingsTooltip);
		}
		
		/**
		 * Tutorial Btn Click hanlder
		 * */
		private function tutorialClickHandler(e:MouseEvent):void
		{
			DefaultTooltip.killAllTooltips();
			TutorialManager.instance.start();
		}
		
		private function updateTitle():void
		{
			//if(Infos.project)
			clearTitle();
			
			titleName.autoSize = "left";
			titleName.multiline = false;
			titleName.text = ""+Infos.project.name;
			titleName.scrollH = 1;
			
			if(titleName.width >= 210) {
				titleName.autoSize = TextFieldAutoSize.NONE;
				titleName.width = 210;
				threePoints.visible = true;
				threePoints.text = "...";//ResourcesManager.getString("class."+Infos.project.classname);// Infos.project.type
				threePoints.x = titleName.x + titleName.width + 1;
			}
			else{
				threePoints.visible = false;
			}
			
			
			
			
			
			if( Infos.isAlbum )
			{
				// ALBUM
				var typeId:String;
				if(Infos.project.type == ProductsCatalogue.ALBUM_CLASSIC)
				{
					if(CoverManager.instance.getCoverVo().coverType == ProductsCatalogue.COVER_CUSTOM)
						typeId = ProductsCatalogue.ALBUM_CONTEMPORARY;
					else
						typeId = ProductsCatalogue.ALBUM_CLASSIC;
				}
				else
					typeId = Infos.project.type;
				
				detailName.text = ResourcesManager.getString("album.type."+typeId) + " " +ResourcesManager.getString("album.prefix."+Infos.project.docPrefix) + " " +  (Infos.project.pageList.length-1) +" "+ ResourcesManager.getString("common.pages");
			}
			// CANVAS
			else if( Infos.isCanvas ){
				detailName.text = ResourcesManager.getString("canvas.type."+Infos.project.canvasType) + " " + ResourcesManager.getString("canvas.orientation."+Infos.project.type) ;
				// size
				var w : Number = Math.round(MeasureManager.PFLToCM(Infos.project.width));
				var h : Number = Math.round(MeasureManager.PFLToCM(Infos.project.height));
				detailName.appendText(" " + w + "cm X " + h + "cm");
				if(CanvasManager.instance.isMultiple) detailName.appendText(" (" + Infos.project.canvasMLayout + ")");
			}
			else if( Infos.isCalendar )
			{
				// CALENDAR
				detailName.text = ResourcesManager.getString("calendar.prefix."+Infos.project.docPrefix);//+ " " +  (Infos.project.pageList.length) +" "+ ResourcesManager.getString("common.pages");
			}
			else if( Infos.isCards )
			{
				// CARDS
				var orientation : String = (Infos.project.type != "types_fc_port_sq" && Infos.project.type != "types_announcecard_sq")? "("+ResourcesManager.getString("cards.orientation."+CardsManager.instance.docOrientation)+")" : "";
				detailName.text = ResourcesManager.getString("cards.type."+Infos.project.type) + " " + orientation;
				var model : String = CardsManager.instance.cardsModel;
				detailName.appendText(" " + model);
			}
			
			if(detailName.width >= 210) {
				detailName.autoSize = TextFieldAutoSize.NONE;
				detailName.width = 210;
				threePointsDetail.visible = true;
				threePointsDetail.text = "...";//ResourcesManager.getString("class."+Infos.project.classname);// Infos.project.type
				threePointsDetail.x = detailName.x + detailName.width + 1;
				threePointsDetail.y = detailName.y - 3;
			}
			else{
				threePointsDetail.visible = false;
			}
			
			
			//
			// upload icon
			addChild(uploadButton);
			uploadButton.y = 8;
			uploadButton.x = (titleName.width > detailName.width)? titleName.x + titleName.width + 25 : detailName.x + detailName.width + 25;
			
			
			
			PhotoUploadManager.OPENING_POINT = new Point (uploadButton.x -40 , uploadButton.y + 55)
		}
		
		/**
		 * Clear title
		 * */
		protected function clearTitle():void
		{
			if(titleName)
				titleName.text = "";
			
			if(detailName)
				detailName.text = "";
		}
		
		/**
		 * Handle stage resize
		 * */
		protected function resizeHandler(event:Event):void
		{
			drawBg();
			buttonContainer.x = Infos.stage.stageWidth - buttonContainer.width - buttonContainer.y;
		}
		
		/**
		 * order button clicked
		 * */
		private function onOrderProcessStart():void
		{
			if(!orderPanel) orderPanel = new OrderPanel();
			stage.addChild(orderPanel);
			
			var openingPoint : Point = new Point(stage.stageWidth-105, 60);
			orderPanel.open(openingPoint);
		}
		
		/**
		 * update order price in button
		 * */
		private function updateOrderPrice():void
		{
			orderButton.updatePrice(Infos.project.price);
		}
		
		/**
		 * update order price in button
		 * */
		private function onPhotoUploadUpdate( completedItem : PhotoUploadItem = null ):void
		{
			var numUploads : int = PhotoUploadManager.instance.numUploadLeft;
			uploadButton.pastille.label.text = numUploads;
			if(numUploads >0) {
				uploadButton.pastille.visible = true;
				uploadButton.alpha = 1;
			}
			else{
				uploadButton.pastille.visible = false;
				uploadButton.alpha = 1;
			}
			
			// add tooltip to refresh list EDIT : not really needed anymore as photos are updated automaticall
			var photoTabIsOpen : Boolean = (TabManager.instance.isOpen && TabManager.instance.selectedIndex == 1)?true:false;
			var photoManagerIsOpen : Boolean = (PhotoManagerModule.instance.parent)?true:false ;
			if(completedItem && !completedItem.frameVo && !photoTabIsOpen && !photoManagerIsOpen) {
				DefaultTooltip.killTooltipById("topBarRefreshTooltip");
				var refreshTooltip:TooltipVo = TooltipVo.TOOLTIP_UPLOAD_COMPLETE(completedItem.photoVo, function():void{
					//SessionManager.instance.updateImageList(); // do not reload
					TabManager.instance.openTab(1,LeftTabs.TABS_ID);
					TabManager.instance.open(true); // force tab opening
					DefaultTooltip.killTooltipById("topBarRefreshTooltip");
				});
				refreshTooltip.uid = "topBarRefreshTooltip";
				//DefaultTooltip.tooltipOnClip(uploadButton,refreshTooltip);	
			}
			
		}
		
		
		/**
		 * photo swap mode change
		 *
		 * */
		private function onPhotoSwapModeChange( flag : Boolean ):void
		{
			enabled = !flag;
		}
		
		/**
		 * reset timer with a delay IN MINUTES 
		 */
		public static function resetAutoSaveTimer():void
		{
			_instance.resetAutoSaveTimer();
		}
		private function resetAutoSaveTimer( delay : Number = AUTO_SAVE_DELAY ):void
		{
			if(autoSaveTimer && autoSaveTimer.running) autoSaveTimer.stop();
			if(!autoSaveTimer){
				autoSaveTimer = new Timer(1000,1);
				autoSaveTimer.addEventListener(TimerEvent.TIMER_COMPLETE, autoSaveTimerComplete );
			}
			
			// force kill existing tooltip
			DefaultTooltip.killTooltipById("autoSaveTooltip");  
			
			// set tooltipVO
			autoSaveTooltip = new TooltipVo("tooltip.topbar.autoSave", TooltipVo.TYPE_SIMPLE,null, Colors.RED, Colors.WHITE); 
			autoSaveTooltip.autoHide = -1;
			autoSaveTooltip.uid = "autoSaveTooltip";
			autoSaveTooltip.persistOnKill = true;
			
			autoSaveTimer.delay = delay * 1000 * 60;
			autoSaveTimer.start();
		}
		
		/**
		 * on autosave timer complete
		 */
		private function autoSaveTimerComplete( e:Event ):void
		{
			if( ProjectManager.instance.needSave ){
				DefaultTooltip.tooltipOnClip(saveBtn, autoSaveTooltip, true, 300);	
			} else resetAutoSaveTimer(5);
		}
		
		
		
		
		
		/**
		 * Draw a opaque background
		 * */
		private function drawBg():void
		{
			graphics.clear();
			if(!Infos.config.isRebranding)
				graphics.beginFill(Colors.BLUE_LIGHT,1);
			else
				graphics.beginFill(Infos.config.rebrandingMainColor,1);
			graphics.drawRect(0,0,Infos.stage.stageWidth,HEIGHT);
			graphics.endFill();
			
		}
		
	}
}