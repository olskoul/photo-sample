package view.menu.lefttabs.project.calendar
{
	
	import view.menu.lefttabs.LeftTabContent;
	import view.menu.lefttabs.project.ProjectSimpleItemHeader;
	
	public class ProjectCalendarTab extends LeftTabContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/calendar/";
		private var configurator:ProjectCalendarConfigurator;
		
		/**
		 * constructor
		 */
		public function ProjectCalendarTab(xmlData:XML)
		{
			super(xmlData);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		/**
		 * init the view (called when show method is called and initiated is fals)
		 */
		override public function init():void
		{
			super.init();
			
			//header
			var listHeader:ProjectSimpleItemHeader = new ProjectSimpleItemHeader("lefttab.project.calendar.title");
			listHeader.init();
			//Content -> Create Calendar options configurator
			configurator = new ProjectCalendarConfigurator(xmlData);
			accordion.addAccordionItem(listHeader, configurator, "calendarType", true,true,false);
						
			// update visible items
			accordion.updateItems();
		}
		
		override public function show():void
		{
			super.show();
			configurator.update();
		}
		
	}
}