package view.menu.lefttabs.project.calendar
{
	import com.bit101.components.ComboBox;
	import com.bit101.components.Label;
	import com.bit101.components.ListItem;
	import com.bit101.components.NumericStepper;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.DataLoadingManager;
	import be.antho.utils.SimpleImageGetter;
	
	import comp.DefaultTooltip;
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemContent;
	import comp.button.InfosButton;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	import comp.button.SimpleThinButton;
	import comp.checkbox.CheckboxTitac;
	import comp.combobox.ComboBoxTitTac;
	import comp.label.LabelTictac;
	import comp.numericstepper.NumericStepperTicTac;
	
	import data.Infos;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectCreationParamsVo;
	import data.ProjectVo;
	import data.TooltipVo;
	
	import manager.PagesManager;
	import manager.ProjectManager;
	
	import offline.manager.PriceManager;
	
	import utils.Colors;
	import utils.TextFormats;
	
	import view.dashboard.Dashboard;
	import view.edition.EditionArea;
	import view.edition.pageNavigator.PageNavigator;
	
	public class ProjectCalendarConfigurator extends AccordionItemContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/calendar/";
		
		private var xml:XML;
		private var typesArray:Array;
		private var weekDaysArray:Array;
		private var monthsArray:Array;
		private var typeCombo:ComboBox;
		private var monthCombo:ComboBox;
		private var weekDayCombo:ComboBox;
		private var yearStepper:NumericStepper;
		private var preview:Sprite;
		private var sizeLabel:TextField;

		private var _selectedType:String;

		private var sig:SimpleImageGetter;

		private var validate:SimpleButton;

		private var toolTipChangeVo:TooltipVo;
		private var forDashboard:Boolean;

		private var labelStartDay:LabelTictac;

		private var labelStartMonth:LabelTictac;

		private var labelStartYear:LabelTictac;

		private var labelType:LabelTictac;
		private var priceWrapper:SimpleThinButton;
		private var coatedPage:CheckboxTitac;
		private var labelOptions:LabelTictac;
		private var infosCoating:InfosButton;
		
		public function ProjectCalendarConfigurator(xmlData:XML, _forDashboard:Boolean = false)
		{
			super();
			xml = xmlData;
			forDashboard = _forDashboard;
			construct();
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		//	OVERIDDENS
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		//	GETTER SETTERS
		////////////////////////////////////////////////////////////////

		
		/*public function set selectedType(value:String):void
		{
			_selectedType = value;
			loadPreview(value);
			setSizeLabel(value);
		}*/
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			super.destroy();
		}
		
		/**
		 * ------------------------------------ UPDATE -------------------------------------
		 */
		/**
		 * on view show, we update configurator to macth current project infos
		 */
		public function update():void
		{
			// update button
			updateBtnLabel();
		}
		
		private function updateBtnLabel():void
		{
			if(Infos.project)
				validate.setLabel("common.update.project");
			else{
				validate.setLabel("common.start.new.project");
			}
		}
		/**
		 * ------------------------------------ CONSTRUCT -------------------------------------
		 */
		private function construct():void
		{
			//this.opaqueBackground = 0x00ff00;
			
			var proj : ProjectVo = Infos.project;
			
			//----Types
			typesArray  = [];
			var list:XMLList = xml..calendarTypes.node;
			var selectedIndex:int = 0;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("calendar.prefix."+list[i].@docidstr);
				item.id = String(list[i].@id);
				item.docidstr = String(list[i].@docidstr);
				typesArray.push(item);
				
				//selected index based on loaded project type
				if(proj && proj.type == item.id)
				{
					selectedIndex = i;					
				}
			}
			labelType = new LabelTictac(this,0,0,ResourcesManager.getString("calendar.configurator.type.label")); 
			typeCombo = new ComboBoxTitTac(this,0,0,typesArray[0].label,typesArray);
			typeCombo.showScrollButtons = false;
			typeCombo.selectedIndex = selectedIndex;

			//days
			weekDaysArray  = [];
			list = xml..calendarWeekday.node;
			for (i = 0; i < list.length(); i++) 
			{
				item = {}
				item.label = ResourcesManager.getString("calendar.weekday."+String(list[i].@id));
				item.id = i;
				weekDaysArray.push(item);
			}
			
			weekDayCombo = new ComboBoxTitTac(this,0,0,weekDaysArray[0].label,weekDaysArray);
			weekDayCombo.showScrollButtons = false;
			weekDayCombo.selectedIndex = (Infos.project)?Infos.project.calendarStartDayIndex:1;
			
			labelStartDay = new LabelTictac(this,0,0,ResourcesManager.getString("calendar.configurator.starting.day.label"),11,Colors.GREY_DARK, "right");
			
			//month
			monthsArray  = [];
			list = xml.content.calendarMonth.node;
			for (i = 0; i < list.length(); i++) 
			{
				item = {}
				item.label = ResourcesManager.getString("calendar.month."+String(list[i].@id));
				item.id = i;
				monthsArray.push(item);
			}
			
			monthCombo = new ComboBoxTitTac(this,0,0,monthsArray[0].label,monthsArray);
			monthCombo.showScrollButtons = false;
			monthCombo.selectedIndex = (Infos.project)?Infos.project.calendarStartMonthIndex:0;
			
			labelStartMonth = new LabelTictac(this,0,0,ResourcesManager.getString("calendar.configurator.starting.month.label"),11,Colors.GREY_DARK, "right");
			
			//year
			yearStepper = new NumericStepperTicTac(this, 0, 0);
			yearStepper.value = (Infos.project)?Infos.project.calendarYear:new Date().fullYear+1;
			
			labelStartYear = new LabelTictac(this,0,0,ResourcesManager.getString("calendar.configurator.starting.year.label"),11,Colors.GREY_DARK, "right");
			
			// --------- OPTIONS
			//label
			labelOptions = new LabelTictac(this,0,0,ResourcesManager.getString("calendar.option"),12,Colors.GREY_DARK, "right");
			
			//Coated page
			var coatedlabel:String = ResourcesManager.getString("lefttab.project.album.coated") + " (0€)";
			coatedPage = new CheckboxTitac(this,0,0, coatedlabel, coatedCheckHandler,true);
			infosCoating = new InfosButton("infos.lefttab.calendar.coating",this, 0, 0, false, TooltipVo.ARROW_TYPE_VERTICAL);
			
			//image preview
			preview = new Sprite();
			preview.graphics.clear();
			preview.graphics.lineStyle(1,Colors.GREY_LIGHT);
			preview.graphics.beginFill(0xFFFFFF);
			preview.graphics.drawRect(0,0,220,172);
			preview.graphics.endFill();
			addChild(preview);
			
			//size libel
			sizeLabel = new TextField();
			sizeLabel.width = preview.width;
			sizeLabel.embedFonts = true;
			sizeLabel.height = 30;
			sizeLabel.background = false;
			sizeLabel.backgroundColor = 0x00ff0f;
			sizeLabel.defaultTextFormat = TextFormats.DEFAULT(18);
			sizeLabel.text = "---";
			addChild(sizeLabel);
			
			//validate button
			//validate = new SimpleButton("common.validate",Colors.BLUE,validateHandler,Colors.GREEN, Colors.WHITE);
			validate = new SimpleSquareButton("common.start.new.project", Colors.BLUE, validateHandler, Colors.GREEN, Colors.WHITE, Colors.WHITE, 1,false,-1);
			addChild(validate);
			
			//check if proejct has been loaded from save, and check for upgrades
			var hasTypeUpgrages:Boolean;
			if(proj)
				hasTypeUpgrages = checkTypeUpgrades(proj.docPrefix);
			
			//image loader
			sig = new SimpleImageGetter();	
			
			//price (offline only and dashboard only)
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper = new SimpleThinButton("0",Colors.GREY_LIGHT,null,Colors.YELLOW,Colors.BLACK,Colors.BLACK,1,false,true);
				priceWrapper.forcedWidth = 80;
				addChild(priceWrapper);
			}
			
			//tooltips
			toolTipChangeVo = new TooltipVo("tooltip.calendar.configurator.month.changed",TooltipVo.TYPE_SIMPLE,null,Colors.BLUE_LIGHT, Colors.BLUE, TooltipVo.ARROW_TYPE_HORIZONTAL);
			toolTipChangeVo.autoHide = 2;
			//disabele/enable ui
			disableUI(hasTypeUpgrages);//updateConfigurator();
			
			//Show preview
			showPreview();
			
			//listen when project is ready
			ProjectManager.PROJECT_READY.add(projectBuildComplete);
			
			//listeners
			setComboBoxListeners(true);
			
			//price
			updatePriceInConfigurator();
			
			//Layout
			if(!forDashboard)
				layItOut();
			else
				layItOutForDash();
			
			//btn label
			updateBtnLabel
		}
		
		private function layItOut():void
		{
			
			//----Types
			labelType.x = 15;
			labelType.y = 0;
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 25);
			typeCombo.x = labelType.x; 
			typeCombo.y = labelType.y + labelType.height + 10; 
			
			//----Days
			weekDayCombo.setSize(150, 25);
			weekDayCombo.x = Accordion.MAX_WIDTH - weekDayCombo.width-15;
			weekDayCombo.y = typeCombo.y + typeCombo.height + 15;
			
			labelStartDay.x = 15;
			labelStartDay.y = weekDayCombo.y + (weekDayCombo.height - labelStartDay.height)/2;
			
			//month
			monthCombo.setSize(150, 25);
			monthCombo.x = Accordion.MAX_WIDTH - monthCombo.width-15;
			monthCombo.y = weekDayCombo.y + weekDayCombo.height + 15;
			
			labelStartMonth.x = 15;
			labelStartMonth.y = monthCombo.y + (monthCombo.height - labelStartMonth.height)/2;
			
			//year
			yearStepper.setSize(150, 25);
			yearStepper.x = Accordion.MAX_WIDTH - yearStepper.width-15;
			yearStepper.y = monthCombo.y + monthCombo.height + 15;
			labelStartYear.x = 15;
			labelStartYear.y = yearStepper.y + (yearStepper.height - labelStartYear.height)/2;
			
			//label option
			labelOptions.x = 15;
			labelOptions.y = yearStepper.y + yearStepper.height + 15;
			
			//Coated pages
			coatedPage.x = labelOptions.x + 5;
			coatedPage.y = labelOptions.y + labelOptions.height + 5;
			coatedPage.setSize(Accordion.MAX_WIDTH - 30, 25);
			infosCoating.y = coatedPage.y;
			infosCoating.x = coatedPage.realWidth() + 10;
			
			//image preview
			preview.scaleX = preview.scaleY = .8;
			preview.x = (Accordion.MAX_WIDTH - preview.width) >> 1;
			preview.y = coatedPage.y + coatedPage.height + 15
			
			//size libel
			sizeLabel.x = preview.x;
			sizeLabel.y = preview.y + preview.height + 15;
			
			//validate button
			validate.forcedWidth = 250;
			validate.x = (Accordion.MAX_WIDTH - validate.width) >> 1;
			validate.y = sizeLabel.y + sizeLabel.height + 15;

		}
		
		private function layItOutForDash():void
		{
			
			//----Types
			labelType.x = 15;
			labelType.y = 0;
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 25);
			typeCombo.x = labelType.x; 
			typeCombo.y = labelType.y + labelType.height + 10; 
			
			//----Days
			weekDayCombo.setSize(150, 25);
			weekDayCombo.x = Accordion.MAX_WIDTH - weekDayCombo.width-15;
			weekDayCombo.y = typeCombo.y + typeCombo.height + 15;
			
			labelStartDay.x = 15;
			labelStartDay.y = weekDayCombo.y + (weekDayCombo.height - labelStartDay.height)/2;
			
			//month
			monthCombo.setSize(150, 25);
			monthCombo.x = Accordion.MAX_WIDTH - monthCombo.width-15;
			monthCombo.y = weekDayCombo.y + weekDayCombo.height + 15;
			
			labelStartMonth.x = 15;
			labelStartMonth.y = monthCombo.y + (monthCombo.height - labelStartMonth.height)/2;
			
			//year
			yearStepper.setSize(150, 25);
			yearStepper.x = Accordion.MAX_WIDTH - yearStepper.width-15;
			yearStepper.y = monthCombo.y + monthCombo.height + 15;
			labelStartYear.x = 15;
			labelStartYear.y = yearStepper.y + (yearStepper.height - labelStartYear.height)/2;
			
			//label option
			labelOptions.x = 15;
			labelOptions.y = yearStepper.y + yearStepper.height + 15;
			
			//Coated pages
			coatedPage.x = labelOptions.x + 5;
			coatedPage.y = labelOptions.y + labelOptions.height + 5;
			coatedPage.setSize(Accordion.MAX_WIDTH - 30, 30);
			infosCoating.y = coatedPage.y;
			infosCoating.x = coatedPage.realWidth() + 10;
			
			//image preview
			preview.x = typeCombo.x + typeCombo.width + 30;
			preview.y = typeCombo.y;
			
			//size libel
			sizeLabel.x = preview.x;
			sizeLabel.y = preview.y + preview.height + 15;
			
			//validate button
			validate.forcedWidth = 250;
			validate.x = width - validate.width >> 1;
			validate.y = sizeLabel.y + sizeLabel.height + 15;
			
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper.x = 15;
				priceWrapper.y = validate.y;
			}

		}
		
		/**
		 * LISTENERS (flag -> enable or disble them)
		 * 
		 * */
		private function setComboBoxListeners(flag:Boolean):void
		{
			typeCombo.removeEventListener(Event.SELECT, typeSelectHandler);
			weekDayCombo.removeEventListener(Event.SELECT, weekDayComboSelectHandler);
			yearStepper.removeEventListener(Event.CHANGE, yearChangedHandler);
			monthCombo.removeEventListener(Event.SELECT, monthComboComboSelectHandler);
			
			if(flag)
			{
				typeCombo.addEventListener(Event.SELECT, typeSelectHandler);
				weekDayCombo.addEventListener(Event.SELECT, weekDayComboSelectHandler);
				yearStepper.addEventListener(Event.CHANGE, yearChangedHandler);
				monthCombo.addEventListener(Event.SELECT, monthComboComboSelectHandler);
			}
		}
		
		/**
		 * PROJECT READY handler
		 * 
		 * */
		private function projectBuildComplete():void
		{
			
		}	
		
		/**
		 * Create the image path based on ID's
		 * then load image
		 * */
		private function showPreview():void
		{
			var imageName:String = typeCombo.selectedItem.id;
			loadPreview(imageName);
			setSizeLabel(typeCombo.selectedItem.id);
		}
		
		/**
		 * Configurator Validation
		 * Happen when there is no project ready
		 * It creates a new project
		 * 
		 * or
		 * when applying an upgrade to the current project
		 * it update the current project vo
		 * 
		 * Also, it disable some options depending on the selected project
		 * 
		 * And finally checks for upgrades if available
		 * 
		 * */
		private function validateHandler():void
		{
			if(forDashboard)
			{
				TweenMax.delayedCall(0,Dashboard.instance.close);
			}
			
			var initParams:ProjectCreationParamsVo = new ProjectCreationParamsVo();
			initParams.calendarStartDayIndex = weekDayCombo.selectedItem.id+1; //+1 to match wizard on prod
			initParams.calendarYear = yearStepper.value;
			initParams.calendarStartMonthIndex = monthCombo.selectedItem.id+1; //+1 to match wizard on prod
			
			var selectedProjectDocType:String = getSelectedDocType();
			
			//set paper value
			if(Infos.project)
			{
				Infos.project.coated = coatedPage.selected;
			}
			else
			{
				initParams.coated = coatedPage.selected;
			}
			
			if(!typeCombo.selectedItem.isUpgrade)
			{
				Infos.session.product_id = selectedProjectDocType;
				ProjectManager.instance.createNewProject(Infos.session.product_id,initParams);
			}
			else
			{
				if(Infos.session.product_id != selectedProjectDocType)
				{
					Infos.session.product_id = selectedProjectDocType;
					ProjectManager.instance.updateCurrentCalendarProject(Infos.session.product_id);
				}
				
			}
			
			//check upgrades
			var hasTypeUpgrade:Boolean = checkTypeUpgrades(typeCombo.selectedItem.docidstr);
			
			//disableUI
			disableUI(hasTypeUpgrade);
			
			//update btn label
			if(Infos.project)
				validate.setLabel("common.update.project");
			
		}
		
		/**
		 * Upate configuration price
		 * desktop and dashBoard only
		 * */
		private function updatePriceInConfigurator():void
		{
			CONFIG::offline
				{
					if(Infos.IS_DESKTOP && forDashboard)
					{
						var currentDocType:String = getSelectedDocType();
						var minimumPrice:Number = PriceManager.instance.getPriceFromDash(currentDocType, false,coatedPage.selected,ProductsCatalogue.PAPER_QUALITY_250, null);
						priceWrapper.setLabel(ResourcesManager.getString("price.minimum.label")+" "+String(minimumPrice)+"€",false);
					}
					
				}
		}
		
		/**
		 * Get selected docType
		 */
		private function getSelectedDocType():String
		{
			var selectedProjectTypeCode:String = String(xml.content.calendarTypes.node.(@id == typeCombo.selectedItem.id)[0].@docidstr);
			var selectedProjectDocType:String = ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
			return ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
		}
		
		/**
		 * DISalbe/enable UI based on the validated project
		 * */
		private function disableUI(hasTypeUpgrade:Boolean):void
		{
				typeCombo.enabled = (!Infos.project)?true:hasTypeUpgrade; 
				validate.enabled = (!Infos.project)?true:hasTypeUpgrade;
				
				//reset all to true
				monthCombo.enabled = true;
				weekDayCombo.enabled = true;
				yearStepper.enabled = true;
				
				switch(typeCombo.selectedItem.docidstr)
				{
					case "WCAL3":
					case "WCAL2":
						break;
					
					case "WCAL4":
						weekDayCombo.enabled = false;
						yearStepper.enabled = false;
						break;
					
					case "WCAL5":
					case "WCAL6":
						weekDayCombo.enabled = false;
						weekDayCombo.selectedIndex = 1;
						break;
					
					case "WCAL":
						break;
					
					case "DCAL2":
						break;
					
					case "DCAL":
						break;
				}
				
				//COATED OPTION
				checkCoatedOption();
				/*var coatedNode:XML = XML(xml.content.calendarTypes.node.(@id == typeCombo.selectedItem.id).page_coating);
				var coatedAllow:Boolean = (coatedNode.@enabled == "true");
				var coatedDefaultValue:Boolean = (coatedNode.@defaultValue == "true");
				coatedPage.enabled = coatedAllow;
				coatedPage.selected = (coatedAllow)?(Infos.project)?Infos.project.coated:coatedDefaultValue:coatedDefaultValue;
				//Retro compatibility
				if(Infos.project && Infos.project.coated && !coatedAllow)
					Infos.project.coated = false;*/

		}
		
		
		/**
		 * Check coated combo state
		 */
		private function checkCoatedOption():void
		{
			var coatedNode:XML = XML(xml.content.calendarTypes.node.(@id == typeCombo.selectedItem.id).page_coating);
			var coatedAllow:Boolean = (coatedNode.@enabled == "true");
			var coatedDefaultValue:Boolean = (coatedNode.@defaultValue == "true");
			if(!coatedAllow)
			{
				coatedPage.selected = coatedDefaultValue;
				coatedPage.enabled = false;
				if(Infos.project)
					Infos.project.coated = coatedDefaultValue;
			}
			else
			{
				coatedPage.enabled = true;
				
				if(Infos.project)
				{
					coatedPage.selected = Infos.project.coated;
				}
				else
				{
					coatedPage.selected = coatedDefaultValue;
				}
			}
			
			coatedPage.label = ResourcesManager.getString("lefttab.project.album.coated") + " (" + PriceManager.instance.getCoatingCost(getSelectedDocType()) +"€)";
			infosCoating.x = coatedPage.x + coatedPage.realWidth() + 5;
		}
		
		/**
		 * get current type project upgrades
		 * if no type upgrade typecombo enabled = false
		 */
		private function checkTypeUpgrades(prefix:String):Boolean
		{
			var upgradeList:Array = getTypeUpgrades(prefix);
			if(upgradeList.length>0)
			{
				//typeCombo.removeAll();
				typeCombo.items = upgradeList;	
				typeCombo.draw();
				//validate.enabled = true;
				//typeCombo.enabled = true;
				for (var i:int = 0; i < upgradeList.length; i++) 
				{
					if(upgradeList[i].docidstr == prefix)
						typeCombo.selectedIndex = i;
				}
				
				return true;
			}
			else
			{
				//validate.enabled = false;
				//typeCombo.enabled = false;	
				return false;
			}
		}
		
		/**
		 * get current project type upgrades
		 */
		private function getTypeUpgrades(prefix:String):Array
		{
			typesArray  = [];
			var list:XMLList = xml..calendarUpgrades.descendants(prefix).node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("calendar.prefix."+list[i].@docidstr);
				item.id = String(list[i].@id);
				item.docidstr = String(list[i].@docidstr);
				item.isUpgrade = true;
				typesArray.push(item);
			}
			return typesArray;
		}
		
		/**
		 * Set Size label
		 */
		private function setSizeLabel(id:String):void
		{
			sizeLabel.text = String(xml.content.calendarTypes.node.(@id == id)[0].@size);
		}
		
		
		/**
		 * Calendar type selected handler
		 */
		protected function typeSelectHandler(event:Event):void
		{
			disableUI(true);
			showPreview();
			updatePriceInConfigurator();
		}
		
		/**
		 * Show / Hide components based on the chosen type
		 * */
		/*private function updateConfigurator():void
		{
			switch(typeCombo.selectedItem.docidstr)
			{
				case "WCAL5":
				case "WCAL6":
					weekDayCombo.selectedIndex = 1;
					weekDayCombo.enabled = false;
					break;
				
				case "WCAL4":
					validate.enabled = true;
					monthCombo.enabled = true;
					weekDayCombo.enabled = false;
					typeCombo.enabled = true;
					yearStepper.enabled = false;
					break;
				
				default: // no project selected
					validate.enabled = true;
					monthCombo.enabled = true;
					weekDayCombo.enabled = true;
					typeCombo.enabled = true; 
					yearStepper.enabled = true;
				
			}
			
			showPreview();
		}*/
		
		/**
		 * Call updateLinkedFrames with a delay to allow loading to show up
		 */
		protected function updateLinkedFrames(tooltipClip:DisplayObject,tooltipLabelKey:String ):void
		{

			var whenDone:Function = function():void
			{
				if(Infos.project)
				{
					toolTipChangeVo.labelKey = tooltipLabelKey;
					DefaultTooltip.tooltipOnClip(tooltipClip,toolTipChangeVo);
				}
			};
			
			PagesManager.instance.updateAllLinkedFrames(whenDone);
			DataLoadingManager.instance.hideLoading();
			
		}
		/**
		 * Calendar type selected handler
		 */
		protected function weekDayComboSelectHandler(event:Event):void
		{
			if(ProjectManager.instance.ready)
			{
				DataLoadingManager.instance.showLoading(false,null,null,true);
				ProjectManager.instance.project.calendarStartDayIndex = weekDayCombo.selectedItem.id;
				TweenMax.delayedCall(.5,updateLinkedFrames,[weekDayCombo,"tooltip.calendar.configurator.day.changed"]);		
			}
		}
		
		/**
		 * Calendar start month selected handler
		 */
		protected function monthComboComboSelectHandler(event:Event):void
		{
			if(ProjectManager.instance.ready)
			{
				DataLoadingManager.instance.showLoading(false,null,null,true);
				ProjectManager.instance.project.calendarStartMonthIndex = monthCombo.selectedItem.id;
				TweenMax.delayedCall(.5,updateLinkedFrames,[monthCombo,"tooltip.calendar.configurator.month.changed"]);	
			}
		}
		
		/**
		 * Calendar year changed handler
		 */
		protected function yearChangedHandler(event:Event):void
		{
			if(ProjectManager.instance.ready)
			{
				ProjectManager.instance.project.calendarYear = yearStepper.value;
				DataLoadingManager.instance.showLoading(false,null,null,true);
				TweenMax.delayedCall(.5,updateLinkedFrames,[yearStepper,"tooltip.calendar.configurator.year.changed"]);	
			}
		}
		
		private function coatedCheckHandler(e:Event):void
		{
			//update price
			updatePriceInConfigurator();
			
			if(Infos.project)
			{
				Infos.project.coated = (coatedPage.selected);
				ProjectManager.instance.updateProjecPrice();
			}
		}
		
		
		/**
		 * Load calendar preview image
		 * */
		private function loadPreview(id:String):void
		{
			cleanPreview();
			preview.addChild(sig.getImage(Infos.config.getValue("imagePath") + THUMB_PATH+id+".jpg",true,preview.getBounds(this)));
		}
		
		/**
		 * Remove previous preview if necessary
		 * */
		private function cleanPreview():void
		{
			if(preview.numChildren > 0)
			{
				var total:int = preview.numChildren;
				for (var i:int = 0; i < total; i++) 
				{
					var child:DisplayObject;
					if(preview.numChildren > 0)
					{
						child = preview.getChildAt(0)
						preview.removeChild(child);
					}
					
				}
			}
			
		}
		
		/**
		 * Draw bg
		 */
		private function drawBg():void
		{
			graphics.clear();
			graphics.beginFill(0xFFFFFF);
			graphics.drawRect(0,0,width,yearStepper.y + yearStepper.height + 10);
			graphics.endFill();
		}
		
		
	}
}