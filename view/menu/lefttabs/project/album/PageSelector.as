package view.menu.lefttabs.project.album
{
	import com.bit101.components.HSlider;
	import com.bit101.components.Slider;
	
	import comp.CustomHSlider;
	import comp.accordion.AccordionItemContent;
	
	import org.osflash.signals.Signal;
	
	public class PageSelector extends AccordionItemContent
	{
		public const CHANGE:Signal = new Signal(int);
		private var _value:int;
		private var WIDTH:Number;
		private var min:Number;
		private var max:Number;

		private var slider:CustomHSlider;
		
		public function PageSelector(WIDTH:Number)
		{
			super();
			this.WIDTH = WIDTH;
		}
		
		public function get value():int
		{
			return slider.value;
		}

		public function set value(value:int):void
		{
			if(value >= min && value <= max)
			{
				_value = value;
				slider.value = value;
			}
			else
			{
				if(value < min)
				{
					_value = min;
					slider.value = min;
				}
				else if(value > max)
				{
					_value = max;
					slider.value = max;
				}
			}
		}

		/**
		 * 
		 * Build a custom hslider based on MAx and min value
		 * incrementation of 10 units (could be changed)
		 * 
		 * */
		public function buildSelector(min:int = 10, max:int = 100, defaultValue:int = 50, sliderWidth:Number = 245, sliderHeight:Number = 15):void
		{
			graphics.beginFill(0xfffffF);
			graphics.drawRect(0,0,WIDTH-5, 100);
			graphics.endFill();
			
			this.min = min;
			this.max = max;
			
			slider = new CustomHSlider(this,(this.width-sliderWidth)*.5,35,testPage);
			slider.setSize(sliderWidth,sliderHeight);
			slider.tick = 10;
			slider.setSliderParams(min,max,defaultValue);
			slider.value = Number(defaultValue);
			slider.CHANGE.add(changeHandler);
		}
		
		private function changeHandler(_slider:Slider):void
		{
			CHANGE.dispatch(value);
		}
		
		override public function destroy():void
		{
			slider.CHANGE.removeAll();
			CHANGE.removeAll();
			super.destroy();
		}
		
		private function testPage(s:HSlider):void
		{
			//trace(s.value);
		}	
	}
}