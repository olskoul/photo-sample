
package view.menu.lefttabs.project.album 
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.ComboBox;
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.bit101.components.ListItem;
	import com.bit101.components.NumericStepper;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.SimpleImageGetter;
	
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemContent;
	import comp.button.InfosButton;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	import comp.button.SimpleThinButton;
	import comp.checkbox.CheckboxTitac;
	import comp.combobox.ComboBoxTitTac;
	import comp.label.LabelTictac;
	
	import data.Infos;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectCreationParamsVo;
	import data.ProjectVo;
	import data.TooltipVo;
	
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import offline.manager.PriceManager;
	
	import ordering.OrderManager;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.dashboard.Dashboard;
	import view.edition.EditionArea;
	import view.popup.PopupAbstract;
	
	public class ProjectAlbumConfigurator extends AccordionItemContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/album/";
		
		private var xml:XML;
		private var typesArray:Array;
		private var papersArray:Array;
		private var sizesArray:Array;
		private var totalPagesArray:Array;
		private var envelopesArray:Array;
		private var qualitiesArray:Array;
		//private var flyLeafColorsArray:Array;
		private var typeCombo:ComboBoxTitTac;
		private var totalPagesCombo:ComboBoxTitTac;
		private var envelopesCombo:ComboBoxTitTac;
		private var sizesCombo:ComboBoxTitTac;
		private var paperQualityCombo:ComboBoxTitTac;
		//private var flyleafColorCombo:ComboBoxTitTac;
		private var preview:Sprite;
		private var _selectedType:String;
		private var sig:SimpleImageGetter;
		private var validate:SimpleButton;
		private var customWidth:NumericStepper;
		private var customSize:HBox;
		private var customHeight:NumericStepper;
		private var multipleSize:HBox;
		private var multipleWidth:NumericStepper;
		private var multipleHeight:NumericStepper;
		private var multipleMargin:NumericStepper;
		private var multipleOptions:VBox;
		private var paper:CheckBox;
		private var coatedPage:CheckBox;
		private var pageNumber:CheckBox;
		private var pageNumberCentered:CheckBox;
		private var labelType:LabelTictac;
		private var labelSize:LabelTictac;
		private var labelTotalPage:LabelTictac;
		private var labelOptions:LabelTictac;		
		private var labelPaperQuality:LabelTictac;		
		//private var labelFlyleaf:LabelTictac;		
		private var forDashboard:Boolean;
		private var priceWrapper:SimpleThinButton; 

		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var infosQuality:InfosButton;
		private var infosCoating:InfosButton;

		

		

		public function ProjectAlbumConfigurator(xmlData:XML, _forDashboard:Boolean = false)
		{
			super();
			xml = xmlData;
			forDashboard = _forDashboard;
			construct();
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTERS
		////////////////////////////////////////////////////////////////

		public function get selectedType():String
		{
			return _selectedType;
		}

		public function set selectedType(value:String):void
		{
			_selectedType = value;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			//Signals
			ProjectManager.PROJECT_READY.remove(projectBuildComplete);
			ProjectManager.PROJECT_UPDATED.remove(projectBuildComplete);
			
			super.destroy();
		}
		
		
		
		/**
		 * ------------------------------------ UPDATE -------------------------------------
		 */
		/**
		 * on view show, we update configurator to macth current project infos
		 */
		public function update():void
		{
			// update button
			updateBtnLabel();
		}
		
		private function updateBtnLabel():void
		{
			if(Infos.project)
				validate.setLabel("common.update.project");
			else{
				validate.setLabel("common.start.new.project");
			}
		}
		/**
		 * ------------------------------------ CONSTRUCT -------------------------------------
		 */
		private function construct():void
		{
			//this.opaqueBackground = 0x00ff00;
			// --------- TYPES
			typesArray  = [];
			var list:XMLList = xml..types.node;
			var selectedIndex:int = 0;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("album.type."+list[i].@type);
				item.type = String(list[i].@type);
				item.id = String(list[i].@id);
				
				if(Infos.project && Infos.project.type == item.id)
				{
					selectedIndex = i;					
				}
				typesArray.push(item);
			}
			
			//label
			labelType = new LabelTictac(this,0,0,ResourcesManager.getString("album.type"),12,Colors.GREY_DARK, "right");
			//Combo box
			typeCombo = new ComboBoxTitTac(this,0,0,typesArray[0].label,typesArray);
			typeCombo.showScrollButtons = false;
			
			//hack for making diference between CLASSIC LEATHER and CLASSIC CUSTOM
			if(selectedIndex == 1)//contemporary
			{
				if(Infos.project && Infos.project.coverType == ProductsCatalogue.COVER_LEATHER)
					selectedIndex = 0;
			}
			typeCombo.selectedIndex = selectedIndex;

			
			// --------- SIZES
			//label
			labelSize = new LabelTictac(this,0,0,ResourcesManager.getString("album.size"),12,Colors.GREY_DARK, "right");
			
			//Sizes
			sizesCombo = new ComboBoxTitTac(this,0,0, null, sizesArray);
			sizesCombo.showScrollButtons = false;
			
			
			// --------- PAGES
			//label
			labelTotalPage = new LabelTictac(this,0,0,ResourcesManager.getString("page.number"),12,Colors.GREY_DARK, "right");
			
			//Total pages
			totalPagesCombo = new ComboBoxTitTac(this,0,0,null,totalPagesArray);
			totalPagesCombo.showScrollButtons = false;
			
			
			// --------- OPTIONS
			//label
			labelOptions = new LabelTictac(this,0,0,ResourcesManager.getString("album.option"),12,Colors.GREY_DARK, "right");
			
			//Page quality
			paperQualityCombo = new ComboBoxTitTac(this,0,0,null,qualitiesArray);
			labelPaperQuality = new LabelTictac(this,0,0,ResourcesManager.getString("leftab.project.album.paper.quality.label"),12,Colors.GREY_DARK, "right");
			infosQuality = new InfosButton("infos.lefttab.albums.paperQuality",this, 0, 0, false, TooltipVo.ARROW_TYPE_VERTICAL);
			
			//Flyleaf
			//flyleafColorCombo = new ComboBoxTitTac(this,0,0,null,flyLeafColorsArray);
			//labelFlyleaf = new LabelTictac(this,0,0,ResourcesManager.getString("leftab.project.album.flyleaf.label"),12,Colors.GREY_DARK, "right");
			
			//Coated page
			var coatedlabel:String = ResourcesManager.getString("lefttab.project.album.coated") + " (0€)";
			coatedPage = new CheckboxTitac(this,0,0, coatedlabel, coatedCheckHandler, true);
			infosCoating = new InfosButton("infos.lefttab.albums.coating",this, 0, 0, false, TooltipVo.ARROW_TYPE_VERTICAL);
			
			//Insert (paper between pages)
			var paperCheckBoxLabel:String = ResourcesManager.getString("lefttab.project.album.paper") + "(0€)";
			paper = new CheckboxTitac(this,0,0, paperCheckBoxLabel, paperCheckHandler,true);
			
			//Page number
			pageNumber = new CheckboxTitac(this,0,0, ResourcesManager.getString("lefttab.project.album.pageNumber"), pageNumberCheckHandler,true);
			pageNumberCentered = new CheckboxTitac(this,0,0, ResourcesManager.getString("lefttab.project.album.pageNumber.center"), pageNumberCenteredCheckHandler,true);
			
			
			//image preview
			preview = new Sprite();
			preview.graphics.clear();
			preview.graphics.lineStyle(1,Colors.WHITE);
			preview.graphics.beginFill(0xFFFFFF);
			preview.graphics.drawRect(0,0,200,117);
			preview.graphics.endFill();
			//preview.scaleX = preview.scaleY = .9;
			
			addChild(preview);
			
			//image loader
			sig = new SimpleImageGetter();
			
			//price (offline only and dashboard only)
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper = new SimpleThinButton("0",Colors.GREY_LIGHT,null,Colors.YELLOW,Colors.BLACK,Colors.BLACK,1,false,true);
				priceWrapper.forcedWidth = 80;
				addChild(priceWrapper);
			}
			
			//validate button
			validate = new SimpleSquareButton("common.start.new.project", Colors.BLUE, validateHandler, Colors.GREEN, Colors.WHITE, Colors.WHITE, 1,false,-1);
			addChild(validate);			
			
			//default value
			updateSizes(null);
			
			//check if proejct has been loaded from save, and check for upgrades
			var hasTypeUpgrages:Boolean;
			if(Infos.project)
				hasTypeUpgrages = checkTypeUpgrades(Infos.project.docPrefix);
			
			if(Infos.project && !hasTypeUpgrages)
				checkSizeUpgrades(Infos.project.docPrefix);
			
			//listen when project is ready
			ProjectManager.PROJECT_READY.add(projectBuildComplete);
			ProjectManager.PROJECT_UPDATED.add(projectBuildComplete);
			
			var project:ProjectVo = Infos.project;
			
			// preselect paper (could be overriden by thex call checkInsertAllowanceAndStock)
			if(Infos.project) paper.selected = Infos.project.paper;
			else if (Infos.session.product_inserts && Infos.session.product_inserts == "1") paper.selected = true;
			else paper.selected = false;
			
			if(Infos.project) pageNumber.selected = Infos.project.pageNumber;
			if(Infos.project) pageNumberCentered.selected = Infos.project.pageNumberCentered;
			pageNumberCentered.enabled = pageNumber.selected;
			
			//Check stock and allowance
			checkAlbumOptionsAndAvailability();
			
			//Layout
			if(!forDashboard)
			layItOut();
			else
			layItOutForDash();
			
			//btn label
			updateBtnLabel();
				
		}
		
		private function layItOut():void
		{
			//----TYPES
			labelType.x = 15;
			labelType.y = 0;
			
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 25);
			typeCombo.y = labelType.y + labelType.height + 5;
			typeCombo.x = 15;

			//----SIZES
			labelSize.x = 15;
			labelSize.y = typeCombo.y + typeCombo.height + 15;
			
			sizesCombo.x = 15;
			sizesCombo.y = labelSize.y + labelSize.height + 5
			sizesCombo.setSize(Accordion.MAX_WIDTH - 30, 25);
			
			//----PAGES
			
			totalPagesCombo.setSize(140, 25);
			totalPagesCombo.x = Accordion.MAX_WIDTH - totalPagesCombo.width-15;
			totalPagesCombo.y = sizesCombo.y + sizesCombo.height + 15;
			
			labelTotalPage.x = 15;
			labelTotalPage.y = totalPagesCombo.y + (totalPagesCombo.height - labelTotalPage.height)/2; 
			
			//Paper quality combo
			paperQualityCombo.setSize(140, 25);
			paperQualityCombo.x = Accordion.MAX_WIDTH - paperQualityCombo.width-15;
			paperQualityCombo.y = totalPagesCombo.y + totalPagesCombo.height + 15;
			
			labelPaperQuality.x = 15;
			labelPaperQuality.y = paperQualityCombo.y + (paperQualityCombo.height - labelPaperQuality.height)/2; 
			infosQuality.y = labelPaperQuality.y;
			infosQuality.x = labelPaperQuality.x + labelPaperQuality.width + 5;
			
			//Flyleaf
			/*flyleafColorCombo.setSize(150, 30);
			flyleafColorCombo.x = Accordion.MAX_WIDTH - flyleafColorCombo.width-15;
			flyleafColorCombo.y = paperQualityCombo.y + paperQualityCombo.height + 15;
			
			labelFlyleaf.x = 15;
			labelFlyleaf.y = flyleafColorCombo.y + (flyleafColorCombo.height - labelFlyleaf.height)/2; */
			
			//----OPTIONS
			labelOptions.x = 15;
			labelOptions.y = paperQualityCombo.y + paperQualityCombo.height + 15;
			
			
			//Coated pages
			coatedPage.x = labelOptions.x + 5;
			coatedPage.y = labelOptions.y + labelOptions.height + 5;
			infosCoating.y = coatedPage.y;
			infosCoating.x = coatedPage.x + coatedPage.realWidth() + 5;
			coatedPage.setSize(Accordion.MAX_WIDTH - 30, 25);
			
			
			
			//inset papaer
			paper.x = coatedPage.x;
			paper.y = coatedPage.y + coatedPage.height - 5;
			paper.setSize(Accordion.MAX_WIDTH - 30, 25);
			//show page numbers
			pageNumber.x = paper.x;
			pageNumber.y = paper.y + paper.height -5;
			pageNumber.setSize(Accordion.MAX_WIDTH - 30, 25);
			//center page number
			pageNumberCentered.x = pageNumber.x;
			pageNumberCentered.y = pageNumber.y + pageNumber.height -5;
			pageNumberCentered.setSize(Accordion.MAX_WIDTH - 30, 25);

			//----PREVIEW IMAGE
			preview.x = (Accordion.MAX_WIDTH - preview.width) >> 1;
			preview.y = pageNumberCentered.y + pageNumberCentered.height + 5;

			//----VALIDATE BTN
			validate.forcedWidth = 250;
			validate.x = (Accordion.MAX_WIDTH - validate.width) >> 1;
			validate.y = preview.y + preview.height + 5;
		}
		
		private function layItOutForDash():void
		{
			//----TYPES
			labelType.x = 15;
			labelType.y = 0;
			
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 25);
			typeCombo.y = labelType.y + labelType.height + 5;
			typeCombo.x = 15;
			
			//----SIZES
			labelSize.x = 15;
			labelSize.y = typeCombo.y + typeCombo.height + 15;
			
			sizesCombo.x = 15;
			sizesCombo.y = labelSize.y + labelSize.height + 5
			sizesCombo.setSize(Accordion.MAX_WIDTH - 30, 25);
			
			//TOTAL PAGES
			totalPagesCombo.setSize(140, 25);
			totalPagesCombo.x = Accordion.MAX_WIDTH - totalPagesCombo.width-15;
			totalPagesCombo.y = sizesCombo.y + sizesCombo.height + 10;
			
			labelTotalPage.x = 15;
			labelTotalPage.y = totalPagesCombo.y + (totalPagesCombo.height - labelTotalPage.height)/2; 
			
			//Paper quality combo
			paperQualityCombo.setSize(140, 25);
			paperQualityCombo.x = Accordion.MAX_WIDTH - paperQualityCombo.width-15;
			paperQualityCombo.y = totalPagesCombo.y + totalPagesCombo.height + 10;
			
			labelPaperQuality.x = 15;
			labelPaperQuality.y = paperQualityCombo.y + (paperQualityCombo.height - labelPaperQuality.height)/2; 
			infosQuality.y = labelPaperQuality.y;
			infosQuality.x = labelPaperQuality.x + labelPaperQuality.width + 5;
			
			//----OPTIONS
			labelOptions.x = labelType.x + typeCombo.width + 30;
			labelOptions.y = labelType.y;
			
			
			//Coated pages
			coatedPage.x = labelOptions.x + 5;
			coatedPage.y = labelOptions.y + labelOptions.height + 5;
			coatedPage.setSize(Accordion.MAX_WIDTH - 30, 25);
			
			infosCoating.y = coatedPage.y;
			infosCoating.x = coatedPage.x + coatedPage.realWidth() + 5;
			
			//inset papaer
			paper.x = coatedPage.x;
			paper.y = coatedPage.y + coatedPage.height - 5;
			paper.setSize(Accordion.MAX_WIDTH - 30, 25);
			
			//show page numbers
			/*pageNumber.x = paper.x;
			pageNumber.y = paper.y + paper.height -5;
			pageNumber.setSize(Accordion.MAX_WIDTH - 30, 30);*/
			
			//Customer asked to remove it on dashboard
			pageNumber.parent.removeChild(pageNumber);
			//center page number
			/*pageNumberCentered.x = pageNumber.x;
			pageNumberCentered.y = pageNumber.y + pageNumber.height -5;
			pageNumberCentered.setSize(Accordion.MAX_WIDTH - 30, 30);*/
			pageNumberCentered.parent.removeChild(pageNumberCentered);
			
			//----PREVIEW IMAGE
			preview.x = labelOptions.x;
			preview.y = paper.y + paper.height + 5;
			
			//----VALIDATE BTN
			validate.forcedWidth = 250;
			validate.x = width - validate.width >> 1;
			validate.y = preview.y + preview.height + 30;
			
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper.x = 15;
				priceWrapper.y = validate.y;
			}
		}
		
		private function paperCheckHandler(e:Event):void
		{
			//update price
			updatePriceInConfigurator();
			showPreview();
			
			if(Infos.project)
			{
				Infos.project.paper = (paper.selected);
				ProjectManager.instance.updateProjecPrice();
				
				//refresh Page if cover
				var currentPage : PageVo = PagesManager.instance.currentEditedPage;
				if(currentPage && currentPage.isCover)
					EditionArea.Refresh();
			}
		}
		
		private function coatedCheckHandler(e:Event):void
		{
			//update price
			updatePriceInConfigurator();
			
			if(Infos.project)
			{
				Infos.project.coated = (coatedPage.selected);
				ProjectManager.instance.updateProjecPrice();
				
				//refresh Page if cover
				var currentPage : PageVo = PagesManager.instance.currentEditedPage;
				if(currentPage && currentPage.isCover)
					EditionArea.Refresh();
			}
		}
		
		private function pageNumberCheckHandler(e:Event):void
		{
			if(Infos.project)
			{
				Infos.project.pageNumber = (pageNumber.selected);
				
				if(Infos.project.pageNumber)
					PagesManager.instance.showPageNumber();
				else
					PagesManager.instance.hidePageNumber();
			}
			pageNumberCentered.enabled = pageNumber.selected;
		}
		
		private function pageNumberCenteredCheckHandler(e:Event):void
		{
			if(Infos.project)
			{
				Infos.project.pageNumberCentered = (pageNumberCentered.selected);
				
				PagesManager.instance.centerPageNumber(Infos.project.pageNumberCentered);
				
			}
		}
		
		private function projectBuildComplete():void
		{
			// validate should always be enabled as wr can always change the page number
			validate.enabled = true;
		}		
		
		private function setComboBoxListeners(flag:Boolean):void
		{
			typeCombo.removeEventListener(Event.SELECT, updateSizes);
			sizesCombo.removeEventListener(Event.SELECT, sizeChangedHandler);
			totalPagesCombo.removeEventListener(Event.SELECT, totalPagesChangedHandler);
			paperQualityCombo.removeEventListener(Event.SELECT, qualityPageChangedHandler);
			
			if(flag)
			{
				typeCombo.addEventListener(Event.SELECT, updateSizes);
				sizesCombo.addEventListener(Event.SELECT, sizeChangedHandler);
				totalPagesCombo.addEventListener(Event.SELECT, totalPagesChangedHandler);
				paperQualityCombo.addEventListener(Event.SELECT, qualityPageChangedHandler);
			}
		}
		
		/**
		 * ComboBox Quality pages changed handler
		 * Change value in projectVo
		 * Update Price
		 * */
		private function qualityPageChangedHandler(e:Event):void
		{
			//update price 
			updatePriceInConfigurator();
			
			if(Infos.project)
			{
				Infos.project.pagePaperQuality = paperQualityCombo.selectedItem.type;
				ProjectManager.instance.updateProjecPrice();
			}
		}
		
		/**
		 * ComboBox Pages changed handler
		 * Check inserts stock
		 * */
		private function totalPagesChangedHandler(e:Event):void
		{
			checkAlbumOptionsAndAvailability();
			//update price
			updatePriceInConfigurator()
		}
		
		/**
		 * ComboBox Sizes changed handler
		 * Check inserts stock
		 * */
		private function sizeChangedHandler(e:Event):void
		{
			checkAlbumOptionsAndAvailability();
			//update price
			updatePriceInConfigurator()
			//preview
			showPreview();
		}
		
		/**
		 * ComboBox project type changed handler
		 * -> update all other combo boxes data providers
		 * */
		private function updateSizes(e:Event):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			
			//sizes
			sizesArray  = [];
			var list:XMLList = xml.content.types.node.(@type==typeCombo.selectedItem.type).sizes.node;
			var selectedIndex:int = 0;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("album.prefix."+list[i].@id)+"  ("+list[i].@size+")" ;
				item.id = String(list[i].@id);
				item.size = String(list[i].@size);
				sizesArray.push(item);
				//selected Item default value
				if(list[i].@defaultValue == "true")
					selectedIndex = i;
				//selected Item existing value
				if(Infos.project && Infos.project.docPrefix == item.id)
					selectedIndex = i;
			}
			sizesCombo.items = sizesArray;
			sizesCombo.draw();
			sizesCombo.selectedIndex = selectedIndex;
			
			//updatequality papers
			updateQualityPapers(null);
			
		}
		
		private function updateQualityPapers(e:Event):void
		{
			//PAGES QUALITY
			qualitiesArray  = [];
			var list:XMLList = xml.content.types.node.(@type==typeCombo.selectedItem.type).page_qualities.quality;
			var selectedIndex:int = -1;
			var defaultIndex:int = 0;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("leftab.project.album.paper.quality."+list[i].@type);
				item.type = String(list[i].@type);
				if(Boolean(list[i].@defaultType) == true) defaultIndex = i;
				if(Infos.project && Infos.project.pagePaperQuality == item.type)
				{
					selectedIndex = i;					
				}
				qualitiesArray.push(item);
			}
			if(selectedIndex == -1)
				selectedIndex = defaultIndex;
			
			paperQualityCombo.items = qualitiesArray;
			paperQualityCombo.draw();
			paperQualityCombo.selectedIndex = selectedIndex;
			
			//enable conditions
			paperQualityCombo.enabled = (list.length() > 1);
			
			//then update size
			updateTotalPageNumber(null);
		}
		
		private function updateTotalPageNumber(e:Event):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			
			//sizes
			totalPagesArray  = [];
			var node:XML = XML(xml.content.types.node.(@type==typeCombo.selectedItem.type));
			var minPage:int = int(node.@pagesMin);
			var maxPage:int = int(node.@pagesMax);
			
			var currentPageNum : Number = (Infos.project)? Infos.project.flooredNumPages : 0;
			var numPageComoboSelectedIndex : Number = 0;
			
			for (var i:int = minPage; i <= maxPage; i+=10) 
			{
				var item:Object = {}
				item.label = String(i) + " " + ResourcesManager.getString("common.pages");
				item.id = String(i);
				
				totalPagesArray.push(item);
				
				if( currentPageNum > 0 && i == currentPageNum ) numPageComoboSelectedIndex = totalPagesArray.length-1;
			}
			totalPagesCombo.items = totalPagesArray;
			totalPagesCombo.draw();
			totalPagesCombo.selectedIndex = numPageComoboSelectedIndex;
			
			
			//preview
			showPreview();
			//put back listeners
			setComboBoxListeners(true);
			//show/hide stuff in configurator
			updateConfigurator();
			//update price
			updatePriceInConfigurator()
				
		}
		
		
		
		/**
		 * Upate configuration price
		 * desktop and dashBoard only
		 * */
		private function updatePriceInConfigurator():void
		{
			CONFIG::offline
				{
					if(Infos.IS_DESKTOP && forDashboard)
					{
						var currentDocType:String = getSelectedDocType();
						var minimumPrice:Number = PriceManager.instance.getPriceFromDash(currentDocType,paper.selected, coatedPage.selected, String(paperQualityCombo.selectedItem.type), null, typeCombo.selectedItem.type);
						if(typeCombo.selectedItem.type == ProductsCatalogue.ALBUM_CONTEMPORARY)
							minimumPrice += 9.5;
						priceWrapper.setLabel(ResourcesManager.getString("price.minimum.label")+" "+String(minimumPrice)+"€",false);
					}
					
				}
		}
		
		/**
		 * Create the image path based on ID's
		 * then load image
		 * */
		private function showPreview():void
		{
			var imageName:String = typeCombo.selectedItem.type + "_" + sizesCombo.selectedItem.id;
			if(typeCombo.selectedItem.type == ProductsCatalogue.ALBUM_CONTEMPORARY || typeCombo.selectedItem.type == ProductsCatalogue.ALBUM_CLASSIC)
			{
				if(paper.selected)
					imageName = imageName+"_paper";
			}
			loadPreview(imageName);
		}
		
		/**
		 * Show / Hide components based on the chosen prodcut
		 * */
		private function updateConfigurator():void
		{
			checkAlbumOptionsAndAvailability();
		}
				
		/**
		 * Check if current selected product allow inserts or not
		 * Check if papers are in stock for the selected product
		 * 
		 * */
		private function checkAlbumOptionsAndAvailability():void
		{
			//INSERTS
			
			var paperAllow:Boolean = true;
			//Only classic and contemporaty are inserts allowed.
			switch(typeCombo.selectedItem.type)
			{
				case ProductsCatalogue.ALBUM_CLASSIC:
				case ProductsCatalogue.ALBUM_CONTEMPORARY:
					paperAllow = true;
					break;
				
				case ProductsCatalogue.ALBUM_CASUAL:
				case ProductsCatalogue.ALBUM_TRENDY:
					paperAllow = false;
					break;
			}
					
			//Inserts are not allowed, no need to check stock
			if(!paperAllow)
			{
				//update price if project
				//paperCheckHandler(null); Removed this on the 9.15.01 : Don't think this is useful.
			}
			else
			{
				//CHECK STOCK
				//Retrieve product type code (101, 102...)
				var selectedProjectTypeCode:String = sizesCombo.selectedItem.id;
				selectedProjectTypeCode += totalPagesCombo.selectedItem.id;
				var selectedProjectID:String = ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
				//Check stock XML
				var result:XMLList = ProjectManager.instance.coversClassicOptionsXML.cover_stock.inserts.node.(@product == selectedProjectID && @stock.contains("ok"));
				paperAllow = (result.length() > 0);
				
				//update price if project
				//paperCheckHandler(null); Removed this on the 9.15.01 : Don't think this is useful.
			}
			
			//Update ui
			paper.enabled = paperAllow;
			paper.alpha = (paperAllow)?1:.2;
			paper.selected = (paperAllow)?(Infos.project)?Infos.project.paper:paper.selected:false;
			var paperCheckBoxLabel:String = ResourcesManager.getString("lefttab.project.album.paper") + " (" + PriceManager.instance.getPaperCost(getSelectedDocType()) +"€)";
			paper.label = paperCheckBoxLabel;
			
			//COATED OPTION
			checkCoatedOption();
			
			
		}
		
		/**
		 * Check coated combo state
		 */
		private function checkCoatedOption():void
		{
			var coatedNode:XML = XML(xml.content.types.node.(@type==typeCombo.selectedItem.type).page_coating);
			var coatedAllow:Boolean = (coatedNode.@enabled == "true");
			
			if(!coatedAllow)
			{
				coatedPage.selected = coatedPage.enabled = false;
				if(Infos.project)
					Infos.project.coated = false;
			}
			else
			{
				coatedPage.enabled = true;
				
				if(Infos.project)
				{
					coatedPage.selected = Infos.project.coated;
				}
				else
				{
					var coatedDefaultValue:Boolean = (coatedNode.@defaultValue == "true");
					coatedPage.selected = coatedDefaultValue;
				}
			}
			
			coatedPage.label = ResourcesManager.getString("lefttab.project.album.coated") + " (" + PriceManager.instance.getCoatingCost(getSelectedDocType()) +"€)";
			infosCoating.x = coatedPage.x + coatedPage.realWidth() + 5;
		}
		
		/**
		 * Configurator Validation
		 * Happen when there is no project ready
		 * It creates a new project
		 * 
		 * or
		 * when applying an upgrade to the current project
		 * it update the current project vo
		 * 
		 * Also, it disable some options depending on the selected project
		 * 
		 * And finally checks for upgrades if available
		 * 
		 * */
		private function validateHandler():void
		{
			if(forDashboard)
			{
				TweenMax.delayedCall(0,Dashboard.instance.close);
			}
			
			var initParams:ProjectCreationParamsVo = new ProjectCreationParamsVo();
			
			var test:ProjectVo = Infos.project;
			//set paper value
			if(Infos.project)
			{
				Infos.project.paper = paper.selected;
				Infos.project.coated = coatedPage.selected;
				Infos.project.pageNumber = pageNumber.selected;
				Infos.project.pagePaperQuality = paperQualityCombo.selectedItem.type;
			}
			else
			{
				initParams.paper = paper.selected;
				initParams.coated = coatedPage.selected;
				initParams.pageNumber = pageNumber.selected;
				initParams.paperQuality = paperQualityCombo.selectedItem.type;
			}
			
			if(typeCombo.selectedItem.id == ProductsCatalogue.ALBUM_CLASSIC)
				initParams.coverType = (typeCombo.selectedItem.type == ProductsCatalogue.ALBUM_CONTEMPORARY)?ProductsCatalogue.COVER_CUSTOM:ProductsCatalogue.COVER_LEATHER;
			else
				initParams.coverType = "";
			
			/*var selectedProjectTypeCode:String = sizesCombo.selectedItem.id;
				selectedProjectTypeCode += totalPagesCombo.selectedItem.id;
			var selectedProjectID:String = ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);*/
			var selectedProjectDocType:String = getSelectedDocType();
			Debug.log("Project ID: "+selectedProjectDocType);
			
			if(!typeCombo.selectedItem.isUpgrade && !Infos.project)
			{
				validate.enabled = false;
				Infos.session.product_id = selectedProjectDocType;
				ProjectManager.instance.createNewProject(Infos.session.product_id,initParams);
				
			}
			else
			{
				var selectedPageNumber:int = int(totalPagesCombo.selectedItem.id)+1;
				var currentPageNumber:int = Infos.project.pageList.length;
				var debugCoverType:String  = Infos.project.coverType;
				if(selectedPageNumber < currentPageNumber)
				{
						PopupAbstract.Alert(ResourcesManager.getString("popup.project.upgrade.less.pages.title"),ResourcesManager.getString("popup.project.upgrade.less.pages.description"), true,function(p:PopupAbstract):void{
						ProjectManager.instance.updateCurrentAlbumProject(selectedProjectDocType, typeCombo.selectedItem.type, initParams.coverType);
						UserActionManager.instance.addAction(UserActionManager.TYPE_PROJECT_UPGRADE);
					});
				}
				else
				{
					ProjectManager.instance.updateCurrentAlbumProject(selectedProjectDocType, typeCombo.selectedItem.type, initParams.coverType);
					UserActionManager.instance.addAction(UserActionManager.TYPE_PROJECT_UPGRADE);
				}
				
			}

			//disable ui
			sizesCombo.enabled = false;
			//totalPagesCombo.enabled = false;	
			
			//check type upgrades
			var hasTypeUpgrade:Boolean = checkTypeUpgrades(sizesCombo.selectedItem.id);
			
			//if no type upgrade, check for size upgrades
			if(!hasTypeUpgrade)
				checkSizeUpgrades(sizesCombo.selectedItem.id);
			
			//update btn label
			if(Infos.project)
				validate.setLabel("common.update.project");
		}
		
		/**
		 * Get selected docType
		 */
		private function getSelectedDocType():String
		{
			var selectedProjectTypeCode:String = sizesCombo.selectedItem.id;
			selectedProjectTypeCode += totalPagesCombo.selectedItem.id;
			return ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
		}
		
		
		/**
		 * get current type project upgrades
		 * at this point, only classic and contemporain have type upgrade, and its between each other
		 * size cannot change (sizecombo enabled = false);
		 * if no type upgrade (casual and trendy) typecombo enabled = false
		 */
		private function checkTypeUpgrades(prefix:String):Boolean
		{
			var upgradeList:Array = getTypeUpgrades(prefix);
			if(upgradeList.length>0)
			{
				//typeCombo.removeAll();
				typeCombo.items = upgradeList;	
				typeCombo.draw();
				typeCombo.enabled = true;
				sizesCombo.enabled = false;	
				return true;
			}
			else
			{
				typeCombo.enabled = false;	
				sizesCombo.enabled = false;	
				return false;
			}
		}
		
		/**
		 * get current project type upgrades
		 */
		private function getTypeUpgrades(prefix:String):Array
		{
			typesArray  = [];
			var list:XMLList = xml..album_upgrades.descendants(prefix).node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("album.type."+list[i].@type);
				item.type = String(list[i].@type);
				item.id = String(list[i].@id);
				item.isUpgrade = true;
				typesArray.push(item);
			}
			return typesArray;
		}
		
		
		/**
		 * get current syize project upgrades
		 * at this point, only casual and trendy have size upgrade
		 * type cannot change (typecombo enabled = false);
		 * if no size upgrade (classic & contemporain) sizeCombo enabled = false
		 */
		private function checkSizeUpgrades(prefix:String):Boolean
		{
			var upgradeList:Array = getSizeUpgrades(prefix);
			if(upgradeList.length>0)
			{
				//typeCombo.removeAll();
				sizesCombo.items = upgradeList;	
				sizesCombo.draw();
				sizesCombo.enabled = true;
				typeCombo.enabled = false;	
				return true;
			}
			else
			{
				typeCombo.enabled = false;	
				sizesCombo.enabled = false;	
				return false;
			}
		}
		
		/**
		 * get current project upgrades
		 */
		private function getSizeUpgrades(prefix:String):Array
		{
			sizesArray  = [];
			var list:XMLList = xml..album_size_upgrades.descendants(prefix).node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("album.prefix."+list[i].@id)+"  ("+list[i].@size+")" ;
				item.id = String(list[i].@id);
				item.size = String(list[i].@size);
				item.isUpgrade = true;
				sizesArray.push(item);
			}
			return sizesArray;
		}
		
		
		/**
		 * Calendar type selected handler
		 */
		protected function weekDayComboSelectHandler(event:Event):void
		{
			if(ProjectManager.instance.ready)
			{
				ProjectManager.instance.project.calendarStartDayIndex = sizesCombo.selectedItem.id;
				if(PagesManager.instance.currentEditedPage)
					PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			}
		}
		
		/**
		 * Calendar start month selected handler
		 */
		protected function monthComboComboSelectHandler(event:Event):void
		{
			if(ProjectManager.instance.ready)
			{
				ProjectManager.instance.project.calendarStartMonthIndex = totalPagesCombo.selectedItem.id;
				/*PageVo.MONTH_INDEX = ProjectManager.instance.project.calendarStartMonthIndex;
				var pageList:Vector.<PageVo> = ProjectManager.instance.project.pageList;
				for (var i:int = 0; i < pageList.length; i++) 
				{
					var pageVo:PageVo = pageList[i];
					pageVo.monthIndex = -1;
					pageVo.updateMonthIndex();
				}*/
				PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			}
		}
		
		
		
		/**
		 * Load calendar preview image
		 * */
		private function loadPreview(imageName:String):void
		{
			cleanPreview();
			preview.addChild(sig.getImage(Infos.config.getValue("imagePath") + THUMB_PATH + imageName + ".png",true,preview.getBounds(this)));
			
		}
		
		/**
		 * Remove previous preview if necessary
		 * */
		private function cleanPreview():void
		{
			if(preview.numChildren > 0)
			{
				var total:int = preview.numChildren;
				for (var i:int = 0; i < total; i++) 
				{
					var child:DisplayObject;
					if(preview.numChildren > 0)
					{
						child = preview.getChildAt(0)
						preview.removeChild(child);
					}
							
				}
			}
			
		}
		
		/**
		 * Draw bg
		 */
		private function drawBg():void
		{
			graphics.clear();
			graphics.beginFill(0xFFFFFF);
			graphics.drawRect(0,0,width,height + 10);
			graphics.endFill();
		}
		
		
	}
}