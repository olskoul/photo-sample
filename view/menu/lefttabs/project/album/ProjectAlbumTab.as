package view.menu.lefttabs.project.album
{
	import flash.geom.Point;
	
	import data.TutorialStepVo;
	
	import manager.TutorialManager;
	
	import view.menu.lefttabs.LeftTabContent;
	import view.menu.lefttabs.project.ProjectSimpleItemHeader;
	
	public class ProjectAlbumTab extends LeftTabContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/calendar/";
		private var configurator:ProjectAlbumConfigurator;
		private var tutorialVo : TutorialStepVo;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function ProjectAlbumTab(xmlData:XML)
		{
			super(xmlData);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		/**
		 * init the view (called when show method is called and initiated is fals)
		 */
		override public function init():void
		{
			super.init();
			
			//header
			var listHeader:ProjectSimpleItemHeader = new ProjectSimpleItemHeader("lefttab.project.album.title");
			listHeader.init();
			//Content -> Create Album options configurator
			configurator = new ProjectAlbumConfigurator(xmlData);
			accordion.addAccordionItem(listHeader, configurator, "AlbumType",true, true, false);
						
			// update visible items
			accordion.updateItems();
			
			
			
		}
		
		override public function show():void
		{
			super.show();
			configurator.update();
		}
		
	}
}