package view.menu.lefttabs.project.album
{
	import be.antho.data.ResourcesManager;
	
	import comp.accordion.AccordionItemHeader;
	
	import data.AlbumVo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import mcs.leftmenu.accordion.item.header.project.view;
	
	
	
	public class ProjectAlbumItemHeader extends AccordionItemHeader
	{
		public var step:String;
		
		public function ProjectAlbumItemHeader()
		{
			super();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{
			var view:MovieClip = new mcs.leftmenu.accordion.item.header.project.view(); // SWC assets
			ResourcesManager.setText(view.title,"lefttab.project.step."+step+".title"); 
			view.index.text = getIndexByStepType();
			addChild(view);
			buttonMode = true;
			mouseChildren = false;
		}
		
		private function getIndexByStepType():String
		{
			var index:int;
			switch(step)
			{
				case ProjectAlbumTab_old.STEP_ALBUM_TYPE:
					index = 1;
					break;
				
				case ProjectAlbumTab_old.STEP_ALBUM_SIZE:
					index = 2;
					break;
				
				case ProjectAlbumTab_old.STEP_ALBUM_PAGES:
					index = 3;
					break;
				
				case ProjectAlbumTab_old.STEP_VALIDATE:
					index = 4;
					break;
					
			}
			return String(index);
		}
	}
}