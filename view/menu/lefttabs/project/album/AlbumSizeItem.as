package view.menu.lefttabs.project.album
{
	import comp.ThumbItem;
	
	import data.ThumbVo;
	
	import flash.events.Event;
	
	public class AlbumSizeItem extends ThumbItem
	{
		private var scaleFactor:Number;
		
		public function AlbumSizeItem(dataVo:ThumbVo, scaleFactor:Number)
		{
			super(dataVo);
			this.scaleFactor = scaleFactor;
		}
		
		override protected function activate(e:Event=null):void
		{
			super.activate();
			thumb.scaleX = thumb.scaleX * scaleFactor;
			thumb.scaleY = thumb.scaleY * scaleFactor;
			
			thumb.x = (container.width - thumb.width) *.5;
			thumb.y = (container.height - thumb.height) *.5;
		}
	}
}