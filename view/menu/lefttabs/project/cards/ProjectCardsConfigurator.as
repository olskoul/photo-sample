package view.menu.lefttabs.project.cards
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.ComboBox;
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.bit101.components.ListItem;
	import com.bit101.components.NumericStepper;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.SimpleImageGetter;
	
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemContent;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	import comp.button.SimpleThinButton;
	import comp.combobox.ComboBoxTitTac;
	import comp.label.LabelTictac;
	
	import data.Infos;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectCreationParamsVo;
	import data.ProjectVo;
	
	import library.envelope.view;
	
	import manager.PagesManager;
	import manager.ProjectManager;
	
	import offline.manager.PriceManager;
	
	import utils.Colors;
	import utils.TextFormats;
	
	import view.dashboard.Dashboard;
	
	public class ProjectCardsConfigurator extends AccordionItemContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/cards/";
		
		private var xml:XML;
		private var typesArray:Array;
		private var orientationArray:Array;
		private var modelsArray:Array;
		private var envelopesArray:Array;
		private var typeCombo:ComboBoxTitTac;
		private var modelCombo:ComboBoxTitTac;
		private var envelopesCombo:ComboBoxTitTac;
		private var orientationCombo:ComboBoxTitTac;
		private var preview:Sprite;
		private var _selectedType:String;
		private var sig:SimpleImageGetter;
		private var validate:SimpleButton;

		private var customWidth:NumericStepper;

		private var customSize:HBox;

		private var customHeight:NumericStepper;

		private var multipleSize:HBox;

		private var multipleWidth:NumericStepper;

		private var multipleHeight:NumericStepper;

		private var multipleMargin:NumericStepper;

		private var multipleOptions:VBox;

		private var envelope:MovieClip;

		private var labelOrientation:LabelTictac;

		private var labelModel:LabelTictac;

		private var labelEnvelope:LabelTictac;
		private var forDashboard:Boolean;

		private var labelType:LabelTictac;
		private var priceWrapper:SimpleThinButton;
		
		public function ProjectCardsConfigurator(xmlData:XML, _forDashboard:Boolean = false)
		{
			super();
			xml = xmlData;
			forDashboard = _forDashboard;
			construct();
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		//	OVERIDDENS
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		//	GETTER SETTERS
		////////////////////////////////////////////////////////////////

		public function get selectedType():String
		{
			return _selectedType;
		}

		public function set selectedType(value:String):void
		{
			_selectedType = value;
		}
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			super.destroy();
		}
		
		
		/**
		 * ------------------------------------ UPDATE -------------------------------------
		 */
		/**
		 * on view show, we update configurator to macth current project infos
		 */
		public function update():void
		{
			// update button
			updateBtnLabel();
		}
		
		private function updateBtnLabel():void
		{
			if(Infos.project)
				validate.setLabel("common.update.project");
			else{
				validate.setLabel("common.start.new.project");
			}
		}
		/**
		 * ------------------------------------ CONSTRUCT -------------------------------------
		 */
		private function construct():void
		{
			//this.opaqueBackground = 0x00ff00;
			
			
			var proj : ProjectVo = Infos.project;
			//types
			typesArray  = [];
			var list:XMLList = xml..types.node;
			var selectedIndex:int = 0;

			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("cards.type."+list[i].@id);
				item.id = String(list[i].@id);
				item.docidstr = String(list[i].@docidstr);
				typesArray.push(item);
				
				//selected index based on loaded project type
				if(proj && proj.type == item.id)
				{
					selectedIndex = i;					
				}
			}
			
			labelType = new LabelTictac(this,0,0,ResourcesManager.getString("cards.configurator.type.label")); 
			typeCombo = new ComboBoxTitTac(this,0,0,typesArray[0].label,typesArray);
			typeCombo.showScrollButtons = false;
			typeCombo.selectedIndex = selectedIndex;

			//Orientation
			orientationCombo = new ComboBoxTitTac(this,0,0, null, orientationArray);
			orientationCombo.showScrollButtons = false;
			orientationCombo.selectedIndex = 0;
			labelOrientation = new LabelTictac(this,0,0,ResourcesManager.getString("cards.configurator.orientation.label"),11,Colors.GREY_DARK, "right");
			
			//Model
			modelCombo = new ComboBoxTitTac(this,0,0,null,modelsArray);
			modelCombo.showScrollButtons = false;
			modelCombo.selectedIndex = 0;
			labelModel = new LabelTictac(this,0,0,ResourcesManager.getString("cards.configurator.model.label"),11,Colors.GREY_DARK, "right");
			
			//Envelopes
			envelopesCombo = new ComboBoxTitTac(this,0,0,null,envelopesArray);
			envelopesCombo.showScrollButtons = false;
			labelEnvelope = new LabelTictac(this,0,0,ResourcesManager.getString("cards.configurator.envelope.label"),11,Colors.GREY_DARK, "right");
			
			//image preview
			envelope = new library.envelope.view();
			addChild(envelope);
			
			//image preview
			preview = new Sprite();
			preview.graphics.clear();
			preview.graphics.lineStyle(1,Colors.WHITE);
			preview.graphics.beginFill(0xFFFFFF);
			preview.graphics.drawRect(0,0,220,129);
			preview.graphics.endFill();
			addChild(preview);
			
			//image loader
			sig = new SimpleImageGetter();
			
			//price (offline only and dashboard only)
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper = new SimpleThinButton("0",Colors.GREY_LIGHT,null,Colors.YELLOW,Colors.BLACK,Colors.BLACK,1,false,true);
				priceWrapper.forcedWidth = 80;
				addChild(priceWrapper);
			}
			
			//validate button
			validate = new SimpleSquareButton("common.start.new.project", Colors.BLUE, validateHandler, Colors.GREEN, Colors.WHITE, Colors.WHITE, 1,false,-1);
			validate.forcedWidth = 250;
			addChild(validate);			
			
			var hasTypeUpgrages:Boolean;
			if(proj)
				hasTypeUpgrages = checkTypeUpgrades(proj.docPrefix);
			
			//dafault values
			updateOrientation(null);
			
			//image loader
			sig = new SimpleImageGetter();		
			
			//disabele/enable ui
			disableUI(hasTypeUpgrages);//updateConfigurator();
			
			//Show preview
			showPreview();
				
			//listeners
			setComboBoxListeners(true);
			
			
			//Layout
			if(!forDashboard)
				layItOut();
			else
				layItOutForDash();
			
			//btn label
			updateBtnLabel();
		}
		
		private function layItOut():void
		{
			
			//Type
			labelType.x = 15;
			labelType.y = 0;
			typeCombo.x = labelType.x; 
			typeCombo.y = labelType.y + labelType.height + 10; 
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 30);

			
			//Orientation
			orientationCombo.setSize(150, 30);
			orientationCombo.x = Accordion.MAX_WIDTH - orientationCombo.width-15;
			orientationCombo.y = typeCombo.y + typeCombo.height + 15;
			
			labelOrientation.x = 15;
			labelOrientation.y = orientationCombo.y + (orientationCombo.height - labelOrientation.height)/2;
			
			//Model
			modelCombo.setSize(150, 30);
			modelCombo.x = Accordion.MAX_WIDTH - modelCombo.width-15;
			modelCombo.y = orientationCombo.y + orientationCombo.height + 15;
			
			labelModel.x = 15;
			labelModel.y = modelCombo.y + (modelCombo.height - labelModel.height)/2;
			
			//Envelopes
			envelopesCombo.setSize(150, 30);
			envelopesCombo.x = Accordion.MAX_WIDTH - envelopesCombo.width-15;
			envelopesCombo.y = modelCombo.y + modelCombo.height + 15;
			
			labelEnvelope.x = 15;
			labelEnvelope.y = envelopesCombo.y + (envelopesCombo.height - labelEnvelope.height)/2;
			
			//image preview
			envelope.x = labelEnvelope.x;
			envelope.y = labelEnvelope.y + labelEnvelope.height + 15;	
			
			//image preview
			preview.x = (Accordion.MAX_WIDTH - preview.width) >> 1;
			preview.y = 270;
			
			//validate button
			validate.forcedWidth = 250;
			validate.x = (Accordion.MAX_WIDTH - validate.width) >> 1;
			validate.y = preview.y + preview.height + 15;
			
		}
		
		private function layItOutForDash():void
		{
			
			//Type
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 30);
			labelType.x = 15;
			labelType.y = 0;
			typeCombo.x = labelType.x; 
			typeCombo.y = labelType.y + labelType.height + 10; 

			
			//Orientation
			orientationCombo.setSize(150, 30);
			orientationCombo.x = Accordion.MAX_WIDTH - orientationCombo.width-15;
			orientationCombo.y = typeCombo.y + typeCombo.height + 15;
			
			labelOrientation.x = 15;
			labelOrientation.y = orientationCombo.y + (orientationCombo.height - labelOrientation.height)/2;
			
			//Model
			modelCombo.setSize(150, 30);
			modelCombo.x = Accordion.MAX_WIDTH - modelCombo.width-15;
			modelCombo.y = orientationCombo.y + orientationCombo.height + 15;
			
			labelModel.x = 15;
			labelModel.y = modelCombo.y + (modelCombo.height - labelModel.height)/2;
			
			//Envelopes
			envelopesCombo.setSize(150, 30);
			envelopesCombo.x = Accordion.MAX_WIDTH - envelopesCombo.width-15;
			envelopesCombo.y = modelCombo.y + modelCombo.height + 15;
			
			labelEnvelope.x = 15;
			labelEnvelope.y = envelopesCombo.y + (envelopesCombo.height - labelEnvelope.height)/2;
			
			
			
			//image preview
			preview.x = typeCombo.x + typeCombo.width + 30;
			preview.y = typeCombo.y;
			
			//Envelope preview
			envelope.x = preview.x + ((preview.width - envelope.width)*.5);
			envelope.y = envelopesCombo.y;	
			
			//validate button
			validate.forcedWidth = 250;
			validate.x = width - validate.width >> 1;
			validate.y = envelopesCombo.y + envelopesCombo.height + 30;
			
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper.x = 15;
				priceWrapper.y = validate.y;
			}

			
		}
		
		private function setComboBoxListeners(flag:Boolean):void
		{
			typeCombo.removeEventListener(Event.SELECT, updateOrientation);
			//orientationCombo.removeEventListener(Event.SELECT, updateModel);
			modelCombo.removeEventListener(Event.SELECT, modelChangedHandler);
			envelopesCombo.removeEventListener(Event.SELECT, envelopesComboChangedHandler);
			
			if(flag)
			{
				typeCombo.addEventListener(Event.SELECT, updateOrientation);
				//orientationCombo.addEventListener(Event.SELECT, updateModel);
				modelCombo.addEventListener(Event.SELECT, modelChangedHandler);
				envelopesCombo.addEventListener(Event.SELECT, envelopesComboChangedHandler);
			}
		}
		
		protected function modelChangedHandler(event:Event):void
		{
			updatePriceInConfigurator();
			
		}
		
		private function updateOrientation(e:Event):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			
			//orientation
			var selectedIndex:int = 0;
			orientationArray  = [];
			var list:XMLList = xml.content.types.node.(@id==typeCombo.selectedItem.id).orientation.node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("cards.orientation."+list[i].@id);
				item.id = String(list[i].@id);
				item.docidstr = String(list[i].@docidstr);
				orientationArray.push(item);
				
				if(Infos.project && Infos.project.docOrientation == item.id)
				{
					selectedIndex = i;					
				}
			}
			orientationCombo.items = orientationArray;
			orientationCombo.draw();
			orientationCombo.selectedIndex = selectedIndex;
			
			//then update size
			updateModel(null);
			
			//update price
			updatePriceInConfigurator();
		}
		
		private function updateModel(e:Event):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			var selectedIndex:int = 0;
			//sizes
			modelsArray  = [];
			var list:XMLList = xml.content.types.node.(@id==typeCombo.selectedItem.id).orientation.node.(@id==orientationCombo.selectedItem.id).model;
			var ids:Array = String(list[0].@ids).split(";");
			
			for (var i:int = 0; i < ids.length; i++) 
			{
				var item:Object = {}
				item.label = getModellabel(ids[i]) +" "+ ResourcesManager.getString("cards.configurator.model.label");
				item.id = ids[i];
				modelsArray.push(item);
				
				if(Infos.project)
				{
					var modelString:String = Infos.project.docCode.split(Infos.project.docPrefix)[1];
					if(item.id == modelString)
					selectedIndex = i;					
				}
				
			}
			modelCombo.items = modelsArray;
			modelCombo.draw();
			modelCombo.selectedIndex = selectedIndex;
			
			//then update style
			updateEnvelopes(null);
		}
		
		private function updateEnvelopes(e:Event):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			var selectedIndex:int = 0;
			
			envelopesArray  = [];
			var list:XMLList = xml.content.types.node.(@id==typeCombo.selectedItem.id).orientation.node.(@id==orientationCombo.selectedItem.id).envelope;
			
			var ids:Array = String(list[0].@ids).split(";");
			for (var i:int = 0; i < ids.length; i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("cards.configurator.envelope."+ids[i]);
				item.id = ids[i];
				item.color =  xml.content.envelopes.node.(@id == ids[i]).@color;
				envelopesArray.push(item);
				
				if(Infos.project && Infos.project.envelope == item.id)
				{
					selectedIndex = i;					
				}
			}
			
			/*if(selectedIndex == 0)
				envelope.visible = false;*/
			
			envelopesCombo.items = envelopesArray;
			envelopesCombo.draw();
			envelopesCombo.selectedIndex = selectedIndex;
			envelopesCombo.enabled = true;

			//setup enveloppe
			envelopesComboChangedHandler(null);
			
			//preview
			showPreview();
			
			//show/hide stuff in configurator
			disableUI(true);
			
			//put back listeners
			setComboBoxListeners(true);
	
		}
		
		/**
		 * DISalbe/enable UI based on the validated project
		 * */
		private function disableUI(hasTypeUpgrade:Boolean):void
		{
			typeCombo.enabled = (!Infos.project)?true:hasTypeUpgrade; 
			validate.enabled = (!Infos.project)?true:hasTypeUpgrade;
			
			//reset all to true
			orientationCombo.enabled = true;
			modelCombo.enabled = true;
			envelopesCombo.enabled = true;
			envelope.visible = true;
			
			switch (typeCombo.selectedItem.docidstr)
			{
			case "PC":
			case "PX":
				orientationCombo.enabled = false;
				envelopesCombo.enabled = false;
				envelope.visible = false;
				break;
			
			case "FQP":
			case "AQP":
				orientationCombo.enabled = false;
				break;
			
			case "ACP":
			case "ACL":
			case "AXP":
			case "AXL":
			case "AUP":
			case "AUL":
			case "FCP":
			case "FCL":
			case "GCP":
			case "GCL":
			case "FUP":
			case "FUL":
			case "FQP":
				orientationCombo.enabled = (!Infos.project);
				break;
			
			}
			
			if(envelopesCombo.selectedItem.id == "none")
				envelope.visible = false;
			
		}
		
		protected function styleChosen(event:Event):void
		{
			//preview
			showPreview();
		}	
		
		/**
		 *Return envelopes label red, green, blue etc...
		 * */
		private function envelopesComboChangedHandler(event:Event):void
		{
			if(Infos.project)
			{
				Infos.project.envelope = envelopesCombo.selectedItem.id;
				ProjectManager.instance.updateProjecPrice();				
			}
			
			
			if(envelopesCombo.selectedItem.id == "none")
			{
				envelope.visible = false;
			}
			else
			{
				envelope.visible = true;
				TweenMax.killTweensOf(envelope.bkg);
				TweenMax.to(envelope.bkg, 0,{tint:envelopesCombo.selectedItem.color});
			}
			
			//update price
			updatePriceInConfigurator();
		}
		
		
		/**
		 *Return envelopes label red, green, blue etc...
		 * */
		private function getEnvelopelabel(id:String):String
		{
			return String(xml.content.envelopes.node.(@id == id).@label);
		}		
		
		/**
		 *Return model label 10 x1, 2x5, 5x1 etc...
		 * */
		private function getModellabel(id:String):String
		{
			return String(xml.content.models.node.(@id == id).@label);
		}
		
		
		/**
		 * Create the image path based on ID's
		 * then load image
		 * */
		private function showPreview():void
		{
			var imageName:String = typeCombo.selectedItem.id + "_" + orientationCombo.selectedItem.id;
			
			loadPreview(imageName);
		}
		
		
		/**
		 * Configurator Validation
		 * Happen when there is no project ready
		 * It creates a new project
		 * 
		 * or
		 * when applying an upgrade to the current project
		 * it update the current project vo
		 * 
		 * Also, it disable some options depending on the selected project
		 * 
		 * And finally checks for upgrades if available
		 * 
		 * */
		private function validateHandler():void
		{
			
			if(forDashboard)
			{
				TweenMax.delayedCall(0,Dashboard.instance.close);
			}
			
			var initParams:ProjectCreationParamsVo = new ProjectCreationParamsVo();
			initParams.envelope = envelopesCombo.selectedItem.id;
			
			/*var selectedProjectTypeCode:String = String(xml.content.types.node.(@id == typeCombo.selectedItem.id).orientation.node.(@id == orientationCombo.selectedItem.id).@docidstr);
				selectedProjectTypeCode += modelCombo.selectedItem.id;
			var selectedProjectDocType:String = ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);*/
			var selectedProjectDocType:String = getSelectedDocType();
			trace("Project ID: "+selectedProjectDocType);
						
			if(!typeCombo.selectedItem.isUpgrade)
			{
				Infos.session.product_id = selectedProjectDocType;
				ProjectManager.instance.createNewProject(Infos.session.product_id,initParams);
				validate.enabled = false;
			}
			else
			{
				if(Infos.session.product_id != selectedProjectDocType)
				{
					Infos.session.product_id = selectedProjectDocType;
					ProjectManager.instance.updateCurrentCardsProject(Infos.session.product_id);
				}
				
			}
			
			
			var hasTypeUpgrages:Boolean;
			if(Infos.project)
				hasTypeUpgrages = checkTypeUpgrades(typeCombo.selectedItem.docidstr);
			
			//update btn label
			if(Infos.project)
				validate.setLabel("common.update.project");
			
		}
		
		/**
		 * Upate configuration price
		 * desktop and dashBoard only
		 * */
		private function updatePriceInConfigurator():void
		{
			CONFIG::offline
				{
					if(Infos.IS_DESKTOP && forDashboard)
					{
						var currentDocType:String = getSelectedDocType();
						var minimumPrice:Number = PriceManager.instance.getPriceFromDash(currentDocType, false, false, "", null);
						priceWrapper.setLabel(ResourcesManager.getString("price.minimum.label")+" "+String(minimumPrice)+"€",false);
					}
					
				}
		}
		
		/**
		 * Get selected docType
		 */
		private function getSelectedDocType():String
		{
			var selectedProjectTypeCode:String = String(xml.content.types.node.(@id == typeCombo.selectedItem.id).orientation.node.(@id == orientationCombo.selectedItem.id).@docidstr);
			selectedProjectTypeCode += modelCombo.selectedItem.id;
			var selectedProjectDocType:String = ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
			return ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
		}
		
		
		/**
		 * get current type project upgrades
		 * if no type upgrade typecombo enabled = false
		 */
		private function checkTypeUpgrades(prefix:String):Boolean
		{
			var upgradeList:Array = getTypeUpgrades(prefix);
			if(upgradeList.length>0)
			{
				typeCombo.items = upgradeList;	
				typeCombo.draw();
				for (var i:int = 0; i < upgradeList.length; i++) 
				{
					if(upgradeList[i].docidstr == prefix)
						typeCombo.selectedIndex = i;
				}
				
				return true;
			}
			else
			{	
				return false;
			}
		}
		
		/**
		 * get current project type upgrades
		 */
		private function getTypeUpgrades(prefix:String):Array
		{
			typesArray  = [];
			var list:XMLList = xml..card_upgrades.descendants(prefix).node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("cards.type."+list[i].@id);//ResourcesManager.getString("cards.type."+list[i].@docidstr);
				item.id = String(list[i].@id);
				item.docidstr = String(list[i].@docidstr);
				item.isUpgrade = true;
				typesArray.push(item);
			}
			return typesArray;
		}
		
		
		/**
		 * get current project upgrades
		 */
		/*private function getUpgrades(id:String):Array
		{
			typesArray  = [];
			var list:XMLList = xml..card_upgrades.descendants(id).node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("cards.type."+list[i].@id);
				item.id = String(list[i].@id);
				item.isUpgrade = true;
				typesArray.push(item);
			}
			return typesArray;
		}
		*/

		/**
		 * Load calendar preview image
		 * */
		private function loadPreview(imageName:String):void
		{
			cleanPreview();
			preview.addChild(sig.getImage(Infos.config.getValue("imagePath") + THUMB_PATH + imageName + ".jpg",true,preview.getBounds(this)));
			
		}
		
		/**
		 * Remove previous preview if necessary
		 * */
		private function cleanPreview():void
		{
			if(preview.numChildren > 0)
			{
				var total:int = preview.numChildren;
				for (var i:int = 0; i < total; i++) 
				{
					var child:DisplayObject;
					if(preview.numChildren > 0)
					{
						child = preview.getChildAt(0)
						preview.removeChild(child);
					}
					
				}
			}
			
		}
		
		/**
		 * Draw bg
		 */
		private function drawBg():void
		{
			graphics.clear();
			graphics.beginFill(0xFFFFFF);
			graphics.drawRect(0,0,width,height + 10);
			graphics.endFill();
		}
		
		
	}
}