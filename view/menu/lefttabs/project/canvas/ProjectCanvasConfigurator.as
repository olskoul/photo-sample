package view.menu.lefttabs.project.canvas
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.ComboBox;
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.bit101.components.ListItem;
	import com.bit101.components.NumericStepper;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.AutoAlphaPlugin;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import mx.events.ResizeEvent;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.SimpleImageGetter;
	
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemContent;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	import comp.button.SimpleThinButton;
	import comp.checkbox.CheckboxTitac;
	import comp.combobox.ComboBoxTitTac;
	import comp.label.LabelTictac;
	
	import data.Infos;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectCreationParamsVo;
	
	import manager.CanvasManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	
	import offline.data.CanvasPriceInfo;
	import offline.manager.PriceManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.TextUtils;
	
	import view.dashboard.Dashboard;
	
	public class ProjectCanvasConfigurator extends AccordionItemContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/canvas/old/";
		
		// data
		private var xml:XML;
		private var typesArray:Array;
		private var orientationArray:Array;
		private var sizesArray:Array;
		private var styleArray:Array;
		
		// combo's
		private var typeCombo:ComboBoxTitTac;
		private var orientationCombo:ComboBoxTitTac;
		private var sizeCombo:ComboBoxTitTac;
		private var styleCombo:ComboBoxTitTac;
		
		private var frameColorBox : HBox;
		private var frameColorCombo:ComboBoxTitTac;
		private var frameColorLabel:Label;
		
		private var preview:Sprite;
		private var _selectedType:String;
		private var sig:SimpleImageGetter;
		private var validate:SimpleButton;
		private var selectedMLayout:String;
		private var layoutGroup : Array = new Array();

		private var customWidth:NumericStepper;
		private var customSize:HBox;
		private var customHeight:NumericStepper;
		private var multipleSize:HBox;
		private var multipleWidth:NumericStepper;
		private var multipleHeight:NumericStepper;
		private var multipleMargin:NumericStepper;
		private var multipleOptions:VBox;
		private var forDashboard:Boolean;

		private var bg:Shape;
		private var priceWrapper:SimpleThinButton;
		
		// small flag to indiquate if we need to update/reset custom size 
		private var resetCustomSizeFlag:Boolean = true;
		

		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var labelType:LabelTictac;

		private var labelOrientation:LabelTictac;

		private var labelSize:LabelTictac;
		
		public function ProjectCanvasConfigurator(xmlData:XML, _forDashboard:Boolean = false)
		{
			super();
			xml = xmlData;
			forDashboard = _forDashboard;

			construct();
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTERS
		////////////////////////////////////////////////////////////////

		public function get selectedType():String
		{
			return _selectedType;
		}

		public function set selectedType(value:String):void
		{
			_selectedType = value;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			super.destroy();
		}
		
		
		/**
		 * ------------------------------------ UPDATE -------------------------------------
		 */
		/**
		 * on view show, we update configurator to macth current project infos
		 */
		public function update():void
		{
			// update button
			updateBtnLabel();
				
			
			// update all combos
			updateTypeCombo(true);
			
			// recover free format from save
			if(Infos.project && Infos.project.docPrefix == "CPF"){ 
				customWidth.value = Math.round(Infos.project.width/10);
				customHeight.value = Math.round(Infos.project.height/10);
			}
			
			// recover Mlayout from save
			if(Infos.project && Infos.project.canvasMLayout) selectedMLayout = Infos.project.canvasMLayout; // = recover from save
			if(!selectedMLayout) selectedMLayout = layoutGroup[0].id; //if(!selectedMLayout) selectMLayout(layoutGroup[0].id);
			
			// recover from save for multiple layout
			if(Infos.project && Infos.project.canvasMLayout){
				multipleMargin.value = Math.round(Infos.project.canvasMargin/10);
				multipleHeight.value = Math.round(Infos.project.height/10);
				multipleWidth.value = Math.round(Infos.project.width/10);
			}

			// update configurator
			//updateConfigurator();
		}
		
		private function updateBtnLabel():void
		{
			if(Infos.project)
				validate.setLabel("common.update.project");
			else{
				validate.setLabel("common.start.new.project");
				resetCustomSizeFlag = true;
			}
		}

		
		/**
		 * ------------------------------------ CONSTRUCT -------------------------------------
		 */
		
		private function construct():void
		{
			//this.opaqueBackground = 0x00ff00;
			
			// type combo (canvas, wood, kadapak, etc...)
			labelType = new LabelTictac(this,0,0,ResourcesManager.getString("canvas.configurator.type.label")); 
			typeCombo = new ComboBoxTitTac(this,0,0,null,typesArray);//),typesArray[0].label );//,typesArray);
			typeCombo.showScrollButtons = false;
			
			//Orientation ( landscape, portrait, multiple, etc...)
			labelOrientation = new LabelTictac(this,0,0,ResourcesManager.getString("canvas.configurator.orientation.label"),11); 
			orientationCombo = new ComboBoxTitTac(this,0,0, null, orientationArray);
			orientationCombo.showScrollButtons = false;
			
			//Size ( predefined size)
			labelSize = new LabelTictac(this,0,0,ResourcesManager.getString("canvas.configurator.size.label"),11); 
			sizeCombo = new ComboBoxTitTac(this,0,0,null,sizesArray);
			sizeCombo.showScrollButtons = false;
			
			//Style ( simple, pelemele, popart) 
			styleCombo = new ComboBoxTitTac(this,0,0,null,styleArray);
			styleCombo.showScrollButtons = false;
			
			var kadapakColors : Array = ["black", "silver", "gold", "red", "blue"];
			var kadapakColorsItem : Array = new Array();
			var kadapakColorSelectedIndex:Number = 0;
			for (var i:int = 0; i < kadapakColors.length; i++) 
			{
				kadapakColorsItem.push({id:kadapakColors[i], label:ResourcesManager.getString("kadapak.color."+kadapakColors[i])});
				if(Infos.project && Infos.project.canvasFrameColor && Infos.project.canvasFrameColor == kadapakColors[i]) kadapakColorSelectedIndex = i; // retrieve from save
			}
			
			// frame color
			frameColorBox = new HBox(this, 0, 0); 
			frameColorLabel = new Label(frameColorBox,0,0,ResourcesManager.getString("kadapak.color.label") + " " ,TextFormats.ASAP_BOLD(13)); 
			frameColorCombo = new ComboBoxTitTac(frameColorBox,0,0,null,kadapakColorsItem);
			frameColorCombo.showScrollButtons = false;
			frameColorCombo.height = 30;
			frameColorCombo.selectedIndex = kadapakColorSelectedIndex;
			
			
			//Multiple (multiple option view)
			multipleOptions = createMultipleOtionsView();
			addChild(multipleOptions);
			
			//Custom Size 
			customSize = new HBox(this, 0,0);
			
			// custom numeric steppers for multiple and free format orientation
			var widthBox:VBox = new VBox(customSize);
			var widthLabel:Label = new Label(widthBox,0,0,ResourcesManager.getString("lefttab.project.canvas.custom.size.width")+" (cm)", TextFormats.ASAP_REGULAR(13));
			customWidth = new NumericStepper(widthBox,0,0, customWidthChangeHandler);
						
			var heightBox:VBox = new VBox(customSize);
			var heightLabel:Label = new Label(heightBox,0,0,ResourcesManager.getString("lefttab.project.canvas.custom.size.height")+" (cm)", TextFormats.ASAP_REGULAR(13));
			customHeight = new NumericStepper(heightBox,0,0, customHeightChangeHandler);
			
			//image preview
			preview = new Sprite();
			preview.graphics.clear();
			preview.graphics.lineStyle(1,Colors.WHITE);
			preview.graphics.beginFill(0xFFFFFF);
			preview.graphics.drawRect(0,0,220,129);
			preview.graphics.endFill();
			addChild(preview);
			
			//image loader
			sig = new SimpleImageGetter();		
			
			//price (offline only and dashboard only)
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper = new SimpleThinButton("0",Colors.GREY_LIGHT,null,Colors.YELLOW,Colors.BLACK,Colors.BLACK,1,false,true);
				priceWrapper.forcedWidth = 80;
				addChild(priceWrapper);
			}
			
			//validate button
			validate = new SimpleSquareButton("common.start.new.project", Colors.BLUE, validateHandler, Colors.GREEN, Colors.WHITE, Colors.WHITE, 1,false,-1);
			validate.forcedWidth = 250;
			addChild(validate);		
			
			// bg // force good size to avoid accordion height bug
			bg = new Shape();
			addChildAt(bg,0);

			
			// Layout / state
			if(!forDashboard)
				layItOut();
			else
				layItOutForDash();
			
			// update
			update();
			
		}
		
		private function layItOut():void
		{
			// type combo (canvas, wood, kadapak, etc...)
			labelType.x = 15;
			labelType.y = 0;
			typeCombo.x = labelType.x;
			typeCombo.y = labelType.y + labelType.height + 10;
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 30);
			
			//Orientation ( landscape, portrait, multiple, etc...)
			labelOrientation.x = 15;
			labelOrientation.y = typeCombo.y + typeCombo.height + 15;
			orientationCombo.setSize(150, 30);
			orientationCombo.x = Accordion.MAX_WIDTH - orientationCombo.width-15;
			orientationCombo.y = labelOrientation.y - 5;
			
			
			//Size ( predefined size)
			labelSize.x = 15;
			labelSize.y = labelOrientation.y + labelOrientation.height + 15;
			sizeCombo.setSize(150, 30);
			sizeCombo.x = Accordion.MAX_WIDTH - sizeCombo.width-15;
			sizeCombo.y = labelSize.y - 5;
			
			
			//Style ( simple, pelemele, popart) 
			styleCombo.setSize(Accordion.MAX_WIDTH - 30, 30);
			styleCombo.x = 15;
			styleCombo.y = (customSize.visible) ? customSize.y + customSize.height + 15 : sizeCombo.y + sizeCombo.height + 15;
			//styleCombo.y = sizeCombo.y + sizeCombo.height + 15;
			
			
			
			// frame color
			frameColorBox.x = typeCombo.width + typeCombo.x - frameColorBox.width;
			frameColorBox.y = styleCombo.y + styleCombo.height + 15;

			frameColorLabel.x = 0;
			frameColorLabel.y = 5;
			frameColorCombo. x = 0;
			frameColorCombo. y = 0;
			
			
			//Multiple (multiple option view)
			multipleOptions.x = 15;
			multipleOptions.y = orientationCombo.y + orientationCombo.height + 35;
			
			//Custom Size 
			customSize.x = 15;
			customSize.y = sizeCombo.y;
			
			
			//image preview
			preview.x = (Accordion.MAX_WIDTH - preview.width) >> 1;
			preview.y = 270// 270; >> put to 380 as its currently the max position for the preview, it's changed in the update Configurator funciton. But here we want to put it to max to avoid bug of accordion mask height
	
			
			//validate button
			validate.forcedWidth = 250;
			validate.x = (Accordion.MAX_WIDTH - validate.width) >> 1;
			validate.y = preview.y + preview.height + 15;
			
			// bg // force good size to avoid accordion height bug
			bg.graphics.clear();
			bg.graphics.beginFill(0xffffff,1);
			bg.graphics.drawRect(0,0,200,550);
			bg.graphics.endFill();
		}
		
		private function layItOutForDash():void
		{
			// type combo (canvas, wood, kadapak, etc...)
			labelType.x = 15;
			labelType.y = 0;
			typeCombo.x = labelType.x;
			typeCombo.y = labelType.y + labelType.height + 10;
			typeCombo.setSize(Accordion.MAX_WIDTH - 30, 30);
			
			//Orientation ( landscape, portrait, multiple, etc...)
			labelOrientation.x = 15;
			labelOrientation.y = typeCombo.y + typeCombo.height + 15;
			orientationCombo.setSize(150, 30);
			orientationCombo.x = Accordion.MAX_WIDTH - orientationCombo.width-15;
			orientationCombo.y = labelOrientation.y - 5;
			
			
			//Size ( predefined size)
			labelSize.x = 15;
			labelSize.y = orientationCombo.y + orientationCombo.height + 15;
			sizeCombo.setSize(150, 30);
			sizeCombo.x = Accordion.MAX_WIDTH - sizeCombo.width-15;
			sizeCombo.y = labelSize.y - 5;
			
			
			//Style ( simple, pelemele, popart) 
			/*styleCombo.x = 15;
			styleCombo.y = sizeCombo.y + sizeCombo.height + 15;
			styleCombo.setSize(Accordion.MAX_WIDTH - 30, 30);*/
			//Customer asked to remove this dropdown
			styleCombo.parent.removeChild(styleCombo);

			frameColorLabel.x = 0;
			frameColorLabel.y = 5;
			frameColorCombo. x = 0;
			frameColorCombo. y = 0;
			
			
			//Multiple (multiple option view)
			multipleOptions.x = 15;
			multipleOptions.y = orientationCombo.y + orientationCombo.height + 35;
			
			//Custom Size 
			customSize.x = 15;
			customSize.y = sizeCombo.y;
			
			
			//image preview
			preview.x = typeCombo.x + typeCombo.width + 30;
			preview.y = typeCombo.y;// 270; >> put to 380 as its currently the max position for the preview, it's changed in the update Configurator funciton. But here we want to put it to max to avoid bug of accordion mask height
			
			// frame color
			frameColorBox.x = preview.x + preview.width - frameColorBox.width;
			frameColorBox.y = preview.y + preview.height + 5;

			// bg // force good size to avoid accordion height bug
			bg.graphics.clear();
			bg.parent.removeChild(bg);
			
			//validate button
			validate.forcedWidth = 250;
			validate.x = width - validate.width >> 1;
			validate.y = height - validate.height - 0;
			
			if(Infos.IS_DESKTOP && forDashboard)
			{
				priceWrapper.x = 15;
				priceWrapper.y = validate.y;
			}
		}
		
		private function setComboBoxListeners(flag:Boolean):void
		{
			typeCombo.removeEventListener(Event.SELECT, updateOrientationCombo);
			orientationCombo.removeEventListener(Event.SELECT, updateSizeCombo);
			sizeCombo.removeEventListener(Event.SELECT, updateStyleCombo);
			styleCombo.removeEventListener(Event.SELECT, styleChosen);
			frameColorCombo.removeEventListener(Event.SELECT, frameColorChosen);
			
			if(flag)
			{
				typeCombo.addEventListener(Event.SELECT, updateOrientationCombo);
				orientationCombo.addEventListener(Event.SELECT, updateSizeCombo);
				sizeCombo.addEventListener(Event.SELECT, updateStyleCombo);
				styleCombo.addEventListener(Event.SELECT, styleChosen);
				frameColorCombo.addEventListener(Event.SELECT, frameColorChosen);
			}
		}
		
		
		/**
		 * update and refill type combo from save
		 */
		private function updateTypeCombo( isInit : Boolean = false):void
		{
			// check if existing project
			if( Infos.project ){
				// check if has upgrade
				var hasTypeUpdate : Boolean = checkTypeUpgrades( Infos.project.canvasType );
				// if no upgrade, we just add one item to box, the one selected
				if(!hasTypeUpdate) {
					typeCombo.items = [{label:ResourcesManager.getString("canvas.type."+Infos.project.canvasType), id:Infos.project.canvasType}];
					typeCombo.selectedIndex = 0;
				}
				disableUI( hasTypeUpdate );
			}
			else // new project
			{
				typesArray = new Array();
				var list:XMLList = xml..types.node;
				for (var i:int = 0; i < list.length(); i++) 
				{
					var item:Object = {}
					item.label = ResourcesManager.getString("canvas.type."+list[i].@id);
					item.id = String(list[i].@id);
					typesArray.push(item);
				}
				typeCombo.items = typesArray;
				typeCombo.draw();
				typeCombo.enabled = true;
				typeCombo.selectedIndex = 0;
			}
			
			//dafault values
			updateOrientationCombo(null, isInit);
		}
		
		/**
		 * update and refill orientation comboe
		 */
		private function updateOrientationCombo(e:Event, isInit : Boolean = false):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			
			//orientation
			var selectedIndex:Number = 0;
			orientationArray  = [];
			var list:XMLList = xml.content.types.node.(@id==typeCombo.selectedItem.id).orientation.node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("canvas.orientation."+list[i].@id);
				item.id = String(list[i].@id);
				item.style = String(list[i].@style);
				orientationArray.push(item);
				if(Infos.project && Infos.project.docPrefix == item.style)
				{
					selectedIndex = i;					
				}
				
			}
			orientationCombo.items = orientationArray;
			orientationCombo.draw();
			
			// set selected index
			orientationCombo.selectedIndex = selectedIndex;
			
			//then update size
			updateSizeCombo(null,isInit);
		}
		
		private function updateSizeCombo(e:Event, isInit : Boolean = false):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			
			// select current project size
			var selectedId : String = (Infos.project && Infos.project.docCode)? Infos.project.docCode.substring(3): null;
			var selectedIndex:Number = 0;
			//sizes
			sizesArray  = [];
			var list:XMLList = xml.content.types.node.(@id==typeCombo.selectedItem.id).orientation.node.(@id==orientationCombo.selectedItem.id).size.node;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = String(list[i].@label);
				item.id = String(list[i].@id);//i;
				if(selectedId && item.id == selectedId) selectedIndex = i;
				sizesArray.push(item);
			}
			sizeCombo.items = sizesArray;
			sizeCombo.draw();
			sizeCombo.selectedIndex = selectedIndex;
			
			//then update style
			updateStyleCombo(null, isInit);
	
		}
		
		private function updateStyleCombo(e:Event,isInit : Boolean = false):void
		{
			//remove listerner when update
			setComboBoxListeners(false);
			
			//style
			// if style is not a "free format (CPF)" or a "multiple (CPM)", then we display the combo box with the available styles for the chosen orientation
			// else there is two different configurator for the "free format" and for the "multiple"
			styleArray  = [];
			var list:XMLList = xml.content.styles.node.(@id==orientationCombo.selectedItem.style).node;
			var selectedIndex:Number = 0;
			for (var i:int = 0; i < list.length(); i++) 
			{
				var item:Object = {}
				item.label = ResourcesManager.getString("canvas.style."+list[i].@id);
				item.id = list[i].@id;
				if(isInit && Infos.project && Infos.project.canvasStyle == item.id) selectedIndex = i;
				styleArray.push(item);
			}
			styleCombo.items = styleArray;
			styleCombo.draw();
			if(isInit) styleCombo.selectedIndex = selectedIndex;
			
			//preview
			showPreview();
			//put back listeners
			setComboBoxListeners(true);
			
			//show/hide stuff in configurator
			updateConfigurator();
			
			//update Price
			updatePriceInConfigurator();
			
		}
		

		
		
		protected function styleChosen(event:Event):void
		{
			//preview
			showPreview();
		}	
		
		protected function frameColorChosen(event:Event):void
		{
			//preview
			showPreview();
		}	
		
		
		/**
		 *Return an object containing the mininum Width and height for a custom made canvas
		 * obj.width
		 * obj.height
		 * */
		private function getMinCustomSize():Object
		{
			var obj:Object = {}
			var value:String = String(xml.content.types.node.(@id==typeCombo.selectedItem.id).@minCustomSize);	
			obj.width = value.split("x")[0];
			obj.height = value.split("x")[1];
			return obj;
		}
		
		/**
		 * Create the image path based on ID's
		 * then load image
		 * */
		private function showPreview():void
		{
			var t : String = typeCombo.selectedItem.id;
			var o : String = orientationCombo.selectedItem.style;
			var s : String = styleCombo.selectedItem.id;
			
			var imageName : String = "";
			var extension : String = ".jpg";
			if( o == "CPM" ) {
				extension = ".png";
				imageName = "divided_style";
			} 
			else
			{
				switch ( t ){
					case CanvasManager.TYPE_KADAPAK : 
						imageName += "kadapak-";
						extension = ".png";
						break;
					case CanvasManager.TYPE_PLEXI :
					case CanvasManager.TYPE_ALUMINIUM :
					case CanvasManager.TYPE_FOREX :
					case CanvasManager.TYPE_REAL_ALUMINIUM :
						imageName += "plexi-";
						break;
					case CanvasManager.TYPE_POSTER : 
						imageName += "soft_poster-";
						break;
				}
				
				switch (s){
					case "styles_style1":
						imageName += "simple-";
						break;
					case "styles_style2":
						imageName += "pelemele-";
						break;
					case "styles_style5":
						imageName += "popart-";
						break;
				}
				
				switch (o){
					case "CPP":
						imageName += "portrait";
						break;
					case "CPL":
						imageName += "landscape";
						break;
					case "CPS":
						imageName += "square";
						break;
					case "CPF":
						imageName += "custom";
						break;
				}
			}
			
			loadPreview(imageName,extension);
		}
		
		/**
		 * Show / Hide components based on the chosen orientation
		 * */
		private function updateConfigurator():void
		{
			switch(orientationCombo.selectedItem.style)
			{
				case "CPP":
				case "CPL":
				case "CPS":
					styleCombo.visible = true;
					sizeCombo.visible = labelSize.visible = true;
					customSize.visible = false;
					multipleOptions.visible = false;
					break;
				case "CPF":
					styleCombo.visible = true;
					sizeCombo.visible = labelSize.visible = false;
					multipleOptions.visible = false;
					customSize.visible = true;
					customWidth.minimum = getMinCustomSize().width;
					customHeight.minimum = getMinCustomSize().height;
					
					if(resetCustomSizeFlag){
						resetCustomSizeFlag = false;
						customWidth.value = customWidth.minimum;
						customHeight.value = customHeight.minimum;	
					}
					
					//customWidth.minimum = 20;
					//customHeight.minimum = 20;
					customWidth.maximum = customHeight.maximum = 150;
					break;
				case "CPM":
					styleCombo.visible = false;
					sizeCombo.visible = labelSize.visible = false;
					customSize.visible = false;
					multipleWidth.minimum = getMinCustomSize().width;
					multipleHeight.minimum = getMinCustomSize().height;
					
					if(resetCustomSizeFlag){
						resetCustomSizeFlag = false;
						multipleWidth.value = multipleWidth.minimum;
						multipleHeight.value = multipleHeight.minimum;	
					}
					
					
					multipleOptions.visible = true;
					if(selectedMLayout) selectMLayout(selectedMLayout);
					break;
			}
			
			// frame color for kadapak
			frameColorBox.visible = (typeCombo.selectedItem.id == CanvasManager.TYPE_KADAPAK)? true : false;
			
			// update layout
			styleCombo.y = (customSize.visible) ? customSize.y + customSize.height + 15 : sizeCombo.y + sizeCombo.height + 15;
			
			if(!forDashboard){
				frameColorBox.y = (styleCombo.visible)? styleCombo.y + 45 : multipleOptions.y + 168 ;//multipleOptions.height + 15;
				frameColorBox.x = typeCombo.width + typeCombo.x - frameColorBox.width;	
			}
			
			
			
			// preview
			if(!forDashboard)
			{
				if(frameColorBox.visible) preview.y = frameColorBox.y + frameColorBox.height + 45;
				else if (multipleOptions.visible) preview.y = multipleOptions.y + 180 + 15;
				else preview.y = 270
				preview.y -=15;
				validate.y = preview.y + preview.height + 15;
			}
			else
			{
				/*if(frameColorBox.visible) preview.y = frameColorBox.y + frameColorBox.height + 45;
				else if (multipleOptions.visible) preview.y = multipleOptions.y + 180 + 15;
				else preview.y = 270
				preview.y -=15;
				validate.y = preview.y + preview.height + 15;*/
			}
			
			
			// force preview at index 1 (0 is bg);
			try
			{
				addChildAt(preview, 1);
			}
			catch(e:Error)
			{
				Debug.warn("ProjectCanvasConfigurator warning : "+e.message); // range error from logs..
				addChild(preview);
			}
		}
		
		/**
		 * Create "Multiple" options pane view 
		 * */
		private function createMultipleOtionsView():VBox
		{
			var container:VBox = new VBox();
			
			//layout 1X3, 3x3, etc...
			var layoutContainer:Sprite = new Sprite();
			var layoutList:XMLList = xml.content.styles.node.(@id=="CPM").node;
			for (var i:int = 0; i < layoutList.length(); i++) 
			{
				var node:XML = layoutList[i];
				var rows:int = int(String(node.@layout).split("x")[0]);
				var cols:int = int(String(node.@layout).split("x")[1]);
				var total:int = rows*cols;
				var layout:Sprite = new Sprite();
				
				//Squares
				var squareContainer:Sprite = new Sprite();
				var squareSize:int = 6;
				var margin:int = 2;
				for (var j:int = 0; j < total; j++) 
				{
					var theX:Number = rows + j%cols * (squareSize + margin);
					var theY:Number = Math.floor( j / cols ) * (squareSize + margin);
					squareContainer.graphics.beginFill(Colors.BLUE, 1);
					squareContainer.graphics.drawRect(theX,theY,squareSize,squareSize);
					squareContainer.graphics.endFill();
				}
				squareContainer.x = Math.round(7-squareContainer.width*.5); //(5 is checkbox width /2)
				squareContainer.y = -squareContainer.height;
				
				layout.addChild(squareContainer);
				layout.buttonMode = true;
				layout.mouseChildren = false;
				layout.addEventListener(MouseEvent.CLICK, onMultipleFormatClickHandler);
				layout.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void{
					e.currentTarget.alpha = .8;
				});
				layout.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent):void{
					e.currentTarget.alpha = 1;
				});
				
				layout.opaqueBackground = 0xffffff;
				
				//Checkbox
				var checkbox:CheckboxTitac = new CheckboxTitac(layout,0,0,"",null,true);
				checkbox.id = String(node.@layout);
				checkbox.x = 0;
				checkbox.y = 15;
				layout.x = 3 + 34*i;
				layoutContainer.addChild(layout);
				container.addChild(layoutContainer);
				
				// add bg for bigger clickable zone
				var layoutBg : Sprite = new Sprite();
				layoutBg.graphics.beginFill(0xffffff,1);
				layoutBg.graphics.drawRect(squareContainer.x,squareContainer.y,layout.width,layout.height);
				layoutBg.graphics.endFill();
				layout.addChildAt(layoutBg,0);
				
				layoutGroup.push({id:checkbox.id, layout:layout, checkBox:checkbox, squareContainer:squareContainer  });
			}
			
			//Size of 1 square
			multipleSize = new HBox(container);
			
			var widthBox:VBox = new VBox(multipleSize);
			var widthLabel:Label = new Label(widthBox,0,0,ResourcesManager.getString("lefttab.project.canvas.multiple.size.width")+" (cm)", TextFormats.ASAP_REGULAR(13));
			multipleWidth = new NumericStepper(widthBox,0,0, multipleWidthChangeHandler);
			multipleWidth.minimum = 20;
			multipleWidth.maximum = 90;
			
			var heightBox:VBox = new VBox(multipleSize);
			var heightLabel:Label = new Label(heightBox,0,0,ResourcesManager.getString("lefttab.project.canvas.multiple.size.height")+" (cm)", TextFormats.ASAP_REGULAR(13));
			multipleHeight = new NumericStepper(heightBox,0,0, multipleHeightChangeHandler);
			
			multipleHeight.minimum = 20;
			multipleHeight.maximum = 120;
			
			var marginWrapper:DisplayObjectContainer = (forDashboard)?multipleSize:container;
			var marginBox:VBox = new VBox(marginWrapper);
			var marginLabel:Label = new Label(marginBox,0,0,ResourcesManager.getString("lefttab.project.canvas.multiple.size.margin")+" (cm)", TextFormats.ASAP_REGULAR(13));
			multipleMargin = new NumericStepper(marginBox,0,0);
			multipleMargin.minimum = 0;
			multipleMargin.maximum = 10;
			multipleMargin.value = 2; // default value is 2 cm ! for the margin between multiple canvas
			
			return container;
		}
		
		
		/**
		 * Configurator Validation
		 * Happen when there is no project ready
		 * It creates a new project
		 * 
		 * or
		 * when applying an upgrade to the current project
		 * it update the current project vo
		 * 
		 * Also, it disable some options depending on the selected project
		 * 
		 * And finally checks for upgrades if available
		 * 
		 * */
		private function validateHandler():void
		{
			
			if(forDashboard)
			{
				TweenMax.delayedCall(0,Dashboard.instance.close);
			}
			
			var initParams:ProjectCreationParamsVo = new ProjectCreationParamsVo();
			var type : String = "" + typeCombo.selectedItem.id;
			var orientation : String = "" +orientationCombo.selectedItem.id;
			var prefix : String = ""+ xml.content.types.node.(@id == type)[0].orientation[0].node.(@id == orientation)[0].@style;
			var size : String = ( prefix == "CPF" || prefix == "CPM" )? "20" : sizeCombo.selectedItem.id;
			
			var selectedProjectTypeCode:String = prefix + size;
			var selectedProjectDocType:String = ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
			
			// CPF project (custom format)
			// > we need to recover the width and height in cm
			
			// params
			initParams.canvasType = type;
			if(prefix == "CPM"){
				initParams.canvasWidth = multipleWidth.value;
				initParams.canvasHeight = multipleHeight.value;
				initParams.canvasMargin = multipleMargin.value;
				initParams.canvasMLayout = selectedMLayout;
			}
			else if (prefix == "CPF"){
				initParams.canvasWidth = customWidth.value;
				initParams.canvasHeight = customHeight.value;
			}
			
			if( styleCombo.visible ){
				initParams.canvasStyle = styleCombo.selectedItem.id;
			}
			
			// kadapak frame color
			initParams.canvasFrameColor = (type == CanvasManager.TYPE_KADAPAK)?frameColorCombo.selectedItem.id : "none";
			
			//Price info for offline calculation price
			var canvasPriceInfo:CanvasPriceInfo = getCanvasPriceInfo();
			
			
			// create new or upgrade
			if(!Infos.project)
			{
				if(Infos.IS_DESKTOP)
				initParams.canvasPriceInfo = canvasPriceInfo;
				Infos.session.product_id = selectedProjectDocType;
				ProjectManager.instance.createNewProject(Infos.session.product_id,initParams);
			}
			else
			{
				if(Infos.IS_DESKTOP)
				Infos.project.canvasPriceInfo = canvasPriceInfo;
				Infos.session.product_id = selectedProjectDocType;
				ProjectManager.instance.updateCurrentCanvasProject(Infos.session.product_id, initParams);
			}
			
			//check upgrades
			var hasTypeUpgrade:Boolean = checkTypeUpgrades(type);
			
			//disableUI
			disableUI(hasTypeUpgrade);
			
			//update btn label
			if(Infos.project)
				validate.setLabel("common.update.project");
		}
		
		/**
		 * Upate configuration price
		 * desktop and dashBoard only
		 * */
		private function updatePriceInConfigurator():void
		{
			CONFIG::offline
				{
					if(Infos.IS_DESKTOP && forDashboard)
					{
						var canvasPriceInfo:CanvasPriceInfo = getCanvasPriceInfo();
						var minimumPrice:Number = PriceManager.instance.getPriceFromDash(canvasPriceInfo.docType,false, false, "",canvasPriceInfo);
						priceWrapper.setLabel(ResourcesManager.getString("price.minimum.label")+" "+String(minimumPrice)+"€",false);
					}
					
				}
		}
		
		/**
		 * Get selected docType
		 */
		private function getCanvasPriceInfo():CanvasPriceInfo
		{
			var currentDocType:String = getSelectedDocType(); //getCanvasModifierFromDash
			if(orientationCombo.selectedItem.id == ProductsCatalogue.CANVAS_ORIENTATION_MULTIPLE)
				currentDocType = String(parseInt(currentDocType) + 1);
			var coeficient:Number = CanvasPriceInfo.getPriceCoeficient(selectedMLayout,orientationCombo.selectedItem.id);
			var currentWidth:int = (orientationCombo.selectedItem.id == ProductsCatalogue.CANVAS_ORIENTATION_MULTIPLE)?multipleWidth.value:customWidth.value;
			var currentHeight:int = (orientationCombo.selectedItem.id == ProductsCatalogue.CANVAS_ORIENTATION_MULTIPLE)?multipleHeight.value:customHeight.value;
			var canvasPriceInfo:CanvasPriceInfo = new CanvasPriceInfo(currentDocType,typeCombo.selectedItem.id,orientationCombo.selectedItem.id,currentWidth,currentHeight,coeficient);
			return canvasPriceInfo;
		}
		
		/**
		 * Get selected docType
		 */
		private function getSelectedDocType():String
		{
			var type : String = "" + typeCombo.selectedItem.id;
			var orientation : String = "" +orientationCombo.selectedItem.id;
			var prefix : String = ""+ xml.content.types.node.(@id == type)[0].orientation[0].node.(@id == orientation)[0].@style;
			var size : String = ( prefix == "CPF" || prefix == "CPM" )? "20" : sizeCombo.selectedItem.id;
			
			var selectedProjectTypeCode:String = prefix + size;
			return ProjectManager.instance.getDocTypeIDByCode(selectedProjectTypeCode);
		}
		
		/**
		 * DISalbe/enable UI based on the validated project
		 * */
		private function disableUI(hasTypeUpgrade:Boolean):void
		{
			typeCombo.enabled = (!Infos.project)?true:hasTypeUpgrade;
			orientationCombo.enabled = (!Infos.project)?true:false;
			styleCombo.enabled = (!Infos.project)?true:false;
			//validate.enabled = (!Infos.project)?true:hasTypeUpgrade;
			
			/*	
			switch(typeCombo.selectedItem.docidstr)
			{
				case "WCAL3":
				case "WCAL2":
					break;
				
				case "WCAL4":
					weekDayCombo.enabled = false;
					yearStepper.enabled = false;
					break;
				
				case "WCAL5":
				case "WCAL6":
					weekDayCombo.enabled = false;
					weekDayCombo.selectedIndex = 1;
					break;
				
				case "WCAL":
					break;
				
				case "DCAL2":
					break;
				
				case "DCAL":
					break;
			}
			*/
			
		}
		
		/**
		 * get current type project upgrades
		 * at this point, only classic and contemporain have type upgrade, and its between each other
		 * size cannot change (sizecombo enabled = false);
		 * if no type upgrade (casual and trendy) typecombo enabled = false
		 */
		private function checkTypeUpgrades( type:String ):Boolean
		{
			var upgradeList:Array = getTypeUpgrades(type);
			if(upgradeList.length>0)
			{
				//typeCombo.removeAll();
				typeCombo.items = upgradeList;	
				typeCombo.draw();
				typeCombo.enabled = true;
				// select correct item in combo
				var selectedIndex : Number = 0;
				for (var i:int = 0; i < upgradeList.length; i++) 
				{
					if(upgradeList[i].id == type) selectedIndex = i;
				}
				typeCombo.selectedIndex = selectedIndex;
				
				return true;
			}
			
			typeCombo.enabled = false;
			return false;
		}
		
		/**
		 * get current project type upgrades
		 */
		private function getTypeUpgrades( type : String ):Array
		{
			typesArray  = [];
			var upgradeNode:XMLList = xml.content.upgrades.node.(@id == type);
			if(upgradeNode.length() > 0 ){
				var list:XMLList = upgradeNode.node;
				for (var i:int = 0; i < list.length(); i++) 
				{
					var node : XML = xml..types.node.(@id == list[i].@id)[0];
					var item:Object = {}
					item.label = ResourcesManager.getString("canvas.type."+node.@id);
					item.id = String(node.@id);
					typesArray.push(item);
				}
			}
			return typesArray;
		}
		
		private function onMultipleFormatClickHandler(e:Event):void
		{
			var layout:Sprite = e.currentTarget as Sprite;
			for (var i:int = 0; i < layoutGroup.length; i++) 
			{
				if(layoutGroup[i].layout == layout) selectMLayout(layoutGroup[i].id);
			}
		}
		
		/**
		 * select a layout checkbox by its id
		 */
		private function selectMLayout( layoutId : String):void
		{
			var squareContainer : Sprite;
			var cb:CheckBox;
			for (var i:int = 0; i < layoutGroup.length; i++) 
			{
				squareContainer = layoutGroup[i].squareContainer;
				cb = layoutGroup[i].checkBox;
				TweenMax.killTweensOf(squareContainer);
				if(cb.id == layoutId) {
					cb.selected = true;
					TweenMax.to(squareContainer, 0,{tint:Colors.GREEN});
				}
				else{
					cb.selected = false;
					TweenMax.to(squareContainer, 0,{tint:null});
				}
			}
			
			selectedMLayout = layoutId;
			
			// update size numeric stepper limits
			var maxw : Number ;
			var maxh : Number ; 
			switch (layoutId) {
				case "1x2" : 
					maxw = 90;
					maxh = 120;
					break;
				case "2x1" : 
					maxw = 120;
					maxh = 90;
					break;
				case "1x3" : 
					maxw = 60;
					maxh = 120;
					break;
				case "3x1" : 
					maxw = 120;
					maxh = 60;
					break;
				case "2x2" : 
					maxw = 90;//60
					maxh = 90;
					break;
				case "2x3" : 
					maxw = 60;
					maxh = 60;
					break;
				case "3x2" : 
					maxw = 60;
					maxh = 60;
					break;
				case "3x3" : 
					maxw = 60;//40
					maxh = 60;
					break;
			}
			if(multipleWidth) multipleWidth.maximum = maxw;
			if(multipleHeight) multipleHeight.maximum = maxh;
			
			// force verification for max values
			if(layoutId == "2x2" || layoutId == "3x3"){
				multipleWidthChangeHandler(null);
				multipleHeightChangeHandler(null);
			}
			
			// update offline price
			updatePriceInConfigurator(); 
		}
		
		/**
		 * on multiple width change
		 */
		private function multipleWidthChangeHandler(e:Event):void
		{
			if(!selectedMLayout || !multipleWidth) return;
			if(selectedMLayout == "2x2"){
				if(multipleWidth.value > 60 && multipleHeight.value > 60 ) multipleHeight.value = 60;
			}
			else if(selectedMLayout == "3x3"){
				if(multipleWidth.value > 40 && multipleHeight.value > 40 ) multipleHeight.value = 40;
			}
			
			//update Price
			updatePriceInConfigurator();
		}
		/**
		 * on multiple height change
		 */
		private function multipleHeightChangeHandler(e:Event):void
		{
			if(!selectedMLayout || !multipleHeight) return;
			if(selectedMLayout == "2x2"){
				if(multipleHeight.value > 60 && multipleWidth.value > 60) multipleWidth.value = 60;
			}
			else if(selectedMLayout == "3x3"){
				if(multipleHeight.value > 40 && multipleWidth.value > 40) multipleWidth.value = 40;
			}
			
			//update Price
			updatePriceInConfigurator();
		}
		
		
		
		/**
		 * on multiple width change
		 */
		private function customWidthChangeHandler(e:Event):void
		{
			if(!customWidth) return;
			if(customWidth.value > 100 && customHeight.value > 100) customHeight.value = 100;
			
			//update Price
			updatePriceInConfigurator();
		}
		/**
		 * on multiple height change
		 */
		private function customHeightChangeHandler(e:Event):void
		{
			if(!customHeight) return;
			if(customHeight.value > 100 && customWidth.value > 100) customWidth.value = 100;
			
			//update Price
			updatePriceInConfigurator();
		}
		
		
		
	
		
		/**
		 * Load calendar preview image
		 * */
		private function loadPreview(imageName:String, extension:String = ".jpg"):void
		{
			preview.visible = false;
			cleanPreview();
			preview.addChildAt(sig.getImage(Infos.config.getValue("imagePath") + THUMB_PATH + imageName + extension,false,preview.getBounds(this),function(e:Event):void{
				// kadapak frame
				if(typeCombo.selectedItem.id == CanvasManager.TYPE_KADAPAK){
					fillPreviewColor(CanvasManager.instance.getKadapakFrameColor(frameColorCombo.selectedItem.id));
				} else {
					fillPreviewColor(0xffffff);
				}
				TweenMax.killTweensOf(preview);
				TweenMax.to(preview, 2, {tint:null,delay:.1, ease:Strong.easeOut,startAt:{tint:0xffffff, autoAlpha:1}});
				
			}),0);
			
			
		}
		
		/**
		 *
		 */
		private function fillPreviewColor(color:Number):void
		{
			preview.graphics.clear();
			preview.graphics.beginFill(0xffffff);
			preview.graphics.drawRect(0,0,220,129);
			
			if(preview.numChildren > 0){
				var childBounds:Rectangle = preview.getChildAt(0).getBounds(preview);
				preview.graphics.beginFill(color);
				preview.graphics.drawRect(childBounds.x, childBounds.y, childBounds.width, childBounds.height);
			}
			preview.graphics.endFill();
		}
		
		/**
		 * Remove previous preview if necessary
		 * */
		private function cleanPreview():void
		{
			if(preview.numChildren > 0)
			{
				var total:int = preview.numChildren;
				for (var i:int = 0; i < total; i++) 
				{
					var child:DisplayObject;
					if(preview.numChildren > 0)
					{
						child = preview.getChildAt(0)
						preview.removeChild(child);
					}
					
				}
			}
			
		}
		
		/**
		 * Draw bg
		 */
		private function drawBg():void
		{
			graphics.clear();
			graphics.beginFill(0xFFFFFF);
			graphics.drawRect(0,0,width,height + 10);
			graphics.endFill();
		}
		
		
	}
}