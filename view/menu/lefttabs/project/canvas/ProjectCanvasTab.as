package view.menu.lefttabs.project.canvas
{
	import view.menu.lefttabs.LeftTabContent;
	import view.menu.lefttabs.project.ProjectSimpleItemHeader;
	
	public class ProjectCanvasTab extends LeftTabContent
	{
		private static const THUMB_PATH:String = "ui/menu/lefttabs/project/canvas/";
		private var configurator:ProjectCanvasConfigurator;
		
		/**
		 * constructor
		 */
		public function ProjectCanvasTab(xmlData:XML)
		{
			super(xmlData);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		/**
		 * init the view (called when show method is called and initiated is fals)
		 */
		override public function init():void
		{
			super.init();
			
			//header
			var listHeader:ProjectSimpleItemHeader = new ProjectSimpleItemHeader("lefttab.project.canvas.title");
			listHeader.init();
			//Content -> Create Calendar options configurator
			configurator = new ProjectCanvasConfigurator(xmlData);
			accordion.addAccordionItem(listHeader, configurator, "canvasType",true, true, false);
						
			// update visible items
			accordion.updateItems();
		}
		
		override public function show():void
		{
			super.show();
			configurator.update();
		}
		
		
		
		
	}
}