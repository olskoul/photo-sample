package view.menu.lefttabs.project
{
	import be.antho.data.ResourcesManager;
	
	import com.greensock.TweenMax;
	
	import comp.accordion.AccordionItemHeader;
	
	import data.AlbumVo;
	import data.Infos;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import manager.SessionManager;
	
	import mcs.leftmenu.accordion.item.header.simple.view;
	
	import utils.Colors;
	
	
	
	public class ProjectSimpleItemHeader extends AccordionItemHeader
	{
		private var resourceKey:String;
		
		public function ProjectSimpleItemHeader(resourceKey:String = "")
		{
			super();
			this.resourceKey = resourceKey;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{
			var view:MovieClip = new mcs.leftmenu.accordion.item.header.simple.view(); // SWC assets
			var color:Number = (!Infos.config.isRebranding)?Colors.BLUE:Infos.config.rebrandingMainColor;
			TweenMax.to(view.title,0,{tint:color});
			ResourcesManager.setText(view.title,resourceKey); 
			addChild(view);
			buttonMode = true;
			mouseChildren = false;
		}
		
		
	}
}