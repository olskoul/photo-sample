package view.menu.lefttabs
{
	import be.antho.data.ResourcesManager;
	import be.antho.utils.tabs.TabButton;
	
	import com.greensock.TweenMax;
	
	import data.Infos;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import manager.SessionManager;
	
	import mcs.leftmenu.tab.btn;
	
	import utils.Colors;
	
	public class LeftTabButton extends TabButton
	{
		public static var WIDTH:Number = 0;
		private var view:MovieClip;
		private var bg:MovieClip;
		private var selectedClip : MovieClip;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function LeftTabButton()
		{
			super();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
	
		/**
		 * Contruct the view and associate it with an ID
		 */
		public function construct(id:String):void
		{
			view = new mcs.leftmenu.tab.btn() as MovieClip; //Asset library
			ResourcesManager.setText(view.txt,"lefttab."+id);
			view.icon.gotoAndStop(id);
			addChild(view);
			
			// refs
			bg = view.bg;
			selectedClip = view.selectedClip;
			selectedClip.visible = false;
			if(Infos.config.isRebranding) TweenMax.to(selectedClip, 0,{tint:Infos.config.rebrandingMainColor});
			
			WIDTH = bg.width;
		}
		
		/**
		 *
		 */
		public override function destroy():void
		{
			super.destroy();
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		override protected function over(e:MouseEvent = null):void
		{
			bg.visible = false;
			selectedClip.visible = true;
			
			// text and icon
			TweenMax.killTweensOf(view.txt);
			TweenMax.killTweensOf(view.icon);
			var color:Number = (!Infos.config.isRebranding)?Colors.BLUE:Colors.WHITE;
			TweenMax.allTo([view.txt, view.icon],.2,{tint:color});
		}
		
		override protected function out(e:MouseEvent = null):void
		{
			if(!selected)
			{
				bg.visible = true;
				selectedClip.visible = false;
				
				// text and icon
				TweenMax.killTweensOf(view.txt);
				TweenMax.killTweensOf(view.icon);
				if(!Infos.config.isRebranding)	TweenMax.allTo([view.txt, view.icon],0,{tint:null});
				else
				{
					TweenMax.allTo([view.txt],0,{tint:null});
					TweenMax.allTo([view.icon],0,{tint:Infos.config.rebrandingMainColor});
				}
			}
		}
		
	}
}