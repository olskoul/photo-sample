package view.menu.lefttabs.overlayers
{
	import com.greensock.TweenMax;
	
	import flash.display.Sprite;
	
	import be.antho.data.ResourcesManager;
	
	import comp.OverlayerItem;
	import comp.ThumbItem;
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItem;
	import comp.accordion.AccordionItemContent;
	import comp.accordion.AccordionItemHeader;
	import comp.button.SimpleSquareButton;
	
	import data.Infos;
	import data.OverlayerVo;
	import data.TooltipVo;
	
	import manager.BackgroundsManager;
	import manager.ClipartsManager;
	import manager.DragDropManager;
	import manager.OverlayerManager;
	import manager.PagesManager;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.EditionArea;
	import view.menu.lefttabs.LeftTabContent;
	import view.menu.lefttabs.cliparts.ClipartsTabBottomZone;
	import view.menu.lefttabs.common.AccordionSimpleContent;
	import view.menu.lefttabs.common.EmptyItemHeader;
	import view.popup.PopupAbstract;
	
	public class OverlayersTab extends LeftTabContent
	{
		
		private var thumbList : Vector.<OverlayerItem>; 
		private var allViews:Vector.<ThumbItem>;
		private var downloadMoreBtn:SimpleSquareButton;
		private var bottomZoneContent:OverlaysTabBottomZone;
		private var tryUpdapteView:int = 0;

		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function OverlayersTab(xmlData:XML)
		{
			super(xmlData);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			super.init();
			
			//Expand/collapse all buttons
			addExpandCollapseBtn();
			
			//create thumbs
			updateView();
			
			//listeners
			OverlayerManager.OVERLAYS_REFRESH_COMPLETE.add(listRefreshHandler);
		}
		
		/**
		 * Destroy overide
		 */
		public override function destroy():void
		{
			
			//listeners
			OverlayerManager.OVERLAYS_REFRESH_COMPLETE.remove(listRefreshHandler);
			
			//dispose
			dispose();
			
			super.destroy();
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function updateView():void
		{
			//first dispose()
			dispose();
			var list:Vector.<OverlayerVo> = OverlayerManager.instance.overlayerList;
			if(!list)
			{
				tryUpdapteView ++;
				if(tryUpdapteView < 4)
				TweenMax.delayedCall(1,updateView);
				else
				{
					Debug.warnAndSendMail("OverlayerTab > Could not update view after "+tryUpdapteView+" tries");
				}
				return;
			}

			// DONWLOAD MORE BACKGROUND
			if(OverlayerManager.instance.showDownloadMoreBtn)
			{
				var emptyHeader : EmptyItemHeader = new EmptyItemHeader();
				emptyHeader.init();
				var downloadBtnWrapper:AccordionSimpleContent = new AccordionSimpleContent();
				
				downloadMoreBtn = new SimpleSquareButton("download.more.overlays", Colors.BLUE, downloadMoreHandler, Colors.BLUE, Colors.WHITE, Colors.WHITE, 1,false,6);
				downloadMoreBtn.forcedWidth = 280;
				downloadMoreBtn.x = 6;
				//downloadMoreBkgBtn.y = 15;
				downloadBtnWrapper.init( downloadMoreBtn );
				
				//Create accordion item
				accordion.addAccordionItem(emptyHeader, downloadBtnWrapper, "donwloadMore", true, true, false);
			}
			
			//create project backgrounds (no categoriszation)
			// create header
			var listHeader : OverlayersItemHeader = new OverlayersItemHeader();
			listHeader.title = ResourcesManager.getString("lefttab.overlayers.alloverlayers");
			listHeader.total = OverlayerManager.instance.overlayerList.length;
			listHeader.init();
			
			//reset allviews
			allViews = new Vector.<ThumbItem>();
			
			// create content
			var listContent:OverlayerItemContent = new OverlayerItemContent();
			var item : OverlayerItem;
			var vo:OverlayerVo;
			var views:Vector.<ThumbItem> = new Vector.<ThumbItem>();
			for (var i:int = 0; i < OverlayerManager.instance.overlayerList.length; i++) 
			{
				vo =  OverlayerManager.instance.overlayerList[i];
				if(vo != null)
				{
					item = new OverlayerItem(vo);
					item.draggable = true;
					item.dropType = DragDropManager.TYPE_MENU_OVERLAYER;
					
					if(!vo.isLocalContent)
						item.CLICK.add(itemSelectedHandler);
					
					vo.label = ResourcesManager.getString("thumb.label.prefix.frame") + " " + String(Number(i+1));//vo.name;
					item.addLabel();
					
					//item.DOUBLE_CLICK.add(itemDoubleClickHandler);
					
					//If desktop and no internet put it on top of them all
					if(Infos.IS_DESKTOP && vo.isLocalContent && !Infos.INTERNET)
					{
						views.unshift(item);
						allViews.unshift(item);
					}
					else					
					{
						views.push(item);
						allViews.push(item);
						
					}
				}
			}
			
			//display views as grid
			showAsGrid(views,listContent,Accordion.MAX_WIDTH);	
			// add it to accordion
			accordion.addAccordionItem(listHeader, listContent, listHeader.title, true, true, false);
			
			//apply to All 
			
			CONFIG::offline
				{
					bottomZoneContent = new OverlaysTabBottomZone();
					addToBottomZone(bottomZoneContent);
				}			
			
			// update visible items
			accordion.updateItems();
		}
		
		/**
		 * Click handler
		 * Add preview to applytoall module
		 * */
		private function itemSelectedHandler(item:ThumbItem):void
		{
			if(bottomZoneContent)
			{
				bottomZoneContent.selectedBkg = item;
				redrawBottomZoneBkg();
				showBottomZone(true);
				TweenMax.killDelayedCallsTo(showBottomZone);
				TweenMax.delayedCall(5,showBottomZone,[false]);
			}
			
		}
		
		/**
		 * Start process of donwloading more overlay
		 * -> download zip package
		 * -> unzip it
		 * -> overwrite the overlay xml
		 * -> Refresh tab
		 * */
		private function downloadMoreHandler():void
		{
			OverlayerManager.instance.downloadMore();
			//PopupAbstract.Alert(ResourcesManager.getString("download.more.overlays"), ResourcesManager.getString("popup.download.more.assets.desc"),true,OverlayerManager.instance.downloadMore,null,null);
		}
		
		/**
		 * Double click handler
		 * -> TODO : if a frame is selected, add this overlayer to current frame
		 * */
		private function itemDoubleClickHandler(item:ThumbItem):void
		{
			if(PagesManager.instance.currentEditedPage)
			{
				// TODO : if a frame is selected, add this overlayer to current frame
			}
		}
		
		/**
		 * Refresh list
		 * */
		private function listRefreshHandler():void
		{
			updateView();
		}	
		
		
		/**
		 * Dispose tab
		 */
		private function dispose():void
		{
			//destroy bottom zone content
			if(bottomZoneContent) bottomZoneContent.destroy();
			
			// dispose accordion
			if(accordion)
			accordion.dispose();
		}
	}
}