package view.menu.lefttabs.overlayers
{
	import com.bit101.components.HBox;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	
	import be.antho.data.ResourcesManager;
	
	import comp.BackgroundCustomItem;
	import comp.BackgroundItem;
	import comp.ThumbItem;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	
	import manager.BackgroundsManager;
	import manager.PagesManager;
	
	CONFIG::offline
		{
			import offline.manager.OfflineAssetsManager;
		}
	
	
	import utils.Colors;
	import utils.Debug;
	import data.TooltipVo;
	import data.ClipartVo;
	import comp.ClipartItem;
	import comp.OverlayerItem;
	
	public class OverlaysTabBottomZone extends Sprite
	{
		
		private var currentSelectedItem:ThumbItem;

		private var preview:Bitmap;

		private var wrapper:HBox;


		//private var addToPage:SimpleButton;
		
		
		private var downloadBtn:SimpleButton;

		private var previewContainer:Sprite;
		
		public function OverlaysTabBottomZone()
		{
			super();
			construct();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Setter of the current selected bkg item
		 * Change preview
		 * */
		public function set selectedBkg(item:ThumbItem):void
		{
			currentSelectedItem = item;
			setPreview();
			downloadBtn.visible = downloadBtn.enabled = (item is OverlayerItem &&  !item.IS_LOCAL_CONTENT);
		}
		
		/**
		 * Dispose
		 * */
		
		public function dispose():void
		{
			if(preview.bitmapData)
			preview.bitmapData.dispose();
		}
		
		/**
		 * Destroy
		 * */
		
		public function destroy():void
		{
			dispose();
			if(parent)
				parent.removeChild(this);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Change preview bitmap
		 * Contraint to bounds
		 * */
		private function setPreview():void
		{
			preview.bitmapData = currentSelectedItem.cloneThumbBitmapdata();
			preview.smoothing = true;
			
			if(preview.bitmapData != null)
			{
				activate(true);
				preview.height = downloadBtn.height;
				preview.scaleX = preview.scaleY;
				var widthToUse:Number = 60;
				if(preview.width > widthToUse)
				{
					preview.width = widthToUse;
					preview.scaleY = preview.scaleX;
				}
				
				previewContainer.graphics.clear();
				previewContainer.graphics.drawRect(0,0,widthToUse,preview.height);
				
				previewContainer.alpha = 0;
				TweenMax.killTweensOf(previewContainer);
				TweenMax.to(previewContainer,1,{alpha:1});
				wrapper.draw();
			}
			else
			{
				activate(false);
				Debug.warn("BackgroundApplyToAll.setPreview: preview.bitmapData is null");
			}
			
		}
		
		private function activate( flag:Boolean ):void
		{
			wrapper.mouseChildren = wrapper.mouseEnabled = flag;
		}
		
		
		/**
		 * Build view
		 * */
		private function construct():void
		{
			wrapper = new HBox(this);
			wrapper.spacing = 5;
			wrapper.alignment = HBox.MIDDLE;
			
			previewContainer = new Sprite();

			preview = new Bitmap();
			previewContainer.addChild(preview);
			wrapper.addChild(previewContainer);
			
			
			
			var donwloadTooltip:TooltipVo = new TooltipVo("tooltip.download.assets.locally",TooltipVo.TYPE_SIMPLE,null,Colors.BLUE_CLOUD);
			downloadBtn = new SimpleSquareButton("", 
				Colors.BLUE, 
				downloadItemLocally, 
				Colors.BLUE_CLOUD,
				Colors.WHITE, 
				Colors.RED,
				1,
				false,
				6);
			downloadBtn.tooltip = donwloadTooltip;
			
			wrapper.addChild(downloadBtn);
			
		}		
		
		
		private function downloadItemLocally():void
		{
			if(currentSelectedItem && !(currentSelectedItem is BackgroundCustomItem) && !currentSelectedItem.IS_LOCAL_CONTENT)
			{
				downloadBtn.enabled = false;
				currentSelectedItem.startDownloadAsset();
			}
		}
		
	}
}