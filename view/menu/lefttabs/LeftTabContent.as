package view.menu.lefttabs
{
	import com.bit101.components.HBox;
	import com.bit101.components.ScrollPane;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.tabs.TabContent;
	import be.antho.utils.tabs.TabManager;
	
	import comp.PhotoItem;
	import comp.ThumbItem;
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItem;
	import comp.accordion.AccordionItemHeader;
	import comp.button.InfosButton;
	import comp.button.SimpleButton;
	
	import data.Infos;
	import data.PhotoVo;
	import data.TooltipVo;
	
	import library.icons.collapseIcon;
	import library.icons.expandIcon;
	
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import mcs.leftmenu.accordion.content.title.view;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	public class LeftTabContent extends TabContent
	{
		public var xmlData:XML;
		public var hasScroll:Boolean; 
		
		protected var accordion:Accordion;
		protected var titleView:MovieClip;
		protected var scrollPane:ScrollPane;
		protected var titleBtnBar:HBox;
		protected var collapseButton : Sprite;
		protected var expandButton : Sprite  ;
		
		
		private var globalPos:Point;
		private var bottomZone:Sprite;
		private var content:DisplayObject;
		private var bottomZoneVisibility:Boolean;
		

		// help/info btn
		protected var infoBtn:InfosButton;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function LeftTabContent(xmlData:XML)
		{
			super();
			
			this.id = xmlData.@id;
			this.xmlData = xmlData;
		
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * init 
		 */
		override public function init():void
		{
			//create title
			addTitle();
			
			//Global posY
			globalPos = new Point(x,y);
			globalPos = parent.localToGlobal(globalPos);
			globalPos = Infos.stage.globalToLocal(globalPos);
			
			// create accordion list or view (container)
			accordion = new Accordion();
			accordion.useParentPosY = true;
			accordion.x = 1;
			accordion.y = accordion.SCROLL_LIMIT_TOP = 0;
			accordion.SCROLL_LIMIT_BOTTOM = Infos.stage.stageHeight-globalPos.y-titleView.height;
			
			accordion.RESIZE.add(accordionResizeHandler);
			
			//scroll pane
			scrollPane = new ScrollPane(this,0,titleView.height);
			scrollPane.x = 1;
			scrollPane.setSize(titleView.width+17,Infos.stage.stageHeight-globalPos.y-titleView.height);
			scrollPane.shadow = false;
			scrollPane.borderAlpha =0;
			scrollPane.showScrollButtons(false);
			scrollPane.autoHideScrollBar = true;
			scrollPane.hScrollbarEnabled = false;
			scrollPane.background.visible = false;
			scrollPane.mouseWheelEnabled = true;
			TweenMax.to(scrollPane.background,0,{tint:0xffffff});
			scrollPane.scrollbarBackColor = 1;
			scrollPane.SCROLLING.add(onScrollhandler);
			scrollPane.addChild(accordion);		
			
			Infos.stage.addEventListener(Event.RESIZE, stageResizeHandler);
			super.init();
		}
		
		
		/**
		 *
		 */
		public override function destroy():void
		{
			// remove listeners
			Infos.stage.removeEventListener(Event.RESIZE, stageResizeHandler);
			
			// kill content
			if(accordion) accordion.destroy();
			
			super.destroy();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		private function onScrollhandler(posy:Number):void
		{
			accordion.updateItems();
		}
		
		/**
		 * ADD title view 
		 *
		 * */
		
		protected function addTitle():void
		{
			titleView = new mcs.leftmenu.accordion.content.title.view();
			titleView.x = 1;
			ResourcesManager.setText(titleView.title,"lefttab."+id); 
			addChild(titleView);
			
			var color:Number = (!Infos.config.isRebranding ) ? Colors.BLUE_LIGHT : Infos.config.rebrandingMainColor;
			TweenMax.to(titleView.title,0,{tint:color});
			
			//Add contenrt under title (overridden)
			addContentUnderTitle();
			
			//create title btn bar
			titleBtnBar = new HBox(this);
			titleBtnBar.spacing = 7;
			titleBtnBar.alignment = HBox.MIDDLE;
			
			//Infos button
			infoBtn = new InfosButton("infos.lefttab."+id,null, 0, 0);
			addBtnToTitle(infoBtn);
			
			
		}	
		
		/**
		 * Add content undr title
		 * */
		protected function addContentUnderTitle():void
		{
			
		}
		
		/**
		 * Add Expand.collapse btns
		 * */
		protected function addExpandCollapseBtn():void
		{
			//expand/collapse btns
			var wrapper:Sprite = new Sprite();
			expandButton = ButtonUtils.makeButton(new expandIcon(), expandHandler, new TooltipVo("tooltip.menu.expand.button"),Colors.GREEN); // TODO : check if we need to dispose this
			collapseButton = ButtonUtils.makeButton(new collapseIcon(), collapseHandler, new TooltipVo("tooltip.menu.collapse.button"),Colors.GREEN); // TODO : check if we need to dispose this
			wrapper.addChild(expandButton);
			wrapper.addChild(collapseButton);
			addBtnToTitle(wrapper);
			collapseButton.visible = false;
			updateIcons();
		}
		
		
		
		/**
		 * Add btn to title btn bar
		 * */
		protected function addBtnToTitle(view:DisplayObject):void
		{
			if(view)
			{
				titleBtnBar.addChild(view);
				
				//put info btn first
				if(view != infoBtn)
					titleBtnBar.addChild(infoBtn);
				
				titleBtnBar.x = titleView.width - titleBtnBar.width - 10;
				titleBtnBar.y = 28 - titleBtnBar.height*.5;
			}
			
			
				
			
		}
		
		/**
		 * SHOW Bottom zone
		 * */
		protected function showBottomZone(flag:Boolean, timming:Number = .5):void
		{
			bottomZoneVisibility = flag;			
			refreshScrollPaneHeight();
			//var posy:Number = (flag)?scrollPane.y+ scrollPane.height - bottomZone.height + 2 :scrollPane.y+ scrollPane.height;
			var posy:Number = scrollPane.y + scrollPane.height;//(flag)?scrollPane.y+ scrollPane.height + 2 :scrollPane.y+ scrollPane.height;
			var autoAlpha:int = (flag)?1:0;
			TweenMax.killTweensOf(bottomZone);
			TweenMax.to(bottomZone,timming,{y:posy, autoAlpha:autoAlpha});
		}
		
		/**
		 * ADD content to bottom zone
		 * */
		protected function addToBottomZone(_content:DisplayObject):void
		{
			if(!bottomZone) createBottomZone();
			
			//Add content
			content = _content;
			bottomZone.addChild(content);
			redrawBottomZoneBkg();
			showBottomZone(false,0);
			//// - bottomZone.height;
		}
		
		/**
		 * REDRAW bottom zone bkg
		 * */
		protected function redrawBottomZoneBkg():void
		{
			content.y = 20;
			content.x = (titleView.width-1 - content.width)*.5;
			//redraw BKG
			bottomZone.graphics.clear();
			bottomZone.graphics.beginFill(Colors.WHITE);
			bottomZone.graphics.drawRect(0,0,titleView.width-1,bottomZone.height + 40);
			/*
			bottomZone.graphics.lineStyle(2,Colors.GREY_LIGHT);
			bottomZone.graphics.moveTo(0,0)
			bottomZone.graphics.lineTo(titleView.width-1,0);
			*/
			bottomZone.graphics.endFill();
			
			// shadow 
			bottomZone.filters = [new DropShadowFilter(2,-90,0,0.1,0,10)];
		}
		
		/**
		 * Show as adaptable grid
		 *
		 * */
		
		protected function showAsGrid(views:Vector.<ThumbItem>, parent:Sprite, maxWidth:Number, margin:Number = 5):void
		{
			var paddingLeft:Number = 15;
			var prev:Sprite;
			var posx:Number = paddingLeft;
			var posy:Number = 10;
			var nextx:Number;
			
			for (var i:int = 0; i < views.length; i++) 
			{
				
				var view:Sprite = views[i] as Sprite;
				
				posx = (i>0)?prev.x + prev.width + margin:posx;
				nextx = (posx + view.width + margin);
				if(nextx < maxWidth)
					view.x = posx;
				else
				{
					view.x = paddingLeft;
					posy += view.height + margin;
				}
				view.y = posy;
				
				prev = view;
				parent.addChild(view);
			}
		}
		
		
		protected function accordionResizeHandler():void
		{
			/*TweenMax.killDelayedCallsTo(scrollPane.update);
			TweenMax.delayedCall(1,scrollPane.update);*/
			//scrollPane.update();
			//trace("Left tab abstract : accordion resize handler"); 
			stageResizeHandler(null);
		}
		
		
		protected function stageResizeHandler(event:Event):void
		{
			refreshScrollPaneHeight();
			
			if(bottomZone)
			{
				//var posy:Number = (bottomZoneVisibility)?scrollPane.y+ scrollPane.height:scrollPane.y+ scrollPane.height;
				bottomZone.y = scrollPane.y + scrollPane.height;//posy;
			}
		}
		
		
		
		/**
		 * CREATE Bottom zone
		 * */
		private function createBottomZone():void
		{
			//bottom zone
			bottomZone = new Sprite();
			bottomZone.graphics.beginFill(Colors.BLUE_LIGHT);
			bottomZone.graphics.drawRect(0,0,titleView.width-1,titleView.height);
			bottomZone.x = 1;
			
			addChild(bottomZone);
		}
		
		/**
		 * update title icons (collapse, expand, sort by date, sort by name)
		 */
		protected function updateIcons():void
		{
			// expand collapse
			if(accordion.accordionItems.length>0)
				expandButton.visible = !(accordion.accordionItems[0].isOpen);
			else
				expandButton.visible = true;
			
			collapseButton.visible = !expandButton.visible;
			
		}
		
		
		/**
		 * when clicking on expand icon
		 * > expand all photo folder
		 * > save it to prefs
		 * > switch button
		 */
		private function expandHandler(e:Event = null):void
		{
			// open all folders
			for (var i:int = 0; i < accordion.accordionItems.length; i++) 
			{
				var item : AccordionItem = accordion.accordionItems[i];
				item.open(true, false, true);
			}
			scrollPane.scrollVerticallyTo(0);
			accordionResizeHandler();
			
			updateIcons();
		}
		
		/**
		 * when clicking on collapse icon
		 * > expand all photo folder
		 * > save it to prefs
		 * > switch buttons
		 */
		private function collapseHandler(e:Event = null):void
		{
			// close all folders
			for (var i:int = 0; i < accordion.accordionItems.length; i++) 
			{
				var item : AccordionItem = accordion.accordionItems[i];
				item.open(false, false, true);
			}
			scrollPane.scrollVerticallyTo(0);
			accordionResizeHandler();
			
			updateIcons();
		}
		
		/**
		 * Refresh scrollpane height
		 * */
		private function refreshScrollPaneHeight():void
		{
			if(!parent) return;
			
			globalPos = new Point(x,y);
			globalPos = parent.localToGlobal(globalPos);
			globalPos = Infos.stage.globalToLocal(globalPos);
			
			var bottomZoneHeight:Number = (bottomZone && bottomZoneVisibility)?bottomZone.height:0;
			
			scrollPane.setSize(scrollPane.width,Infos.stage.stageHeight-globalPos.y-titleView.height-bottomZoneHeight);
			accordion.SCROLL_LIMIT_BOTTOM = scrollPane.height;
		}
		
		
	}
}