﻿/***************************************************************
 COPYRIGHT 		: Jonathan@nguyen.eu
 YEAR			: 2013
 ****************************************************************/
package view.menu.lefttabs 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	import be.antho.utils.tabs.TabManager;
	
	import comp.accordion.Accordion;
	
	import data.Infos;
	import data.ProductsCatalogue;
	import data.TutorialStepVo;
	
	import manager.PhotoSwapManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	import manager.TutorialManager;
	
	import offline.manager.OfflineAssetsManager;
	
	import org.osflash.signals.Signal;
	
	import utils.Debug;
	
	import view.menu.lefttabs.backgrounds.BackgroundsTab;
	import view.menu.lefttabs.cliparts.ClipartsTab;
	import view.menu.lefttabs.layouts.LayoutsTab;
	import view.menu.lefttabs.overlayers.OverlayersTab;
	import view.menu.lefttabs.photos.PhotosTab;
	import view.menu.lefttabs.photos.PhotosTabOffline;
	import view.menu.lefttabs.project.album.ProjectAlbumTab;
	import view.menu.lefttabs.project.calendar.ProjectCalendarTab;
	import view.menu.lefttabs.project.canvas.ProjectCanvasTab;
	import view.menu.lefttabs.project.cards.ProjectCardsTab;

	public class LeftTabs extends Sprite
	{
		//Static
		public static const TABS_ID:String = "leftmenutabs";	
		
		// signals
		public static var RESIZE:Signal = new Signal();
		
		// view
		private var tabContentWrapper:Sprite;
		private var tabButtonContainer : Sprite;
		
		// data
		private var tabContentList:Vector.<LeftTabContent>;
		private var tabButtonList:Vector.<LeftTabButton>;
		private var tutorialVo : TutorialStepVo;
		private var _WIDTH:Number;
				
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////

		private var tabBtnBottomPosY:*;
		
		public function LeftTabs() 
		{
			// listeners
			PhotoSwapManager.SWAP_IMAGE_MODE.add(onSwapModeChange);
			
			//Listen for the retract event
			TabManager.OPEN.add(retractHandler);
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		public static function openDefaultTab():void
		{
			// set current opened tab
			var defaultTabIndex:int = (ProjectManager.instance.ready)?1:0;
			TabManager.instance.openTab(defaultTabIndex,TABS_ID);
			TabManager.instance.open(true);
		}
		
		/**
		 * update tab view depending on projectmanager content
		 */
		public function updateView(forceReset:Boolean = false):void
		{
			// do we need a reset ?
			if(! tabContentWrapper  || forceReset) reset();
			// TODO we should check here when to reset tabs (if classname has changed for example)
			
			// set tab enabled
			var leftTabBtn : LeftTabButton;
			for (var i:int = 0; i < tabButtonList.length; i++) 
			{
				leftTabBtn = (tabButtonList[i] as LeftTabButton);
				leftTabBtn.enabled = ( ProjectManager.instance.ready || leftTabBtn.content.id == "project") ? true : false; 
			}
			
			openDefaultTab();
		}
		
		
		
		
		
		/**
		 * Handling stage resize
		 * For now, just update background
		 * */
		public function updateLayout(event:Event = null):void
		{
			if(tabContentWrapper)
			{
				tabContentWrapper.graphics.clear();
				tabContentWrapper.graphics.beginFill(0xFFFFFF);
				tabContentWrapper.graphics.drawRect(1,0,Accordion.MAX_WIDTH-1,stage.stageHeight-y);
				tabContentWrapper.graphics.endFill();
			}
		}	
		
		/**
		 * DESKTOP project change
		 * update view does the trick
		 */
		public function desktopProjectChange():void
		{
			updateView(true);
		}
		
		////////////////////////////////////////////////////////////////
		//	getter/setter
		////////////////////////////////////////////////////////////////
		
		public function get WIDTH():Number
		{
			if(!tabContentWrapper) return 0;
			return (tabContentWrapper.visible)? 390 : tabContentWrapper.x;
		}
		
		public function set WIDTH(value:Number):void
		{
			_WIDTH = value;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 * reset tabs with correct menu xml
		 * THIS should be done only once at start and if user changes classname !
		 */
		private function reset():void
		{
			if(!stage) throw new Error("LeftTab.init must be done once leftTab is on display list");
			
			// first dispose and reset content
			dispose();
			
			// construct content base on menu xml
			construct( ProjectManager.instance.menuXML );
			
			// update layout
			updateLayout();
		}
		
		
		
		/**
		 * construct is the function used to construct the view, add children, prepare animations, everything before the show method.
		 * This function should be only called once after the creation of the view
		 */
		private function construct(menuXML:XML):void
		{	
			// create containers
			tabContentWrapper = new Sprite();
			addChild(tabContentWrapper);
			tabButtonContainer = new Sprite();
			addChild(tabButtonContainer);
			
			
			// list 
			tabContentList = new Vector.<LeftTabContent>;
			tabButtonList = new Vector.<LeftTabButton>;
			
			// Retreive tabs from XML
			var tabsList:XMLList = menuXML.menu.item;
			for (var i:int = 0; i < tabsList.length(); i++) 
			{
				// tab id
				var tabXml:XML = tabsList[i];
				var id:String =tabXml.@id;
				
				//create tab button
				var tabBtn:LeftTabButton = new LeftTabButton();
				tabBtn.construct(id);
				tabBtn.y = i*(tabBtn.height+1);
				
				
				
				//if no project selected, disable all tab except project tab
				if(!ProjectManager.instance.ready && id!="project")
					tabBtn.enabled = false;

				
				//tab content
				var tabContent:LeftTabContent;
				switch(id)
				{
					case "project":
						tabContent = getProjectContentByProjectClass(tabXml); // TODO: switch content type based on the product type (album, calendars, cards etc..)
						if(tabContent != null)
						{
							//tabContent.x = tabBtn.width;
							tabContent.hide();
							tabContentWrapper.addChild(tabContent);
						}
						else
						{
							Debug.error("Could not find the related project tab for the classname '"+SessionManager.instance.session.classname+"'");
							return;
						}
						
						break;
					
					case "photos":
						
						// ONLINE EDITOR
						CONFIG::online
						{
							tabContent = new PhotosTab(tabXml);
						}
						
						// OFFLINE EDITOR
						CONFIG::offline
						{
							tabContent = new PhotosTabOffline(tabXml);
						}
						
						
						//tabContent.x = tabBtn.width;
						tabContent.hide();
						tabContentWrapper.addChild(tabContent);
						break;
					
					case "layouts":
						tabContent = new LayoutsTab(tabXml);
						//tabContent.x = tabBtn.width;
						tabContent.hide();
						tabContentWrapper.addChild(tabContent);
						break;
					
					case "backgrounds":
						tabContent = new BackgroundsTab(tabXml);
						//tabContent.x = tabBtn.width;
						tabContent.hide();
						tabContentWrapper.addChild(tabContent);
						break;
					
					case "cliparts":
						tabContent = new ClipartsTab(tabXml);
						//tabContent.x = tabBtn.width;
						tabContent.hide();
						tabContentWrapper.addChild(tabContent);
						break;	
					
					case "overlayers":
						tabContent = new OverlayersTab(tabXml);
						//tabContent.x = tabBtn.width;
						tabContent.hide();
						tabContentWrapper.addChild(tabContent);
						break;	
				}
				
				
				//addchild btn
				tabButtonContainer.addChild(tabBtn);
				tabBtnBottomPosY = tabBtn.y + tabBtn.height;
				
				//add tab to groupTab
				TabManager.instance.addTab(tabBtn,tabContent,TABS_ID);
				
				// add to list
				tabContentList.push(tabContent);
				tabButtonList.push(tabBtn);
			}	
			
			// positionate 
			//tabContentWrapper.x = tabButtonContainer.width - 5;
			//position wrapper
			tabContentWrapper.x = LeftTabButton.WIDTH;
			
			//Add to tutorial flow if not already done for this component
			if(!tutorialVo)
			{
				tutorialVo = new TutorialStepVo(
					this,
					TutorialManager.KEY_LEFT_MENU,
					TutorialManager.ARROW_LEFT,
					new Point(this.width + 20, 200)
				);
				TutorialManager.instance.addStep(tutorialVo);
			}
		}
		
		/**
		 *
		 */
		public function dispose():void
		{
			Debug.log("LeftTabs.dispose");
			if(!tabContentWrapper) return;
			
			tabContentWrapper.graphics.clear();
			
			for (var i:int = 0; i < tabButtonList.length; i++) 
			{
				// destroy button
				(tabButtonList[i] as LeftTabButton).destroy();
				
				// destroy content
				(tabContentList[i] as LeftTabContent).destroy();
			}
			
			// remove wrappers
			tabContentWrapper.parent.removeChild(tabContentWrapper);
			tabButtonContainer.parent.removeChild(tabButtonContainer);
			tabContentWrapper = null;
			tabButtonContainer = null;
			
			// clean lists
			tabButtonList= null;
			tabContentList = null;
			
			// remove tab group
			TabManager.instance.killGroup(TABS_ID);
			
		}
		
		
		
		/**
		 * Retract handler
		 * Hide/show the content wrapper
		 * */
		private function retractHandler(flag:Boolean):void
		{
			if( TutorialManager.instance.isRunning || Infos.project && tabContentWrapper )
			{
				tabContentWrapper.visible = flag;
				RESIZE.dispatch();
			}
		}
		
		private function getProjectContentByProjectClass(tabXml:XML):LeftTabContent
		{
			switch(SessionManager.instance.session.classname)
			{
				case ProductsCatalogue.CLASS_ALBUM:
						return new ProjectAlbumTab(tabXml);
					break;
				
				case ProductsCatalogue.CLASS_CALENDARS:
						return new ProjectCalendarTab(tabXml);
					break;
				
				case ProductsCatalogue.CLASS_CANVAS:
						return new ProjectCanvasTab(tabXml);
					break;
				
				case ProductsCatalogue.CLASS_CARDS:
						return new ProjectCardsTab(tabXml);
					break;
						
				
			}
			return null
		}
		
		
			
		
		/**
		 *
		 */
		private function onSwapModeChange( flag:Boolean ):void
		{
			alpha = (flag)?.2 : 1;
			mouseChildren = mouseEnabled = !flag;
		}
		
		

	}
	
}