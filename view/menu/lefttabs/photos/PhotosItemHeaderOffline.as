package view.menu.lefttabs.photos
{
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import be.antho.data.ResourcesManager;
	
	import comp.accordion.AccordionItemHeader;
	
	import data.Infos;
	import data.PhotoVo;
	import data.TooltipVo;
	
	import library.icons.autofill;
	
	import manager.ProjectManager;
	
	import mcs.leftmenu.accordion.item.header.photos.view;
	
	import utils.ButtonUtils;
	import utils.Colors;
	
	import view.popup.PopupAbstract;
	
	public class PhotosItemHeaderOffline extends AccordionItemHeader
	{
		public var folderName:String
		public var photoList:Vector.<PhotoVo>;
		private var autoFillButton : Sprite;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PhotosItemHeaderOffline()
		{
			super();
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{
			var view:MovieClip = new mcs.leftmenu.accordion.item.header.photos.view(); // SWC assets
			var color:Number = (!Infos.config.isRebranding)?Colors.BLUE:Infos.config.rebrandingMainColor;
			TweenMax.to(view.title,0,{tint:color});
			if(Infos.config.isRebranding) TweenMax.to(view.icon,0,{tint:color});
			
			view.title.text = folderName;
			view.infos.txt.text = photoList.length;
			bg = view.bg;
			addChild(view);
			
			view.title.mouseEnabled = view.title.mouseWheelEnabled = false;
			view.infos.mouseEnabled = view.infos.mouseChildren = false,
			mouseChildren = true;
			
			autoFillButton = ButtonUtils.makeButton(new library.icons.autofill(), onAutoFillClick, new TooltipVo("tooltip.folder.autoFill"),Colors.GREEN); 
			addChild(autoFillButton);
			autoFillButton.x = 235;
			autoFillButton.y = 18;
			
			//icon
			view.icon.gotoAndStop(2);
			
			setDefaultHitZone(bg);
		}
		
		/**
		 * autofill click handler 
		 */
		private function onAutoFillClick(e:MouseEvent):void
		{
			PopupAbstract.Alert(ResourcesManager.getString("popup.autoFill.warning.title"),ResourcesManager.getString("popup.autoFill.warning.description"),true, autoFillContinueHandler);
		}
		private function autoFillContinueHandler(p:PopupAbstract):void
		{
			ProjectManager.instance.makeAutoFill();
		}
		
	}
}