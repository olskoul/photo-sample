package view.menu.lefttabs.photos
{
	import comp.PhotoItem;
	import comp.ThumbItem;
	import comp.accordion.AccordionItemContent;
	
	public class PhotosItemContent extends AccordionItemContent
	{
		public var thumbList : Vector.<ThumbItem>;
		
		public function PhotosItemContent()
		{
			super();
		}
		
		
		/**
		 * update list items
		 */
		override public function updateContentVisibility(contentPosY:Number):void
		{
			var posY: int = contentPosY;
			var thumb: ThumbItem;
			thumbList = new Vector.<ThumbItem>();
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				thumb = this.getChildAt(i) as ThumbItem;
				if(thumb)
				{
					if(posY + thumb.y + thumb.height < 0 || posY + thumb.y > contentVisibilityYLimit) {
						thumb.outro();
					}
					else {
						thumb.intro();
					}
					thumbList.push(thumb);
				}
			}
			
		}
		
		/**
		*
		*/
		override public function destroy():void
		{
			thumbList = null;
			super.destroy();
		}
	}
}