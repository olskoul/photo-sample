package view.menu.lefttabs.photos
{
	import be.antho.data.ResourcesManager;
	
	import com.bit101.components.PushButton;
	import com.greensock.TweenMax;
	
	import comp.accordion.AccordionItemHeader;
	
	import data.AlbumVo;
	import data.Infos;
	import data.TooltipVo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import library.icons.autofill;
	
	import manager.FolderManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import mcs.icons.photUsedIcon;
	import mcs.leftmenu.accordion.item.header.photos.view;
	import mcs.navigator.lib_groupDropGfx;
	
	import utils.ButtonUtils;
	import utils.Colors;
	
	import view.popup.PopupAbstract;
	
	public class PhotosItemHeader extends AccordionItemHeader
	{
		public var albumVo:AlbumVo;
		private var autoFillButton : Sprite;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PhotosItemHeader()
		{
			super();
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{
			var view:MovieClip = new mcs.leftmenu.accordion.item.header.photos.view(); // SWC assets
			var color:Number = (!Infos.config.isRebranding)?Colors.BLUE:Infos.config.rebrandingMainColor;
			TweenMax.to(view.title,0,{tint:color});
			if(Infos.config.isRebranding) TweenMax.to(view.icon,0,{tint:color});
			
			view.title.text = FolderManager.instance.getFolderDisplayName( albumVo.name );
			view.infos.txt.text = albumVo.photoList.length;
			bg = view.bg;
			addChild(view);
			
			view.title.mouseEnabled = view.title.mouseWheelEnabled = false;
			view.infos.mouseEnabled = view.infos.mouseChildren = false,
			mouseChildren = true;
			
			autoFillButton = ButtonUtils.makeButton(new library.icons.autofill(), onAutoFillClick, new TooltipVo("tooltip.folder.autoFill"),Colors.GREEN); 
			addChild(autoFillButton);
			autoFillButton.x = 235;
			autoFillButton.y = 18;
			
			//icon
			var folderType:int = FolderManager.instance.getFolderOrderPriority(albumVo);
			if(folderType == 0) view.icon.gotoAndStop(2);
			if(folderType == 1) view.icon.gotoAndStop(3);
			
			setDefaultHitZone(bg);
		}
		
		/**
		 * autofill click handler 
		 */
		private function onAutoFillClick(e:MouseEvent):void
		{
			PopupAbstract.Alert(ResourcesManager.getString("popup.autoFill.warning.title"),ResourcesManager.getString("popup.autoFill.warning.description"),true, autoFillContinueHandler);
		}
		private function autoFillContinueHandler(p:PopupAbstract):void
		{
			ProjectManager.instance.makeAutoFill( albumVo );
		}
		
	}
}