package view.menu.lefttabs.layouts
{
	import comp.accordion.AccordionItemHeader;
	
	import data.AlbumVo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import mcs.leftmenu.accordion.item.header.photos.view;
	
	public class LayoutItemHeader extends AccordionItemHeader
	{
		public var title:String;
		public var totalLayout:int;

		private var content:MovieClip;
		
		public function LayoutItemHeader()
		{
			super();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		override public function init():void
		{
			super.init();	
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{	
			content = new mcs.leftmenu.accordion.item.header.photos.view(); // SWC assets
			content.title.text = title;
			content.infos.txt.text = String(totalLayout);
			addChild(content);
			content.mouseChildren = false;
			
			setDefaultHitZone(content);
		}
		
		public function updateInfos(string:String = "0"):void
		{
			content.infos.txt.text = string;
		}
	}
}