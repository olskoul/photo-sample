package view.menu.lefttabs.layouts
{
	import be.antho.data.ResourcesManager;
	
	import com.bit101.components.HBox;
	import com.greensock.TweenMax;
	
	import comp.BackgroundItem;
	import comp.DefaultTooltip;
	import comp.LayoutItem;
	import comp.ThumbItem;
	import comp.button.SimpleButton;
	import comp.button.SimpleThinButton;
	
	import data.BackgroundVo;
	import data.ThumbVo;
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import manager.LayoutManager;
	import manager.PagesManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.popup.PopupAbstract;
	
	public class LayoutCustomBar extends Sprite
	{
		public static const CUSTOM_LAYOUTS_UPDATED:Signal = new Signal();
		
		private var currentSelectedBkg:BackgroundItem;
		private var preview:Bitmap;
		private var wrapper:HBox;
		private var deleteBtn:SimpleThinButton;
		private var saveBtn:SimpleThinButton;
		private var previewContainer:Sprite;
		private var selectedItem:LayoutItem;
		
		public function LayoutCustomBar()
		{
			super();
			construct();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Setter of the current selected bkg item
		 * Change preview
		 * */
		public function set selectedBkg(item:BackgroundItem):void
		{
			currentSelectedBkg = item;
			setPreview()
		}
		
		/**
		 * Dispose
		 * */
		
		public function dispose():void
		{
			if(preview.bitmapData)
			preview.bitmapData.dispose();
		}
		
		/**
		 * Destroy
		 * */
		
		public function destroy():void
		{
			dispose();
			if(parent)
				parent.removeChild(this);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Change preview bitmap
		 * Contraint to bounds
		 * */
		private function setPreview():void
		{
			preview.bitmapData = currentSelectedBkg.cloneThumbBitmapdata();
			if(preview.bitmapData != null)
			{
				activate(true);
				preview.height = deleteBtn.height;
				preview.scaleX = preview.scaleY;
				
				previewContainer.graphics.clear();
				previewContainer.graphics.lineStyle(2,Colors.GREY);
				previewContainer.graphics.drawRect(0,0,preview.width,preview.height);
				
				previewContainer.alpha = 0;
				TweenMax.killTweensOf(previewContainer);
				TweenMax.to(previewContainer,1,{alpha:1});
				wrapper.draw();
			}
			else
			{
				activate(false);
				Debug.warn("LayoutCustomBar.setPreview: preview.bitmapData is null");
			}
			
		}
		private function activate( flag:Boolean ):void
		{
			wrapper.mouseChildren = wrapper.mouseEnabled = flag;
		}
		
		/**
		 * Build view
		 * */
		private function construct():void
		{
			wrapper = new HBox(this);
			wrapper.spacing = 10;
			wrapper.alignment = HBox.MIDDLE;
			
			/*previewContainer = new Sprite();
			preview = new Bitmap();
			previewContainer.addChild(preview);
			wrapper.addChild(previewContainer);*/
			
			saveBtn = new SimpleThinButton(	"customLayout.save", // label
												Colors.GREY_LIGHT, //over color
												saveCurrentLayout, //handler
												Colors.GREEN, //off color
												Colors.WHITE, //off color label
												Colors.GREEN); // over color label
			
			saveBtn.tooltip = new TooltipVo("tooltip.customLayout.save",TooltipVo.TYPE_SIMPLE,null,Colors.GREEN, Colors.WHITE);			
			
			deleteBtn = new SimpleThinButton("common.delete", // label
												Colors.GREY_LIGHT, //over color
												deleteHandler, //handler
												Colors.RED, //off color
												Colors.WHITE, //off color label
												Colors.RED); // over color label
			
			deleteBtn.tooltip = new TooltipVo("tooltip.customLayout.delete",TooltipVo.TYPE_SIMPLE,null,Colors.RED, Colors.WHITE);
			
			wrapper.addChild(saveBtn);				
			wrapper.addChild(deleteBtn);	
			
			//listeners
			ThumbItem.SELECTED.add(thumbSelectedHandler);
			
			//Disable delete by default
			deleteBtn.enabled = false;
		}
		
		/**
		 * ThumbItem SELECTED 
		 * Check thumb type
		 * If LayoutItem -> if custom -> activate delete
		 * */
		private function thumbSelectedHandler(_item:ThumbItem):void
		{
			if(_item && _item is LayoutItem)
			{
				var item:LayoutItem = _item as LayoutItem;
				if(item.isCustomLayout)
				{
					selectedItem = item;
					deleteBtn.enabled = true;
				}
				else
					deleteBtn.enabled = false;
			}
		}
		
		/**
		 * Apply bkg to current page
		 * */
		private function saveCurrentLayout():void
		{
			Debug.log("LayoutCustomBar.saveCurrentLayout");
			try
			{
				LayoutManager.instance.saveCurrentPageLayout();
				//Send XML to server
				ServiceManager.instance.uploadCusotmLayoutXML();
				//refresh views
				CUSTOM_LAYOUTS_UPDATED.dispatch();
				//Popup confimration
				PopupAbstract.Alert(ResourcesManager.getString("popup.customlayout.title"), ResourcesManager.getString("popup.customlayout.save.confirmation"),false,null,null);	
			}
			catch(e:Error)
			{
				Debug.warnAndSendMail("Error Catched when trying to save page layout as custom Layout : "+e.toString());
			}
		}
		
		/**
		 * Delete btn handler
		 * */
		private function deleteHandler():void
		{
			Debug.log("LayoutCustomBar > deleteSelectedLayout");
			
			if(selectedItem)
			{
				PopupAbstract.Alert(ResourcesManager.getString("popup.customlayout.title"), ResourcesManager.getString("popup.customlayout.delete.confirmation"),true,deleteLayout,null);	
			}
		}
		
		/**
		 * Delete layout after Alert confirmation
		 * */
		private function deleteLayout(popup:PopupAbstract):void
		{
			Debug.log("LayoutCustomBar > deleteSelectedLayout > SelectediTem is: "+selectedItem.layoutId);
			//Delete layout in data provider
			LayoutManager.instance.deleteCustomLayoutById(selectedItem.layoutId);
			//disable delete btn
			deleteBtn.enabled = false;
			//Send XML to server
			ServiceManager.instance.uploadCusotmLayoutXML();
			//refresh views
			CUSTOM_LAYOUTS_UPDATED.dispatch();
		}
	}
}