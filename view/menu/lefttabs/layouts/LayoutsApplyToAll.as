package view.menu.lefttabs.layouts
{
	import be.antho.data.ResourcesManager;
	
	import com.bit101.components.HBox;
	import com.greensock.TweenMax;
	
	import comp.BackgroundItem;
	import comp.LayoutItem;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	
	import data.BackgroundVo;
	import data.ThumbVo;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	
	import manager.LayoutManager;
	import manager.PagesManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.popup.PopupAbstract;
	
	public class LayoutsApplyToAll extends Sprite
	{
		public static const APPLY_LAYOUT:Signal = new Signal();
		public static const APPLY_LAYOUT_TO_ALL:Signal = new Signal();
		
		private var currentSelectedItem:LayoutItem;

		private var preview:Bitmap;

		private var wrapper:HBox;

		private var deleteBtn:SimpleButton;
		
		private var applyToAllBtn:SimpleButton;

		private var applyBtn:SimpleButton;

		private var previewContainer:Sprite;
		
		public function LayoutsApplyToAll()
		{
			super();
			construct();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Setter of the current selected bkg item
		 * Change preview
		 * */
		public function set selectedItem(item:LayoutItem):void
		{
			if(item)
			{
				currentSelectedItem = item;
				setPreview();
				
				deleteBtn.enabled = (item.isCustomLayout);
				applyBtn.enabled = true;
				if(!PagesManager.instance.currentEditedPage.isCover)
				applyToAllBtn.enabled = true;
				else
					applyToAllBtn.enabled = false;
			}
			else
			{
				dispose();
				deleteBtn.enabled = false;
				applyBtn.enabled = false;
				applyToAllBtn.enabled = false;
			}
			
		}
		
		/**
		 * Dispose
		 * */
		
		public function dispose():void
		{
			if(preview.bitmapData)
			preview.bitmapData.dispose();
		}
		
		/**
		 * Destroy
		 * */
		
		public function destroy():void
		{
			dispose();
			if(parent)
				parent.removeChild(this);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Change preview bitmap
		 * Contraint to bounds
		 * */
		private function setPreview():void
		{
			preview.bitmapData = currentSelectedItem.cloneThumbBitmapdata();
			if(preview.bitmapData != null)
			{
				activate(true);
				preview.width = 40;
				preview.height = 32;
				
				
				previewContainer.graphics.clear();
				previewContainer.graphics.lineStyle(2,Colors.GREY);
				previewContainer.graphics.drawRect(0,0,preview.width,preview.height);
				
				previewContainer.alpha = 0;
				TweenMax.killTweensOf(previewContainer);
				TweenMax.to(previewContainer,1,{alpha:1});
				wrapper.draw();
			}
			else
			{
				activate(false);
				Debug.warn("LayoutApplytoAll.setPreview: preview.bitmapData is null");
			}
		}
		private function activate( flag:Boolean ):void
		{
			wrapper.mouseChildren = wrapper.mouseEnabled = flag;
		}
		
		/**
		 * Build view
		 * */
		private function construct():void
		{
			wrapper = new HBox(this,0);
			wrapper.spacing = 5;
			wrapper.alignment = HBox.MIDDLE;
			
			previewContainer = new Sprite();
			preview = new Bitmap();
			preview.smoothing = true;
			previewContainer.addChild(preview);
			wrapper.addChild(previewContainer);
			
			applyBtn = SimpleButton.createApplyButton("common.apply", applyToCurrentPage );
			applyBtn.margin = 7;
			
			applyToAllBtn = SimpleButton.createApplyToAllButton( "lefttab.backgrounds.applytoall",applyToAllPages);
			applyToAllBtn.margin = 7;
			
			deleteBtn = new SimpleSquareButton("", 
																Colors.GREY_LIGHT, 
																deleteCurrentLayout, 
																Colors.RED, 
																Colors.WHITE, 
																Colors.RED,
																1,
																false,
																3);
			
			wrapper.addChild(deleteBtn);
			wrapper.addChild(applyToAllBtn);
			wrapper.addChild(applyBtn);		
					
			
			applyBtn.enabled = false;
			applyToAllBtn.enabled = false;
			deleteBtn.enabled = false;
			
		}
		
		/**
		 * Apply bkg to current page
		 * */
		private function applyToCurrentPage():void
		{
			Debug.log("LayoutsApplyToAll > applyToCurrentPage");
			
			if(currentSelectedItem)
				APPLY_LAYOUT.dispatch(currentSelectedItem);
		}
		
		private function applyToAllPages():void
		{
			Debug.log("LayoutsApplyToAll > applyToAllPages");
			
			if(currentSelectedItem)
				APPLY_LAYOUT_TO_ALL.dispatch(currentSelectedItem);
		}
		
		/**
		 * Delete btn handler
		 * */
		private function deleteCurrentLayout():void
		{
			Debug.log("LayoutsApplyToAll > deleteSelectedLayout");
			
			if(currentSelectedItem && currentSelectedItem.isCustomLayout)
			{
				PopupAbstract.Alert(ResourcesManager.getString("popup.customlayout.title"), ResourcesManager.getString("popup.customlayout.delete.confirmation"),true,deleteLayout,null);	
			}
			else
				deleteBtn.enabled = false;
		}
		
		/**
		 * Delete layout after Alert confirmation
		 * */
		private function deleteLayout(popup:PopupAbstract):void
		{
			Debug.log("LayoutsApplyToAll > deleteSelectedLayout > SelectediTem is: "+currentSelectedItem.layoutId);
			//Delete layout in data provider
			LayoutManager.instance.deleteCustomLayoutById(currentSelectedItem.layoutId);
			//disable delete btn
			deleteBtn.enabled = false;
			//Send XML to server
			ServiceManager.instance.uploadCusotmLayoutXML();
			//refresh views
			LayoutsTab.CUSTOM_LAYOUTS_UPDATED.dispatch();
			//reset selected item
			selectedItem = null;
		}
	}
}