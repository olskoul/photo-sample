package view.menu.lefttabs.layouts
{
	import com.bit101.components.Style;
	
	import flash.display.Shape;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	
	import be.antho.data.ResourcesManager;
	
	import comp.LayoutItem;
	import comp.ThumbItem;
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItem;
	import comp.button.SimpleSquareButton;
	
	import data.Infos;
	import data.LayoutVo;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.ThumbVo;
	
	import manager.LayoutManager;
	import manager.OverlayerManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.EditionArea;
	import view.edition.PageArea;
	import view.menu.lefttabs.LeftTabContent;
	import view.menu.lefttabs.layouts.LayoutCustomBar;
	import view.menu.lefttabs.photos.PhotosItemContent;
	import view.popup.PopupAbstract;
	
	public class LayoutsTab extends LeftTabContent
	{
		private var lastPage:PageVo;
		private var allViews:Vector.<ThumbItem>;
		private var views:Vector.<ThumbItem>;
		private var layoutCustomBar:LayoutCustomBar;
		private var saveCurrentLayoutBtn : SimpleSquareButton;
		private var lineBeforeButton : Shape;
		private var applyToAll:LayoutsApplyToAll;
		
		private var emptyLabel : TextField;
		
		public static const CUSTOM_LAYOUTS_UPDATED:Signal = new Signal();
		private static const CUSTOM_LAYOUT_UID:String = "CUSTOM_LAYOUT_UID";
		
		public function LayoutsTab(xmlData:XML)
		{
			super(xmlData);
		}
		
		override public function init():void
		{
			Debug.log("LayoutTab > init");
			
			super.init();
			
			//Expand/collapse all buttons
			addExpandCollapseBtn();
			
			//apply to All 
			applyToAll = new LayoutsApplyToAll();
			LayoutsApplyToAll.APPLY_LAYOUT.add(applyLayoutToCurrentPage);
			LayoutsApplyToAll.APPLY_LAYOUT_TO_ALL.add(applyLayoutToAllPage);
			addToBottomZone(applyToAll);
			
			//Show thumbs
			updateView();
			
			
			//listen to the page draw
			EditionArea.PAGE_DRAW_COMPLETE.add(pageDrawCompleteHandler);
			//listen to custom layout actions
			CUSTOM_LAYOUTS_UPDATED.add(updateCustomLayoutAccordion);
		}
		
		/**
		 * Destroy overide
		 */
		public override function destroy():void
		{
			
			//listeners
			LayoutsApplyToAll.APPLY_LAYOUT.remove(applyLayoutToCurrentPage);
			LayoutsApplyToAll.APPLY_LAYOUT_TO_ALL.remove(applyLayoutToAllPage);
			//listen to the page draw
			EditionArea.PAGE_DRAW_COMPLETE.remove(pageDrawCompleteHandler);
			//listen to custom layout actions
			CUSTOM_LAYOUTS_UPDATED.remove(updateCustomLayoutAccordion);
			
			//dispose
			dispose();
			
			super.destroy();
		}
		
		/**
		 * Add Layout Custom bar to bottom zone
		 * */
		/*private function addLayoutCustomBar():void
		{
			layoutCustomBar = new LayoutCustomBar();
			LayoutCustomBar.CUSTOM_LAYOUTS_UPDATED.add(updateCustomLayoutAccordion);
			addToBottomZone(layoutCustomBar);
		}*/
		
		/**
		 * Add content undr title
		 * */
		override protected function addContentUnderTitle():void
		{
			if(!Infos.isAlbum) return;
			
			// title bar height
			titleView.bg.height = 92;
			titleView.line.y = titleView.bg.height -0.5;
			
			// Save curretn layout btn
			saveCurrentLayoutBtn = new SimpleSquareButton("customLayout.save", Colors.BLUE, saveCurrentLayout, Colors.YELLOW, Colors.WHITE, Colors.WHITE, 1,false,2);
			saveCurrentLayoutBtn.forcedWidth = 288;
			saveCurrentLayoutBtn.x = 2;
			saveCurrentLayoutBtn.y = 55;
			titleView.addChild( saveCurrentLayoutBtn );
			
			// line before button
			lineBeforeButton = new Shape();
			lineBeforeButton.graphics.beginFill(0xE5E5E5,1);
			lineBeforeButton.graphics.drawRect(0,0,titleView.bg.width,1);
			lineBeforeButton.graphics.endFill();
			lineBeforeButton.y = saveCurrentLayoutBtn.y-3;
			titleView.addChild(lineBeforeButton);
		}
		
		/**
		 * Save current page layout
		 * */
		private function saveCurrentLayout():void
		{
			Debug.log("LayoutsTab > Save Current Layout");
			try
			{
				// save and create layout 
				LayoutManager.instance.saveCurrentPageLayout();
				//Send XML to server
				ServiceManager.instance.uploadCusotmLayoutXML();
				//refresh views
				CUSTOM_LAYOUTS_UPDATED.dispatch();
				//Popup confimration
				PopupAbstract.Alert(ResourcesManager.getString("popup.customlayout.title"), ResourcesManager.getString("popup.customlayout.save.confirmation"),false,null,null);	
			}
			catch(e:*)
			{
				Debug.warnAndSendMail("Error Catched when trying to save page layout as custom Layout");
			}
		}
		
		/**
		 * Dispose custom layout accodion item
		 * And re-buid it
		 * */
		private function updateCustomLayoutAccordion():void
		{
			var accordionItem:AccordionItem = accordion.getItemById(CUSTOM_LAYOUT_UID);
			Debug.log("LayoutTab > updateCustomLayoutAccordion");
			
			//create conten wrapper
			var listContent:PhotosItemContent = new PhotosItemContent();
			//fill with content
			var layoutDataProvider:Vector.<String> = (!PagesManager.instance.currentEditedPage.isCover)?LayoutManager.instance.publicAvailableCustomLayoutIds:LayoutManager.instance.publicAvailableCustomCoverLayoutIds;
			//view refs
			views = new Vector.<ThumbItem>();
			var layoutVo : LayoutVo;
			var layoutItem : LayoutItem;
			var isCover : Boolean;
			for (var i:int = 0; i < layoutDataProvider.length; i++) 
			{
				isCover = PagesManager.instance.currentEditedPage.isCover;
				layoutVo = LayoutManager.instance.getLayoutById(layoutDataProvider[i],isCover, true);
				if(layoutVo != null)
				{
					layoutItem = LayoutManager.instance.createLayoutItem( layoutVo , isCover );
					layoutItem.DOUBLE_CLICK.add(applyLayoutToCurrentPage);
					layoutItem.CLICK.add(itemSelectedHandler);
					
					views.push(layoutItem);
					allViews.push(layoutItem);
				}
			}
			//Update headr infos
			LayoutItemHeader(accordionItem.header).updateInfos(String(views.length));
			
			//display views as grid
			showAsGrid(views,listContent,Accordion.MAX_WIDTH);	
			
			//replace content
			accordionItem.replaceContent(listContent,true,true);
		}
		
		
		
		
		
		/**
		 * enable/disable save current Layouts
		 */
		private function enableSaveCurrentLayout(flag:Boolean):void
		{
			if(saveCurrentLayoutBtn)
			saveCurrentLayoutBtn.enabled = flag;
		}
		
		/**
		 * create or update thumbs
		 */
		private function updateView():void
		{
			Debug.log("LayoutTab.updateView");
			
			// at first we dispose already existing content
			dispose()
			
			//Security
			if(PagesManager.instance.currentEditedPage is PageCoverClassicVo)
			{
				accordion.updateItems();
				
				// DISPLAY NO LAYOUT MESSAGE
				if(!emptyLabel){
					emptyLabel = new TextField();
					emptyLabel.width = Accordion.MAX_WIDTH - 30//,60;
					emptyLabel.autoSize = "center";
					emptyLabel.wordWrap = true;
					emptyLabel.multiline = true;
					emptyLabel.embedFonts = Style.embedFonts;
					emptyLabel.antiAliasType = AntiAliasType.ADVANCED;
					emptyLabel.selectable = false;
					emptyLabel.mouseEnabled = false;
					
					emptyLabel.defaultTextFormat = TextFormats.ASAP_REGULAR(13,Colors.GREY_DARK,TextFormatAlign.CENTER);
					emptyLabel.htmlText = ResourcesManager.getString("lefttab.layout.empty.label");
					
					emptyLabel.x = 15;
					emptyLabel.y = 100;
				}
				addChild(emptyLabel);
				
				showBottomZone(false);
				enableSaveCurrentLayout(false);
				return;
			}
			else if(Infos.isAlbum)
			{
				enableSaveCurrentLayout(true);
			}
			
			// remove empty label if needed
			if(emptyLabel && emptyLabel.parent) emptyLabel.parent.removeChild(emptyLabel);
			
			//show bottom zone
			showBottomZone(true);
			
			// create content
			var totalLayoutShown:int = 0;
			var listContent : PhotosItemContent;
			var listHeader : LayoutItemHeader;
			var layoutItem : LayoutItem;
			var thumbVo:ThumbVo;
			var layoutVo:LayoutVo;
			var layoutDataProvider:Vector.<String>
			var layoutByPhotoFrame:Object = {};
			var layoutCategories:Array = [];
			var isCover : Boolean;
			allViews = new Vector.<ThumbItem>();
			views = new Vector.<ThumbItem>();
			
			//CUSTOM LAYOUTS (album only)
			if(Infos.isAlbum && !(PagesManager.instance.currentEditedPage is PageCoverClassicVo))
			{
				Debug.log("LayoutTab.updateView : Showing custom layout");
				//create conten wrapper
				listContent = new PhotosItemContent();
				
				//fill with content
				layoutDataProvider = (!PagesManager.instance.currentEditedPage.isCover)?LayoutManager.instance.publicAvailableCustomLayoutIds:LayoutManager.instance.publicAvailableCustomCoverLayoutIds;
				
				for (var i:int = 0; i < layoutDataProvider.length; i++) 
				{
					isCover = PagesManager.instance.currentEditedPage.isCover;
					layoutVo = LayoutManager.instance.getLayoutById(layoutDataProvider[i],PagesManager.instance.currentEditedPage.isCover, true);
					
					if(layoutVo != null)
					{
						layoutItem = LayoutManager.instance.createLayoutItem( layoutVo, isCover );
						layoutItem.DOUBLE_CLICK.add(applyLayoutToCurrentPage);
						layoutItem.CLICK.add(itemSelectedHandler);
						views.push(layoutItem);
						allViews.push(layoutItem);
					}
				}
				
				// create header
				listHeader = new LayoutItemHeader();
				listHeader.title = ResourcesManager.getString("lefttab.layouts.customLayouts");
				listHeader.totalLayout = views.length;
				listHeader.init();
				
				//display views as grid
				showAsGrid(views,listContent,Accordion.MAX_WIDTH);
				
				//Enable state
				var isEnabled:Boolean = (layoutDataProvider.length > 0);
					
				
				// add it to accordion
				accordion.addAccordionItem(listHeader, listContent, CUSTOM_LAYOUT_UID,isEnabled, isEnabled, isEnabled);
			}
			
			//ALL LAYOUTS
			views = new Vector.<ThumbItem>();
			//create content wrapper
			listContent = new PhotosItemContent();
			layoutDataProvider = (PagesManager.instance.currentEditedPage.isCover)?LayoutManager.instance.publicAvailableCoverLayoutIds:LayoutManager.instance.publicAvailableLayoutIds;
			var calendarConditions:Boolean = true;
			var layoutIsHidden:Boolean;
			for (i = 0; i < layoutDataProvider.length; i++) 
			{
				isCover = PagesManager.instance.currentEditedPage.isCover;
				layoutVo = LayoutManager.instance.getLayoutById(layoutDataProvider[i],isCover);
				
				//calendar condition to show or not the layout
				calendarConditions = getCalendarVisibilityConditions(layoutVo);
				
				//isHidden
				layoutIsHidden = LayoutManager.instance.isHidden(layoutDataProvider[i],PagesManager.instance.currentEditedPage.isCover);
				
				//Based on old version xml, some layout doesn't have any frame in them, so it returns null.
				if(layoutVo != null && layoutVo.type != "hide" && !layoutIsHidden && calendarConditions)
				{
					//Categorize layout by frame photo amount
					categorizeLayout(layoutVo, layoutByPhotoFrame, layoutCategories);
					
					layoutItem = LayoutManager.instance.createLayoutItem( layoutVo, isCover );
					layoutItem.listenSelection();
					layoutItem.DOUBLE_CLICK.add(applyLayoutToCurrentPage);
					layoutItem.CLICK.add(itemSelectedHandler);
					views.push(layoutItem);
					allViews.push(layoutItem);
				}
			}
			
			// create header
			listHeader = new LayoutItemHeader();
			listHeader.title = ResourcesManager.getString("lefttab.layouts.allLayouts");
			listHeader.totalLayout = views.length;//LayoutManager.instance.publicAvailableLayoutIds.length;
			listHeader.init();
			
			//display views as grid
			showAsGrid(views,listContent,Accordion.MAX_WIDTH);	
			// add  to accordion
			accordion.addAccordionItem(listHeader, listContent, listHeader.title,false, true, true);
			
			
			
			//ALL LAYOUTS CATEGORIZED BY NUMBER OF PHOTO FRAME
			//order categories
			layoutCategories.sort(Array.NUMERIC);
			//reuse views vector
			
			
			//loop through cateogries and create item in accordion
			for (var j:int = 0; j < layoutCategories.length; j++) 
			{
				views = new Vector.<ThumbItem>();
				listContent = new PhotosItemContent();
				
				//loop through each layout vo of this category and create thumbs
				for (var k:int = 0; k < layoutByPhotoFrame[layoutCategories[j]].length; k++) 
				{
					layoutVo = layoutByPhotoFrame[layoutCategories[j]][k] as LayoutVo;
					//layoutVo = LayoutManager.instance.getLayoutById(layoutDataProvider[i],PagesManager.instance.currentEditedPage.isCover);
					
					//Based on old version xml, some layout doesn't have any frame in them, so it returns null.
					if(layoutVo != null)
					{
						layoutItem = LayoutManager.instance.createLayoutItem( layoutVo, isCover );
						layoutItem.DOUBLE_CLICK.add(applyLayoutToCurrentPage);
						layoutItem.CLICK.add(itemSelectedHandler);
						views.push(layoutItem);
						allViews.push(layoutItem);
					}
				
				}
				
				// create header
				listHeader = new LayoutItemHeader();
				var titlePlus:String = (layoutCategories[j] == 6)?"+":"";
				if(layoutCategories[j] < 2)
					listHeader.title = ""+layoutCategories[j]+titlePlus+" "+ResourcesManager.getString("common.photo");
				else
					listHeader.title = ""+layoutCategories[j]+titlePlus+" "+ResourcesManager.getString("common.photos");
				
				listHeader.totalLayout = views.length;
				listHeader.init();
				
				//display views as grid
				showAsGrid(views,listContent,Accordion.MAX_WIDTH);	
				// add  to accordion
				accordion.addAccordionItem(listHeader, listContent, listHeader.title,false, true, true);
			}
			
			
			
			// update visible items
			accordion.updateItems();
		}
		
		/**
		 * Layout item delete clicked
		 *
		 * */
		private function deletCustomLayoutHandler():void
		{
			trace("yomamama");
		}
		
		/**
		 * Layout item clicked
		 * set selected state
		 * dispatch SELECTED
		 * */
		private function itemSelectedHandler(item:ThumbItem):void
		{
			if(item is LayoutItem)
			{
				item.selected = true;
				applyToAll.selectedItem = item as LayoutItem;
				ThumbItem.SELECTED.dispatch(item);
				redrawBottomZoneBkg();
			}
		}
		
		/**
		 * Categorize LayoutVO by number of frame
		 * Put them into arrays
		 * */
		private function categorizeLayout(layoutVo:LayoutVo, layoutByPhotoFrame:Object, layoutCategories:Array):void
		{
			var photoNumber:int = layoutVo.photoNumber;
			photoNumber = (photoNumber>= 6)?6:photoNumber;
			if(!layoutByPhotoFrame[photoNumber])
			{
				layoutByPhotoFrame[photoNumber] = [];
				layoutCategories.push(photoNumber);
			}
			layoutByPhotoFrame[photoNumber].push(layoutVo);
		}
		
		/**
		 * 
		 * */
		private function getCalendarVisibilityConditions(layoutVo:LayoutVo):Boolean
		{
			if(!layoutVo) return false;
			
			if(layoutVo.isCalendar && !PagesManager.instance.currentEditedPage.isCalendar)
				return false;
			
			switch(Infos.project.docCode)
			{
				case "WCAL":
				case "DCAL":
				case "WCAL4":
				case "WCAL5":
				case "WCAL6":
				case "MCAL1":
				case "MCAL2":
					return true;
			}
			
			
			
			return true;
		}
		
		/**
		 * Page draw complete
		 * if the page isCover, show cover layout, else show normal layout
		 * */
		private function pageDrawCompleteHandler():void
		{
			var currentEditedPageVo:PageVo = PagesManager.instance.currentEditedPage;
			var projectHasCustomCover:Boolean = Infos.project.hasCustomCover;
			
			// force a redraw of content
			if( LayoutManager.instance.forceRedrawLayoutsAfterUpgrade ){
				LayoutManager.instance.forceRedrawLayoutsAfterUpgrade = false;
				updateView();
			}
			
			// refresh if no last page
			else if(!lastPage) updateView();
			
			//Calendar conditions (to be reviewed)
			else if(PagesManager.instance.currentEditedPage.isCalendar != lastPage.isCalendar) updateView();
			
			// page is cover but last page is not and vice versa
			else if( 	(currentEditedPageVo.isCover && !lastPage.isCover) 
					||	(!currentEditedPageVo.isCover && lastPage.isCover) ) updateView();
			
			//When upgrading between classic and contemporary
			else if( 	(lastPage is PageCoverClassicVo && !(currentEditedPageVo is PageCoverClassicVo)) 
					 || (!(lastPage is PageCoverClassicVo) && currentEditedPageVo is PageCoverClassicVo)) updateView();
	
			// save last page
			lastPage = PagesManager.instance.currentEditedPage;
		}
		
		/**
		 *
		 */
		private function dispose():void
		{
			// dispose accordion
			if(accordion)
			accordion.dispose();
						
			// destroy thumb items
			var layoutItem : LayoutItem;
			if(views)
				views = null;
			if(allViews){
				for (var i:int = 0; i < allViews.length; i++) 
				{
					layoutItem = allViews[i] as LayoutItem;
					layoutItem.DOUBLE_CLICK.remove(applyLayoutToCurrentPage);
					layoutItem.CLICK.add(itemSelectedHandler);
					layoutItem.destroy();
				}
				allViews = null;	
			}
		}
		
		/**
		 * Double click handler
		 * Replace the current layoutVo of the current edited pageVo if not null
		 * 
		 * Conditions:
		 * 
		 * page layout is not fixed (cards)
		 * if page is not a calendar page (TODO: when dev on calendar, maybe we'll have to change this condition
		 * If page is cover, make sur its a cover custom that uses layout
		 * */
		private function applyLayoutToCurrentPage(layoutItem:LayoutItem):void
		{
			Debug.log("LayoutTab > applyLayoutToCurrentPage");
			
			var test:PageVo = PagesManager.instance.currentEditedPage;
			if(PagesManager.instance.currentEditedPage)
			{
				var page : PageArea = EditionArea.getCurrentPageAreaFromPageVo(PagesManager.instance.currentEditedPage);
				
				// condition to not update layouts by double clicking
				if(!page || page.hasFixedLayout) return;
				if(PagesManager.instance.currentEditedPage.isCalendar) return;
				if(!layoutItem.isCalendar && PagesManager.instance.currentEditedPage.layoutVo.isCalendar) return;
				if(layoutItem.isCalendar && !PagesManager.instance.currentEditedPage.layoutVo.isCalendar) return;
				if(PagesManager.instance.currentEditedPage.isCover && PagesManager.instance.currentEditedPage.layoutId == "")return;
				
				/*PagesManager.instance.currentEditedPage.updateLayout(LayoutManager.instance.getLayoutById(layoutItem.layoutId,PagesManager.instance.currentEditedPage.isCover, layoutItem.isCustomLayout));
				UserActionManager.instance.addAction(UserActionManager.TYPE_PAGE_LAYOUT_UPDATE);*/
				
				applyLayoutToPageVo(layoutItem,PagesManager.instance.currentEditedPage);
				UserActionManager.instance.addAction(UserActionManager.TYPE_PAGE_LAYOUT_UPDATE);
				ProjectManager.instance.remapPhotoVoList();
			}
		}
		
		/**
		 * Apply layout to all pages
		 * Replace the layoutVo of the page pageVo of the project
		 * 
		 * Conditions:
		 * 
 		 * page layout is not fixed (cards)
		 * if page is not a calendar page (TODO: when dev on calendar, maybe we'll have to change this condition
		 * If page is cover, make sur its a cover custom that uses layout
		 * */
		private function applyLayoutToAllPage(layoutItem:LayoutItem):void
		{
			var pageVo:PageVo;
			var pageArea:PageArea;
			
			Debug.log("LayoutsTab > applyLayoutToAllPage");
			
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				pageVo = Infos.project.pageList[i];
				
				if(pageVo.isCalendar) continue;
				if(LayoutManager.instance.isLayoutFixed(pageVo.layoutId)) continue;
				if(!layoutItem.isCalendar && pageVo.layoutVo.isCalendar) continue;
				if(layoutItem.isCalendar && !pageVo.layoutVo.isCalendar) continue;
				if(pageVo.isCover && pageVo.layoutId == "") continue;
				
				Debug.log("LayoutsTab > loop through pages: page index: "+i);
				applyLayoutToPageVo(layoutItem,pageVo);
			}
			
			ProjectManager.PROJECT_UPDATED.dispatch(); //update project signal 
			UserActionManager.instance.addAction(UserActionManager.TYPE_ALL_LAYOUTS_CHANGED); // undo/redo
			ProjectManager.instance.remapPhotoVoList();
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc, false);		
		}
		
		/**
		 * Apply a layout to a pageVo
		 * no coditions checked here
		 * should never be called directly
		 * */
		private function applyLayoutToPageVo(layoutItem:LayoutItem, pageVo:PageVo):void
		{
			
			pageVo.updateLayout(LayoutManager.instance.getLayoutById(layoutItem.layoutId,pageVo.isCover, layoutItem.isCustomLayout));
			
		}
		
		
	}
}