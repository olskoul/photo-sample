package view.menu.lefttabs.common
{
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import be.antho.data.ResourcesManager;
	
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemHeader;
	
	import data.AlbumVo;
	import data.Infos;
	
	import manager.SessionManager;
	
	import mcs.leftmenu.accordion.item.header.simple.view;
	
	import utils.Colors;
	
	
	
	public class EmptyItemHeader extends AccordionItemHeader
	{
		private var resourceKey:String;
		
		public function EmptyItemHeader(resourceKey:String = "")
		{
			super();
			this.resourceKey = resourceKey;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{
			bg.graphics.lineStyle(1,Colors.GREY_LIGHT, 1);
			bg.graphics.lineTo(Accordion.MAX_WIDTH-7,0);
			
			bg.graphics.lineStyle(0,Colors.GREY_LIGHT, 0);
			bg.graphics.beginFill(Colors.WHITE, 1);
			bg.graphics.drawRect(0,1,Accordion.MAX_WIDTH-7,15);
			bg.graphics.endFill();
			addChild(bg);
			setDefaultHitZone(bg);
		}
		
		
	}
}