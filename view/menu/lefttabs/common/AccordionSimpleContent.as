package view.menu.lefttabs.common
{

	
	import flash.display.DisplayObject;
	
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemContent;

	
	/**
	 * 
	 * */
	public class AccordionSimpleContent extends AccordionItemContent
	{
		
		public function AccordionSimpleContent()
		{
			super();
		}
		
		public function init(content:DisplayObject):void
		{
			addChild(content);
			drawBg();
		}
		
		/**
		 * Draw a transparent BG to fix the height (component 101 draw delay issue)
		 * */
		private function drawBg():void
		{
			graphics.beginFill(0x000000,0);
			graphics.drawRect(0,0,Accordion.MAX_WIDTH,height);
			graphics.endFill();
		}
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		override public function destroy():void
		{		
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				var child:DisplayObject = this.getChildAt(0);
				removeChild(child);
				child = null;
			}
			
		}
		
	}
}