package view.menu.lefttabs.backgrounds
{
	import com.bit101.components.HBox;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	
	import be.antho.data.ResourcesManager;
	
	import comp.BackgroundCustomItem;
	import comp.BackgroundItem;
	import comp.ThumbItem;
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	
	import manager.BackgroundsManager;
	import manager.PagesManager;
	
	CONFIG::offline
		{
			import offline.manager.OfflineAssetsManager;
		}
	
	
	import utils.Colors;
	import utils.Debug;
	import data.TooltipVo;
	import comp.AssetItem;
	
	public class BackgroundTabBottomZone extends Sprite
	{
		
		private var currentSelectedBkg:ThumbItem;

		private var preview:Bitmap;

		private var wrapper:HBox;

		private var applyToAllBtn:SimpleButton;

		private var applyBtn:SimpleButton;
		
		private var deleteBtn:SimpleButton;
		
		private var downloadBtn:SimpleButton;

		private var previewContainer:Sprite;
		
		public function BackgroundTabBottomZone()
		{
			super();
			construct();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Setter of the current selected bkg item
		 * Change preview
		 * */
		public function set selectedBkg(item:ThumbItem):void
		{
			currentSelectedBkg = item;
			setPreview();
			
			if(item is BackgroundCustomItem)
				wrapper.addChild(deleteBtn);
			else
			{
				if(deleteBtn.parent)
					deleteBtn.parent.removeChild(deleteBtn) 
			}
			 
			downloadBtn.visible = downloadBtn.enabled = (item is BackgroundItem &&  !item.IS_LOCAL_CONTENT);
			applyToAllBtn.enabled = (!PagesManager.instance.currentEditedPage.isCover);
			
			wrapper.x = preview.width+ 2;
		}
		
		/**
		 * Dispose
		 * */
		
		public function dispose():void
		{
			if(preview.bitmapData)
			preview.bitmapData.dispose();
		}
		
		/**
		 * Destroy
		 * */
		
		public function destroy():void
		{
			dispose();
			if(parent)
				parent.removeChild(this);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Change preview bitmap
		 * Contraint to bounds
		 * */
		private function setPreview():void
		{
			preview.bitmapData = currentSelectedBkg.cloneThumbBitmapdata();
			if(preview.bitmapData != null)
			{
				activate(true);
				preview.height = applyToAllBtn.height;
				preview.scaleX = preview.scaleY;
				
				if(preview.width > 60)
				{
					preview.width = 60;
					preview.scaleY = preview.scaleX;
				}
				
				previewContainer.graphics.clear();
				previewContainer.graphics.lineStyle(2,Colors.GREY);
				previewContainer.graphics.drawRect(0,0,preview.width,preview.height);
				
				previewContainer.alpha = 0;
				TweenMax.killTweensOf(previewContainer);
				TweenMax.to(previewContainer,1,{alpha:1});
				wrapper.draw();
			}
			else
			{
				activate(false);
				Debug.warn("BackgroundApplyToAll.setPreview: preview.bitmapData is null");
			}
			
		}
		
		private function activate( flag:Boolean ):void
		{
			wrapper.mouseChildren = wrapper.mouseEnabled = flag;
		}
		
		
		/**
		 * Build view
		 * */
		private function construct():void
		{
			wrapper = new HBox(this);
			wrapper.spacing = 2;
			wrapper.alignment = HBox.MIDDLE;
			
			previewContainer = new Sprite();
			preview = new Bitmap();
			previewContainer.addChild(preview);
			addChild(previewContainer);
			
			applyBtn = SimpleButton.createApplyButton( "common.apply", applyToBkgToCurrentPage ); 
			applyBtn.margin = 7;
			
			applyToAllBtn = SimpleButton.createApplyToAllButton( "lefttab.backgrounds.applytoall", applyToBkgToAllPages );
			applyToAllBtn.margin = 7;
			
			deleteBtn = new SimpleSquareButton("", 
				Colors.GREY_LIGHT, 
				deleteCurrentBkg, 
				Colors.RED, 
				Colors.WHITE, 
				Colors.RED,
				1,
				false,
				3);
			
			var donwloadTooltip:TooltipVo = new TooltipVo("tooltip.download.assets.locally",TooltipVo.TYPE_SIMPLE,null,Colors.BLUE_CLOUD);
			downloadBtn = new SimpleSquareButton("", 
				Colors.BLUE, 
				downloadItemLocally, 
				Colors.BLUE_CLOUD,
				Colors.WHITE, 
				Colors.RED,
				1,
				false,
				6);
			downloadBtn.tooltip = donwloadTooltip;
			
			wrapper.addChild(deleteBtn);
			wrapper.addChild(downloadBtn);
			wrapper.addChild(applyToAllBtn);
			wrapper.addChild(applyBtn);		
			
		}
		
		/**
		 * Apply bkg to current page
		 * */
		private function applyToBkgToCurrentPage():void
		{
			if(currentSelectedBkg)
			{
				if(currentSelectedBkg is BackgroundCustomItem)
				{
					PagesManager.instance.updateCurrentPageBackgroundWithCustom(currentSelectedBkg.thumbVo as BackgroundCustomVo, PagesManager.instance.currentEditedPage);
				}
				else
				{
					PagesManager.instance.updateCurrentPageBackground( currentSelectedBkg.thumbVo as BackgroundVo );
					CONFIG::offline
					{
						currentSelectedBkg.startDownloadAsset();
					}					
				}
			}			
		}
		
		private function applyToBkgToAllPages():void
		{
			if(currentSelectedBkg)
			{
				if(currentSelectedBkg is BackgroundCustomItem)
				{
					PagesManager.instance.updateAllPageBackgroundWithCustom(currentSelectedBkg.thumbVo as BackgroundCustomVo);
				}
				else
				{
					PagesManager.instance.updateAllPageBackground(currentSelectedBkg.thumbVo as BackgroundVo);
					CONFIG::offline
						{
							currentSelectedBkg.startDownloadAsset();
						}
					
				}
			}
		}
		
		private function deleteCurrentBkg():void
		{
			if(currentSelectedBkg && currentSelectedBkg is BackgroundCustomItem)
			{
				BackgroundsManager.instance.deleteCustomBackground(BackgroundCustomVo(currentSelectedBkg.thumbVo).bkgId);
			}
		}
		
		private function downloadItemLocally():void
		{
			if(currentSelectedBkg && !(currentSelectedBkg is BackgroundCustomItem) && !currentSelectedBkg.IS_LOCAL_CONTENT)
			{
				downloadBtn.enabled = false;
				currentSelectedBkg.startDownloadAsset();
			}
		}
		
	}
}