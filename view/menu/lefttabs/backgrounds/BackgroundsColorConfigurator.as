package view.menu.lefttabs.backgrounds
{
	import com.bit101.components.ColorChooser;
	import com.bit101.components.HBox;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.profiler.showRedrawRegions;
	
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItemContent;
	import comp.button.SimpleButton;
	
	import data.BackgroundVo;
	
	import manager.PagesManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.PrintColorsManager;
	
	
	/**
	 * Backgrounds Color Configurator
	 * Tool to choose a background color
	 * options: Apply to all background in the current project
	 * Add action to undo/redo
	 * */
	public class BackgroundsColorConfigurator extends AccordionItemContent
	{
		private static const LAST_USED_ITEM_WIDTH:Number = 45;
		private static const LAST_USED_ITEM_HEIGHT:Number = 30;
		private static const PALETTE_ITEM_HEIGHT:Number = 10;
		private static const PALETTE_ITEM_WIDTH:Number = 15;
		private static const PALETTE_COLS:Number = 7;
		private const GAP_X: int = 5;
		private const GAP_Y: int = 5;
		
		// old color palette
		private var paletteColorArr:Array = [0xffc0c0,0xffe0c0,0xffe0c0,0xc0ffc0,0xc0ffff,0xc0c0ff,0xffc0ff,0xff8080,0xffc080,0xffff80,0x80ff80,0x80ffff,0x8080ff,0xff80ff,0xff0000,0xff8000,0xffff00,0x00ff00,0x00ffff,0x0000ff,0xff00ff,0xc00000,0xc04000,0xc0c000,0x00c000,0x00c0c0,0x0000c0,0x800080,0x800000,0x804000,0x808000,0x008000,0x008080,0x000080,0x800080,0x400000,0x804040,0x404000,0x004000,0x004040,0x004040,0x400040,0x400040,0x808080, 0x000000,0xffffff];
		
		private var palette:Sprite;
		private var lastUsedColor:Number = 0xffffff;
		private var selectedBorder : Shape ;

		private var lastColorUsedItem:Sprite;
		
		public function BackgroundsColorConfigurator()
		{
			super();
			
			if( !Debug.USE_OLD_COLOR_SYSTEM )
				paletteColorArr = PrintColorsManager.getVisibleColorList();
			 
			construct();
		}
		
		private function construct():void
		{
			drawBg();

			var row:int = 0;
			var initY:Number = 0;
			var posY:Number = initY;
			
			//color palette
			if(!selectedBorder){
				selectedBorder = new Shape();
				selectedBorder.graphics.lineStyle(3,0,1,true);
				selectedBorder.graphics.beginFill(0,0);
				selectedBorder.graphics.drawRect(-1,-1,PALETTE_ITEM_WIDTH+2, PALETTE_ITEM_HEIGHT+2);
				selectedBorder.graphics.endFill();
			}
			
			palette = new Sprite();
			for (var i:int = 0; i < paletteColorArr.length; i++) 
			{
				var coloritem:MovieClip = new MovieClip();
				coloritem.id = paletteColorArr[i];
				
				var g:Graphics = coloritem.graphics;
				g.lineStyle(1,0,1);
				g.beginFill(paletteColorArr[i],1)
				g.drawRect(0,0,PALETTE_ITEM_WIDTH,PALETTE_ITEM_HEIGHT);
				g.endFill();
				
				coloritem.x = row * (coloritem.width+GAP_X);
				coloritem.y = posY;
				row = (row == PALETTE_COLS-1)?0:row+1;
				posY = (row == 0 && i>0)?posY + coloritem.height + GAP_Y:posY;
				
				
				palette.addChild(coloritem);
				
				coloritem.buttonMode = true;
				coloritem.doubleClickEnabled = true;
				coloritem.addEventListener(MouseEvent.DOUBLE_CLICK, coloritemDoubleClickHandler);
				coloritem.addEventListener(MouseEvent.CLICK, coloritemClickHandler);
				
				if(i==paletteColorArr.length-1){
					placeSelectedBorder(coloritem);
				}
			}
			palette.y = 10;
			palette.x = width - palette.width >> 1;
			
			addChild(palette);
			
			//Last used Color
			var applyBox : HBox = new HBox(this);
			applyBox.spacing = 8;
			lastColorUsedItem = new Sprite();
			updateLastColorUsedItem();
			//lastColorUsedItem.x = palette.x;
			//lastColorUsedItem.x = 10;
			lastColorUsedItem.y = palette.y + palette.height + 40;
			applyBox.addChild(lastColorUsedItem)
			
			//Apply to all button
			var applyBtn:SimpleButton = SimpleButton.createApplyButton("lefttab.backgrounds.apply",applyToOne);
			//	applyBtn.x = lastColorUsedItem.x + lastColorUsedItem.width + 10;
			applyBtn.y = lastColorUsedItem.y + (lastColorUsedItem.height - applyBtn.height >>1);
			
			
			var applyToAllBtn:SimpleButton = SimpleButton.createApplyToAllButton("lefttab.backgrounds.applytoall",applyToAll);
			//	applyToAllBtn.x = applyBtn.x + applyBtn.width + 10;
			applyToAllBtn.y = applyBtn.y;
		
			applyBox.addChild(applyToAllBtn);
			applyBox.addChild(applyBtn);
			applyBox.x = (this.width - applyBox.width) /2
		}
		
		/**
		 * Apply color to current background
		 * */
		private function applyToOne():void
		{
			PagesManager.instance.updateCurrentPageBackground(null,lastUsedColor);
		}
		
		/**
		 * Apply the last used Color to all Background
		 * Should Alert a confirmation
		 * */
		private function applyToAll():void
		{
			PagesManager.instance.updateAllPageBackground(null,lastUsedColor);
		}
		
		
		/**
		 * Change color of the Apply to all
		 * */
		private function updateLastColorUsedItem():void
		{
			var g:Graphics = lastColorUsedItem.graphics;
			g.clear();
			g.lineStyle(1,0,1);
			g.beginFill(lastUsedColor,1)
			g.drawRect(0,0,LAST_USED_ITEM_WIDTH,LAST_USED_ITEM_HEIGHT);
			g.endFill();
		}
		
		/**
		 * Draw a transparent BG to fix the height (component 101 draw delay issue)
		 * */
		private function drawBg():void
		{
			graphics.beginFill(0xFF0000,0);
			graphics.drawRect(0,0,Accordion.MAX_WIDTH,150);
			graphics.endFill();
		}
		
		
		/**
		 * A color item from the palette has been clicked
		 * use it as the last ColorUsed
		 * */
		protected function coloritemClickHandler(event:MouseEvent):void
		{
			lastUsedColor = event.target.id;
			updateLastColorUsedItem();
			placeSelectedBorder(event.currentTarget as MovieClip);
		}
		
		/**
		 * A color item from the palette has been double clicked
		 * Let's update the background frame of the current edited page
		 * */
		protected function coloritemDoubleClickHandler(event:MouseEvent):void
		{
			createBackgroundVoAndUpdatePage( event.target.id );	
			coloritemClickHandler(event);
		}
		
		protected function placeSelectedBorder(item:MovieClip):void
		{
			if(!selectedBorder.parent) palette.addChild(selectedBorder);
			selectedBorder.x = item.x;
			selectedBorder.y = item.y;
		}
		
		/**
		 * update the current edited page background Frame
		 * */
		private function createBackgroundVoAndUpdatePage ( color:Number ) : void
		{
			PagesManager.instance.updateCurrentPageBackground(null,color);
		}
		
	}
}