package view.menu.lefttabs.backgrounds
{
	import comp.PhotoItem;
	import comp.ThumbItem;
	import comp.accordion.AccordionItemContent;
	
	public class BackgroundsItemContent extends AccordionItemContent
	{
		public function BackgroundsItemContent()
		{
			super();
		}
		
		
		/**
		 * update list items
		 */
		override public function updateContentVisibility(contentPosY:Number):void
		{
			//var posY: int = this.parent.parent.y + this.parent.y; //parent parent parent..i know this is ugly... when I have time I'll do diferrently
			var posY: int = contentPosY; //parent parent parent..i know this is ugly... when I have time I'll do diferrently
			var thumb: ThumbItem;
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				thumb = this.getChildAt(i) as ThumbItem;
				if(thumb){
					if(posY + thumb.y + thumb.height < 0 || posY + thumb.y > contentVisibilityYLimit) { 
						thumb.outro();
					}
					else {
						thumb.intro();
					}
				}
			}
		}
	}
}