package view.menu.lefttabs.backgrounds
{
	import comp.accordion.AccordionItemHeader;
	
	import data.AlbumVo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import mcs.leftmenu.accordion.item.header.photos.view;
	
	public class BackgroundsItemHeader extends AccordionItemHeader
	{
		public var title:String = "";
		public var total:int;
		
		public function BackgroundsItemHeader()
		{
			super();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		override public function init():void
		{
			super.init();
		}
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible)
		 * 
		 */
		override protected function construct():void
		{
			var view:MovieClip = new mcs.leftmenu.accordion.item.header.photos.view(); // SWC assets
			view.title.text = title;
			view.infos.txt.text = String(total);
			addChild(view);
			
			view.mouseChildren = false;
			setDefaultHitZone(view);
		}
	}
}