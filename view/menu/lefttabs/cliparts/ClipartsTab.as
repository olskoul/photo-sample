package view.menu.lefttabs.cliparts
{
	import com.greensock.TweenMax;
	
	import flash.display.Sprite;
	
	import be.antho.data.ResourcesManager;
	
	import comp.BackgroundItem;
	import comp.ClipartItem;
	import comp.PhotoItem;
	import comp.ThumbItem;
	import comp.accordion.Accordion;
	import comp.accordion.AccordionItem;
	import comp.accordion.AccordionItemContent;
	import comp.accordion.AccordionItemHeader;
	import comp.button.SimpleSquareButton;
	
	import data.AlbumVo;
	import data.BackgroundVo;
	import data.ClipartVo;
	import data.Infos;
	import data.PhotoVo;
	
	import manager.BackgroundsManager;
	import manager.ClipartsManager;
	import manager.DragDropManager;
	import manager.OverlayerManager;
	import manager.PagesManager;
	import manager.SessionManager;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.menu.lefttabs.LeftTabContent;
	import view.menu.lefttabs.backgrounds.BackgroundTabBottomZone;
	import view.menu.lefttabs.common.AccordionSimpleContent;
	import view.menu.lefttabs.common.EmptyItemHeader;
	import view.popup.PopupAbstract;
	
	public class ClipartsTab extends LeftTabContent
	{
		
		private var thumbList : Vector.<ThumbItem>; 
		private var downloadMoreBtn:SimpleSquareButton;
		private var allViews:Vector.<ThumbItem>;
		private var bottomZoneContent:ClipartsTabBottomZone;
		private var tryUpdapteView:int = 0;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function ClipartsTab(xmlData:XML)
		{
			super(xmlData);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		override public function init():void
		{
			super.init();
			
			//Expand/collapse all buttons
			addExpandCollapseBtn();
			
			//create thumbs
			updateView();
			
			//add listeners
			ClipartsManager.CLIPARTS_REFRESH_COMPLETE.add(listRefreshHandler);
		}
		
		/**
		 * Destroy overide
		 */
		public override function destroy():void
		{
			
			//listeners
			ClipartsManager.CLIPARTS_REFRESH_COMPLETE.remove(listRefreshHandler);
			
			//dispose
			dispose();
			
			super.destroy();
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function updateView():void
		{
			//first dispose
			dispose();
			
			// DONWLOAD MORE BACKGROUND
			if(ClipartsManager.instance.showDownloadMoreBtn)
			{
				var emptyHeader : EmptyItemHeader = new EmptyItemHeader();
				emptyHeader.init();
				var downloadBtnWrapper:AccordionSimpleContent = new AccordionSimpleContent();
				
				downloadMoreBtn = new SimpleSquareButton("download.more.clipart", Colors.BLUE, downloadMoreHandler, Colors.BLUE, Colors.WHITE, Colors.WHITE, 1,false,6);
				downloadMoreBtn.forcedWidth = 280;
				downloadMoreBtn.x = 6;
				//downloadMoreBkgBtn.y = 15;
				downloadBtnWrapper.init( downloadMoreBtn );
				
				//Create accordion item
				accordion.addAccordionItem(emptyHeader, downloadBtnWrapper, "donwloadMore", true, true, false);
			}
			
			//create project backgrounds (no categoriszation)
			// create header
			if(ClipartsManager.instance.availableIds)
			{
				
				
				//reset allviews
				allViews = new Vector.<ThumbItem>();
				
				// create content
				var listContent:ClipartItemContent = new ClipartItemContent();
				var item : ClipartItem;
				var vo:ClipartVo;
				var voUsed:Vector.<ClipartVo> = new Vector.<ClipartVo>();
				var views:Vector.<ThumbItem> = new Vector.<ThumbItem>();
				for (var i:int = 0; i < ClipartsManager.instance.availableIds.length; i++) 
				{
					vo =  ClipartsManager.instance.getVoById(ClipartsManager.instance.availableIds[i]);
					
					if(vo != null)
					{
						//voUsed.push(vo);
						item = new ClipartItem(vo);
						item.draggable = true;
						item.dropType = DragDropManager.TYPE_MENU_CLIPART;
						vo.label = ResourcesManager.getString("thumb.label.prefix.clipart") + " " + String(Number(i+1));//vo.name;
						item.addLabel();
						item.CLICK.add(itemSelectedHandler);
						item.DOUBLE_CLICK.add(itemDoubleClickHandler);
						//views.push(item);
						//allViews.push(item);
						
						//If desktop and no internet put it on top of them all
						if(Infos.IS_DESKTOP && vo.isLocalContent && !Infos.INTERNET)
						{
							voUsed.unshift(vo);
							views.unshift(item);
							allViews.unshift(item);
						}
						else					
						{
							voUsed.push(vo);
							views.push(item);
							allViews.push(item);
						}
					}
				}
				
				var listHeader : ClipartsItemHeader = new ClipartsItemHeader();
				listHeader.title = ResourcesManager.getString("lefttab.cliparts.allcliparts");
				listHeader.total = voUsed.length;
				listHeader.init();
				
				//display views as grid
				showAsGrid(views,listContent,Accordion.MAX_WIDTH);	
				// add it to accordion
				accordion.addAccordionItem(listHeader, listContent, listHeader.title, true);
				
				//create  backgrounds by category
				for (var j:int = 0; j <  ClipartsManager.instance.availableCategoriesIds.length; j++) 
				{
					var categoryId:String = ClipartsManager.instance.availableCategoriesIds[j];
					
					// create content
					listContent = new ClipartItemContent();
					views = new Vector.<ThumbItem>();
					for (var k:int = 0; k < voUsed.length; k++) 
					{
						if(voUsed[k].category == categoryId)
						{
							item = new ClipartItem(voUsed[k]);
							item.draggable = true;
							item.dropType = DragDropManager.TYPE_MENU_CLIPART;
							item.addLabel();
							item.CLICK.add(itemSelectedHandler);
							item.DOUBLE_CLICK.add(itemDoubleClickHandler);
							views.push(item);
							allViews.push(item);
						}
					}
					
					// create header
					listHeader = new ClipartsItemHeader();
					listHeader.title = ResourcesManager.getString("lefttab.category."+categoryId);
					listHeader.total = views.length;
					listHeader.init();
					trace("clipart lefttab.category."+categoryId);
					//display views as grid
					showAsGrid(views,listContent,Accordion.MAX_WIDTH);	
					// add it to accordion
					accordion.addAccordionItem(listHeader, listContent, listHeader.title,false);
					
				}
				
				//apply to All 
				bottomZoneContent = new ClipartsTabBottomZone();
				addToBottomZone(bottomZoneContent);
			}
			else
			{
				tryUpdapteView ++;
				if(tryUpdapteView < 4)
					TweenMax.delayedCall(1,updateView);
				else
					Debug.warnAndSendMail("ClipartsTabs > Could not update view after "+tryUpdapteView+" tries");
				return;
			}
			
			// update visible items
			accordion.updateItems();
		}
		
		/**
		 * Click handler
		 * Add preview to applytoall module
		 * */
		private function itemSelectedHandler(item:ThumbItem):void
		{
			bottomZoneContent.selectedBkg = item;
			redrawBottomZoneBkg();
			showBottomZone(true);
			TweenMax.killDelayedCallsTo(showBottomZone);
			TweenMax.delayedCall(5,showBottomZone,[false]);
		}
		
		/**
		 * Start process of donwloading more cliparts
		 * -> download zip package
		 * -> unzip it
		 * -> overwrite the clipart xml
		 * -> Refresh tab
		 * */
		private function downloadMoreHandler():void
		{
			ClipartsManager.instance.downloadMore();
			//PopupAbstract.Alert(ResourcesManager.getString("download.more.clipart"), ResourcesManager.getString("popup.download.more.assets.desc"),true,ClipartsManager.instance.downloadMore,null,null);
		}
		
		/**
		 * Refresh list
		 * */
		private function listRefreshHandler():void
		{
			updateView();
		}	
		
		/**
		 * Double click handler
		 * Replace the current backgroundVo of the current edited pageVo if not null
		 * */
		private function itemDoubleClickHandler(item:ClipartItem):void
		{
			if(PagesManager.instance.currentEditedPage)
			{
				PagesManager.instance.addClipartFrameToCurrentPage( item.thumbVo as ClipartVo );
				item.startDownloadAsset();
			}
		}
		
		
		/**
		 * check if a folder is open in pref panel
		 */
		private function checkIfFolderIsOpen(albumName:String):Boolean
		{
			/*	var folderOpenList : Array = Infos.prefs.folderOpenList; 
			if(!folderOpenList) return false;
			for (var i:int = 0; i < folderOpenList.length; i++) 
			{
			if(albumName == folderOpenList[i]) return true;	
			}
			return false;*/
			return false;
		}	
		
	
		
		
		/**
		 *
		 */
		private function dispose():void
		{
			//Check donwload more bkg btn visibility
			if(downloadMoreBtn)
			{
				if(downloadMoreBtn.parent)
					downloadMoreBtn.parent.removeChild(downloadMoreBtn);
				downloadMoreBtn = null;
			}
			
			//destroy bottom zone content
			if(bottomZoneContent) bottomZoneContent.destroy();
			
			// dispose accordion
			if(accordion)accordion.dispose();
			
			if(allViews){
				var item : *;
				for (var i:int = 0; i < allViews.length; i++) 
				{
					item = allViews[i];
					item.DOUBLE_CLICK.add(itemDoubleClickHandler);
					item.destroy();
				}
				allViews = null;	
			}
		}
		
		
		/**
		 *
		 */
		private function onImageListUpdated():void
		{
			dispose();
			updateView();
		}
		
		
	}
}