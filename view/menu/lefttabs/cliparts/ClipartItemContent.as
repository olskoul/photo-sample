package view.menu.lefttabs.cliparts
{
	import comp.PhotoItem;
	import comp.ThumbItem;
	import comp.accordion.AccordionItemContent;
	
	public class ClipartItemContent extends AccordionItemContent
	{
		public function ClipartItemContent()
		{
			super();
		}
		
		
		/**
		 * update list items
		 */
		override public function updateContentVisibility(contentPosY:Number):void
		{
			//var posY: int = this.parent.parent.y + this.parent.y; //parent parent parent..i know this is ugly... when I have time I'll do diferrently
			var posY: int = contentPosY; //parent parent parent..i know this is ugly... when I have time I'll do diferrently
			var thumb: ThumbItem;
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				thumb = this.getChildAt(i) as ThumbItem;
				if(posY + thumb.y + thumb.height < 0 || posY + thumb.y > contentVisibilityYLimit) { 
					thumb.outro();
				}
				else {
					thumb.intro();
				}
			}
		}
	}
}