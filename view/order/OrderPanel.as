/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package view.order
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.AutoAlphaPlugin;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	
	import mx.effects.Tween;
	import mx.resources.ResourceManager;
	
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.utils.FormValidator;
	
	import comp.DefaultTooltip;
	import comp.FormInput;
	import comp.PanelAbstract;
	import comp.button.SimpleButton;
	
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.ProductsCatalogue;
	import data.ProjectVo;
	import data.TooltipVo;
	
	import manager.CanvasManager;
	import manager.CardsManager;
	import manager.CoverManager;
	import manager.MeasureManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import mcs.common.inputField;
	import mcs.order.orderPanel;
	
	import ordering.OrderManager;
	
	import service.ServiceManager;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.PageCoverClassicArea;
	import view.popup.PopupAbstract;
	import view.popup.PopupEmailInput;
	import view.popup.PopupSimpleInput;
	import view.uploadManager.PhotoUploadManager;

	public class OrderPanel extends PanelAbstract
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var view:orderPanel = new orderPanel();
		
		private var detailTitle : TextField;
		private var detailDescription : TextField;
		private var detailTotal : TextField;
		
		private var promoTitle : TextField;
		private var promoDescription : TextField;
		private var promoInput : FormInput;
		
		private var bg : MovieClip;
		private var detailBg : MovieClip;
		private var promoBg : MovieClip;
		private var closeBtn:Sprite;
		
		private var promoButton : SimpleButton;
		private var okButton : SimpleButton;
		private var cancelButton : SimpleButton;
		
		private var skipEmptyPagesWarning:Boolean = false; 	// flag to skip order check on empty pages
		private var skipQualityWarning:Boolean = false;		// flag to skip order check on frames quality
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		
		public function OrderPanel() 
		{ 
			visible = false;
			addChild(view);
			
			// map
			detailTitle = view.detailTitle;
			detailDescription = view.detailDescription;
			detailTotal = view.detailTotal;
			promoTitle = view.promoTitle;
			promoDescription = view.promoDescription;
			
			promoInput = new FormInput(view.promoInput, "order.panel.promo.watermark", false, 20);
				
			// remove mousewheel and selected
			detailTitle.selectable = detailTitle.mouseEnabled = 
				detailDescription.selectable = detailDescription.mouseEnabled =
				detailTotal.selectable = detailTotal.mouseEnabled =
				promoTitle.selectable = promoTitle.mouseEnabled =			
				promoDescription.selectable = promoDescription.mouseWheelEnabled = promoDescription.mouseEnabled = false;
				
			
			bg = view.bg;
			promoBg = view.promoBg;
			detailBg = view.detailBg;
			closeBtn = view.closeBtn;
			
			
			// buttons action
			okButton = SimpleButton.createPopupOkButton("order.panel.ok",onOkClick);
			promoButton = SimpleButton.createWhiteButton("order.panel.promo.apply",onPromoClick);
			cancelButton = SimpleButton.createPopupCancelButton("order.panel.cancel",onCancelClick);
			ButtonUtils.makeButton(closeBtn, function(e:Event):void{close()});
			
			okButton.forcedWidth = 120;
			cancelButton.forcedWidth = 120;
			promoButton.forcedWidth = 93;
			replaceButton(view.okButton, okButton, true);
			replaceButton(view.cancelButton, cancelButton,true);
			replaceButton(view.promoButton, promoButton,true);
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * open at precise point
		 */
		public override function open( $pos : Point ):void
		{
			$pos.x -= 219;
			updateView();
			
			super.open($pos);
		}
		
		
		
		/**
		 *
		 */
		public override function close(e:Event = null):void
		{
			killPromoTooltip();
			super.close();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * update elements in view before show
		 */
		private function updateView():void
		{
			
			// TODO : make switch here depending on project type (album, calendar, etc)
			
			// detail
			detailTitle.autoSize = "left";
			detailDescription.autoSize ="left";
			ResourcesManager.setText(detailTitle, "order.panel.detail.title");
			var detailString: String ="";
			
			// ALBUM DETAIL
			if( Infos.isAlbum ){
				
				var typeId:String;
				if(Infos.project.type == ProductsCatalogue.ALBUM_CLASSIC)
				{
					if(CoverManager.instance.getCoverVo().coverType == ProductsCatalogue.COVER_CUSTOM)
						typeId = ProductsCatalogue.ALBUM_CONTEMPORARY;
					else
						typeId = ProductsCatalogue.ALBUM_CLASSIC;
				}
				else
					typeId = Infos.project.type;
				
				
				detailString += ResourcesManager.getString("album.type."+typeId) + " " +ResourcesManager.getString("album.prefix."+Infos.project.docPrefix);
				detailString += ": " +  (Infos.project.pageList.length-1) +" "+ ResourcesManager.getString("common.pages");
				detailString += "\n" + ResourcesManager.getString("order.panel.detail.paper") + ": " + getYesNoValue(Infos.project.paper);
				detailString += "\n" + ResourcesManager.getString("order.panel.detail.cover") + ": " + CoverManager.instance.getCoverLabelName();
				
				if(CoverManager.instance.getCorner() != "" && CoverManager.instance.getCorner() != "none")
					detailString += "\n" + ResourcesManager.getString("order.panel.detail.corners") + ": " + CoverManager.instance.getCornerLabel();
				
				if(typeId == ProductsCatalogue.ALBUM_CLASSIC)
				detailString += "\n" + ResourcesManager.getString("order.panel.detail.coverText") + ": " + getYesNoValue(ProjectManager.instance.coverText);
				detailString += "\n" + ResourcesManager.getString("leftab.project.album.paper.quality.label") + ": " + ResourcesManager.getString("leftab.project.album.paper.quality."+Infos.project.pagePaperQuality);
				detailString += "\n" + ResourcesManager.getString("lefttab.project.album.coated") + ": " + getYesNoValue(Infos.project.coated);
				detailString += "\n" + ResourcesManager.getString("order.panel.has.flyleaf") + ": " + ResourcesManager.getString("album.flyleaf.label."+Infos.project.flyleafColor);//getYesNoValue((Infos.project.flyleafColor != "white"));
			}
			// CALENDAR
			else if( Infos.isCalendar ){
				detailString += ResourcesManager.getString("calendar.prefix."+Infos.project.docPrefix);
				detailString += "\n" +  (Infos.project.pageList.length-1) +" "+ ResourcesManager.getString("common.pages");
				//detailString += "\n" + ResourcesManager.getString("leftab.project.album.paper.quality.label") + ": " + ResourcesManager.getString("leftab.project.album.paper.quality."+Infos.project.pagePaperQuality);
				detailString += "\n" + ResourcesManager.getString("lefttab.project.album.coated") + ": " + getYesNoValue(Infos.project.coated);
			}
				// CANVAS
			else if( Infos.isCanvas ){
				detailString += ResourcesManager.getString("canvas.type."+Infos.project.canvasType);
				detailString += "\n" +  ResourcesManager.getString("order.panel.detail.format") + ": " + ResourcesManager.getString("canvas.orientation."+Infos.project.type) ;
				var w : Number = Math.round(MeasureManager.PFLToCM(Infos.project.width));
				var h : Number = Math.round(MeasureManager.PFLToCM(Infos.project.height));
				detailString += "\n" + w + "cm X " + h + "cm";
				if(CanvasManager.instance.isMultiple) detailString += "\n(" + Infos.project.canvasMLayout + ")";
				
			}
			
			// CARDS
			else if( Infos.isCards ){
				var orientation : String = (Infos.project.type != "types_fc_port_sq" && Infos.project.type != "types_announcecard_sq")? "\n"+ResourcesManager.getString("cards.orientation."+CardsManager.instance.docOrientation) : "";
				var model : String = CardsManager.instance.cardsModel;
				detailString += ResourcesManager.getString("cards.type."+Infos.project.type) + " " + orientation;
				detailString += "\n" + ResourcesManager.getString("cards.configurator.model.label") + " : " + model;
				detailString += "\n" + ResourcesManager.getString("cards.configurator.envelope.label") + " : " + ResourcesManager.getString("cards.configurator.envelope."+Infos.project.envelope) ;
				
			}

			
			// inject string to description
			ResourcesManager.setTextLiteral(detailDescription, detailString);	
			
			
			var totalString : String = ResourcesManager.getString("order.panel.detail.totalPrice") + " : " + Infos.project.price.toFixed(2) + " €";
			ResourcesManager.setTextLiteral(detailTotal, totalString);
			
			// promo
			promoTitle.autoSize = "left";
			promoDescription.autoSize ="left";
			ResourcesManager.setText(promoTitle, "order.panel.promo.title");
			ResourcesManager.setText(promoDescription, "order.panel.promo.description");
			
			// buttons
			
			// bg
			detailDescription.y = detailTitle.y + detailTitle.height + 10;
			detailTotal.y = detailDescription.y + detailDescription.height + 10;
			detailBg.height = detailTotal.y + detailTotal.textHeight + 10 - detailBg.y;
			
			promoBg.y = detailBg.y + detailBg.height + 10;
			promoTitle.y = promoBg.y + 10;
			promoDescription.y = promoTitle.y + promoTitle.height + 10;
			promoInput.y = promoDescription.y + promoDescription.height + 10;
			promoButton.y = promoInput.y;
			promoBg.height = promoInput.y + promoInput.height + 15 - promoBg.y;
			
			okButton.y = promoBg.y + promoBg.height + 15;
			cancelButton.y = okButton.y
				
			bg.height = okButton.y + okButton.height +30;
			
			// if promo code is active
			if(Infos.project.promoCode) promoInput.text = Infos.project.promoCode;
			
		}
		
			
		
		/**
		 * Yes/No helper
		 */
		private function getYesNoValue(b:Boolean):String
		{
			if(b) return ResourcesManager.getString("common.yes");
			else return ResourcesManager.getString("common.no");
		}
		
		
		/**
		 * click on ok button
		 */
		private function onOkClick( e:MouseEvent = null ):void
		{
			skipEmptyPagesWarning = false;
			skipQualityWarning = false;
			checkOrderConditions();	
		}
		
		
		/**
		 * to place order we need to fill multiple conditions : 
		 * > project must not have uploading files
		 * > project must exist server side
		 * > project must have all pages filled (or the user accept warnings)
		 * > project must have all frames with good print quality (or the user accept warnings)
		 */
		private function checkOrderConditions(p:PopupAbstract = null):void
		{
			var title : String;
			var desc : String;
			
			
			// if photos are uploading, abord process
			if ( PhotoUploadManager.instance.numUploadLeft > 0 ){
				title = ResourcesManager.getString("popup.order.warn.upload.title");
				desc = ResourcesManager.getString("popup.order.warn.upload.description");
				PopupAbstract.Alert(title, desc, false);
				return ;
			}
			
			
			// Save project first !
			if ((!Infos.project.id || ProjectManager.instance.needSave) && !Debug.DO_NOT_ALLOW_SAVE){
				title = ResourcesManager.getString("popup.order.warn.saveproject.title");
				desc = ResourcesManager.getString("popup.order.warn.saveproject.description");
				PopupAbstract.Alert(title, desc, true, function(p:PopupAbstract):void{
					ProjectManager.PROJECT_SAVE_COMPLETE.addOnce( checkOrderConditions );
					ProjectManager.instance.saveProject();
				})
			}
				
			// check for empty pages 
			else if( !skipEmptyPagesWarning && ProjectManager.instance.hasEmptyPages) {
				title = ResourcesManager.getString("popup.order.warn.emptyPages.title");
				desc = ResourcesManager.getString("popup.order.warn.emptyPages.description");
				PopupAbstract.Alert(title, desc, true, function(p:PopupAbstract):void{
					skipEmptyPagesWarning = true;
					checkOrderConditions();
				});
			}
				
			// check for poor quality warnings
			else if( !skipQualityWarning && ProjectManager.instance.hasPrintWarning) {
				title = ResourcesManager.getString("popup.order.warn.redWarning.title");
				desc = ResourcesManager.getString("popup.order.warn.redWarning.description");
				PopupAbstract.Alert(title, desc, true, function(p:PopupAbstract):void{
					skipQualityWarning = true;
					checkOrderConditions();
				});
			}
				
			// ELSE OK
			else startOrderProcess();
		}
		
		
		/**
		 * start order process after order condition check
		 */
		private function startOrderProcess( p:PopupAbstract = null ):void
		{
			//var hasPrintWarnings : Boolean = (p != null)? true : false ;
			//if(Infos.IS_DESKTOP && !Debug.ORDER_TO_PDF){
			if(Infos.IS_DESKTOP){
				var emailPopup:PopupEmailInput= new PopupEmailInput(ResourcesManager.getString("popup.order.email.watermark"),"popup.order.email.title","popup.order.email.description");
				emailPopup.open();
				emailPopup.OK.add(popupEmailOkHandler);
			}
			else OrderManager.instance.start( ProjectManager.instance.hasPrintWarning );
		}
		
		/**
		 * OFFLINE : when user has completed email form
		 */
		private function popupEmailOkHandler( p : PopupEmailInput ):void
		{
			SessionManager.instance.session.email = "" + p.fieldValue;
			Debug.log("OrderPanel.PopupEmailOkHandler : "+SessionManager.instance.session.email);
			// save it in cookie
			SharedObjectManager.instance.write("userEmail", SessionManager.instance.session.email);
			OrderManager.instance.start( ProjectManager.instance.hasPrintWarning );
		}
		
		
		/**
		 * click on cancel button
		 */
		private function onCancelClick(e:MouseEvent = null ):void
		{
			close();
		}
		
		/**
		 * click on promo button
		 */
		private function onPromoClick(e:MouseEvent = null ):void
		{
			Infos.project.promoReduction = 0;
			Infos.project.promoIsPercent = false;
			Infos.project.promoCode = null;
			promoInput.showError(false);
			killPromoTooltip();
			
			// check infos in form
			var result:String = FormValidator.isFieldValid(promoInput.text, 4, promoInput.waterMark);
			if( result != FormValidator.FIELD_VALID){
				promoInput.showError();
				showInputTooltip(ResourcesManager.getString("form.error."+result),promoInput);
				ProjectManager.PRICE_UPDATED.dispatch();
				updateView();
				return;
			}
			
			ServiceManager.instance.verifyPromoCode( promoInput.input.text, onPromoCodeSuccess );
		}
		private function onPromoCodeSuccess( result : Object ):void
		{
			// TEST PROMO CODE FOR REDUCTION OF 10€ : TEST52AMEM
			// TEST PROMO CODE FOR REDUCTION OF 35€ : NSPUTPNXJE
			//
			
			// TODO : put this condition in service manager for global service error handling
			if(result.substr(0,1) == "*")
			{
				PopupAbstract.Alert(ResourcesManager.getString("error.warning"), ""+result); 
				return; 
			}
			
			// check content
			var tempXML:XML = new XML(result);
			
			tempXML.normalize();
			var str:String = tempXML.response;
			var check:Number = Number(tempXML.check);
			if(check == 0) {
				promoInput.showError();
				showInputTooltip(ResourcesManager.getString("order.panel.promo.wrongCode"),promoInput);
			}
			else {
				var val:String = tempXML.value;
				var promoIsPercent : Boolean = (val.indexOf("%") != -1)? true : false;
				var reduction : Number = parseFloat(val);
				//if(!isNaN(reduction) && reduction > 0) {
				if(!isNaN(reduction) && reduction > 0) {
					Infos.project.promoReduction = reduction;
					Infos.project.promoIsPercent = promoIsPercent;
					Infos.project.promoCode = promoInput.input.text;
					showInputTooltip(ResourcesManager.getString("order.panel.promo.goodCode"),promoInput,false);					
				}else{
					// it looks like some voucher give a reduction of 0..  TODO : maybe we should display a wrongCode tooltip..
					Debug.warn("OrderPanel > onPromoCodeSuccess : this should never happen : reduction is "+reduction);
				}
			}
			updateView();
			
			ProjectManager.PRICE_UPDATED.dispatch();
		}
		private function showInputTooltip(message:String, displayObject:DisplayObject, isError:Boolean = true):void
		{
			var tooltipColor : Number = ( isError ) ? Colors.RED : Colors.GREEN;
			var tooltipVo:TooltipVo = new TooltipVo(message,TooltipVo.TYPE_SIMPLE,null,tooltipColor, Colors.WHITE, TooltipVo.ARROW_TYPE_VERTICAL);
			tooltipVo.labelIsNotKey = true;
			DefaultTooltip.tooltipOnClip(displayObject , tooltipVo, true);
			TweenMax.delayedCall(5,killPromoTooltip);
		}
		private function killPromoTooltip():void
		{
			TweenMax.killDelayedCallsTo(killPromoTooltip);
			DefaultTooltip.killAllTooltips();	
		}
		
		
		
		
		
	}
}