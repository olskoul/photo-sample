﻿p/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/
package view 
{
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	
	import data.Infos;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.osflash.signals.Signal;
	
	import comp.ScreenAnimation;

	public class AbstractScreen extends Sprite
	{
		// signals
		public var OUTRO_FINISHED:Signal = new Signal(Sprite);
		
		// properties
		public var initialized:Boolean = false;
		public var params:Object;
		protected var screenBitmap:Bitmap;
		protected var screenLang:String;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function AbstractScreen() 
		{
			this.visible = false;
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * init view
		 * this is called the first time the view is added to display list (called by the screencontroller)
		 */
		public function init():void
		{}
		
		/**
		 * set view enabled
		 */
		public function activate (flag:Boolean = true) :void
		{
			this.mouseChildren = this.mouseEnabled = flag;
		}
		
		/**
		 * view intro
		 * should be overriden
		 */
		public function intro():void
		{
			visible = false;
			// animation
			ScreenAnimation.instance.animate(this, ScreenAnimation.INTRO_TILE, 1, introCompleted);
			
			// block app mouse event
			App.blockMouse();	
		}
		
		
		/**
		 * view outro
		 */
		public function outro():void
		{
			// animation
			ScreenAnimation.instance.animate(this, ScreenAnimation.OUTRO_FADE, 1, destroy);
			
			// block app mouse event
			App.blockMouse();			
		}
		
		
		/**
		 * destroy view
		 */
		public function destroy():void
		{
			OUTRO_FINISHED.dispatch(this);
			TweenMax.killTweensOf(this);
			
			if(parent) this.parent.removeChild(this);
			
			App.blockMouse(false);
		}
		
		
		/**
		 * update lang resources
		 */
		public function updateResources():void
		{
			if(Infos.lang == screenLang) return;
			screenLang = Infos.lang;
		}
		
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		protected function introCompleted():void
		{
			App.blockMouse(false);
		}
		
		
	}
	
}