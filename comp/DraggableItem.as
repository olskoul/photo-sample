package comp
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import manager.DragDropManager;
	
	public class DraggableItem extends DropTargetArea
	{
		// the type of drag&drop associated
		public var dropType : String;
		public var distanceCurve : Boolean;
		private  var _dragEnabled : Boolean;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function DraggableItem( dragDropType : String, drawDistanceCurve : Boolean = true)
		{
			dropType = dragDropType;
			distanceCurve = drawDistanceCurve;
			super();
		}
		
		/**
		 * enable drag or not
		 */
		public function get dragEnabled():Boolean
		{
			return _dragEnabled;
		}
		
		/**
		 * enable drag or not
		 */
		public function set dragEnabled( value:Boolean ):void
		{
			removeEventListener( MouseEvent.MOUSE_DOWN, mouseDownHandler );
			if( value ) addEventListener( MouseEvent.MOUSE_DOWN, mouseDownHandler );
			
			_dragEnabled = value;
		}
		
		
		/**
		 * start dragging the item
		 * 
		 */
		protected function mouseDownHandler(e:MouseEvent):void
		{
			_isDragging = true;
			DragDropManager.instance.startDrag( this );
		}
		
		
		/**
		 *
		 */
		private var _isDragging:Boolean = false;
		public function get isDragging():Boolean
		{
			return _isDragging;
		}
		
		
		/**
		 * destroy
		 * 
		 */
		public function destroy():void
		{
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		public function mouseUpHandler(e:MouseEvent = null):void
		{
			_isDragging = false;
		}
	}
}