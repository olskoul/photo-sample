/***************************************************************
 PROJECT 		: Collect game
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp
{
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import utils.AssetUtils;

	public class ScreenAnimation 
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// types
		public static var INTRO_FADE : String = "INTRO_FADE";
		public static var INTRO_TILE : String = "INTRO_TILE";
		public static var INTRO_FLASH : String = "INTRO_FLASH";
		public static var INTRO_ZOOM : String = "INTRO_ZOOM";
		
		
		public static var OUTRO_NONE : String = "OUTRO_NONE";
		public static var OUTRO_FADE : String = "OUTRO_FADE";
		public static var OUTRO_TILE : String = "OUTRO_TILE";
		public static var OUTRO_ZOOM : String = "OUTRO_ZOOM";
		
		public static var INTRO_FROM_LEFT : String = "INTRO_FROM_LEFT";
		public static var INTRO_FROM_RIGHT : String = "INTRO_FROM_RIGHT";
		public static var OUTRO_TO_LEFT : String = "OUTRO_TO_LEFT";
		public static var OUTRO_TO_RIGHT : String = "OUTRO_TO_RIGHT";
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function ScreenAnimation(sf:SingletonEnforcer) { if(!sf) throw new Error("ScreenAnimation is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:ScreenAnimation;
		public static function get instance():ScreenAnimation
		{
			if (!_instance) _instance = new ScreenAnimation(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public function animate(screen:Sprite, type:String, duration:Number, onCompleteFunction:Function = null, delay:Number = 0):void
		{
			var anim : AnimationObject = new AnimationObject();
			anim.screen = screen;
			anim.name = screen.name;
			anim.type = type;
			anim.duration = duration;
			anim.parent = screen.parent;
			anim.delay = delay;
			anim.onComplete = onCompleteFunction;
			
			// create bitmap clone
			var bmd : BitmapData = new BitmapData(App.WIDTH, App.HEIGHT, true, 0x000000);
			bmd.draw(screen);
			var clone : Bitmap = new Bitmap(bmd,"auto", true)//AssetUtils.drawClipBitmap(screen);
			anim.clone = clone;
			
			// switch types
			switch ( type )
			{
				case OUTRO_NONE : 
					switchClipWithBitmap(anim);
					TweenMax.delayedCall(duration, animComplete, [anim]);
					break;
				
				case OUTRO_FADE : 
					switchClipWithBitmap(anim);
					TweenMax.to(clone, duration, {delay:delay, autoAlpha:0, ease:Strong.easeOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case INTRO_FADE : 
					switchClipWithBitmap(anim);
					clone.alpha = 0;
					TweenMax.to(clone, duration, {delay:delay, autoAlpha:1, ease:Strong.easeOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case INTRO_ZOOM : 
					switchClipWithBitmap(anim);
					clone.alpha = 0;
					clone.scaleX = clone.scaleY = .3;
					clone.x = screen.x + (App.WIDTH - clone.width)*.5
					clone.y = screen.y + (App.HEIGHT - clone.height)*.5
					TweenMax.to(clone, duration, {delay:delay, autoAlpha:1, transformMatrix:{scale:1, x:screen.x, y:screen.y}, ease:Strong.easeOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case OUTRO_ZOOM : 
					switchClipWithBitmap(anim);
					clone.alpha = 0;
					var newScale :Number = .4;
					var newX :Number = screen.x + (App.WIDTH - clone.width*newScale)*.5;
					var newY :Number = screen.y + (App.HEIGHT - clone.height*newScale)*.5;
					TweenMax.to(clone, duration, {delay:delay, autoAlpha:0, tranformMatrix:{scale:newScale, x:newX, y:newY}, ease:Strong.easeOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case INTRO_FROM_LEFT : 
					switchClipWithBitmap(anim);
					clone.x = -App.WIDTH;
					TweenMax.to(clone, duration, {delay:delay, x:0, ease:Strong.easeInOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case INTRO_FROM_RIGHT : 
					switchClipWithBitmap(anim);
					clone.x = App.WIDTH;
					TweenMax.to(clone, duration, {delay:delay, x:0, ease:Strong.easeInOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case OUTRO_TO_LEFT : 
					switchClipWithBitmap(anim);
					TweenMax.to(clone, duration, {delay:delay, x:-App.WIDTH, ease:Strong.easeInOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case OUTRO_TO_RIGHT : 
					switchClipWithBitmap(anim);
					TweenMax.to(clone, duration, {delay:delay, x:App.WIDTH, ease:Strong.easeInOut, onComplete:animComplete, onCompleteParams:[anim]});
					break;
				
				case INTRO_TILE : 
					createTiles(anim);
					break;
				
				case OUTRO_TILE : 
					createTiles(anim);
					break;
			}
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * switch a clip by it's bitmap
		 */
		private function animComplete(anim:AnimationObject):void
		{
			switch (anim.type){
				
				case OUTRO_TILE :
					replaceScreenTiles(anim);
					break;
				
				case INTRO_TILE :
					replaceScreenTiles(anim);
					break;
				
				default : 
					replaceScreen(anim);
					break;
			}
			
			if(anim.onComplete != null) anim.onComplete.call();
		}
		
		
		
		/**
		 * switch a clip by it's bitmap
		 */
		private function switchClipWithBitmap(anim:AnimationObject):void
		{
			anim.clone.x = anim.screen.x;
			anim.clone.y = anim.screen.y;
			
			anim.parent.addChildAt(anim.clone, anim.parent.getChildIndex(anim.screen));
			//anim.parent.removeChild(anim.screen);
			anim.screen.visible = false;
		}
		
		/**
		 * replace screen at right position
		 * 
		 */
		private function replaceScreen(anim:AnimationObject):void
		{
			//anim.parent.addChildAt(anim.screen, anim.parent.getChildIndex(anim.clone));
			anim.parent.removeChild(anim.clone);	
			anim.screen.visible = true;
		}
		
		/**
		 * replace screen at right position
		 */
		private function replaceScreenTiles(anim:AnimationObject):void
		{
			anim.parent.addChildAt(anim.screen, anim.parent.getChildIndex(anim.tilesContainer));
			anim.parent.removeChild(anim.tilesContainer);
			anim.screen.visible = true;
		}
		
		
		
		/**
		 * create a bitmp of the current screen state
		 */
		private function createTiles(anim:AnimationObject):void
		{
			var tilesContainer:MovieClip = new MovieClip();
			tilesContainer.x = anim.screen.x;
			tilesContainer.y = anim.screen.y;
			
			anim.tilesContainer = tilesContainer;
			
			// params
			var cols:uint = 1;
			var rows:uint = 5;
			var numTiles:uint = cols*rows;
			var tileW : uint = App.WIDTH/cols;
			var tileH : uint = App.HEIGHT/rows;
			var hspace:uint = 0;
			var vspace:uint = 0;
			
			// timeline
			var tl:TimelineMax = new TimelineMax({delay:anim.delay, onComplete:animComplete, onCompleteParams:[anim]});
			var screenBd : BitmapData = anim.clone.bitmapData;
			
			// build grid
			var newTile : Sprite;
			var bd:BitmapData;
			var bp:Bitmap;
			var sourceRect : Rectangle;
			for (var i:int = 0; i< numTiles; i++)
			{
				newTile = new Sprite();
				var currentCol:uint = i % cols;
				var currentRow:uint = uint(i / cols);
				
				// bitmapdata
				bd = new BitmapData(tileW, tileH,true, 0x000000);
				sourceRect = new Rectangle(currentCol*tileW +1, currentRow*tileH, tileW, tileH);
				bd.copyPixels(screenBd, sourceRect,new Point());
				bp = new Bitmap(bd);
				bp.x = - Math.ceil(tileW*.5);
				bp.y = - Math.ceil(tileH*.5);
				newTile.addChild(bp);
				
				
				//positioning formula
				newTile.x = newTile.width*.5 + currentCol * (newTile.width + hspace)+1;
				newTile.y = newTile.height*.5 + currentRow * (newTile.height + vspace);
				

				switch (anim.type)
				{
					case OUTRO_TILE :
						TweenMax.to(newTile,0,{scale:1, tint:null});
						tl.insert( TweenMax.to( newTile, .3, {scale:0, alpha:0, tint:null, delay:i * .1}) );
						break;
					
					case INTRO_TILE :
						newTile.alpha = 0;
						TweenMax.to(newTile,0,{scale:1, x:"+500", tint:0xffffff});
						tl.insert( TweenMax.to( newTile, .7, {scale:1, alpha:1, x:"-500", tint:null, delay:i * .1, ease:Strong.easeOut}) );
						break;
				}
				
				
				//TWEEN STYLE FORMULAS: choose ONLY one
				//column by column
				//tl.insert( TweenMax.to( newTile, .5, {scale:1, alpha:1, tint:null, rotation:0, delay:currentCol * .1}) );
				
				//row by row
				//tl.insert( TweenMax.to( newTile, .5, { scale:1, alpha:1, delay:currentRow * .1 }) );
				
				//1 by 1 horizontal
				//tl.insert( TweenMax.to( newTile, .5, { scale:1, alpha:1, delay:i*.02}) );
				
				//1 by 1 vertical
				//tl.insert( TweenMax.to( newTile, .5, { scale:1, alpha:1, delay:((rows * currentCol) + currentRow) *.02 }) );
				
				//circular
				//tl.insert( TweenMax.to( newTile, .5, { scale:1, alpha:1, delay:(newTile.y * newTile.x)*.00001 }) );
				
				//diagonal top left to bottom right
				//tl.insert( TweenMax.to( newTile, .5, { scale:1, alpha:1, delay: (currentCol + currentRow)  *  .05  }) );
				
				
				//diagonal bottom right to top left
				
				//tl.insert(TweenMax.to(newTile, .5, {scale:1, alpha:1, delay: ((numTiles/cols + cols -2)- (currentCol + currentRow))  *  .05  }) );
				
				
				//END TWEEN STYLES
				tilesContainer.addChild(newTile);
			}
			
			anim.parent.addChildAt(tilesContainer, anim.parent.getChildIndex(anim.screen));
			anim.parent.removeChild(anim.screen);
			tl.play();
			
			
			//tl.reversed = ! tl.reversed;
			//tl.reversed = false;
			//tl.resume();
			//tl.reverse);
			
		}
	}
}


import flash.display.Bitmap;
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.display.Sprite;

class AnimationObject
{
	public var screen : Sprite;
	public var name : String;
	public var parent : DisplayObjectContainer;
	public var duration : Number;
	public var delay : Number;
	public var type : String;
	public var clone : Bitmap;
	public var onComplete : Function;
	public var tilesContainer : MovieClip;
}

class SingletonEnforcer{}