package comp
{
	import com.greensock.TweenMax;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.TextEvent;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	import be.antho.form.Watermark;
	
	import data.TooltipVo;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Filters;
	import utils.TextFormats;
	
	import view.popup.PopupAbstract;

	/**
	 * ...
	 * @author Anthony De Brackeleire
	 */
	public class FormInput extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////	
		
		public var TEXT_UPDATE : Signal = new Signal();
		
		public var content:MovieClip;
		public var input:TextField;
		private var bg:MovieClip;
		private var border:MovieClip;
		private var wt:Watermark;
		
		private var _text : String;
		
		public var isEmpty:Boolean = true;
		public var isPassword : Boolean = false;
		private var formWidth : Number = 400;
		private var _watermark:String ;
		private var _useWatermark:Boolean ;
		
		public var errorMessage:String = "";
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////	
		public function FormInput(v:MovieClip, _label:String = "", _isPassword:Boolean = false, maxChars:int = 50, _width:Number = NaN, restrict:String = null, useWatermark:Boolean = true ) 
		{
			isPassword = _isPassword;
			content = v;
			_useWatermark = useWatermark;
			_watermark = ResourcesManager.getString(_label);
			//trace("FormInput > _watermark = "+_watermark);
			// replace content clip by this component
			x = v.x;
			y = v.y;
			if(v.parent){
				v.parent.addChildAt(this, v.parent.getChildIndex(v));
				v.x = 0;
				v.y = 0;
			}
			addChild(content);
			
			// set references
			input = content.input;
			bg = content.bg;
			border = content.border;
			
			// set listeners
			input.addEventListener(Event.CHANGE, onUpdateText);
			input.addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
			input.addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
			
			
			input.displayAsPassword = isPassword;
			input.setTextFormat(TextFormats.ASAP_BOLD(15,Colors.BLUE));
			input.backgroundColor = Colors.WHITE;
			input.borderColor = Colors.GREY_LIGHT;
			input.filters = [Filters.INPUT_INNER_SHADOW];
			
			//
			if(_useWatermark)
			wt = new Watermark(input, _watermark,isPassword);
			else
			input.text = _label;
			input.maxChars = maxChars;
			if(restrict) input.restrict = restrict;
			
		}
		
				
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////		
		
		public function set text(value:String):void
		{
			if (input)
			{
				input.text = value;
				input.displayAsPassword = isPassword;
			}
			
		}
		
		public function get text():String 
		{
			if(wt && wt.isEmpty()) return "";
			return input.text;
		}
		
		public function get waterMark():String 
		{
			return _watermark;
		}
		
		
		// show error function 
		public function showError(value:Boolean = true):void
		{
			TweenMax.killTweensOf(bg);
			TweenMax.killTweensOf(input);
			if(value) 
			{
				TweenMax.to(border, 0.3, { tint: 0xff0000 } );
				TweenMax.to(input, 0.3, { tint: 0xff0000 } );
			}
			else
			{
				TweenMax.to(border, 0, { tint: null } );
				TweenMax.to(input, 0, { tint: null } );
			}
		}
		
			
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		// on update text
		private function onUpdateText(e:Event):void 
		{
			isEmpty = (wt)?wt.isEmpty():(input.text == "");
			TEXT_UPDATE.dispatch();
		}
		
		// on focus in
		private function onFocusIn(e:FocusEvent):void 
		{
			
			
		}
		
		
		// on focus out
		private function onFocusOut(e:FocusEvent):void 
		{
			/*
			if (input.text == "")
			{
				input.text = defaultText;
				isEmpty = true;
				TEXT_UPDATE.dispatch();
				setDefaultStyle(true);
			}
			*/
			
		}
	
		
		private function setDefaultStyle(value:Boolean):void 
		{
			if (value) 
			{
				input.alpha = 0.7;
			}
			else
			{
				input.alpha = 1;
			}
		}
		
	}

}