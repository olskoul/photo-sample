package comp
{
	import com.bit101.components.HSlider;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import mcs.hslider.tooltip.View;
	
	public class CustomHSlider extends HSlider
	{
		protected var tooltip:MovieClip;
		
		public function CustomHSlider(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, defaultHandler);
		}
		
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			tooltip = new mcs.hslider.tooltip.View();
			tooltip.x = _handle.x + _handle.width/2;
			tooltip.y = _handle.y;
			tooltip.txt.text = value;
			addChildAt(tooltip,0);
		}
		
		override protected function onSlide(event:MouseEvent):void
		{
			super.onSlide(event);
			tooltip.x = _handle.x + _handle.width/2;
			tooltip.txt.text = value;
		}
		
		override protected function onBackClick(event:MouseEvent):void
		{
			super.onBackClick(event);
			tooltip.x = _handle.x + _handle.width/2;
			tooltip.txt.text = value;
		}
		
		/**
		 * Adjusts position of handle when value, maximum or minimum have changed.
		 * TODO: Should also be called when slider is resized.
		 */
		override protected function positionHandle():void
		{
			super.positionHandle();
			tooltip.x = _handle.x + _handle.width/2;
			tooltip.y = _handle.y;
			tooltip.txt.text = value;
		}
		
	}
}