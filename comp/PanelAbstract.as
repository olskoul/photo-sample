/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class PanelAbstract extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public static var STAGE : DisplayObjectContainer;
		
		// opening position
		protected var openPos : Point;
		protected var isOver : Boolean = false;
		protected var isOpen : Boolean = false;
		protected var blockOutClick : Boolean= false;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PanelAbstract() {}
		
		////////////////////////////////////////////////////////////////
		//	SET
		////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * open at precise point
		 */
		public function open( $pos : Point ):void
		{
			visible = false;
			openPos = $pos;
			
			mouseEnabled = true;
			// show
			TweenMax.killTweensOf(this);
			TweenMax.to(this, .4, { ease : Strong.easeOut, x:openPos.x, y:openPos.y, autoAlpha:1, startAt:{autoAlpha :0, y:openPos.y - 30, x:openPos.x}, onComplete:openCompleted});
			
			// check if out of zone
			this.addEventListener(MouseEvent.ROLL_OUT, onOut);
			this.addEventListener(MouseEvent.ROLL_OVER, onOver);
			
			isOpen = true;
			
			if(!parent) STAGE.addChild(this);
			
		}
		
		
		private function openCompleted():void
		{
			if(!isOver) stage.addEventListener(MouseEvent.CLICK, onOutClick);
		}

		private function onOver(e:Event):void
		{
			isOver = true;
			stage.removeEventListener(MouseEvent.CLICK, onOutClick);
		}
		private function onOut(e:Event):void
		{
			isOver = false;
			stage.addEventListener(MouseEvent.CLICK, onOutClick);
		}
		
		private function onOutClick(e:Event):void
		{
			if(blockOutClick) return;
			stage.removeEventListener(MouseEvent.CLICK, onOutClick);
			close();
		}
		
		
		/**
		 *
		 */
		public function close(e:Event = null):void
		{
			if(!isOpen) return;
			this.removeEventListener(MouseEvent.ROLL_OUT, onOut);
			this.removeEventListener(MouseEvent.ROLL_OVER, onOver);
			stage.removeEventListener(MouseEvent.CLICK, close);
			
			mouseEnabled = false;
			isOver = false;
			TweenMax.killTweensOf(this);
			TweenMax.to(this, .4, {ease : Strong.easeOut, y:openPos.y - 20, autoAlpha:0, onComplete:destroy});
			isOpen = false;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * destroy must be done using close
		 */
		protected function destroy():void
		{
			this.removeEventListener(MouseEvent.ROLL_OUT, onOut);
			this.removeEventListener(MouseEvent.ROLL_OVER, onOver);
			stage.removeEventListener(MouseEvent.CLICK, onOutClick);
			if(this.parent) this.parent.removeChild(this);
		}
		
		
		/**
		 * replace button by new one
		 */
		protected function replaceButton( buttonToReplace : Sprite, newButton:Sprite, alignOnRight : Boolean = false ):void
		{
			newButton.y = buttonToReplace.y;
			
			if(alignOnRight) newButton.x = buttonToReplace.x + buttonToReplace.width - newButton.width;
			else newButton.x = buttonToReplace.x;
			
			if(buttonToReplace.parent){
				buttonToReplace.parent.addChildAt(newButton, buttonToReplace.parent.getChildIndex(buttonToReplace));
				buttonToReplace.parent.removeChild(buttonToReplace);
			}
		}
		
	}
}