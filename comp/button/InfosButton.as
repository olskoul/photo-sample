package comp.button
{
	/**
	 * Infos button class
	 * Use to add information on anything in the app.
	 * Just add it to display list, give it the string you want to see on rollOver.
	 * */
	
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	
	import data.Infos;
	import data.TooltipVo;
	
	import library.infos.icon;
	
	import manager.SessionManager;
	
	import utils.Colors;
	
	public class InfosButton extends Sprite
	{
		private static const infoIconBmd:BitmapData = new library.infos.icon();
		
		//privates
		private var toolTipVo:TooltipVo;
		private var icon:Bitmap;
		
		//publics
		public var overColor:Number = Colors.GREEN;
		/**
		 * ResourceKey: The key corresponding to the text node in the resource file
		 * parent: The display object that will add it to the its displaylist
		 * posX: The X position (of the parent)
		 * posy: The Y position (of the parent)
		 * */
		public function InfosButton(resourceKey:String, _parent:DisplayObjectContainer = null, posX:Number = 0, posY:Number = 0, persistOnKill:Boolean = false, arrowType:String = TooltipVo.ARROW_TYPE_HORIZONTAL)
		{
			super();
			
			//construct
			construct(resourceKey, arrowType);
			
			//Position
			x = posX;
			y = posY;
			
			//addChild if parent
			if(_parent)
				_parent.addChild(this);
			
		}
		
		/**
		 * Construct view
		 * Add icon
		 * Add listeners
		 * */
		private function construct(resourceKey:String, arrowType:String):void
		{
			// create tooltip VO
			toolTipVo = new TooltipVo(resourceKey,TooltipVo.TYPE_SIMPLE,null,Colors.GREEN, Colors.WHITE, arrowType );
			//toolTipVo.autoHide = -1;
			//toolTipVo.persistOnKill = false;
			
			//icon
			icon = new Bitmap(infoIconBmd);
			addChild(icon);
			
			var color:Number = (!Infos.config.isRebranding ) ? Colors.BLUE_LIGHT : Infos.config.rebrandingMainColor;
			TweenMax.killTweensOf(icon);
			TweenMax.to(icon,0,{tint:color});
			
			//listeners
			addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void
			{
				TweenMax.killTweensOf(icon);
				TweenMax.to(icon,0,{tint:overColor});
				//DefaultTooltip.killTooltipById("infoproject");  
				DefaultTooltip.tooltipOnClip(icon , toolTipVo, true);
				
			});
			addEventListener(MouseEvent.ROLL_OUT, function():void
			{
				TweenMax.killTweensOf(icon);
				TweenMax.to(icon,0,{tint:color});
				DefaultTooltip.killAllTooltips();
			});
			
			
		}
		
		
		
		/**
		 * Destroy 
		 * */
		public function destroy():void
		{
			toolTipVo = null;
			
			if(icon)
			{
				TweenMax.killTweensOf(icon);
				if(icon.parent)
					icon.parent.removeChild(icon);
			}
			
			if(parent)
				parent.removeChild(this);
		}
	}
}