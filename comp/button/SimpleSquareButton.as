package comp.button
{
	import flash.display.MovieClip;
	
	import mcs.simpleSquareButton.view;
	
	import utils.Colors;
	import utils.TextFormats;

	public class SimpleSquareButton extends SimpleButton
	{
		protected var iconIndex:Number = -1;

		public static function createBasicToolTipBtn( label:String, handler:Function = null, iconIndex:int = -1):SimpleSquareButton
		{
			var btn:SimpleSquareButton = new SimpleSquareButton( label, // label
				Colors.BLUE, //over color
				handler, //handler
				Colors.WHITE, //off color
				Colors.BLUE, //off color label
				Colors.WHITE,1,true,iconIndex // over color label
				
			); 
				return btn;
		}
		
		public function SimpleSquareButton(labelKey:String="no label", overColor:Number=Colors.BLUE, handler:Function=null, _offColor:Number=Colors.WHITE, _labelColor:Number=Colors.GREY_DARK, _labelOverColor:Number=0, _offAlpha:Number=1, showBottomShadow:Boolean=true, _iconIndex:int = -1,buttonLabelAsString:Boolean = false)
		{
			iconIndex = _iconIndex;
			super(labelKey, overColor, handler, _offColor, _labelColor, _labelOverColor, _offAlpha, showBottomShadow,false,buttonLabelAsString);
		}
		
		override protected function getBtnView():MovieClip
		{
			var v : MovieClip = new mcs.simpleSquareButton.view();
			icon = v.icon;
			if(icon){
				if(iconIndex == -1 && icon.parent) icon.parent.removeChild(icon);
				else icon.gotoAndStop( iconIndex);
			}
			return v; 
		}
		
		protected function get hasIcon():Boolean
		{
			if(icon && icon.parent) return true;
			return false;
		}
		
		
		override protected function redraw():void
		{
			cacheAsBitmap = false;
			var iconMargin : Number = 5;
			var left : Number;
			var btnWidth:Number;
			
			
			if( forcedWidth == -1 )
			{
				if(hasIcon)
				{
					if(label.text != "")
					{
						bg.width = label.width + icon.width + iconMargin + _margin*2;
						icon.x = _margin;
						label.x = icon.x + icon.width + iconMargin;
					}
					else
					{
						
						bg.width = _margin + icon.width;
						icon.x = bg.width/2;
					}
				}
				else
				{
					bg.width = label.width + _margin*2;
					label.x = _margin;
				}
			}
			else
			{
				bg.width = forcedWidth;
				if( hasIcon ) 
				{
					icon.x = Math.floor(forcedWidth*.5 - (label.width+icon.width+iconMargin)*.5)
					label.x = icon.x + icon.width + iconMargin;
					if(label.width > forcedWidth - icon.width - iconMargin) label.width = forcedWidth - icon.width - iconMargin;
				}
				else
				{
					label.x = Math.floor(forcedWidth*.5 - label.width*.5)
					if(label.width > forcedWidth) label.width = forcedWidth;
				}
			}
		}
	}
}