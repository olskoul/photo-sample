package comp.button
{
	import com.greensock.TweenMax;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	
	import data.TooltipVo;
	
	import mcs.simpleButton.view;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;

	public class SimpleButton extends Sprite
	{
		// ui
		protected var btn:MovieClip;
		protected var label:TextField;
		protected var bg:MovieClip;
		protected var icon:MovieClip;
		// data

		

		

		protected var handler:Function;
		protected var _handlerParam:*;

		protected var offColor:Number;
		protected var offAlpha:Number;
		protected var labelColor:Number;
		protected var labelOverColor:Number;
		protected var _enabled:Boolean;
		protected var _margin:Number = 17;
		protected var _tooltip:TooltipVo;
		private var _selected:Boolean;
		
		protected var _forcedWidth : Number = -1;
		
		/**
		 
		  
		 applyToAllBtn = new SimpleButton(	"lefttab.backgrounds.applytoall", // label
																Colors.GREY_LIGHT, //over color
																applyToBkgToAllPages, //handler
																Colors.GREEN, //off color
																Colors.WHITE, //off color label
																Colors.GREEN); // over color label
		 * 
		 * */
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		///////////////////////////////////////////////////////////////
		public function SimpleButton(labelKey:String="no label", overColor:Number = Colors.BLUE, handler:Function = null, _offColor:Number = Colors.WHITE, _labelColor:Number = Colors.GREY_DARK, _labelOverColor:Number = 0, _offAlpha:Number = 1, showBottomShadow:Boolean = true,isMini:Boolean = false, buttonLabelAsString:Boolean = false)
		{
			setupBtn(handler,overColor, _offColor, _labelColor, _labelOverColor, _offAlpha,isMini);		
			setLabel(labelKey, !buttonLabelAsString);
			bg.shadow.visible = showBottomShadow;
		}
		
		////////////////////////////////////////////////////////////////
		//	QUICK HELPERS
		////////////////////////////////////////////////////////////////
		
				public static function createApplyButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, 
				Colors.BLUE, //over color
				handler, //handler
				Colors.GREEN, //off color
				Colors.WHITE, //off color label
				Colors.WHITE // over color label
			);
		}
		
		public static function createApplyToAllButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton( label, // label
				Colors.BLUE, //over color
				handler, //handler
				Colors.GREY_LIGHT, //off color
				Colors.GREEN, //off color label
				Colors.WHITE // over color label
			); 
		}
		
		public static function createPopupOkButton( label:String, handler:Function = null, buttonLabelAsString:Boolean = false):SimpleButton
		{
			return new SimpleButton(label, Colors.WHITE, handler, Colors.GREEN, Colors.WHITE, Colors.GREEN,1,true,false,buttonLabelAsString);
		}
		
		public static function createPopupCancelButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.WHITE, handler, Colors.RED, Colors.WHITE, Colors.RED);
		}
		
		public static function createBlueButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.WHITE, handler, Colors.BLUE, Colors.WHITE, Colors.BLUE);
		}
		
		public static function createGreyButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.WHITE, handler, Colors.GREY, Colors.WHITE, Colors.GREY);
		}
		
		public static function createWhiteButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.GREEN, handler, Colors.WHITE, Colors.GREY_DARK, Colors.WHITE);
		}
		
		public static function createDeleteButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.GREY_LIGHT, handler, Colors.RED, Colors.WHITE, Colors.RED);
		}
		
		public static function createTopBarButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.BLUE, handler, Colors.WHITE, Colors.GREY_DARK, Colors.WHITE);
		}
		
		public static function createTopBarYellowButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.BLUE, handler, Colors.YELLOW, Colors.BLACK, Colors.WHITE);
		}
		
		public static function createGreenButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.GREY_LIGHT,handler,Colors.GREEN,Colors.WHITE,Colors.GREEN);
		}
		
		public static function createGreenAndBlueButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.BLUE,handler,Colors.GREEN,Colors.WHITE,Colors.WHITE);
		}
		
		public static function createRedButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.GREY_LIGHT, handler, Colors.RED, Colors.WHITE, Colors.RED);
		}
		
		public static function createLangButton( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.BLUE, handler, Colors.BLUE_LIGHT, Colors.WHITE, Colors.WHITE);
		}
		
		public static function createFrameTextCTA( label:String, handler:Function = null):SimpleButton
		{
			//return new SimpleButton(label, Colors.BLUE, handler, Colors.GREY_LIGHT, Colors.BLUE, Colors.WHITE);
			//return new SimpleButton(label, Colors.WHITE, handler, Colors.BLUE, Colors.WHITE, Colors.BLUE);
			return new SimpleButton(label, Colors.WHITE, handler, Colors.BLUE_LIGHT, Colors.BLUE, Colors.BLUE);
		}
		
		public static function createFramePhotoCTA( label:String, handler:Function = null):SimpleButton
		{
			//return new SimpleButton(label, Colors.ORANGE, handler, Colors.GREY_LIGHT, Colors.ORANGE, Colors.WHITE);
			//return new SimpleButton(label, Colors.WHITE, handler, Colors.ORANGE, Colors.WHITE, Colors.ORANGE);
			return new SimpleButton(label, Colors.WHITE, handler, Colors.GREY_LIGHT, Colors.GREY_DARK, Colors.GREY_DARK);
		}
		
		public static function createPhotoErrorCTA( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.WHITE, handler, Colors.RED_FLASH, Colors.WHITE, Colors.RED_FLASH,1,false, true);
		}
		
		public static function createLangChoiceBTN( label:String, handler:Function = null):SimpleButton
		{
			return new SimpleButton(label, Colors.GREEN, handler, Colors.WHITE, Colors.GREY_DARK, Colors.WHITE,1,false, false,true);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		public function get handlerParam():*
		{
			return _handlerParam;
		}
		
		public function set handlerParam(value:*):void
		{
			_handlerParam = value;
		}
		
		
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void
		{
			_selected = value;
			if(value)
			{
				this.dispatchEvent(new MouseEvent(MouseEvent.ROLL_OVER,false));
			}
			else
			{
				this.dispatchEvent(new MouseEvent(MouseEvent.ROLL_OUT,false));
			}
		}
		
		/**
		 * getter setter for tooltip
		 */
		public function set tooltip(value : TooltipVo):void
		{
			_tooltip = value;
		}
		public function get tooltip():TooltipVo
		{
			return _tooltip;
		}
		
		
		public function set margin( value:Number ):void
		{
			_margin = value;
			redraw();
		};
		
		
		/**
		 *
		 */
		public function set forcedWidth(value : Number):void
		{
			_forcedWidth = value;
			redraw();
		}
		public function get forcedWidth():Number
		{
			return _forcedWidth;
		}
		
		
		/**
		 * getter setter for enabled
		 */
		public function set enabled(value : Boolean):void
		{
			_enabled = value;
			mouseEnabled = value;
			alpha = (value)?1:.5;
		}
		public function get enabled():Boolean
		{
			return _enabled;
		}
	
		////////////////////////////////////////////////////////////////
		//	PUBLIC
		////////////////////////////////////////////////////////////////
		
		public function setLabel(labelKey:String, isResource:Boolean = true):void
		{
			ResourcesManager.setText(this.label,labelKey,null,isResource);
			redraw();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE
		////////////////////////////////////////////////////////////////
		
		protected function getBtnView():MovieClip
		{
			return new mcs.simpleButton.view();
		}
		
		protected function redraw():void
		{
			cacheAsBitmap = false;
			if(forcedWidth ==-1){
				label.x = _margin;
				bg.width = _margin + label.width + _margin
			}
			else
			{
				bg.width = forcedWidth;
				if(label.width > forcedWidth) label.width = forcedWidth;
				label.x = Math.floor(forcedWidth*.5 - label.width*.5)
			}
			cacheAsBitmap = true;
		}
		
		private function setupBtn(handler:Function,overColor:Number,_offColor:Number, _labelColor:Number, _labelOverColor:Number, _offAlpha:Number, _isMini:Boolean):void
		{
			offColor = _offColor;
			offAlpha = _offAlpha;
			labelColor = _labelColor;
			labelOverColor = (_labelOverColor == 0)?labelColor:_labelOverColor;
			
			btn = getBtnView();
			
			label = btn.label;
			bg = btn.bg;
			if(_isMini) {
				bg.scaleY = .7;
				//label.scaleY = label.scaleX = .9;
				label.y = bg.height*.5 - label.height*.5;
			}
			
			
			label.autoSize = "left";
			mouseChildren = false;
			buttonMode = true;
			this.handler = handler;
			TweenMax.to(bg.face,0,{tint:offColor, alpha:offAlpha});
			TweenMax.to(label,0,{tint:labelColor});
			if(icon)
			TweenMax.to(icon,0,{tint:labelColor});
			
			addEventListener(MouseEvent.CLICK, clickHandler);
			addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void
			{
				if(selected)
					return;
				TweenMax.killTweensOf(label);
				TweenMax.killTweensOf(bg.face);
				if(icon)
				{
					TweenMax.killTweensOf(icon);
					TweenMax.to(icon,0,{tint:labelOverColor});
				}
				
				TweenMax.to(bg.face,0,{tint:overColor});
				TweenMax.to(label,0,{tint:labelOverColor});
				
				if(_tooltip){
					DefaultTooltip.tooltipOnClip(e.currentTarget as flash.display.DisplayObject, _tooltip);
				}
			});
			addEventListener(MouseEvent.ROLL_OUT, function():void
			{
				if(selected)
					return;
				TweenMax.killTweensOf(label);
				TweenMax.killTweensOf(bg.face);
				
				TweenMax.to(bg.face,0,{tint:offColor});
				TweenMax.to(label,0,{tint:labelColor});
				if(icon)
				{
					TweenMax.killTweensOf(icon);
					TweenMax.to(icon,0,{tint:labelColor});
				}
				
				if(_tooltip)
				DefaultTooltip.killAllTooltips();
			});
			
			addChild(btn);
		}
		
		protected function clickHandler(event:MouseEvent):void
		{
			if(handler != null)
			{
				if(handlerParam)
					handler.call(this,handlerParam);
				else
				handler.call();
			}
		}
	}
}