package comp.button
{
	import flash.display.MovieClip;
	
	import mcs.simpleThinButton.view;
	
	import utils.Colors;

	public class SimpleThinButton extends SimpleButton
	{
		public function SimpleThinButton(labelKey:String="no label", overColor:Number=Colors.BLUE, handler:Function=null, _offColor:Number=Colors.WHITE, _labelColor:Number=Colors.GREY_DARK, _labelOverColor:Number=0, _offAlpha:Number=1, showBottomShadow:Boolean=true, buttonLabelAsString:Boolean = false)
		{
			super(labelKey, overColor, handler, _offColor, _labelColor, _labelOverColor, _offAlpha, showBottomShadow, false, buttonLabelAsString);
		}
		
		override protected function getBtnView():MovieClip
		{
			return new mcs.simpleThinButton.view();
		}
	}
}