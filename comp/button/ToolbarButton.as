package comp.button
{
	import com.greensock.TweenMax;
	
	import comp.DefaultTooltip;
	import comp.label.LabelTictac;
	
	import data.TooltipVo;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import library.button.toobarButton;
	
	import utils.Colors;
	import utils.TextFormats;

	public class ToolbarButton extends Sprite
	{
		// icon
		public static const TYPE_SWAP : int = 1;
		public static const TYPE_UNDO : int = 2;
		public static const TYPE_REDO : int = 3;
		public static const TYPE_COPY : int = 4;
		public static const TYPE_CUT : int = 5;
		public static const TYPE_PASTE : int = 6;
		public static const TYPE_ADD_TEXT : int = 7;
		public static const TYPE_PREVIEW : int = 8;
		public static const TYPE_AUTO_FILL : int = 9;
		public static const CALENDAR_COLOR : int = 10;
		public static const TYPE_EXPAND : int = 11;
		public static const TYPE_COLLAPSE : int = 12;
		public static const TYPE_GRID : int = 13;
		public static const TYPE_ADD_PHOTO : int = 14;
		public static const TYPE_QR_CODE : int = 15;
		
		// ui
		private var btn:MovieClip = new library.button.toobarButton();
		private var bg:MovieClip;
		private var icon:MovieClip;
		
		// data
		private var handler:Function;
		private var _enabled:Boolean;
		private var _selected:Boolean;
		private var _tooltip:TooltipVo;
		private var label:LabelTictac;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		///////////////////////////////////////////////////////////////
		public function ToolbarButton(iconType:int, $tooltip:String = null, $handler:Function = null, _label:String = "")
		{
			if($tooltip) _tooltip = new TooltipVo($tooltip);
			this.icon = btn.icon;
			this.bg = btn.bg;
			icon.gotoAndStop(iconType);
			mouseChildren = false;
			buttonMode = true;
			this.handler = $handler;
			
			addChild(btn);
			
			if(_label != "")
			{
				icon.visible = false;
				label = new LabelTictac(this,icon.x,0,_label);
				label.textFormatUpdate(TextFormats.ASAP_BOLD(11));
				bg.width = icon.x + label.width + icon.x;
				label.y = bg.height - label.height >> 1;
			}
			
			// listeners
			if(handler != null) addEventListener(MouseEvent.CLICK, handler);
			addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent):void
			{
				if(!_selected) // rollover only on non selected buttons
				{
					var bgColor : Number = Colors.BLUE;
					var iconColor : Number = Colors.WHITE;
					var labelColor : Number = Colors.WHITE;
					TweenMax.killTweensOf(bg);
					TweenMax.killTweensOf(icon);
					TweenMax.to(bg,0,{tint:bgColor});
					TweenMax.to(icon,0,{tint:iconColor});
					
					if(label)
					{
						TweenMax.killTweensOf(label);
						TweenMax.to(label,0,{tint:labelColor});
					}
				}
				if(_tooltip){
					DefaultTooltip.tooltipOnClip(e.currentTarget as flash.display.DisplayObject, _tooltip);
				}
			});
			addEventListener(MouseEvent.ROLL_OUT, function():void
			{
				var bgColor : Object = (_selected)? Colors.YELLOW : null;
				var iconColor : Object = (_selected)? Colors.WHITE : null;
				var labelColor : Object = (_selected)? Colors.WHITE : null;
				TweenMax.killTweensOf(bg);
				TweenMax.killTweensOf(icon);
				TweenMax.to(bg,0,{tint:bgColor});
				TweenMax.to(icon,0,{tint:iconColor});
				
				if(label)
				{
					TweenMax.killTweensOf(label);
					TweenMax.to(label,0,{tint:labelColor});
				}
				
				DefaultTooltip.killAllTooltips();
			});
			
			
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * getter setter for tooltip
		 */
		public function set tooltip(value : TooltipVo):void
		{
			_tooltip = value;
		}
		public function get tooltip():TooltipVo
		{
			return _tooltip;
		}
		
		/**
		 * getter setter for enabled
		 */
		public function set enabled(value : Boolean):void
		{
			_enabled = value;
			mouseEnabled = value;
			alpha = (value)?1:.2;
		}
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		
		
		/**
		 * getter setter for selected
		 */
		public function set selected(value : Boolean):void
		{
			_selected = value;
			dispatchEvent(new MouseEvent(MouseEvent.ROLL_OUT)); // force mouse up
		}
		public function get selected():Boolean
		{
			return _selected;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC
		////////////////////////////////////////////////////////////////
		/**
		 * Change icon
		 */
		public function changeIcon(value : int):void
		{
			icon.gotoAndStop(value);
		}
		
		/**
		 * Change tooltip label
		 */
		public function changeTooltipLabel(value : String):void
		{
			tooltip.labelKey = value;
		}
		
		/**
		 * Change handler
		 */
		public function changeHandler(value : Function):void
		{
			handler = value;
			removeEventListener(MouseEvent.CLICK, handler);
			addEventListener(MouseEvent.CLICK, handler);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE
		////////////////////////////////////////////////////////////////
		

	}
}