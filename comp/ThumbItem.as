/**
 * jonathan@nguyen.eu
 * 
 * LEFT CONTENT ITEM
 * is the base class of all clickable photo thumb, covertype, background thumb... of the left menu
 * 
 * 
 * 
 * */
package comp
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.globalization.LocaleID;
	import flash.net.URLRequest;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.engine.TypographicCase;
	import flash.utils.getQualifiedClassName;
	
	import be.antho.data.ResourcesManager;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import data.BackgroundVo;
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	import data.ThumbVo;
	import data.TooltipVo;
	
	import manager.DragDropManager;
	
	import offline.data.LocalInstances;
	import offline.manager.LocalStorageManager;
	
	CONFIG::offline
		{
			import offline.manager.OfflineAssetsManager;
		}
	
	
	import org.osflash.signals.Signal;
	
	import utils.BitmapUtils;
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.TextUtils;
	import offline.data.OfflinePhotoLoader;

	TweenPlugin.activate([GlowFilterPlugin]);
	
	public class ThumbItem extends DraggableItem
	{	
		// signals
		public const LOAD_COMPLETE : Signal = new Signal(ThumbItem);
		public const LOAD_FAIL : Signal = new Signal(ThumbItem, String);
		public static const SELECTED:Signal = new Signal(ThumbItem);
		public const DOUBLE_CLICK:Signal = new Signal(ThumbItem);
		public const CLICK:Signal = new Signal(ThumbItem);
		
		// const
		public const MARGIN:int = 10;
		
		// public 
		public var thumbVo : ThumbVo;
		public var draggable:Boolean = true;
		public var allowDoubleClick:Boolean = true;
		public var labelView:TextField;
		public var tooltip:TooltipVo;
		
		// view
		protected var container:Sprite = new Sprite();
		protected var thumb:Bitmap = new Bitmap();
		
		protected var _isLoaded : Boolean = false;
		protected var isLoading : Boolean = false;
		protected var imageLoader:BulkLoader; 
		private var _selected:Boolean;
		private var _bounds:Rectangle;
		protected var bgFillColor:uint = Colors.WHITE;
		
		// error
		private static var errorIconBitmapData:BitmapData;
		private var errorIcon:Bitmap;
		
		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function ThumbItem(thumbVo:ThumbVo, dragGroupType:String = null, _bgFillColor:uint = Colors.WHITE)
		{
			super(dragGroupType);
			this.thumbVo = thumbVo;
			this.bgFillColor = _bgFillColor;
			_bounds = (thumbVo.bounds)?thumbVo.bounds:new Rectangle(0,0,80,65);
			this.visible = false;
			this.alpha = 0;
			
			construct();
		}
		
		/**
		 * ------------------------------------ GETTER / SETTER -------------------------------------
		 */

		/**
		 * GETTER SETTER selected state
		 */
		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(value:Boolean):void
		{
			_selected = value;
			if(value)
				overHandler(null);
			else
				outHandler(null);
		}
		
		public function get bounds():Rectangle{ return _bounds };
		
		
		public function get isLoaded():Boolean
		{
			return _isLoaded
		}
		
		
		/**
		 * add thumb without load
		 * thumb sould be set in the Vo
		 */
		public function set constraint( value : Boolean ):void
		{
			if(value)constraintIntoBounds();
		}
		
		/**
		 * get thumb bitmapdata
		 */
		public function get thumbBitmapData():BitmapData
		{
			if(thumb)
				return thumb.bitmapData;
			else
				return null;
		}
		
		/**
		 * check if content is a local content or an external content
		 */
		public function get IS_LOCAL_CONTENT():Boolean
		{
			if( !Infos.IS_DESKTOP ) return false;
			if(this is BackgroundItem)
			{
				//overriden in BackgroundItem				
				
			}
			if(this is ClipartItem) return true;
			if(this is PhotoItem) return true;
			if(this is OverlayerItem) return true;
			if(this is BackgroundCustomItem) return true;
			return true;
		}
		
		
		/**
		 * ------------------------------------ INTRO / OUTRO -------------------------------------
		 */
		
		/**
		 * intro is the show method, when adding view on display list, we should directly call the intro method
		 */
		public function intro():void
		{		
			if(visible) return;
			
			this.visible = true;
			mouseEnabled = false;
			
			
			if(isLoaded) TweenMax.to(this, 1, {delay:Math.random()*0.2, autoAlpha:1, tint:null, ease:Strong.easeOut, onComplete:activate});
			else TweenMax.delayedCall(Math.random()*1, loadThumb);
		}
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public function outro():void
		{			
			mouseEnabled = false;
			TweenMax.killTweensOf(this);
			if(container) TweenMax.killTweensOf(container);
			TweenMax.killDelayedCallsTo(loadThumb);
			this.visible = false;
			this.alpha = 0;
		}
		
		
		/**
		 * ------------------------------------ PUBLIC Clone Thumb data -------------------------------------
		 */
		
		public function cloneThumbBitmapdata() :BitmapData
		{
			if(thumb.bitmapData != null)
				return thumb.bitmapData.clone();
			else
				return null;
		}
		
		/**
		 * ------------------------------------ PUBLIC Download assets locally -------------------------------------
		 */

		public function startDownloadAsset():void
		{
			CONFIG::offline
				{
					if(IS_LOCAL_CONTENT) return;
					var photoVo:PhotoVo = thumbVo as PhotoVo;
					var isPng:Boolean = (photoVo is BackgroundVo)?false:true;
					OfflineAssetsManager.instance.downloadAssetLocally(this, downloadAssetsComplete, thumbBitmapData, isPng);
					photoVo.resetLocalContentValue();
				}
		}
		
		protected function downloadAssetsComplete():void
		{
			var photoVo:PhotoVo = thumbVo as PhotoVo;
			photoVo.resetLocalContentValue();
		}
		
		/**
		 * ------------------------------------ PUBLIC update Thumb data -------------------------------------
		 */
		
		public function updateThumbData( bmd : BitmapData ) :void
		{
			if(thumb.bitmapData) thumb.bitmapData.dispose();
			thumb.bitmapData = bmd;
			
			// check ratio of thumb 
			//if(thumbVo is PhotoVo) trace("THUMB RATIO is : " + (bmd.width/bmd.height) + " & PHOTO RATIO is : " +((thumbVo as PhotoVo).width /(thumbVo as PhotoVo).height) );
			
			_isLoaded = true;
			thumb.smoothing = true;
			
			// place it correcly
			constraintIntoBounds();
		}
		
		
		/**
		 * ------------------------------------ View creation and layout -------------------------------------
		 */
		
		/**
		 * Build view
		 * */
		protected function construct():void
		{
			addChild(container);
			
			container.graphics.beginFill(bgFillColor);
			container.graphics.lineStyle(1,Colors.GREY_LIGHT,1);
			container.graphics.drawRect(bounds.x,bounds.y, bounds.width+5,bounds.height+5);
			container.graphics.endFill();
			addChild(thumb);
		}
		
		/**
		 * add textfiled under thumb if label is set
		 * */
		public function addLabel():void
		{
			if(thumbVo.label != "")
			{
				if(!labelView)
				{
					labelView = new TextField();
					labelView.embedFonts = true;
					var textformat:TextFormat = TextFormats.ASAP_REGULAR(11,Colors.GREY_DARK);
					textformat.align = TextFormatAlign.CENTER;
					labelView.defaultTextFormat = textformat;
					labelView.antiAliasType = AntiAliasType.ADVANCED;
					labelView.y = container.height;
					labelView.x = container.x;
					labelView.width = container.width;
					labelView.height = 20;
					addChild(labelView);
				}
				labelView.text = thumbVo.label;
			}
		}
		
		/**
		 * Constraint thumb into bounds
		 */
		protected function constraintIntoBounds():void
		{
			//constraint dimension
			var thumbWidth : Number = thumb.width/thumb.scaleX;
			var thumbHeight : Number = thumb.height/thumb.scaleY;
			
			var thumbScale : Number = bounds.width/thumbWidth;
			if(thumbHeight*thumbScale > bounds.height) thumbScale = bounds.height/thumbHeight;
			
			thumb.scaleY = thumb.scaleX = thumbScale;
			thumb.x = (container.width - thumb.width) *.5;
			thumb.y = (container.height - thumb.height) *.5;
		}
		
		
		/**
		 * show thumb content once image is loaded
		 */
		protected function show():void
		{
			//show
			activate();
			
			// show
			//TweenMax.killTweensOf(this);
			//TweenMax.to(this, 1, { autoAlpha:1, tint:null, ease:Strong.easeOut});
			alpha = 1;
			visible = 1;
		}
		
		/**
		 * activate clip
		 * */
		protected function activate(e:Event = null):void
		{
			mouseEnabled = true;
			mouseChildren = false;
			
			setListeners();
			
			// cache as bitmap once image is loaded and created
			cacheAsBitmap = true;
			opaqueBackground = 0xffffff;
		}
		
		
		
		
		/**
		 * ------------------------------------ IMAGE LOAD -------------------------------------
		 */
		
		/**
		 * load thumb
		 */
		protected function loadThumb():void
		{
			// if we are already loading, do nothing
			if(isLoading) return;
			
			// security
			if(!thumbVo || !thumbVo.thumbUrl) {
				Debug.warn("ThumbItem.loadThumb has no thumbUrl");
				return;
			}
			
			//make sure you show only item that are locally available when INTERNET IS OFF
			if(!Infos.INTERNET && Infos.IS_DESKTOP)
			{
				if(!IS_LOCAL_CONTENT)
				{
					displayErrorState();
					return;
				}
			}
			
			
			// log thumb info
			//Debug.log("ThumbItem ("+ getQualifiedClassName(this) +") > loadThumb : "+thumbVo.thumbUrl);
			
			// be sure to reset imageloader
			killImageLoader();
			
			// flags
			_isLoaded = false;
			isLoading = true;
			
			//remove error
			container.graphics.clear();
			container.graphics.beginFill(bgFillColor);
			container.graphics.lineStyle(1,Colors.GREY_LIGHT,1);
			container.graphics.drawRect(bounds.x,bounds.y, bounds.width+5,bounds.height+5);
			container.graphics.endFill();
			
			//removeToolTip (WHY??, we cannot do this otherwise photo thumbs do not work anymore)
			// tooltip = null;
			
			// load online
			if(!IS_LOCAL_CONTENT){
				imageLoader = new BulkLoader();
				imageLoader.addEventListener(BulkProgressEvent.COMPLETE, imageLoadedHandler);
				imageLoader.addEventListener(BulkLoader.ERROR, imageLoadFail);
				imageLoader.add(thumbVo.thumbUrl, {id:"thumb", type:BulkLoader.TYPE_IMAGE});
				imageLoader.start();	
			}
			// load local
			else
			{
				LocalStorageManager.instance.loadLocalImage( thumbVo.thumbUrl , localImageLoaded, imageLoadFail );
			}
		}
		protected function imageLoadedHandler(e:BulkProgressEvent):void
		{
			// update thumb data
			updateThumbData( imageLoader.getBitmapData("thumb", true) );
			
			// remove image loader
			killImageLoader();
			
			// show image
			show();
		}
		protected function localImageLoaded( thumbData : BitmapData ):void
		{
			// update thumb data
			updateThumbData( thumbData );
			
			// show image
			show();
		}
		protected function imageLoadFail(e:ErrorEvent):void
		{
			Debug.warn("ThumbItem > imageLoadFail : ("+thumbVo.thumbUrl+") "+e.text);
			displayErrorState();
		}
		protected function displayErrorState():void
		{
			allowDoubleClick = false;
			draggable = false;
			
			// show error
			var color:Number = Colors.RED_FLASH;
			
			if(this is AssetItem)
				color = Colors.BACKGROUND_COLOR;
			
			container.graphics.clear();
			container.graphics.beginFill(color,0.5);
			container.graphics.lineStyle(1,color,1);
			container.graphics.drawRect(bounds.x,bounds.y, bounds.width+5,bounds.height+5);
			container.graphics.endFill();
			
			// add error icon bitmap
			/*if(!errorIconBitmapData)
			{
				var errorTxt : TextField = new TextField();
				errorTxt.defaultTextFormat =  TextFormats.ASAP_REGULAR(30, Colors.RED_FLASH);
				errorTxt.autoSize = "left";
				errorTxt.text = "X";
				errorTxt.alpha = 0.5;
				errorIconBitmapData = BitmapUtils.drawClipBitmap(errorTxt).bitmapData;
			}
			if(!errorIcon)
			{
				errorIcon = new Bitmap(errorIconBitmapData);
				errorIcon.x = (width - errorIcon.width)/2;
				errorIcon.y = (height - errorIcon.height)/3;
				//addChild(errorIcon);
			}*/
			
			// set state
			_isLoaded = (!Infos.INTERNET && !(this is PhotoItem))?false:true; //TODO:John describe this condition
			isLoading = false;
			
			// set error tooltip
			var thumbInfo : String = "";
			var tooltipContentKey:String = "";
			
			if(thumbVo is PhotoVo)
			{
				thumbInfo = " : "+(thumbVo as PhotoVo).name; 
				tooltipContentKey = "popup.framephoto.error.title";
				tooltip = new TooltipVo(tooltipContentKey,TooltipVo.TYPE_SIMPLE, null, Colors.RED_FLASH, Colors.WHITE );
				tooltip.additionalUntranslatedContent = thumbInfo;
			}
			
			if(this is AssetItem)
			{
				tooltipContentKey = "tooltip.thumb.assets.no.internet";
				tooltip = new TooltipVo(tooltipContentKey,TooltipVo.TYPE_SIMPLE, null, Colors.BLUE, Colors.WHITE );
			}			

			// show error
			show();
		}
		
		
		
		
		/**
		 * ------------------------------------ DESTROY / DISPOSE -------------------------------------
		 */
		
		public override function destroy():void
		{
			if(thumb && thumb.bitmapData) thumb.bitmapData.dispose();
			_isLoaded = false;
			killImageLoader();
			
			// clear graphics
			graphics.clear();
			
			// remove local events
			removeEventListener(MouseEvent.DOUBLE_CLICK, doubleClickHandler);
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			removeEventListener(MouseEvent.ROLL_OVER, overHandler);
			removeEventListener(MouseEvent.ROLL_OUT, outHandler);
			
			// kill signals
			DOUBLE_CLICK.removeAll();
			CLICK.removeAll();
			LOAD_COMPLETE.removeAll();
			LOAD_FAIL.removeAll();
			
			// remove from display list
			if(parent) parent.removeChild(this);
			
			// kill tweens
			TweenMax.killTweensOf(this);
			TweenMax.killTweensOf(container);
			TweenMax.killDelayedCallsTo( loadThumb );
			
			super.destroy();
		}
		
		// dispose loader
		protected function killImageLoader():void
		{
			if(imageLoader) {
				imageLoader.removeAll();
				imageLoader.clear();
				imageLoader.removeEventListener(BulkProgressEvent.COMPLETE, imageLoadedHandler);
				imageLoader.removeEventListener(BulkLoader.ERROR, imageLoadFail);
				imageLoader = null;
			}
			isLoading = false;
		}		
		
		/**
		 * ------------------------------------ ONLINE / OFFLINE -------------------------------------
		 */
		
		
		
		/**
		 * OFFLINE switch for url
		 * Define what url to return
		 * Checks if the file exists offline or not, return the offline version first, if not, returns the online
		 */
		protected function urlToReturn(onlineURL:String, offlineUrl:String, localInstance:String):String
		{
			if(Infos.IS_DESKTOP)
			{
				//directly return the path without checking again if exsit
				if(localInstance == LocalInstances.EXIST_VALUE_YES)
					return offlineUrl;
				
				//Check if exsit then set the flag return path
				CONFIG::offline
					{
						if(OfflineAssetsManager.instance.exist(offlineUrl))
						{
							localInstance = LocalInstances.EXIST_VALUE_YES;
							return offlineUrl;
						}
					}
					
					//does not exist, return the online path and this vo to the download Queue
					localInstance = LocalInstances.EXIST_VALUE_NO;
			}
			return onlineURL;
		}
		
		
		/**
		 * ------------------------------------ MOUSE HANDLER -------------------------------------
		 */
		
		
		/**
		 * listeners
		 * */
		protected function setListeners():void
		{
			dragEnabled = true;
			doubleClickEnabled = true;
			addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickHandler);
			addEventListener(MouseEvent.ROLL_OVER, overHandler);
			addEventListener(MouseEvent.ROLL_OUT, outHandler);
		}
		
		protected function doubleClickHandler(event:MouseEvent):void
		{
			if(allowDoubleClick) DOUBLE_CLICK.dispatch(this);
		}
		
		protected function overHandler(event:MouseEvent):void
		{
			opaqueBackground = null;
			TweenMax.killTweensOf(container);
			TweenMax.to(container, .1, {glowFilter:{color:0x66a83e, blurX:18, blurY:18, strength:2, alpha:.5}});
			if(tooltip){DefaultTooltip.tooltipOnClip(this, tooltip);}
		}
		
		protected function outHandler(event:MouseEvent):void
		{
			if(!selected)
			{
			TweenMax.killTweensOf(container);
			TweenMax.to(container, .1, {glowFilter:{color:0x66a83e, blurX:0, blurY:0, strength:0, alpha:0}, onComplete:function():void{container.filters = [];opaqueBackground = 0xffffff;}});
			}
			if(tooltip){DefaultTooltip.killAllTooltips()}
		}
		
		/**
		 * Click handler, simple dispatch of the signal CLICK
		 * */
		override protected function mouseDownHandler(event:MouseEvent):void
		{
			CLICK.dispatch(this);
			if(draggable) super.mouseDownHandler(event);
		}	
		
	}
}