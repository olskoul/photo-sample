package comp
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	import library.font.asap.bold;
	
	import utils.Colors;
	
	public class DropTargetArea extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	Properties
		////////////////////////////////////////////////////////////////
		
		protected var highlightBorder:Sprite;
		public var dropTypes:Array;
		public var usePixelCheck:Boolean = true; // Use pixel check instead of bounding box. It allows more precise check. If false, the object bounding box is used for drop hit
		public var isCustomDropArea : Boolean; // drop target area is custom if it's managing by itself the drop areas --> checkCustomDrop
		public var customCheckHandler : Function;
		public var customHighLight : Function;

		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function DropTargetArea( $isCustom : Boolean =false)
		{
			isCustomDropArea = $isCustom;
			super();
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * rectangle of highlighted zone
		 * -> is not use in calculation, just display
		 * -> purpose is to be overriden to allow display different hightlight zone
		 */
		public function get dropBounds():Rectangle
		{
			return this.getRect(this);
		}
		
		
		/**
		 * highlight drop area
		 */
		public function highLight(flag:Boolean, dragItem : DraggableItem ):void
		{
			if(customHighLight != null) {
				customHighLight(flag, dragItem);
				return;
			}
			
			if( flag ) // highlight
			{ 
				if(!highlightBorder) highlightBorder = new Sprite();	
				highlightBorder.graphics.clear();
				highlightBorder.graphics.lineStyle(3, Colors.YELLOW);
				var bounds:Rectangle = dropBounds;
				highlightBorder.graphics.beginFill(Colors.YELLOW, .7);
				highlightBorder.graphics.drawRect(bounds.x+1, bounds.y+1, bounds.width-2, bounds.height-2);
				addChild(highlightBorder);
			} 
			else // remove hightlight
			{
				if(highlightBorder && highlightBorder.parent) highlightBorder.parent.removeChild(highlightBorder);
				highlightBorder = null;
			}
		}
		
		/**
		 *  make custom drop check
		 *  -> must be overriden in custom dropTargetAreas
		 */
		public function checkCustomDrop( dragItem:DraggableItem ):Boolean
		{
			if(customCheckHandler != null) return customCheckHandler( dragItem );
			return false;
		}
		
	}
}