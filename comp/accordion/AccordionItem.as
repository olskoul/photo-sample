package comp.accordion
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	import org.osflash.signals.Signal;

	public class AccordionItem extends Sprite
	{
		// signals
		public const RESIZE : Signal = new Signal();
		
		public var id:String;
		public var header:AccordionItemHeader;
		public var content:AccordionItemContent;
		private  var theMask:Sprite;
		private var _enabled:Boolean;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function AccordionItem(header:AccordionItemHeader, content:AccordionItemContent, clickable:Boolean = true)
		{
			this.header = header;
			this.content = content;
			
			content.y = header.y + header.height;
			addChild(header);
			addChild(content);
			
			header.contentHeight = content.height;
			header.clickable = clickable;
			if(clickable)
				header.CLICKED.add(headerClicked);
			
			theMask = new Sprite();
			theMask.y = content.y;
			theMask.graphics.beginFill(0xFF00FF,.5);
			theMask.graphics.drawRect(0,0,header.width, header.contentHeight);
			theMask.mouseEnabled = false;
			content.mask = theMask;
			addChild(theMask);
			
			//scrollRect = new Rectangle(0,0, width,Math.ceil(height)+5);
			
			//opaqueBackground = 0x0000ff;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		public function get contentHeight():Number
		{
			return theMask.y + theMask.height;
		}
		
		

		public function set enabled(value:Boolean):void
		{
			_enabled = value;
			mouseEnabled = value;
			mouseChildren = value;
			/*if(!value)
				openItem(false,0);*/
			header.alpha = content.alpha = (value)?1:.5;
			
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * replace content by new one
		 */
		public function replaceContent(newContent:AccordionItemContent, openIt:Boolean = true, enable:Boolean = true):void
		{
			if(content)
			content.destroy();
			
			content = newContent;
			content.y = header.y + header.height;
			addChild(content);
			
			header.contentHeight = content.height;
			
			theMask.graphics.clear();
			theMask.graphics.beginFill(0xFF00FF,1);
			theMask.graphics.drawRect(0,0,header.width, header.contentHeight);
			theMask.y = content.y;
			
			openItem(openIt);
			enabled = enable;
			RESIZE.dispatch();
		}
		
		
		/**
		 * Destroy view
		 */
		public function destroy():void
		{
			RESIZE.removeAll();
			TweenMax.killTweensOf(this);
			if(header) header.destroy();
			if(content) content.destroy();
			if(theMask.parent)theMask.parent.removeChild(theMask);
			if(theMask) {
				TweenMax.killTweensOf(theMask);
				theMask = null;
			}
		}
		
		/**
		 * read only
		 * check if accordion item is open
		 */
		public function get isOpen():Boolean
		{
			return header.isOpen;
		}
		
		
		/**
		 * open accordion view
		 * > flag > true or false to open/close
		 * > with animation > open animation or not
		 * > enable > is the accordion item enabled or not
		 */
		public function open( flag:Boolean, withAnimation : Boolean = true, enable:Boolean = true):void
		{
			var duration : Number = (withAnimation)? null : 0.3; // cannot use 0 otherwise scrollrect is not updated after the second frame
			openItem(flag, duration);
			enabled = enable;
			visible = true;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * when header is clicked
		 */
		protected function headerClicked():void
		{
			if(header.isOpen) openItem(false);
			else openItem()
		}
		
		/**
		 * open/close list box
		 */
		private function openItem(open : Boolean = true, duration:Number = .5 ):void
		{
			if(!theMask || !content) return;
			if(open){
				header.isOpen = true;
				if(!content.parent) addChild(content);
				//TweenMax.to( this, duration, { scrollRect:{height:Math.ceil(content.y+content.height)+5}, ease:Strong.easeOut, onUpdate:dispatchResizeEvent, onComplete:dispatchResizeEvent } );
				TweenMax.killTweensOf( theMask );
				TweenMax.to( theMask, duration, { height:header.contentHeight+10, ease:Strong.easeOut, onUpdate:dispatchResizeEvent, onComplete:dispatchResizeEvent } );
			}
			else
			{
				header.isOpen = false;
				//TweenMax.to( this, duration, { scrollRect:{height:header.y+header.height}, ease:Strong.easeOut, onUpdate:dispatchResizeEvent, onComplete:dispatchResizeEvent  } );
				TweenMax.killTweensOf( theMask );
				TweenMax.to( theMask, duration, { height:0, ease:Strong.easeOut, onUpdate:dispatchResizeEvent, onComplete:dispatchResizeEvent  } );
			}
		}
		
		/**
		 * Resize dispatcher
		 */
		private function dispatchResizeEvent():void
		{
			if(theMask && content && theMask.height == 0 && content.parent) content.parent.removeChild(content);
			RESIZE.dispatch();
		}
		
		
	}
}