/**
 * originally antho.be and refined jonathan@nguyen.eu
 * CUSOTM ACCORDION CLASS
 * 
 * */


package comp.accordion
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class Accordion extends Sprite
	{
		// signals
		public var RESIZE : Signal = new Signal(); // when content has been resized
		public var UPDATE : Signal = new Signal(); // when content has been updated (can be resize or scroll);
		
		//Statics
		private static const SCROLLTHUMB_WIDTH:Number = 15;
		public static const MAX_WIDTH:Number = 292;
		public var SCROLL_LIMIT_TOP:Number ;
		public var SCROLL_LIMIT_BOTTOM:Number;
		
		// data
		public var accordionItems : Vector.<AccordionItem> = new Vector.<AccordionItem>;
		private var _contentHeight:Number;
		protected var posY:Number;
		protected var margin:int = 5;
		
		//Scroll
		private var _currentItem : AccordionItem; // first accordion item currently visible 
		public var useParentPosY:Boolean;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		
		public function Accordion()
		{
			super();
		}
		
		////////////////////////////////////////////////////////////////
		//	getter
		////////////////////////////////////////////////////////////////
		
		public function get yPosition():Number
		{
			return (useParentPosY)?parent.y:y;
		}

		/**
		 *
		 */
		public function get currentItem():AccordionItem
		{
			return _currentItem;
		}
		
		/**
		 *
		 */
		public function get contentHeight():Number
		{
			return _contentHeight;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * on mouse wheel
		 * Difficulty here is to have a cross platform normalized wheel delta (which is different on different browsers and OS)
		 */
		/*
		private var wheelTimer : Timer ;
		private var wheelPosY : Number = NaN;
		public function wheel(delta:Number):void
		{
			// http://stackoverflow.com/questions/5527601/normalizing-mousewheel-speed-across-browsers
			// normalize accross browsers and platforms
			var d : Number = (delta<0)?-1:1; 
			
			// create wheel timer to check for acceleration
			if(!wheelTimer){
				wheelTimer = new Timer(500,1);
				//wheelTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onWheelTimerComplete);
			}
			if(!wheelTimer.running) {
				wheelTimer.start();
				wheelPosY = y;
			}
			
			// new pos y
			var newPos : int = wheelPosY + d*10;
			
			// check limits
			if(newPos >SCROLL_LIMIT_TOP) newPos = SCROLL_LIMIT_TOP;
			else if(newPos < SCROLL_LIMIT_BOTTOM-height) newPos = SCROLL_LIMIT_BOTTOM-height;
			
			// tween to
			TweenMax.killTweensOf(this);
			TweenMax.to(this, .3, {ease:Strong.easeOut, y:newPos, onUpdate:updateItems});
		}
		*/
		
		
		
		/**
		 * Add an item to accordion
		 * - accordionHeader (required)
		 * - accordionContent (required)
		 * - uniqId : uniqId for the accordion item (required)
		 * - isOpen : is the accordion open or closed at start
		 * - isEnabled : is the accordion enabled or not at start
		 * - clickable : is it possible to open/close the accordion item
		 */
		public function addAccordionItem(accordionHeader:AccordionItemHeader, accordionContent:AccordionItemContent, uniqId:String, _isOpen:Boolean = true, _isEnabled:Boolean = true, _clickable:Boolean = true):AccordionItem
		{
			var item:AccordionItem = new AccordionItem(accordionHeader, accordionContent,_clickable);
			item.visible = false;
			item.id = uniqId;
			item.y = posY;
			addChild(item);
			accordionItems.push(item);
			item.RESIZE.add(onItemResize);
			posY = item.y + item.height+margin; // upadte y pos
			//item.enabled = _isEnabled;
			TweenMax.delayedCall(0,item.open,[_isOpen,false, _isEnabled]);
			//item.open(_isOpen,false, _isEnabled);
			
			//update BG
			drawBg();
			return item;
		}
		
		/**
		 * Draw bg
		 */
		public function drawBg():void
		{
			graphics.clear();
			graphics.beginFill(0xFFFFFF);
			graphics.drawRect(0,0,width,height+5);
			graphics.endFill();
		}
		
		/**
		 * Returns a AccordionItem based on a id
		 */
		public function getItemById(id:String):AccordionItem
		{
			var itemToReturn:AccordionItem;
			for (var i:int = 0; i < accordionItems.length; i++) 
			{
				var item:AccordionItem = accordionItems[i];
				if(item.id == id)
					return item;
			}
			
			return itemToReturn;
		}
		
		/**
		 * on list animation update
		 * > update item visiblity
		 * > draw background
		 */
		public function updateItems():void
		{
			
			var posY : int = 0;
			var accordionItem : AccordionItem;
			var visibleZoneHeight : int = SCROLL_LIMIT_BOTTOM;
			var contentPosY:Number;
			_currentItem = null;
			
			for (var i:int = 0; i < accordionItems.length; i++) 
			{
				accordionItem = accordionItems[i];
				// visibility
				if(yPosition + posY + accordionItem.contentHeight < 0) accordionItem.visible = false;
				else if (yPosition + posY > visibleZoneHeight ) accordionItem.visible = false;
				else 
				{
					accordionItem.visible = true;
					if(!_currentItem) _currentItem = accordionItem;
				}
				
				// calculate position
				accordionItem.y = posY;
				
				// check if item must update some of its content
				if(accordionItem.visible){ 
					contentPosY = yPosition + posY;
					accordionItem.content.updateContentVisibility(contentPosY);
				} 	
				
				// set new poss
				posY = posY + accordionItem.contentHeight + margin;
			
			}
			
			drawBg();
			// update 
			UPDATE.dispatch();
			
		}
		
		
		/**
		 * Go directly to accordion item
		 */
		public function goToItem( itemId : String ):void
		{
			/*for (var i:int = 0, item:AccordionItem; i < accordionItems.length; i++) 
			{
				item = accordionItems[i];
				if(item.id == itemId){
					scrollTo(-item.y);
					updateItems();
				}
			}*/
		}
		
		
		
		/**
		 * clean all items, lists and event from this accordion
		 * --> this is not a destroy, just cleaning of content
		 */
		public function dispose():void
		{
			var accordionItem : AccordionItem;
			for (var i:int = 0; i < accordionItems.length; i++) 
			{
				accordionItem = accordionItems[i];
				accordionItem.destroy();
				if(accordionItem.parent) accordionItem.parent.removeChild(accordionItem);
			}
			accordionItems = new Vector.<AccordionItem>;
			
			// dispose events
			//RESIZE.removeAll();
			UPDATE.removeAll();
		}
		
		public function destroy():void
		{
			dispose();
			RESIZE.removeAll();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * an item has been resized
		 */
		private function onItemResize():void
		{
			updateItems();
			//trace("accordion item resize");
			RESIZE.dispatch();	
		}
		
		/*
		private function scrollTo( posY : Number ):void
		{
			var newPos : int = posY;
			if(newPos >SCROLL_LIMIT_TOP) newPos = SCROLL_LIMIT_TOP;
			else if(newPos < SCROLL_LIMIT_BOTTOM-height) newPos = SCROLL_LIMIT_BOTTOM-height;
			y = newPos;
		}
		*/
		
	}
	
	
}