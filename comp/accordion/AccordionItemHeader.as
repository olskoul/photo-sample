﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp.accordion 
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;

	public class AccordionItemHeader extends Sprite
	{
		// signals
		public const CLICKED : Signal = new Signal();
		
		// public vars
		public var isOpen : Boolean = true; // is current associated list open 
		public var contentHeight : Number = 0;
		
		// private
		protected var _clickable : Boolean = true;
		protected var bg:Sprite = new Sprite();
		protected var hitZone : Sprite;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function AccordionItemHeader() 
		{
			clickable = true;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * init accordion item header
		 */
		public function init():void
		{
			construct();
		}
		
		
		/**
		 * intro is the show method, when adding view on display list, we should directly call the intro method
		 */
		public function intro():void
		{			
		}
		
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public function outro():void
		{					
		}
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public function destroy():void
		{		
			if(parent) parent.removeChild(this);
			CLICKED.removeAll();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * 
		 * CONSTRCUT HEADER (overriden, because many type of header possible);
		 * 
		 */
		protected function construct():void
		{
			bg.graphics.beginFill(0x0000ff, 0.9);
			bg.graphics.drawRect(0,0,Accordion.MAX_WIDTH,20);
			bg.graphics.endFill();
			addChild(bg);
			setDefaultHitZone(bg);
		}
		
		
		/**
		 * define hitzone for open behavior
		 */
		protected function setDefaultHitZone( view : Sprite):void
		{
			hitZone = view;
			clickable = _clickable;
		}
		
		
		/**
		 *  Getter / setter clickable
		 *  > activate or not click on thumb item
		 */
		public function get clickable():Boolean
		{
			return _clickable;
		}
		public function set clickable( value : Boolean ):void
		{
			_clickable = value;
			if(!hitZone) return;
			
			hitZone.buttonMode = _clickable;
			if(_clickable)
			{
				hitZone.addEventListener(MouseEvent.CLICK, onClick);
				hitZone.addEventListener(MouseEvent.ROLL_OUT, onOut);
				hitZone.addEventListener(MouseEvent.ROLL_OVER, onOver);
			}
			else removeListeners();
		}
		
		/**
		 *
		 */
		protected function removeListeners():void
		{
			if(!hitZone) return;
			hitZone.removeEventListener(MouseEvent.CLICK, onClick);
			hitZone.removeEventListener(MouseEvent.ROLL_OUT, onOut);
			hitZone.removeEventListener(MouseEvent.ROLL_OVER, onOver);
		}
		
		
		/**
		 * on item clicked
		 */
		protected function onClick(e:MouseEvent):void
		{		
			//TweenMax.to(stage, .5, {tint:null, ease:Strong.easeOut, startAt:{tint:0xffffff} });
			CLICKED.dispatch();
		}
		
		/**
		 * on item clicked
		 */
		protected function onOut(e:MouseEvent):void
		{		
			TweenMax.killTweensOf(this);
			TweenMax.to(this, 0, {alpha:1});
		}
		
		/**
		 * on item clicked
		 */
		protected function onOver(e:MouseEvent):void
		{		
			TweenMax.killTweensOf(this);
			TweenMax.to(this, 0, {alpha:.6});
		}
		
	}
	
}