﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp.accordion 
{
	import comp.ThumbItem;
	
	import flash.display.Sprite;

	public class AccordionItemContent extends Sprite
	{
		
		protected const contentVisibilityYLimit:int = 800;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function AccordionItemContent() 
		{
			//opaqueBackground = 0xff0000;
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * upadte content visibility
		 */
		public function updateContentVisibility(contentPosY:Number):void
		{		
			
		}
		
		
		
		/**
		 * intro is the show method, when adding view on display list, we should directly call the intro method
		 */
		public function intro():void
		{		
			visible = true;
			x = Math.round(x);
			y = Math.round(y);
		}
		
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public function outro():void
		{		
			visible = false;
		}
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public function destroy():void
		{		
			while (numChildren) 
			{
				var child:* = getChildAt(0);
				if(child is ThumbItem)
					(ThumbItem(child).destroy());
				else
					removeChild(child);
				
				child = null;
			}
			if(parent) parent.removeChild(this);
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////


	}
	
}