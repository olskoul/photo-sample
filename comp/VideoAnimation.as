package comp 
{
	import be.antho.utils.DataLoadingManager;
	
	import com.greensock.TweenMax;
	
	import data.Infos;
	
	import fl.video.CuePointType;
	import fl.video.FLVPlayback;
	import fl.video.MetadataEvent;
	import fl.video.VideoEvent;
	import fl.video.VideoPlayer;
	import fl.video.VideoScaleMode;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import org.osflash.signals.Signal;
	import utils.Debug;

	/**
	 * ...
	 * @author Anthony De Brackeleire
	 */
	public class VideoAnimation extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////	
		
		private const INTRO_VIDEO_INDEX:Number = 0;
		private const LOOP_VIDEO_INDEX:Number = 1;
		
		/*
		private var stream:NetStream;
		private var connection:NetConnection;
		private var vid:Video;
		private var videoURL:String;
		*/
		public const READY_SIGNAL:Signal = new Signal();
		public const COMPLETE_SIGNAL:Signal = new Signal();
		public const MESSAGE_SIGNAL:Signal = new Signal();
		public const SHOWN_SIGNAL:Signal = new Signal();
		
		private var videosReady:Number = 0 ;
		private var currentVideo:Number = 0;
		private var completed:Boolean = false ;
		private var messageShowed:Boolean = false;
		
		
		private var video:FLVPlayback;
		private var urls:Array = ["intro.flv", "loop.flv"];
		private var endVideoBmp:Bitmap;
		private var endVideoBmd:BitmapData;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////	
		public function VideoAnimation() 
		{
			Debug.log("VIDEO ANIMATION CREATION");
			this.visible = false;
			this.alpha = 0;
			endVideoBmd = new BitmapData(App.instance.stage.stageWidth, App.instance.stage.stageHeight, false, 0x231f21);
			endVideoBmp = new Bitmap(endVideoBmd);
			addChild(endVideoBmp);
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////		
		
		public function init():void
		{
			Debug.log("VideoAnimation.init");
			
			// create video if it doesn't exist
			if (!video)
			{
				video = new FLVPlayback();
				video.scaleMode = VideoScaleMode.EXACT_FIT;
				
				// for perfect match
				video.width = 974;
				video.height = 600;
				video.y = 0;
				video.x = 0;
				
				addChild(video);
				video.autoPlay = false;
				video.visible = false;
			}
			
			prepareVideo();
			DataLoadingManager.instance.showLoading();
		}
		
		public function stop():void
		{
			video.stop();
			endVideoBmp.visible = false;
		}
		
		public function start():void
		{
			messageShowed = false;
			endVideoBmp.visible = false;
			completed = false;
			video.visible = false;
			
			video.getVideoPlayer(INTRO_VIDEO_INDEX).seek(0);
			video.getVideoPlayer(LOOP_VIDEO_INDEX).seek(0);
			
			video.activeVideoPlayerIndex = INTRO_VIDEO_INDEX;
			video.visibleVideoPlayerIndex = INTRO_VIDEO_INDEX;
			currentVideo = INTRO_VIDEO_INDEX;
			
			if(videosReady == 2) startFirstVideo();
			TweenMax.delayedCall(0.2, function():void{video.visible = true});
		}
		
		/**
		 * restart video
		 */
		public function restart():void
		{
			video.stop();
			start();
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function prepareVideo():void
		{
			// listeners
			video.addEventListener(VideoEvent.COMPLETE, onVideoComplete);
			video.addEventListener(VideoEvent.READY, onVideoReady);
			//video.addEventListener(MetadataEvent.CUE_POINT, onVideoCuePoint);
			video.addEventListener(VideoEvent.PLAYHEAD_UPDATE, onPlayHeadUpdate);
			
			// prepare first video
			video.activeVideoPlayerIndex = INTRO_VIDEO_INDEX;
			video.load(Infos.config.videoPath + urls[0]);
			
			// prepare second video
			video.activeVideoPlayerIndex = LOOP_VIDEO_INDEX;
			video.load(Infos.config.videoPath + urls[1]);
			
			video.visible = false;
		}
		
		
	
		/**
		 * play loop video
		 */
		private function playLoopVideo():void
		{
			if(currentVideo == LOOP_VIDEO_INDEX)
			{
				video.play();
				return;
			}
			
			video.activeVideoPlayerIndex = LOOP_VIDEO_INDEX;
			video.play();
			video.visibleVideoPlayerIndex = LOOP_VIDEO_INDEX;
			currentVideo = LOOP_VIDEO_INDEX;
		}
		
		
		
		
		
		
		/**
		 * Show
		 */
		
		public function show():void
		{
			TweenMax.to(this, 0.5, {autoAlpha:1, onComplete:function():void{ SHOWN_SIGNAL.dispatch(); } });
		}
		
		
		
		/**
		 *
		 */
		private function startFirstVideo():void
		{
			//video.getVideoPlayer(1).removeEventListener(MetadataEvent.CUE_POINT, onVideoCuePoint);
			//video.getVideoPlayer(1).addEventListener(MetadataEvent.CUE_POINT, onVideoCuePoint);
			/*
			var checkPoint:Object = video.findCuePoint("msg", CuePointType.ACTIONSCRIPT); 
			Debug.log('checkPoint = '+checkPoint);
			if(checkPoint) Debug.log('checkPoint = '+checkPoint.time);
			video.addASCuePoint(4, "msg");
			*/
		
			DataLoadingManager.instance.hideLoading();
			video.play();
		}
		
		/**
		 * onVideo Ready
		 */
		private function onVideoReady(e:Event):void 
		{
			Debug.log("VideoAnimation.Ready");
			videosReady +=1;
			if(videosReady == 2) startFirstVideo();
		}
		
		/**
		 * on cuepoint
		 * NOT USED BECAUSE OF PRECISION PROBLEM AND REUSE OF EVENT NOT WORKING ON REPLAY
		 */
		private function onVideoCuePoint(e:MetadataEvent):void 
		{
			Debug.log("Video Cuepoint : "+e.info.name);
			if(e.info.name == "msg"){
				MESSAGE_SIGNAL.dispatch();
			}
		}
		
		/**
		 * on cuepoint
		 */
		private function onPlayHeadUpdate(e:VideoEvent):void 
		{
			//Debug.log("Video playhead : "+e.playheadTime);
			if(!messageShowed && e.playheadTime > 5)
			{
				messageShowed = true;
				MESSAGE_SIGNAL.dispatch();
			}
		}
		
		
		
		/**
		 * on video , dispatch complete signal
		 */
		private function onVideoComplete(e:VideoEvent):void 
		{
			if(currentVideo == INTRO_VIDEO_INDEX ) Debug.log("VIDEO COMPLETE");
			//else Debug.log("LOOP COMPLETE");
			
			endVideoBmd.draw(video);
			endVideoBmp.visible = true;
			
			if(!completed) 
			{
				COMPLETE_SIGNAL.dispatch();
				completed = true
			}
			
			playLoopVideo();
			
			//video.removeEventListener(VideoEvent.COMPLETE, onVideoComplete);
			//this.visible = false;
			//this.parent.removeChild(this);	
		}
		
		
		
		public function destroy():void
		{}
		
		/*
		public function init()
		{	
			videoURL = ApplicationData.config.videoPath + "messageAnimation.flv";
			
			connection = new NetConnection();
            connection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
            connection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
            connection.connect(null);
		}
		
		public function start():void
		{
			trace("VideoAnimation.start");
			//stream.play(videoURL);
		}
		
		
		public function destroy():void
		{}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		private function connectStream():void 
		{
			trace("VideoAnimation.connectStream");
			var customClient:Object = new Object();
			customClient.onMetaData = metaDataHandler;

			stream = new NetStream(connection);
			stream.client = customClient;
			
            stream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
            stream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			vid = new Video(stage.stageWidth, stage.stageHeight);
            vid.attachNetStream(stream);
            addChild(vid);
			
        }
		
		private function netStatusHandler(event:NetStatusEvent):void 
		{
            switch (event.info.code) {
                case "NetConnection.Connect.Success":
                    connectStream();
                    break;
                case "NetStream.Play.StreamNotFound":
                    trace("Unable to locate video: " + videoURL);
                    break;
            }
        }
		
        private function securityErrorHandler(event:SecurityErrorEvent):void 
		{
            trace("securityErrorHandler: " + event);
        }
        
        private function asyncErrorHandler(event:AsyncErrorEvent):void 
		{
			trace("VideoAnimation.asyncErrorHandler > event : " + event);
            // ignore AsyncErrorEvent events.
        }
		
		function metaDataHandler(infoObject:Object):void 
		{
			//vid.width = infoObject.width;
			trace("infoObject.width : " + infoObject.width);
			//vid.height = infoObject.height;
			trace("infoObject.height : " + infoObject.height);
			stream.play(videoURL);
		}
		*/
	}

}