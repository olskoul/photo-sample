package comp.checkbox
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.Component;
	import com.bit101.components.Label;
	import com.bit101.components.Style;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import library.checkbox.selected.icon;
	
	import utils.Colors;
	import utils.TextFormats;
	
	public class CheckboxTitac extends CheckBox
	{
		private static var checkedBmd:BitmapData = new library.checkbox.selected.icon();
		private var _checked:Bitmap;
		private var _isMini : Boolean;
		private var _labelColor:Number;
		private var _texFormat:TextFormat;
		
		public function CheckboxTitac(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null, isMini:Boolean = false, labelColor:Number = Colors.GREY_DARK)
		{
			_isMini = isMini;
			_labelColor = labelColor;
			super(parent, xpos, ypos, label, defaultHandler);
		}
		
		public function get texFormat():TextFormat
		{
			return _texFormat;
		}

		public function set texFormat(value:TextFormat):void
		{
			_texFormat = value;
			draw();
		}

		/**
		 * Creates the children for this component
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			//_back.filters = [getShadow(2, true)];
			addChild(_back);
			
			_checked = new Bitmap(checkedBmd);
			_button = new Sprite();
			_button.addChild(_checked);
			//_button.filters = [getShadow(1)];
			_button.visible = false;
			addChild(_button);
			
			if(_isMini) {
				_checked.cacheAsBitmap = true;
				_checked.smoothing = true;
				_checked.scaleX = _checked.scaleY = .75;
			}
			
			var labelFontSize:int = (_isMini)?12:13;
			_label = new Label(this, 0, 0, _labelText);
			_label.textField.defaultTextFormat = (_texFormat)?_texFormat:TextFormats.ASAP_REGULAR(labelFontSize,_labelColor);
			draw();
			
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			dispatchEvent(new Event(Component.DRAW));
			
			var size : Number = (_isMini)?15:20;
			
			_back.graphics.clear();
			_back.graphics.lineStyle(1,Colors.GREY);
			_back.graphics.beginFill(0xFFFFFF);
			_back.graphics.drawRect(0, 0, size, size);
			_back.graphics.endFill();
			
			_button.x = _back.width - _button.width >> 1;
			_button.y = _back.height - _button.height >> 1;
			
			var labelFontSize:int = (_isMini)?12:13;
			_label.textField.defaultTextFormat = (_texFormat)?_texFormat:TextFormats.ASAP_REGULAR(labelFontSize,_labelColor);
			_label.text = _labelText;
			_label.setSize(_label.width,((_isMini)?_label.height*0.75:_label.height));
			_label.draw();
			_label.x = _back.width + 5;
			_label.y = ( _back.height - _label.height) / 2;
			_width = _label.width + 12;
			_height = size;
		}
	}
}