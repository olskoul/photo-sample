/***************************************************************
 PROJECT 		: Common code
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
****************************************************************/
package comp
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	public class ScrollView extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		protected var content:DisplayObject;
		protected var HScrollBar : Sprite;
		protected var VScrollBar : Sprite;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function ScrollView(_width:Number, _height:Number, _content:DisplayObject = null) 
		{ 
			this.scrollRect = new Rectangle(0,0,_width, _height);
			
			content = _content;
			if(content) addChild(content);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
	}
}