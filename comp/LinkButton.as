package comp
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.events.MouseEvent;
	
	import mcs.linkButton;
	
	import org.osflash.signals.Signal;
	
	public class LinkButton extends linkButton
	{
		public var CLICKED : Signal = new Signal()
		private var _text : String;
			
			
		public function LinkButton()
		{
			super();
			txt.autoSize = "left";
			this.mouseChildren = false;
			buttonMode = true;
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		/**
		 *
		 */
		public function set text(value : String):void
		{
			_text = value;
			txt.htmlText = "<u>" + _text + "</u>";
		}
		
		/**
		 *
		 */
		private function onRollOver(e:MouseEvent):void
		{
			txt.htmlText = "<u>" + _text + "</u>";
			TweenMax.to(this, .2, {ease:Strong.easeOut, tint:0x7f206f});
		}
		
		/**
		 *
		 */
		private function onRollOut(e:MouseEvent):void
		{
			txt.htmlText = "<u>" + _text + "</u>";
			TweenMax.to(this, .5, {ease:Strong.easeOut, tint:null});
		}
		
		/**
		 *
		 */
		private function onClick(e:MouseEvent):void
		{
			CLICKED.dispatch();
		}
		
	}
}