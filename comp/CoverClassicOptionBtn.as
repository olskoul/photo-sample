package comp
{
	import data.ProductsCatalogue;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import library.cover.classic.corner.icon;
	import library.cover.option.selected.view;
	
	import manager.CoverManager;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	
	public class CoverClassicOptionBtn extends Sprite
	{
		public static const COVER_OPTION_CLICK:Signal = new Signal(CoverClassicOptionBtn);
		private var WIDTH:Number = 21;

		private var _type:String; // "corner" or ProductsCatalogue.COVER_FABRIC_LINEN or ProductsCatalogue.COVER_FABRIC_LEATHER
		private var _color:Number; // the color of the cover / -1 if it's a corner button type
		private var _id:String; // the id : leathergreen, leatherwhite
		private var _selected:Boolean;
		private var selectedView:Sprite;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		// @Type is cover type : ProductsCatalogue.COVER_FABRIC_LINEN or ProductsCatalogue.COVER_FABRIC_LEATHER OR Corner if this is a corner type
		// @id is node value from xml : leathergreen, leatherwhite, etc...
		// @color is color ref from xml : color=0x1e423e
		////////////////////////////////////////////////////////////////
		public function CoverClassicOptionBtn(type:String, id:String, color:Number = -1)
		{
			super();
			_type = type;
			_id = id;
			_color = color;
			
			// button mode
			mouseChildren = false;
			buttonMode = true;
			
			// listeners
			addEventListener(MouseEvent.CLICK,clickHandler);
			COVER_OPTION_CLICK.add(checkSelection);
			
			// construct
			construct();
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * return 
		 */
		public function get labelName():String
		{
			return _id;
		}
		
		public function get id():String
		{
			if(_type == "corner")  				return _id;
				
			return CoverManager.instance.getCoverLabelNameById(_id,type);
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function get color():Number
		{
			return _color;
		}

		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void
		{
			_selected = value;
			selectedView.visible = value;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * construct component
		 */
		private function construct():void
		{
			// if there is a color, it's a cover
			if(color!=-1) //Type leather or linen
			{
				graphics.beginFill(color,1);
				graphics.drawRect(0,0,WIDTH,WIDTH);
				graphics.endFill();
			}
			else //otherwise it's a Type corner
			{
				var icon:MovieClip = new library.cover.classic.corner.icon();
				icon.gotoAndStop(_id);
				addChild(icon);
			}
			
			// create selected view
			selectedView = new library.cover.option.selected.view();
			selectedView.x = width - selectedView.width >> 1;
			selectedView.y = (color!=-1)?-8:-2;
			addChild(selectedView);
		}
		
		
		/**
		 * check if button is selected or not
		 */
		private function checkSelection(btn:CoverClassicOptionBtn):void
		{
			switch(type)
			{
				case "leather":
				case "linen":
					
					if(btn.type == "leather" || btn.type == "linen")
					selected = (btn == this);
					
					break;
				
				case "corner":
					if(btn.type == "corner")
						selected = (btn == this);
					
					break;
			}
			
		}
		
		

		protected function clickHandler(event:MouseEvent):void
		{
			COVER_OPTION_CLICK.dispatch(this);
		}
		
		
		
		
	}
}