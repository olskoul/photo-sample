package comp.textarea
{
	import com.bit101.components.TextArea;
	import com.bit101.components.VScrollBar;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	import be.antho.form.Watermark;
	
	import utils.Colors;
	import utils.Filters;
	import utils.TextFormats;
	
	public class TextAreaTictac extends TextArea
	{
		public var watermark:Watermark;
		
		public function TextAreaTictac(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, text:String="")
		{
			
			super(parent, xpos, ypos, text);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			_tf.defaultTextFormat = TextFormats.ASAP_BOLD(13,Colors.BLUE);
			_scrollbar.showButtons(false);
			_scrollbar.autoHide = true;
			filters = [Filters.INPUT_INNER_SHADOW];
		}
		
		/**
		 *overide draw function
		 */
		override public function draw():void
		{
			super.draw();
			if(watermark)
				watermark.reset();
		}
		
		/**
		 * Add watermark to Textfield
		 */
		public function addWatermark(text:String):void
		{
			watermark = new Watermark(_tf,text);
		}
		
		/**
		 * Set Tab index on textfield
		 */
		public function setTabIndex(value:int):void
		{
			_tf.tabIndex = value;
			
		}
	}
}