package comp
{
	import com.greensock.TweenMax;
	
	import data.LayoutVo;
	import data.ThumbVo;
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mcs.icons.thumbDeleteIcon;
	
	import utils.AssetUtils;
	import utils.Colors;
	
	public class LayoutItem extends ThumbItem
	{
		protected static var deleteIconBitmapData : BitmapData;
		
		public var layoutId:String;
		public var isCalendar:Boolean;
		private var _isCustomLayout:Boolean;
		private var deleteIcon:Bitmap;
		
		public function LayoutItem(dataVo:ThumbVo)
		{
			super(dataVo);
			
			
			
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////
		
		public function get isCustomLayout():Boolean
		{
			return _isCustomLayout;
		}

		public function set isCustomLayout(value:Boolean):void
		{
			_isCustomLayout = value;
			
			/*if(value)
			{
				if(!deleteIconBitmapData){
					var usedIconClip : MovieClip = new mcs.icons.thumbDeleteIcon();
					deleteIconBitmapData = AssetUtils.drawClipBitmap(usedIconClip).bitmapData;
				}
				
				if(!deleteIcon){
					deleteIcon = new Bitmap(deleteIconBitmapData);
					deleteIcon.x = bounds.x + bounds.width - deleteIcon.width+5;
					deleteIcon.y = bounds.y + bounds.height - deleteIcon.height+4;
					deleteIcon.addEventListener(MouseEvent.CLICK, deleteClickHandler,false,0,true);
				}
				addChild(deleteIcon);
			}*/
	
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Listen to LayoutItem SELECTED Signal
		 * */
		public function listenSelection():void
		{
			SELECTED.add(layoutItemSelected);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
				
		/**
		 * Listen to LayoutItemClicked Handler
		 * */ 
		protected function deleteClickHandler(event:Event):void
		{
			trace("yo mama");
		}

		/**
		 * Listen to LayoutItemClicked Handler
		 * */
		private function layoutItemSelected(item:ThumbItem):void
		{
			if(item != this)
				selected = false;
		}
		
		/**
		 * Build view
		 * */
		protected override function construct():void
		{
			addChild(container);
			
			container.graphics.beginFill(bgFillColor);
			container.graphics.lineStyle(1,Colors.GREY_LIGHT,1);
			container.graphics.drawRect(bounds.x,bounds.y, bounds.width+5,bounds.height+5);
			container.graphics.endFill();
			
			addChild(thumb);
		}
		
		
		/**
		 * Constraint thumb into bounds
		 */
		protected override function constraintIntoBounds():void
		{
			//constraint dimension
			/*
			var thumbWidth : Number = thumb.width/thumb.scaleX;
			var thumbHeight : Number = thumb.height/thumb.scaleY;
			
			var thumbScale : Number = bounds.width/thumbWidth;
			if(thumbHeight*thumbScale > bounds.height) thumbScale = bounds.height/thumbHeight;
			
			thumb.scaleY = thumb.scaleX = thumbScale;
			*/
			thumb.x = (container.width - thumb.width) *.5;
			thumb.y = (container.height - thumb.height) *.5;
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		
		
	}
}