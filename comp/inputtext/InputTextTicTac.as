package comp.inputtext
{
	import com.bit101.components.InputText;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import be.antho.form.Watermark;
	
	import utils.Colors;
	import utils.Filters;
	import utils.TextFormats;
	
	public class InputTextTicTac extends InputText
	{
		public var watermark:Watermark;
		
		public function InputTextTicTac(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, text:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, text, defaultHandler);
		}
		
		/**
		 * Creates and adds child display objects.
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			_back.filters = [];
			addChild(_back);
			
			_tf = new TextField();
			_tf.embedFonts = true;
			_tf.selectable = true;
			_tf.type = TextFieldType.INPUT;
			_tf.defaultTextFormat = TextFormats.ASAP_BOLD(13,Colors.BLUE);
			addChild(_tf);
			_tf.addEventListener(Event.CHANGE, onChange);
			filters = [Filters.INPUT_INNER_SHADOW];
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			super.draw();
			_back.graphics.clear();
			//_back.graphics.lineStyle(1,Colors.GREY_LIGHT);
			_back.graphics.beginFill(Colors.WHITE);
			_back.graphics.drawRect(0, 0, _width, _height);
			_back.graphics.endFill();
			
			_tf.displayAsPassword = _password;
			
			if(_text != null)
			{
				_tf.text = _text;
			}
			else 
			{
				_tf.text = "";
			}
			
			_tf.width = _width - 10;
			if(_tf.text == "")
			{
				_tf.text = "X";
				_tf.height = Math.min(_tf.textHeight + 4, _height);
				_tf.text = "";
			}
			else
			{
				_tf.height = Math.min(_tf.textHeight + 4, _height);
			}
			_tf.x = 5;
			_tf.y = Math.round(_height / 2 - _tf.height / 2);
			
			
			if(watermark && _tf.text == watermark.value)
				watermark.reset();
		}
		
		/**
		 * Add watermark to Textfield
		 */
		public function addWatermark(text:String):void
		{
			watermark = new Watermark(_tf,text);
		}
		
		/**
		 * Set Tab index on textfield
		 */
		public function setTabIndex(value:int):void
		{
			_tf.tabIndex = value;
				
		}
	}
}