package comp.numericstepper
{
	import com.bit101.components.InputText;
	import com.bit101.components.NumericStepper;
	
	import comp.inputtext.InputTextTicTac;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	import utils.TextFormats;
	
	public class NumericStepperTicTac extends NumericStepper
	{
		public function NumericStepperTicTac(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, defaultHandler);
		}
		
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		protected override function addChildren():void
		{
			_valueText = new InputTextTicTac(this, 0, 0, "0", onValueTextChange);
			_valueText.restrict = "-0123456789.";
			_valueText.height = 30;
			_valueText.textField.maxChars = 4;
			_minusBtn = new NumericStepperBtn(this, 0, 0, "-");
			_minusBtn.addEventListener(MouseEvent.MOUSE_DOWN, onMinus);
			_minusBtn.setSize(30, 30);
			_plusBtn = new NumericStepperBtn(this, 0, 0, "+");
			_plusBtn.addEventListener(MouseEvent.MOUSE_DOWN, onPlus);
			_plusBtn.setSize(30, 30);
		}
		
		
		/**
		 * Draws the visual ui of the component.
		 */
		public override function draw():void
		{
			_plusBtn.x = _width - 30;
			_minusBtn.x = _width - 61;
			_valueText.text = (Math.round(_value * Math.pow(10, _labelPrecision)) / Math.pow(10, _labelPrecision)).toString();
			_valueText.width = _width - 62;
			_valueText.draw();
		}
	}
}