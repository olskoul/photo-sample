package comp
{
	import data.ThumbVo;
	
	import utils.Colors;
	
	public class ClipartItem extends AssetItem
	{
		public function ClipartItem(thumbVo:ThumbVo)
		{
			super(thumbVo,null,Colors.GREY_LIGHT);
		}
	}
}