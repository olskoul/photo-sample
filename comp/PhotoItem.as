﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp 
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.AutoAlphaPlugin;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import data.Infos;
	import data.PhotoVo;
	
	import manager.DragDropManager;
	
	import mcs.icons.photUsedIcon;
	import mcs.icons.thumbUploadIcon;
	import mcs.icons.uploadIcon;
	
	import org.osflash.signals.Signal;
	
	import utils.AssetUtils;
	
	import view.uploadManager.PhotoUploadItem;
	import view.uploadManager.PhotoUploadManager;

	public class PhotoItem extends ThumbItem
	{
		// signals
		public const START_DRAG : Signal = new Signal(PhotoItem)
		
		// view
		protected static var usedBitmapData : BitmapData;
		protected static var loadingBitmapData : BitmapData;
		protected static var uploadingBitmapData : BitmapData;
		private var usedIcon:Bitmap;
		private var loadingIcon:Bitmap;
		private var uploadingIcon:Bitmap;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function PhotoItem(photoVo:PhotoVo) 
		{
			// create only once used bitmapdata
			if(!usedBitmapData){
				var usedIconClip : MovieClip = new mcs.icons.photUsedIcon();
				usedBitmapData = AssetUtils.drawClipBitmap(usedIconClip).bitmapData;
			}
			if(!loadingBitmapData){
				var loadingIconClip : MovieClip = new mcs.icons.thumbLoadingIcon();
				loadingBitmapData = AssetUtils.drawClipBitmap(loadingIconClip).bitmapData;
			}
			if(!uploadingBitmapData){
				var uploadingIconClip : MovieClip = new mcs.icons.thumbUploadIcon();
				uploadingBitmapData = AssetUtils.drawClipBitmap(uploadingIconClip).bitmapData;
			}
			
			super(photoVo);
			
			this.thumbVo = photoVo;
			
			used = photoVo.used;
			
			// check if we can drag it or not
			draggable = false;
			if(!Infos.IS_DESKTOP && photoVo && photoVo.id) draggable = true;
			else if(Infos.IS_DESKTOP && photoVo) draggable = true;
			
			// bg 
			graphics.beginFill(0xffffff, 1);
			graphics.drawRect(0,0,bounds.width,bounds.height);
			graphics.endFill();
			buttonMode = true;
			mouseChildren = false;
			alpha = 0;
			
			loadingIcon = new Bitmap(loadingBitmapData);
			loadingIcon.x = Math.ceil((bounds.width - loadingIcon.width)/2) + 2;
			loadingIcon.y = Math.ceil((bounds.height - loadingIcon.height)/2) + 2;
			addChild(loadingIcon);
			
			// temp photovo
			if(photoVo.tempID)
			{
				updateTempPhotoItem();
				uploadingIcon = new Bitmap(uploadingBitmapData);
				uploadingIcon.x =  2;
				uploadingIcon.y = Math.ceil(bounds.height - uploadingIcon.height) + 4;
				uploadingIcon.visible = (photoVo.id)? true : false;
				addChild(uploadingIcon);
			}
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		/**
		 *
		 */
		public function updateUsedIcon():void
		{
			used = (thumbVo as PhotoVo).used;
		}
		
		/**
		 * get current photoVo
		 */
		public function get photoVo():PhotoVo
		{
			if(thumbVo && thumbVo is PhotoVo) return thumbVo as PhotoVo;
			return null;
		}
		
		
		/**
		 *
		 */
		private var _used : Boolean;
		public function set used(value:Boolean):void
		{
			_used = value;
			if(value){
				if(!usedIcon){
					usedIcon = new Bitmap(usedBitmapData);
					usedIcon.x = bounds.x + bounds.width - usedIcon.width+5;
					usedIcon.y = bounds.y + bounds.height - usedIcon.height+4;
				}
				addChild(usedIcon);
			} else {
				if(usedIcon && usedIcon.parent) usedIcon.parent.removeChild(usedIcon);
			}
		}
		public function get used():Boolean
		{
			return _used;
		}
		
		
		
		/**
		 * Update temp photo item view
		 */
		public function updateTempPhotoItem():void
		{
			_isLoaded = true; // do not load anything
			
			// check if photo vo is still a temp photovo
			if(photoVo.temp)
			{
				// retrieve current upload item
				var uploadItem : PhotoUploadItem = PhotoUploadManager.instance.getPhotoUploadItemByPhotoId( photoVo.id );
				
				// check if item was aborded
				if(uploadItem.currentState == PhotoUploadItem.STATE_ABORDED)
				{
					displayErrorState();
					removeLoadingIcon();
					removeUploadingIcon();
					photoVo.temp = false;
					photoVo.tempID = null;
					return;
				}
				
				// check thumb 
				if(!thumb) thumb = new Bitmap();
				if(!thumb.bitmapData)
				{
					var tempData : BitmapData = PhotoUploadManager.instance.getTempThumbDataClone( photoVo.id )
					if(tempData) {
						updateThumbData( tempData );
						removeLoadingIcon();
					}
				}
				else
				{
					if(uploadingIcon){
						if(!uploadingIcon.visible) uploadingIcon.visible = true;
						// upload percent
						var uploadItem : PhotoUploadItem = PhotoUploadManager.instance.getPhotoUploadItemByPhotoId( photoVo.id );
						if(uploadItem.currentState >= PhotoUploadItem.STATE_UPLOADING) uploadingIcon.alpha = 1;
						else uploadingIcon.alpha = .6;
					}
				}
			}
			
			draggable = (photoVo && photoVo.id)? true : false;
		}
		
		
		/**
		 * override destroy function of thumbitem
		 */
		override public function destroy():void
		{
			super.destroy();
			
			// signals
			START_DRAG.removeAll();
			
			
			// remove refs to static bitmapdata to be sure GC works
			if(loadingIcon) loadingIcon.bitmapData = null;
			if(usedIcon) usedIcon.bitmapData = null;
			if(uploadingIcon) uploadingIcon.bitmapData = null;
		}
		
	
		
		/**
		 * ------------------------------------ LOAD EVENT OVERRIDES -------------------------------------
		 */

		override protected function imageLoadedHandler(e:BulkProgressEvent):void
		{
			super.imageLoadedHandler(e);
			removeLoadingIcon();
		}
		override protected function localImageLoaded(thumbData:BitmapData):void
		{
			super.localImageLoaded( thumbData );
			removeLoadingIcon();
		}
		override protected function imageLoadFail(e:ErrorEvent):void
		{
			super.imageLoadFail( e );
			removeLoadingIcon();
			removeUploadingIcon();
		}
		
		
		
		/**
		 * ------------------------------------ REMOVE ICONS -------------------------------------
		 */
		
		protected function removeLoadingIcon():void
		{
			if(loadingIcon)
			{
				if(loadingIcon.parent) loadingIcon.parent.removeChild(loadingIcon);
				loadingIcon.bitmapData = null;
				loadingIcon = null;
			}
		}
		
		public function removeUploadingIcon():void
		{
			if(uploadingIcon)
			{
				if(uploadingIcon.parent) uploadingIcon.parent.removeChild(uploadingIcon);
				uploadingIcon.bitmapData = null;
				uploadingIcon = null;
			}
		}
		
		
	}
	
}