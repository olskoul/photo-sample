package comp.combobox
{
	import com.bit101.components.ListItem;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	import utils.TextFormats;
	
	public class ComboBoxListItem extends ListItem
	{
		public function ComboBoxListItem(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, data:Object=null)
		{
			super(parent, xpos, ypos, data);
		}
		
		/**
		 * Initilizes the component.
		 */
		protected override function init() : void
		{
			super.init();
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			setSize(100, 30);
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		public override function draw() : void
		{
			//super.draw();
			graphics.clear();
			graphics.lineStyle(1,0xededed);
			if(_selected)
			{
				graphics.beginFill(0xededed);
			}
			else if(_mouseOver)
			{
				graphics.beginFill(_rolloverColor);
			}
			else
			{
				graphics.beginFill(0xffffff);
			}
			graphics.drawRect(0, 0, width-1, height);
			graphics.endFill();
			
			if(_data == null) return;
			
			if(_data is String)
			{
				_label.text = _data as String;
			}
			else if(_data.hasOwnProperty("label") && _data.label is String)
			{
				_label.text = _data.label;
			}
			else
			{
				_label.text = _data.toString();
			}
			_label.y = Math.round(height - _label.height >> 1);
			_label.x = Math.round(_label.x);
			_label.textField.defaultTextFormat = TextFormats.ASAP_REGULAR(12);
			/*_label.textField.background = true;
			_label.textField.backgroundColor = 0xFF00FF;*/
		}
	}
}