package comp.combobox
{
	import com.bit101.components.List;
	
	import flash.display.DisplayObjectContainer;
	import flash.filters.DropShadowFilter;
	
	public class ComboBoxList extends List
	{
		public function ComboBoxList(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, items:Array=null)
		{
			super(parent, xpos, ypos, items);
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		public override function draw() : void
		{
			super.draw();
			
			_selectedIndex = Math.min(_selectedIndex, _items.length - 1);
			
			
			// panel
			_panel.shadow = false;
			_panel.filters = [];
			_panel.setSize(_width, _height);
			_panel.color = _defaultColor;
			_panel.draw();
			
			
			// scrollbar
			_scrollbar.x = _width - 10;
			var contentHeight:Number = _items.length * _listItemHeight;
			_scrollbar.setThumbPercent(_height / contentHeight); 
			var pageSize:Number = Math.floor(_height / _listItemHeight);
			_scrollbar.maximum = Math.max(0, _items.length - pageSize);
			_scrollbar.pageSize = pageSize;
			_scrollbar.height = _height;
			_scrollbar.draw();
			scrollToSelection();
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected override function addChildren() : void
		{
			super.addChildren();
			_panel.filters = [];
		}
		
		override protected function getShadow(dist:Number, knockout:Boolean = false):DropShadowFilter
		{
			return new DropShadowFilter(0, 45, 0, 0, dist, dist, .3, 1, knockout);
		}
	}
}