package comp.combobox
{
	import com.bit101.components.Component;
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	
	import utils.Colors;
	import utils.TextFormats;
	
	public class ComboBoxLabelBtn extends PushButton
	{
		public function ComboBoxLabelBtn(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
		}
		
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		override protected function drawFace():void
		{
			_face.graphics.clear();
			if(_down)
			{
				_face.graphics.beginFill(0xededed,.5); 
			}
			else
			{
				_face.graphics.beginFill(0xededed,1); 
			}
			
			_face.graphics.drawRoundRectComplex(0, 0, _width, _height, 5, 0, 0, 0);
			_face.graphics.endFill();
			
			_face.filters = [];
			_back.filters = [];
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			_back.filters = [getShadow(2, true)];
			_back.mouseEnabled = false;
			addChild(_back);
			
			_face = new Sprite();
			_face.mouseEnabled = false;
			_face.filters = [getShadow(1)];
			_face.x = 0;
			_face.y = 0;
			addChild(_face);
			
			_label = new Label();
			addChild(_label);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
			addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
			
			_label.textField.defaultTextFormat = TextFormats.ASAP_BOLD(12);
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			dispatchEvent(new Event(Component.DRAW));
			
			/*_back.graphics.clear();
			_back.graphics.beginFill(Style.BACKGROUND);
			_back.graphics.drawRect(0, 0, _width, _height);
			_back.graphics.endFill();*/
			
			drawFace();
			
			_label.text = _labelText;
			_label.autoSize = true;
			_label.draw();
			if(_label.width > _width - 4)
			{
				_label.autoSize = false;
				_label.width = _width - 4;
			}
			else
			{
				_label.autoSize = true;
			}
			_label.draw();
			_label.move(10, _height / 2 - _label.height / 2);
			
		}
		
		override protected function getShadow(dist:Number, knockout:Boolean = false):DropShadowFilter
		{
			return new DropShadowFilter(0, 45, 0, 0, dist, dist, .3, 1, knockout);
		}
	}
}