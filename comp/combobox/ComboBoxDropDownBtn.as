package comp.combobox
{
	import com.bit101.components.Component;
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	
	import data.Infos;
	
	import library.combobox.dropdown.arrow;
	
	import manager.SessionManager;
	
	import utils.BitmapUtils;
	import utils.Colors;
	
	public class ComboBoxDropDownBtn extends PushButton
	{
		private static var arrowBmd:BitmapData = new library.combobox.dropdown.arrow();
		private var _arrow:Bitmap;
		
		public function ComboBoxDropDownBtn(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, label:String="", defaultHandler:Function=null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
		}
		
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		override protected function drawFace():void
		{
			var color:Number = (!Infos.config.isRebranding ) ? Colors.BLUE_LIGHT : Infos.config.rebrandingMainColor;
			
			_face.graphics.clear();
			if(_down)
			{
				_face.graphics.beginFill(color,.5); 
			}
			else
			{
				_face.graphics.beginFill(color,1); 
			}
			
			_face.graphics.drawRect(0, 0, _width, _height);
			_face.graphics.endFill();
			
			_face.filters = [];
			_back.filters = [];
		}
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			dispatchEvent(new Event(Component.DRAW));
			
			/*_back.graphics.clear();
			_back.graphics.beginFill(Style.BACKGROUND);
			_back.graphics.drawRect(0, 0, _width, _height);
			_back.graphics.endFill();*/
			
			drawFace();
			
			_arrow.x = _width - _arrow.width >> 1;
			_arrow.y = _height - _arrow.height >> 1;
			
			_label.visible = false;
			
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			//_back.filters = [getShadow(2, true)];
			_back.mouseEnabled = false;
			addChild(_back);
			
			_face = new Sprite();
			_face.mouseEnabled = false;
			//_face.filters = [getShadow(1)];
			_face.x = 0;
			_face.y = 0;
			addChild(_face);
			
			_label = new Label();
			addChild(_label);
			
			_arrow = new Bitmap(arrowBmd);
			addChild(_arrow);
			
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
			addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
		}
		
		override protected function getShadow(dist:Number, knockout:Boolean = false):DropShadowFilter
		{
			return new DropShadowFilter(0, 45, 0, 0, dist, dist, .3, 1, knockout);
		}
	}
}