package comp.combobox
{
	import com.bit101.components.ComboBox;
	import com.bit101.components.List;
	import com.bit101.components.PushButton;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	
	public class ComboBoxTitTac extends ComboBox
	{
		public function ComboBoxTitTac(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, defaultLabel:String="", items:Array=null)
		{
			super(parent, xpos, ypos, defaultLabel, items);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			//super.addChildren();
			_list = new List(null, 0, 0, _items);
			_list.autoHideScrollBar = true;
			_list.addEventListener(Event.SELECT, onSelect);
			_list.listItemClass = ComboBoxListItem;
			_list.listItemHeight = 25;
			
			
			_labelButton = new ComboBoxLabelBtn(this,0,0,"",onDropDown); //new PushButton(this, 0, 0, "", onDropDown);
			_dropDownButton = new ComboBoxDropDownBtn(this, 0, 0, "+", onDropDown);//new PushButton(this, 0, 0, "+", onDropDown);
		}
		
		override protected function getShadow(dist:Number, knockout:Boolean = false):DropShadowFilter
		{
			return new DropShadowFilter(0, 45, 0, 0, dist, dist, .3, 1, knockout);
		}
	}
}