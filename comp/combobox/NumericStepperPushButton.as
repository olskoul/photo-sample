package comp.combobox
{
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.states.OverrideBase;
	
	import utils.Colors;
	import utils.TextFormats;
	
	public class NumericStepperPushButton extends PushButton
	{

		public function NumericStepperPushButton(parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number =  0, label:String = "", defaultHandler:Function = null)
		{
			super(parent, xpos, ypos, label, defaultHandler);
		}
		
		/**
		 * Initializes the component.
		 */
		override protected function init():void
		{
			super.init();
			buttonMode = true;
			useHandCursor = true;
			setSize(100, 20);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			_back = new Sprite();
			//_back.filters = [getShadow(2, true)];
			_back.mouseEnabled = false;
			//addChild(_back);
			
			_face = new Sprite();
			_face.mouseEnabled = false;
			//_face.filters = [getShadow(1)];
			_face.x = 0;
			_face.y = 0;
			addChild(_face);
			
			_label = new Label();
			addChild(_label);
			_label.textField.defaultTextFormat = TextFormats.ASAP_BOLD(15);
			//_label.leftMargin = -5;
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseGoDown);
			addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
		}
		
		/**
		 * Draws the face of the button, color based on state.
		 */
		override protected function drawFace():void
		{
			_face.graphics.clear();
			if(_down)
			{
				_face.graphics.beginFill(Colors.BLUE_LIGHT,.5);
			}
			else
			{
				_face.graphics.beginFill(Colors.BLUE_LIGHT);
			}
			_face.graphics.drawRect(0, 0, _width , _height );
			_face.graphics.endFill();
		}
		
		
		/**
		 * Draws the visual ui of the component.
		 */
		override public function draw():void
		{
			super.draw();
			_back.graphics.clear();
			//_back.graphics.beginFill(Style.BACKGROUND);
			_back.graphics.drawRect(0, 0, _width, _height);
			_back.graphics.endFill();
			
			drawFace();
			
			_label.text = _labelText;
			_label.autoSize = true;
			_label.draw();
			if(_label.width > _width - 4)
			{
				_label.autoSize = false;
				_label.width = _width - 4;
			}
			else
			{
				_label.autoSize = true;
			}
			_label.draw();
			_label.move(_width / 2 - _label.width / 2 , _height / 2 - _label.height / 2 - 4);
			
		}
		
		
		///////////////////////////////////
		// public methods
		///////////////////////////////////
		
		///////////////////////////////////
		// event handlers
		///////////////////////////////////
		
		/**
		 * Internal mouseOver handler.
		 * @param event The MouseEvent passed by the system.
		 */
		override protected function onMouseOver(event:MouseEvent):void
		{
			_over = true;
			addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
		}
		
		/**
		 * Internal mouseOut handler.
		 * @param event The MouseEvent passed by the system.
		 */
		override protected function onMouseOut(event:MouseEvent):void
		{
			_over = false;
			if(!_down)
			{
				//_face.filters = [getShadow(1)];
			}
			removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
		}
		
		/**
		 * Internal mouseOut handler.
		 * @param event The MouseEvent passed by the system.
		 */
		override protected function onMouseGoDown(event:MouseEvent):void
		{
			_down = true;
			drawFace();
			//_face.filters = [getShadow(1, true)];
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseGoUp);
		}
		
		/**
		 * Internal mouseUp handler.
		 * @param event The MouseEvent passed by the system.
		 */
		override protected function onMouseGoUp(event:MouseEvent):void
		{
			if(_toggle  && _over)
			{
				_selected = !_selected;
			}
			_down = _selected;
			drawFace();
			//_face.filters = [getShadow(1, _selected)];
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseGoUp);
		}
		
		
		
		
		///////////////////////////////////
		// getter/setters
		///////////////////////////////////
		
		/**
		 * Sets / gets the label text shown on this Pushbutton.
		 */
		override public function set label(str:String):void
		{
			_labelText = str;
			draw();
		}
		override public function get label():String
		{
			return _labelText;
		}
		
		override public function set selected(value:Boolean):void
		{
			if(!_toggle)
			{
				value = false;
			}
			
			_selected = value;
			_down = _selected;
			//_face.filters = [getShadow(1, _selected)];
			drawFace();
		}
		override public function get selected():Boolean
		{
			return _selected;
		}
		
		override public function set toggle(value:Boolean):void
		{
			_toggle = value;
		}
		override public function get toggle():Boolean
		{
			return _toggle;
		}
		
	}
}