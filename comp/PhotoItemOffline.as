﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp 
{
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	import data.PhotoVo;
	
	import offline.data.OfflinePhotoLoader;
	
	import utils.BitmapUtils;
	import utils.Debug;
	import utils.debug.TimeLog;

	public class PhotoItemOffline extends PhotoItem
	{
		// private icons
		private var offlineLoader:OfflinePhotoLoader;
		
		
		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		
		public function PhotoItemOffline(photoVo:PhotoVo) 
		{
			super(photoVo);
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////

		public override function updateThumbData( bmd : BitmapData ) :void
		{
			if(thumb.bitmapData) thumb.bitmapData.dispose();
			
			thumb.bitmapData = BitmapUtils.createThumbData(bmd);
			thumb.smoothing = true;
			bmd.dispose();
			
			_isLoaded = true;
			
			// place it correcly
			constraintIntoBounds();
		}
		
		
		/**
		 * ------------------------------------ INTRO / OUTRO -------------------------------------
		 */
		
		/**
		 * intro is the show method, when adding view on display list, we should directly call the intro method
		 */
		public override function intro():void
		{		
			this.visible = true;
			alpha = 1;
			mouseEnabled = false
				
			if(isLoaded) activate();
		}
		
		/**
		 * outro method should be called when hiding the view, it can be directly followed (after animation) by the destroy method if we need to clear the view
		 */
		public override function outro():void
		{			
			mouseEnabled = false;
			TweenMax.killTweensOf(this);
			if(container) TweenMax.killTweensOf(container);
			
			this.visible = false;
			this.alpha = 0;
		}
		
		
		
		/**
		 * ------------------------------------ DESTROY -------------------------------------
		 */
		
		/**
		 * ------------------------------------ LOAD THUMB From photo tab offline, so this is loaded in a queue -------------------------------------
		 */
		
		public function loadLocalThumb():void
		{
			// if we are already loading, do nothing
			if(isLoading) return;
			
			// security
			if(!thumbVo || !thumbVo.thumbUrl) {
				Debug.warn("PhotoItemOffline.loadLocalThumb has no thumbUrl");
				return;
			}
			
			// be sure to reset imageloader
			killImageLoader();
			
			// flags
			_isLoaded = false;
			isLoading = true;
			
			// load online
			offlineLoader = new OfflinePhotoLoader();
			offlineLoader.LOAD_SUCCESS.add(onImageLoadedHandler);
			offlineLoader.LOAD_FAIL.add(onImageLoadFail);
			offlineLoader.load((thumbVo as PhotoVo));	
		}
		
		/**
		 * ------------------------------------ LOAD EVENT OVERRIDES -------------------------------------
		 */

		private function onImageLoadedHandler():void
		{	
			// Debug.log("----> BYTES : " + TimeLog.log("LeftItemPreview"));
			var imageData:BitmapData = offlineLoader.image;
			
			// recover image
			imageData = offlineLoader.image;
			
			// update thumb data
			var thumbData:BitmapData = BitmapUtils.createThumbData(imageData, photoVo.exifRot);
			
			//Debug.log("----> BITMAPDATA : " + TimeLog.log("LeftItemPreview"));
			imageData.dispose();
			
			updateThumbData( thumbData );
			
			//Debug.log("----> THUMB : " + TimeLog.log("LeftItemPreview"));
			
			// remove image loader
			killImageLoader();
			
			// notify
			LOAD_COMPLETE.dispatch(this);
			
			// show image
			activate();
			
			// remove icon
			removeLoadingIcon();
		}
		private function onImageLoadFail(error:String):void
		{
			// notify
			Debug.warn("PhotoItemOffline.onImageLoadFail : "+error);
			LOAD_FAIL.dispatch(this, error);
			removeLoadingIcon();
			removeUploadingIcon();
			displayErrorState();
		}
		
		
		/**
		 * ------------------------------------ REMOVE ICONS -------------------------------------
		 */
		
		
		// dispose loader
		protected override function killImageLoader():void
		{
			if(offlineLoader) {
				offlineLoader.destroy();
			}
		}	
	}
	
}