﻿/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp 
{
	import be.antho.data.ResourcesManager;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.AutoAlphaPlugin;
	
	import data.PhotoVo;
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	import manager.DragDropManager;
	
	import mcs.icons.thumbDeleteIcon;
	import mcs.icons.thumbEditIcon;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;

	public class ManagerThumbItem extends PhotoItem
	{
		// signals
		public const DELETE : Signal = new Signal(ManagerThumbItem);
		public const EDIT : Signal = new Signal(ManagerThumbItem);
			
		// public
		
		// view
		private var deleteIcon : thumbDeleteIcon = new thumbDeleteIcon();
		private var editIcon : thumbEditIcon = new thumbEditIcon();
		
			
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function ManagerThumbItem(photoVo:PhotoVo) 
		{
			super(photoVo);
			this.thumbVo = photoVo
			
			draggable = true;
			
			/*
			graphics.beginFill(0xff0000, 0);
			//graphics.drawRect(0,0,50+ 50*Math.random(),HEIGHT);
			graphics.drawRect(0,0,bounds.width,bounds.height);
			graphics.endFill();
			*/
			buttonMode = true;
			mouseChildren = true;
			alpha = 0;
			
			// delete icon
			deleteIcon.x = Math.round(bounds.width - deleteIcon.width*.5);
			deleteIcon.y = Math.round(-deleteIcon.height*.5) + 2;
			ButtonUtils.makeButton(deleteIcon, deleteThumb);
			
			// edit icon
			editIcon.x = deleteIcon.x;
			editIcon.y = Math.round(bounds.height - editIcon.height*.5);
			ButtonUtils.makeButton(editIcon, editThumb);
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		
		
		/**
		 * Completely destroy the view, remove signals, events, references and remove from display list
		 */
		public override function destroy():void
		{		
			super.destroy();
			DELETE.removeAll();
			EDIT.removeAll();
			DOUBLE_CLICK.removeAll();
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 *
		 */
		private function deleteThumb(e:Event = null):void
		{
			DELETE.dispatch(this);
		}
		
		/**
		 *
		 */
		private function editThumb(e:Event = null):void
		{
			EDIT.dispatch(this);
		}
		
		
		override protected function overHandler(event:MouseEvent):void
		{
			mouseChildren = true;
			addChild(deleteIcon);
			addChild(editIcon);
			TweenMax.killTweensOf(container);
			TweenMax.to(container, .1, {glowFilter:{color:0x66a83e, blurX:18, blurY:18, strength:2, alpha:.5}});
			if(tooltip){DefaultTooltip.tooltipOnClip(this, tooltip);}
		}
		
		override protected function outHandler(event:MouseEvent):void
		{
			mouseChildren = false;
			if(contains(deleteIcon)) removeChild(deleteIcon);
			if(contains(editIcon)) removeChild(editIcon);
			TweenMax.killTweensOf(container);
			TweenMax.to(container, .1, {glowFilter:{color:0x66a83e, blurX:0, blurY:0, strength:0, alpha:0}, onComplete:function():void{container.filters = [];}});
			if(tooltip){DefaultTooltip.killAllTooltips()}
		}
		
		/**
		 * set thumb actions
		 */
		override protected function setListeners():void
		{	
			// inherited listeners
			super.setListeners();
		}
		
	}
	
}