package comp
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	
	import data.AssetVo;
	import data.Infos;
	import data.PhotoVo;
	import data.ThumbVo;
	
	import mcs.icons.thumbOnlineIcon;
	
	import utils.AssetUtils;
	import utils.Colors;
	
	public class AssetItem extends ThumbItem
	{
		protected static var onlineIconBitmapData : BitmapData;
		private var onlineIcon:Bitmap;
		
		//CONSTRUCTOR
		public function AssetItem(thumbVo:AssetVo,dragGroupType:String = null, _bgFillColor:uint = Colors.WHITE)
		{
			if(Infos.IS_DESKTOP && !onlineIconBitmapData){
				var uploadingIconClip : MovieClip = new mcs.icons.thumbOnlineIcon();
				onlineIconBitmapData = AssetUtils.drawClipBitmap(uploadingIconClip).bitmapData;
			}
			
			super(thumbVo,dragGroupType,_bgFillColor);
			
			
			if(Infos.IS_DESKTOP  && !IS_LOCAL_CONTENT)
			{
				// online icon
				onlineIcon = new Bitmap(onlineIconBitmapData);
				onlineIcon.x =  2;
				onlineIcon.y = Math.ceil(bounds.height - onlineIcon.height) + 4;
				addChild(onlineIcon);				
				
			}
		}
		
		
		/**
		 * check if content is a local content or an external content
		 */
		override public function get IS_LOCAL_CONTENT():Boolean
		{
			if( !Infos.IS_DESKTOP ) return false;
			
			return PhotoVo(thumbVo).isLocalContent;
		}
		
		/**
		 * ------------------------------------ PUBLIC Download assets locally -------------------------------------
		 */
		
		override public function startDownloadAsset():void
		{
			super.startDownloadAsset();
			if(onlineIcon)
			{
				TweenMax.to(onlineIcon, 1, {alpha:0.2, ease:Linear.easeNone, repeat:-1});
			}
		}
		
		override protected function downloadAssetsComplete():void
		{
			super.downloadAssetsComplete();
			if(onlineIcon)
			{
				TweenMax.killTweensOf(onlineIcon);
				TweenMax.to(onlineIcon, 1, {autoAlpha:0, ease:Linear.easeNone});
			}
		}
		
	}
}