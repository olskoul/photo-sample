package comp
{
	import com.bit101.components.ScrollPane;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import library.dropdown.btn;
	
	import org.osflash.signals.Signal;
	
	import utils.ButtonUtils;
	import utils.TextFormats;
	
	public class FontSelector extends Sprite
	{
		public var FONT_CHOSEN:Signal = new Signal();
		private var fontList:Array;
		private var fontSelected:Sprite;
		private var fontListContainer:ScrollPane;
		private var buttonsFonts:Object = {};
		private var defaultFont:String;
		private var openCloseBtn:MovieClip;
		
		public function FontSelector(fontList:Array, defaultFont:String = "")
		{
			this.fontList = fontList;
			this.defaultFont = defaultFont;
			construct();
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHOD
		////////////////////////////////////////////////////////////////
		public function setFont(fontName:String):void
		{
			var bitmap:Bitmap = fontSelected.getChildAt(0) as Bitmap;
			bitmap.bitmapData = TextFormats.getFontBitmapByName(fontName);
			bitmap.smoothing = true;
			fontSelected.y = openCloseBtn.height - fontSelected.height >>1;
		}
		
		/**
		 *
		 */
		public function close():void
		{
			if(fontListContainer.parent) fontListContainer.parent.removeChild(fontListContainer);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHOD
		////////////////////////////////////////////////////////////////
		/**
		 * Build VIEW
		 * */
		private function construct():void
		{

			//Selected font
			fontSelected = new Sprite();
			fontSelected.x = 0;
			fontSelected.y = 0;
			//set Default font
			defaultFont = (defaultFont!="")?defaultFont:fontList[0].fontName;
			var bitmap:Bitmap = new Bitmap(TextFormats.getFontBitmapByName(defaultFont));
			bitmap.smoothing = true;
			fontSelected.addChild(bitmap);
			addChild(fontSelected);
			
			//dropdown btn
			openCloseBtn = new library.dropdown.btn();
			openCloseBtn.x = fontSelected.x + fontSelected.width + 5;
			addChild(openCloseBtn);
			
			//drop down list
			fontListContainer = new ScrollPane(null, -10,fontSelected.height+14);
			fontListContainer.mouseWheelEnabled = true;
			fontListContainer.autoHideScrollBar = true;
			fontListContainer.showScrollButtons(false);
			fontListContainer.setSize(175, 100);
			for (var j:int = 0; j < fontList.length; j++) 
			{
				var fontItem:MovieClip = new MovieClip();
				fontItem.x = 10;
				fontItem.y = 30*j;
				fontItem.id = fontList[j].fontName;
				bitmap = new Bitmap(TextFormats.getFontBitmapByName(fontItem.id));
				bitmap.smoothing = true;
				fontItem.addChild(bitmap);
				fontListContainer.addChild(fontItem);
				buttonsFonts[fontItem.id] = fontItem;
				ButtonUtils.makeButton(fontItem,fontChosen);
			}
			
			//listeners
			ButtonUtils.makeButton(fontSelected,toggleFontlistVisiblity);
			ButtonUtils.makeButton(openCloseBtn,toggleFontlistVisiblity);
		}
		
		/**
		 * A font has been chosen in the drop down
		 * */
		private function fontChosen(e:MouseEvent):void
		{
			var fontItem:MovieClip = e.target as MovieClip;
			var bitmap:Bitmap = fontSelected.getChildAt(0) as Bitmap;
			bitmap.bitmapData = Bitmap(fontItem.getChildAt(0)).bitmapData;
			bitmap.smoothing = true;
			fontSelected.y = openCloseBtn.height - fontSelected.height >>1;
			toggleFontlistVisiblity();
			FONT_CHOSEN.dispatch(fontItem.id);
		}
		
		/**
		 *HIde or show dropdown
		 * */
		private function toggleFontlistVisiblity(e:MouseEvent = null):void
		{
			if(contains(fontListContainer))
				removeChild(fontListContainer);
			else
				addChild(fontListContainer);
		}
	}
}