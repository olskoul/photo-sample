package comp.label
{
	import com.bit101.components.Label;
	import com.bit101.components.Style;
	
	import flash.display.DisplayObjectContainer;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import utils.Colors;
	import utils.TextFormats;
	
	public class LabelTictac extends Label
	{
		private var fontSize:int;
		private var color:Number;
		private var hAlign:String;
		public function LabelTictac(parent:DisplayObjectContainer=null, xpos:Number=0, ypos:Number=0, text:String="", fontSize:int = 13, color:Number = Colors.GREY_DARK, hAlign:String = TextFormatAlign.LEFT )
		{
			this.fontSize = fontSize;
			this.color = color;
			this.hAlign = hAlign;
			super(parent, xpos, ypos, text);
		}
		
		/**
		 * Creates and adds the child display objects of this component.
		 */
		override protected function addChildren():void
		{
			_height = 13;
			_tf = new TextField();
			_tf.height = _height;
			_tf.embedFonts = Style.embedFonts;
			_tf.antiAliasType = AntiAliasType.ADVANCED;
			_tf.selectable = false;
			_tf.mouseEnabled = false;
			_tf.defaultTextFormat = TextFormats.ASAP_REGULAR(fontSize,color, hAlign);
			_tf.text = _text;			
			addChild(_tf);
			draw();
		}
		
		public function textFormatUpdate(textFormat:TextFormat):void
		{
			_tf.defaultTextFormat =textFormat;
			draw();
		}
		
		public function updateText(text:String):void
		{
			_tf.text = text;
			draw();
		}
		
		public function setBackgroundColor(color:Number):void
		{
			_tf.background = true;
			_tf.backgroundColor = color;
			draw();
		}
	}
}