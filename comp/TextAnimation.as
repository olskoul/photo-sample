/***************************************************************
 PROJECT 		: BELGACOM ECARD 2013
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/

package comp
{
	import be.antho.utils.DSprite;
	import be.antho.utils.MathUtils2;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import utils.Debug;
 
	public class TextAnimation
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////
		private var mc:MovieClip;
		private var txt:TextField;
		private var leading:Number;
		private var topOffset:Number;
		private var bottomOffset:Number;
		private var textFlyAtEnd:Boolean = false;
		
		private var txtBmd:BitmapData;
		private var letterList:Vector.<DSprite>; 
		private const bitmapLeading:Number = 100;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function TextAnimation($mc:MovieClip, textFlyAtEnd:Boolean = false, leading:Number = -20, topOffset:int = -10, bottomOffset:int = 0)
		{
			if($mc.bitmaps){
				Debug.error(" in TextAnimation, mc is already linked to a text animation, use reset instead of new constructor");
				return;
			}
			
			this.mc = $mc;
			this.leading = leading;
			this.topOffset = topOffset;
			this.bottomOffset = bottomOffset;
			this.textFlyAtEnd = textFlyAtEnd;
			
			txt = mc.txt;
			txt.autoSize = "center";
			
			
			Debug.log("CONSTRUCTOR CONSTRUCTOR CONSTRUCTOR");
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public function generateBitmapLetters():void
		{
			if(letterList) 
			{
				Debug.warn(" in TextAnimation, 'generateLetterBitmaps' has been called before calling 'reset' > automatic call reset");
				reset();
			}

			var tf:TextFormat = txt.getTextFormat();
			tf.leading = bitmapLeading; // use a bigger leading to be sure the letter doesn't overlap
			txt.setTextFormat(tf);
			
			
			// create letter list
			letterList = new Vector.<DSprite>();
			
			// generate txt bitmapdata
			txtBmd = new BitmapData(txt.width, txt.height-topOffset-bottomOffset, true, 0x000000);
			var txtMatrix:Matrix = new Matrix();
			txtMatrix.ty = -topOffset;
			txtBmd.draw(txt, txtMatrix);
			//mc.addChild(new Bitmap(txtBmd));
			
			var rect:Rectangle;
			var bmd:BitmapData;
			var bmp:Bitmap;
			var lineIndex:Number;
			
			//transformParagraphsToLineBreak();
			
			for( var i:uint, l:uint = txt.text.length ; i <l ; i++ )
			{
				// retrive char boundaries					
				rect = txt.getCharBoundaries(i);
				if(rect) // new line creates null rect.. good to know
				{
					lineIndex = txt.getLineIndexOfChar(i);
					rect.y = rect.y ;
					rect.height = rect.height - topOffset - bottomOffset;
					
					// create bitmap
					bmd = new BitmapData(rect.width, rect.height, true, 0x000000);
					bmd.copyPixels(txtBmd, rect, new Point());
					bmp = new Bitmap(bmd, "auto", true);
					
					
					// add to list
					var sp:DSprite = new DSprite();
					sp.addChild(bmp);
					
					// positionate bitmap
					sp.x = txt.x + rect.x;
					sp.y = txt.y + rect.y - lineIndex*bitmapLeading + leading;
					sp.initX = sp.x;
					sp.initY = sp.y;
					mc.addChild(sp);
					
					
					if(textFlyAtEnd)
					{
						sp.radius = MathUtils2.randomRange(4,5);
						sp.vx = MathUtils2.randomRange(4,5) / 100;
						sp.vy = MathUtils2.randomRange(4,5) / 100;
					}
					
					letterList.push(sp);
					
				}else{
					Debug.warn("char problem at index : "+i + " & value is :" +txt.text.charAt(i));
					//l += 1; // because special chars are not taken in the length .. strange 
				}
			}
			
			//
			mc.removeChild(txt);
			mc.bitmaps = letterList;
		}
		
		
		/*
		private function transformParagraphsToLineBreak():void
		{
			// transform here every htmlLineBreak to simple line break 
			var nextParagraphIndex:uint = txt.getParagraphLength(0); 
			var charIndex:uint = 0;

			for( var i:uint, l:uint = txt.text.length ; i <l ; i++ )
			{
				// check if the current char index is the paragaph index
				while(charIndex > (nextParagraphIndex-2))
				{
					charIndex ++;
					nextParagraphIndex
				}
				
		}
		*/
		
		/**
		 *
		 */
		public function show():void
		{
			if(!letterList) throw new Error("ERROR in TextAnimation, 'show' has been called before calling 'generateBitmaps'");
			mc.removeEventListener(Event.ENTER_FRAME, moveText);
			mc.visible = true;
			mc.alpha = 1;
			mc.cacheAsBitmap = false;
			var bmp:DSprite, startPoint:Point, endPoint:Point, rot:Number;
			for( var i:uint, l:uint = letterList.length ; i <l ; i++ )
			{
				bmp = letterList[i] as DSprite;
				endPoint = new Point(bmp.initX, bmp.initY);
				startPoint = new Point(bmp.initX + MathUtils2.randomRange(-300, 300), bmp.initY + MathUtils2.randomRange(-300, 300));
				rot = MathUtils2.randomRange(0,360)
				
				//TweenMax.to(bmp, 1.5,  {transformMatrix:{x:50, y:300, scaleX:2, scaleY:2}}); 
				bmp.visible = false;
				bmp.alpha = 1;
				var completeFunction:Function;
				if(i == (l-1)) completeFunction = showComplete;
				
				TweenMax.to(bmp, 1.2, 	{ 	delay:i*0.04,//delay:i*0.04,
											transformMatrix:{x:endPoint.x, y:endPoint.y, scaleX:1, scaleY:1, rotation:0}, 
											ease:Strong.easeOut, 
											tint:null,
											startAt:{ visible:true, tint:0xffffff, transformMatrix:{ scaleX:3, scaleY:3, x:startPoint.x, y:startPoint.y, rotation:rot}},
											onComplete:completeFunction
				});
			}
		}
		
		/**
		 *
		 */
		public function hide():void
		{
			if(!letterList) throw new Error("ERROR in TextAnimation, 'hide' has been called before calling 'generateBitmaps'");
			mc.visible = true;
			mc.alpha = 1;
			mc.cacheAsBitmap = false;
			var bmp:DSprite, startPoint:Point, endPoint:Point, rot:Number;
			for( var i:uint, l:uint = letterList.length ; i <l ; i++ )
			{
				bmp = letterList[i] as DSprite;
				endPoint = new Point(bmp.x, bmp.y);
				startPoint = new Point(bmp.x + MathUtils2.randomRange(-300, 300), bmp.y + MathUtils2.randomRange(-300, 300));
				rot = MathUtils2.randomRange(-100,100)
				
				TweenMax.to(bmp, 1, 	{ 	delay:i*0.05,
											autoAlpha:0,
											transformMatrix:{x:startPoint.x, y:startPoint.y, scaleX:0, scaleY:0, rotation:rot}, 
											ease:Strong.easeIn
							});
			}
		}
		
		
		/**
		 *
		 */
		public function reset(newText:String = null):void
		{
			mc.removeEventListener(Event.ENTER_FRAME, moveText);
			
			var bmp:DSprite;
			for( var i:uint=0, l:uint = letterList.length ; i <l ; i++ )
			{
				bmp = letterList[i] as DSprite;
				mc.removeChild(bmp);
			}

			letterList = null;
			mc.addChild(txt);
			if(newText){
				txt.htmlText = newText;
			}
			mc.bitmaps = null;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function showComplete():void
		{
			if(textFlyAtEnd)
			{
				mc.addEventListener(Event.ENTER_FRAME, moveText);
			}
		}
		
		/**
		 *
		 */
		protected function moveText(event:Event):void
		{
			var bmp:DSprite, startPoint:Point, endPoint:Point, rot:Number;
			var rad:Number;
			for( var i:uint, l:uint = letterList.length ; i <l ; i++ )
			{
				bmp = letterList[i] as DSprite;
				rad = (bmp.radius >>1);
				if(Math.abs(bmp.x -bmp.initX) > rad ) {
					bmp.x = bmp.initX + rad * bmp.vx / Math.abs(bmp.vx);
					bmp.vx = -bmp.vx;
				}
				if(Math.abs(bmp.y -bmp.initY) > rad){
					bmp.y = bmp.initY + rad * bmp.vy / Math.abs(bmp.vy);
					bmp.vy = -bmp.vy;
				}
				bmp.y += bmp.vy;
				bmp.x += bmp.vx;		
			}		
		}
	}
}