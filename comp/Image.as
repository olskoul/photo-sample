package comp
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	import utils.AssetUtils;
	
	public dynamic class Image extends Sprite
	{
		public var bp:Bitmap;
		
		/**
		 *  image accept asset of type String, Class, BitmapData, Bitmap, Sprite
		 */
		public function Image( asset:*, center:Boolean = true )
		{
			super();
			if(asset is String) bp = AssetUtils.getBitmap(asset); 
			else if(asset is BitmapData) bp = new Bitmap( asset, "auto", true );
			else if(asset is Bitmap) bp = asset;
			else bp = new Bitmap( new asset(), "auto", true );
			
			if ( center ) {
				bp.x = - bp.width >> 1;
				bp.y = - bp.height >> 1;
			}
			addChild( bp );
		}
		
		/**
		 *
		 */
		public function destroy():void
		{
			if(bp && bp.bitmapData){
				bp.bitmapData.dispose();
			}
			if(bp && contains(bp)) removeChild(bp);
			bp = null;
		}
	}
}