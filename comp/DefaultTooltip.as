/***************************************************************
 PROJECT 		: TicTac
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package comp
{
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import be.antho.data.ResourcesManager;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	import br.com.stimuli.loading.loadingtypes.LoadingItem;
	
	import comp.button.SimpleButton;
	import comp.button.SimpleSquareButton;
	
	import data.TooltipVo;
	
	import offline.manager.LocalStorageManager;
	
	import utils.Debug;
	import utils.TextFormats;

	public class DefaultTooltip extends Sprite
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// stage ref
		public static var STAGE : DisplayObjectContainer;
		private static const tooltipContentLoader : BulkLoader = new BulkLoader("tooltipContentLoader");
		
		
		// ui
		private var bg : Sprite;
		private var arrow : Sprite;
		private var container : VBox;
		
		// data
		private var vo: TooltipVo;
		private var openPos : Point;
		private var isOver : Boolean = false;
		private var isOpen : Boolean = false;
		
		private var contentList : Array;
		private var shadow: DropShadowFilter = new DropShadowFilter(2,90,0,0.3, 5,5);
		private var arrowWidth : int = 15;
		private var maxWidth : Number = 300;
		private var localLoadList : Array;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function DefaultTooltip( $vo:TooltipVo = null, _maxWidth:Number = 300 )
		{
			maxWidth = _maxWidth;
			vo = $vo;
			if(!vo) vo = new TooltipVo();
			if(!isNaN(vo.maxWidth))maxWidth = vo.maxWidth;
			
			// try construct
			try{ construct(); }
			catch(e:Error)
			{
				Debug.warn("Error while creating tooltip : "+e.toString());
				destroy();
				return;
			}
			
			filters = [shadow];
			
			// ref in global tooltips
			tooltips.push(this); 
		}
		
		////////////////////////////////////////////////////////////////
		//	STATIC PART
		////////////////////////////////////////////////////////////////
		
		private static var tooltips:Vector.<DefaultTooltip> = new Vector.<DefaultTooltip>();
		
		// helper for quick access to tooltip creation
		public static function tooltipOnClip( clip:DisplayObject, vo:TooltipVo, center:Boolean = true, _maxWidth:Number = 250, $offSet:Point = null  ):void
		{
			var tt : DefaultTooltip = new DefaultTooltip(vo,_maxWidth);
			tt.openOnClip(clip, center, $offSet);
		}
		
		// helper for quick access to tooltip destruction
		public static function killAllTooltips():void
		{
			// destroy tooltip loader
			tooltipContentLoader.removeAll();
			
			for (var i:int = 0; i < tooltips.length; i++) 
			{
				var tt:DefaultTooltip = tooltips[i];
				if(!tt.vo.persistOnKill) tt.close();
			}
		}
		
		public static function killTooltipById(id:String):void
		{
			// destroy tooltip loader
			if(tooltipContentLoader) tooltipContentLoader.removeAll();
			
			for (var i:int = 0; i < tooltips.length; i++) 
			{
				var tt:DefaultTooltip = tooltips[i];
				if(tt.vo.uid == id) tt.close();
			}
		}
		
		public static function killTooltipByType(type:String):void
		{
			// destroy tooltip loader
			if(tooltipContentLoader) tooltipContentLoader.removeAll();
			
			for (var i:int = 0; i < tooltips.length; i++) 
			{
				var tt:DefaultTooltip = tooltips[i];
				if(tt.vo.type == type) tt.close();
			}
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * open at precise point
		 */
		public function open( $pos : Point, offset:Point = null ):void
		{
			if(!offset) offset = new Point(0,0);
			visible = false;
			mouseEnabled = mouseChildren = vo.interactive; // by default tooltips are open and closed by caller
			
			var openingPos : Point = new Point($pos.x, $pos.y); 
			
			// checks where to open tooltip
			var contentWidth : Number;
			var contentHeight:Number;
			if( vo.arrowType == TooltipVo.ARROW_TYPE_HORIZONTAL )
			{
				contentWidth = bg.width + arrow.width;
				contentHeight = bg.height;
				// check horizontal
				if(openingPos.x>STAGE.stage.stageWidth - contentWidth){ 
					arrow.x = contentWidth;
					arrow.scaleX = -1;
				} 
				else {
					arrow.x = -arrow.width;
					offset.x = -offset.x
				}
				
				// check vertical
				if(openingPos.y + contentHeight > STAGE.stage.stageHeight )
				{ 
					// be sure to not go off screen
					var diff:Number = openingPos.y + contentHeight - STAGE.stage.stageHeight;
					arrow.y = diff + arrow.height;					
				}
				else arrow.y = arrow.height;
				if(arrow.y + arrow.height*.5 > bg.height) arrow.y = bg.height-arrow.height*.5;// security for goodlooking tooltip
			} 
			else
			{
				contentWidth = bg.width;
				contentHeight = bg.height+arrow.height;
				// check vertical
				if(openingPos.y - contentHeight < 0 )
				{ 
					arrow.scaleY = -1;
					arrow.y = - arrow.height;
					offset.y = -offset.y;
				}
				else arrow.y = bg.height+arrow.height;
				
				// check horizontal
				if(openingPos.x>STAGE.stage.stageWidth - contentWidth){ 
					arrow.x = bg.width-arrow.width;
				} 
				else arrow.x = arrow.width;
			}
			
			// place tooltip at arrow pos
			openingPos.x -= arrow.x + offset.x;
			openingPos.y -= arrow.y + offset.y;
			
			// force pixel perfect position
			openingPos.x = Math.round(openingPos.x);
			openingPos.y = Math.round(openingPos.y);
			
			
			// for better perfs
			cacheAsBitmap = true;
			openPos = openingPos;
			
			// show
			TweenMax.killTweensOf(this);
			TweenMax.to(this, .3, { ease : Strong.easeOut, delay:.2, x:openPos.x, y:openPos.y, autoAlpha:1, startAt:{autoAlpha :0, y:openPos.y - 10, x:openPos.x} }); //, onComplete:openCompleted});
			
			// check if out of zone
			//this.addEventListener(MouseEvent.ROLL_OUT, onOut);
			//this.addEventListener(MouseEvent.ROLL_OVER, onOver);
			
			if( vo.autoHide != -1 && vo.autoHide > 0 ){
				TweenMax.delayedCall(vo.autoHide, close);
			}
			
			
			if(!parent) STAGE.addChild(this);				
		}
		
		
		/**
		 * open on specific clip
		 * center means the opening point is on the center of the clip
		 * otherwise it uses point (0.0)
		 */
		public function openOnClip( clip:DisplayObject, center:Boolean = true, $offSet:Point = null):void
		{
			// security
			if(!clip || !clip.parent) return;
			
			var clipPos : Point = new Point(clip.x, clip.y);
			
			// center
			if(center)
			{
				clipPos.x = clip.x + clip.width*.5;
				clipPos.y = clip.y + clip.height*.5;
			}
			
			clipPos = clip.parent.localToGlobal(clipPos);
			clipPos = globalToLocal(clipPos);
			
			// offset
			var offset:Point = ($offSet)?$offSet:new Point(0,0);
			if(vo.arrowType == TooltipVo.ARROW_TYPE_HORIZONTAL) offset.x += clip.width*.5;
			else offset.y += clip.height*.5;
			open(clipPos, offset);
		}
		
		
		/**
		 * close current popup
		 */
		public function close(e:Event = null):void
		{
			// remove loader listenersmo
			if(tooltipContentLoader){
				tooltipContentLoader.removeEventListener(BulkProgressEvent.COMPLETE, onImageLoaded);
				tooltipContentLoader.removeEventListener(BulkLoader.ERROR, onImageFail);
				tooltipContentLoader.removeAll();
			}
			/*
			this.removeEventListener(MouseEvent.ROLL_OUT, onOut);
			this.removeEventListener(MouseEvent.ROLL_OVER, onOver);
			stage.removeEventListener(MouseEvent.CLICK, close);
			*/
			
			mouseEnabled = false;
			isOver = false;
			isOpen = false;
			TweenMax.killTweensOf(this);
			//TweenMax.to(this, .4, {ease : Strong.easeOut, y:openPos.y - 20, autoAlpha:0, onComplete:destroy});
			
			destroy();
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * construct tooltip based on vo
		 */
		private function construct():void
		{
			Debug.log("Default tooltip.construct : "+vo.toString());
			
			// draw bg
			bg = new Sprite();
			bg.graphics.beginFill(vo.bgColor, vo.bgAlpha);
			bg.graphics.drawRect(0,0,10,10);
			bg.graphics.endFill();
			addChild(bg);
			
			// draw arrow
			arrow = new Sprite();
			arrow.graphics.beginFill(vo.bgColor, 1);
			if(vo.arrowType == TooltipVo.ARROW_TYPE_VERTICAL)
			{
				arrow.graphics.lineTo(-arrowWidth,-arrowWidth);
				arrow.graphics.lineTo(arrowWidth,-arrowWidth);
			}
			else 
			{
				arrow.graphics.lineTo(arrowWidth,-arrowWidth);
				arrow.graphics.lineTo(arrowWidth,arrowWidth);
			}
			arrow.graphics.lineTo(0,0);
			arrow.graphics.endFill();
			addChild(arrow);
			
			// add content
			container = new VBox();
			container.x = vo.margin;
			container.y = vo.margin;
			addChild(container);
			
			contentList = new Array();
			// content type
			if(!vo.content){ // if no special content, add default textfield
				addLabel(vo.labelKey, vo.additionalUntranslatedContent, null, vo.labelIsNotKey);
			} else {
				var obj:Object;
				var hasImage:Boolean = false;
				for (var i:int = 0; i < vo.content.length; i++) 
				{
					obj = vo.content[i];
					if(obj.type == "spacer") 
					{
						addSpacer(obj.spacerHeight);
					}
					if(obj.type == "text") 
					{
						if(obj.value is Array)
							addLabel(obj.value[0],obj.value[1], null, vo.labelIsNotKey);
						else
							addLabel(obj.value, null, null, vo.labelIsNotKey);
					}
					if(obj.type == "image") {
						addImage(obj.value, obj.width, obj.height, obj.rotation);
						hasImage = true;
					}
					if(obj.type == "button") addButton(obj.value, obj.handler, obj.fixWidth, (obj.iconIndex)?obj.iconIndex:-1, obj.handlerParam);
					if(obj.type == "buttonDelete") addDeleteButton(obj.value, obj.handler);
				}
				
				// retrieve image
				if(hasImage && vo) {
					tooltipContentLoader.addEventListener(BulkProgressEvent.COMPLETE, onImageLoaded);
					tooltipContentLoader.addEventListener(BulkLoader.ERROR, onImageFail);
					tooltipContentLoader.start();
				}
			}
			
			
				
			// update positions
			bg.width = container.width + 2*vo.margin;
			bg.height = container.height + 2*vo.margin;
		}
		
		
		/**
		 * add spacer
		 */
		private function addSpacer( height:int = 5):void
		{
			var spacer : Sprite = new Sprite();
			spacer.graphics.beginFill(0,0);
			spacer.graphics.drawRect(0,0,height,height);
			container.addChild(spacer);
			contentList.push(spacer);
		}
		
		/**
		 * add a label to the tooltip
		 */
		private function addLabel( labelKey:String, additionalUntranslatedContent:String = null, tf:TextFormat = null, labelIsNotKey:Boolean = false):void
		{
			var label : TextField = new TextField();
			label.autoSize = "left";
			label.embedFonts = true;
			label.antiAliasType = AntiAliasType.ADVANCED;
			if(!tf) tf = TextFormats.DEFAULT_TOOLTIP(12, vo.textColor);
			label.defaultTextFormat = tf;
			var fullcontent:String;
			if(!labelIsNotKey)
				fullcontent = (additionalUntranslatedContent && additionalUntranslatedContent!= "")?ResourcesManager.getString(labelKey) + additionalUntranslatedContent:ResourcesManager.getString(labelKey);
			else
				fullcontent = (additionalUntranslatedContent && additionalUntranslatedContent!= "")? labelKey + additionalUntranslatedContent: labelKey;
				
			label.htmlText = fullcontent;
			if(label.width > maxWidth-2*vo.margin) {
				label.width = maxWidth-2*vo.margin;
				label.wordWrap = true;
			}
			container.addChild(label);
			contentList.push(label);
		}
		
		/**
		 * add image in the tooltip
		 */
		private function addImage( url:String, w:Number, h:Number, imageRotation:Number = 0):void
		{
			// security
			if(w ==0 || h==0) {
				Debug.warn("DefaultTooltip.addImage > bitmap error : w = "+w+" & h="+h);
				return;
			}
			
			// image size given is for fullsize, we need the one for working image
			if(w>h && w>720)
			{
				h = (720/w)*h;
				w = 720;
			}
			else if (h>w && h>720)
			{
				w = (720/h)*w;
				h = 720;
			}
			
			var imageScale : Number;
			if(w >= maxWidth)
				imageScale = (maxWidth-2*vo.margin) / w;
			else
				imageScale = 1;
			
			// image rotation
			if(isNaN(imageRotation)) imageRotation = 0;
			
			// prepare bitmap data, if there is an image rotation invert w and h for calculation
			var bmd:BitmapData = ( imageRotation == 90 || imageRotation == -90 )? new BitmapData(h, w, false, 0x000000) : new BitmapData(w, h, false, 0x000000);
			var image : Bitmap = new Bitmap(bmd,"auto", false);
			image.scaleX = image.scaleY = imageScale;
			
			// if there is an image rotation, rotate and move image inside another container to let the VBox continue it's auto layout
			if(imageRotation != 0)
			{
				var imageContainer:Sprite = new Sprite();
				image.rotation = imageRotation;
				
				if(imageRotation == 90) image.x = w*imageScale;
				else if(imageRotation == -90) image.y = h*imageScale;
				else //180 
				{
					image.x = w*imageScale;
					image.y = h*imageScale;
				}
				
				imageContainer.addChild(image);
				container.addChild(imageContainer);
			}
			else container.addChild(image);
			
			
			// add content to tooltip
			contentList.push(image);
			
			// 
			/*
			if(vo){
				if(!vo.isLocal) tooltipContentLoader.add(url, {id:(contentList.length-1), type:BulkLoader.TYPE_IMAGE});
				else{
					LocalStorageManager.instance.loadLocalImage(url, null, null, bmd );
				}
			}
			*/
			if(vo) tooltipContentLoader.add(url, {id:(contentList.length-1), type:BulkLoader.TYPE_IMAGE});
		}
		
		
		/**
		 * add button in the tooltip
		 */
		private function addButton( label:String, handler:Function, fixWidth:Number, iconIndex:int = -1, handlerParam:* = null):void
		{
			var btn : SimpleButton;
			if(!iconIndex || iconIndex == -1)
			{
				btn = SimpleButton.createWhiteButton(label, handler);
				if(handlerParam)
					btn.handlerParam = handlerParam
			}
			else
			{
				btn = SimpleSquareButton.createBasicToolTipBtn(label,handler,iconIndex);
			}
			if(fixWidth)
			btn.forcedWidth = fixWidth;
			container.addChild(btn);
			contentList.push(btn);
		}
		
		/**
		 * add RED button in the tooltip
		 */
		private function addDeleteButton( label:String, handler:Function):void
		{
			var btn : SimpleButton = SimpleButton.createDeleteButton(label, handler);
			container.addChild(btn);
			contentList.push(btn);
		}
		
		
		
		/**
		 * 
		 */
		private function onImageLoaded( e:BulkProgressEvent ):void
		{
			// place image on bitmap
			for (var i:int = 0; i < tooltipContentLoader.items.length; i++) 
			{
				var loadedItem : LoadingItem = tooltipContentLoader.items[i] as LoadingItem;
				var index:int = parseInt(loadedItem.id); 
				var loadedBitmap : BitmapData = tooltipContentLoader.getBitmapData(index, true);
				var bitmap: Bitmap = (contentList[index] as Bitmap);
				var imageScale : Number;
				
				// as overriding bitmapdata inside a bitmap reset the scale, we re-calculate here the bitmap scale (+rotation if exif rotation for offline)
				var loadedWidth:Number = (bitmap.rotation == 90 || bitmap.rotation == -90 )? loadedBitmap.height : loadedBitmap.width; 
				if(loadedWidth >= maxWidth)
					imageScale= (maxWidth-2*vo.margin) / loadedWidth;
				else
					imageScale = 1;
				
				
				bitmap.bitmapData = loadedBitmap;
				bitmap.scaleX = bitmap.scaleY = imageScale;
				bitmap.smoothing = true;
			}
				
		}
		
		/**
		 * 
		 */
		private function onImageFail( e:ErrorEvent ):void
		{
			Debug.warn("Tooltip image fail : "+e.text);
		}
		
		
		/**
		 * destroy must be done using close
		 */
		private function destroy():void
		{
			// remove tooltip from list
			for (var i:int = 0; i < tooltips.length; i++) 
			{
				if(tooltips[i] == this){
					tooltips.splice(i,1);
					i--;
				}
			}
			// dispose possible images
			if( contentList){
				var item : DisplayObject;
				for (var j:int = 0; j < contentList.length; j++) 
				{
					item = contentList[j] as DisplayObject;
					if(item is Bitmap){
						(item as Bitmap).bitmapData.dispose();
					} 
				}
			}
			
			// remove autohide if there is one
			TweenMax.killDelayedCallsTo(close);
			
			// remove tooltip from displaylist
			if(this.parent) this.parent.removeChild(this);
		}
	}
}