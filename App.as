/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package 
{
	import com.bit101.components.Label;
	import com.google.analytics.AnalyticsTracker;
	import com.google.analytics.GATracker;
	import com.google.analytics.core.TrackerMode;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.ScalePlugin;
	import com.greensock.plugins.ScrollRectPlugin;
	import com.greensock.plugins.TransformMatrixPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	
	import be.antho.air.FileUtils;
	import be.antho.data.SharedObjectManager;
	import be.antho.utils.DataLoadingManager;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import comp.DefaultTooltip;
	import comp.PanelAbstract;
	
	import data.Infos;
	
	import manager.FolderManager;
	import manager.NewsManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	import manager.TutorialManager;
	import manager.UserActionManager;
	
	import mcs.dataLoadingClip;
	
	import net.hires.debug.Stats;
	
	import offline.ProjectPatcher;
	import offline.view.LangChoice;
	import offline.view.TermsAndConditions;
	
	import service.ServiceManager;
	
	import utils.Analytic;
	import utils.Colors;
	import utils.CursorManager;
	import utils.Debug;
	import utils.KeyboardManager;
	import utils.PrintColorsManager;
	import utils.TextFormats;
	
	import view.dashboard.Dashboard;
	import view.edition.EditionArea;
	import view.edition.imageEditor.ImageEditorModule;
	import view.menu.lefttabs.LeftTabs;
	import view.menu.topmenu.TopBar;
	import view.popup.PopupAbstract;
	import view.popup.PopupError;

	
	
	// OFFLINE only import
	CONFIG::offline
	{
		import flash.desktop.NativeApplication;
		import offline.manager.OfflineAssetsManager;
		import manager.ApplicationUpdateManager;
	}
	

	public class App extends Sprite 
	{
		/**
		 * ------------------------------------ PROPERTIES -------------------------------------
		 */
		
		public static var instance : App;  
		public static const WIDTH : uint = 811;
		public static const HEIGHT : uint = 640;
		
		// vars
		private var stats:Stats;
		private var configLoader:BulkLoader;
		
		// ui
		private var rootView : Sprite;
		private var topBar:TopBar;
		private var leftMenu : LeftTabs;
		private var editionArea : EditionArea;
		private var errorPopup:PopupError;
		private var showDashboard:Boolean;
		private var versionInfoLabel:Label;

		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		/**
		 * IMPORTANT DEVELOPER NOTICE : 
		 * > When something needs to be done later but we know already what to do insert a TODO comment with rapid description. 
		 * > On publishing online or on test servers, update Info version value.
		 * > When publishing, check all Debug variables in the Debug class.
		 * > When specific note for other developer add a note TODO @antho or TODO @jon (name of developer) 
		 * > MOST IMPORTANT NOTE > Comment every function of the code ! (use textExpander like application to create easy snippets)
		 * > This notice can be updated with cool/great/intelligent ideas to ease team developement !
		 */
		public function App():void 
		{
			// Tweenmax special plugins activation
			TweenPlugin.activate([GlowFilterPlugin, TransformMatrixPlugin, ScalePlugin, ScrollRectPlugin]); //MotionBlurPlugin
			
			// app instance for global shortcuts
			instance = this;
			Infos.root = this;
			
			// show dashboard only if this is the first time we use the app
			showDashboard = true ; //Infos.isFirstUse; --> always show dashboard at start 
			
			
			// listen to added event
			addEventListener(Event.ADDED_TO_STAGE, init);
			
			// init config loader 
			configLoader = new BulkLoader("Configuration");
			
			// init textFormats
			TextFormats.init();
				
		}
		
		
		/**
		 * ------------------------------------ APP STATIC SHORTCUTS -------------------------------------
		 */
		//	SHORTCUTS
		//	> shortcuts are static top application function easily accessed using App.nameOfTheFunction()
		//	> do not use this too often but sometimes is really usefull !
		// 	> var instance is private so we can't access from anywhere in the code, we must create public functions to use it
		
		
		/**
		 * reload application
		 * Only Online
		 */
		public function reload(params:Object = null):void 
		{
			CONFIG::online
			{ 
				navigateToURL(new URLRequest("javascript:location.reload();"),"_self");
			}
				
			CONFIG::offline
			{
				NativeApplication.nativeApplication.exit();
			}	
		}
			
		/**
		 * ResetViews
		 * Reload does not work for desktop, so wee need to reset/dispose views
		 * Tell view to do what they need to do 
		 * They need to be ready to catch the process either on PROJECT_READY or PROJECT_NO_PROJECT_READY signal dispatch
		 */
		public function disposeViews() :void
		{
			hide();
			leftMenu.dispose();
			topBar.update();
			editionArea.dispose();
			//Dashboard.instance.update();
			
		}
		
		
		/**
		 * ------------------------------------ APP INITIALIZATION -------------------------------------
		 */
		
		/**
		 *  Application init
		 */
		private function init(e:Event):void 
		{	
			// remove listener
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// stage init
			stage.frameRate = Infos.framerate;
			Infos.stage = stage;
			//stage.align = StageAlign.TOP_LEFT
			//stage.scaleMode = StageScaleMode.NO_SCALE;
			
			
			// init google analytic (only for non dev sessions)
			if(!Debug.CONSOLE)
			{
				var googleID : String = (Infos.IS_DESKTOP)? "UA-56676170-1" : "UA-56676170-3";
				Analytic.init(this,googleID,false);
				Analytic.trackPage("HOME");
				var os:String = Capabilities.os;
				Analytic.trackEvent("OS","run",os);
				Analytic.trackEvent("OS", "build", "" + Infos.REBRANDING_ID +"_"+ Infos.buildString);
			}
			
			// init popup system
			PopupAbstract.stageRef = stage;
			
			// init debug
			Debug.init(this);
			
			// init panel and tooltip global container
			PanelAbstract.STAGE = stage;
			DefaultTooltip.STAGE = stage;
			
			// init general data loading clip
			var dataLoading:MovieClip = new dataLoadingClip();
			addChild(dataLoading);
			var defaultText : String = (Infos.lang == "FR")? "Chargement..." : "Loading...";
			DataLoadingManager.instance.init(stage, dataLoading, defaultText);
			DataLoadingManager.instance.showLoading(false,null,"Setup...",true);
			
			//Init Cursor Manager
			CursorManager.instance.init(stage);
			
			//  Init KeyboardManager
			KeyboardManager.instance.init(stage);
			
			// load config file
			TweenMax.delayedCall(2,loadConfig);
			
			/////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////
			// TODO : comment those lines 
			// helper/generator for print colors (comment it if not needed, we don't want to embed a bitmap for nothing, see PrintColorManager)
			
			/*
			if(Debug.GENERATE_PRINT_COLORS) 
				PrintColorsManager.generateAndTracePaletteColorList();
			*/
			
			
			if(Debug.GENERATE_PRINT_COLORS_FROM_CSV)
				PrintColorsManager.generateAndTracePrintRgbColorList();
			
			
			/////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////
			
			
			//generate color palette
			if(!Debug.USE_OLD_COLOR_SYSTEM) 
				Colors.PALETTE = PrintColorsManager.generateColorPalette();
		}
		
		/**
		 * ------------------------------------ CONFIG XML LOAD -------------------------------------
		 */
		
		private function loadConfig():void
		{
			configLoader.addEventListener(BulkProgressEvent.COMPLETE, onConfigLoadSuccess);
			configLoader.addEventListener(BulkLoader.ERROR, onConfigLoadError);
			configLoader.add( Infos.configURL + "?t=" + new Date().time , { id:"config" });
			configLoader.start();
		}
		private function onConfigLoadError(e:ErrorEvent):void
		{
			Debug.log("App.onConfigLoadError (app is broken) : "+e.toString())
			Debug.error("App.onConfigLoadError : "+e.toString());
			DataLoadingManager.instance.hideLoading();
		}
		private function onConfigLoadSuccess(e:Event):void
		{
			// remove listeners
			configLoader.removeEventListener(BulkLoader.ERROR, onConfigLoadError);
			configLoader.removeEventListener(BulkProgressEvent.COMPLETE, onConfigLoadSuccess);
			
			var configXML:XML = configLoader.getXML("config", true);
			
			//DESKTOP ONLY : Copy freshly used config file locally
			CONFIG::offline
			{
				//Write synchroneously, small file
				FileUtils.writeTextFile(configXML.toString(),File.applicationStorageDirectory.resolvePath("config.xml"));
			}
			
			// fill config
			Infos.config.fill(configXML);
			
			// dispose loader
			configLoader.clear();
			configLoader = null;
			
			// init service manager
			ServiceManager.instance.init();
			
			//DESKTOP ONLY : Copy mandatory files to :app-storage if not done already.
			CONFIG::offline
			{
				OfflineAssetsManager.MANDATORY_FILES_COPIED.addOnce(checkAppStatus);
				OfflineAssetsManager.instance.copyMandatoryFiles();
			}
			
			//online
			CONFIG::online
			{
				checkAppStatus();
			}			
		}
		
		/**
		 * ------------------------------------ APPLICATION STATS and STATUS CHECK -------------------------------------
		 */
		
		/**
		 * check app validity online
		 * this is done only once
		 */
		private function checkAppStatus():void
		{
			// check app status
			if(!Debug.NO_STATS_SERVICE)
			{
				ServiceManager.instance.checkAppStatus(function(result:Object):void
				{
					Debug.NO_STATS_SERVICE = true; // be sure to call this only once by session
					try{
						if(new XML(result).status =="KO"){
							Debug.error("Check app stats KO");
						}	
					} catch (e:Error)
					{
						Debug.warn("Error with stats service : "+e.message);
					}
				},function(msg:String = null):void{
					Debug.warn("Error while reaching stats service : "+msg);
				});	
			}
			
			// init project
			startSession();
		}
		
		
		/**
		 * ------------------------------------ CHECK AND START SESSION -------------------------------------
		 */
		
		private function startSession():void
		{
			Debug.log("SessionManager.startSession");
			
			//listen for session ready signal
			SessionManager.SESSION_READY.addOnce(langAndSessionReady);

			if(!Infos.IS_DESKTOP)
			{
				//Init session
				SessionManager.instance.checkSession();
			}
			else
			{
				// load cookie general prefs
				Infos.prefs.loadGeneralPrefs();
				if(Infos.prefs.projectToOpenClassname && Debug.FORCE_TYPE == null)
					SessionManager.instance.session.classname = Infos.prefs.projectToOpenClassname;
				
				
				// case rebranding have a forced lanruage at start (no initial lanruage choice = forced general conditions in one lanruage)
				if(Infos.config.isRebranding && Infos.config.rebrandingForcedLanguage)
				{
					// check if user did already accept conditions, if yes, do not force language unless no language at all
					var conditionsAccepted : Boolean = SharedObjectManager.instance.read(SharedObjectManager.KEY_TERMS);
					if( ( !conditionsAccepted || Debug.FORCE_TERMS_AND_CONDITIONS ) // we must display conditions
						|| !Infos.prefs.lang ) //no language selected at all 
					{
						// FORCE LANG 
						Debug.log("FORCE LANGUAGE : "+Infos.config.rebrandingForcedLanguage);
						Infos.prefs.lang = Infos.config.rebrandingForcedLanguage;
					}
				}
					
					
				if(Infos.prefs.lang)
				{
					Debug.log("App.startSession : Infos.prefs.lang is set");
					SessionManager.instance.session.lang = Infos.prefs.lang;
					SessionManager.instance.loadLangResources();
				}
				else
				{
					DataLoadingManager.instance.hideLoading();
					var langChoice:LangChoice = new LangChoice();
					addChild(langChoice);
					langChoice.LANG_CHOSEN.add(function(chosenLang:String):void
					{
						Debug.log("App.startSession : LANG_CHOSEN signal triggered");
						Infos.prefs.lang = chosenLang;
						Infos.prefs.saveGeneralPrefs();
						SessionManager.instance.session.lang = chosenLang;
						SessionManager.instance.loadLangResources();
					});
				}
			}
		}
		
		
		/**
		 * ------------------------------------ INIT CONTENT (views and project) -------------------------------------
		 */
		
		private function langAndSessionReady():void
		{
			// create initial content
			constructChildren() ;
			
			// lang and session ready, we can start of online editor
			CONFIG::online
			{
				// online : init project manager
				initProject();
			}
			
			// for offline, we check first if there is an update available
			CONFIG::offline
			{
				// check app build update and content file update
				ApplicationUpdateManager.CONTENT_UPDATE_COMPLETE.add(updateCheckComplete);
				ApplicationUpdateManager.instance.checkForAppBuildUpdate( true );
			}
		}
			
		
		/**
		 * ------------------------------------ update check done -------------------------------------
		 */
		private function updateCheckComplete():void
		{
			// offline, first check if general conditions have been accepted
			CONFIG::offline 
			{
				var conditionsAccepted : Boolean = SharedObjectManager.instance.read(SharedObjectManager.KEY_TERMS);
				if(conditionsAccepted && !Debug.FORCE_TERMS_AND_CONDITIONS) initProject();
				else{
					
					DataLoadingManager.instance.hideLoading();
					// show general conditions
					var Terms : TermsAndConditions = new TermsAndConditions();
					addChild(Terms);
					Terms.ACCEPT_CLICKED.add(function():void
					{
						Infos.isFirstUse = true;
						SharedObjectManager.instance.write(SharedObjectManager.KEY_TERMS, true);
						initProject();
					});
				}	
			}
		}
			
		private function initProject():void 
		{ 
			// init project manager
			ProjectManager.PROJECT_READY.add(projectLoadedHandler);
			ProjectManager.PROJECT_NO_PROJECT_SELECTED.add(noProjectLoadedHandler);
			ProjectManager.instance.init();	
		}
		
		/**
		 *  construct children 
		 *  > creates all the view element of the root view
		 *  > instantiate popup and tooltip manager
		 *  > show animation of view elements
		 */
		private function constructChildren():void 
		{
			// construct content if not done yet
			if(! rootView )
			{
				// create root view, the main top view of application.
				rootView = new Sprite();
				rootView.opaqueBackground = Colors.BACKGROUND_COLOR;
				addChild(rootView);
				
				//addTopBar
				topBar = new TopBar();
				rootView.addChild(topBar);
				
				// add editor view
				editionArea = new EditionArea();
				editionArea.x = 399//399;//leftMenu.x + leftMenu.width;//400;
				rootView.addChild(editionArea);
				
				// add left menu
				leftMenu = new LeftTabs();
				leftMenu.x = topBar.x;
				leftMenu.y = TopBar.HEIGHT + 1;
				rootView.addChild(leftMenu);
				
				//listen to LEFT TAB RESIZE Signal
				LeftTabs.RESIZE.add(onLeftTabResize);
				
				// display version
				versionInfoLabel = new Label(rootView);
				versionInfoLabel.text = "V "+ Infos.buildString;
				versionInfoLabel.textField.textColor = Colors.GREY_BLUE;
				versionInfoLabel.y = stage.stageHeight-20;
				versionInfoLabel.x = 5;
				
				
				// stat/performance module 
				if(Debug.SHOW_STATS)
				{
					if(Infos.IS_DESKTOP) stats = new Stats(null,true);
					else stats = new Stats();
					
					stats.y = stage.stageHeight-100;
					addChild( stats );
				}
				
				//hide loading
				DataLoadingManager.instance.hideLoading();
	
				// LISTENER
				//resize event listener
				stage.addEventListener(Event.RESIZE, updateLayout);
				
				// TUTORIAL
				//Init Tutorial manager
				TutorialManager.instance.init(stage);
				
				if(Infos.IS_DESKTOP)
				{
					// DASHBORAD
					//Init Dashboard
					Dashboard.instance.init(stage);
				}
				
				
				// at first set everything to invisible
				rootView.visible = false;
				
				
			}
		}
		
		/**
		 * project has been created and is ready to display
		 * -> nor it was created by left menu project selection
		 * -> nor it was created by automatic opening (with cookie values)
		 */
		private function projectLoadedHandler():void
		{

			Debug.log("App.projectIsReady");
			

			//Reset manager 
			ProjectManager.instance.resetManagers();
			
			//////////////// USE THIS TO TEST ERROR SYSTEM and dev mail ////////////////
			
			////////////////////////////////////////////////////////////////////////////////
			//////////////// USE THIS TO TEST STUFF ////////////////
			////////////////////////////////////////////////////////////////////////////////
			
			
			// crash in X seconds
			/*
			TweenMax.delayedCall(10, function():void
			{
				// ** ERROR SEND MAIL TEST **
				throw new Error("Crash test");
				
				// Debug.warnAndSendMail("un petit warning test copain");
				//Debug.error("TEST error and error feedback") ;
			});
			*/
			
			
			
			// ** IMAGE ENCODING TEST ** 
			/*
			var bmd : BitmapData = new BitmapData(4000,4000,false, 0xfff000);
			bmd.draw(this)
				
			var startTime : Number = new Date().getTime();
			
			
			trace('Encoder Master');
			var encoder:Encoder = new Encoder();
			encoder.encodePNGAsync(bmd, function(result:ByteArray):void{
				trace('-----> onComplete1', result.length);
				//file = new FileReference();
				//file.save(result, "sample.png");
			}, 300);
			
			
			TweenMax.delayedCall(2, function():void
			{
				trace("" + (new Date().getTime() - startTime) + "ms");
				encoder.encodePNGAsync(bmd, function(result:ByteArray):void{
					trace('-----> onComplete2', result.length);
					//file = new FileReference();
					//file.save(result, "sample.png");
				}, 300);
			});
			
			TweenMax.delayedCall(2, function():void
			{
				encoder.encodePNGAsync(bmd, function(result:ByteArray):void{
					trace('-----> onComplete3', result.length);
					//file = new FileReference();
					//file.save(result, "sample.png");
				}, 300);
			});
			//imageEncoder.encodeBitmap(bmd);
			*/
			
			
			
			
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			// BITMAP SCALER MEMORY LEAK TEST
			////////////////////////////////////////////////////////////////////////////////
			// EDIT : this is OK ! worker doesn't have leaks, but the memory can go really high with big images
			/*
			var bitmapScaler : BitmapScaler = new BitmapScaler();
			var count : int = 0;
			var scaleBitmapF:Function = function():void
			{
				if(count == 50) return;
				
				Debug.log("START SCALING ITEM : "+count);
				count ++;
				
				// create bitmap
				var theBmd : BitmapData = new BitmapData(6000,6000,true,0);
				theBmd.fillRect(theBmd.rect, 0x000000);
				
				// scale it
				bitmapScaler.ScaleBitmap(theBmd.clone(), 3100, 3100,0,scaleBitmapF);
				
				// dispose
				theBmd.dispose();
				theBmd = null;
			};
				
			// start;
			scaleBitmapF();
			return;
			*/
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			// BITMAP ENCODER MEMORY LEAK TEST
			////////////////////////////////////////////////////////////////////////////////
			// EDIT : this is OK ! worker doesn't have leaks, but the memory can go really high with big images
			/*
			var bitmapEncoder : Encoder = new Encoder();
			var count : int = 0;
			var encodeBitmapF:Function = function():void
			{
				if(count == 10) return;
				
				Debug.log("START ENCODING ITEM : "+count);
				count ++;
				
				// create bitmap
				Debug.log("before the bitmap");
				var theBmd : BitmapData = new BitmapData(6000,6000,true,0);
				theBmd.fillRect(theBmd.rect, 0x000000);
				Debug.log("after the bitmap");
				
				// scale it
				bitmapEncoder.encodePNGAsync(theBmd.clone(), encodeBitmapF);
				
				// dispose
				theBmd.dispose();
				theBmd = null;
				Debug.log("after the dispose");
			};
			
			// start;
			encodeBitmapF();
			return;	
			*/
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			
			// ** DRAW HEX/PANTONE COLOR PICKER TEST ** 
			/*
			var colorList : Vector.<Number> = PantoneColors.getPantoneColorList();
			var boxSize : Number = 15;
			var cols:int = 25;
			var row:int = Math.ceil(colorList.length/cols)
			var bmd : BitmapData = new BitmapData(cols*boxSize, row*boxSize, false, 0x000000);
			for (var i:int = 0; i < colorList.length; i++) 
			{
			bmd.fillRect( new Rectangle((i%cols)*boxSize, Math.floor(i/cols)*boxSize , boxSize,boxSize), colorList[i] );
			}
			
			addChild( new Bitmap(bmd)) ;
			visible = true;
			return;
			*/
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			
			// construct children if not already done
			//if(!rootView) constructChildren();
			leftMenu.updateView(true);
			
			// init edition area
			editionArea.init();
			
			// init photo editor module
			ImageEditorModule.instance.init(stage);
			
			// refresh photo list (this seems to be needed for folder update stuff, otherwise we have strange behavior and we should review the whole image list upload process)
			SessionManager.instance.refreshImageList();
			
			// open photo tab
			//TabManager.instance.openTab(1,LeftTabs.TABS_ID);  // should be done in leftMenu.updateview
			
			// topbar reset
			topBar.update();
			
			// > if project has temp folder but project has an id, we allow user to merge folders or to save temp folder as custom folder
			FolderManager.instance.checkFolderStatus();
			
			// show
			show();
			
			// show news if needed
			showNewsIfNeeded(showTutorialIfNeeded);
			
			// for offline, once project is loaded, make a project image check to allow user to fix possible issues
			CONFIG::offline
			{
				ProjectPatcher.instance.checkOfflineImages();
			}
		}
		
		
		/**
		 * app is ready, no project is loaded, we show wizard
		 */
		private function noProjectLoadedHandler():void
		{
			//Reset manager 
			//ProjectManager.instance.resetManagers();
			
			// just update left tab to display wizard
			leftMenu.updateView();

			// show
			show();
			
			//showDashborad
			if(Infos.IS_DESKTOP && showDashboard)
			{
				showDashboard = false;
				ProjectManager.instance.startNewProject();
			}
			
			// show news if needed (this has to be done here to be over dashboard)
			showNewsIfNeeded();
			
		}
		
		
		/**
		 * display NEWS popup if needed
		 */
		private function showNewsIfNeeded(onNewsCompleteFunction:Function = null):void
		{
			if(NewsManager.instance.hasNews())
			{
				NewsManager.instance.init(stage);
				NewsManager.instance.open(onNewsCompleteFunction);
			}
			else if(onNewsCompleteFunction!=null)
				TweenMax.delayedCall(2,onNewsCompleteFunction);
		}
		
		/**
		 * display TUTORIAL if needed
		 */
		private function showTutorialIfNeeded():void
		{
			if(TutorialManager.instance.hasNeverSeenTutorial())
			{
				TutorialManager.instance.start();
			}
		}
		
		
		
		/**
		 * show content view (with animation)
		 */
		private function show():void
		{
			// show 
			rootView.alpha = 0;
			
			// repositionate content
			updateLayout();
			
			TweenMax.to(rootView, 1, {delay:.1, autoAlpha:1, ease:Strong.easeOut});
		}
		
		
		/**
		 * hide content view
		 */
		private function hide():void
		{
			rootView.alpha = 0;			
			TweenMax.to(rootView, 1, {delay:0, autoAlpha:0, ease:Strong.easeOut});
		}
		
		
		
		/**
		 * on left tab resize handler
		 */
		private function onLeftTabResize() :void
		{
			// update edition area
			if(editionArea){
				editionArea.x = leftMenu.WIDTH + 15;
				editionArea.collapsePageNavigator();
				editionArea.updateLayout();	
			}
		}
		
		
		/**
		 * on stage resize handler
		 */
		private function updateLayout( e:Event = null ) :void
		{
			// update left tabls
			if(leftMenu) leftMenu.updateLayout();
			
			// update edition area
			if(editionArea) {
				editionArea.updateLayout();
				if(leftMenu) editionArea.x = leftMenu.WIDTH+15;
			}
			
			versionInfoLabel.y = stage.stageHeight-20;
			versionInfoLabel.x = 5;
			
			// update background
			graphics.beginFill( Colors.BACKGROUND_COLOR );
			graphics.drawRect( 0, 0, stage.stageWidth, stage.stageHeight );
			graphics.endFill();
		}
		
		
		
	}
}	