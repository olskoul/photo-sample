package utils.debug
{
	public class TimeLog
	{
		private static var logs:Object = new Object();
		
		public static function start(logId:String = "default" ):void
		{
			logs[logId] = {start:new Date().time, end:new Date().time};
		}
		
		public static function log(logId:String = "default" ):String
		{
			if(!logs[logId]) return "null";
			
			var logObj:Object = logs[logId];
			var now:Number = new Date().time;
			var timeElapsed : Number = now - logObj.end;
			var totalTimeElapsed : Number = now - logObj.start;
			
			logObj.end = now;
			
			return "[TIMELOG] : "+logId +" > "+timeElapsed + "ms (total:"+totalTimeElapsed+"ms)";
		}
		
		public static function kill(logId:String = "default" ):void
		{
			logs[logId] = null;
		}
		
	}
}