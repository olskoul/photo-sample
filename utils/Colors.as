package utils
{
	import flash.display.BitmapData;
	
	import library.colors.palette;

	public class Colors
	{
		public static var PALETTE:BitmapData = new library.colors.palette;
		public static const BACKGROUND_COLOR:Number = 0xe5e5e5;
		public static const FULLSCREEN_COLOR:Number = 0x7c7c7c;
		public static const GRID_COLOR:Number = 0x00e7e7;

		public static const BLACK : Number = 0x000000;
		public static const WHITE : Number = 0xffffff;
		public static const GREEN : Number = 0x62a639;
		public static const GREEN_FLASH : Number = 0x00ff00;
		public static const GREEN_TRANSFORM_TOOL : Number = 0x70ae4b;
		
		public static const RED : Number = 0xd45a5a;
		public static const RED_FLASH : Number = 0xff0000;
		public static const YELLOW : Number = 0xfebe66;
		public static const GREY : Number = 0xbdbdbd;
		public static const GREY_LIGHT : Number = 0xe9e8e9;
		public static const GREY_DARK : Number = 0x7c7c7c;
		public static const GREY_BLUE : Number = 0x8c98a6;
		public static const BLUE : Number = 0x0068a0;
		public static const BLUE_LIGHT : Number = 0xb5e0f8;
		public static const BLUE_SCROLL : Number = 0x578ab6
		public static const BLUE_CLOUD : Number = 0x36b5eb;
		public static const ORANGE : Number = 0xff934b;//0xd27f48;
		
	}
}