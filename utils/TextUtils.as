package utils
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import comp.inputtext.InputTextTicTac;
	
	import flashx.textLayout.formats.TextAlign;

	public class TextUtils
	{
		
		
		/**
		 * create a simple label
		 */
		public static function createLabelWithTextFormat(text:String="", textFormat:TextFormat=null, x:Number = 0, y:Number = 0, width:Number=100, height:Number=20 , isHtmlText:Boolean = false, multiline:Boolean = false ):TextField
		{
			var labelView:TextField = new TextField();
			labelView.embedFonts = true;
			labelView.antiAliasType = AntiAliasType.ADVANCED;
			labelView.multiline = multiline;
			if(textFormat)	labelView.defaultTextFormat = textFormat;
			
			labelView.y = y;
			labelView.x = x;
			labelView.width = width;
			labelView.height = height;
			if(!isHtmlText)
			labelView.text = text;
			else
			{
				labelView.htmlText = text;
			}
			
			return labelView;
		}
		
		
		public static function createInputField(text:String="", textFormat:TextFormat=null, x:Number = 0, y:Number = 0, width:Number=100, height:Number=20, autoSize:String = TextAlign.LEFT):TextField
		{
			var _tf:TextField = new TextField();
			_tf.autoSize = autoSize;
			_tf.antiAliasType = AntiAliasType.ADVANCED;
			_tf.embedFonts = true;
			_tf.selectable = true;
			_tf.type = TextFieldType.INPUT;
			if(!textFormat) textFormat = TextFormats.ASAP_BOLD(13,Colors.BLUE);
			_tf.defaultTextFormat = textFormat;
			
			_tf.y = y;
			_tf.x = x;
			_tf.width = width;
			_tf.height = height;
			
			_tf.text = text;
			
			return _tf;
		}
		
		
	}
}