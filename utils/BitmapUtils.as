package utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.display.StageQuality;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	import be.antho.utils.MathUtils2;
	
	import data.Infos;

	public class BitmapUtils
	{
		// thumb values
		public static const THUMB_MAX_WIDTH : int = 200;
		public static const THUMB_MAX_HEIGHT : int = 150;
		
		
		/**
		 * Util to resize bitmapdata using max width and max height
		 */ 
		public static function resizeBitmapData( bmd : BitmapData, maxWidth : Number, maxHeight : Number, smoothing:Boolean = true):BitmapData
		{
			//	Infos.stage.quality = StageQuality.LOW;
			
			var m : Matrix = new Matrix();
			var originalWidth : Number = bmd.width;
			var originalHeight : Number = bmd.height;
			var newScaleX:Number = maxWidth / originalWidth;
			var newScaleY:Number = maxHeight / originalHeight;
			var scale:Number = Math.min(newScaleX, newScaleY);
			var newWidth:Number = originalWidth * scale;
			var newHeight:Number = originalHeight * scale;
			
			Debug.log("BitmapUtils.resizeBitmapData : originalWidth="+originalWidth+" originalHeight="+originalHeight+ " > newWidth="+newWidth + "  newHeight="+newHeight );
			
			if(scale >= 1) return bmd.clone();// do not resize up the bitmapdata
			
			var tempData :BitmapData = new BitmapData(newWidth, newHeight);
			m.scale(scale, scale);
			tempData.draw(bmd, m, null, null, null,smoothing);
			return tempData;
		}
		
		// create a thumb bitmap data
		public static function createThumbData( bmd : BitmapData, exifRotation:Number = 0 ):BitmapData
		{
			var thumbData : BitmapData = resizeBitmapData( bmd, THUMB_MAX_WIDTH, THUMB_MAX_HEIGHT, true );
			if(exifRotation == 90 || exifRotation == -90)
			{
				thumbData = transformBitmapData(thumbData, exifRotation, 1, true);
			}
			return thumbData;
		}
		
		
		/**
		 *  draw a clip (movieclip or sprite) into a bitmap
		 */
		public static function drawClipBitmap(sp:DisplayObject):Bitmap
		{
			var bounds:Rectangle = sp.getBounds(sp);
			var bmd:BitmapData, bp:Bitmap;
			bmd = new BitmapData(sp.width, sp.height, true, 0x000000); //sp.width+1????
			
			var boundsMatrix:Matrix = new Matrix();
			boundsMatrix.tx = -bounds.x;
			boundsMatrix.ty = -bounds.y;
			bmd.draw(sp,boundsMatrix);
			
			bp = new Bitmap(bmd, "auto", true);
			bp.x = bounds.x;
			bp.y = bounds.y;
			return bp;
		}
		
		
		/**
		 *  apply an exif rotation to a bitmap data
		 */
		public static function transformBitmapData(bmd:BitmapData, rotation:Number, scale:Number, dispose:Boolean = false, forceTransparent:Boolean = false):BitmapData
		{
			var newBmd:BitmapData;
			if(isNaN(rotation)) rotation = 0; // security if NaN
			
			// if rotation and scale are default, just make a clone
			if(rotation == 0 && scale == 1) newBmd = bmd.clone();
			else
			{	
				var newW:Number = bmd.width;
				var newH:Number = bmd.height;
				// invert width/height if rotation is -90 or 90 
				if( rotation != 0 && rotation != 180 ){
					var tempW : int = newW;
					newW = newH;
					newH = tempW;
				}
				
				// rotation and scale matrix
				var rm : Matrix = new Matrix();
				rm.translate( -bmd.width*.5, -bmd.height*.5); // put it in center
				rm.rotate(MathUtils2.toRadian(rotation)); // rotate it
				rm.translate( newW*.5, newH*.5); // put it back in center
				if (scale!=1) rm.scale(scale, scale);
				newW = newW*scale;
				newH = newH*scale;
				
				if(forceTransparent) newBmd = new BitmapData( newW, newH, forceTransparent, 0x000000);
				else newBmd = new BitmapData( newW, newH);
				newBmd.draw( bmd, rm, null, null, null, true );
			}
			
			// clear origin bmd if needed
			if(dispose) bmd.dispose();
		
			// return new bitmap data
			return newBmd;
		}
		
	}
}