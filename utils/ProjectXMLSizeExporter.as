package utils
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	
	import data.ProductsCatalogue;
	
	import manager.MeasureManager;
	
	import view.popup.PopupAbstract;

	public class ProjectXMLSizeExporter
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////
		private var projectXMLCopy : XML;
		private var documentXMLCopy : XML;
		private var coverXMLCopy : XML;
		private var sizeUpdateList : Array;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function ProjectXMLSizeExporter( projectsXML:XML, documentsXML:XML, coversDocumentXML:XML):void
		{
			projectXMLCopy = new XML(projectsXML.toString());
			documentXMLCopy  = new XML(documentsXML.toString());
			coverXMLCopy  = new XML(coversDocumentXML.toString());
			
			//////////////////////////////////////////
			// fill this if you want some project size update (in cm)
			sizeUpdateList = new Array(
				// M				
				{id:"M30", w:31.35, h:24.40},
				{id:"M40", w:31.35, h:24.40},
				{id:"M50", w:31.35, h:24.40},
				{id:"M60", w:31.35, h:24.40},
				{id:"M70", w:31.35, h:24.40},
				{id:"M80", w:31.35, h:24.40},
				{id:"M90", w:31.35, h:24.40},
				{id:"M100", w:31.35, h:24.40},
				{id:"M110", w:31.35, h:24.40},
				{id:"M120", w:31.35, h:24.40},
				// M covers				
				{id:"M30C", w:64.90, h:25.20},
				{id:"M40C", w:64.90, h:25.20},
				{id:"M50C", w:65.30, h:25.20},
				{id:"M60C", w:65.30, h:25.20},
				{id:"M70C", w:65.30, h:25.20},
				{id:"M80C", w:65.50, h:25.20},
				{id:"M90C", w:65.70, h:25.20},
				{id:"M100C", w:65.70, h:25.20},
				{id:"M110C", w:65.90, h:25.20},
				{id:"M120C", w:65.90, h:25.20},
				// L
				{id:"L30", w:38.25, h:32.85},
				{id:"L40", w:38.25, h:32.85},
				{id:"L50", w:38.25, h:32.85},
				{id:"L60", w:38.25, h:32.85},
				{id:"L70", w:38.25, h:32.85},
				{id:"L80", w:38.25, h:32.85},
				{id:"L90", w:38.25, h:32.85},
				{id:"L100", w:38.25, h:32.85},
				{id:"L110", w:38.25, h:32.85},
				{id:"L120", w:38.25, h:32.85},
				// L covers
				{id:"L30C", w:78.60, h:33.70},
				{id:"L40C", w:78.60, h:33.70},
				{id:"L50C", w:79.00, h:33.70},
				{id:"L60C", w:79.00, h:33.70},
				{id:"L70C", w:79.00, h:33.70},
				{id:"L80C", w:79.20, h:33.70},
				{id:"L90C", w:79.40, h:33.70},
				{id:"L100C", w:79.40, h:33.70},
				{id:"L110C", w:79.60, h:33.70},
				{id:"L120C", w:79.60, h:33.70},					
				// Q covers
				{id:"Q20C", w:44.20, h:21.60},
				{id:"Q30C", w:44.40, h:21.60},
				{id:"Q40C", w:44.40, h:21.60},
				{id:"Q50C", w:44.80, h:21.60},
				{id:"Q60C", w:44.80, h:21.60},
				{id:"Q70C", w:44.80, h:21.60},
				{id:"Q80C", w:45.00, h:21.60},
				// QA covers
				{id:"QA20C", w:50.00, h:24.60},
				{id:"QA30C", w:50.20, h:24.60},
				{id:"QA40C", w:50.20, h:24.60},
				{id:"QA50C", w:50.60, h:24.60},
				{id:"QA60C", w:50.60, h:24.60},
				{id:"QA70C", w:50.60, h:24.60},
				{id:"QA80C", w:50.80, h:24.60},
				// QB covers
				{id:"QB20C", w:67.60, h:33.60},
				{id:"QB30C", w:67.80, h:33.60},
				{id:"QB40C", w:67.80, h:33.60},
				{id:"QB50C", w:68.20, h:33.60},
				{id:"QB60C", w:68.20, h:33.60},
				{id:"QB70C", w:68.20, h:33.60},
				{id:"QB80C", w:68.40, h:33.60}
			);
			//////////////////////////////////////////
			
		}
		
		/*
		we have a PFL size of 105 (in xml)
		and a coverMeasure of 6.95 (in xml)
		so the size in postscript is : 730 ( = 105*6,95 )
		730 points / 28.3464567 (cm to poin)  = 25,75 cm
		*/
		public function traceProjectsSize():void
		{
			var updateSize :Boolean = (sizeUpdateList && sizeUpdateList.length > 0)?true:false;
			var projectNode : XML ;
			var documentNode : XML ;
			var coverNode : XML;
			var docCode : String;
			var w : Number; // width
			var h: Number; //height
			var dm : Number; // doc measure
			var cm:Number; // cover measure
			var s:Number; // spine
			
			var printSize : Point ; // size in cm
			var sizeUpdateVo : SizeUpdateVo; // new PFL and measure info after update
			
			for (var i:int = 0; i < projectXMLCopy.projects.children().length(); i++) 
			{
				projectNode = projectXMLCopy.projects.children()[i];
				docCode =  projectNode.@docidstr;
				coverNode = coverXMLCopy.documents.document.(@name==("C"+docCode))[0];
				
				// check normal page
				documentNode = documentXMLCopy.document.(@name==docCode)[0];
				w = parseFloat(documentNode.@pagewidth);
				h = parseFloat(documentNode.@pageheight);
				dm = parseFloat(projectNode.docMeasure.@value);
				printSize = PFLToCM( w, h, dm );
				trace("Project with ID : "+docCode + " (w:"+w+" - h:" +h+ " - measure:"+dm +")");
				trace("Page size : " + printSize.x.toFixed(2) + "cm x "+ printSize.y.toFixed(2) +"cm");
				
				sizeUpdateVo = needSizeUpdate (docCode,w,h);
				if(sizeUpdateVo)
				{
					documentNode.@pageheight = sizeUpdateVo.height
					projectNode.docMeasure.@value = sizeUpdateVo.measure;
				}
				sizeUpdateVo = null;
				
				// check cover
				cm = parseFloat(projectNode.coverMeasure.@value);
				cm = (!isNaN(cm))?cm:dm;
				if(coverNode != null ){
					s = SpineUtils.customSpineWidth(docCode, false, true, ProductsCatalogue.PAPER_QUALITY_250);
					w = parseFloat(coverNode.@pagewidth);
					h = parseFloat(coverNode.@pageheight);
					printSize = PFLToCM( w*2, h, cm, s );
					trace("Project has cover (w:"+w+" - h:" +h+ " - coverMeasure:"+cm+ " - spine : "+s+"mm )");
					trace("Cover size : "+ printSize.x.toFixed(2) + "cm x "+ printSize.y.toFixed(2) +"cm");
					
					sizeUpdateVo = needSizeUpdate (docCode+"C", w,h,s);
					if(sizeUpdateVo)
					{
						coverNode.@pageheight = sizeUpdateVo.height
						projectNode.coverMeasure.@value = sizeUpdateVo.measure;
					}
					
				}
				
				trace("----------------------------");
			}
			
			
			// export new xmls 
			if(updateSize){
				PopupAbstract.Alert("New XML SIZE EXPORT", "Do you want to export new updated xml content ?", true, function(p:Object):void{
					var exportStr : String = "";
					exportStr += "\n\n\n\n\n\n\n\n\n";
					exportStr += "--------------------------------------\n";
					exportStr += "----- PROJECT XML after size update --\n";
					exportStr += "--------------------------------------\n\n\n";
					exportStr += '<?xml version="1.0" encoding="utf-8"?>\n';
					exportStr += projectXMLCopy.toString();
					exportStr += "\n\n\n\n\n\n\n\n\n";
					exportStr += "--------------------------------------\n";
					exportStr += "----- DOCUMENT XML after size update --\n";
					exportStr += "--------------------------------------\n\n\n";
					exportStr += '<?xml version="1.0" encoding="utf-8"?>\n';
					exportStr += documentXMLCopy.toString();
					exportStr += "\n\n\n\n\n\n\n\n\n";
					exportStr += "--------------------------------------\n";
					exportStr += "----- COVER XML after size update --\n";
					exportStr += "--------------------------------------\n\n\n";
					exportStr += '<?xml version="1.0" encoding="utf-8"?>\n';
					exportStr += coverXMLCopy.toString();
					
					var exportFile : FileReference = new FileReference();
					exportFile.save(exportStr, "xmlSizeExport.txt");
					
				});
				
			}
			
		}
		
		/**
		 * transform a pfl measure to cm
		 */
		private function PFLToCM( PFLWidth:Number, PFLHeight:Number, docMeasure:Number, spineWidth:Number = 0): Point
		{
			var newWidth:Number = PFLWidth * docMeasure / MeasureManager.cmToPoint + spineWidth/10 ;
			var newHeight:Number = PFLHeight * docMeasure / MeasureManager.cmToPoint ;
			return new Point(newWidth, newHeight);
		}
		
		
		/**
		 * check if content need a size update
		 */
		private function needSizeUpdate( docCode : String, w:Number, h:Number, spineSize:Number = 0 ) : SizeUpdateVo
		{
			var sizeUpdateObj : Object;
			var sizeUpdateVo : SizeUpdateVo;
			for (var i:int = 0; i < sizeUpdateList.length; i++) 
			{
				if(sizeUpdateList[i].id == docCode) sizeUpdateObj = sizeUpdateList[i];
			}
			
			if(sizeUpdateObj){
				sizeUpdateVo = new SizeUpdateVo();
				var wantedWidth : Number = (spineSize > 0)? (sizeUpdateObj.w - (spineSize/10)) /2 : sizeUpdateObj.w;
				var wantedRatio : Number = wantedWidth / sizeUpdateObj.h;
				sizeUpdateVo.height = (w/wantedRatio).toFixed(1);// Math.round(w / wantedRatio);
				//sizeUpdateVo.width = w;
				sizeUpdateVo.measure = (wantedWidth / w * MeasureManager.cmToPoint).toFixed(6);
				
				
			}
			
			return sizeUpdateVo;
		}
	}
}

internal class SizeUpdateVo
{
	//public var width:String; // not used anymore as we modify only pageheight property (to keep track and compare with older versions)
	public var height:String;
	public var measure:String;
}