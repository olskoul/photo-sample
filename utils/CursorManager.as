package utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.ui.MouseCursorData;
	
	import library.cursor.move;
	
	import mcs.icons.handIcon;

	public class CursorManager
	{
		// cursor types
		public static const CURSOR_MOVE:String = "CURSOR_MOVE";
		public static const CURSOR_HAND:String = "CURSOR_HAND";
		
		private var _stage:Stage;
		private var cursors:Object = {};
		private var currentCursor:Bitmap;
		
		public function CursorManager(sf:SingletonEnforcer)
		{
			if(!sf) throw new Error("LayoutManager is a singleton, use instance"); 
		}
		/**
		 * instance
		 */
		private static var _instance:CursorManager;
		public static function get instance():CursorManager
		{
			if (!_instance) _instance = new CursorManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Init the manager
		 * ref the stage
		 * init different cursors types
		 * */
		public function init(stage:Stage):void
		{
			_stage = stage;
			initCursors();
		}
		
		/**
		 * Hide the default mouse cursor
		 * Check if the required cursor exist
		 * show it
		 * make it follow the mouse
		 * reset function stops everything and show the default mouse curosr
		 * */
		public function showCursorAs(cursorId:String):void
		{
			if(cursors[cursorId])
			{
				Mouse.hide();
				currentCursor = cursors[cursorId];
				//currentCursor.cacheAsBitmap = true;
				//currentCursor.mouseChildren = false;
				//currentCursor.mouseEnabled = false;
				stickCursorToMouse();
				_stage.addChild(currentCursor);
				followMouse();
				
			}
			else
			{
				Debug.warn("CURSOR "+cursorId+" does not exist");
			}
			
			//Mouse.cursor = MouseCursor.HAND; // INSANE BUG ON GOOGLE CHROME... NOT WORKING.. but other browser ar ok
		}
		
		/**
		 *Reset to default cursor
		 * */
		public function reset():void
		{
			// Mouse.cursor = MouseCursor.AUTO; NOT WORKING ON GOOGLE CHROME
			
			_stage.removeEventListener(Event.ENTER_FRAME,enterFrameHandler);
			//_stage.removeEventListener(MouseEvent.MOUSE_MOVE,enterFrameHandler);
			
			if(currentCursor && _stage.contains(currentCursor))
				_stage.removeChild(currentCursor);
			currentCursor = null;
			
			Mouse.show();
			
			
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Add enterframe listener to follow mouse movement
		 * */
		private function followMouse():void
		{
			_stage.removeEventListener(Event.ENTER_FRAME,enterFrameHandler);
			_stage.addEventListener(Event.ENTER_FRAME,enterFrameHandler);
			//_stage.removeEventListener(MouseEvent.MOUSE_MOVE,enterFrameHandler);
			//_stage.addEventListener(MouseEvent.MOUSE_MOVE,enterFrameHandler);
		}
		
		/**
		 * Enterframe envent handler
		 * */
		protected function enterFrameHandler(event:Event):void
		{
			stickCursorToMouse()
		}		
		
		/**
		 * Stiuck the custom cursor view to the mouse
		 * */
		private function stickCursorToMouse():void
		{
			currentCursor.x = _stage.mouseX - currentCursor.width*.5;
			currentCursor.y = _stage.mouseY- currentCursor.height*.5;
		}
		
		/**
		 * init different cursors types
		 * Associate view and static const 
		 * */
		private function initCursors():void
		{
			// move cursor
			var cursor : MovieClip = new library.cursor.move();
			cursors[CURSOR_MOVE] = cursorToBitmap(cursor);	
			//registerCursor(CURSOR_MOVE,cursorToBitmap(cursor)); // not working on google chrome
			
			// hand cursor
			cursor  = new mcs.icons.handIcon();
			cursors[CURSOR_HAND] = cursorToBitmap(cursor); // not working on google chrome
			//registerCursor(CURSOR_HAND,cursorToBitmap(cursor));
		}
		
		
		/**
		 * register native cursor
		 * The bitmap must be 32x32 pixels or smaller, due to an OS limitation
		 * EDIT : this is the best option but not working on google chrome.. no documentation, no clue... don't know why
		 */
		private function registerCursor(cursorName:String, bitmapCursor : Bitmap ):void
		{
			// Create a MouseCursorData object
			var cursorData:MouseCursorData = new MouseCursorData();
			// Specify the hotspot
			cursorData.hotSpot = new Point(15,15);
			// Pass the cursor bitmap to a BitmapData Vector
			var bitmapDatas:Vector.<BitmapData> = new Vector.<BitmapData>(1, true);
			// Pass the value to the bitmapDatas vector
			bitmapDatas[0] = bitmapCursor.bitmapData;
			// Assign the bitmap to the MouseCursor object
			cursorData.data = bitmapDatas;
			// Register the MouseCursorData to the Mouse object with an alias
			Mouse.registerCursor(cursorName, cursorData);
			// When needed for display, pass the alias to the existing cursor property
			Mouse.cursor = cursorName;
		}
		
		
		/**
		 *
		 */
		private function cursorToBitmap(cursor : DisplayObject):Bitmap
		{
			var bmd:BitmapData = new BitmapData(cursor.width, cursor.height, true, 0);
			bmd.draw(cursor);
			return new Bitmap(bmd);
		}
		
	}
}
class SingletonEnforcer{}