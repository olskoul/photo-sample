package utils
{
	import be.antho.bitmap.snapshot.SnapShot;
	
	import data.FrameVo;
	import data.Infos;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import library.font.asap.regular;
	
	import manager.MeasureManager;
	

	public class TextFormats
	{
		
		/**
		 * ASAP is the font used in the assets, to avoid possible conflicts we keep it in this project
		 * Other fonts are in the FontsManager , located in a shared library project
		 * */
		private static var FONT_ASAP_REGULAR:Font = new library.font.asap.regular();
		private static var FONT_ASAP_BOLD:Font = new library.font.asap.bold();
		
		/**
		 * Font availability
		 * For text tool and for cover text
		 * */
		public static var AVAILABLE_FONTS:Array;
		public static var AVAILABLE_COVER_FONTS:Array;
		public static var AVAILABLE_FONTS_BITMAP:Vector.<Bitmap> = new Vector.<Bitmap>();
		
		public static var LAST_PAGE_NUMBER_USED:TextFormat = null;
		
		public function TextFormats(){}
		
		/**
		 * Initiation
		 * Retreives Fonts from FontManarger (external shared project)
		 * */
		public static function init():void
		{
			AVAILABLE_FONTS = FontsManager.AVAILABLE_FONTS;
			AVAILABLE_COVER_FONTS = FontsManager.AVAILABLE_COVER_FONTS;
			
			AVAILABLE_FONTS.sortOn("fontName");
			createFontSelector();
		}
		
		/**
		 * Creates bitmap of font name to put in font selector combo box
		 * */
		private static function createFontSelector():void
		{
			var bitmap:Bitmap;
			var tf:TextFormat = new TextFormat();
			tf.size = 19;
			tf.bold = false;
			tf.italic = false;
			var t:TextField;
			
			
			for (var i:int = 0; i < AVAILABLE_FONTS.length; i++) 
			{
				t = new TextField();
				t.embedFonts = true;
				t.multiline = false;
				t.width = 120;
				t.height = 30;
				//t.x = 160*i;
				tf.font = AVAILABLE_FONTS[i].fontName;
				t.text = AVAILABLE_FONTS[i].fontName;
				t.setTextFormat(tf);
				bitmap = new Bitmap(SnapShot.snap(t));
				bitmap.name = AVAILABLE_FONTS[i].fontName;
				//bitmap.x = 160*i;
				AVAILABLE_FONTS_BITMAP.push(bitmap);
				//Infos.stage.addChild(bitmap);
			}
			
		}
		
		/**
		 * Return a bitmapData
		 * Font name draw on a bitmapdata (use for font selector combo box)
		 * */
		public static function getFontBitmapByName(name:String):BitmapData
		{
			for (var i:int = 0; i < AVAILABLE_FONTS_BITMAP.length; i++) 
			{
				if(AVAILABLE_FONTS_BITMAP[i].name == name)
					return AVAILABLE_FONTS_BITMAP[i].bitmapData.clone();
			}
			
			return null;
		}
		
		
		public static function ASAP_REGULAR(textSize:int = 25, color:Number = 0x7c7c7c, align:String = TextFormatAlign.LEFT):TextFormat
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.font = FONT_ASAP_REGULAR.fontName;
			textFormat.align = align;
			textFormat.color = color;
			textFormat.size = textSize;
			return textFormat;
		}
		
		public static function ASAP_BOLD(textSize:int = 25, color:Number = 0x7c7c7c, align:String = TextFormatAlign.LEFT):TextFormat
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.align = align;
			textFormat.font =  FONT_ASAP_BOLD.fontName;
			textFormat.color = color;
			textFormat.size = textSize;
			return textFormat;
		}
		
		public static function DEFAULT(textSize:int = 25, color:Number = 0x7c7c7c, align:String = TextFormatAlign.CENTER):TextFormat
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.align = align;
			textFormat.font =  FONT_ASAP_REGULAR.fontName;
			textFormat.color = color;
			textFormat.size = textSize;
			return textFormat;
		}
		
		public static function DEFAULT_FRAME_TEXT(textSize:int = 25, color:Number = 0x000000, align:String = TextFormatAlign.CENTER):TextFormat
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.align = align;
			textFormat.font =  FontsManager.FONT_GIll_SANS.fontName;
			textFormat.color = color;
			textFormat.size = textSize;
			return textFormat;
		}
		
		public static function LAST_USED(textSize:int = 25):TextFormat
		{
			if(!Infos.project.lastUsedTextFormat)
				return DEFAULT_FRAME_TEXT(textSize);
			
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = textSize;
			textFormat.font =  Infos.project.lastUsedTextFormat.font;
			textFormat.color = Infos.project.lastUsedTextFormat.color;
			textFormat.underline = Infos.project.lastUsedTextFormat.underline;
			textFormat.bold = Infos.project.lastUsedTextFormat.bold;
			textFormat.italic = Infos.project.lastUsedTextFormat.italic;
			textFormat.align = TextFormatAlign.CENTER;
			return textFormat;
		}
		

		
		public static function DEFAULT_TOOLTIP(textSize:int = 15, color:Number = 0xffffff, align:String = TextFormatAlign.LEFT):TextFormat
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.align = align;
			textFormat.font =  FONT_ASAP_REGULAR.fontName;
			textFormat.color = color;
			textFormat.size = textSize;
			return textFormat;
		}
		
		public static function getCalendarTextFormatByDateAction(frameVo:FrameVo, align:String):TextFormat
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.align = align;
			textFormat.font =  frameVo.calFontName;//FontsManager.FONT_Helevetica.fontName;
			textFormat.bold = frameVo.calBold;
			textFormat.italic = frameVo.calItalic;
			textFormat.color = frameVo.color;
			textFormat.size = MeasureManager.getFontSize(Number(frameVo.calFontSizeRaw),false);//frameVo.fontSize;
			return textFormat;
		}
		
		/**
		 * Validation Check
		 * Check if the fontName exist in the available font list
		 * if not, change it with the first font from the list (MOstly Arial)
		 * */
		public static function validateFont(fontName:String):String
		{
			if(fontName == "null")
			{
				Debug.log("TextFormats > validateFont > Fontname is null, apply default font which is : "+AVAILABLE_FONTS[0].fontName);
				return AVAILABLE_FONTS[0].fontName;
			}
			
			for (var i:int = 0; i < AVAILABLE_FONTS.length; i++) 
			{
				if(fontName == AVAILABLE_FONTS[i].fontName)
					return fontName;
			}
			if(fontName != "null")
			{
				Debug.warn("TextFormats > validateFont > Font validation failed, trying to apply: "+fontName+"... Changed it to: "+AVAILABLE_FONTS[0].fontName);
				return AVAILABLE_FONTS[0].fontName;
			}
			
			return "null";
		}

	}
}