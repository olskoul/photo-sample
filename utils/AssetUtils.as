package utils
{

	//IMPORT FROM SWC
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	import comp.Image;

	
	// class
	public class AssetUtils
	{	
		// ref classes
		public static const gameAssetPool : Object = new Object();
		
			
		public static function getGameAssetData( assetName : String ):BitmapData
		{
			if( gameAssetPool[assetName] ) return gameAssetPool[assetName] as BitmapData;
			
			var asset:MovieClip = getClip(assetName);
			
			gameAssetPool[assetName] = drawClipBitmap(asset).bitmapData;
			return gameAssetPool[assetName];
		}
		
		
		
		
		/**
		 *  Returns a Bitmap from a className in SWC 
		 */ 
		public static function getBitmap(name:String):Bitmap
		{
			var finalAssetName:String = "bmp." +name;
			var assetClass:Class =  getDefinitionByName(finalAssetName) as Class;
			
			var bmd:BitmapData = new assetClass() as BitmapData;
			var myBitmap:Bitmap = new Bitmap(bmd, "auto", true);
			
			return myBitmap;
		}
		
		/**
		 *  Returns a clip from a classname in SWC
		 */
		public static function getClip(name:String):*
		{
			var finalAssetName:String = "mcs." + name;
			var assetClass:Class =  getDefinitionByName(finalAssetName) as Class;
			
			var clip:DisplayObject = new assetClass() as DisplayObject;
			
			if(clip is Sprite) return clip as Sprite;
			if(clip is MovieClip) return clip as MovieClip;
			
			return clip;
		}
		
		/**
		 *  Returns a clip existing in SWC and transform it directly into image
		 */
		public static function getClipAsImage(name:String, center:Boolean = false):Image
		{
			var clip:Sprite = getClip(name);
			return getImageClone(clip, center);
		}
		

		/**
		 *  Returns clone image if a sprite
		 */
		public static function getImageClone(obj:DisplayObject, center:Boolean = false, samePosition:Boolean = true):Image
		{
			var img:Image = new Image(drawClipBitmap(obj), center);
			if(samePosition)
			{
				if(center){
					img.x = obj.x + img.width>>1;
					img.y = obj.y + img.height>>1;
				}else{
					img.x = obj.x;
					img.y = obj.y;
				}
			}
			return img;
		}

		
		/**
		 *  draw a clip (movieclip or sprite) into a bitmap
		 */
		public static function drawClipBitmap(sp:DisplayObject):Bitmap
		{
			var bounds:Rectangle = sp.getBounds(sp);
			var bmd:BitmapData, bp:Bitmap;
			bmd = new BitmapData(sp.width, sp.height, true, 0x000000); //sp.width+1????
			
			var boundsMatrix:Matrix = new Matrix();
			boundsMatrix.tx = -bounds.x;
			boundsMatrix.ty = -bounds.y;
			bmd.draw(sp,boundsMatrix);
			
			bp = new Bitmap(bmd, "auto", true);
			bp.x = bounds.x;
			bp.y = bounds.y;
			return bp;
		}
		
		
		
		
	}
}