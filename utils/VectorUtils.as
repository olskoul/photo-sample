package utils
{
	import data.AlbumVo;
	
	import manager.FolderManager;

	public class VectorUtils
	{
		public function VectorUtils()
		{
		}
		
		/**
		 *  SortOn method for Vectors
		 * options:
			Array.CASEINSENSITIVE or 1
			Array.DESCENDING or 2
			Array.UNIQUESORT or 4
			Array.RETURNINDEXEDARRAY or 8
			Array.NUMERIC or 16
		 * */
		public static function sortOn(vector:*, propertyToSortOn:String, options:*=0):*
		{
			var tmpArr:Array = [];
			
			for (var i:int = 0; i < vector.length; i++) 
			{
				tmpArr.push(vector[i]);
			}
			tmpArr.sortOn(propertyToSortOn,options);
			for (i = 0; i < tmpArr.length; i++) 
			{
				vector[i] = tmpArr[i];
			}
			return vector;
		}
		
		/**
		 *  SortOn  specific nested property
		 *  ex: "myObject.Object.myProps" 
		 * 
		 *  Array.CASEINSENSITIVE or 1
			Array.DESCENDING or 2
			Array.UNIQUESORT or 4
			Array.RETURNINDEXEDARRAY or 8
			Array.NUMERIC or 16
		 * */
		public static function sortOnNestedProps(vector:*, vectorType:Class, propertyToSortOn:String, options:*=0):*
		{
			var tmpArr:Array = [];
			var nestedProps:Array = propertyToSortOn.split(".");
			for (var i:int = 0; i < vector.length; i++) 
			{	
				var obj:Object = {};
				obj.parent = vector[i];
				obj.prop = vector[i];
				for (var j:int = 0; j < nestedProps.length; j++) 
				{
					obj.prop = obj.prop[nestedProps[j]];  
				}
				tmpArr.push(obj);
			}
			tmpArr.sortOn("prop",options);
			for (i = 0; i < tmpArr.length; i++) 
			{
				vector[i] = tmpArr[i].parent;
			}
			return vector;
		}
		
		// copy a vector : http://stackoverflow.com/questions/10175810/deep-copy-of-vector-in-as3
		
		/**
		 *  add elements at position
		 * */
		public static function addVectorElements(vector:*, elementsToAdd : *, newIndex:int ):void
		{
			for (var i:int = 0; i < elementsToAdd.length; i++) 
			{
				vector.splice(newIndex+i, 0, elementsToAdd[i]);
			}
		}
		
		/**
		 *  transform vector to array
		 * */
		public static function toArray(vector:* ):Array
		{
			var tmpArr:Array = [];
			for (var i:int = 0; i < vector.length; i++) 
			{
				tmpArr.push(vector[i]);
			}
			return tmpArr;
		}
		
		

		
		
		/**
		 * Compare function to srot albums by most recent id
		 * DESCENDING
		 * */
		/*
		public static function sortMostRecentID(a:AlbumVo,b:AlbumVo):int 
		{
			if(FolderManager.instance.isProjectFolder(  b.name ) ) return 1;
			return a.mostRecentId < b.mostRecentId ? 1 : -1;
		}
		*/
	}
}