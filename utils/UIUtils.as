package utils
{
	import be.antho.button.RollButton;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.geom.Point;

	public class UIUtils
	{
		
		/**
		 * simple yellow small button
		 */
		public static function setupSimpleButton( buttonClip:MovieClip, text:String):void
		{
			buttonClip.txt.txt.autoSize = "left";
			buttonClip.txt.txt.htmlText = text;
			buttonClip.bg.bg.width = buttonClip.txt.txt.width + buttonClip.txt.x + 15;
		}
		
		
		
		
		/**
		 * save init pos of selected clips in "initPos" variables.
		 * Clips must be of type movieClip
		 */
		public static function saveClipInitCoords( elements : Array ):void
		{
			for (var i:int = 0; i < elements.length; i++) 
			{
				var obj:MovieClip = elements[i] as MovieClip;
				obj.initPos = new Point(obj.x, obj.y);
			}
		}
		
		
		/**
		 * save init pos of selected clips in "initPos" variables.
		 * Clips must be of type movieClip
		 */
		public static function removeAllChildren( clip : DisplayObjectContainer ):void
		{
			var numChildren : uint = clip.numChildren;
			for (var i:int = 0; i < numChildren; i++) 
			{
				clip.removeChildAt(numChildren-1-i);
			}
		}
		
		/**
		 *
		 */
		public static function removeAllChildrenAndDestroy( clip : DisplayObjectContainer ):void
		{
			var numChildren : uint = clip.numChildren;
			var child:DisplayObject;
			for (var i:int = 0; i < numChildren; i++) 
			{
				child = clip.getChildAt(numChildren-1-i);
				Object(child).destroy();
				if(child.parent) child.parent.removeChild(child);
			}
		}
		
		

	}
}