/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/
package utils
{
	import com.google.analytics.GATracker;
	
	import flash.display.DisplayObject;
	
	public class Analytic
	{
		private static var gaTracker:GATracker;
		
		public static function init( display:DisplayObject, AnalyticID:String, debug:Boolean = false):void
		{
			gaTracker = new GATracker( display, AnalyticID, "AS3", debug );
		}
		
		public static function trackPage(page:String):void
		{
			if(!gaTracker) return; // fail silentely
			gaTracker.trackPageview(page);
		}
		
		public static function trackEvent(cat:String, action:String, id:*):void
		{
			if(!gaTracker) return; // fail silentely
			gaTracker.trackEvent(cat, action, id);
		}
	}
}