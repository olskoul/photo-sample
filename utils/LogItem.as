package utils
{
	public class LogItem
	{
		public static const TYPE_LOG : int = 0;
		public static const TYPE_DEV : int = 1;
		public static const TYPE_WARN : int = 2;
		public static const TYPE_ERROR : int = 3;
		
		// vars
		public var type : int;
		public var value : String;
		
		public function LogItem(value : String, type:int = TYPE_LOG):void
		{
			this.type = type;
			this.value = value;
		}
		
		// retrieve color depending on type
		public function get color():int
		{
			if(type == 0) return 0x00ff00;
			if(type == 1) return 0x2fccff;
			if(type == 2) return 0xE0AF1B;
			return 0xff0000;
		}
		
	}
}