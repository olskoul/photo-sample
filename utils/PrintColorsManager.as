package utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class PrintColorsManager
	{
		// color used for print ( same position as color used to display ) ( FROM NICO RGB !!)
		private static var displayColorDictionnary : Array = [
			0XFFFFFF,0XFBB481,0XEFA4FF,0XC999FF,0X5D95FF,0X3391D4,0XA1D433,
			0XDFFF7E,0XEF5E66,0XEF56B7,0XA97EDF,0X4A85FF,0X3399BC,0X99E733,
			0XDFFF28,0XF7333B,0XEF0099,0X9A7ECF,0X467DEF,0X206CCF,0X53B930,
			0XFFCC00,0XFF4633,0XBF0081,0X927ED7,0X203C9C,0X0891C7,0X43A930,
			0XD79928,0XC72333,0XC1007E,0X5C66CF,0X2E3484,0X4BB176,0X289133,
			0XDF9100,0XCF1B33,0X96006E,0X645ECF,0X46289C,0X63A94B,0X187923,
			0XCF9900,0XC12820,0X87007C,0X9C6EFF,0X462074,0X3B9133,0X007630,
			0XD46600,0XA95028,0X56004E,0X3D56D7,0X0B3381,0X2B9189,0X285E28,
			0XAF3B00,0XA94818,0X380056,0X304BAF,0X3C3279,0X239156,0X083C00,
			0XB94630,0X662B18,0X4F3028,0X2E439F,0X2A1046,0X33A943,0X003300,
			0XA17E18,0X492326,0X230800,0X263B97,0X183371,0X00666E,0X00435B,
			0X996618,0X646974,0X201828,0X3A36A9,0X37314E,0X3B4E79,0X002F00,
			0X896930,0X889098,0X4B5264,0X2B2864,0X222346,0X505868,0X181818,
			0X815628,0X8A8892,0X6C6970,0X4E4C56,0X20182B,0X383840,0X000000 ];
		
		
		// Color used to display in editor ( FROM RGB PALETTE IMAGE )
		// DO NOT CHANGE IT
		private static var printColorDictionnary : Array = [
			0XFFFFFF,0XF4AB8D,0XF0AFCF,0XBAB0D6,0X63C5F1,0X04B3CC,0XC6D12D,
			0XFDF497,0XEB6863,0XE86CA6,0X907FBB,0X24B3E8,0X16B1BD,0X95BF3D,
			0XFCEE3B,0XE7212F,0XE50585,0X8D90C5,0X2CA1DB,0X00A3D8,0X6FB345,
			0XFDCB0F,0XE94F2E,0XC71783,0X6377B9,0X0461AB,0X00A2BA,0X32A84A,
			0XF6B719,0XE52128,0XAB2175,0X3681C3,0X03519D,0X4BB692,0X08A14A,
			0XF49821,0XD61F28,0X8F2682,0X436FB6,0X263D8E,0X57B36E,0X089547,
			0XF4A41E,0XB22026,0X732A83,0X8AA0D2,0X312F83,0X26AB5A,0X0D8242,
			0XED7A24,0X933D20,0X5A184F,0X1A81C4,0X02508E,0X009F96,0X0D6835,
			0XE85325,0X8B2F1D,0X1F194F,0X026EB6,0X213E79,0X009A71,0X034D2C,
			0XCC4A27,0X4C1E11,0X4C2815,0X0E6AB3,0X1E2351,0X00984B,0X182F1B,
			0XC3902C,0X2C1310,0X260F0F,0X085CA7,0X044C7C,0X008A92,0X056068,
			0XC37F2A,0X7E8C97,0X151226,0X0A4F91,0X3A405C,0X415874,0X0F2E1C,
			0XB58636,0XA2ADB3,0X5C7280,0X1D3F74,0X163158,0X585C68,0X1A1B1F,
			0XA26127,0XB0AEB3,0X818085,0X4F4F54,0X111327,0X37363B,0X000000 ];
		
		
		/**
		 *  returns an array of color 
		 */
		public static function getVisibleColorList():Array
		{
			return displayColorDictionnary;
		}
		
		
		/**
		 *  returns an array of hex values
		 */
		/*
		public static function getPantoneColorList():Vector.<Number>
		{
			// first we sort colors
			
			// then we export hex values as color numbers
			var hexPantoneList : Vector.<Number> = new Vector.<Number>(colorDictionnary.length);
			for (var i:int = 0; i < colorDictionnary.length; i++) 
			{
				hexPantoneList[i] = hexToInt( colorDictionnary[i][4] );
			}
			return hexPantoneList;
		}
		*/
		
		/**
		 * retrieve r, g, b for hex pantone value
		 * example : [255,7,79]
		 */
		/*
		public static function getRGBForPantoneColor( color : Number ):Array
		{
			for (var i:int = 0; i < colorDictionnary.length; i++) 
			{
				if(colorDictionnary[i][4] == intToHex( color )) return [colorDictionnary[1], colorDictionnary[2], colorDictionnary[3]];
			}
			return null;
		}
		*/
		
		public static function getPrintColor( color:Number ):Number
		{
			if(Debug.USE_OLD_COLOR_SYSTEM) return color;
			for (var i:int = 0; i < displayColorDictionnary.length; i++) 
			{
				if(color == displayColorDictionnary[i])
					return printColorDictionnary[i];
			}
			Debug.warn("No print color found for color : "+intToHex(color));
			return color;
		}
		
		
		public static function replacePrintColorInText( text:String ):String
		{
			if(Debug.USE_OLD_COLOR_SYSTEM) return text;
			// TODO
			// example
			//<P ALIGN="CENTER"><FONT FACE="GillSans" SIZE="55" COLOR="#04B3CC" LETTERSPACING="0" KERNING="0">THIS IS A TEST</FONT></P>
			return text;
		}
		
		
		/** 
		 * generate an horizontal color palette based on vertical palette list we have
		 */
		public static function generateColorPalette():BitmapData
		{
			var W:Number = 15;
			var H:Number = 10;
			var XGap:Number = 2;
			var YGap:Number = 2;
			var cols : int = 7;
			var rows : int = 14;
			var bmd : BitmapData = new BitmapData( (W+XGap)*cols+XGap,(H+YGap)*rows+YGap,false, 0xffffff);
			var rect:Rectangle;
			bmd.lock();
			for (var i:int = 0; i < displayColorDictionnary.length; i++) 
			{	
				rect = new Rectangle(XGap + (i%cols)*(W+XGap), YGap + Math.floor(i/cols)*(H+YGap),W,H);
				bmd.fillRect(rect, displayColorDictionnary[i]);
				
			}
			bmd.unlock();
			
			return bmd;
		}
		
		
		/**
		 * give hex string from int color
		 */
		public static function intToHex ( color : Number, prefix:String = "0x" ) : String
		{	
			var hexString : String = ""+color.toString( 16 ); 
			while( hexString.length < 6 )
			{
				hexString = '0' + hexString;
			}
			
			return (prefix + hexString).toUpperCase();
		}
		
		
		/**
		 * give int color from hex string
		 */
		public static function hexToInt ( color : String ) : Number
		{
			// remove possible prefix
			color = color.split("0x").join("");
			color = color.split("#").join("");
			
			// retrieve int
			return parseInt( color, 16 );
		}
		
		private static function rgbToHex(R,G,B):String
		{
			return toHex(R)+toHex(G)+toHex(B);
		}
		private static function toHex(n):String 
		{
			n = parseInt(n,10);
			if (isNaN(n)) return "00";
			n = Math.max(0,Math.min(n,255));
			return "0123456789ABCDEF".charAt((n-n%16)/16)
				+ "0123456789ABCDEF".charAt(n%16);
		}
		
		
		
		
		/**
		 * ------------------------------------ HELPER TO GENERAGE COLOR LIST BASED ON IMAGE -------------------------------------
		 */
		/*
		[Embed(source="/assetsEmbed/images/palette.png")] 
		private static var embedPalette:Class;
		
		public static function generateAndTracePaletteColorList():void
		{
			// import image
			var img:Bitmap = new embedPalette() as Bitmap;
			var imgData:BitmapData = img.bitmapData;
			
			var cols : int = 7;
			var rows : int = 14;
			var colorPickW : Number = imgData.width/cols;
			var colorPickH : Number = imgData.height/rows;
			
			// trace formated array
			var pickColorPoint : Point;
			var color:Number;
			var colorString : String;
			var list : String = "";
			for (var i:int = 0; i < rows; i++) 
			{
				for (var j:int = 0; j < cols; j++) 
				{
					// find color
					pickColorPoint = new Point( colorPickW*j + colorPickW*.5, colorPickH*i + colorPickH*.5 );
					color = imgData.getPixel(pickColorPoint.x, pickColorPoint.y);
					colorString = intToHex(color);
					
					// add to color string
					list += colorString + ",";
					
					//
				}
				list += "\n";
			}
			trace("///////////////////////////////////////////////////");
			trace("COLOR LIST : ");
			trace(list);
			trace("///////////////////////////////////////////////////");
			
		}
		*/
		
		private static const printRgbColors : String = "255,255,255;251,180,129;239,164,255;201,153,255;93,149,255;51,145,212;161,212,51;223,255,126;239,94,102;239,86,183;169,126,223;74,133,255;51,153,188;153,231,51;223,255,40;247,51,59;239,0,153;154,126,207;70,125,239;32,108,207;83,185,48;255,204,0;255,70,51;191,0,129;146,126,215;32,60,156;8,145,199;67,169,48;215,153,40;199,35,51;193,0,126;92,102,207;46,52,132;75,177,118;40,145,51;223,145,0;207,27,51;150,0,110;100,94,207;70,40,156;99,169,75;24,121,35;207,153,0;193,40,32;135,0,124;156,110,255;70,32,116;59,145,51;0,118,48;212,102,0;169,80,40;86,0,78;61,86,215;11,51,129;43,145,137;40,94,40;175,59,0;169,72,24;56,0,86;48,75,175;60,50,121;35,145,86;8,60,0;185,70,48;102,43,24;79,48,40;46,67,159;42,16,70;51,169,67;0,51,0;161,126,24;73,35,38;35,8,0;38,59,151;24,51,113;0,102,110;0,67,91;153,102,24;100,105,116;32,24,40;58,54,169;55,49,78;59,78,121;0,47,0;137,105,48;136,144,152;75,82,100;43,40,100;34,35,70;80,88,104;24,24,24;129,86,40;138,136,146;108,105,112;78,76,86;32,24,43;56,56,64;0,0,0"
		public static function generateAndTracePrintRgbColorList():void
		{
			// create an array of rgb colors
			var rgbColors : Array = printRgbColors.split(" ").join("").split(";");
			
			var cols : int = 7;
			var rows : int = 14;
			
			// trace formated array
			var color:Number;
			var colorString : String;
			var list : String = "";
			var r:Number;
			var g:Number;
			var b:Number;
			var index:Number = 0;
			for (var i:int = 0; i < rows; i++) 
			{
				for (var j:int = 0; j < cols; j++) 
				{
					// find rgb
					r = parseInt(rgbColors[index].split(",")[0]);
					g = parseInt(rgbColors[index].split(",")[1]);
					b = parseInt(rgbColors[index].split(",")[2]);
					
					colorString = "0X" + rgbToHex(r,g,b);
					//colorString = intToHex(color);
					// add to color string
					list += colorString + ",";
					index ++;
				}
				list += "\n";
			}
			trace("///////////////////////////////////////////////////");
			trace("PRINT COLOR LIST : ");
			trace(list);
			trace("///////////////////////////////////////////////////");
			
		}
	}
}
