package utils
{
	import data.ProductsCatalogue;

	public class SpineUtils
	{
		public function SpineUtils()
		{
		}
		/**
		 * Helper: Get the album spine width based on the docID and paper 
		 * Paper is that page put between each pages of the book, it impact the width of the spine
		 * result in mm !!!
		 * 
		 * Soft cover = Casual
		 * Hard cover = Classic, comptemporary et trendy
		 * 
		 * Q* = trendy
		 * QA* = trendy
		 * QB* = trendy
		 * 
		 * QE* = Casual
		 * QD* = Casual
		 * A* = Casual
		 * B* = Casual
		 * */
		public static function customSpineWidth(docid:String, paper:Boolean, coated:Boolean, pagePaperQuality:String):Number {
			// 
			var result:Number = 0;
			switch(docid)
			{
				/* SOFT COVERS */
				
				case "A20": // soft cover A5 portrait
				case "B20": // soft cover A4 portrait
				case "QD20": // soft cover square
				case "QE20": // soft cover square
					result = 2;
					break;
				
				case "A30": // soft cover A5 portrait
				case "B30": // soft cover A4 portrait
				case "QD30": // soft cover square
				case "QE30": // soft cover square
					result = 3;
					break;
				
				case "A40": // soft cover A5 portrait
				case "B40": // soft cover A4 portrait
				case "QD40": // soft cover square
				case "QE40": // soft cover square
					result = 3.8;
					break;
				case "A50": // soft cover A5 portrait
				case "B50": // soft cover A4 portrait
				case "QD50": // soft cover square
				case "QE50": // soft cover square
					result = 4.5;
					break;
				
				case "A60": // soft cover A5 portrait
				case "B60": // soft cover A4 portrait
				case "QD60": // soft cover square
				case "QE60": // soft cover square
					result = 5.3;
					break;
					
				case "A70": // soft cover A5 portrait
				case "B70": // soft cover A4 portrait
				case "QD70": // soft cover square
				case "QE70": // soft cover square
					result = 6;
					break;
					
				case "A80": // soft cover A5 portrait
				case "B80": // soft cover A4 portrait
				case "QD80": // soft cover square
				case "QE80": // soft cover square 
					result = 6.8;
					break;
					
				case "A90": // soft cover A5 portrait
				case "B90": // soft cover A4 portrait
				case "QD90": // soft cover square
				case "QE90": // soft cover square 
					result = 7.5;
					
				case "A100": // soft cover A5 portrait
				case "B100": // soft cover A4 portrait
				case "QD100": // soft cover square
				case "QE100": // soft cover square 
					result = 8.3;
					break;
					
				case "A110": // soft cover A5 portrait
				case "B110": // soft cover A4 portrait
				case "QD110": // soft cover square
				case "QE110": // soft cover square 
					result = 9.1;
					break;
					
				case "A120": // soft cover A5 portrait
				case "B120": // soft cover A4 portrait
				case "QD120": // soft cover square
				case "QE120": // soft cover square 
					result = 10;
					break;
					
					
				/* HARD COVERS TRENDY aka square*/
					
					
				case "Q20":	// square 20
				case "QA20":	// square 20
				case "QB20":	// square 20
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 8 : 8;
							break;
					}
					
					break;
				
				case "Q30":	// square 30
				case "QA30":	// square 30
				case "QB30":	// square 30
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 10 : 10;
							break;
					}
					break;
				
				case "Q40":	// square 40
				case "QA40":	// square 40
				case "QB40":	// square 40
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 10 : 10;
							break;
					}
					break;
				
				case "Q50":	// square 50
				case "QA50":	// square 50
				case "QB50":	// square 50
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 14 : 14;
							break;
					}
					break;
				
				case "Q60":	// square 60
				case "QA60":	// square 60
				case "QB60":	// square 60
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 10 : 10;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 14 : 14;
							break;
					}
					break;
				
				case "Q70":	// square 70
				case "QA70":	// square 70
				case "QB70":	// square 70
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 10 : 10;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 14 : 14;
							break;
					}
					break;
				
				case "Q80":	// square 80
				case "QA80":	// square 80
				case "QB80":	// square 80
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 10 : 10;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 16 : 16;
							break;
					}
					break;
				
				case "Q90":	// square 90
				case "QA90":	// square 90
				case "QB90":	// square 90
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 18 : 18;
							break;
					}
					break;
				
				case "Q100":	// square 100
				case "QA100":	// square 100
				case "QB100":	// square 100
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 20 : 20;
							break;
					}
					break;
				
				case "Q110":	// square 110
				case "QA110":	// square 110
				case "QB110":	// square 110
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 20 : 20;
							break;
					}
					break;
				
				case "Q120":	// square 120
				case "QA120":	// square 120
				case "QB120":	// square 120
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 20 : 20;
							break;
					}
					break;
				
				
				/* HARD COVERS CLASSICS & CONTEMPORARY*/
				
								
				case "S20":	
				case "M20":	
				case "L20":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							if(paper)
								result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 8 : 8;
							if(paper)
								result = (coated) ? 8 : 8;
							break;
					}
					
					break;
				
				case "S30":	
				case "M30":	
				case "L30":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							if(paper)
								result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 10 : 10;
							if(paper)
								result = (coated) ? 10 : 10;
							break;
					}
					break;
				
				case "S40":	
				case "M40":	
				case "L40":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							if(paper)
								result = (coated) ? 8 : 8;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 10 : 10;
							if(paper)
								result = (coated) ? 14 : 14;
							break;
					}
					break;
				
				case "S50":	
				case "M50":	
				case "L50":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 8 : 8;
							if(paper)
								result = (coated) ? 10 : 10;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 14 : 14;
							break;
					}
					break;
				
				case "S60":	
				case "M60":	
				case "L60":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 10 : 10;
							if(paper)
								result = (coated) ? 10 : 10;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 14: 14;
							break;
					}
					break;
				
				case "S70":	
				case "M70":	
				case "L70":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 10 : 10;
							if(paper)
								result = (coated) ? 14: 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 16: 16;
							break;
					}
					break;
				
				case "S80":	
				case "M80":	
				case "L80":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 10 : 10;
							if(paper)
								result = (coated) ? 14: 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 16 : 16;
							if(paper)
								result = (coated) ? 18: 18;
							break;
					}
					break;
				
				case "S90":	
				case "M90":	
				case "L90":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 14: 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 18 : 18;
							if(paper)
								result = (coated) ? 22: 22;
							break;
					}
					break;
				
				case "S100":	
				case "M100":	
				case "L100":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 14: 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 20 : 20;
							if(paper)
								result = (coated) ? 22: 22;
							break;
					}
					break;
				
				case "S110":	
				case "M110":	
				case "L110":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 14: 14;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 20 : 20;
							if(paper)
								result = (coated) ? 22: 22;
							break;
					}
					break;
				
				case "S120":	
				case "M120":	
				case "L120":	
					switch(pagePaperQuality)
					{
						case ProductsCatalogue.PAPER_QUALITY_170:
							result = (coated) ? 14 : 14;
							if(paper)
								result = (coated) ? 16: 16;
							break;
						
						case ProductsCatalogue.PAPER_QUALITY_250:
							result = (coated) ? 20 : 20;
							if(paper)
								result = (coated) ? 26: 26;
							break;
					}
					break;
			}
			
			return result;
		}
	}
}