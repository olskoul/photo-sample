/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/
package utils 
{
	import com.bit101.components.PushButton;
	
	import flash.display.BitmapData;
	import flash.display.JPEGEncoderOptions;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.errors.ScriptTimeoutError;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	
	import flash.events.UncaughtErrorEvent;
	import flash.external.ExternalInterface;
	import flash.net.FileReference;
	
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.utils.ByteArray;
	
	import be.antho.data.DateUtil;
	import be.antho.data.ResourcesManager;
	
	import data.Infos;
	import deng.fzip.FZip;
	
	import manager.UserActionManager;
	
	import ru.inspirit.net.MultipartURLLoader;
	
	import service.ServiceManager;
	
	import view.popup.PopupAbstract;
	import view.popup.PopupContact;
	import view.popup.PopupError;
	import view.popup.PopupSendLogs;
	
	CONFIG::offline
	{
		import com.adobe.utils.StringUtil;
		import offline.RecoveryManager;
		import offline.data.LocalDB;
		import offline.manager.LocalStorageManager;
		
		import flash.desktop.NativeApplication;
		
		import be.antho.data.SharedObjectManager;
		import data.ProductsCatalogue;
		import flash.net.URLLoaderDataFormat;
		import flash.filesystem.FileMode;
		import flash.events.IOErrorEvent;
		import flash.events.SecurityErrorEvent;
		import flash.filesystem.FileStream;
		import flash.filesystem.File;
		import flash.filesystem.FileMode;
		import flash.filesystem.FileStream;
		import flash.display.NativeMenu;
		import flash.display.NativeMenuItem;
		import offline.manager.OfflineAssetsManager;
	}
	
	
	public class Debug 
	{
		////////////////////////////////////////////////////////
		// DEBUG PROPERTIES
		// TODO : before each release, check those values...
		// TODO : DO NOT FORGET TO RESET onLeave return in javascript !
		////////////////////////////////////////////////////////
		
public static var IS_DEV:Boolean 					= true; 	// Forces the value of editorHost to be editordev.tictacphoto.com -> Set this to false if this release goes on staging or to prod
/*FV*/	public static var CONSOLE:Boolean 					= false; 	// add a console to see logs and error (right click, toggle console) (can be set using flashvar console = true

		// Sort by most used 
		public static var SHOW_TRACE:Boolean 				= false; 	// show trace or not		
		public static var SHOW_STATS:Boolean 				= false; 	// display memory and speed stats
		public static var SHOW_PAGE_INFOS :Boolean 			= false;	// show page number on top left of page area
		public static var NO_DEV_MAIL_LOG :Boolean 			= false;	// Don't send report to dev on error
		public static var DO_NOT_MAXIMIZE :Boolean 			= false;	// Don't maximize window

public static var USE_LOCAL_CONFIG:Boolean 			= false;		// Offline only, forces the app to load config.xml from ApplicationDirectory.
public static var USE_LOCAL_RESSOURCE:Boolean 		= false;		// Offline only, forces the app to load ressources from ApplicationDirectory.

		public static var SHOW_REAL_FOLDER_NAME :Boolean 	= false;	// show real names instead of project names
		
		public static var OPEN_CONSOLE_ON_ERROR:Boolean 	= false 	// open console on error
		public static var OPEN_CONSOLE_ON_WARN:Boolean 		= false; 	// open console on warnings

		public static var CLEAR_ALL_FLASH_COOKIE:Boolean 	= false; 	// clear all app related cookie or not
		public static var ENUMERATE_FONTS :Boolean 			= false;	// Log fonts properties at startup
		public static var EXPORT_PROJECT_SIZE :Boolean 		= false;	// trace/export/update the size of all projects in mm at launch
		public static var NO_STATS_SERVICE :Boolean 		= false;	// log info on antho.be

		public static var FORCE_LANG :String 				= null;		// force specific lang resources ( DON'T FORGET TO PUT IT BACK TO null ) 
		public static var FORCE_TYPE :String 				= null;		// Default : null -> Force specific project type > ProductsCatalogue.CLASS_ALBUM;
		public static var DO_NOT_LOAD_RECENT :Boolean 		= false;	// load default project from session
		public static var NO_BROWSER_LEAVE_WARNING :Boolean	= false;	// Don't Show browser leave warning
		public static var TRACE_PAGE_FRAMES :Boolean 		= false;	// traces the page frame depth info when showing a page
		public static var SHOW_PASSWORD :Boolean 			= false;	// show password in login popup (no ***)
		public static var SHOW_TEXT_BOUNDS :Boolean 		= false;	// show bounds of text in frame text
		public static var USE_OLD_BKG_FILL_SYSTEM:Boolean 	= false; 	// use old bkg style (no image send for CMYK Problem) // EDIT : keep this to true untill fully tested
		public static var DO_NOT_USE_EXIF_ROTATION:Boolean 	= false; 	// do not use the exif rotation fix
		//public static var HIDE_UPLOAD_BTN_FOR_WEB:Boolean 	= false; 	// hide upload information btn on web
		
		
		// user session debug / preview
/*FV*/	public static var DO_NOT_ALLOW_SAVE :Boolean 			= false;	// Do not allow to save project ( if in debug session for example )
/*FV*/	public static var DO_NOT_UPLOAD_PHOTOS :Boolean 		= false;	// Debug value to avoid uploading images and test project with temp photos
		public static var USE_DEBUG_PROJECT_STRING :Boolean	 	= false;	// use debug project string stored in ProjectDebugString.value
		public static var USE_DEBUG_SESSION_XML :Boolean 		= false;	// use debug session string string stored in sessionVo

		// PROD VALUES THAT MUST STAY TRUE (for now)
		public static var DO_NOT_SHOW_TUTORIAL:Boolean			= false;		// DO NOT SHOW TUTORIAL FOR NOW
		public static var USE_OLD_ALBUM_SIZE_SYSTEM:Boolean		= false;	// DO NOT USE NEW UPGRADE SIZE SYSTEM (and put back old xmls)
		public static var USE_OLD_COLOR_SYSTEM :Boolean 		= false;	/// do we use old palettes system or new one
public static var DO_NOT_DO_APP_CONTENT_UPDATE :Boolean = true;		// Flag for skiping content updates
		
		
		// order values
/*FV*/	public static var ORDER_DEBUG :Boolean 					= true;		// Use debug mode for ordering system > we use debug job ID
/*FV*/  public static var ORDER_TO_PDF :Boolean 				= false;		// Instead of sending final ORDER, we generate pdf
		public static var ORDER_DEMO_PDF :Boolean 				= false;	// Instead of sending final ORDER, we use an hard coded order to test
		public static var ORDER_COVER_ONLY :Boolean 			= false;	// make cover ordering only (with ordersuffix)
		public static var ORDER_NO_COVER :Boolean 				= false;	// skip cover ordering
		public static var WORK_WITH_ONLY_3PAGES :Boolean 		= false;	// To speed up process (print) we work only with two pages
/*(!)*/	public static var PRINT_SPECIFIC_PAGES:String			= null;	// print specific page only ( DON'T FORGET TO PUT IT BACK TO null ) (string : 1 to print page 1 ::  12,14,18 to print page 12 and 14 and 18

		// Print color system
		public static var GENERATE_PRINT_COLORS :Boolean 		= false;		// Based on the palette in assetEmbed/images, we generate an array of colors to use in application
		public static var GENERATE_PRINT_COLORS_FROM_CSV:Boolean= false;			// based on csv with rgb colors from nico, we generate an array of hex colors
		
		// offline
public static var OPEN_OFFLINE_LAST_FOLDER:Boolean		= true;			// open last folder that was browsed on this project
		public static var OPEN_OFFLINE_LAST_PROJECT:Boolean		= false;		// open last opened project / otherwise open dashboard
		
		
		// cookie
		public static var CLEAR_OFFLINE_APP_XML :Boolean 			= false; 		// Flag use to clear all offline XMl (project, document, menu, layouts, resources..)
		public static var CLEAR_OFFLINE_ASSETS :Boolean 			= false;			// Flag use to cleat all offline content (backgrounds, cliparts, frames)
		public static var CLEAR_OFFLINE_CUSTOM_FOLDER :Boolean 		= false;			// Flag use to cleat all offline custom bkg and layouts

		public static var FORCE_NO_INTERNET :Boolean 				= false;	// Flag use to simulate complete offline
		public static var USE_DEBUG_UPDATE :Boolean 				= false;		// use debug update = use antho and jonny server for updates
		
		public static var FORCE_TERMS_AND_CONDITIONS :Boolean 		= false;	// flag to force general conditions to be shown at startup
		public static var DISPLAY_MEMORY_ON_LOGS:Boolean			= false;	// !!! WARNING !!! : this is CPU intensive, use with precaution ! do not display the current memory on each log

/*(!)*/	public static var SIMULATE_IMPORT_ERRORS:Boolean			= false; 	// DO NOT FORGET TO SET IT TO FALSE, simulate some errors on imports
		
		// adding a prefix to the build version 
		public static const BUILD_PREFIX : String = ""; 		// nothing for merged versions (albums, calendars, canvas, cards)
		
		
		
		////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////
		// ERROR TYPES
		////////////////////////////////////////////////////////
		public static const ERROR_DEFAULT : String = "ERROR_DEFAULT";
		public static const ERROR_SID_NOT_FOUND : String = "ERROR_SID_NOT_FOUND";
		public static const ERROR_CONNECTION : String = "ERROR_CONNECTION";
		public static const ERROR_CONNECTION_SECURITY : String = "ERROR_CONNECTION_SECURITY";
		public static const ERROR_CONNECTION_IO : String = "ERROR_CONNECTION_IO";
		public static const ERROR_CONNECTION_TIMEOUT : String = "ERROR_CONNECTION_TIMEOUT";
		public static const ERROR_SCORE : String = "ERROR_SCORE";
		public static const ERROR_GLOSSARY : String = "ERROR_GLOSSARY";
		////////////////////////////////////////////////////////
		
		private static var MAX_CONSOLE_LENGTH : int = 1000000;
		private static var MAX_CONSOLE_LOG_LENGTH : int = 10000;
		private static var root:Sprite;
		private static var stage:Stage;
		private static var console:TextField ;
		private static var logs:Vector.<LogItem> = new Vector.<LogItem>;
		private static var logString : String;
		private static var errorSentCount:int = 0;
		private static var warnSentCount:int = 0;
		private static var sessionStartTime : Date = new Date();
		
		private static var currentTextColor:Number;
		
		// buttons
		private static var closeConsoleBtn : PushButton;
		private static var clearConsoleBtn : PushButton;
		private static var showFullLogBtn : PushButton;
		private static var sendConsoleBtn : PushButton;
		
		
		/**
		 * init debugger
		 * add console to stage
		 */
		public static function init($root:Sprite):void
		{
			clearConsole();
			
			root = $root;
			stage = root.stage;	
			stage.stageFocusRect = false;
			if(CONSOLE) addConsole();
			//createContextMenu();
			
			
			// global error handling
			root.loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, onGlobalErrorHandler);
		}
		
		
		private static var logStartTime : int;
		private static function get logTime():String
		{
			//if(!logStartTime) logStartTime = new Date().time;
			var now:Date = new Date();
			
			if(DISPLAY_MEMORY_ON_LOGS)
			{
				var currentMemory:String = "" + Number((System.privateMemory * 0.000000954).toFixed(3)) + "mb";
				return "["+now.hoursUTC+ ":" + now.minutesUTC+":" + now.secondsUTC + ":"+now.millisecondsUTC + "]" + "["+currentMemory+"]";
			}
			else 
				return "["+now.hoursUTC+ ":" + now.minutesUTC+":" + now.secondsUTC + ":"+now.millisecondsUTC + "]";
		}
		
		
		/**
		 *  simple log
		 */
		public static function log(msg:String):void
		{
			echo(logTime + "[LOG] " + msg, LogItem.TYPE_LOG);
		}
		
		/**
		 *  something is strange.. but you don't want to report it as an error.. we have what you need, just warn it
		 */
		public static function warn(msg:String):void
		{
			echo(logTime + "[WARNING] " + msg, LogItem.TYPE_WARN);
			if(OPEN_CONSOLE_ON_WARN && !console.parent){
				toggleConsole();
			}
		}
		
		/**
		 *  something is strange.. but you don't want to report it as an error.. we have what you need, just warn it
		 *  But you still want to send mail to dev to report it
		 */
		public static function warnAndSendMail(msg:String):void
		{
			warn(msg);
			if( !NO_DEV_MAIL_LOG) {
				var errorString : String = logTime + "[SILENT WARNING] " + msg;
				sendDevMail(errorString, true);
			}
		}
		
		
		/**
		 *  log an error
		 *  error type is a string used to display a message in the popup
		 *  error detail gives more detail about the error if needed
		 *  error log is the content of the error that will be send to backend
		 */
		public static function error( errorMessage : String , errorType:String = ERROR_DEFAULT, moreDetail:String = ""):void
		{
			
			var title : String = getErrorMessage(errorType);
			echo(logTime + "[ERROR] [" + errorType + "] "+errorMessage + "\n\nDetails : "+moreDetail, LogItem.TYPE_ERROR);
			
			// remove projet to open on offline to be sure we do not enter in an infinite loop
			if(Infos.IS_DESKTOP)
			{
				Infos.prefs.projectToOpen = null;
				Infos.prefs.saveGeneralPrefs();
				
				// save app screenshot to be sent later
				saveScreenshot();
			}
			
			// IF dev we toggle console
			if(OPEN_CONSOLE_ON_ERROR && !console.parent){
				toggleConsole();
			}
			// if not dev, we open error window
			else if( !OPEN_CONSOLE_ON_ERROR){
				openErrorWindow();
				Analytic.trackPage("POPUP_ERROR");
			}
			
			if(sendConsoleBtn) sendConsoleBtn.visible = true;
			if(clearConsoleBtn) clearConsoleBtn.visible = false;
			
			if( !NO_DEV_MAIL_LOG) {
				// send
				var errorString : String = logTime + "[ERROR] [" + errorType + "] "+errorMessage + "\n\nDetails : "+moreDetail;
				sendDevMail(errorString);
				
				// once user has one error, he can't have another one (just the popup to display)
				NO_DEV_MAIL_LOG = true;
				
				// offline errors does send user files and try to save recovery
				CONFIG::offline
				{
					sendUserFiles(true, errorString);
				}
			}
			
			// try to save recovery file for offline
			CONFIG::offline
			{
				RecoveryManager.instance.SaveProjectRecovery(Infos.project);
			}
				
			
			// remove browser leave warning once an error occured
			NO_BROWSER_LEAVE_WARNING = true;
		}
		
		
		/**
		 * A ticked id for a session, allows to verify mails from the same session (maybe gmail will even order them automagically)
		 */
		private static var _ticketId : String;
		public static function get ticketId():String
		{
			if(!_ticketId) _ticketId = "T"+new Date().time;
			return _ticketId;
		}
		
		
		/**
		 * send mail to dev
		 */
		public static function sendDevMail( message:String, isSilentWarning:Boolean = false, isFeedback:Boolean = false, onSuccessHandler:Function = null, onErrorHandler:Function = null):void
		{
			// security for max mail sent by session (this avoids crazy loop bug to send 100 mails at once)
			if(!isFeedback) 
			{
				if(isSilentWarning)
				{
					warnSentCount ++;
					if(warnSentCount > 3) return;
				}
				else 
				{
					errorSentCount ++;
					if(errorSentCount > 3) return;
				}
			}
			
			
			if(isFeedback) message = getLogEmailContent("USER FEEDBACK", message );
			else message = getLogEmailContent("ERROR MESSAGE", message);
			
			ServiceManager.instance.sendDevMail(message, Infos.session.email, ticketId, isSilentWarning, isFeedback, onSuccessHandler,onErrorHandler );
		}
		
		
		public static function getLogEmailContent( mailHeaderText:String, message:String ):String
		{
			// session details we want to log
			var sessionDetails : String = "";// "::\n:: INFOS ::\n::\n\n";
			sessionDetails += "BUILD : " + Infos.buildString;
			sessionDetails += "\nLANG : " + Infos.lang;
			sessionDetails += "\nEMAIL : " + Infos.session.email;
			sessionDetails += "\nURL : " + stage.loaderInfo.url;
			
			// for offline we need some specific info to ease debug
			CONFIG::offline
			{
				//sessionDetails += "\nUSER : " + Infos.session.userId;
				sessionDetails += "\nUSER FOLDER : " + LocalDB.USER_FOLDER.nativePath;
			}
				
			var now : Date = new Date();
			var duration : String = DateUtil.millisecondsToHHMMSS( now.time - sessionStartTime.time );
			sessionDetails += "\nDURATION : " + duration + " sec (start:" + sessionStartTime.toString() + " end: " + now.toString() + " )";
			sessionDetails += "\nSWF MEMORY : " + Number((System.totalMemory * 0.000000954).toFixed(3)) + "mb (free allocated : " + Number((System.freeMemory * 0.000000954).toFixed(3)) + "mb)";
			sessionDetails += "\nFULL MEMORY : " + Number((System.privateMemory * 0.000000954).toFixed(3)) + "mb";
			
			// last ten logs
			var lastTenLogs : String = "\n\n::\n:: LAST 10 LOGS ::\n::\n\n";
			lastTenLogs += getLogs(logs.length-11, logs.length-1, true);
			
			// full logs
			var fullLogs : String = logString;
			
			message = sessionDetails + "\n\n::\n:: "+mailHeaderText+" ::\n::\n\n" + message;
			message += lastTenLogs;
			
			message += "\n\n::\n:: FULL LOGS ::\n::\n\n" + fullLogs;
			
			return message;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		private static function echo(value:String, type:int = 0, clear:Boolean = false):void
		{
			// update logs
			if(clear) {
				logString = "";
				logs = new Vector.<LogItem>();
			}
			
			logString += value + "\n";
			logs.push(new LogItem(value, type));
			
			if (console && console.parent) showLogs();
			
			// update console view
			//if( CONSOLE && console ) {
				// security to be sure not crashing app having to much datas in console !
				// TODO : reactivate this security, otherwise the application will lag after sometimes due to textfield content
				//if(value.length > MAX_CONSOLE_LOG_LENGTH) value = value.slice(0, MAX_CONSOLE_LOG_LENGTH) + "\n<font color='#ffffff'> [ ... ] </font>";
				//if(logs.length > MAX_CONSOLE_LENGTH) logs = "<font color='#ffffff'> [ ... ] </font>\n" + logs.slice(logs.length-MAX_CONSOLE_LENGTH, logs.length);
				
				/*
				var currentTextPos : int;
				
				if(clear){
					currentTextPos = 0;
					console.text = value;
				} else {
					currentTextPos = console.text.length;
					value = "\n" + value;
					console.appendText(value);
				}
				 
				// update color
				
				if(currentTextColor != color){
					var tf : TextFormat = new TextFormat();
					tf.color = color;
					currentTextColor = color
					console.setTextFormat(tf, currentTextPos, currentTextPos+value.length);
				}
				*/
				
				
				// always go to bottom of textfield
				//console.scrollV = console.numLines;
			//}
			
			// trace if allowed
			if( SHOW_TRACE ) trace ( value );
		}
		
		
		private static function clearConsole(e:Event = null):void
		{
			var defaultString : String = "CONSOLE : Build : "+ Debug.BUILD_PREFIX + Infos.buildString+" \n\n";
			defaultString += "Rebranding ID = "+Infos.REBRANDING_ID +"\n"
			var userAgent : String = (ExternalInterface.available)?ExternalInterface.call("getUserAgent"):"ExternalInterface not Available";
			defaultString += "UserAgent : " + userAgent + "\n";
			defaultString += "OS : "+Capabilities.os+"\n";
			echo( defaultString, 0, true);
		}
	
		
		/**
		 *  get error message from resource or hardcoded if resources object not found
		 */
		private static function getErrorMessage(errorId:String):String
		{
			/*
			if(errorId == ERROR_DEFAULT) return "Sorry, an error occured. Please try refreshing the page.";
			else if(errorId == ERROR_SID_NOT_FOUND) return "User sid not found, session terminated";
			else if (errorId == ERROR_CONNECTION) return "Sorry, a connection problem occured. Please refresh the page in your browser";
			else if (errorId == ERROR_CONNECTION_SECURITY) return "Sorry, a connection problem occured. Please refresh the page in your browser";
			else if (errorId == ERROR_CONNECTION_IO) return "Sorry, a connection problem occured. Please refresh the page in your browser";
			else if (errorId == ERROR_CONNECTION_TIMEOUT) return "Sorry, a connection problem occured. Please refresh the page in your browser";
			else if (errorId == ERROR_SCORE) return "Sorry, an error occured. Please try refreshing the page.";
			else if (errorId == ERROR_GLOSSARY) return "Sorry, an error occured. Please try refreshing the page.";
			else 
			*/
			if(!ResourcesManager.ready) return "Sorry, an error occured. Please try refreshing the page.";
			return ResourcesManager.getString(errorId);
		}
		
		
		/**
		 * add console to stage
		 */
		private static function addConsole():void
		{
			if(console){
				warn("CONSOLE ALREADY EXISTS");
				return;
			}
			
			console = new TextField();
			console.multiline = true;
			console.textColor = 0x00ff00;
			console.opaqueBackground = 0x000000;
			console.backgroundColor = 0x000000;
			console.background = true;
			console.width = stage.stageWidth-50;
			console.height = stage.stageHeight-50;
			console.wordWrap = true;
			console.text = logString;
			console.selectable = true;
			currentTextColor = 0x00ff00;
			
			closeConsoleBtn = new PushButton(null, 400,0,"Close console", toggleConsole);
			clearConsoleBtn = new PushButton(null, 200,0,"Clear", clearConsole);
			//sendConsoleBtn = new PushButton(null, 200,0,"Report Bug", sendBugReport);
			showFullLogBtn = new PushButton(null, 300,0,"Show full log", showFullLogs);
			//sendConsoleBtn.visible = false;
			
		}
		
		public static function createContextMenu():void
		{
			/// ONLINE ////
			CONFIG::online{
				var menu:ContextMenu = (root.contextMenu as ContextMenu);
				menu.hideBuiltInItems();
				
				// Contact us menu
				var contactUsMenu:ContextMenuItem = new ContextMenuItem(ResourcesManager.getString("btn.contact.us"));
				contactUsMenu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, function():void
				{
					var popupContact:PopupContact = new PopupContact();					
					popupContact.open();
				});
				menu.customItems.unshift(contactUsMenu);
				
				// Send log
				var sendLogMenu:ContextMenuItem = new ContextMenuItem(ResourcesManager.getString("popup.userlog.title"),true);
				sendLogMenu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, openSendLogWindow);
				menu.customItems.push(sendLogMenu);
				
				if(CONSOLE){
					var menu:ContextMenu = (root.contextMenu as ContextMenu);
					// toggle console menu
					var contactUsMenu:ContextMenuItem = new ContextMenuItem("toggle console");
					contactUsMenu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, toggleConsole);
					menu.customItems.push(contactUsMenu);
					
					// export log menu
					/*var exportLogMenu:ContextMenuItem = new ContextMenuItem("export log");
					exportLogMenu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, exportLog);
					menu.customItems.push(exportLogMenu);*/
				}
			}
				
			/// OFFLINE ////
			CONFIG::offline{
				
				var menu:NativeMenu = (root.contextMenu as NativeMenu);
				
				// CONTACT US
				var seperator:NativeMenuItem = menu.addItemAt(new NativeMenuItem("",true),0);
				var contactMenu:NativeMenuItem = menu.addItemAt(new NativeMenuItem(ResourcesManager.getString("btn.contact.us")),0); 
				contactMenu.addEventListener(Event.SELECT, function():void
				{
					PopupContact.OPEN();
				});
				
				// SEND Log
				if(!CONSOLE)
				{
					var seperator:NativeMenuItem = menu.addItem(new NativeMenuItem("",true));
					var consoleMenu:NativeMenuItem = menu.addItem(new NativeMenuItem(ResourcesManager.getString("popup.userlog.title"))); 
					consoleMenu.addEventListener(Event.SELECT, openSendLogWindow); 	
				}
				
				
				if(CONSOLE)
				{
					// separator
					var consoleMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("--",true)); 
					
					// Toggle console
					var consoleMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("toggle console")); 
					consoleMenu.addEventListener(Event.SELECT, toggleConsole); 
					
					// export log menu
					var consoleMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("export log"));
					consoleMenu.addEventListener(Event.SELECT, exportLog);
					
					var resetAppFileMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("Clear App files (Closes app)")); 
					resetAppFileMenu.addEventListener(Event.SELECT, clearFirstUseFile);
					
					var resetAssetsMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("Clear Assets files (Closes app)")); 
					resetAssetsMenu.addEventListener(Event.SELECT, clearAssetsFile); 
					
					var resetCookiesMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("Clear Cookies (Closes app)")); 
					resetCookiesMenu.addEventListener(Event.SELECT, clearCookiesFile); 
				
				}
				
				// export log menu
				/* CRASH??
				var exportLogMenu:NativeMenuItem = menu.addItem(new NativeMenuItem("export log"));
				exportLogMenu.addEventListener(Event.SELECT, exportLog);
				*/
			}
			
			// set menu
			root.contextMenu = menu;
			if(root.parent) root.parent.contextMenu = menu;
		}
		
		
		private static function toggleConsole(e:Event = null):void
		{
			if(console.parent) {
				stage.removeChild(console);
				stage.removeChild(closeConsoleBtn);
				stage.removeChild(clearConsoleBtn);
				stage.removeChild(showFullLogBtn);
				//stage.removeChild(sendConsoleBtn);
				console.text =  "";
			}
			else showLogs();
		}
		
		private static function clearFirstUseFile(e:Event = null):void
		{
			CONFIG::offline
			{
				PopupAbstract.Alert("Debug : Clear App files","Are you sure you want to clear the Application files? This will restore the latest version of the app you've downloaded. You will have to re-launch the application.",true,function(p:PopupAbstract):void
				{
					OfflineAssetsManager.instance.clearFirstUseFile();
					App.instance.reload();
				});
			}
		}
		
		private static function clearAssetsFile(e:Event = null):void
		{
			CONFIG::offline
			{
				PopupAbstract.Alert("Debug : Clear Assets files","Are you sure you want to clear the Application assets files? All the assets, like backgrounds, cliparts, that you may have downloaded will be removed. You will have to re-launch the application.",true,function(p:PopupAbstract):void
				{
					OfflineAssetsManager.instance.deleteAssetFolder();
					App.instance.reload();
				});
			}
		}
		
		private static function clearCookiesFile(e:Event = null):void
		{
			CONFIG::offline
			{
				PopupAbstract.Alert("Debug : Clear Cookies","Are you sure you want to clear the ApplicationCookies? All the cookies will be removed. You will have to re-launch the application.",true,function(p:PopupAbstract):void
				{
					SharedObjectManager.instance.clear();
					App.instance.reload();
				});
			}
		}
		
		private static function exportLog(e:Event = null):void
		{
			/// ONLINE ////
			CONFIG::online{
				var fr : FileReference = new FileReference();	
				var bytes : ByteArray = new ByteArray();
				bytes.writeUTFBytes( ""+logString );
				fr.save( bytes, "logExport_"+new Date().time+".txt");
			}
				
			/// OFFLINE ////
			CONFIG::offline{
				var filePath : String = File.desktopDirectory.nativePath + "/log_"+ new Date().time+".txt";
				var fr : File = new File(filePath);
				
				// stream
				var fileStream:FileStream = new FileStream();
				fileStream.open(fr, FileMode.WRITE);
				
				// write
				fileStream.writeUTFBytes( ""+logString );
				fileStream.close();
			}
		}
		
		
		private static function showLogs( numLogs : int = 15):void
		{
			stage.addChild(console);
			stage.addChild(closeConsoleBtn);
			stage.addChild(clearConsoleBtn);
			stage.addChild(showFullLogBtn);
			
			var logsRef : Vector.<LogItem> = logs;
			var log:LogItem;
			var tf:TextFormat;
			var newTextPos : int;
			var startIndex : Number = logs.length-numLogs;
			currentTextColor=NaN; // reset text color
			if(startIndex < 0) startIndex = 0;
			
			console.text = ( startIndex != 0 )? "[...]\n" : "";
			var currentTextPos : int = console.text.length;
			
			for (var i:int = startIndex; i < logs.length; i++) 
			{
				log = logs[i];
				console.appendText(log.value + "\n");
				newTextPos = console.text.length;
				if( currentTextColor != log.color){
					tf = new TextFormat();
					tf.color = log.color;
					currentTextColor = log.color;
					console.setTextFormat(tf, currentTextPos, newTextPos);
				}
				currentTextPos = newTextPos;
			}
			
			console.scrollV = console.numLines;
		}
		
		private static function showFullLogs(e:Event = null):void
		{
			showLogs( logs.length );
		}
		
		/**
		 * return logs at specific index
		 */
		private static function getLogs(startIndex:Number, endIndex:Number, truncateLongLogs:Boolean = false):String
		{
			if(startIndex < 0) startIndex = 0;
			if(endIndex > logs.length-1) endIndex = logs.length-1;
			var logsValue : String = "";
			var log:LogItem;
			var val:String;
			for (var i:int = startIndex; i < endIndex; i++) 
			{
				log = logs[i];
				val = (log.value + "\n");
				if(truncateLongLogs && val.length > 1500){
					val = val.substr(0,1000) + "\n[....................]\n" + val.substr(val.length-500, 500);
				}
				logsValue += val;
			}
			return logsValue;
		}
		
		
		
		
		/**
		 *  GLobal error handler
		 *  allow to always catch error and log it
		 */
		private static function onGlobalErrorHandler(e:UncaughtErrorEvent):void
		{
			var stackInfo:String = "null";
			try 
			{
				//log(e.error.getStackTrace());
				stackInfo = e.error.getStackTrace();
			} 
			catch (err:Error){};
			
			// TODO : here we should probably handle different error types (for example IO, security, script time out etc...) They should be handled different ways..
			if(e.error && e.error is ScriptTimeoutError){
				// script timeout, we catch it and let user continue.. It seems to often happen with filereference.browse
				warn("Uncaught script timeout, we let user continue...");
			}
			// default we stop user edition
			else error(ERROR_DEFAULT, "\n[NAME] : "+ e.error.name+ " \n[STACK INFOS] : "+ stackInfo +" \n[DETAIL] : "+e.error.message + "\n[OBJECT] : " +e.toString() );
		}
		
		
		/**
		 * ------------------------------------ OPEN Error window -------------------------------------
		 */
		
		private static var errorShown:Boolean = false;
		public static function openErrorWindow():void
		{
			// security to avoid crazy error loop
			if(errorShown) return;
			
			// display popup
			var errorPopup : PopupError = new PopupError( ticketId );
			errorPopup.open();
			errorPopup.OK.add(function(p:PopupAbstract):void
			{
				// be sure to not go in the "need save" process
				UserActionManager.instance.actionsSinceLastSave = 0;
				App.instance.reload();
			});
			
			// once an error has been shown, app must reload
			errorShown = true;
		}
		
		
		/**
		 * ------------------------------------ OPEN Feedback window -------------------------------------
		 */
		
		
		/**
		 * send specific feebdack to team
		 */
		public static function openSendLogWindow(e:Event = null):void
		{
			if(Infos.IS_DESKTOP)
			saveScreenshot();
			var feedbackPopup : PopupSendLogs = new PopupSendLogs();
			feedbackPopup.open();
		}
		
		
		
		/**
		 * ------------------------------------ SAVE CURRENT SCREEN -------------------------------------
		 */
		private static var screenshotBytes : ByteArray;
		public static function saveScreenshot():void
		{
			if(stage)
			{
				var screenShot : BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
				screenShot.draw(stage);
				var compression : JPEGEncoderOptions = new JPEGEncoderOptions(65);
				if(screenshotBytes){ // dispose
					screenshotBytes.clear();
					screenshotBytes = null;
				}
				screenshotBytes = screenShot.encode(screenShot.rect,compression);
				screenShot.dispose();
			}
		}
		
		
		
		
		/**
		 * ------------------------------------ SEND USER FILE SYSTEM -------------------------------------
		 */
		
		
		private static var onUserFileSaveSuccess:Function; 	// return nulls
		private static var onUserFileSaveError:Function;	// return error event
		private static var userZip:FZip;
		private static var userZipBytes:ByteArray;
		private static var userZipLoader:MultipartURLLoader;
		
		/**
		 *  isError > param to know if files came from an editor error or a user feedback
		 * 	message > the main message to display on logs (user feedback or error string)
		 *  onSuccess > on success function returns nothing
		 *  onError > on error function returns an error event
		 */
		CONFIG::offline
		{
			public static function sendUserFiles( isError:Boolean = false, message = null, onSuccess:Function = null, onError:Function = null):void
			{	
				Debug.log("Debug.sendUserFiles");
				
				// listeners
				onUserFileSaveSuccess = onSuccess;
				onUserFileSaveError = onError;
				
				// create files
				userZip = new FZip();
				
				// TODO : review what we send here now with the new system...
				
				// logs
				var context : String = (isError)? "ERROR" : "USER FEEDBACK";
				// add build file
				userZip.addFileFromString("BUILD "+Infos.buildString, "BUILD "+ Infos.buildString);
				// add log file
				userZip.addFileFromString("log.txt", Debug.getLogEmailContent(context, message));
				
				// image list file
				//var imagelistInfo : String = ""+LocalStorageManager.instance.getUserImagesInfos();			
				//userZip.addFileFromString("userImages.txt", imagelistInfo);
				
				// folder structure file
				var directoryListingString : String = "DIRECTORY LISTING : \n"+LocalDB.RecursiveDirectoryListing(LocalDB.USER_FOLDER);
				
				
				/*
				var files:Array = LocalDB.USER_FOLDER.getDirectoryListing();
				var filesName:Array = new Array();
				for (var i:uint=0; i<files.length;i++)
				{
					trace(files[i].name);
					trace(files[i].nativePath);
					filesName.push({label:files[i].name,fpath:files[i].nativePath});
					
				}
				
				
				+;	
				*/
				userZip.addFileFromString("directoryListing.txt", directoryListingString);
				
				
				if(Infos.project){
					// project save file
					userZip.addFileFromString("saveString.txt", Infos.project.getSaveString());
					// screenshot file
					if(screenshotBytes)	userZip.addFile("screenshot.jpg",screenshotBytes);
				}
				
				userZipBytes = new ByteArray();
				userZip.serialize(userZipBytes);
				
				userZipLoader = new MultipartURLLoader();
				userZipLoader.addEventListener(Event.COMPLETE, onSendFileSuccess);
				userZipLoader.addEventListener(IOErrorEvent.IO_ERROR, onSenFileFail);
				userZipLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSenFileFail);
				userZipLoader.dataFormat = URLLoaderDataFormat.TEXT;
				var prefix : String = (isError)? "Err_" : "Fb_";
				userZipLoader.addFile(userZipBytes,  prefix + Debug.ticketId+"_("+Infos.session.email+").zip");
				var url:String = "http://www.antho.be/prod/tictac/userLogs.php?t" + TimeStamp.Create();
				userZipLoader.load(url, false);
			}
			private static function onSendFileSuccess(e:Event):void
			{
				var result : String = MultipartURLLoader(e.currentTarget).loader.data; 
				if(result != "success"){
					Debug.warn("Debug.onSendFileSuccess : result is : "+result);
					if(onUserFileSaveError != null) onUserFileSaveError(e);
					disposeUserZipAndFiles();
				}
				else{
					Debug.log("Debug.onSendFileSuccess");
					if(onUserFileSaveSuccess != null) onUserFileSaveSuccess();
					disposeUserZipAndFiles();
				}
				
			}
			private static function onSenFileFail(e:Event):void
			{
				var result : String = MultipartURLLoader(e.currentTarget).loader.data; 
				Debug.warn("Debug.onSenFileFail : error is : "+result);
				if(onUserFileSaveError != null) onUserFileSaveError(e);
				disposeUserZipAndFiles();
			}
			private static function disposeUserZipAndFiles():void
			{
				if(userZipBytes) {
					userZipBytes.clear();
					userZipBytes = null;
				}
				if( userZip ){
					userZip.dispose();
					userZip = null;
				}
				if(screenshotBytes){ // dispose
					screenshotBytes.clear();
					screenshotBytes = null;
				}
				if( userZipLoader ){
					userZipLoader.dispose();
					userZipLoader.removeEventListener(Event.COMPLETE, onSendFileSuccess);
					userZipLoader.removeEventListener(IOErrorEvent.IO_ERROR, onSenFileFail);
					userZipLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSenFileFail);
					userZipLoader = null;
				}
				onUserFileSaveSuccess = null;
				onUserFileSaveError = null;
			}
		}
		
		

		
		/**
		 * ------------------------------------ OBJECT DUMPER -------------------------------------
		 */
		
		
		/*
		public static var od_count:uint = 0;
		public static function objectDump(o:*):void{
			var obj:Object = o as Object;
			for (var prop:String in obj){
				var tab:String = Debug.getTabString();
				trace(tab + "   > " + prop + " : " + obj[prop]);
				if(o[prop] is Object){
					od_count++;
					objectDump(obj[prop]);
					od_count--;
				}
			}
		}
		
		public static function getTabString():String{
			var str:String = "";
			for (var i:int = 0; i < od_count; i++) str += "\t";
			return str;
		}
*/	
	}
}

