package utils
{
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import org.osflash.signals.Signal;
	
	public class KeyboardManager
	{
		// KEY EVENT
		public static const DELETE:Signal = new Signal(); // delete
		public static const CTRL_Z:Signal = new Signal(); // undo
		public static const CTRL_S:Signal = new Signal(); // save
		
		
		private var _stage:Stage;
		
		
		/**
		 * ------------------------------------ SINGLETON -------------------------------------
		 */
		
		
		public function KeyboardManager(sf:SingletonEnforcer){ if(!sf) throw new Error("KeyboardManager is a singleton, use instance");}
		private static var _instance:KeyboardManager;
		public static function get instance():KeyboardManager
		{
			if (!_instance) _instance = new KeyboardManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		/**
		 * ------------------------------------ INIT by setting stage -------------------------------------
		 */
		
		public function init(stage:Stage):void
		{
			Debug.log("KeyboardManager.init");
			_stage = stage;
			_stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		
		/**
		 * ------------------------------------ on Key Down -------------------------------------
		 */
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			switch (e.keyCode)
			{
				// delete
				case Keyboard.DELETE : 
					DELETE.dispatch();
					break;
				
				// ctrl s
				case Keyboard.S : 
					if(e.ctrlKey) CTRL_S.dispatch();
					break;
				
				// ctrl z
				case Keyboard.Z : 
					if(e.ctrlKey) CTRL_Z.dispatch();
					break;
				
				default : 
					break;
			}
		}
		
	}
}
class SingletonEnforcer{}