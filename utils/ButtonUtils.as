package utils
{
	import com.greensock.TweenMax;
	
	import comp.DefaultTooltip;
	
	import data.TooltipVo;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;

	public class ButtonUtils
	{
		/**
		 * simple helper to activate a sprite as button and add quick rollover
		 */
		public static function makeButton(target:*, handler:Function = null, tooltip:TooltipVo = null, overTint:Number = -1):*
		{
			var btn : Sprite;
			if(target is BitmapData){
				target = new Bitmap(target);
			}
			if(target is Bitmap){
				btn = new Sprite();
				if(target.parent) target.parent.addChild(btn); // if parent replace it
				btn.addChild(target);
				btn.x = target.x; btn.y = target.y; target.x = 0; target.y = 0 // at same pos	
			} 
			else btn = target as Sprite;
			
			if(!target){
				Debug.warn("ButtonUtils > makeButton > error target is null");
				return null;
			}
			
			btn.mouseChildren = false;
			btn.buttonMode = true;
			btn.addEventListener(MouseEvent.ROLL_OVER, function():void{
				if(overTint != -1) {
					TweenMax.killTweensOf(btn);
					TweenMax.to(btn, 0, {tint:overTint});
				}else btn.alpha = .8;
				if(tooltip) DefaultTooltip.tooltipOnClip(btn,tooltip);
			});
			btn.addEventListener(MouseEvent.ROLL_OUT, function():void{
				if(overTint != -1) {
					TweenMax.killTweensOf(btn);
					TweenMax.to(btn, 0, {tint:null});
				}else btn.alpha = 1;
				if(tooltip) DefaultTooltip.killAllTooltips();
			});
			
			if(handler != null) btn.addEventListener(MouseEvent.CLICK, handler);
			
			return btn;
		}
		
		
		/**
		 * simple helper to activate a sprite as button and add quick rollover
		 */
		public static function makeButtonWithIcon(btn:MovieClip, handler:Function = null, rollColor:Number = Colors.BLUE):void
		{
			btn.mouseChildren = false;
			btn.buttonMode = true;
			btn.addEventListener(MouseEvent.ROLL_OVER, function():void{
				TweenMax.killTweensOf(btn.bg);
				TweenMax.to(btn.bg,0,{tint:rollColor});
				TweenMax.killTweensOf(btn.icon);
				TweenMax.to(btn.icon,0,{tint:Colors.WHITE});
			});
			btn.addEventListener(MouseEvent.ROLL_OUT, function():void{
				TweenMax.killTweensOf(btn.bg);
				TweenMax.to(btn.bg,0,{tint:null});
				TweenMax.killTweensOf(btn.icon);
				TweenMax.to(btn.icon,0,{tint:null});
			});
			
			if(handler != null) btn.addEventListener(MouseEvent.CLICK, handler);
		}
		
		/**
		 * simple helper to activate a Textfiled as button and add quick rollover and handler
		 */
		public static function makeHyperLink(textfield:TextField, link:String):void
		{
			textfield.mouseEnabled = true;
			textfield.addEventListener(MouseEvent.ROLL_OVER, function():void{
				textfield.alpha = .5;
			});
			textfield.addEventListener(MouseEvent.ROLL_OUT, function():void{
				textfield.alpha = 1;
			});
			textfield.addEventListener(MouseEvent.CLICK, function():void{
				navigateToURL(new URLRequest(link));
			});
		}
		
		
	}
}