package data
{
	public class PopupParams
	{
		// default
		public var title:String;
		public var description:String;
		public var action:String;
		public var imageIndex : uint;
		public var closeFunction : Function;
		
	}
}