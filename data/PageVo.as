/**
 * jonathan@nguyen.eu
 * 
 * Page Vo contains all infos about a given page within a projectVo
 * 
 */
package data
{
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import manager.CanvasManager;
	import manager.LayoutManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.UserActionManager;
	
	import org.osflash.signals.Signal;
	
	import utils.Debug;
	
	import view.edition.FrameBackground;
	import view.edition.FramePhoto;

	
	
	public class PageVo
	{
		public static var MONTH_INDEX:int = 0;
		public static const PAGEVO_UPDATED : Signal = new Signal(PageVo); // Current page to be redraw : PageVo , use animation
		
		public var index:int;
		private var _bounds:Rectangle;
		public var layoutId:String;		
		public var isCover:Boolean = false;		
		public var coverType:String;		
		public var monthIndex:int = -1;
		private var _layoutVo:LayoutVo = new LayoutVo();
		//calendar colors
		public var customColors:CalendarColors;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PageVo()
		{}
		
		
		
		public function setLayout( _layoutID: String, _isCover:Boolean = false ):void
		{
			isCover = _isCover;
			layoutId = _layoutID
			var lVo:LayoutVo = LayoutManager.instance.getLayoutById(layoutId,_isCover);
			_layoutVo = (lVo != null)?lVo:new LayoutVo();
			
			if(lVo && lVo.isCalendar && monthIndex==-1)
			{
				//set month Index
				if(MONTH_INDEX > 11)MONTH_INDEX = 0;
				monthIndex = MONTH_INDEX;
				MONTH_INDEX++;
			}
			
			//check frame
			for (var i:int = 0; i < _layoutVo.frameList.length; i++) 
			{
				var frameVo:FrameVo = _layoutVo.frameList[i];
				
				if(Infos.isCanvas && CanvasManager.instance.isPopart)
					_layoutVo.frameList[i].isPopart = true;
			}
			
			//Calendar
			if(layoutVo.isCalendar)
			{
				customColors = new CalendarColors();
			}
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////
		
		public function get bounds():Rectangle
		{
			return _bounds;
		}

		public function set bounds(value:Rectangle):void
		{
			_bounds = value;
		}

		public function get layoutVo():LayoutVo
		{
			return _layoutVo;
		}
		
		public function set layoutVo(value:LayoutVo):void
		{
			_layoutVo = value;
			Debug.warn("PageVo SETTER: LayoutVo: Layout has been set");
			//PAGEVO_UPDATED.dispatch(this);			
		}
		
		/**
		 * a page is empty when none of her frames are filled
		 */
		public function get isEmpty():Boolean
		{
			var frame : FrameVo;
			for (var i:int = 0; i < layoutVo.frameList.length; i++) 
			{
				frame = layoutVo.frameList[i];
				if(frame.type == FrameVo.TYPE_BKG && frame.photoVo ) return false; // we have at least a background
				else if(frame.type != FrameVo.TYPE_BKG && !frame.isEmpty) return false; // we have at least another frame which is not empty
			}
			return true;
		}
		
		/**
		 * a page has warnings when one of its frame is poor quality
		 */
		public function get hasPrintWarning():Boolean
		{
			var frame : FrameVo;
			for (var i:int = 0; i < layoutVo.frameList.length; i++) 
			{
				frame = layoutVo.frameList[i];
				if(frame.printQuality<0) return true; // set <1 to see if there is yellow warnings
			}
			return false;
		}
		
		
		/**
		 * Helper to know if a page is a calendar page
		 */
		public function get isCalendar():Boolean
		{
			if(monthIndex > -1)
				return true
				
			return false;
		}
		
		/**
		 * Helper to know if a page is a postCard back
		 */
		public function get isPostCardBack():Boolean
		{
			if(layoutId == "3000")
				return true
			
			return false;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * add a new frame on top of this pageVo based on a photovo
		 */
		public function createNewFrameFromPhotoVo( photoVo : PhotoVo, position:Point = null ):FrameVo
		{
			var frameVo:FrameVo = new FrameVo(photoVo);
			if(position){
				frameVo.x = position.x;
				frameVo.y = position.y;	
			} else {
				frameVo.x = bounds.width*.5; // center it
				frameVo.y = bounds.height*.5; // center it
			}
			
			// find best fitting scale
			var photoScale : Number = getNewFrameScale( frameVo );
			frameVo.width = photoVo.width *photoScale;
			frameVo.height = photoVo.height * photoScale;
			
			// inject photovo in frame
			FrameVo.injectPhotoVoInFrameVo( frameVo, photoVo, true );
			
			// add frame to list
			layoutVo.frameList.push( frameVo );
			
			return frameVo;
		}
		
		
		
		/**
		 * Retrieve optimal scale size of a new frame on stage
		 * -> this happen only when creating a new frame and adding to the page, we then need to choose a nice size for this frame
		 */
		private function getNewFrameScale( frameVo : FrameVo ):Number
		{
			// security
			if(!frameVo.photoVo) {
				Debug.warn("PageVo.getNewFrameScale : frameVo has not photovo.. strange for new frame creation");
				return 1;
			}
			
			// if photo is landscape (take .4 of page width)
			if(frameVo.photoVo.width > frameVo.photoVo.height )
				return Infos.project.width*Infos.project.PFLMultiplier * .4 / frameVo.photoVo.width;
			
			// else photo is portrait (take .6 of page height)
			return Infos.project.height*Infos.project.PFLMultiplier * .6 / frameVo.photoVo.height;
		}
		
		
		
		
		/**
		 * Scale layout  
		 * When upgrading an album (or downgrading) we scale up/down all elements on a page
		 */
		public function scaleLayout(scaleX:Number, scaleY:Number = NaN):void
		{
			_layoutVo.scaleLayout(scaleX, scaleY);
		}
		
		/**
		 * Update Layout 
		 * Retreive current photoVo used and inject them in the new LayoutVo
		 */
		public function updateLayout(newLayoutVo:LayoutVo, isUpgrade:Boolean = false):void
		{
			if(newLayoutVo == null)
			{
				Debug.log("PageVo.updateLayout : newLayoutVo is null, stop action");
				return;
			}
			
			Debug.log("PageVo.updateLayout : Layout ID: "+newLayoutVo.id)
			
			if(layoutVo.id == newLayoutVo.id && !isUpgrade)
			{
				Debug.log("PageVo > updateLayout: Layout ID is the same, do not update layout ("+newLayoutVo.id+")");
					return;
			}
				
			var savedLayoutProps:Object = _layoutVo.getLayoutProps();
			_layoutVo = newLayoutVo;
			_layoutVo.injectProps(savedLayoutProps, this);
			
			PAGEVO_UPDATED.dispatch(this);	
		}
		
		/**
		 * (Calendar) Look for calendar frames
		 * return true if find one
		 */
		public function hasCalendarFrame():Boolean
		{
			if(_layoutVo)
			{
				for (var i:int = 0; i < _layoutVo.frameList.length; i++) 
				{
					var frameVo:FrameVo = _layoutVo.frameList[i];
					if(LayoutManager.instance.isCalendarFrame(frameVo.dateAction))
						return true;
				}
				return false;
			}
			return false;
		}
		
		/**
		 * (Calendar) Update month Index
		 * Month inx is used to define which month is displayed on this pageVO
		 */
		public function updateMonthIndex():void
		{
			if(_layoutVo && _layoutVo.isCalendar)
			{
				if(MONTH_INDEX > 11)MONTH_INDEX = 0;
				monthIndex = MONTH_INDEX;
				MONTH_INDEX++;
			}
		}
		
		
		/**
		 *	fill a page vo from project save
		 */
		public function fillFromSave(pageObj:Object):void
		{
			// start fill
			customColors = new CalendarColors();
			for (var props:String in pageObj.customColors) {
				if(customColors.hasOwnProperty(props))
				customColors[props] = pageObj.customColors[props];
			}
			
			layoutId = pageObj.layoutId;
			isCover = pageObj.isCover;
			coverType = pageObj.coverType;
			monthIndex = pageObj.monthIndex;
			
			// TODO : here we should probably use Project manager width*multiplier to see when there is something wrong...
			bounds = new Rectangle(pageObj.bounds.x, pageObj.bounds.y, pageObj.bounds.width, pageObj.bounds.height);
			
			index = pageObj.index;
			
			//Debug.log("PageVo > construct > page with Index : "+index + " has "+ pageObj.layoutVo.frameList.length + " frames");
			
			if(!pageObj.layoutVo)
				Debug.warn("PageVo > fillFromSave > pageObj doesn't have a layoutVo... can't be");
					
			layoutVo.frameList = new Vector.<FrameVo>;
			layoutVo.id = pageObj.layoutVo.id;
			for (var i:int = 0; i < pageObj.layoutVo.frameList.length; i++) 
			{
				var frame : FrameVo = new FrameVo();
				frame.fill(pageObj.layoutVo.frameList[i]);
				if(frame.photoVo)
				{
					if(frame.photoVo.isValidVo)
						layoutVo.frameList.push(frame);
					else
						Debug.warn("PageVo > ConstructFromSave > frame.photoVo in page "+ index +" (name:"+frame.photoVo.name+") is not valid > we do not render the frame");
				}
				else
					layoutVo.frameList.push(frame);
				
				
			}
		}
	}
}