package data
{
	import com.adobe.serialization.json.JSONEncoder;
	
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	
	import mx.resources.ResourceManager;
	
	import be.antho.data.ResourcesManager;
	
	import manager.CalendarManager;
	import manager.CanvasManager;
	import manager.CardsManager;
	import manager.CoverManager;
	import manager.LayoutManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	import manager.ProjectManager;
	import manager.SessionManager;
	import manager.UserActionManager;
	
	import offline.data.CanvasPriceInfo;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	import utils.SpineUtils;
	import utils.TextFormats;
	import utils.debug.TimeLog;

	/**
	 * jonathan@nguyen.eu
	 * 
	 * Project Vo contains all infos about the current project used by the app.
	 * 
	 */
	
	public class ProjectVo
	{
		// offline image storage preferences
		public static const STORAGE_TYPE_REFERENCE : String = "ref";
		public static const STORAGE_TYPE_COPY : String = "copy";
		
		
		// project uniq id (getter/Setter)
		private var _id : String;
		
		public var classname: String	// Project class (albums, agenda, etc...)
		public var type:String;			// Project menu type ( types_albums_classic, types_albums_trendy, types_albums_casual )
		public var name:String;			// Project display name
		public var build:String;		// Version of App on which project has been created
		public var coverType:String;		// Cover style ID
		public var docType:String		// Server Document type (101, 102, etc...) 
		public var docCode:String;		// Product code (S30, S40, etc...)
		public var docPrefix:String;		// Product code prefix (S, M, L, Q, QD...)
		public var docOrientation:String;		// Product orientation ("portrait" , "landscape", "square")
		public var width:Number;		// Product width in PFL's
		public var height:Number;		// Product height in PFL's
		public var coverWidth:Number;		// cover width in PFL's
		public var coverHeight:Number;		// cover height in PFL's
		public var resolution:Number;		// resolution minimum for good quality printing (I think.. but not sure :))
		public var PFLMultiplier:Number;	// multiplier/ratio to convert project width and height in Postscript points (See measurementManager)
		public var PFLCoverMultiplier:Number;	// multiplier/ratio to convert project width and height in Postscript points (See measurementManager) (For COVER)
		public var docModifier:String = "";			
		
		public var docCutBorder : Number;	// the cut border placement on the final order (where the paper will be cut, it's drawn on the pdf by server ) (in MM)
		public var coverCutBorder : Number;	// the cut border placement on the final order (where the paper will be cut, it's drawn on the pdf by server ) (in MM)
		public var adjustCropX : Number = 0;	
		public var adjustCropY : Number = 0;	
		public var lastUsedTextFormat: Object; //  only font, bold, underline, italic and color;
		
		// pages
		private var pageBounds : Rectangle;
		public var pageList:Vector.<PageVo> = new Vector.<PageVo>();
		
		//ALBUMS ONLY
		private var _paper:Boolean;			// Use or not paper between pages
		private var _coated:Boolean = true;			// Use or not coated pages. TRUE is the default value because all older ablums were coated
		private var _pagePaperQuality:String = ProductsCatalogue.PAPER_QUALITY_250;			// Use 200gr or 250 type of paper for pages. 250 is the default value because all older ablums were coated
		public var pageNumber:Boolean;			// show or hide page number
		public var pageNumberCentered:Boolean;			// show or hide page number
		public var flyleafColor:String = "white"; // colors used to draw flyeaf in editionArea. Defautl white
		
		// CALENDARS ONLY
		public var calendarYear : int = new Date().fullYear+1;
		public var calendarStartDayIndex : int = 0;
		public var calendarStartMonthIndex : int = 0;
		//public var globalCalendarColors:CalendarColors; //Global calendar colors

		
		// CANVAS ONLY 
		public var canvasType : String ; // is the canvas type (material) : canvas, wood, forex, etc...
		public var canvasMargin : Number; // margin in mm between multiple canvas
		public var canvasMLayout : String; // Layout of the canvas multiple format
		public var multipleFrameVo : FrameVo; // multiple frame vo reference
		public var canvasFrameColor : String; // frame color for kadapak (black, silver, gold, red, blue..);
		public var canvasStyle : String ; // = styles_style1, styles_style2, styles_style5
		public var canvasPriceInfo : CanvasPriceInfo ; //Offline only, infos to calculate price offline for multiple and customs canvas
		
		// CARDS ONLY 
		public var envelope : String ; // is the envelope's type ID
		
		// OFFLINE
		private var _imageIdCount : int = 0; // each time an image is added to the project, we generate an id based on this incremented count
		public var creationDate : String; // creation date
		public var modificationDate : String; // last modification date
		public var lastOpenedOfflinePath:String;
		public var imageStorageSystem : String = STORAGE_TYPE_REFERENCE;
		public var patched:Boolean = false;
		
		// not saved (runtime values that are not recovered on fillFromSave)
		private var _price : Number = 0 ;
		private var _numPages:int;		// total pages of project !
		public var promoReduction : Number = 0;
		public var promoIsPercent : Boolean ;
		public var promoCode : String = null;
		
		
		/**
		 * ------------------------------------ CONSTRUCTOR -------------------------------------
		 */
		
		public function ProjectVo(initParams:ProjectCreationParamsVo = null)
		{
			fillInitParams(initParams);
		}
		
				
		
		/**
		 * ------------------------------------ FILL WITH INITIALIZATION PARAMETERS -------------------------------------
		 */
		
		public function fillInitParams( initParams:ProjectCreationParamsVo, isUpdate : Boolean = false ):void
		{
			if(initParams)
			{
				// ALBUM
				if(initParams.paper != null) paper = initParams.paper;
				if(initParams.pageNumber) pageNumber = initParams.pageNumber;
				if(initParams.coated  != null) coated = initParams.coated;
				if(initParams.paperQuality) pagePaperQuality = initParams.paperQuality;
				if(initParams.flyleaf) flyleafColor = initParams.flyleaf;
				
				// CALENDAR
				if(initParams.calendarStartDayIndex) calendarStartDayIndex = initParams.calendarStartDayIndex-1;
				if(initParams.calendarStartMonthIndex) calendarStartMonthIndex = initParams.calendarStartMonthIndex-1;
				if(initParams.calendarYear) calendarYear = initParams.calendarYear;
				
				// CANVAS
				if(initParams.canvasType) canvasType = initParams.canvasType;
				if(initParams.canvasFrameColor) canvasFrameColor = initParams.canvasFrameColor;
				if(!isNaN(initParams.canvasMargin)) canvasMargin = initParams.canvasMargin *10; // should be in mm
				if(initParams.canvasWidth) width = initParams.canvasWidth *10; // should be in mm
				if(initParams.canvasHeight) height = initParams.canvasHeight *10; // should be in mm
				if(initParams.canvasMLayout) canvasMLayout = initParams.canvasMLayout; // should be in mm
				if(initParams.canvasStyle) canvasStyle = initParams.canvasStyle; 
				
				// CARDS
				if(initParams.envelope) envelope = initParams.envelope;
			}
		}
		
		
		/**
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * GETTERS / SETTERS
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		
		public function get pagePaperQuality():String
		{
			return _pagePaperQuality;
		}
		
		public function set pagePaperQuality(value:String):void
		{
			_pagePaperQuality = value;
		}
		
		public function get coated():Boolean
		{
			return _coated;
		}
		
		public function set coated(value:Boolean):void
		{
			_coated = value;
		}
		
		
		public function getSaveBytes():ByteArray
		{
			// get project as bytes
			var saveBytes : ByteArray = new ByteArray();
			//saveBytes.writeUTFBytes(projectString);
			Debug.log("-----> bytes created " + TimeLog.log("SaveProcess"));
			//trace("bytes before compression : " + saveBytes.length);
			//saveBytes.compress();
			saveBytes.writeObject(Infos.project);
			Debug.log("-----> bytes compressed " + TimeLog.log("SaveProcess"));
			//trace("bytes after compression : " + saveBytes.length);
			return saveBytes;
		}
		
		
		// project ID, setter is public for online as ID received from server
		// but is private for offline as
		public function get id():String
		{
			return _id;
		}
		public function set id(value:String):void
		{
			_id = value;
		}

		
		// paper
		public function get paper():Boolean
		{
			return _paper;
		}
		public function set paper(value:Boolean):void
		{
			trace("set papaer value to: "+value);
			_paper = value;
		}
		
		
		
		
		// offline image Id count, we need a public getter/setter so this is saved inside project save file 
		// This getter/getter is extremely important
		public function get imageIdCount():int
		{
			return _imageIdCount;
		}
		public function set imageIdCount(value:int):void
		{
			_imageIdCount = value;
		}
		public function setImageIdCountAfterPossibleError(value:int):void
		{
			_imageIdCount = value;
		}
		// we use image id count to find next image ID/name
		public function getNewPhotoVoId():String
		{
			_imageIdCount ++;
			return (_imageIdCount/10000).toFixed(4).split(".")[1]; // 1 > 00001
		}
		
		
		/**
		 * is already saved, meaning the project has already been saved once
		 */
		public function get isSaved():Boolean
		{
			return (modificationDate != null);
		}
		
		
		
		/**
		 * retrieve the amount of pages (multiple of 10 = remove cover pages from count)
		 */
		public function get flooredNumPages():int
		{
			return Math.floor(_numPages/10) *10;
		}

		public function get numPages():int
		{
			if(Infos.isAlbum)
			return _numPages-1; // remove cover from count
			
			return _numPages;  // so far other product use the real num pages
		}
		
		public function set numPages(value:int):void
		{
			_numPages = value;
		}

				
		public function get hasCustomCover():Boolean
		{
			return (coverType == ProductsCatalogue.COVER_CUSTOM);
		}
		
		/**
		 * retrieve cover vo
		 */
		public function get coverVo():PageVo
		{
			if(pageList && pageList[0].isCover) return pageList[0];
			return null;
		}
		
		
		
		/**
		 * price with reductions (if there is one)
		 */
		public function get price():Number
		{
			var calculatedPrice : Number = _price;
			if(promoReduction != 0){
				if(promoIsPercent) calculatedPrice = _price - _price*promoReduction/100;
				else calculatedPrice = _price-promoReduction;
			}
			if(calculatedPrice <0) return 0;
			return calculatedPrice;
		}
		public function set price(value:Number):void
		{
			_price = value;
		}
		/**
		 * price without reduction
		 */
		public function get fullPrice():Number
		{
			return _price
		}
		
		
		/**
		 * retrieve albumvo list
		 */
		public function get albumList():Vector.<AlbumVo>
		{
			return SessionManager.instance.albumList;
		}

		
		
		/**
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * NEW PROJECT METHODS
		 * 
		 * createNew
		 * createPagesVo
		 * buildProjectAsAlbum
		 * buildProjectAsCalendar
		 * buildProjectAsCards
		 * addCoverClassicPage
		 * addNewAlbumPage / addNewCalendarPage / addNewCardPage
		 * 
		 * 
		 * 
		 * 
		 */
		
		
		/**
		 * A new project is constructed with a project node from projects.xml and a document node from documents.xml
		 */
		public function createNew(projectXML:XML, documentXML:XML, coverXML:XML, _coverType:String = ""):void
		{
			// fill properties with xml datas
			fillFromXML(projectXML, documentXML);
			
			//override covertype because of ols structure from appV1 si shit!
			if(_coverType != "") coverType = _coverType;
			
			// add build version to project vo
			build = Infos.buildVersion ;
			
			//Reset manager 
			ProjectManager.instance.resetManagers();
			
			//project name
			name = ResourcesManager.getString("topbar.save.unsavedProject");
			
			//create pages
			createPagesVo(documentXML,coverXML);
			
		}
		
		
		/**
		 * create project PageVos
		 */
		public function createPagesVo(documentXML:XML, coverXML:XML):void
		{
			//Used layout list
			var usedLayoutList:XMLList = documentXML.page.@layout;
			//pagesInProject = documentXML.page.length();
			
			//create pages based on product type
			switch(Infos.session.classname)
			{
				case ProductsCatalogue.CLASS_ALBUM:
					buildProjectAsAlbum(usedLayoutList, coverXML);
					break;
				case ProductsCatalogue.CLASS_CALENDARS:
					buildProjectAsCalendar(usedLayoutList);
					break;
				case ProductsCatalogue.CLASS_CARDS:
					buildProjectAsCards(usedLayoutList);
					break;
				case ProductsCatalogue.CLASS_CANVAS:
					if(CanvasManager.instance.isPelemele) addNewCanvasPage(documentXML.layout.(@type == "2")[0].@name);
					else buildProjectAsCanvas(usedLayoutList);
					break;
			}
		}
		
		/**
		 * BUILD project as CALENDAR
		 * Add pages, and calendars specifics 
		 */
		private function buildProjectAsCalendar(usedLayoutList:XMLList):void
		{
			pageList = new Vector.<PageVo>;
			for (var i:int = 0; i < numPages; i++) 
			{
				addNewCalendarPage(usedLayoutList[i]);
			}
		}
		
		/**
		 * BUILD project as CANVAS
		 * Add pages, and calendars specifics 
		 */
		private function buildProjectAsCanvas(usedLayoutList:XMLList):void
		{
			pageList = new Vector.<PageVo>;
			for (var i:int = 0; i < numPages; i++) 
			{
				// first layout is simple layout, last layout is pele mele
				addNewCanvasPage(usedLayoutList[0]);
			}
		}
		
		
		/**
		 * BUILD project as CARDS
		 * Add pages, and cards specifics 
		 */
		private function buildProjectAsCards(usedLayoutList:XMLList):void
		{
			pageList = new Vector.<PageVo>;
			for (var i:int = 0; i < numPages; i++) 
			{
				addNewCardPage(usedLayoutList[i]);
			}
		}
		
		
		/**
		 * BUILD project as ALBUM
		 * Add pages, add cover etc...
		 */
		private function buildProjectAsAlbum(usedLayoutList:XMLList, coverXML:XML):void
		{
			//Cover 
			//Get layout if its not a classic album
			var coverLayoutId:String = getCoverLayoutId(coverType);
			//Cover bounds
			coverWidth = coverXML.@pagewidth;
			coverHeight = coverXML.@pageheight;
			
			pageList = new Vector.<PageVo>;
			for (var i:int = 0; i < numPages; i++) 
			{
				if(i == 0)
				{
					if(coverLayoutId != "") // it is a custom cover because it has a layoutID
						addNewAlbumPage(coverLayoutId,true);
					else
						addCoverClassicPage();
				}
				else
					addNewAlbumPage(usedLayoutList[i]);
			}
		}		
		
		/**
		 * Add a Cover Classic page to a album project
		 * */
		private function addCoverClassicPage():void
		{
			pageList.push(createCoverClassicPage());	
		}	
		
		/**
		 * Add a page to a album project
		 * */
		public function addNewAlbumPage(layoutId:String, isCustomCover:Boolean = false):void
		{
			var pageVo:PageVo = createAlbumPage(layoutId,isCustomCover);
			if(pageNumber)
			{
				var tempTf:TextField = new TextField();
				var tempTextFormat:TextFormat = (TextFormats.LAST_PAGE_NUMBER_USED)?TextFormats.LAST_PAGE_NUMBER_USED:TextFormats.DEFAULT_FRAME_TEXT(LayoutManager.instance.getFontSizeForAlbumPageNumber(),Colors.BLACK);
				
				var pageNumberFrameVo:FrameVo = PagesManager.instance.getPageNumberFrameVo(pageVo,pageList.length,tempTf,tempTextFormat);
				if(pageNumberFrameVo != null)
				pageVo.layoutVo.frameList.push(pageNumberFrameVo);
			}
			pageList.push(pageVo);
		}
		
		/**
		 * Add a page to a calendar project
		 * */
		public function addNewCalendarPage(layoutId:String):void
		{
			var pageVo:PageVo = new PageVo();
			pageVo.setLayout(layoutId);
			pageVo.index = pageList.length;
			pageVo.bounds = getPageBounds();
			pageList.push(pageVo);
		}
		
		/**
		 * Add a page to a card project
		 * */
		public function addNewCardPage(layoutId:String):void
		{
			var pageVo:PageVo = new PageVo();
			pageVo.setLayout(layoutId);
			pageVo.index = pageList.length;
			pageVo.bounds = getPageBounds();
			pageList.push(pageVo);
		}
		
		/**
		 * Add a page to a canvas project
		 * */
		public function addNewCanvasPage(layoutId:String):void
		{
			var pageVo:PageVo = new PageVo();
			pageVo.setLayout(layoutId);
			pageVo.index = pageList.length;
			pageVo.bounds = getPageBounds();
			pageList.push(pageVo);
		}
		
		
		
		
		/**
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * UPDATE PROJECT METHODS
		 * 
		 * update
		 * updatePagesVo
		 * updateProjectAsAlbum
		 * changeCoverTo
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		
		/**
		 * update the data
		 * happens when applying an upgrade to a project
		 */
		public function update(projectXML:XML, documentXML:XML, coverXML:XML, newCoverType:String = ""):void
		{
			fillFromXML( projectXML, documentXML); // fill properties with new xml datas
			
			//Mandatory hack
			if(newCoverType != "") coverType = newCoverType; //override covertype because of ols structure from appV1 is shit! (Concern CLASSIC VS CONTEMPORARY, using same project configuration but has different cover)... long story!
	
			//update page number
			var newPageNumber:int = numPages;
			var offsetPages:int = numPages - pageList.length;
			updatePagesVo(documentXML,coverXML, offsetPages);
		}
		
		
		
		/**
		 * Update project PageVos
		 * Remove or add pages to the current project
		 * Album only for now
		 */
		public function updatePagesVo(documentXML:XML, coverXML:XML, offsetPages:int):void
		{
			//Used layout list
			var usedLayoutList:XMLList = documentXML.page.@layout;
			
			//upadte pages based on product type
			switch(Infos.session.classname)
			{
				case ProductsCatalogue.CLASS_ALBUM:
					updateProjectAsAlbum(usedLayoutList,coverXML,offsetPages);
					break;
				case ProductsCatalogue.CLASS_CALENDARS:
					updateProjectAsCalendar();
					break;
				case ProductsCatalogue.CLASS_CARDS:
					updateProjectAsCards(usedLayoutList,offsetPages);
					break;
				case ProductsCatalogue.CLASS_CANVAS:
					updateProjectAsCanvas( usedLayoutList, offsetPages);
					break;
			}
			
		}
		
		/**
		 * UPDATE project as CARDS
		 * Mostly, change bounds of pages. As calendar upgrades are just scale up/down of same layouts
		 */
		private function updateProjectAsCards(usedLayoutList:XMLList,offsetPages:int):void
		{
			
			//reset pageBounds to force re-create it with new PFL
			pageBounds = null;
			
			//let loop through all pages and do updates if needed
			for (var i:int = 0; i < pageList.length; i++) 
			{
				var pageVo:PageVo = pageList[i];
				
				//update bounds
				var newPageBounds:Rectangle = getPageBounds();
				//ratio
				var scaleRatio:Number = newPageBounds.width / pageVo.bounds.width;
				
				pageVo.bounds = newPageBounds;
				
				//scale layout
				pageVo.scaleLayout(scaleRatio);
				
			}
			
			//Add or remove pages
			if(offsetPages < 0)
				pageList.splice(pageList.length-(Math.abs(offsetPages)),Math.abs(offsetPages));
			else if(offsetPages >0)
			{
				var lastPageIndex:int = pageList.length;
				for (i = lastPageIndex; i < lastPageIndex+offsetPages; i++) 
				{
					addNewAlbumPage(usedLayoutList[i]);
				}
			}			
		}	
		
		
		/**
		 * If project size (width / height) has been modified, we need to scale the pages of the project to fit new size
		 * This is done on project upgrade or when project size has been changed (in document xml)
		 * @ param keepRatio : set to true if you don't want frame ratio to be modified (should not be used but we keep option in case of)
		 */
		private function scaleProjectSize( keepRatio : Boolean = false ):void
		{
			//reset pageBounds to force re-create it with new PFL
			pageBounds = null;
			var newPageBounds : Rectangle = getPageBounds();
			
			var pageVo : PageVo; // the page to scale
			var scaleX : Number; // the scale modification on X
			var scaleY : Number; // the scale modifiction on Y
			
			//let loop through all pages and do updates size
			for (var i:int = 0; i < pageList.length; i++) 
			{
				pageVo = pageList[i];
				if(pageVo.isCover){
					scaleCover( pageVo );
					continue;
				}
				
				//var scaleRatio:Number = (newPageBounds.width + CanvasManager.instance.edgeSize*2) / (pageVo.bounds.width + CanvasManager.instance.edgeSize*2);
				if(newPageBounds.width != pageVo.bounds.width || newPageBounds.height != pageVo.bounds.height)
				{
					scaleX = newPageBounds.width / pageVo.bounds.width;
					scaleY = (keepRatio)? scaleY : newPageBounds.height / pageVo.bounds.height;
					pageVo.bounds = newPageBounds;
					
					//scale layout
					pageVo.scaleLayout(scaleX, scaleY);	
				}
			}
		}
		
		/**
		 * Scale Cover to new cover size
		 */
		private function scaleCover( coverVo : PageVo ):void
		{
			Debug.log("ProjectVo.scaleCover : " + coverVo.coverType);
			var newCoverBounds:Rectangle = getCoverBounds();
				
			//scale ratio 
			var scaleX : Number = newCoverBounds.width / coverVo.bounds.width;
			var scaleY : Number = newCoverBounds.height / coverVo.bounds.height;
			
			// update bounds ref
			coverVo.bounds = newCoverBounds;
			
			//if(type == ProductsCatalogue.ALBUM_CLASSIC || type == ProductsCatalogue.ALBUM_CONTEMPORARY)
			//	coverVo.updateLayout(LayoutManager.instance.getLayoutById(coverVo.layoutVo.id,true,coverVo.layoutVo.isCustom),true); // TODO Check why..
			//else
			coverVo.scaleLayout(scaleX, scaleY);
		}
		
		
		/**
		 * UPDATE project as CANVAS
		 * > if multiple update num pages
		 * > update pages size
		 * > update layouts (size, pelemele and poparts)
		 */
		private function updateProjectAsCanvas( usedLayoutList:XMLList, offsetPages : Number):void
		{
			// update project size
			scaleProjectSize();
			
			// add pages if it's positive
			//Add or remove pages
			if(offsetPages < 0)	pageList.splice(pageList.length-(Math.abs(offsetPages)),Math.abs(offsetPages));
			else if(offsetPages >0)
			{
				var lastPageIndex:int = pageList.length-1;
				for (var i:int = lastPageIndex; i < lastPageIndex+offsetPages; i++) 
				{
					addNewCanvasPage(usedLayoutList[0]);
				}
			}
			
		}	
		
		
		/**
		 * UPDATE project as CALENDAR
		 * Mostly, change bounds of pages. As calendar upgrades are just scale up/down of same layouts
		 */
		private function updateProjectAsCalendar():void
		{
			// update project size
			scaleProjectSize( true ); // we keep ratio in case of project scale
		}	
		
		
		/**
		 * UPDATE project as ALBUM
		 * add/remove pages, change cover etc...
		 */
		private function updateProjectAsAlbum(usedLayoutList:XMLList, coverXML:XML, offsetPages:int):void
		{
			//Cover 
			//Get layout if its not a classic album
			var coverLayoutId:String = getCoverLayoutId(coverType);
			
			//Cover bounds
			coverWidth = coverXML.@pagewidth;
			coverHeight = coverXML.@pageheight;
			
			// get cover VO
			var coverVo:PageVo = CoverManager.instance.getCoverVo();
			
			//change cover completely if type has changed
			if(coverVo.coverType != coverType) {
				updateCover();
				//Refresh current coverVo after update
				coverVo = CoverManager.instance.getCoverVo();
			}
			
			//Update cover bounds and layouts
			if(coverVo.coverType != ProductsCatalogue.COVER_LEATHER) scaleCover( coverVo );
			
			// scale project size
			scaleProjectSize();
			
			//Add or remove pages
			if(offsetPages < 0)
				pageList.splice(pageList.length-(Math.abs(offsetPages)),Math.abs(offsetPages));
			else if(offsetPages >0)
			{
				var lastPageIndex:int = pageList.length-1;
				for (var i:int = lastPageIndex; i < lastPageIndex+offsetPages; i++) 
				{
					addNewAlbumPage(usedLayoutList[i]);
				}
			}
			
		}	
		
		
		/**
		 *Switch cover type
		 * Save current one (remove the first element of the page list)
		 * check for existing 
		 * if not create new one
		 */
		public function updateCover():void
		{
			//unshift the cover page vo
			var currentCoverPageVo:PageVo = Infos.project.pageList.shift();
			
			//Save the currenct cover and removes it from the pagelist
			UserActionManager.instance.savedCurrentCoverPageVo(currentCoverPageVo);
			
			//get new or saved pageVo
			var newCoverPageVo:PageVo;
			//if(coverType == ProductsCatalogue.COVER_LEATHER)
				newCoverPageVo = UserActionManager.instance.getSavedCoverPageVo(coverType);
			
			//if null, let's create a new one
			if(newCoverPageVo == null)
			{
				switch(coverType)
				{
					case ProductsCatalogue.COVER_CUSTOM:
						newCoverPageVo = createAlbumPage(getCoverLayoutId(coverType),true);
						break;
					
					case ProductsCatalogue.COVER_LEATHER:
						newCoverPageVo = createCoverClassicPage();
						break;
				}
			}
			
			//Add cover page vo to he beginning of the page List
			pageList.unshift(newCoverPageVo);
			//Reset the current edited page to trigger update event
			var currenEditedPage:PageVo = PagesManager.instance.currentEditedPage;
			if(PagesManager.instance.currentEditedPage && PagesManager.instance.currentEditedPage.index == newCoverPageVo.index)
				PagesManager.instance.currentEditedPage = newCoverPageVo;
		}	
		
		
		
		
		/**
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * HELPERS
		 * 
		 * getSaveString
		 * getPageBounds
		 * getCoverLayoutId
		 * getCoverBounds
		 * getPageVoByIndex
		 * createCoverClassicPage
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		
		
		/**
		 * transfrom project vo in json string to send to server
		 */
		public function getSaveString():String
		{
			var proj:String = new JSONEncoder(this).getString();
			//trace("proj = "+proj);
			
			return proj;
		}
		
		
		/**
		 * Returns the product page width and height as Rectangle
		 */
		public function getPageBounds():Rectangle
		{
			if(!pageBounds){
				var pageWidth:Number =  Math.round( MeasureManager.PFLToPixel(width));
				var pageHeight:Number = Math.round( MeasureManager.PFLToPixel(height));
				pageBounds = new Rectangle(0,0,pageWidth, pageHeight);
			}
			return pageBounds;
		}
		
		
		/**
		 * Returns the product COVER page width and height as Rectangle
		 * Classic and contemporary only
		 */
		public function getCoverBounds():Rectangle
		{
			var coverBounds:Rectangle;
			var pageWidth:Number =  Math.round( MeasureManager.PFLToPixel(coverWidth, true));
			var pageHeight:Number = Math.round( MeasureManager.PFLToPixel(coverHeight, true));
			
			var spineWidth:Number = getSpineWidth();
			coverBounds = new Rectangle(0,0,pageWidth*2+spineWidth, pageHeight);
			return coverBounds;
		}
		
		/**
		 * Returns spine width
		 * Custom cover only
		 */
		public function getSpineWidth():Number
		{
			return MeasureManager.millimeterToPixel(SpineUtils.customSpineWidth(docCode,paper, coated,_pagePaperQuality));//true
		}
		
		/**
		 * Retrieve the first availbale Cover id
		 */
		private function getCoverLayoutId(_coverType:String):String
		{
			return (_coverType == ProductsCatalogue.COVER_CUSTOM)?LayoutManager.instance.publicAvailableCoverLayoutIds[0]:"";
		}
		
		
		/**
		 * Get Pagevo by Index
		 * */
		public function getPageVoByIndex(index:int):PageVo
		{
			for (var i:int = 0; i < pageList.length; i++) 
			{
				var pageVo:PageVo = pageList[i];
				if(pageVo.index == index)
					return pageVo;
			}
			return null;  
		}
		
		/**
		 * Create a Cover Classic page to a album project
		 * */
		private function createCoverClassicPage():PageCoverClassicVo
		{
			var coverClassicVo:PageCoverClassicVo = new PageCoverClassicVo();
			coverClassicVo.index = 0;//Should always be 0 (it's a cover!)
			coverClassicVo.bounds = getPageBounds();//getCoverBounds();
			coverClassicVo.isCover = true;
			coverClassicVo.coverType = ProductsCatalogue.COVER_LEATHER;
			return coverClassicVo;
		}
		
		/**
		 * Create a  page to a album project
		 * */
		private function createAlbumPage(layoutId:String, isCustomCover:Boolean = false):PageVo
		{
			var pageVo:PageVo = new PageVo();
			pageVo.setLayout(layoutId, isCustomCover);
			pageVo.index = (isCustomCover)?0:pageList.length;
			//pageVo.isCover = (pageVo.index>0)?false:true;
			pageVo.coverType = (pageVo.index>0)?"":ProductsCatalogue.COVER_CUSTOM;
			pageVo.bounds = (!pageVo.isCover)?getPageBounds():getCoverBounds();
			return pageVo;
		}
		
		/**
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Fill project vo 
		 * 
		 * FROM XML
		 * FROM SAVE
		 * FROM HISTORY
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		
		/**
		 
		 * Fill a project vo with projectXML, documentXML and coverXML
		 * This happens At three different times : 
		 * 1. new project creation > we fill the projectvo with the content of the xml
		 * 2. upgrade > we fill the proejctvot with new upgraded xml values
		 * 3. clean&verify > each time we open/load a project, we clean and verify the project. We refill project with values to ensure everything is up to date.
		 
		 * returns true if project need to be updated (size change) and false if everything went right
		 */
		public function fillFromXML(projectXML:XML, documentXML:XML, isCleaning:Boolean = false):Boolean
		{		
			docOrientation = projectXML.docOrientation.@value ; 
			classname = projectXML.docClass.@value ; 
			docType = projectXML.@docidnum ; 
			docCode = projectXML.@docidstr ;
			docPrefix = projectXML.docPrefixStr.@value ;
			resolution = parseInt(projectXML.docResolution[0].@value);
			docModifier =  (projectXML.docModifier[0] != null)? ""+projectXML.docModifier[0].@value :"" ;
			docCutBorder = parseFloat(projectXML.docCutBorder[0].@value) ; 
			coverCutBorder = (projectXML.coverCutBorder[0] != null)?parseFloat(projectXML.coverCutBorder[0].@value):docCutBorder ;
			//docEdgeOffset = (projectXML.docEdgeOffset[0] != null)? ""+projectXML.docEdgeOffset[0].@value : docEdgeOffset ;
			
			// type for canvas do not correspond to docMenuName
			type = projectXML.docMenuName.@value ;
			
			
			var newMultiplier : Number = parseFloat(projectXML.docMeasure[0].@value) ;
			var newCoverMultiplier : Number = (projectXML.coverMeasure[0] != null)?parseFloat(projectXML.coverMeasure[0].@value):newMultiplier;
			var newWidth : Number = parseFloat(documentXML.@pagewidth);
			var newHeight : Number =  parseFloat(documentXML.@pageheight);
			
			// check possible project size modification to display warning
			var needSizeUpdate : Boolean = false;
			if(isCleaning)
			{
				if(newMultiplier != PFLMultiplier) {
					Debug.warn("ProjectVo.cleanAndVerify : Project PFL multiplier has changed (before:"+PFLMultiplier+" after:"+newMultiplier); 
					needSizeUpdate = true;
				}
				if(newCoverMultiplier != PFLCoverMultiplier) {
					Debug.warn("ProjectVo.cleanAndVerify : Project Cover PFL multiplier has changed (before:"+PFLCoverMultiplier+" after:"+newCoverMultiplier); 
					needSizeUpdate = true;
				}
				if(newWidth != width) {
					Debug.warn("ProjectVo.cleanAndVerify : Project Width has changed (before:"+width+" after:"+newWidth);
					needSizeUpdate = true;
				}
				if(newHeight != height) {
					Debug.warn("ProjectVo.cleanAndVerify : Project Height has changed (before:"+height+" after:"+newHeight);
					needSizeUpdate = true;
				}
			}
			// @end of size update check
			
			
			PFLMultiplier = newMultiplier;
			PFLCoverMultiplier = newCoverMultiplier;
			
			// width and height comes from xml, but for canvas Multiple and Custom, with and height comes from init params (which are already set before)
			if( docPrefix != "CPM" && docPrefix != "CPF" ){
				width = newWidth;
				height = newHeight;	
			}
			
			// num pages are specified in the document xml file
			// but for CPM format, user can choose the canvas layout and so the canvas pages
			if( docPrefix == "CPM" ){
				if(!canvasMLayout) {
					Debug.error("ProjectVo > FillFromXML > canvas prefix is CPM but no canvasMLayout specified, project is blocked.");
					//ProjectManager.instance.startNewProject();
					return false;
				}
				numPages = CanvasManager.instance.getMultiplePageAmount();
			} else numPages = parseInt(projectXML.docPages[0].@value);
			
			
			// as we enter in this function two times (creation and load verification) we should only apply the cover type if it's a fresh project creation
			if(!isCleaning)
			coverType = projectXML.coverManifestName.@value;
			
			adjustCropX = (projectXML.adjustCropX[0] != null)?parseFloat(projectXML.adjustCropX[0].@value):adjustCropX ;
			adjustCropY = (projectXML.adjustCropY[0] != null)?parseFloat(projectXML.adjustCropY[0].@value):adjustCropY ;
			
			/* Example ofProject xml
			------------------
			<code0 docidnum="101" docidstr="S30">
			<docPrefixStr value="S"/>
			<docPages value="32"/>
			<docMeasure value="4.677165"/>
			<docResolution value="144.0"/>
			<docCutBorder value="12.5"/>
			<docEdgeOffset value="-3"/>
			<docClass value="albums"/>
			<docMenuName value="types_albums_classic"/>
			<pagesPerUnit value="2"/>
			<pagesPerJump value="2"/>
			<docOrientation value="landscape"/>
			<coverManifestName value="Leather Black"/>
			<coverMeasure value="4.640000"/>
			<coverCutBorder value="25.0"/>
			<adjustCropX value="5.0"/>
			</code0>
			-----------------------
			
			Example of Document xml
			-----------------------
			<document name="S20" 
			type="album" 
			coverrequired="true" 
			pagecount="22" 
			pagewidth="138" 
			pageheight="100">
			-----------------------
			*/
			
			
			return needSizeUpdate;
		}
		
		/**
		 * Fill a project vo with projectXML, documentXML and coverXML
		 */
		public function fillFromHistory(projectObj:Object):void
		{
			flyleafColor = projectObj.flyleafColor;
			pageNumberCentered = projectObj.pageNumberCentered;
			pageNumber = projectObj.pageNumber;
			docOrientation = projectObj.docOrientation;
			classname = projectObj.classname;
			docType = projectObj.docType;
			docCode = projectObj.docCode;
			docPrefix = projectObj.docPrefix;
			numPages = projectObj.numPages;
			resolution = projectObj.resolution;
			PFLMultiplier = projectObj.PFLMultiplier;
			PFLCoverMultiplier = projectObj.PFLCoverMultiplier;
			docCutBorder = projectObj.docCutBorder;
			coverCutBorder = projectObj.coverCutBorder;
			type = projectObj.type;
			width = projectObj.width;
			height = projectObj.height;
			coverWidth = projectObj.coverWidth;
			coverHeight = projectObj.coverHeight;
			coverType = projectObj.coverType;
			adjustCropX = projectObj.adjustCropX;
			adjustCropY = projectObj.adjustCropY;
			paper = projectObj.paper;
			coated = projectObj.coated;
			pagePaperQuality = projectObj.pagePaperQuality;
			
			// calendar specific
			calendarStartDayIndex = projectObj.calendarStartDayIndex;
			calendarStartMonthIndex = projectObj.calendarStartMonthIndex;
			calendarYear = projectObj.calendarYear;
			
			// canvas specific
			canvasType = projectObj.canvasType;
			canvasMargin = projectObj.canvasMargin;
			canvasMLayout = projectObj.canvasMLayout;
			if( projectObj.multipleFrameVo ){
				multipleFrameVo = new FrameVo()
				multipleFrameVo.fill( projectObj.multipleFrameVo );
			}
			canvasFrameColor = projectObj.canvasFrameColor;
			canvasStyle = projectObj.canvasStyle;
			if( projectObj.canvasPriceInfo )
				canvasPriceInfo = CanvasPriceInfo.CreateFromSave(projectObj.canvasPriceInfo);
			
			// cards specific
			envelope = projectObj.envelope;
			
			// offline only
			_imageIdCount = (!isNaN(projectObj.imageIdCount))? projectObj.imageIdCount : 0;
			creationDate = projectObj.creationDate;
			lastOpenedOfflinePath = projectObj.lastOpenedOfflinePath;
			imageStorageSystem = (projectObj.imageStorageSystem)? projectObj.imageStorageSystem : STORAGE_TYPE_COPY; // default for retro compatibility is by copy
			modificationDate = projectObj.modificationDate;
			patched = projectObj.patched;
			
			// pages
			pageList = new Vector.<PageVo>;
			for (var i:int = 0; i < projectObj.pageList.length; i++) 
			{
				var page:PageVo = (projectObj.pageList[i].isClassicCover)?new PageCoverClassicVo():new PageVo();
				page.fillFromSave(projectObj.pageList[i]);
				pageList.push(page);
			}
			
		}
		
		/**
		 * construct a project vo by a saved string
		 */
		public function fillFromSave(jsonProj:Object):void
		{
			// Calendar colors
			/*if(jsonProj.globalCalendarColors)
			{
				globalCalendarColors = new CalendarColors();
				for (var props:String in jsonProj.globalCalendarColors) {
					globalCalendarColors[props] = jsonProj.globalCalendarColors[props];
				}	
			}
			else globalCalendarColors = new CalendarColors();
			//apply it to the static var
			CalendarColors.GLOBAL_COLORS = globalCalendarColors;*/
			
			//Calendars 
			calendarStartDayIndex = jsonProj.calendarStartDayIndex;
			calendarStartMonthIndex = jsonProj.calendarStartMonthIndex;
			calendarYear = jsonProj.calendarYear;
			
			// canvas specific
			canvasType = jsonProj.canvasType;
			canvasMargin = jsonProj.canvasMargin;
			canvasMLayout = jsonProj.canvasMLayout;
			if( jsonProj.multipleFrameVo ){
				multipleFrameVo = new FrameVo()
				multipleFrameVo.fill( jsonProj.multipleFrameVo );
			}
			canvasFrameColor = jsonProj.canvasFrameColor;
			canvasStyle = jsonProj.canvasStyle;
			if( jsonProj.canvasPriceInfo )
				canvasPriceInfo = CanvasPriceInfo.CreateFromSave(jsonProj.canvasPriceInfo);
			
			// cards specific
			envelope = jsonProj.envelope;
			
			
			flyleafColor = (jsonProj.flyleafColor)?jsonProj.flyleafColor:"white";
			pageNumberCentered = jsonProj.pageNumberCentered ;
			pageNumber = jsonProj.pageNumber ;
			classname = jsonProj.classname ;
			docOrientation = jsonProj.docOrientation ;
			build = jsonProj.build ;
			docType = jsonProj.docType ;
			docCode = jsonProj.docCode ;
			numPages = jsonProj.numPages ;
			width = jsonProj.width ;
			height = jsonProj.height ;
			coverWidth = jsonProj.coverWidth;
			coverHeight = jsonProj.coverHeight;
			PFLMultiplier = jsonProj.PFLMultiplier ;
			PFLCoverMultiplier = jsonProj.PFLCoverMultiplier ;
			if(isNaN(PFLCoverMultiplier)) PFLCoverMultiplier = PFLMultiplier; // TODO remove this later, this was done to avoid crash while adding new PFLCovermultiplier to existing projects
			resolution = jsonProj.resolution ;
			type = jsonProj.type;
			
			docCutBorder = jsonProj.docCutBorder;
			coverCutBorder = jsonProj.coverCutBorder;
			adjustCropX = jsonProj.adjustCropX;
			adjustCropY = jsonProj.adjustCropY;
			
			// name can be changed offline using the wizard, we can't then be sure name is still correct.
			//name = jsonProj.name;
			
			docPrefix = jsonProj.docPrefix ;
			coverType = jsonProj.coverType ;
			paper = jsonProj.paper;
			coated = (jsonProj.pagePaperQuality)?jsonProj.coated:(classname == ProductsCatalogue.CLASS_ALBUM)?true:false;
			pagePaperQuality = (jsonProj.pagePaperQuality)?jsonProj.pagePaperQuality:(type == ProductsCatalogue.ALBUM_CASUAL)?ProductsCatalogue.PAPER_QUALITY_170:ProductsCatalogue.PAPER_QUALITY_250;
			
			// offline only
			_imageIdCount = (!isNaN(jsonProj.imageIdCount))? jsonProj.imageIdCount : 0;
			creationDate = jsonProj.creationDate;
			modificationDate = jsonProj.modificationDate;
			lastOpenedOfflinePath = jsonProj.lastOpenedOfflinePath;
			imageStorageSystem = (jsonProj.imageStorageSystem)? jsonProj.imageStorageSystem : STORAGE_TYPE_COPY; // default for old project is by copy
			patched = jsonProj.patched;
			
			// pagelist
			pageList = new Vector.<PageVo>;
			for (var i:int = 0; i < jsonProj.pageList.length; i++) 
			{
				var page:PageVo = (jsonProj.pageList[i].isClassicCover)?new PageCoverClassicVo():new PageVo();
				page.fillFromSave(jsonProj.pageList[i]);
				pageList.push(page);
			}
		}
		
	}
}