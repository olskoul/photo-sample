package data
{
	
	/**
	 * AlbumVo contains a list of all photos inside the album
	 */
	public class AlbumVo
	{
		public var name : String; // name of the photo folder
		public var photoList : Vector.<PhotoVo>;
		public var mostRecentId : int ;
		
		public function AlbumVo():void{};
	}
}