package data
{
	import flash.geom.Rectangle;
	
	import offline.data.LocalInstances;

	public class ThumbVo
	{
		public var id:String; // uniq ID
		public var bounds:Rectangle; // bounds
		public var label:String; // bounds
		private var _thumbUrl : String; // url to the thumb
		//public var localInstances : LocalInstances = new LocalInstances(); 	// if assets exist locally (Bkg, cliparts, overlays) not saved
		
		public function ThumbVo()
		{}
		
		
		/**
		 * Check if the vo is valid
		 */
		public function get isValidVo():Boolean
		{
			return (id!=null);
		}
		
		public function get thumbUrl():String
		{
			return _thumbUrl;
		}

		public function set thumbUrl(value:String):void
		{
			_thumbUrl = value;
		}

		/**
		 * FIll Vo
		 */
		public function fill(obj:Object):void
		{
			id = obj.id;
			bounds = obj.bounds;
			label = obj.label;
			thumbUrl = obj.thumbUrl;

		}
	}
}