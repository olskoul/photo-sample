package data
{
	public class QRVo extends FrameVo
	{
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function QRVo(){ super(); }
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		/**
		 * Check if the vo is valid
		 */
		override public function get isValidVo():Boolean
		{
			return (QRLink != "");
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		
		
		/**
		 * FIll Vo from json or object
		 */
		override public function fill(obj:Object):void
		{
			super.fill(obj);
			
			
			
		}
	}
}