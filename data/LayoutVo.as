package data
{
	import flash.geom.Rectangle;
	
	import manager.CanvasManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	
	import utils.Debug;
	
	import view.edition.PageArea;

	public class LayoutVo
	{
		public var id:String;				// The xml id of layout
		public var frameList:Vector.<FrameVo> = new Vector.<FrameVo>(); // list of frame vo
		public var isCalendar:Boolean;		// is this a calendar layout
		public var isCustom:Boolean;		// is this a custom layout
		public var type:String = "";		// hide or not 
		public var photoNumber:int = 0;		// number of photos in this layout
		
		public function LayoutVo()
		{
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * scale a layout with a 
		 */
		public function scaleLayout( scaleX:Number, scaleY:Number = NaN):void
		{
			if(isNaN(scaleY)) scaleY = scaleX;
			
			// scale each frame of layout
			for (var i:int = 0; i < frameList.length; i++) 
			{
				var frameVo:FrameVo = frameList[i];				
				
				// scale spine frame
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
				{
					var safetyZone:Number = MeasureManager.millimeterToPixel(20)
					frameVo.width = Infos.project.getCoverBounds().height - (safetyZone*2);
					frameVo.height = Infos.project.getSpineWidth();
					frameVo.y = frameVo.width/2 + safetyZone;
					frameVo.x = Infos.project.getCoverBounds().width/2;
				}
				
				// scale cover edition frame
				else if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
				{
					frameVo.width = Infos.project.getSpineWidth();
					frameVo.height =  MeasureManager.millimeterToPixel(15);//frameVo.height;
					frameVo.y = Infos.project.getCoverBounds().height - frameVo.height/2;
					frameVo.x = Infos.project.getCoverBounds().width/2;
				}

				// if is canvas
				else if (Infos.isCanvas ) {
					// if we are in canvas, modify layout by taking edge size into account
					scaleCanvasFrame( frameVo , scaleX, scaleY );
					if(frameVo.photoVo) FrameVo.injectPhotoVoInFrameVo(frameVo, frameVo.photoVo); // if framevo has a photovo, we reinject photo into it
				}
					
				// normal framevo scale
				else
				{
					frameVo.width = frameVo.width*scaleX;
					frameVo.height = frameVo.height*scaleY;
					frameVo.x = frameVo.x*scaleX;
					frameVo.y = frameVo.y*scaleY;
					
					
					/* TODO we need this go this way, do not inject in frame but update frame zoom and cleft to be the more fitting original picture
					// update crop and scale values
					frameVo.cLeft = frameVo.cLeft*scaleX;
					frameVo.cTop = frameVo.cTop*scaleX;
					frameVo.zoom = frameVo.zoom*scaleX;
					
					// frame vo has a photo but frame ratio has changed, we will need to check if new photo placement fits in frame
					// 
					
					if(scaleX != scaleY && frameVo.photoVo)
					{
						
						// if not reinject it
						//FrameVo.injectPhotoVoInFrameVo(frameVo, frameVo.photoVo);
					}
					*/
						
					
					
					// if scaleX and scaleY are different, we must inject photo in frame again not only modify values
					if(scaleX != scaleY && frameVo.photoVo){ // if scale x and y are different and there is a photo, we just reinject it correctly in new frame
						FrameVo.injectPhotoVoInFrameVo(frameVo, frameVo.photoVo);
					} 
					// else we can securely update crop, zoom as there is no photovo or frame ratio is identic as previously
					else 
					{
						frameVo.cLeft = frameVo.cLeft*scaleX;
						frameVo.cTop = frameVo.cTop*scaleX;
						frameVo.zoom = frameVo.zoom*scaleX;
					}
					
					
				}
			}
		}
		
		/**
		 * for canvas, scaling a frame can be difficult as we need to scale only the content 
		 * so we check afte a frame update that nothing goes further than the edge, if there is, modify the frame
		 */ 
		private function scaleCanvasFrame( frame : FrameVo, ratio:Number, ratioH:Number):void
		{
			var edgeSize : Number = CanvasManager.instance.edgeSize;
			var frameX : Number = frame.x - frame.width*.5;
			var frameY : Number = frame.y - frame.height*.5;
			var pageBounds : Rectangle = Infos.project.getPageBounds();
			var oldBounds:Rectangle = new Rectangle(0,0, pageBounds.width/ratio, pageBounds.height/ratioH);
			
			var offsets:Array = [0,0,0,0]; // rectangle representing the offset of the current frame (left, top, right, bottom), check what's outside the page bounds
			if(frameX < 0) offsets[0] = -frameX;
			if(frameY < 0) offsets[1] = -frameY;
			if(frame.x + frame.width*.5 > oldBounds.width ) offsets[2] = frame.x + frame.width*.5 - oldBounds.width;
			if(frame.y + frame.height*.5 > oldBounds.height )offsets[3] = frame.y + frame.height*.5 - oldBounds.height;
			
			// offest left
			frame.width = (frame.width - offsets[0] - offsets[2] )*ratio + offsets[0] + offsets[2];
			frame.height = (frame.height - offsets[1] - offsets[3] )*ratioH + offsets[1] + offsets[3];
			
			frame.x = (frame.x + (offsets[0]*.5-offsets[2]*.5))*ratio - offsets[0]*.5 + offsets[2]*.5 ;
			frame.y = (frame.y + (offsets[1]*.5-offsets[3]*.5))*ratioH - offsets[1]*.5 + offsets[3]*.5 ;
			
		}
		
		
		public function getLayoutProps():Object
		{
			var savedObj:Object = {};
			//Save photos
			var savedText:Vector.<String> = new Vector.<String>();
			var savedProps:Vector.<FrameVo> = new Vector.<FrameVo>();
			for (var i:int = 0; i < frameList.length; i++) 
			{
				//photo
				var frameVo:FrameVo = frameList[i];				
				if(frameVo.type == FrameVo.TYPE_PHOTO && frameVo.photoVo != null)
					savedProps.push(frameVo);
				
				//text
				if(frameVo.type == FrameVo.TYPE_TEXT && frameVo.grouptype == FrameVo.GROUP_TYPE_NONE && frameVo.text != "" && !frameVo.isPageNumber )
					savedText.push(frameVo.text);
				
				//spine
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
					savedObj.spine = frameVo.text;
				//edition
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
					savedObj.edition = frameVo.text;
				
				//PageNumber
				if(Infos.project.pageNumber && frameVo.isPageNumber)
					savedObj.pageNumberFrame = frameVo;
			}
			//Save BKG props
			savedObj.background = {};
			savedObj.background.fillColor = frameList[0].fillColor;
			savedObj.background.photoVo = frameList[0].photoVo;
			
			savedObj.props = savedProps;
			savedObj.texts = savedText;
			return savedObj;
		}
		
		public function injectProps(savedProps:Object, pageVo:PageVo):void
		{
			Debug.log("LayoutVo.injectProps");
			//Inject props
			for (var i:int = 0; i < frameList.length; i++) 
			{
				var frameVo:FrameVo = frameList[i];
				if(frameVo.type == FrameVo.TYPE_PHOTO && frameVo.photoVo == null && savedProps.props.length > 0)
				{
					var propsToUse:FrameVo = savedProps.props.shift();
					frameVo.mask = propsToUse.mask;
					frameVo.border = propsToUse.border;
					frameVo.fillColor = propsToUse.fillColor;
					frameVo.shadow = propsToUse.shadow;
					FrameVo.injectPhotoVoInFrameVo(frameVo,propsToUse.photoVo);	
				}
				
				if(frameVo.type == FrameVo.TYPE_TEXT && frameVo.grouptype == FrameVo.GROUP_TYPE_NONE && frameVo.text == "" )
				{
					var textToUse:String = savedProps.texts.shift();
					if(textToUse != null && textToUse != "")
					frameVo.text = textToUse;
				}
				
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
					frameVo.text = savedProps.spine;
				
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
					frameVo.text = savedProps.edition;
			}
			
			//page Number
			if(Infos.project.pageNumber && savedProps.pageNumberFrame != null)
				frameList.push(savedProps.pageNumberFrame);
			
			//injectBkg
			frameVo = frameList[0];
			if(savedProps.background.photoVo)
			{
				FrameVo.injectPhotoVoInFrameVo(frameVo,savedProps.background.photoVo);
				frameVo.fillColor = -1;
			}
			else
			{
				frameVo.fillColor = savedProps.background.fillColor;
				frameVo.photoVo = null;
			}
			
		}
	}
}