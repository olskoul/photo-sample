/***************************************************************
 COPYRIGHT 		: Jonathan@nguyen.eu
 YEAR			: 2013
 * 
 * Product Catalogue Class
 * 
 * Contains hard coded information (at the moment) about product available
 * more?...
 ****************************************************************/

package data
{
	import flash.geom.Rectangle;

	public class ProductsCatalogue
	{
		// those constants are the different type of product that can be chosen whitin the app
		public static const CLASS_ALBUM			:String = "albums";
		public static const CLASS_CANVAS		:String = "canvas";
		public static const CLASS_CALENDARS		:String = "calendars";
		public static const CLASS_CARDS			:String = "cards";
		public static const CLASS_FUN			:String = "fun";
		
		//COVERS 
		public static const COVER_LEATHER		:String = "Leather Black";
		public static const COVER_CUSTOM		:String = "Custom Cover";
		public static const COVER_FABRIC_LEATHER:String = "leather";
		public static const COVER_FABRIC_LINEN	:String = "linen";
		
		//ALBUM TYPES
		public static const ALBUM_CLASSIC		:String = "types_albums_classic";
		public static const ALBUM_CONTEMPORARY	:String = "types_albums_contemporary";
		public static const ALBUM_TRENDY		:String = "types_albums_trendy";
		public static const ALBUM_CASUAL		:String = "types_albums_casual";
		
		public static const CANVAS_ORIENTATION_CUSTOM	:String = "types_canvas_freeformat";
		public static const CANVAS_ORIENTATION_MULTIPLE	:String = "types_canvas_multiple";
		
		
		public static const PAPER_QUALITY_170	:String = "170";
		public static const PAPER_QUALITY_200	:String = "200";
		public static const PAPER_QUALITY_250	:String = "250";
		
		
		
		public function ProductsCatalogue()
		{
		}
		
		
		
		
	}
}