/**
 * PageCoverClassicVo
 * This contains information about the cover (page 0 in pagList)
 * It should be linked to a PageCoverClassicArea
 * 
 * */

package data
{
	import utils.Debug;
	
	public class PageCoverClassicVo extends PageVo
	{
		public var isClassicCover:Boolean = true;
		public var line1:String = "";
		public var line2:String = "";
		public var spine:String = "";
		public var edition:String = "";
		public var cover:String = "Leather Black";
		public var corners:String = "none";
		public var color:String = "gold";
		public var fontName:String = "Arial";
		public var coverLabelName:String = "leatherblack";
		public var coverFabric:String = ProductsCatalogue.COVER_FABRIC_LEATHER;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PageCoverClassicVo()
		{
			isCover = true;
			super();
		}
		
		////////////////////////////////////////////////////////////////
		//	OVERRIDEN METHODS
		////////////////////////////////////////////////////////////////
		/**
		 *	fill a page vo from project save
		 */
		override public function fillFromSave(pageObj:Object):void
		{
			super.fillFromSave(pageObj);
			isClassicCover = pageObj.isClassicCover;
			line1 = pageObj.line1;
			line2 = pageObj.line2;
			spine = pageObj.spine;
			edition = pageObj.edition;
			cover = pageObj.cover;
			fontName = pageObj.fontName;
			corners = pageObj.corners;
			color = pageObj.color;
			coverLabelName = pageObj.coverLabelName;
			coverFabric = pageObj.coverFabric;
			
			// VERIFY !! as there was a bug here with coverLabelName, we add a fix to be sure there wasn't an issue with user's project
			if( cover && cover != ""){
				var coverLabelNameCheck : String = cover.split(" ").join("").toLowerCase();
				if(coverLabelNameCheck != coverLabelName){
					//
					Debug.warnAndSendMail("PageCoverClassicVo.fillFromSave : fix for coverlabelname not saved : cover = '" + cover + "' but coverLabelname was : " + coverLabelName);
					coverLabelName = coverLabelNameCheck;
					
					// to be sure, replace coverFarbric too
					coverFabric = (coverLabelName.indexOf(ProductsCatalogue.COVER_FABRIC_LINEN) != -1)? ProductsCatalogue.COVER_FABRIC_LINEN : ProductsCatalogue.COVER_FABRIC_LEATHER ;
				}
			}
		}
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHOD
		////////////////////////////////////////////////////////////////
		
		/**
		 * TODO : maybe use a variable in the vo created in the editor view to be sure
		 */
		public function get hasCoverText():Boolean
		{
			if(line1 == "" && line2 =="" && spine == "" && edition == "") return false;
			return true;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHOD
		////////////////////////////////////////////////////////////////
	}
}