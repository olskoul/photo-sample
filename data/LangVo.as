package data
{
	import utils.Debug;

	public class LangVo
	{
		public var id:String;
		public var label:String;
		public var loadingLabel:String;
		public var videoTutorialUrl:String;
		public var faqUrl:String;
		
		public function LangVo()
		{
		}
		
		public function fill(node:XML) :void
		{
			id = node.@id;
			//label = node.@label;
			loadingLabel = node.@loadingLabel;
			videoTutorialUrl = (node.videoTutorialUrl)?node.videoTutorialUrl:"";
			faqUrl = (node.faqUrl)?node.faqUrl:"";
			
			Debug.log("LangVo.fill > " + id +": "+ node);
		}
	}
}