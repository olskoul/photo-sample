package data
{
	import manager.CanvasManager;
	
	import offline.data.CanvasPriceInfo;

	/**
	 * Parameters that can be passed to the projectManager createProject function
	 * Those parameters are set in the configuratorTabs
	 */ 	
	public class ProjectCreationParamsVo
	{
		// COMMON
		public var coated : Boolean; //aka vernish 
		public var paperQuality : String; //aka vernish 
		public var flyleaf : String; //aka vernish 
		
		// ALBUM
		public var coverType : String;
		public var paper : Boolean;
		public var pageNumber : Boolean;

		// CALENDARS
		public var calendarStartDayIndex:int;
		public var calendarStartMonthIndex:int;
		public var calendarYear:int;
		
		// CANVAS
		public var canvasType : String ; 	// = canvas type/material (forex, wood, alu, etc..)
		public var canvasFrameColor : String ; 	// = canvas frame color for kadapak (black, silver, gold, blur or red)
		public var canvasWidth : Number; 	// = width of canvas for custom/multiple canvas type
		public var canvasHeight : Number; 	// = height of canvas for custom/multiple canvas type
		public var canvasMargin : Number;	// = margin of canvas for multiple canvas type
		public var canvasMLayout : String;	// = layout for multiple canvas type
		public var canvasStyle : String ; // = styles_style1, styles_style2, styles_style5
		public var canvasPriceInfo : CanvasPriceInfo ; // offline calculation price
		
		// CARDS
		public var envelope : String ;
		
		/**
		 * SHORTCUT TO CREATE project init params based on session xml
		 */ 
		public static function createWithSessionClassname():ProjectCreationParamsVo
		{
			var sess : SessionVo = Infos.session;
			var classname : String =  sess.classname;
			var params : ProjectCreationParamsVo = new ProjectCreationParamsVo();
			
			// CASE ALBUMS
			if( classname == ProductsCatalogue.CLASS_ALBUM ){
				params.paper = (sess.product_inserts == "1")? true : false;
				params.paperQuality = sess.product_paperQuality;
				params.flyleaf = sess.product_flyleaf;
				params.coated = (sess.product_coated == "1")? true : false;
			

				params.coverType = (sess.cover_name == ProductsCatalogue.COVER_LEATHER)? ProductsCatalogue.COVER_LEATHER : ProductsCatalogue.COVER_CUSTOM;
			}
			
			//CASE CALENDARS
			else if( classname == ProductsCatalogue.CLASS_CALENDARS ){
				params.calendarStartDayIndex = int(sess.weekstart);
				params.calendarStartMonthIndex = int(sess.month);
				params.calendarYear = int(sess.year);
				params.coated = (sess.product_coated == "1")? true : false;
			}
			
			//CASE CANVAS
			else if( classname == ProductsCatalogue.CLASS_CANVAS ){
				// check for canvas modifier system
				params.canvasType = CanvasManager.instance.getCanvasTypeByModifiedProductId( parseInt(sess.product_id) )
				sess.product_id = ""+ (parseInt(sess.product_id) - CanvasManager.instance.getCanvasModifierByCanvasType(params.canvasType));
				params.canvasFrameColor = sess.frame_color;
				params.canvasMargin = (!isNaN(parseFloat(sess.gap)))? parseFloat(sess.gap)  : null; // in cm
				params.canvasWidth = (!isNaN(parseFloat(sess.width)))? parseFloat(sess.width)  : null; // in cm
				params.canvasHeight = (!isNaN(parseFloat(sess.height)))? parseFloat(sess.height) : null; // in cm
				params.canvasMLayout = (!isNaN(parseFloat(sess.rows)))? sess.rows + "x" + sess.cols : null;
				switch (sess.product_style ) {
					case "popart" :
						params.canvasStyle = "styles_style5";
						break;
					case "pelemele" :
						params.canvasStyle = "styles_style2";
						break;
					default : 
						params.canvasStyle = "styles_style1";
						break;
				}
			}
			
			//CASE CARDS
			else if( classname == ProductsCatalogue.CLASS_CARDS ){
				params.envelope = sess.envelope;
			}
			
			
			return params;
		}
		

	}
}