package data
{

	/**
	 * BackgroundCustomVo is a photoVo that is used as a background.
	 * It uses a regular photo that is cropped, zoomed if necessary
	 */
	public class BackgroundCustomVo extends ThumbVo
	{
		public var bkgId:String; //type of project this custom background has been saved to be used into
		public var docType:String; //type of project this custom background has been saved to be used into
		public var isCover:Boolean;	
		public var frameVoStr:String;
		public var thumbImagePath:String;
		//public var frameVoStr:String = '{"mask":0,"width":617.95270136,"visible":false,"height":867.32488536,"cTop":0,"border":0,"cLeft":464.0740906191304,"uniqFrameLinkageID":null,"isMultiple":false,"alignment":0,"calFontName":null,"PopArtTreshold":50,"fixed":false,"PopArtDarkColor":173029,"isDeletable":true,"vAlign":"top","cr":0,"calFontSizeRaw":null,"calItalic":false,"editable":true,"PopArtLightColor":10344175,"calBold":false,"PopArtBlackTreshold":30,"dateAction":null,"fillColor":16777215,"dateIndex":0,"fontSize":0,"x":297.6,"photoVo":{"thumbImagePath":"/resources/8ad17b74c80ce0f3d1669cf8478479469fb33379.6c311cd9577f92449b42bff45689505bddb341ca.98c3da69fa1566f7edeb98e16d3a4f95a1c60240/tn/3914485_tn.jpg","originalDate":1347910051000,"width":4592,"folderName":"@@182925@@","workingImagePath":"/resources/8ad17b74c80ce0f3d1669cf8478479469fb33379.6c311cd9577f92449b42bff45689505bddb341ca.98c3da69fa1566f7edeb98e16d3a4f95a1c60240/work/3914485_work.jpg","used":true,"name":"DSC01271.JPG","height":2576,"lastModified":1401874822000,"id":"3914485","temp":false,"label":null,"bounds":null,"fullsizeImagePath":"/resources/8ad17b74c80ce0f3d1669cf8478479469fb33379.6c311cd9577f92449b42bff45689505bddb341ca.98c3da69fa1566f7edeb98e16d3a4f95a1c60240/3914485.jpg","tempID":null,"suffix":"001/257171/images/3914485.jpg","highUrl":"http://editordev.tictacphoto.com/resources/8ad17b74c80ce0f3d1669cf8478479469fb33379.6c311cd9577f92449b42bff45689505bddb341ca.98c3da69fa1566f7edeb98e16d3a4f95a1c60240/3914485.jpg","normalUrl":"http://editordev.tictacphoto.com/resources/8ad17b74c80ce0f3d1669cf8478479469fb33379.6c311cd9577f92449b42bff45689505bddb341ca.98c3da69fa1566f7edeb98e16d3a4f95a1c60240/work/3914485_work.jpg","thumbUrl":"http://editordev.tictacphoto.com/resources/8ad17b74c80ce0f3d1669cf8478479469fb33379.6c311cd9577f92449b42bff45689505bddb341ca.98c3da69fa1566f7edeb98e16d3a4f95a1c60240/tn/3914485_tn.jpg","isValidVo":true},"y":422.3,"stroke":false,"hAlign":"center","isPopart":false,"isPostCardBackground":false,"color":0,"zoom":0.33669444307453417,"grouptype":0,"rotation":0,"text":"","shadow":false,"printQuality":1,"isEmpty":false,"printCoords":{"x":-11.376350679999973,"y":-11.362442680000015,"length":16.07875798843374},"isPrintUpload":false,"boundRect":{"x":-308.97635068,"width":617.95270136,"height":867.32488536,"y":-433.66244268,"size":{"x":617.95270136,"y":867.32488536,"length":1064.9497630793946},"bottom":433.66244268,"top":-433.66244268,"topLeft":{"x":-308.97635068,"y":-433.66244268,"length":532.4748815396973},"bottomRight":{"x":308.97635068,"y":433.66244268,"length":532.4748815396973},"left":-308.97635068,"right":308.97635068},"type":"bkg","isValidVo":true}';
		
		
		public function BackgroundCustomVo()
		{
			super();
			
		}
		
		////////////////////////////////////////////////////////////////
		//	OVEERIDES
		////////////////////////////////////////////////////////////////
		/**
		 * url to the thumb 
		 */
		override public function get thumbUrl():String{ 
			if(!Infos.IS_DESKTOP)
			return Infos.config.serverNormalUrl + thumbImagePath;
			else
				return thumbImagePath;
		};
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
				
		/**
		 * FIll Vo from XML
		 */
		public function fillFromXML(node:XML):void
		{
			bkgId = node.@id;
			docType = node.@docType;
			isCover = (node.@isCover == "true")?true:false;
			frameVoStr = node.framevo;
			thumbImagePath = node.thumbUrl;
		}
	}
}