package data
{
	import manager.ProjectManager;
	
	import utils.Colors;
	
	/**
	 * Colorisation of the calendars
	 * Each customisation has its variable (month, year..)
	 * Posibility to use the global set of color and apply it to every calendar pageVO
	 * */
	public class CalendarColors
	{
		
		private var _month:Number = Colors.BLACK; // color of the month's textfield (january, february...)
		private var _year:Number = Colors.BLACK; // color of the year's textfield (2013,2014 etc..)
		private var _border:Number = Colors.BLACK; // color of the borders (when showBorder is set to true)
		private var _weekDay:Number = Colors.BLACK; // color of the week day numbers
		private var _weekEndDay:Number = Colors.RED_FLASH; // color of the weekEnd day numbers
		private var _offSetDay:Number = Colors.GREY; // color of the previous and next month day numbers
		private var _dayBackground:Number = -1; // color of days background (-1) = transparent
		
		public function CalendarColors()
		{
			
		}

		public function get dayBackground():Number
		{
			return _dayBackground;
		}
		public function set dayBackground(value:Number):void
		{
			_dayBackground = value;
		}

		public function get offSetDay():Number
		{
			return _offSetDay;
		}
		public function set offSetDay(value:Number):void
		{
			_offSetDay = value;
		}
		
		public function get weekEndDay():Number
		{
			return _weekEndDay;
		}
		public function set weekEndDay(value:Number):void
		{
			_weekEndDay = value;
		}
		
		public function get weekDay():Number
		{
			return _weekDay;
		}
		public function set weekDay(value:Number):void
		{
			_weekDay = value;
		}

		public function get border():Number
		{
			return _border;
		}
		public function set border(value:Number):void
		{
			_border = value;
		}

		public function get year():Number
		{
			return _year;
		}
		public function set year(value:Number):void
		{
			_year = value;
		}

		public function get month():Number
		{
			return _month;
		}
		public function set month(value:Number):void
		{
			_month = value;
		}

	}
}