/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/
/***************************** 
  
 RE-BRANDING ONLINE HOW TO PROCESS 
 
 * - Define and uniq identifier for the brand ex: "babyboomphoto"
 * - Use the same in every modification (folder, path, config... etc)
 * 
 * - Create a node <brand> inside <rebranding> tag in config.xml. (ex: see config.xml file)
 * ----- Change values
 * 
 * - To apply rebranding pass a flashvars named "rebrandingID" with value of the uniq identifier difned before (ex:"babyboomphoto");
 
 *****************************/ 

/*****************************
 
 RE-BRANDING OFFLINE HOW TO PROCESS
 
 * - Define and uniq identifier for the brand ex: "babyboomphoto"
 * - Use the same in every modification (folder, path, config... etc)
 * 
 * - Create icons (all of them in a separate folder ex: /babyboom)
 * 
 * - Duplicate -app.xml and:
 * ----- Change app id (ex:com.babyboomphoto.desktopApp)
 * ----- Change Filename and Name (ex:babyboomphoto)
 * ----- Change rebrandingid = uniq identifier
 * ----- Change icon Path (ex:icons/babyboom/16x16.png)
 * ----- Change instalFolder
 * 
 * - Create a node <brand> inside <rebranding> tag in config.xml. (ex: see config.xml file)
 * ----- Change values
 * 
 * - To apply rebranding, copy paste content to -app.xml
 * - Compile
 
 *****************************/ 

package data
{
	import com.google.analytics.GATracker;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import org.osflash.signals.Signal;

	CONFIG::offline
	{
		import flash.desktop.NativeApplication;
		import flash.filesystem.File;
	}
	
	public class Infos
	{
		
		/**
		 * 91003:
		 * 
		 * Add card version
		 * 
		 * */
		
		/**
		 * 91004:
		 * 
		 * Corrected bug in photomanager with lot of photos and optimize scrollbar in scrollpane
		 * Album project configurator updated (new version with combo boxes)
		 * Album upgrades added (from type to type)
		 * Page Navigator dispose and update added
		 * Album cover saved if changed. When changing back, cover has kept the photo and cusotmisation
		 * Album option insert (papers between pages) added
		 * Left Menu retractable added
		 * Album upgrades add/removes pages, undo/redo added
		 * Overlayers system added
		 * 
		 * Friday 8 nov 2013
		 * Album Cover Add text button -> Change process ok
		 * Album cover mettre une limite pour le max. de caractères sur la cover ok
		 * Album cover design text panel update
		 * Album cover design Option panel update  
		 * Album cover stock aivailability bug resolved
		 * Album cover update price on chages ok
		 * TextFrame double textfield when click on CTA ok 		
		 * Background system update (dragdrop photos, ratio, always background)
		 * Album Cover fonts added
		 * LayoutTab shows layout of cover when its a cover ok (and layout of page when its a page)
		 * 
		 * Saturday 9 nov 2013
		 * Double click add photo on page
		 * Outside click to unstick transform tool
		 * Fonts refactoring -> External SWC
		 * Quality indicator on 2 levels (LOW and KO)
		 * Quality indicator tooltips
		 * Backgrounds: Color fill configurator added, apply to all added, undo enabled (yet, the redo does not work).
		 * Cover classic: Price update when changing corners
		 * TextTool Bug sticking toolbar (hacked) can be done better!
		 * Display layout text with lines (in layout tab)
		 * Color chooser bug onclick
		 * Kill overlayer bug with linkage corrected
		 * Stop frame mouse actions while editing
		 * Crop and resize system limits
		 * Bug in construct from save with backgrounsd (can receive photovo AND background vo)
		 * 
		 * Monday 11 nov 2013
		 * EditionArea: current edited page highlight
		 * EditionArea: Add Text button enable conditions (Off on cover for example)
		 * Error popup system
		 * Text focus double click
		 * Warning popup when leaving
		 * Combo boxes skin ok
		 * Check Boxes skin ok (may have to replace regular one by the Custom one where its needed)
		 * Edition Area: current edited hightlight alpha bug resolved
		 * Image edition module (rotation and mirror are now working)
		 * Infos button class added
		 * */
		
		/**
		 * 91005:
		 * 
		 * Friday 15 nov 2013
		 * BUG with background photo dispose fixed
		 * Background edition as image is now possible
		 * Added : filter on new/old projects
		 * Added : display error in dev log mail more easily
		 * Added : do not save button while quitting page
		 * 
		 * Monday 18 nov 2013
		 * LayoutVo Update: keeps old layoutVo PhotoVo and inject them in the new one
		 * Frame delete in two steps : Clear frame, then delete
		 * Frame Colors: Colors of frame by type added
		 * Frame Text: Edition on a selected text only added
		 * Frame Text: Toolbar text always shown when text frame
		 * 
		 * Tuesday 19 nov 2013
		 * Text Frame: enhanced toolbar, selected text modification allowed
		 * Text Frame: Font size by text added
		 * Fullscreen bug corrected for IE
		 * Embed fonts for topbar + tooltip + thumb item + navigator pages + page indexes
		 * Project folder open order saved
		 **/
		
		/**
		 * 91006:
		 * 
		 * Tuesday 19 nov 2013
		 * Don't allow drag/drop of cover in page navigator
		 * Cover classic: MaxChars updates based on book size
		 * Bug corrected in Frame Text when changing page
		 * 
		 **/
		
		/**
		 * 91007:
		 * 
		 * Tuesday 19 nov 2013
		 * Text preview in navigator
		 * Photo list is sorted on most recent
		 * Bug corrected when uploading photo (folder name was not sent correcltly)
		 * Font added, not all (arround 10)
		 * 
		 * Tuesday 20 nov 2013
		 * Photo list expand/collapse all feature
		 * Text preview in navigator in Bitmapdata for better perfs
		 * BUG corrected when selecting first index of textfield 
		 * 
		 * Wednesday 21 nov 2013
		 * Albums upgrades: Classic to Contemporary upgrades fixed (+projectVo refactroing)
		 * Fixed page highlight crop bug
		 * Page area shadow removed
		 * Custom Cover: Layout, Spine, edition -> ok
		 * Album Project Upgrades -> ok
		 * Background saved when applying Layout
		 * Add text on custom cover ok
		 * Move frame photo when empty ok
		 * 
		 * Fullscreen background change
		 * page navigator select as line
		 * Bug for cover bounds corrected
		 * Bug when clear frame > update navigator corrected
		 * Auto Fill Bug resolved
		 * 
		 **/
		
		/**
		 * 91008:
		 * 
		 * Friday 22 nov 2013
		 * 
		 * Spine width calculation corrected
		 * Fullscreen Preview: Cover classic ok
		 * Fullscreen Preview: Page after cover added 
		 * FullScreen Preview: Use fullscreen width, no more limitation
		 * Folder filter in photo manager
		 **/
		
		
		/**
		 * 91009:
		 * 
		 * Monday 25 nov 2013
		 * 
		 * Page name Label bug corrected (Front - page1 -page2 etc..)
		 * Fake page after cover and before back added
		 * Cover Classic Mouse click Bug resolved
		 * Save project: Update Name added
		 * Left Tab: background behing Tab button added
		 * Layouts: Apply layouts on page keeps images and background
		 * Fullscreen: close btn added
		 * Editor & Fullscreen: Gradient between pages added
		 * TextTool: Review and usability enhanced
		 * Tranform Tool: Delete icon for background stick to the frame for more visibility
		 * Print of overlayers
		 * Print of shadows and borders
		 * Print of backgrounds 
		 **/
		
		/**
		 * 91010:
		 * 
		 * Bug on textfield resolved
		 * Tooltip on transform tool added
		 * 
		 * 28 nov 2013
		 * 
		 * Photo upload filter change (no more possible to import png or gif files)
		 * ResourceExporter
		 * Resource in project checked and fixed
		 * Tooltip on Page navigation (drag information)
		 * Close Btn: Order panel, upload panel
		 * Page next and previous make navigator follow to display selected page
		 * PopArt panel design updated (we should create a panel that can be dragged... for now its just like the text toolbar)
		 * AutoFill icon changed (text instead)
		 * Price update in ordering panel
		 * Completed upload items state
		 * Upload items tooltip custom
		 * Cut border integration (for albums only)
		 * Page info in print integration (for albums only)
		 * 
		 **/
		
		/**
		 * 91011
		 * 
		 * 29 nov 2013
		 * 
		 * LeftTabAbstract: Bottom zone added
		 * BackgroundTab: Apply to all module added
		 * ColorChooser: Added palette instead of gradient
		 * Cover classic: Tooltip added on colors
		 * Cover classic: Color GOLD and Silver apply to PageNavigator
		 * FrameVo.isEmpty is now the reference to know if a frame is empty
		 * FameText: Refactored add update states.
		 * FrameText Selection when empty ok
		 * OverAll Frame allowance refactored (no more setter, just conditions in getter)
		 * Cover custom: spine and edition frame alowance modified
		 * Print manifest system created for albums and cover
		 * Order process updated with multiple step for covers 
		 * Phototab opens automatically on "view photo" button
		 * 
		 * **/
		
		/**
		 * 91012
		 * 
		 * 30 nov 2013
		 * 
		 * Edition area and Fullscreen: firstPage and LastPage button added
		 * NEW: PageNavigator expandable -> Solves problem of swaping pages and respond to a functionality request
		 * Album configurator: "pages" added after number 80 70 60 etc...
		 * AutoFill Bug resolved : Frame were not selectable after an auto-fill
		 * BUG 52 Solved
		 * BUG image edition when apply solved
		 * Add app url in dev log mail (to know if this is online testing or debug version)
		 * Wizard to app system
		 * Auto select correct values in album configurator
		 * CoverManager Added
		 * Checkout infos corrected
		 * Quality indicator visible in navigator
		 * Popup warning on bad quality while order
		 * popup warning on empty frams while order
		 * BUG with quality indicator on frame update
		 * page navigator update on frame zoom
		 * border in navigator
		 * add photo in frame system update
		 * 
		 * GROS BUG AVEC LE TEXT FRAME solved
		 * POPART : sometimes popart toolbar disapear. Solved
		 * NAVIGATOR : last page not visible (resolved because backCover added)
		 * NAVIGATOR : Back cover page added
		 * EDITION AREA : next page - prev page (centered and aligned to pageContainer)
		 * EDITION AREA: page bouttons overlap on 1280 -> Resolved
		 * PHOTO MANAGER:  we need to unflag all the other folders at once -> Done (+design refactored).
		 * BACKGROUND RATIO BUG SOLVED
		 * 
		 * ALL FONTS ADDED
		 * BUG textFrame selection solved
		 * BUG textframe apply modiciation when has not focus solved (applying a modification set the focus to textfield)
		 * */
		
		/**
		 * 91013
		 * 
		 * 2 dec 2013
		 * AUTOFILL ON FOLDER
		 * BUG COVER CHECK OK
		 * 
		 **/
		
		/**
		 * 91014
		 * 
		 * 2 dec 2013
		 * Better bottom right zoom system
		 * MOVE UNIQUE PAGE IN NAVIGATOR
		 * INCREASED IMAGE QUALITY IN FULLSCREEN
		 * INCREASED BUTTON SHADOW QUALITY
		 * COVER SPINE text security added
		 * COVER EDITION text security added
		 * BORDER TOOLBAR Added
		 * BORDER Apply to all added
		 * SHADOW merge with border toolbar
		 * IMPROVED SAVE SYSTEM
		 *
		 **/
		
		/**
		 * 91015
		 * 
		 * 2 dec 2013
		 * 
		 * LAYOUT TAB BUG :  COVER LAYOUT update when classic cover fixed
		 * BUG Text frame: Apply textformat when textfield.length ==0 fixed
		 * SPINE default font size max based on textfield height
		 * Upgrades (Sizes) for trendy and casual
		 * */
		
		/**
		 * 91016
		 * - Cover classic color text in page navigator
		 * - added flashvars for orderDebug
		 * - added order id on pdf pages
		 * 
		 * */
		
		/**
		 * 91017 CALENDAR
		 * - Order panel detail 
		 * - Topbar detail
		 * - Configurator: update design just like album
		 * - manifest integration for calendars
		 * - order process for calendar
		 * - border calculation correction
		 * 
		 * - Add ProjectCreationParamsVo for calendars values (need to be tested in wizard tho)
		 * - EDITOR: Remove page "fake shadow"
		 * - FULLSCREEN PREVIEW: change pages position top/bottom
		 * - FULLSCREEN PREVIEW: Show frame calendars
		 * - FULLSCREEN PREVIEW: Enhance space used to display page container (now using full height avialable)
		 * - EDITION AREA: all CALENDAR except XXL and XL: SHOW only one page at a time
		 * - MAGNET CALENDARS: Handle 1 page calendars exception (no navigator, no next/prev btn, no expand, no shadow...)
		 * - PAGE NAVIGATOR:  SWAP/MOVE rules to update for calendars
		 * - PAGE NAVIGATOR: no more group pages
		 * - CAL: LAYOUTS: MAGNET CALENDARS doesn't show up
		 * - CAL: CAL : LAYOUTS : It should not be possible to drag drop layouts on the calendar page ! Or at least this should be calendar layouts.
		 * - Handle hidden layouts (layout counts is now correct)
		 * - LayoutTab: Update content depending on current page type (is Calendar or not)
		 * - PageNavigator: PageLabel for calendar added
		 * - Edition Area page label for calendars added
		 * - PageNavigator: Scroll to the end (last item not entirely visible) -> solved
		 * - Calendar configurator: Add updgrades
		 * - Calendar Configurator: load existing project update configurator
		 * - Calendar EDITION AREA: Calendars colors - Complete refacotring
		 * - Calendar linkage frame system
		 * - Calendar outside text editor system
		 * - Calendar magnet layout modification
		 * - Resources updated Tictac_Resources_20131206_20h45
		 * - tooltip on title
		 * - bug fix in frame text selection
		 * - double click on frame text empty allow selection
		 * - BUG: Current edited page not updated when click on page in page navigator (XXL and XL) (that happen mostly when there is two page display) -> solved
		 * */
		
		/**
		 * 91018 (MERGED)
		 * 
		 * - BUG 73 : problème des borders qui disparaissent qd on passe d’un classic vers un contemp. 
		 * - BUG 55 : layout tab update en fonction des types de pages affichées.
		 * - BUG 51 : dans la fenêtre récap du checkout il n’est pas mentionné les coins quand ils sont choisis. Par contre, le prix des coins est tenu en compte
		 * - BUG 29 : cover classic, text color, re-checked, it is perfectly fine.
		 * - BUG 27 : ajouter les 90, 110, 110 et 120p pour les classics et contemporary + 70 et 80 pour les casual
		 * - BUG 34 : swap des images ok
		 * - BUG 69 : Backgrounds for covers
		 * - BUG COVER RATIO PRINT : OK
		 * - BUG 63 : Imgage for causal square added
		 * - BUG 88 : voucher with percent ok
		 * - Voucher in session xml
		 * - Undo/redo for photo swap
		 * - Check app status ok
		 * */
		
		/**
		 * 91019 (ALBUM)
		 * 
		 * - BUG 55: LayoutTabs updates
		 * - Cover layouts adjustements ok.
		 * - Cover
		 * */
		
		/**
		 * 91020 (ALBUM)
		 * 
		 * - Cover print manifest correction
		 * - Cover print background with correct bleed
		 * */
		
		/**
		 * 91020 (Calendar) (*)but some updates should be merged with ALBUMS
		 * 
		 * - BUG 75: mini calendar renderer (XXL & XL) check other products
		 * - BUg 74: for the XL calendar, there's only 1 page in the preview. there should be 2 pages just like the XXL calendars
		 * - Calendar colors panel closes after color choice
		 * - BUG 81: it's important to have the year next to the month. right now the year is not mentionned at all for the XXL and XL.
		 * - BUG 82: Show double year label when starting month is not january
		 * - BUG 83: when 1st month is not January, the bottom preview is wrong because it still shows January as the 1st month
		 * - BUG 84 : add tooltip when changing start day, start month and year
		 * - BUG 85: it's not that clear but we need to mention that we can choose the starting date, 'starting date'. is that possible?
		 * - (*)Dans la version beta il y a un problème avec vos categories pour les backgrounds. Je suis en train de faire les trads mais il y a des catégories qui portent le nom de backgrounds en fait.
		 * - BUG 77: the weeks on the left side of the calendar should be vertically centred. now they are centred on the top
		 * - Frame CTA: Show btn or not based on width : ok
		 * */
		
		/**
		 * 91021 (ALBUM)
		 * 
		 * - Cover bleed changed for CASUAL
		 * */
		
		/**
		 * 91022 (ALBUM)
		 * 
		 * - Cover bleed changed for TRENDY -> 4mm
		 * */
		
		/**
		 * 91021 (MERGED)
		 * 
		 * - Add warn popup when save with name already existing in project list.
		 * - Corrected bug with 2 pages more in album print
		 * - Add order ID in print page manifest
		 * - Optimize console system for perfs
		 * */
		
		/**
		 * 91023 (MERGED)
		 * 
		 * - Spine & edition in outside text frame
		 * 
		 * */
		
		/**
		 * 91024 (MERGED : ALBUMS & CALENDAR)
		 * 
		 * - Spine > select all > delete > crash --> resolved
		 * - Calendars Month and weekdays localization added
		 * - Albums upgrade refactoring ok 
		 * - Calendars upgrade refactoring ok 
		 * - Frame text bug with selection and move corrected
		 * - Frame text bug when deleting content corrected
		 * - Default frame text font size calculated instead of xml version
		 * - Cover print borde size problem corrected + add white page on print
		 * - Calendar print issues (wrong manifest size, wrong page numbers, some calendar frame not printed) corrected
		 
		 * */
		
		/**
		 * 91025 (MERGED : ALBUMS & CALENDAR)
		 * 
		 * - Cover classic textfield width
		 * - Casual 70 & 80 removed
		 * - Font size hack for frameText set to cover only
		 * - Trad added for vertical Align
		 * - Cal: First day of week default is monday
		 * - Cal: Wizard stzrting month and week indexes minus 1
		 * - Cal: Month color for magnet cal changeable
		 * - Cal: Text box bug when press enter ok
		 *
		 * */
		
		/** 91026 (MERGED : ALBUMS & CALENDAR)
		 * 
		 * - Cover upgrade -> spine width, photos and text updated.
		 *
		 * */
		
		/** 91027 (MERGED : ALBUMS & CALENDAR)
		 * 
		 * - Cover upgrade -> cover bounds updated
		 *
		 * */
		
		/** 91028 (MERGED : ALBUMS & CALENDAR)
		 * 
		 * - Top bar project name updated
		 * - BKG apply to all but not on cover 
		 *
		 * */
		
		/** 91029 (MERGED : ALBUMS & CALENDAR)
		 * 
		 * - New user no photo : error when uploading photos -> fixed
		 * - Error with default selected folder index --> fixed
		 * 
		 * */
		
		/** 91030 (MERGED : ALBUMS & CALENDAR) :: 11/01/2014
		 * 
		 * - Security added in action MOVE after first tester bugs
		 * - Added more log information on user action to better follow user flow
		 * 
		 * */
		
		/** 91031 (MERGED : ALBUMS & CALENDAR) :: 12/01/2014
		 * 
		 * - Double browse dialog causing crash fixed
		 * - Added more log information on user action to better follow user flow
		 * - Added a feedback form when a crash happen + send a email
		 * 
		 * */
		
		/** 91032 (dev : cards) :: 19/01/2014
		 * 
		 * - Print system for cards done
		 * - corret a bug in calendar
		 * - Order details for cards
		 * - Order url update for cards 
		 * 
		 * */
		
		/** 91032 (MERGED : ALBUMS & CALENDAR) :: 21/01/2014
		 * 
		 * - Manifest cover classic name fixed
		 * - Get price cover name fixed
		 * - Stack trace error added
		 * */
		
		
		
		/** 91033 (MERGED : ALBUMS & CALENDAR) :: 21/01/2014
		 * 
		 * - Manifest cover classic name fixed
		 * - Get price cover name fixed
		 * - Stack trace error added
		 * */
		
		/** 91033 (dev : CARDS + TODO) :: 20/01/2014
		 * 
		 * - Cards edition part ready for testing by client
		 * - Grid added
		 * - Add new photo (empty) added
		 * - Sebastian Font added
		 * 
		 * - allow resize/crop of empty frame photo
		 * - allow resise/crop of empty frame text
		 * - corrected bug with max frame text limit (wrong bitmapdata)
		 * - added lang parameter at logout to login with correct info
		 * - Print warning in order url should be fixed right now
		 * - Order xml with a precision of 6 digits max
		 * - new improved upload system !!!
		 * - two uploads at a time
		 * - add some code error securities
		 * 
		 * - Save/load custom layout start not finished yet (so stopped to complete some todos)
		 * - Photo manager btn made Yellow 
		 * - Inside photo manager btn yellow
		 * - Calendars birthday weekend day color same as weekday
		 * - LayoutTab thumb made bigger
		 * - Fixed bug with ordering when save popup dide hide dataloading manager
		 * 
		 * */
		
		/** 91034 (dev) :: 23/01/2014
		 * - fix max font size for cover at 30
		 * - Ressource popup error send btn XLS updated 
		 * */
		
		/** 91035 (dev) :: 24/01/2014
		 * - Corrected bug with bulkloader uniqid
		 * */
		
		/** 91036 (dev) :: 24/01/2014
		 * - 
		 * - New border print system and display (inner white border) + shadow system the same as print system (better coherence, what you see is what you get)
		 * 
		 * - Albums Classics: page 1 et last page des albums pas centré : Fixed.
		 * - Grid: Added 2 level of grid (big and small grid)
		 * - Grid: Hidden on fullscreen and reset back like it was before when exiting the fullscreen
		 * - Layout: Checker si l'ID du layout à déposer est identique à celui de la page. Si oui, do nothing
		 * - PhotoTab: PhotoItem Custom Tooltip : Text (Double click to add this photo) added to tooltip
		 * - Cover classic Page: Options chooser: Display chosen Cover under title
		 * - Frame contraste enhanced
		 * - TransformTool : icon "Inside move"'s size reduced (Page Navigator "move both pages"'s hand icon'size reduced too) 
		 * - Layouts: Categorization by number of photo added
		 * - BackgroundTab : Show nothing when current page is Cover classic
		 * 
		 * - security if user opens old project with new editor, at uncompress error, start new project and send a silent warning !
		 * - snap lines dones
		 * - inside rotation alternative (rotate and keep same frame bounds and rotation !)
		 * - Toolbar position system changed
		 * - reviewd crop system
		 * - zoom and inside move for frame calendars and overlayers
		 * - changed casual order id position in print pdf
		 * - display 30p instead of 31p for example in pdf (cover not into count)
		 * - removed "view photo" tooltip on photo upload complete when photo tab is open or photomanager is open
		 * */
		
		
		/** 91037 (dev) :: 29/01/2014
		 
		 * - Paper (inserts) stock check added
		 * - Ressources updates (avec titre layouts classifications by frames)
		 * - Remonter la frame edition sur cover de 5 mm
		 * - Text ennoir par défaut
		 * - Grid cols on cover idem que pages
		 
		 * - DocModifier correction for voucher check
		 * - Correction of the zoom overview in edition area (positionning issues)
		 * - Project id on cover casual position changed (from left to top)
		 * - added logs and securities in photomanager module
		 * - font size change with frame text zoom value
		 * - Font cover manifest name sent correctly ("" = Arial)
		 * - new session service called on new project (not only flash cookie)
		 * - open project session service called on open project (not only flash cookie)
		 * */
		
		/** 91038 (prod beta) :: 30/01/2014
		 * 
		 * - BUG brackground resolved
		 * */
		
		/** 91039 (prod beta) :: 30/01/2014
		 * 
		 * - BUG Cover layout resolved
		 * - Bug spine et edition resolved
		 * */
		
		/** 91040 (prod beta) :: 03/02/2014
		 * 
		 * - Save/load custom layouts
		 * - Bug cover layouts solved
		 * - Bug frame Spine and edition resolved
		 * - LayoutTab: Group layout with 6+ photos together
		 * - Tabs: Expand/Collpse accordion refoctored (added to all tab except project tab)
		 *
		 * - Canvas : numeric stepper max values update
		 * - Canvas : pageArea scrollrect system update (edge)
		 * - Canvas : Multiple canvas system behavior and display
		 * - BUG - Fixed error 1009 with spine text frame (last bugs received by mail)
		 * 
		 * - CARDS: Page with URL "www.tictacphoto.com" : frame that are part of the layout can't be deleted
		 * - CARDS: POST CARDS: Preview postcard background in bottom navigation
		 * - CARDS: Greetings cards : Show page Recto - both inside - verso
		 * - CARDS: Page with URL "www.tictacphoto.com" : can't be swapped or moved
		 * */
		
		/** 91041  :: 04/02/2014
		 * 
		 * - new frame text system (always outside)
		 * - Canvas Multiple layout fullscreen display
		 * - ALBUMS: Save/load layouts for cover added
		 * - CARDS: Frame with URL tictacphoto.com always on top
		 * - CARDS: Frame on page with url tictacphoto.com can be deleted now
		 * - CARDS: Default envelope bug fixed
		 * - CARDS: Gap between insides page reduce to 1 pixel to simulate page's bending area
		 * - ALL: TextFrame bug when "type your text here" stays after editing the size.
		 * - CARDS: POSTCARDS: URL Tictacphoto.com added
		 * - Calendars: WCAL Frame Weekday centered vertically
		 * - Calendars: checkbox transparent does not cloes the panel and allow user to apply it to all
		 */
		
		/** 91042  :: 05/02/2014
		 * 
		 * - BUG WITH FrameVoURL on non cards project.. ( != -1 )
		 */
		
		/** 91043  (CANVAS ONLY) :: 06/02/2014
		 * 
		 * - improved page layout display in navigator and frame border for better visibility
		 * - canvas save/load multiple format layout
		 * - canvas project detail infos
		 * - canvas manifest done
		 * - canvas kadapak frame color integration
		 */
		
		/** 91044  (MERGED) :: 13/02/2014
		 * 
		 * ALL
		 * - Layout tab: 0 and 1 photo, 2 photos, 3 photos etc..
		 * - Order photo Alphabetically
		 * - Bottom navigation: Text Vertical alignment update correctly now
		 * - swap now allowed with empty frames
		 * 
		 * CANVAS
		 * - layout update thumbs on each upgrade
		 * - layout check for canvas edge alpha (with colored background)
		 * - do not allow to delete background or photo frame not movable in canvas
		 * - do not allow to drag/drop image on background for canvas (if no isMultiple)
		 * - changed checkbox graphics for multiple canvas and calendar transparent background
		 * - maximum/minimum for multiple canvas margin (0-10)
		 * - detail for canvas on the bottom
		 * - close button for multiple canvas choice popup
		 * - reduce kadapak edge from 5mm to 1mm
		 * - Layout square 0 photos does not exist anymore.
		 * - Selection's Green dashed frame removed because not relevant in Canvas
		 * 
		 * ALBUMS
		 * - Classic cover: Text color saved and load correctly
		 * - Background Tab: Custom Cover background list not showing after update from classic cover. Solved
		 * 
		 * CALENDARS
		 * - Color panel behavior refactored (Apply button added)
		 */
		
		/** 91045  (MERGED) :: 17/02/2014
		 * 
		 * ALL
		 * - Swap allowed now even if there is only one photo available (swap with empty frame)
		 * - Reviewed quality indicator level to fit existing editor values. 
		 * - Taille des fonts de base maximum de 30 baissée à 15.
		 * - Font size on add text set to 25 (or different for albums size) idem for custom layout Bug
		 * - Auto fill icon changed
		 * 
		 * CANVAS : 
		 * - a minimum gap of 2pixels is set between multiple canvas to see separation when marin is set to 0
		 * - Corrected bug for multiple canvas that disabled big image on multiple canvas (layout issue)
		 
		 * CALENDARS :	
		 * - Bug calendars colors fixed
		 */
		
		/** 91046  (MERGED) :: 18/02/2014
		 * 
		 * ALL :
		 * - optimized text font size system
		 * 
		 * CANVAS : 
		 * - wizard integration bug
		 
		 * CARDS	
		 * - wizard integration bug
		 */
		
		/** 91047  (MERGED) :: 18/02/2014
		 * 
		 * CANVAS : 
		 * - url width and height values corrected (cm)
		 * - bug with limits of size corrected in project tab
		 * - limit for red indicator set to 75% of doc resolution (previously 85%)
		 * 
		 **/
		
		/** 91048  (MERGED) :: 25/02/2014
		 * 
		 * BUG FIX : image positionning in editor not working correctly when frame values were exactly the same as image value multiplied by the scale.  
		 * 
		 **/
		
		/** 91049  (MERGED) :: 03/03/2014
		 * 
		 * Removed beta logo  
		 * 
		 **/
		
		/** 91050  (MERGED) :: 04/03/2014
		 * 
		 * Problem with canvas max/min size
		 * 
		 **/
		
		/** 91051  (MERGED) :: 05/03/2014
		 * 
		 * Bug minicalendar font size 
		 * 
		 **/
		
		/** 91052  (MERGED) :: 08/03/2014
		 * 
		 * - Frame_border issue > remove the check that adds it to the exported frames so it's always exported
		 * - Cover cut border change > should fix cut marks and border position
		 * 
		 **/
		
		/** 91053  (MERGED) :: 08/03/2014
		 * 
		 - Photo ordered by name: re-tested here, and it works.
		 - Calendar start month and day bug, solved.
		 - Custom layouts rotation included
		 - Bottom preview: Text vertical alignment bug solved
		 
		 -Babyboom skinning: add flashvar: isBabyBoom:true
		 This is the best I could do for a temporary version.
		 * 
		 **/
		
		/** 91054  (MERGED) :: 08/03/2014
		 * 
		 * Sort photo Alphanumerically (img01, img02 etc..)
		 * ADD last version of resources (+ES)
		 * 
		 * */
		
		/** 91055  (MERGED) :: 17/03/2014
		 * 
		 * Replace hard codded image path for thumb, working and fullsize image by those in the xml.
		 * 
		 * */
		
		/** 91056 (SCOPE1 BUGS) :: 25/03/2014
		 * 
		 * Added timing in logs
		 * Log sessions in stat.php
		 * Handler server file upload errors
		 * Allow multiple service call to same handler at same time
		 * File check before upload for size more than 29mb
		 * 
		 * Error #1125: Range error when downgrading project solved.
		 * ImageEdition : Dimension limited to 3100 px max
		 * try/catch for file ref browse and upload (illegal argument error)
		 * Transform tool : 1009 on start action wrapped with try catch + clear frame + popup alert.
		 * New securities and popups on error with image edition
		 * Removed yellow artifact on some edited photos
		 * Added precision on zoom to allow better check for quality
		 * 
		 * CUSTOM Covers: Mettre en évidence la spine sur les cover
		 * CALENDARS Colors : Add close btn to panel
		 * 
		 * */
		
		
		/** 91057 (SCOPE1 BUGS) :: 31/03/2014
		 * 
		 * SECURITIES in transform tool (unstick when problem + clean action system)
		 * USER BACKGROUND BUG CORRECTED (new check at start with background coordinates)
		 * 
		 * */
		
		
		/** 91058 (SCOPE1 BUGS) :: 01/04/2014
		 * 
		 * Reset debug value for session cookie and add flag in debug to use it
		 * 
		 * */
		
		/** 91059 (SCOPE1 BUGS) :: 04/04/2014
		 * 
		 * - Bug on voucher check fixed (hardcoded value)
		 * - 
		 * 
		 * */
		
		/** 91060 (SCOPE1 BUGS) :: 08/04/2014
		 * 
		 * - Bug Text frame not printing due to ismultiline case not created for dayDate action
		 * 
		 * */
		
		/** 91061 (SCOP1 BUGS ) :: 18/04/2014
		 * 
		 * - updated hardcoded resources for dataloading ( mostly in serviceManager )
		 * - Intégration avec github !! et création de la phase 2 en github too
		 * 
		 *** 05/05/2014
		 * - cutmarks= false for cards (to remove line bug)
		 * - poparts translation update
		 * - added try catch in transform tool update image
		 * - removed tictac URL for cards
		 * - Bug fix with stat service (xml not formated makes app crash)
		 * 
		 * */
		
		/** 91062 (SCOPE1 BUGS) :: 19/05/2014
		 * 
		 * - TicTac URL removed
		 * 
		 * */
		
		/** 91063(SCOPE1 BUGS) :: 10/06/2014
		 * 
		 * - TicTac CutMarks issue solved for albums type casual (trimborder=0)
		 * - BUG: 29 days for Birthday calendars in february
		 * 
		 * */
		
		/** 91064(SCOPE1 BUGS) :: 13/06/2014
		 * 
		 * - Bug Calendars fixed
		 * 
		 * */
		
		
		/** 91064 (BackLoading) :: 19/05/2014
		 * 
		 * - corrected issue with text frame when textFrame was still "fixed" 
		 * - some fixes with textframe to be sure the text size in toolbar is right
		 * - new upload/preview folder system 
		 * - ????? how to correct the temp folder for items being uploaded while saving..?????
		 * - new save as system
		 * - added green color in page navigator when frame quality is ok
		 * 
		 * ** 21/05/2014 (Antho)
		 * - WORKING bords arrondis
		 * 
		 
		 * 
		 */
		
		/** 91065 (BackLoading) :: 28/05/2014
		 * 
		 * - Bords Arrondis + print implementation 
		 * - Border Colors + print implementation
		 * - .........CMYK -> RGB for background (check DEBUG.USE_OLD_BKG_FILL_SYSTEM) .............
		 * 
		 * ** 26/05/2014
		 * - Apply same text format to all.
		 * - Save and propose last used text format
		 * - Layout apply to all
		 * - Show/hide used photos
		 
		 * ** 03/06/2014
		 * - New Folder system more robust !!
		 * - Update of photos manager
		 * 
		 * ** 04/06/2014
		 * - skinning folder Manager
		 * 
		 */
		
		/** 9.10.66 :: 10/06/2014
		 *
		 * - BUG :Layout apply to all update used photo list. Fixed 
		 * - Photo Tab: Sorting photos by name or by date
		 *
		 * ** 11/06/2014
		 * - Warning popup while ordering if photos still uploading
		 * - New build version system
		 * - Modify icon border radius
		 * - Project name folder displayed when project is saved
		 * - Temporary fix do not allow shadow for circle masks..
		 */
		
		/** 9.10.67 :: 11/06/2014
		 *
		 * - Added upload button in photomanager 
		 * - Added some more logs in debug mail
		 * - Visual bug in photomanager folder separators when old project opening
		 * - Photomanager acticate/Desactive more clear for user with separated click zones (eye + button)
		 * - Save project does now rename project folder also
		 **/
		
		/** 9.10.68 :: 13/06/2014
		 *
		 * - Bug Calendars solved
		 
		 **/
		
		/** 9.10.69 :: 16/06/2014
		 *
		 * - Change logout button
		 * - Remap Photolist after UNDO/REDO
		 **/
		
		
		/** 9.10.70 :: 17/06/2014
		 *
		 * - When no photo on left zone, import button open photo manager instead of upload window
		 * - Auto open photo tab when project is ready
		 * - Change import photo button color to yellow ( secondary action) and add photo to green (primary action)
		 * - force opening photomanager on first folder (project folder)
		 * - Added some securities with fileRef
		 * - BUG : apply to all layout custom cover fixed
		 **/
		
		/** 9.10.71
		 *
		 * - Fixed width bug in multiple button alerts
		 * - Fixed bug with copy/paste frame from cover right area to another page.
		 * - Fixed bug with some frame text not displaying correctly some time (should fix cropped text issue in print also)
		 * 
		 **/
		
		/** 9.10.72
		 *
		 * - uniq ticked ID for logs relative to session
		 * - Display Silent Warning inside dev log mail
		 * - Changed url of tictac mail php file (prod)
		 * - Added Debug.FORCE_LANG for debugging different languages
		 * - Visual FIX for label and addphoto button in phototab 
		 **/
		
		/** 9.10.73
		 *
		 * - Photo not uploaded yet used for bkg make crash -> Fixed
		 **/
		
		/** 9.10.74
		 *
		 * - Calendars Frame stroke colors and other colors didn't show up at print -> fixed
		 **/
		
		/** 9.10.75
		 *
		 * - 1034 error bug on start fixed (type error on service pending) --> fixed
		 * - Cross platform mouse wheel delta --> fixed
		 * - Bug with text Frame edition box size on large albums --> fixed
		 **/
		
		/**********************************************************************
		 ////  PROD 9.11  
		 ***********************************************************************/
		
		/** 9.11.00 :: 10/06/2014
		 * 
		 * - Do not allow to close photo accordion item if only one
		 * - Frame text background + Print test
		 * - Frame text background apply to all
		 * - Vertical mirror
		 * - Move frame from one page to another
		 * - Move Frame outside becomes red if about to delete
		 * - shadow color and new toolbar
		 * - shadow with different mask 
		 * - Auto save warning after 20 mins
		 * - Tutorial engine done but infos needed.
		 **/
		
		/** 9.11.01 :: 17/06/2014
		 * - Custom backgrounds
		 * - Sepia option added 
		 * - New CMYK to RGB engine (pngs)
		 **/
		
		/** 9.11.02 :: 01/07/2014
		 * 
		 * - BUG: When editing a text, "type your text here" should never appear -> Fixed
		 * - Page Numbers Added
		 * - QR Code module added
		 * - Infos sur image added to edition area (toolbar)
		 * - Add label for cliparts and Frames
		 * - Rotation by step for frame text (45°, 90° etc..) added
		 * - Show Frame dimentions on editor and in CM - > Done
		 * - updated design for layouts in left tab
		 * - New album format implemented -> still need to check retro compatibility with older projects but should work with new ones
		 **/
		
		/** 9.11.03 :: 03/07/2014
		 * - new album formats retro compatibility -> Done
		 * - Close Calendard color toolbar on apply.
		 * - Custom layout fail when new user -> Fixed
		 * - Sorting photo re-cheched and enhance with AZ-> ZA-> Buttons
		 * - Page number - Can't delete or change its value
		 * - Page number - Center or not -> Added
		 * - QR code : double http:// possible mistake removed
		 * - QR code : Size limitation to 2.5 cm added
		 * - Text Frame Background Icon changed
		 * - BUG : Frame text in page navigator (background not displaying) > fixed 
		 * - BUG : TransformTool red frame stays red after delete > fixed
		 * - BUG : Shadow do not apply on bottom menu > fixed
		 * - BUG : Popart effect still there when photo change > fixed
		 * - Added labels in photo tab
		 */
		
		/** 9.11.04 :: 04/07/2014
		 * - Drag&Drop photos on page navigator frames >> done
		 * - Valign et Halign on text apply to all (HAlign only works if there is only one alignment on frame)
		 * - Tutorial in comment
		 */
		
		/** 9.11.05 :: 07/07/2014
		 * - Bug when ordering some background with popart effect (no photo check) --> fixed
		 * - Bug when background clear that kept image in bottom preview --> fixed
		 * - remove new album size system for prod release
		 * - Added "export log" system for dev on right click
		 * - Buttons in "FR" too big for apply to all "background" --> fixed
		 * - fullscreen mode bug fixed (sometimes user got fullscreen mode not allowed)
		 */
		
		/** 9.11.06 :: 07/07/2014
		 * - Bottom preview position issue when tab closed --> fixed
		 */
		
		/** 9.11.07 :: 09/07/2014
		 * - cover leather black bug (problem of variables from save) --> fixed
		 * - image rotation issue (new exif bytes system) --> fixed
		 * - removed warning when voucher value is 0
		 */
		
		/** 9.11.08 :: 09/07/2014
		 * - bug fix on navigator preview open/close
		 */
		
		/** 9.11.09 :: 09/07/2014
		 * - PFLMultiplier crash on new albums --> fixed
		 */
		
		/** 9.11.10 :: 10/07/2014
		 * - CoverFabric update on project load if coverLeather variable do not correspond (update of fix from 9.11.07)
		 * - CoverManager > getCoverlabelname security to avoid crash
		 * - script timeout do not crash app anymore.
		 */
		
		/** 9.11.11 :: 15/07/2014
		 * - Silent warning sent via GET and modification of mail subject (+ DB add warning + error) --> done
		 * - log time of session start ! --> done
		 * - new Framevo.toString system to get more info when error --> done
		 */
		
		/** 9.11.12 :: 16/07/2014
		 * - php log system stability and logs saved on DB
		 * - Custom layout error fix and securities
		 */	
		
		/** 9.11.13 :: 16/07/2014
		 * - cover multiple line bug on print --> done
		 * - BUG > FramePhoto.localImageLoadSuccess --> securities added
		 * - max mail log size to 200 000 chars
		 */
		
		/** 9.11.14 :: 23/07/2014
		 * - Rebranding system implemented --> done
		 */
		
		/** 9.11.15 :: 28/07/2014
		 * - only one error log by user session -> done
		 * - new resources in EXCELL -> done
		 * - Add security: BackgroundsTab > enableSaveCurrentLayout
		 * - MERGE with FlashBuilder 4.7 
		 * - make stats call async (avoid long connection time between antho.be and tictac)
		 * - new labels for empty layout list and empty background list in left tab
		 */
		
		/** 9.11.16 :: 07/08/2014
		 * - Bug CANVAS -> Not sending the modified docType for VCVerify and save_project
		 * - Added logs to track Error  Error #1034 at service::ServiceManager/onErrorHandler()
		 */
		
		/** 9.11.17 :: 8/08/2014
		 * - ServiceManager -Add a variable "cover" to the Voucher Code verify call
		 */
		
		/** 9.11.18 :: 14/08/2014
		 * - EXIF 270deegree bug added
		 */
		
		/** 9.11.19 :: 15/08/2014
		 * - BUG From compilation, customLayouts were sometimes crashing the app, sometime the tab did not reload... weird...
		 * - Clean the project, discard the actionScriptSettings conflict 
		 * - Re-comiled and it worked well.
		 * 
		 * -UPDATE: It was keith adding new layout that broke the system. Fixed now
		 */
		
		/** 9.11.20 :: 18/08/2014
		 * - Page Number Bug on addign more pages to project - Fixed
		 * - Page Number Bug when adding a layout - fixed
		 * - Added Clean And verify function to check page number at statup.
		 */
		
		/** 9.11.21 :: 19/08/2014
		 * - Bug inserting anull frame removed
		 */
		
		/** 9.11.22 :: 21/08/2014
		 * - Max char for QR code set to 150.
		 */
		
		/** 9.11.23 :: 31/08/2014
		 * - PageNumber - Fix for bug when moving pages.
		 */
		
		/** 9.11.24 :: 01/09/2014
		 * - BUG frame text rotation, removed frameExporter TEXT_FRAME_SECURITY_MARGIN as this seems to cause the bad placement of frame while printing. 
		 * 	This could make an old bug come back to life :) Text frame cut issue..
		 */
		
		/** 9.11.25 :: 02/09/2014
		 * - BUG fix for 1034 error in serviceManager (from Logs)
		 */
		
		/** 9.11.26 :: 10/09/2014
		 * - BUG fix Calendars  - Birthday calendars offset frame were printed when they should not - fixed
		 * 						- XL calendars - VAlign for frame with dateaction: [weekday] - fixed
		 */
		
		/** 9.11.27 :: 12/09/2014
		 * - Added logs for initial cotent load errors (from log mails error)
		 * - BUG fix Calendars  - Magnet calendar scale bug fixed
		 */
		
		/** 9.11.28 :: 23/09/2014
		 * - Added security and logs in SimpleImageGetter (oncompleteHandler) (from log mails error)
		 * - Added security and logs in UserACtionManager.addAction (from log mails error)
		 * - Added security and logs in CalendarColorToolbar.applyChanges (from log mails error)
		 * - BUG Fix : new folder creation not working when using app for first time.
		 * - Sorting system set back to normal alpha sorting
		 */
		
		/** 9.11.29 :: 25/09/2014
		 * - BUG fix Calendars  - Apply layout on PageVo.isCalendar should not be allowed -> fixed
		 * - BUG PageNumber - fix for page number on cover
		 */
		
		/** 9.11.30 :: 30/09/2014
		 * - BUG PageNumber - block transform action on those frames
		 */
		
		/** 9.11.31 :: 01/10/2014 (not on prod yet)
		 * - Bug 117 Calendars -> Date action year was not considered as a frame calendar : fixed
		 * - Bug Calendars -> Calendars colors vo not created at launch: fixed.
		 * - Bug 121 Albums Cover -> Stretch cover preview in pagenavigator
		 * - Save current layout was saving the pageNumber -> fixed
		 * - New layout for albums added
		 * - PageNavigator: space between cards preview
		 * - DateAction Url bug fixed
		 * - Paper pergamine checkbox behavior fixed
		 * - Grid color changed to flashy one
		 * - Apply to All confirmation popup
		 */
		
		/** 9.11.32 :: 06/10/2014 
		 * - Bug Underline on text quick fix (Hacked)... Result in a little bug with the "Underline" btn on the toolbar staying in Active mode sometimes... minor.
		 * - Add Warning and popup when server is down and can't load XML (clipart so far..)
		 */
		
		/** 9.11.33 :: 10/10/2014 
		 * - Security added for crash in fileRef (#2038)
		 * - added fix on layout apply to all for calendars
		 */
		
		/** 9.11.34 :: 20/10/2014 
		 * - Calendar frame now accepts text and photos (refactoring of linked frame system)
		 */
		
		/** 9.11.33 :: 10/10/2014 
		 * - Security added for crash in fileRef (#2038)
		 * - added fix on layout apply to all for calendars
		 */
		
		/** 9.11.34 :: 20/10/2014 
		 * - Calendar frame now accepts text and photos (refactoring of linked frame system)
		 */
		
		/** 9.11.35 :: 20/10/2014 
		 * - undo on frame text remove all when apply background color (done with new "apply" button)
		 * - apply button for color background, borders, shadow and masks toolbar
		 * - changed position and color for apply/apply to all buttons
		 */


		/**********************************************************************
		 ////  PROD 9.12  
		 ***********************************************************************/

		/** 9.12.00 :: 09/07/2014 
		 * - new album size integrated
		 * - tutorial visible again
		 */

		/**********************************************************************
		 ////  PROD 9.13  
		 ***********************************************************************/
		
		/** 9.13.00 :: 11/07/2014 
		 * - app initialization refactoring
		 * - new open project system
		 * - load/save local system
		 * - Desktop imageList system
		 * - local check session
		 * - local rename folder
		 * - tooltip local image preview
		 * - project rename system
		 * - project save as system
		 * - Full size image print working, need to only upload frame size
		 * - added new CONFIG:: system for compilation
		 * 
		 * :: 30/07/2014 
		 * - flash builder 4.7 and worker debugging system implemented
		 * - switch from flex desktop to actionscript desktop
		 * - move offline assets outside src/ to avoid long build time
		 * - added minimum size for application in app.xml
		 * - image encoding worker --> done
		 * - Image crop/encode/send for ordering --> done
		 
		 * :: 01/08/2014 
		 * - Canvas problem with images --> done
		 * - Added login form when ordering --> done
		 * - check on windows --> done
		 * - correct image import bug --> done
		 * - exit app on crash --> done
		 * 
		 * 
		 * :: 4/08/2014
		 * - New project process added
		 * - Project Wizard (basic) system added (new project)
		 * - Open last project used at start
		 * - Add icons
		 * 
		 * :: 14/08/2014
		 * - Make backgrounds work offline ok
		 * - WIP - Load more bkg from server process
		 * 
		 * :: 20/08/2014
		 * - All assets working offline (backgrounds, cliparts, overlays) 
		 * - All assets available for "download more" (backgrounds, cliparts, overlays)  (still on my server right now)
		 * - engine for generating content for offline use enhanced (works with all assets) (still need to run it for other classname)
		 * 
		 * :: 26/08/2014
		 * - App file Update process
		 * - Refactoring offline assets XML load
		 * - Allow update of any file into the offline_xml folder
		 * 
		 *:: 27/08/2014
		 * - GetPrice locally
		 *  
		 *:: 01/09/2014
		 * - Image edition module works now offline
		 * - Save local user id and email in flash cookie
		 * - Offline logs (only when connection, no cache for now) (mainly works in php files)
		 * 
		 * :: 05/09/2014
		 *  - Application update system 
		 *  - Wizard skinning ok
		 *  - Project configurator: - Auto-show tooltip when no project
		 * 							- Add label for usability
		 * 							- Change label btn (start or update project)
		 * 
		 * :: 10/09/2014
		 *  - Import images from frame btn
		 * 
		 * BUGS :
		 * - Image partrait issue everywhere
		 * - PhotoManager - Imag deletion does not work
		 * 
		 * CLEAN AND VERIFY
		 * - If a thumb is missing (.jpg file), add clean function to try to recreate it from the fullsize path 
		 *  

		 * TODO : 
		 * - [OK] make app working when no internet!!
		 * - [OK] Import images from frame btn (add images) does not work at all
		 * _ [OK] PhotoManager - Imag deletion does not work
		 * - [OK] Wizard, add icons and link to online fun product + tooltip(only online)
		 * - [OK] check new project process
		 * - [OK] Get price
		 * - [OK] Get price for all products (+ envelopes)
		 * - [OK] How do we get the price of project
		 * - [OK] Project wizard system
		 * - [OK] Project wizard system design
		 * - [OK] Open last project used at start
		 * - [OK] How do we change language / select languages
		 * - [OK] custom backgrounds and layout system
		 * - [OK] background, cliparts, frames system
		 * - [OK] Application update system (AIR)
		 * - [WIP] Application update files
		 * - image edition to review again... (savethumbs not good with new image storage system)
		 * - When adding an image, check if image doesn't already exists (to avoid dupplicates (uniq signature?)
		 * - Error log system (offline, store it locally and send when online)
		 * - [OK] create/delete folder system
		 * - [OK] Modify image edition system (no upload, (maybe no encoding) store it locally)
		 * 
		 * JONNY : add error callback for LocalStorageManager.uploadLayoutXML
		 */

		/** 9.13.03 :: 01/10/2014 :: OFFLINE
		 * 
		 * - Calendars bug: dateAction year was not considered as a frame calendar -> fixed
		 * - Calendars bug: Calendar color vo not created at launch -> fixed
		 * - Reload process bug: Resetmanager -> fixed
		 * - Dashboard design updated
		 * - Dashboard Full box is clickable
		 * - Logo opens Dashboard
		 * - Start App show loading straight away (Bug 122)
		 * - Loading design update
		 * - Show page number on cover (fixed)
		 * - Save Current layout was saving pageNumbers -> fixed
		 * - Added new layouts for albums
		 * - PageNavigator: space between cards preview added
		 * - ProjectCalendarConfigurator : Starting Day label bug fixed	
		 * - DateAction URL bug fixed
		 * - Paper pergamine checkbox behavior fixed
		 * - Save project name (double classname bug) fixed
		 * - Grid color changed to flashy one
		 * - Paper pergamine aded for project 101
		 * - Apply to All confirmation popup
		 * - Change language process and UI ok
		 * - When adding image, create a working and high resol image to save in user folder (do not use original path) (async system)
		 * - Check exif system (remove it from image when importing)
		 * 
		 */
		
		/** 9.13.04 :: 10/10/2014 :: OFFLINE
		 * 
		 * - Assets download more system up and running(bkg cliparts overlayers)
		 * - Calendar linked frame following dates ok
		 * - General Conditions integration
		 * - Lang splash page
		 * - Bug tooltip fixed
		 * - App leave system (warning when project not save or photos still uploading)
		 * - New encoding/bitmap system for performances
 		 * - handle errors when images are not available in list (image delete can be done via photo manager)
		 * - image edition module
		 * - added email input in error feedback popup for offline
		 * - remove project to open pref in offline when crash to avoid infinite error loop
		 * - application update build info and allow button click
		 */
		
		/** 9.13.05 ::  :: OFFLINE
		 * 
		 * - Wizard V2
		 * - Async system applied to Mandatory assest and download assests
		 * - Added check on version for updating app file
		 * - Bug on lang choice
		 * 
		 * */
		
		/** 9.13.06 ::  :: OFFLINE
		 * - undo on frame text remove all when apply background color (done with new "apply" button)
		 * - added timestamps for update urls
		 * - apply button for color background, borders, shadow and masks toolbar
		 * - changed position and color for apply/apply to all buttons
		 * 
		 * */
		
		/** 9.13.07 ::  :: OFFLINE
		 * - Memory leak problem fixed for workers
		 * - Added securities in langchoice positionate (crash logs)
		 * 
		 * */
		
		/** 9.13.08 ::  :: OFFLINE
		 * - BIG improvement on resources performances !! replacing xml search by dictionary
		 * - Uploading in photomanager folders bug fixed (photomanager closing)
		 * - removed "cookie clear" at App update 
		 * [TODO] still freeze when copypixel from bitmapdata to bytearray
		 * [TODO] Cover classic still load online
		 * - Wizard Feedback V2 : Hide stuff from configurator, change title fo dash color, add Peper pergamine image...
		 * - Bug Custom Bkg save (bridgeSuccess) fixed
		 * - Get Prices offline for Cards and Canvas incorrect values fixed
		 * [TODO] Still need to find a solution for canvas multiple/custom price algrorithm (waiting keith answer)
		 * [TODO] Calendar follow date 1) Bug from date from month before 2) Check with triple linkage
		 * 
		 * 
		 * */
		
		/** 9.13.09 ::  :: OFFLINE
		 * - Fixed bug for offline ordering producing "none" framedir
		 * - Fixed bug price contemporary
		 * - Wizard price wrapper color changed
		 */
		
		/** 9.13.10 ::  :: OFFLINE
		 * - Added project save file when ordering ( sent HTTPUploadPage )
		 * - Order manager getPrice removed project param for offline
		 * - Changed install folder to be TicTacPhoto2014
		 * - new resources added
		 * - Added temporary hack for Price update in dashboard (canvas multiple and custom)
		 */
		
		/** 9.13.11 ::  :: OFFLINE
		 * - Never open wizard at startup if user did already use wizard (isFirstUse property)
		 */
		
		/** 9.13.12 ::  :: OFFLINE
		 * - Vendor id set to 1 instead of 9999
		 * - Changed Application Logo
		 */
		
		/** 9.13.13 ::  :: OFFLINE
		 * - Price calculation for Canvas multiple and custom added
		 */
		
		/** 9.13.14 ::  :: OFFLINE
		 * - Price calculation for canvas bug fixed
		 * - Minimum size for canvas multiple and custom added
		 * - Project create/update label bug fixed 
		 */
		
		/** 9.13.15 ::  :: OFFLINE
		 * - Max send mail by session set to 4 
		 * - Avoid feedback popup loop by allowing only one error popup by session
		 * - use Fileref.size only in try/catch wrapper as it can lead to 2038 issues
		 * - Image editor module error 1009 -> security added
		 * - Better view on user folder in logs for offline
		 */
		
		/** 9.13.16 ::  :: OFFLINE
		 * - Layout cover bug (scaleX) fixed.
		 * - Update cover options xml
		 * - Update layouts for casuals
		 * - Bug pageNumber in injectProps
		 * - Calendar follow date 1) Bug from date from month before 2) Check with triple linkage
		 * - Multiple feedback from nico 
		 * - Bug utilisateur Range error in configurator Canvas
		 * - Add log to catch error from calendars colors...
		 */
		
		/** 9.13.17 ::  MERGED
		 * - Add re-branding ready for offline (changes affect online too)
		 * - For rebranding process check top of this class
		 */
		
		/** 9.13.18 ::  MERGED (14/11/12)
		 * - Bug fix -> cLeft positive value + desktop condition.
		 * - Add try/catch in PageNavigatorFramePhoto.renderframe
		 * - Term and conditions for rebranding
		 * - GA Analytics integration
		 * - Removed file size while ordering upload
		 * - Bug fix -> cLeft positive value + desktop condition.
		 * - security : local load image file.exists fix (2037 issue)
		 * - security : for range error in project canvas configurator> updateConfigurator (2006 issue)
		 * - security : cloneBitmapData 1009 : BackgroundApplyToAll/setPreview()
		 * - Bug fix Week of year in calendars with starting month different
		 * - security : termandconditions.destroy
		 * - security : appContentUpdate json.parse in try catch
		 */
		
		
		/** 9.13.19 ::  MERGED (13/11/12)
		 * - Rebranding ID for offline is now in app.xml descriptor file (avoid errors)
		 * - Bug fix with file.exist condition..
		 * - Week number: new calculation added
		 */
		
		/** 9.13.20 ::  MERGED (17/11/12)
		 * - Center native window ok
		 * - Can't scroll in GeneralCondition... Can't reproduce this bug.
		 * - Cards: postal card background condition added 
		 * - [SCOPE 2] Add more pages in pageNavigator OK.
		 */
		
		/** 9.13.21 ::  MERGED (19/11/12)
		 * - center window fix
		 * - added build info in screen (for better debugging when client send screenshots)
		 * - New Google analytic separation for online and offline
		 * - stat service only called once by offline session
		 * - new send feebdack system on right click
		 * - send user screenshot and project save on desktop errors
		 * - new update urls put to Tictac server : http://cdn-1.tictacphoto.com/offline2/updates/
		 * - new fix and visual for impage import fail (bug importing never complete)
		 * - new project size xml updated
		 * - NEW IMPORT IMAGE SYSTEM
		 * - Added warning on connection error/lost when clicking to order button
		 * - added warning when leaving while ordering !
		 */
		
		/** 9.13.22 ::  MERGED (24/11/12)
		 * - Added vendor id in the verify service system
		 * - added original date in offline by image metadatas
		 * - remove save warning tooltip on save cancel too
		 * - Added logs to track where the double SessionManager.loadResources call was coming from + add fix
		 * - Shadow apply to all was not enable when checkbox "use shadow" was not selected => Fixed
		 * - Big bug fix on all frames depth added to the page with a double click or a drag an drop
		 * - Bug fix cover price for casual and trendy
		 * - Copy mandatoryFiles logic updated
		 * - added securities to avoid native process crash
		 */ 
		
		/** 9.13.23 ::  MERGED (24/11/12)
		 * - new color system (print map) // disabled
		 * - Bug ordering when frame has widht or height <= 0 + clean and verify
		 * - Bug Range Error undo/redo
		 * - Log user OS
		 * - add origin url in image xml
		 * - security add for tooltip open on clip
		 * - new system (projectpatcher) to check corrupted images in project
		 * - warning when some file import problems
		 * - fixed bug for framedir set as resource instead of upload for backgrounds with user photos
		 */
		
		
		/** 9.13.24 ::  MERGED (02/12/14)
		 * - Fix for images not well uploaded while ordering + changed name of file location for better visibility
		 * - Fix for encoding image (error simulation)
		 * - Fix for error with popart ordering
		 * - Fix on reimport corrupted images 
		 */
		
		/** 9.13.25 ::  MERGED (02/12/14)
		 * - added more logs when user as corrupted images
		 * - fix on possible bug of uploading items while switching projects
		 * - added new files on user feedback to debug user directory listing
		 * - on project image list error, try to find a matching photovo by OriginUrl and then by name to fill the corrupted images.
		 * - activation of new album formats
		 * - activation of new print color systemµ
		 * 
		 * - Corrected multiple 1034 bug on drag&drop (mouseevent > event type issue)
		 * - added security in dashboard.hide (error from logs)
		 * - add security in undo action from history (possible range error)
		 * - add other securities and logs in bulk encoder
		 * 
		 **/
		
		/** 9.13.26 ::  MERGED (07/12/14)
		 * - New assets system added
		 * - Remove Path reference like "app-storage" and replace by File.applicationStorage, could fix copy mandatory file bug
		 * - Add "More page" btn to Albums in edition area
		 * - Fix potential bug in PageNavigator
		 * - Add popup "download installer" if copymandatory files failed
		 */
		
		/** 9.13.27 ::  MERGED (10/12/14)
		 * - XML bug in new assets system fixed
		 * - New colors
		 * - Add mor epage bug fix
		 * - Add Skip flag for downloading XML assets if already done for the given classpath
		 */
		
		
		/** 9.13.28 ::  MERGED (15/12/14)
		 * - XML bug in new assets system fixed (claendars/calendar) type of bug
		 * - 100% done, Memory leak detected when dashboard is shown to many times 
		 * - Fix save process showing dash
		 * - Upload error 203X - add Retry (x5) on upload image when order
		 */
		
		/** 9.13.29 ::  MERGED (16/12/14)
		 * - Background Tab update view bug fix
		 * - Overlayer XML load bug.
		 * - Manager auto-reset removed
		 * - New projectManager asset process async
		 */
		
		/** 9.13.30 ::  MERGED (17/12/14)
		 * - Frame exporter, add new security in case of no response from server after success (retry)..
		 * - add same condition in frame exporte and order manager for frame color upload
		 * - Add security in pageManager current page is null = return
		 */
		
		/** 9.13.31 ::  MERGED (13/01/15)
		 * - Code merge for offline and online [IN PROGRESS]
		 * 
		 * MERGED (15/01/15)
		 * - Code merde - bug assets for online fixed
		 * 
		 */
		
		/** 9.13.32 ::  MERGED (22/01/2015)
		 * - Bug Ordering (no photo sent) [FIXED] (wrong condition)
		 * - Do not show upload button anymore (upload panel)
		 * - Bug Mail service "IS DESKTOP" sent for online content [FIXED]
		 * - Bug photo tooltip over image [FIXED] (commented thumbItem.loadThumb : tooltip = null)
		 
		 
		 ** 9.13.33 ::  MERGED (28/01/2015)
		 * - Bug Tooltip lang change fixed
		 * - Bug Data loading jumping arround (Change in assets)
		 * - Bug language not saved 
		 * - Bug Calendar linked photo did not follow date fixed
		 * - New colors added (21/01)
		 * 
		 ** 9.13.34 ::  MERGED (03/02/2015)
		 * - REMOVED STOCK CHECK IN PAGECOVERCLASSICAREA !! TO CHECK!!
		 * 
		 */
 
		/** 9.14.01 ::  OFFLINE (new photo system) (02/02/2015)
		 * - removed photo manager
		 * - new folder like projects system
		 * 
		 *** 9.14.02 
		 * - improved load of bitmap using loader context
		 * - new scrollbar design (yellow)
		 * - added scrollbar in page navigator expanded
		 * - review of save popup [IN PROGRESS]
		 * 
		 *** 9.14.03
		 * - new save by reference system
		 * - new save warning popup
		 * - auto save timer issue
		 * - improved thumbnail size and quality for offline
		 
		 
		 *
		 * ** 9.15.01 ::  MERGED (24/02/2015)
		 * - New options added (Coating, paper quality, flyleaf color)
		 * - Coating involve retro-compatibility with Coating set by default (based on XLS reveived by nico)
		 * - Same for paper quality (250 by default)
		 * - WIP: Add offline calculation for new options (waiting keith to add them in API)
		 * - Price files auto updated from API (offline)
		 * - Add new type for Canvas (Real aluminium)
		 * - WIP: Price calculation won't work for this product as long as the price API is not up to date
		 * - BUGFIX: Calendar Lux weekend day color bug fixed
		 * - Spine width modifications added
		 
		  
		 *  * ** 9.15.03 ::  MERGED (06/03/2015)
		 * - All Popups design reviewed
		 * - Contact Panel Finished and plugged to Suport process (tested with Keith)
		 * - Nico feedback from : 25/02/2015 
		 * - Album project detail in project list popups (open / save)
		 * - added security and character restriction for project names
		 * - new checkbox in save allow to change from copy to reference system on the first project save
		 * - Maximize window at startup
		 * - Double click pour ajouter une photo
		 * - Bug new project -> change lang -> temp issue
		 * - Options price calculation added
		 * 
		 *  * ** 9.15.04 ::  MERGED (10/03/2015)
		 * - Communication Panel (News) Added -> Waitin Keith to update URL and put file on server
		 * - Flyleaf: Add color label + price (online and offline) + update price when change color
		 * - Bonus: Paper (inserts) add price on configurator and dashboard (online also)
		 * - Bkg colors palette (removed black colors line)
		 * - Order Panel: Comtemporary label instead of classic all the time
		 * - Calendar coating rules updated ( !! Send new menu.xml to Keith for online calendar and Albums)
		 * - Calendar Lux weekend colors when year offset fixed
		 * - OrderPanel: Add flyleaf color instead of Yes/no
		 * - BKG Apply to all but not cover OK
		 * - Add manifest frame extra info for albums
		 * 
		 *  * ** 9.15.05 ::  MERGED (10/03/2015)
		 * 	- skip to 9.15.06
		 * 
		 * 
		 * ** 9.15.06 :: 11/03/2015
		 * - News: Add flag for offline and online (showing different newsgroup)
		 * - News: Bug not showing texts  Fixed
		 * - OrderPanel: Text on cover (only if contemporary)
		 * - Add manifest frame extra info  for all products
		 * - Add new visual for albums
		 * - RETRO COMPATIBILITY SYSTEM
		 * 
		 * 
		 * ** 9.15.07 :: 13/03/2015
		 * 
		 * - BUG fixed : offline save with no images in copy mode did make app crash
		 * - added new keyboard manager (delete button and ctrl-z for undo and ctrl-s for save)
		 * - added "file path" in image preview tooltip
		 * - BUG fixed : retro compatibility, the first time the patch was applied, if we did open a project on the same session, app would crash.
		 * - Added warning icon in patched projects (in image tap) to give information to user about patched project
		 * - Add new button on photo tab for patched project that allows user to open old image folder with previously imported images
		 * 
		 * ** 9.15.08 :: 15/03/2015
		 * 
		 * - BUG fixed : frame zoom issue in prints for overlayers with popart effect
		 * - App now always open on dashboard view, there is a new "open project" button allowing to open existing project
		 * - BUG fixed : color apply to all for background crash
		 * - added check for "border apply to all" issue on big frames (fixed and editable case)
		 * - BUG fixed : modify project > quit app > cancel save > start order > save --> app did quit, which was not ok
		 * 
		 * ** 9.15.09 :: 16/03/2015
		 * 
		 * - Changed frame "mouse over" effect for more consistency
		 * - Frame fullscreen do not allow border and move ('showborder' and 'move' property in layout)
		 * - Bug fixed : Calendar frame overlay text with color background 
		 * - Bug fixed : App did not close on when crash error

		 * 
		 * 
		 * ** 9.15.10 :: 17/03/2015
		 * 
		 * - Bug OrderPanel, text on cover fixed
		 * - Bug ApplyToAll colored Bkg fixed
		 * - Ressources Bug in FR -> Encoding issue? Fixed it by copying the XLS cell content, delete it, paste it again.
		 * - Fly leaves presentation adapted
		 * - Save Project popup: Check box label set to blue and Bold for better visibility
		 * - Tutorial step selection + first copy proposal for Nico.
		 * 
		 * ** 9.15.11 :: 18/03/2015
		 
		 * - Bug Cliparts missing fixed
		 * - Bug Aluminium real crash, fixed
		 * - Top bar project details: Remove PageNumber if not Albums
		 * - Error popup not closing and reloading bug fixed
		 * - Top bar project detail too long: Added "..."
		 * - Dashboard Open project btn always visible
		 
		 * 
		 * ** 9.15.12 :: 20/03/2015
		 * 
		 * - Add canvas skip conditions for tutorial step
		 * - Add Auto-launch for tutorial
		 * - Add edition Area Step to tutorial (update resources)
		 * - Add edition toolbar Step to tutorial (update resources)
		 * - Remove step Page navigatio from tutorial
		 * - Add url for youtube in tutorial
		 * - Remove Url for youtube for language not ready 
		 * - Add btn in settings for tutorials
		 * - Init Session param from checkSess added
		 * 
		 * * ** 9.15.13 :: 23/03/2015
		 * 
		 * - Bug Cal Lux always 31 days, fixed
		 * - Tutorial Btn removed for none availalble languages
		 * - Prices issue corrected (Diff between dash and in App)
		 * - Tutorial : Canvas exceptions bug fixed
		 * - Canvas add upgrade to Aluminium (same rules as Dibond)
		 * - Album default size : Set Medium
		 * 
		 * 
		 * ** 9.15.14 :: 25/03/2015
		 * 
		 * - new recovery system that save project "recovery" when crash
		 * - added new "forced" parameter in update.xml to force updates if needed (<forced>false</forced>)
		 * - Bug fixed : calendar layout crash for old project (retro)
		 * - Bug fixed : undo > apply to all background for calendar > crash 
		 * - -compiler.verbose-stacktraces=true in compiler option for stack trace (IF NOT WORKING, put swf version to at least 18 (=flashplayer 11.5) which includes stack trace)
		 * - Folders in photoManager (online) can now be selected even if they are not added to project
		 * - Added new calendar text background option
		 * - project detail in popup (offline only, and for new projects only (quick fix))
		 * 
		 * * 9.15.15 :: 26/03/2015
		 * 
		 * - Config location changed : now in "/src"
		 * - Resources location changed : now in "/assets"
		 * - Offline: Config auto update system from online files added
		 * - Offline: Resources auto update system from online files added
		 * - Bug when prices are not up to date (File were overwritten by local ones) fixed
		 * - Language management externalized in config.xml
		 * - Tutorial Link anf FAQ Link externalized in confgi.xml (language depedency)
		 * - Contact panel topics management externalized in config.xml (topic added must be added in backend support flow see with keith)
		 * - News back button y position security added
		 * - Manifest {extrInfos} y pos corrected
		 * - Calendar paper quality in order panel removed
		 * - Background Custom save btn enabled conditions fixed
		 * 
		 * 
		 * * * 9.15.16 :: 01/04/2015
		 * 
		 * - Bug online when coming from WIZARD: WIP
		 * -> Thought: Coming from lang param used in lowercase...
		 * -> Added fix in getter
		 * -> Added logs
		 * - STACK INFOS : OK (swf version set to 18) Check why we don't have STACK info on crash anymore ? EDIT : -compiler.verbose-stacktraces=true in compiler option for stack trace (IF NOT WORKING, put swf version to at least 18 (=flashplayer 11.5) which includes stack trace)
		 * 
		 * * * 9.15.17 :: 02/04/2015
		 * 
		 * - Bug fixed : online ordering with background and user photo
		 * - Bug fixed : undo redo for image background did not reset "used" photos
		 * - Added security for ressource updates
		 * - Bug fixed : on tutorial step index not showing when text is too long
		 * - Bug fixed : resources for tutorial (key was wrong)
		 * - Bug fixed : offline price difference
		 * - Bug fixed : old project bkg/clipart issue
		 * - Bug fixed : Quality paper casual
		 * - Manifest : vertical algnment fixed for classic albums 
		 * 
		 * * * * 9.15.18 :: 03/04/2015
		 * 
		 * - Bug fixed : no flyleaf options for Casual
		 */
	
			
		/***** [ TODO ] ****
		 * 
		 * --- JONNY ---
		 * [TODO][jonny] Layout apply to all not working for new wall calendar project
		 * [TODO][jonny] image size information (TransformToolInfoBox) blocks mouse --> surtout pour les petites frames, Jonny on peut pas montrer ca autrement?
		 * [TODO][jonny] Cover classic still load online 
		 * [TODO][jonny] Layout scaling for albums may not work with covers (to check when not using old layout system) --> TO BE CONFIRMED
		 * 
		 * --- ANTHO ---
		 * [TODO][antho] check possible drag/drop photo on background issue (photovo is still in temp mode at some point, Nico's bug) EDIT : wasn't able to reproduce :( 
		 * [TODO][antho] image refresh when sort by date should not relead images, just sort...
		 * [TODO][antho] BUG.. when used/unused check is clicked, each drag drop refresh Full image list !
		 * [TODO][antho] Color system, do it for text string too
		 * [TODO][antho] Offline performance next/previous next/previous page load to improve
		 * [TODO][antho] improve ordering experience !! better messages, loading bar?, restart where it stopped in case of connection lost?
		 * 
		 * --- JONNY OR ANTHO ---
		 * [TODO] Modify "mask" icon in toolbar...
		 * 
		  
		*/
		
		
		/**
		 * ------------------------------------ BUILD VERSION (offline version is managed in app.xml) -------------------------------------
		 */
		
		CONFIG::online
		{
			public static const buildString:String = "9.15.17";	// DO NOT FORGET TO CHANGE THIS also in "app_desktop.xml"
			public static const IS_DESKTOP : Boolean = false; 
		}
		CONFIG::offline
		{
			private static var _buildString:String;
			public static function get buildString():String
			{
				if(!_buildString){
					var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
					var ns:Namespace = appXML.namespace();
					_buildString = "" + appXML.ns::versionNumber;
				}
				return _buildString;
			}	
			public static const IS_DESKTOP : Boolean = true;
		}
		
		private static var _buildVersion:String;
		public static function get buildVersion():String
		{
			if(_buildVersion) return _buildVersion;
			
			if(!Infos.IS_DESKTOP)
				_buildVersion = buildString.split(".").join("");
			else
				_buildVersion = String(int(parseInt(buildString.split(".").join(""))+10000)); //Make a diference for backend => Offline
			return _buildVersion;
		}
		
		
		/**
		 * ------------------------------------ REBRANDING (offline version is managed in app.xml) -------------------------------------
		 */
		
		CONFIG::online
		{
			//Flashvars for online
			//USed for many things, updateDescriptor URL, ordering etc... must be "" for tictac
			public static var REBRANDING_ID: String = ""; //ex "babyboomphoto", babyboomfoto; 
		}
		CONFIG::offline
		{
			private static var _REBRANDING_ID:String;
			public static function get REBRANDING_ID():String
			{
				if(!_REBRANDING_ID){
					var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
					var ns:Namespace = appXML.namespace();
					_REBRANDING_ID = "" + appXML.ns::name; 
					_REBRANDING_ID = _REBRANDING_ID.toLowerCase().split(" ").join(""); // the rebranding id is the file name without any uper case nor spaces
					
					if(_REBRANDING_ID == "tictacphoto") _REBRANDING_ID = "";
				}
				return _REBRANDING_ID;
			}
		}
		
		
	
		/**
		 * ------------------------------------ GLOBAL PROPS -------------------------------------
		 */
		
		// Desktop
		public static var INTERNET : Boolean = false;
		public static var INTERNET_CHANGED : Signal = new Signal();
		

		// Stage
		public static var root:Sprite; // root view
		public static var stage : Stage; // stage ref
		
		CONFIG::offline
		{
			public static var framerate:uint = 30;
		}
		CONFIG::online
		{
			public static var framerate:uint = 30;
		}
		
		
		// datas 
		public static var classname : String;
		public static var configURL : String = "config.xml";
		public static var siteURL:String;
		
		// config
		public static const config : ConfigVo = new ConfigVo();
		
		// user preferences
		public static const prefs : PreferencesVo = new PreferencesVo();
		
		// Offline only, This indicates that this is the first time the app is used on the computer (based on cookie, so if cookies are cleared, isFirstUse will be true) 
		public static var isFirstUse : Boolean = false;
		// offline path to folder, this is updated each time a project is opened, it is used to compose offline image path
		public static var currentProjectPath : String; 
		
		// last user browse directory path
		public static var lastUserBrowsePath : String;
		
		
		
		
		/**
		 * ------------------------------------ GLOBAL GETTER -------------------------------------
		 */
		
		public static function get session():SessionVo
		{
			return SessionManager.instance.session;
		}

		public static function get lang():String
		{
			return SessionManager.instance.session.lang;
		}

		public static function get project():ProjectVo
		{
			return ProjectManager.instance.project;
		}
		
		public static function get selectedAlbumVo():AlbumVo
		{
			return ProjectManager.instance.selectedAlbumVo;
		}
		
		public static function get currentLangVo():LangVo
		{
			for (var i:int = 0; i < config.languages.length; i++) 
			{
				if(config.languages[i].id == lang)
					return config.languages[i];
			}
			
			return null;
		}
		
		public static function getLangVoById(id:String):LangVo
		{
			for (var i:int = 0; i < config.languages.length; i++) 
			{
				if(config.languages[i].id == id)
					return config.languages[i];
			}
			
			return null;
		}
		
		/**
		 * ------------------------------------ TYPE CHECK SHORTCUTS -------------------------------------
		 */
		
		public static function get isAlbum():Boolean
		{
			if(project && project.classname == ProductsCatalogue.CLASS_ALBUM) return true;
			return false;
		}
		public static function get isCalendar():Boolean
		{
			if(project && project.classname == ProductsCatalogue.CLASS_CALENDARS) return true;
			return false;
		}
		public static function get isCanvas():Boolean
		{
			if(project && project.classname == ProductsCatalogue.CLASS_CANVAS) return true;
			return false;
		}
		public static function get isCards():Boolean
		{
			if(project && project.classname == ProductsCatalogue.CLASS_CARDS) return true;
			return false;
		}
		
		
		/**
		 * ------------------------------------ OFFLINE PATHS -------------------------------------
		 */
		
		CONFIG::offline
			{
				private static function get storagePath():String
				{
					return File.applicationStorageDirectory.url; //return "app-storage:/";
				}
				
				public static function get offlineAssetsPath():String
				{
					return storagePath + Infos.config.getValue("offlineAssetPath");
				}
				
				private static function get offlineStatusPath():String
				{
					return storagePath + "status/";
				}
				
				public static function get offlineStatusAssetsPath():String
				{
					return offlineStatusPath + "assets/";
				}
				
				public static function get offlineAssetsCommonPath():String
				{
					return storagePath + Infos.config.getValue("offlineAssetPath") + "common";
				}
				
				public static function get offlineAppFilePath():String
				{
					return storagePath + Infos.config.getValue("offlineAppFilePath");
				}
				
				public static function get offlinePricingFilePath():String
				{
					return storagePath + Infos.config.getValue("offlineAppFilePath") + Infos.config.getValue("offlinePricingPath");
				}
				
				public static function get offlineUserPath():String
				{
					return storagePath + "user/";
				}
				
				public static function get offlineProjectsPath():String
				{
					return offlineUserPath + "projects/";
				}
				
				public static function get offlineCustomPath():String
				{
					return offlineUserPath + Infos.config.getValue("offlineCustomPath");
				}
				
				public static function get offlineCustomBackgroundsPath():String
				{
					return offlineCustomPath + "backgrounds/";
				}
				
				public static function get offlineCustomBackgroundsXML():String
				{
					return offlineCustomBackgroundsPath + "customBackgrounds.xml";
				}
				
				public static function get offlineCustomLayoutsXML():String
				{
					return offlineCustomPath + "customLayouts.xml";
				}
				
			}
		
		
		
		
		/**
		 * ------------------------------------ BLOC APPLICATION MOUSE (user interactions) -------------------------------------
		 */
		
		public static function blockMouse(flag:Boolean = true):void 
		{
			if(root) 
				root.mouseEnabled = root.mouseChildren = !flag;
		}
	}
}
