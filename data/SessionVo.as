package data
{
	import be.antho.data.SharedObjectManager;
	
	import manager.ProjectManager;
	
	import utils.Debug;

	/**
	 * 
	 * Session vo contains all the information about the current session of user
	 * it's the object correspondance to the xml result of the chksess.php service
	 * 
	 */
	public class SessionVo
	{
		// from flash var
		public var classname : String;	// project class (albums, calendar, etc..) // set by flash var
		
		// from session xml
		public var id : String; 		// id of the current session
		public var userId : String; 	// id of user, I think 0 if no user connected
		public var vendorId : String; 		// vendor id .. don't know yet 
		public var vendorUrl : String;  
		public var serverName : String; 
		public var product_id : String;
		public var product_style : String; 	// style of product (simple, pelemele,popart, etc.. and lot of other I suppose...)
		public var campaign_id : String;	
		
		public var email:String;
		public var return_page:String;
		public var recentProjectId:String;
		public var recentProjectName:String;
		public var recentProjectUrl:String;
		public var recentProjectClass:String;
		public var recentProjectAutosave:String;
		
		// voucher
		public var voucher:String;
		public var voucher_value:String;
		public var voucher_type:String;
		
		// album specific
		public var cover_name:String ;
		public var cover_corners:String ;
		public var product_inserts:String ;
		public var product_paperQuality:String ;
		public var product_flyleaf:String ;
		public var product_coated:String ; // also available for calendars
		
		// canvas 
		public var width:String ; // in cm
		public var height:String ; // in cm
		public var rows:String ; 
		public var cols:String ;
		public var gap:String ; // in cm
		public var frame_color : String;
		
		// calendar 
		public var year:String ; // (2014)
		public var month:String ; // (1)
		public var weekstart:String ; //(2)
		
		// calendar 
		public var envelope:String ; // <envelope>yellow</envelope>
		
		//News
		public var newsToShow:Boolean; //if true, NewsManager.instance is called (when TBD) WIP.
		
		
		// private
		private var sessionXml : XML;
		private var _lang : String = "EN"; 		// lang of current editor session
		
		
		// online accounts for testing
		// login : anthodb@gmail.com	=> pass : coucou
		// login : antho@test.com		=> pass : coucou
	
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function SessionVo():void
		{
			
			/*
			EXAMPLE OF SESSION XML
			
			<session>
			<id>s195fk6gqtcrtg99s0lvocog23</id>
			<user_id>257171</user_id>
			<vendor>1</vendor>
			<lang>en</lang>
			<vendor_url>www.tictacphoto.com</vendor_url>
			<return_page>albums</return_page>
			<cover_name>Leather Black</cover_name>
			<cover_corners>none</cover_corners>
			<product_inserts>0</product_inserts>
			<product_id>305</product_id>
			<product_style>simple</product_style>
			
			<vendor_url>www.tictacphoto.com</vendor_url>
			<recent_project_id>150620</recent_project_id>
			<recent_project_name>This is  a test</recent_project_name>
			<recent_project_url>/flex/project.php?id=150620</recent_project_url>
			<recent_project_class>albums</recent_project_class>
			<recent_project_autosave>0</recent_project_autosave>
			
			<email>jonathan@nguyen.eu</email>
			<preferences>popups:0|mainmenu:1|lastphotoalbum:0</preferences>
			<server_name>editor.tictacphoto.com</server_name>
			</session> 
			*/
			
			
			// ...
			/* TODO check if we do an automatic fill
			
			for ( var i:Number = 0; i<sessionXml.session.children().length(); i++)
			{
				this[sessionXml.session.children()[i].name().localName] = sessionXml.children()[i].text();				
				this[sessionXml.session.children()[i].name().localName] = sessionXml.children()[i].text();				
			}
			*/
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		public function get lang():String
		{
			if(Debug.FORCE_LANG) return Debug.FORCE_LANG;
			return _lang.toUpperCase();
		}

		public function set lang(value:String):void
		{
			_lang = value;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC
		////////////////////////////////////////////////////////////////

		public function fill($sessionXml : XML):void
		{
			sessionXml = $sessionXml;
			
			
			///////////////////////
			// DEBUG XML 
			// allow to test session parameters
			///////////////////////
			var debugSession:XML;
			debugSession = new XML(
				<root>
				<session>
					<id>j74an7vvsmdmbenrr6sglg6fa3</id>
					<user_id>265044</user_id>
					<lang>nl</lang>
					<vendor>1</vendor>
					<vendor_url>www.tictacphoto.com</vendor_url>
					<return_page>albums</return_page>
					<cover_name>Custom Cover</cover_name>
					<cover_corners>none</cover_corners>
					<product_inserts>0</product_inserts>
					<product_id>201</product_id>
					<product_style>simple</product_style>
					<email>sabrina.ruzzini@rettigicc.com</email>
					<preferences></preferences>
					<server_name>editor.tictacphoto.com</server_name>
				</session>
				</root>

				);
			///////////////////////
			// end DEBUG XML 
			///////////////////////

		
			if( Debug.USE_DEBUG_SESSION_XML ) sessionXml = debugSession;
			
			
			// common
			id = checkNodeValue("id");
			lang = checkNodeValue("lang", "EN")
			userId = checkNodeValue("user_id");
			serverName = checkNodeValue("server_name");
			return_page = checkNodeValue("return_page");
			product_id = checkNodeValue("product_id", "");
			product_style = checkNodeValue("product_style");
			email = checkNodeValue("email", "");
			campaign_id = checkNodeValue("campaign_id");
			vendorId = checkNodeValue("VendorCode");
			
			// auto promocode (voucher)
			voucher = checkNodeValue("voucher");
			voucher_value = checkNodeValue("voucher_value");
			voucher_type = checkNodeValue("voucher_type");
			
			
			// security to warn if project type (flashvar) correspond to cookie info (checkSess)
			/*
			if(return_page && return_page.indexOf(classname)==-1 && return_page != "account")
			{
				Debug.warn("Project return page do not correspond to project className, App class is '------" + classname + "------' but return_page is : '------"+ return_page +"------'");
			}
			*/
			
			// recent project info (to reopen)
			recentProjectId = checkNodeValue("recent_project_id");
			recentProjectName = checkNodeValue("recent_project_name");
			recentProjectUrl = checkNodeValue("recent_project_url");
			recentProjectClass = checkNodeValue("recent_project_class");
			recentProjectAutosave = checkNodeValue("recent_project_autosave");
			
			// security to check if project type (flashvar) correspond to cookie info (checkSess)
			if(recentProjectClass && recentProjectClass!= classname)
			{
				Debug.warn("Project class mismatch, App class is '------" + classname + "------' but session is looking for a RECENT PROJECT with class : '------"+ recentProjectClass+"------Pr'");
				if(!Infos.prefs.projectToOpen) {
					ProjectManager.instance.forceNewProject = true;
					Debug.warn("Force New Project = "+ProjectManager.instance.forceNewProject+ " & projectToOpen = "+Infos.prefs.projectToOpen);
				}
				
				return;
			}
			
			
			//
			// ALBUM SPECIFIC
			if(classname == ProductsCatalogue.CLASS_ALBUM)
			{
				product_id = (product_id == "")?"":product_id;
				cover_name = checkNodeValue("cover_name");
				cover_corners = checkNodeValue("cover_corners");
				product_inserts = checkNodeValue("product_inserts");
				product_paperQuality = checkNodeValue("product_paperQuality");
				product_flyleaf = checkNodeValue("product_flyleaf");
				product_coated = checkNodeValue("product_coated");
			}
			
			//
			// CANVAS SPECIFIC
			if(classname == ProductsCatalogue.CLASS_CANVAS)
			{
				width = checkNodeValue("width");
				height = checkNodeValue("height");
				rows = checkNodeValue("rows");
				cols = checkNodeValue("cols");
				gap = checkNodeValue("gap");
				frame_color = checkNodeValue("frame_color");
			}
			
			//
			// CALENDAR SPECIFIC
			if(classname == ProductsCatalogue.CLASS_CALENDARS)
			{
				year = checkNodeValue("year");
				month = checkNodeValue("month");
				weekstart = checkNodeValue("weekstart");
				product_coated = checkNodeValue("product_coated");
			}
			
			//
			// CARDS SPECIFIC
			if(classname == ProductsCatalogue.CLASS_CARDS)
			{
				envelope = checkNodeValue("envelope");
			}
			
			
			
			Debug.log("Session fill completed");
		}
		
		
		/**
		 * return value if it exists
		 */
		private function checkNodeValue( value : String, defaultValue : String = null ) : String
		{
			if(sessionXml.session.hasOwnProperty(value))
			{
				return sessionXml.session.child(value)[0];
			}
			return defaultValue;
		}
		
		
	}
}



/* 

# XML EXAMPLE 1 

<?xml version="1.0" encoding="utf-8"?>
<root>
	<session>
		<id>pau26rpi9urmgfh1lk9oabogl6</id>
		<user_id>0</user_id>
		<vendor>1</vendor>
		<return_page>albums</return_page>
		<lang>fr</lang>
		<vendor_url>www.tictacphoto.com</vendor_url>
		<server_name>editor.tictacphoto.com</server_name>
	</session>
</root>


# XML EXAMPLE 2

<?xml version="1.0" encoding="utf-8"?>
<root>
	<session>
		<id>7qt2fdvf1tecffbf6b24rjtbf0</id>
		<user_id>0</user_id>
		<vendor>1</vendor>
		<lang>en</lang>
		<vendor_url>www.tictacphoto.com</vendor_url>
		<cover_name>Leather Black</cover_name>
		<cover_corners>none</cover_corners>
		<product_inserts>0</product_inserts>
		<product_id>101</product_id>
		<product_style>simple</product_style>
		<return_page>albums</return_page>
		<server_name>editor.tictacphoto.com</server_name>
	</session>
</root>


*/