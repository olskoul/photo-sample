package data
{
	public class ThumbAlbumVo extends ThumbVo
	{
		public var albumType:String;
		
		public function ThumbAlbumVo()
		{
			super();
		}
		
		/**
		 * FIll Vo
		 */
		override public function fill(obj:Object):void
		{
			super.fill(obj);
			albumType = obj.albumType;
			
		}
	}
}