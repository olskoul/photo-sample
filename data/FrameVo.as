package data
{
	import com.adobe.serialization.json.JSONDecoder;
	import com.adobe.serialization.json.JSONEncoder;
	
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	
	import be.antho.utils.MathUtils2;
	
	import manager.CalendarManager;
	import manager.MeasureManager;
	import manager.PagesManager;
	
	import offline.data.LocalInstances;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	
	import view.edition.FrameBackground;

	/**
	 * FrameVo contains all the information about the frame that display a photo in a project
	 */
	public class FrameVo
	{
		public static const TYPE_PHOTO:String = "picture";
		public static const TYPE_TEXT:String = "text";
		public static const TYPE_BKG:String = "bkg";
		public static const TYPE_CLIPART:String = "clipart";
		public static const TYPE_CALENDAR:String = "calendar";
		public static const TYPE_OVERLAYER:String = "overlayer";
		public static const TYPE_QR:String = "qr";
		
		public static const GROUP_TYPE_NONE:int = 0;
		public static const GROUP_TYPE_SPINE:int = 5;
		public static const GROUP_TYPE_EDITION:int = 6;
		
		public static const MASK_TYPE_NONE : int = 0;
		public static const MASK_TYPE_CIRCLE : int = 1;
		public static const MASK_TYPE_CORNER_RADIUS : int = 2;
		
		
		//public static const SHADOW_SCALE : Number = 1.08;
		
		//TYPE (text, image, bkg..°
		private var _type:String;
		public var width:Number = 200;	// curent frame width
		public var height:Number = 200;	// current frame height
		public var grouptype:int; //none, spine, edition

		// props
		public var rotation:Number = 0;	// current frame rotation (based on center)
		public var x:Number = 0;		// current frame position x (center)
		public var y:Number = 0;		// current frame position y (center)
		
		
		// use for print system, x and y real coordinates no relative to 0
		// as printing system use non zero relative rotation calculation we need to store this when we rotate the frame
		//public var left : Number = 0;
		//public var top : Number = 0;
		
		////////////////////////////////TYPE IMAGE
		// scale of image
		public var zoom:Number = 1;
		
		// crop values
		public var cLeft:Number = 0; // crop value in pixel relative to frame width (not photo width)
		public var cTop:Number = 0; // crop value in pixel relative to frame height (not photo height)
		
		// datas
		public var photoVo:PhotoVo;
		
		// border & shadow
		public var showBorder:Boolean = true;	// can this frame have a border
		public var border : Number = 0;
		public var shadow :Boolean = false;
		public var fillColor : Number = Colors.WHITE //-1;// TODO : reset this to -1 // TODO : actually if put to -1, it breaks as the background is empty. It should never be empty
		public var sCol : Number = Colors.BLACK; // Shadow color
		
		////////////////////////////////TYPE TEXT
		public var text:String  = "";
		public var alignment:int = 0;
		public var fontSize:Number;
		public var color:Number;
		public var hAlign:String = "center";
		public var vAlign : String = "top";
		public var tb : Boolean = false; 	// text background property flag
		public var fillAlpha : Number = 1; 	// For now it's used only in text frame for background
		
		
		////////////////////////////////POPARTS VALUES
		public var isPopart : Boolean = false;
		public var PopArtDarkColor:Number = 0x02a3e5;
		public var PopArtLightColor:Number = 0x9dd6ef;
		public var PopArtTreshold:Number = 50;
		public var PopArtBlackTreshold:Number = 30;
		
		////////////////////////////////CORNER RADIUS
		public var mask:int = MASK_TYPE_NONE; 		// type of mask
		public var cr:Number = 0;					// value of corner radius
		
		
		
		////////////////////////////////TYPE CALENDAR
		public var dateAction:String;
		public var dateIndex:int;
		public var stroke:Boolean;
		public var editable:Boolean = true;
		public var calBold:Boolean;
		public var calItalic:Boolean;
		public var calFontName:String;
		public var calFontSizeRaw:String;
		private var _visible:Boolean = true;
		public var isPageNumber:Boolean;
		public var posXAdjustement:Number = 0; // Added to the X position . For some calendar frame (Cal lux) X position can change depending on the month index
		
		
		// linkage id between frames, frames having same linkage will be set in a group of frame, moving together, rotating together
		// -> used for calendar frames
		// -> used for overlayer frames
		public var uniqFrameLinkageID:String;
		
		// linkage id representing the date in a calendar project
		// -> used for re-link to specific calendar frame when startMonth or startDay or year is changed
		// a date (day and month) based on a combination of dateAction, dateIndex , PageVo MonthIndex, starting day, starting month (year is not supposed to be taken into account)
		public var dateLinkageID:DateLinkageVo;
		
		////////////////////////////////TYPE CARDS
		public var isPostCardBackground:Boolean;
		public var isDeletable:Boolean = true;
		
		
		////////////////////////////////TYPE CANVAS
		public var isMultiple:Boolean = false;
		/*
			fixed is used in two different cases :
			> for frame text :  it means that the text bounds is fixed and can't be changed, no resize allowed and we open the outside text editor when clicking on frame
			> for frame photo (canvas only?) : it means that the frame cannot be resized, moved, cropped and deleted. It looks like a frame background behavior but can be for multiple frame inside canvas layouts.
		*/ 
		public var fixed:Boolean = false;
		
		////////////////////////////////TYPE QR
		public var QRLink:String = "";
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function FrameVo(_photoVo:PhotoVo = null):void
		{
			this.photoVo = _photoVo;
			
			if(_photoVo is BackgroundVo) type = TYPE_BKG;
			else if(_photoVo is ClipartVo) type = TYPE_CLIPART;
			else if(_photoVo is OverlayerVo) type = TYPE_OVERLAYER;
			else if(_photoVo is PhotoVo) type = TYPE_PHOTO;
			else if(_photoVo is QRVo) type = TYPE_QR;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTERS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Get and set dateLinkageID
		 */
		
		/*public function get dateLinkageID():DateLinkageVo
		{
			return _dateLinkageID;
		}
		
		public function set dateLinkageID(value:DateLinkageVo):void
		{
			_dateLinkageID = value;
		}*/

		
		public function get visible():Boolean
		{
			return shouldIBePrinted();
		}

		public function set visible(value:Boolean):void
		{
			_visible = value;
		}

		/**
		 * Get and set Type
		 */
		public function get type():String
		{
			return _type;
		}
		public function set type(value:String):void
		{
			_type = value;
		}
		
		
		/**
		 * quality of current frame, by default is 1 (0 = BAD , 1 = OK, -1 = NOT OK);
		 */ 
		public function get printQuality():Number
		{
			if(!photoVo || photoVo is BackgroundVo) return 1;
			return MeasureManager.checkImageResolution(zoom);
		}
		
		
		

		
		/**
		 *	Get Frame bounds (frame is always centered)
		 */
		public function get boundRect():Rectangle
		{
			return new Rectangle( -width*.5 , -height*.5, width, height);
		}
		
		
		/**
		 * check if content is a local content or an external content
		 * // TODO : we need to review this IS LOCAL CONTENT SYSTEM now that we can skip the localLoad via file system (TO CHECK WITH JON)
		 */
		public function get IS_LOCAL_CONTENT():Boolean
		{
			if( !Infos.IS_DESKTOP ) return false;
			if( photoVo && photoVo is AssetVo)
			{
				if(photoVo.isLocalContent)
					return true; 
				else
					return false;
			}
			//if( type == FrameVo.TYPE_CLIPART && photoVo && photoVo is ClipartVo) return true;
			if( type == FrameVo.TYPE_OVERLAYER && photoVo && photoVo is OverlayerVo) return true;
			if(photoVo && photoVo is ClipartVo) return false;
			
			// else this is a local offline content
			return true;
		}
		
		
		/**
		 *	Get print coordinates (in server side coord system : Rotation from Bottom Left)
		 */
		public function get printCoords():Point
		{
			var p:Point = new Point();
			var frameOffset : Number = MeasureManager.millimeterToPixel(border);
			var w : Number = width - frameOffset*2;
			var h : Number = height - frameOffset*2;
			
			if( rotation == 0 ){
				p.x = (x + posXAdjustement) - w*.5;
				p.y = y - h*.5;	
			} 
			else{
				var m : Matrix = new Matrix();
				m = new Matrix(w, 0, 0, h,0,0);
				//m.translate( -width*.5,-height*.5)
				m.rotate(MathUtils2.toRadian(rotation));
				m.translate((x + posXAdjustement),y);
				
				m.rotate(MathUtils2.toRadian(-rotation)); // reset to rotation 0
				m.translate(-w*.5, h*.5); // translate to bottom left point
				m.rotate(MathUtils2.toRadian(rotation)); // rotate to rotation needed
				m.translate(0, -h); // translate to top left point //m.translate(0, -boundRect.height); // translate to top left point

				p.x = m.tx;
				p.y = m.ty;
			}
			
			return p;
		}
		
		
		
		/**
		 * this allows to translate a frame in the same rotation space than another frame
		 * It's mainly used for linked frame or shadows, it returns correct translation by taking into account the rotation
		 */
		public function translateWithRotation( tx:Number, ty:Number ):Point
	  	{
			var translation : Point = new Point();
			var m : Matrix = new Matrix();
			m = new Matrix(width, 0, 0, height,0,0);
			m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(tx, ty); 						// translate to new position
			m.rotate(MathUtils2.toRadian(rotation)); 	// re-rotate to normal rotation
			
			return new Point(m.tx, m.ty);
			
			/*
			// as the rotation modify the pos calculation, we need to use a matrix to reset rotation first and be sure elements are at the right place
			var m : Matrix = new Matrix(startImageBounds.width, 0, 0, startImageBounds.height,0,0);
			m.rotate(MathUtils2.toRadian(-rotation)); 	// reset to rotation 0
			m.translate(translateX, translateY); 		// translate to new position
			m.rotate(MathUtils2.toRadian(rotation)); 	// rotate to rotation needed
			
			
			m.rotate(MathUtils2.toRadian(-rotation)); // reset to rotation 0
			m.translate(-w*.5, h*.5); // translate to bottom left point
			m.rotate(MathUtils2.toRadian(rotation)); // rotate to rotation needed
			m.translate(0, -h); // translate to top left point //m.translate(0, -boundRect.height); // translate to top left point
			
			p.x = m.tx;
			p.y = m.ty;
			*/
	  	}
		
		/**
		 * retrieve shadow nice size for this frame vo
		 */
		public function getShadowSize():Number
		{
			var shadowSize : Number = MeasureManager.millimeterToPixel(5); // 3 mm of shadow
			shadowSize = shadowSize * width/300;
			if(shadowSize > 20) shadowSize = 20;
			return shadowSize;
		}
		
		
		/**
		 *  is this frame using a user photo 
		 */
		public function hasUserPhoto():Boolean
		{
			if( photoVo != null && ( (type == FrameVo.TYPE_PHOTO) || (type == FrameVo.TYPE_BKG && !(photoVo is BackgroundVo)))) return true;
			return false;
		}
		
		
		
		/**
		 * Check if the vo is valid
		 * > IF photovo ID cannot be null
		 */
		public function get isValidVo():Boolean
		{
			if(photoVo && !photoVo.id) return false;
			return true;
		}
		
		public function get isEmpty():Boolean
		{
			var flag:Boolean;
			//if( width <= 0 || height <= 0 ) return true;
			if( type == TYPE_PHOTO && photoVo != null) return false;
			if( type == TYPE_CLIPART && photoVo != null) return false;
			if( type == TYPE_BKG && (photoVo != null || fillColor !=-1)) return false;
			if( type == TYPE_TEXT && text != null && text != "") return false;
			if( type == TYPE_OVERLAYER && photoVo != null) return false;
			if( type == TYPE_CALENDAR && visible) return false;
			if( type == TYPE_QR && QRLink != "") return false;
			return true;
		}
		
		/**
		 * check if the frame needs to be uploaded as bitmap for print
		 */
		public function get isPrintUpload():Boolean
		{
			// TEXT --> always upload
			if(type == FrameVo.TYPE_TEXT) return true;			
			
			// QR	--> always upload
			if (type == FrameVo.TYPE_QR) return true;			
			
			// CALENDAR	--> upload some specific types
			if( type == TYPE_CALENDAR && 
				(dateAction == CalendarManager.DATEACTION_NOTES 
					|| dateAction == CalendarManager.DATEACTION_MINICALENDAR_PREVIOUS
					|| dateAction == CalendarManager.DATEACTION_MINICALENDAR_NEXT)) return true; // if it's a note or a mini calendar, export as bitmap			
			
			
			// POPART EFFECT --> awlays upload popart effect
			if( isPopart && photoVo ) return true;		
			
			// BKG --> export only for offline and if there is a user photo in it
			if (Infos.IS_DESKTOP && type == FrameVo.TYPE_BKG && photoVo && !(photoVo is BackgroundVo)) return true			
			
			// PHOTO --> export only for offline
			if (Infos.IS_DESKTOP && type == FrameVo.TYPE_PHOTO && photoVo) return true;			
			
			// all others are not uploaded (photo online, cliparts, overlayers, backgrounds)
			return false;
		}
		 
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * fill a framevo object form anoter object
		 * > from save, from copy, from history
		 */
		public function fill(frameObj : Object):void
		{
			// COMMUN
			x= frameObj.x;
			y = frameObj.y;
			width = frameObj.width;
			height = frameObj.height;
			rotation = frameObj.rotation;
			type = frameObj.type;
			fillColor = frameObj.fillColor; 
			
			// FRAME ZOOM AND CROP VALUES
			zoom = frameObj.zoom;
			cLeft = frameObj.cLeft;
			cTop = frameObj.cTop;
			
			// EDITION
			isDeletable = frameObj.isDeletable;
			grouptype = frameObj.grouptype;
			uniqFrameLinkageID = frameObj.uniqFrameLinkageID;
			editable = frameObj.editable;
			if(frameObj.dateLinkageID)
			{
				dateLinkageID = new DateLinkageVo();
				dateLinkageID.fill(frameObj.dateLinkageID);
			}
			
			
			// QR
			QRLink = frameObj.QRLink;
			
			
			// TEXT ONLY
			isPageNumber = frameObj.isPageNumber;
			fontSize = frameObj.fontSize;
			color = frameObj.color;
			hAlign = frameObj.hAlign;
			vAlign = frameObj.vAlign;
			alignment = frameObj.alignment;
			text = frameObj.text;
			fixed = (frameObj.fixed)? frameObj.fixed : false;
			if (frameObj.fillAlpha != null ) fillAlpha = frameObj.fillAlpha;
			tb = frameObj.tb;
			
			
			// CALENDAR ONLY
			dateAction = frameObj.dateAction;
			dateIndex = frameObj.dateIndex;
			stroke = frameObj.stroke;
			calFontSizeRaw = frameObj.calFontSizeRaw;
			calBold = frameObj.calBold
			calItalic = frameObj.calItalic
			calFontName = frameObj.calFontName
			//posXAdjustement = frameObj.posXAdjustement
				
			// CARDS
			isPostCardBackground = frameObj.isPostCardBackground;
			

			// MASK and corner radius
			mask = (frameObj.mask)? frameObj.mask : MASK_TYPE_NONE;
			if (frameObj.cr != null ) cr = frameObj.cr;
			
			// BORDER AND SHADOW
			showBorder = ( frameObj.showBorder != null )? frameObj.showBorder : true;
			border = frameObj.border;
			shadow = frameObj.shadow;
			if (frameObj.sCol != null) sCol = frameObj.sCol; // otherwhise default value
			
			// POPART
			isPopart = frameObj.isPopart;
			PopArtDarkColor = frameObj.PopArtDarkColor;
			PopArtLightColor = frameObj.PopArtLightColor;
			PopArtTreshold = frameObj.PopArtTreshold;
			PopArtBlackTreshold = frameObj.PopArtBlackTreshold;
			
			// CANVAS ONLY
			isMultiple = frameObj.isMultiple;
			
			
			//bounds = new Rectangle(frameObj.bounds.x, frameObj.bounds.y, frameObj.bounds.width, frameObj.bounds.height);
			if(frameObj.photoVo != null){
				switch(type)
				{
					case TYPE_PHOTO:
						photoVo = new PhotoVo();
						photoVo.fill(frameObj.photoVo);
					break;
					
					case TYPE_BKG: // Background frame can receive photovo's and backgroundvo's
						if(frameObj.photoVo.proxy) {
							photoVo = new BackgroundVo()
							photoVo.fill(frameObj.photoVo);
						}
						else {
							photoVo = new PhotoVo();
							photoVo.fill(frameObj.photoVo);
						}
						break;
						
					case TYPE_CLIPART:
						photoVo = new ClipartVo()
						photoVo.fill(frameObj.photoVo);
						break;
					
					case TYPE_OVERLAYER:
						photoVo = new OverlayerVo();
						photoVo.fill(frameObj.photoVo);
						break;
				}
			}			
		}
		
		
		/**
		 * create a copy of this framevo
		 */
		public function copy():FrameVo
		{
			var f:String = new JSONEncoder(this).getString();
			var d:JSONDecoder = new JSONDecoder(f);
			var newFrame : FrameVo = new FrameVo();
			newFrame.fill(d.getValue());
			return newFrame;
		}
		
		/**
		 * apply frame zoom, crop and transformations to allow image to fit completely in frame
		 * > frame vo is the frame width/height info used to determine crop and zoom property
		 * > photovo is the photo that will be injected in current frame
		 * > keepPhotoRatio > if set to false, we modify zoom and crop to fit photo in frame, if set to true, we modify the frame to have same ratio as photovo
		 */
		public static function injectPhotoVoInFrameVo( frameVo : FrameVo, photoVo : PhotoVo, keepPhotoRatio:Boolean = false):void
		{
			if(photoVo == null){Debug.warn("FrameVo > injectPhotoVoInFrameVo > photoVo is null");return;};
			
			// we want the frame to fill the frame
			var photoWidth : Number = photoVo.width;
			var photoHeight : Number = photoVo.height;
			var photoRatio : Number = photoWidth/photoHeight;
			
			// if the image should not fit inside frame, we modify the frame to keep ratio
			if(keepPhotoRatio){
				frameVo.height = photoHeight * frameVo.width/photoWidth;
			}
			
			var neededWidth : Number = frameVo.width;
			var neededHeight : Number = frameVo.height;
			var neededRatio : Number = neededWidth/neededHeight;
			
			// reset crop values
			frameVo.cLeft = frameVo.cTop = 0;
			
			// new image scale and crop values
			var newScale : Number;
			var offset : Number; // offset part of the image that need to be cropped to fit in current frame limits
			if(neededRatio > photoRatio) {
				// we fit on the width
				newScale = neededWidth/photoWidth;
				// as we fit on the width, we need to crop on the height
				offset = photoHeight*newScale - neededHeight;
				frameVo.cTop = offset*.5;
			}
			else {
				// we fit on the height
				newScale  = neededHeight/photoHeight;
				// as we fit on the width, we need to crop on the width
				offset = photoWidth*newScale - neededWidth;
				frameVo.cLeft = offset*.5;
			}
			
			frameVo.zoom =  newScale;
			frameVo.photoVo = photoVo;
		}
		
		
		/**
		 * Util to retrieve real size cropped area of photo in the fullsize image (in pixel)
		 */
		public static function GetRealSizeCropRect(frameVo:FrameVo):Rectangle
		{
			// return new Rectangle(0,0,frameVo.width, frameVo.height);
			
			if(!frameVo) return null;
			if(!frameVo.photoVo) return new Rectangle(0,0,frameVo.width, frameVo.height);
			var l : Number = Math.round(frameVo.cLeft/frameVo.zoom);
			var t : Number = Math.round(frameVo.cTop/frameVo.zoom);
			var w : Number = Math.round(frameVo.width/frameVo.zoom);
			var h : Number = Math.round(frameVo.height/frameVo.zoom);
			if(l<0) l = 0;
			if(t<0) t = 0;
			return new Rectangle( l,t,w,h )
		}
		
		
		/**
		 * Reset a framevo to empty
		 * 
		 */
		public function reset():void
		{
			// reset crop values
			cLeft = cTop = 0;
			zoom = 0;
			photoVo = null;
		}
		
		
		public function toString():String
		{
			var f : String = "FrameVo type:"+type + " width:"+width + " height:"+height  ;
			switch (type)
			{
				case TYPE_TEXT :
					return f + " text:"+text ;
					break;
				default :
					return f + " photo:"+photoVo + " isPopart:"+isPopart;
					break;
				
			}
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATES
		////////////////////////////////////////////////////////////////
		
		private function shouldIBePrinted():Boolean
		{
			var monthIndex:int = CalendarManager.instance.getFrameMonthIndex(this);	
			
			/**
			 * Birthday Calendars Rules:
			 * They should not display offset days
			 * But in the calculation, the 29th of feb can be considered as offset depending on the year
			 * Nico wants to always show the 29th
			 */
			if(Infos.project.docCode == "WCAL4" && dateAction == CalendarManager.DATEACTION_DAYDATE)//birthday calendars
			{
				
				var monthId:String = CalendarManager.instance.getMonthWithOffSetYear(monthIndex, false);
				var text : String = CalendarManager.instance.getFrameText(this);
				var dayType:String = CalendarManager.instance.whatKindofDayDateAmI(this,monthIndex,text);
				if(dayType == CalendarManager.DAYDATE_KIND_OFFSET) // exception for 29th february
				{
					if(dateIndex == 29 &&  monthId == "february")
					{
						//iAm29thFebOfBirthDayCal = true; //TODO: set to true
						return true;
					}
					else
						return false;
				}
			}
			
			/**
			 * Calendar Lux Rules: 
			 * Layout provides page with 31 Frames with dateAction DateName (Mon 1, Tue 2 etc...) 
			 * But not all the month have 31 days
			 * Find out how many frame must no be printed/displayed based on the pageVo's monthIndex
			 */
			else if(Infos.project.docPrefix == "WCAL5" || Infos.project.docPrefix == "WCAL6")
			{
				if(dateAction == CalendarManager.DATEACTION_DATENAME)
				{
					var dayInThisMonth:int = CalendarManager.instance.getDaysInMonth(monthIndex);
					var offSetDays:int = 31 - dayInThisMonth;
					
					if(dateIndex <= dayInThisMonth) 
					{
						//apply X adjustement to keep the layout centered
						var totalOffSetX:Number = offSetDays*width;
						var xModification:Number = totalOffSetX/2;
						posXAdjustement = xModification;
						return true;
					}
					else
						return false;
				}
				return true;
			}
			
			return true;
		}
		
	}
}