package data
{
	import be.antho.data.SharedObjectManager;
	
	import manager.ProjectManager;
	import manager.SessionManager;
	
	import utils.Debug;

	/**
	 * Store user preferences in here
	 */ 
	public class PreferencesVo
	{
		////////////////////////////////////////////////////////////////
		//	PROPS
		////////////////////////////////////////////////////////////////
		private var _PATH_GENERAL_PREFS:String; 
		private var _PATH_PROJECT_PREFS:String; 
		
		// general prefs
		public var projectToOpen : String; 				// when clicking on open project, we store the value in the prefs and restart app with new project id
		public var projectToOpenName : String			// store the project to open name as this name can have been changed offline, we got it from project list info
		public var projectToOpenClassname : String		// store the project to open classname as this name can have been changed offline, we got it from project list info
		public var lang : String						// store the lang
		public var isNewProject : Boolean = false;		// when clicking on new project, we store this info and open app with no project 
		
		// project prefs
		public var foldersUsedList : Array ; 			// list of folders used in this project
		public var folderClosedList : Array ; 			// list of previous opened folder in left menu
		public var currentFolderVisible : String;		// current photo folder position
		public var photoSortingType : String = ProjectManager.PHOTO_SORT_ON_NAME_ASC;		// current photo sorting property (name or date)
		
		
		
		// private prefs obj
		private var generalPrefs : Object ;
		private var projectPrefs : Object ;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		private static var instance:PreferencesVo;
		public function PreferencesVo()
		{
			if(instance) Debug.error("PreferencesVo must only have one instance");
			instance = this;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////
		public function get PATH_GENERAL_PREFS():String
		{
			return (Infos.IS_DESKTOP)?"_generalPrefs_" : "_generalPrefs_"+Infos.session.classname;
		}

		public function get PATH_PROJECT_PREFS():String
		{
			return "_prefs_"+Infos.session.classname+"_";
		}
		
		
		

		////////////////////////////////////////////////////////////////
		//	PUBLICS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Load preferences from flash cookie (general only)
		 */
		public function loadGeneralPrefs():void
		{
			var prefObj : Object =  SharedObjectManager.instance.read(Infos.session.userId + PATH_GENERAL_PREFS);
			if(prefObj != null){
				projectToOpen = prefObj.projectToOpen;	
				projectToOpenName = prefObj.projectToOpenName;	
				projectToOpenClassname = prefObj.projectToOpenClassname;	
				lang = prefObj.lang;	
				isNewProject = prefObj.isNewProject;	
			}
		}
		public function generalPrefToString():String
		{
			var str:String = "";
			str += "projectToOpen : "+projectToOpen + "\n";
			str += "projectToOpenName : "+projectToOpenName + "\n";
			str += "projectToOpenClassname : "+projectToOpenClassname + "\n";
			str += "isNewProject : "+isNewProject + "\n";
			str += "lang : "+lang + "\n";
			return str;
		}
		
		
		/**
		 * Load preferences from flash cookie (project only)
		 */
		public function loadProjectPrefs():void
		{
			var prefObj : Object =  SharedObjectManager.instance.read(Infos.session.userId + PATH_PROJECT_PREFS +Infos.project.id);
			Debug.log( "PreferencesVo.loadProjectPrefs " + prefObj );
			
			if(!Infos.project.id) 
			{
				// we are in a temp project, we use fresh new preferences
				folderClosedList = null;
				foldersUsedList = null;
				currentFolderVisible = null;	
				return;
			}
			
			if(prefObj != null)
			{
				folderClosedList = prefObj.folderClosedList;
				if(prefObj.foldersUsedList != null)	foldersUsedList = prefObj.foldersUsedList; // new system
				else foldersUsedList = convertFoldersActivatedList( prefObj.folderActivatedList ); // old system
				
				currentFolderVisible = prefObj.currentFolderVisible;	
				photoSortingType = prefObj.photoSortingType;
			}
		}
		/**
		 * conver old folder activatedList system to new foldersUsedList
		 */
		private function convertFoldersActivatedList(folderDesactivatedList:Array):Array
		{
			Debug.log("PreferencesVo.loadProjectPrefs : " + folderDesactivatedList);
			var newList:Array;
			var addFolder:Boolean = false;
			// old system used folderActivated to store DESACTIVATED folders. Which was difficult to understand. We know store only project used folder and we are converting this if it still exists
			for (var i:int = 0; i < Infos.project.albumList.length; i++) 
			{
				addFolder = true;
				
				// check in old desactivated folder list
				if(folderDesactivatedList){
					for (var j:int = 0; j < folderDesactivatedList.length; j++) 
					{
						if( folderDesactivatedList[j] == Infos.project.albumList[i].name ) addFolder = false;
					}
				} else addFolder = false;
				
				
				// add it
				if(addFolder)
				{
					if(!newList) newList= new Array();
					newList.push( Infos.project.albumList[i].name );
				}
			}
			
			return newList;
		}
		
		
		
		/**
		 *
		 */
		public function save():void
		{
			saveGeneralPrefs();
			saveProjectPrefs();
		}
		
		/**
		 ** save preferences from flash cookie (general only)
		 */
		public function saveGeneralPrefs():void
		{
			generalPrefs = new Object();
			generalPrefs.projectToOpen = projectToOpen;
			generalPrefs.projectToOpenName = projectToOpenName;
			generalPrefs.projectToOpenClassname = projectToOpenClassname;
			generalPrefs.lang = lang;
			generalPrefs.isNewProject = isNewProject;
			SharedObjectManager.instance.write(Infos.session.userId + PATH_GENERAL_PREFS, generalPrefs);	
		}
		
		/**
		 ** save preferences from flash cookie (project only)
		 */
		public function saveProjectPrefs():void
		{
			if(Infos.project && Infos.project.id){ // we save project prefs if we have a project and that project has been saved (project id)
				var cookieName : String = Infos.session.userId + PATH_PROJECT_PREFS +Infos.project.id;
				Debug.log( "PreferencesVo > saveProjectPrefs " + cookieName );
				projectPrefs = new Object();
				projectPrefs.folderClosedList = folderClosedList;
				projectPrefs.currentFolderVisible = currentFolderVisible;
				projectPrefs.foldersUsedList = foldersUsedList;
				projectPrefs.folderActivatedList = null; //folderActivatedList; // Fix for old projects
				projectPrefs.photoSortingType = photoSortingType; //folderActivatedList; // Fix for old projects
				SharedObjectManager.instance.write(cookieName, projectPrefs);
			}
		}
		
		
		
	}
}