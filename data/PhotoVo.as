package data
{
	import com.adobe.serialization.json.JSONDecoder;
	import com.adobe.serialization.json.JSONEncoder;
	
	import utils.Debug;
	
	import manager.SessionManager;
	
	CONFIG::offline
	{
		import flash.filesystem.File;
		import offline.data.LocalInstances;
	}
	

	/**
	 * PhotoVo contains all the information about a photo object
	 */
	public class PhotoVo extends ThumbVo
	{
		//File infos
		public var name : String; 		// name of the photo
		public var suffix : String; 	// used for print as frameDir
		public var folderName : String; // The name of the album/folder the photo belongs to
		public var width : int; 		// The width of loaded image
		public var height : int; 		// The height of loaded image
		public var thumbImagePath : String			// the thumb path
		public var fullsizeImagePath : String; 		// The fullsize image direct path
		public var workingImagePath : String; 		// The working image direct path
		
		
		// ONLINE : temp = the photo is imported but the upload is not yet finished, this photovo is temporary ( = STATE UPLOADING )
		// OFFLINE : temp = the photo is not yet imported in project, thumb/image urls are still the origin pos
		public var temp: Boolean = false; 	
		public var tempID : String; 	// temp id when photovo is not yet created serverside. (photo is importing), we have a temporary id for this image ( = STATE IMPORTING )
		public var edited : Boolean = false;	// is this photoVo a user edited photo (important for offline as it changes the path from absolute to relative to user folder)
		
		public var lastModified : Number; 	// last modified date of the photo
		public var originalDate : Number; 	// date of the photo
		protected var isLocalContentValue : String = "?"; 	// is the content of the vo (thumb, normal, high) are all availlable locally (used for assets only, BKG, cliparts, overlays)
		
		// offline only
		public var originUrl:String;		// the original file url, this will help to find matching files
		public var exifRot:Number = 0;		// this new value allows us to add an "inside" rotation for a frame, and manage easily exif rotation
		
		//Project related (not saved)
		public var used : Boolean ; 		// if the photo is already used in the current album.
		public var error:Boolean = false;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		public function PhotoVo():void{};
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		///////////////////////////////////////////////////////////////
		/**
		 * Check if the content of the vo (thumb, normal, high) are all availlable locally (used for assets only, BKG, cliparts, overlays)
		 */
		public function get isLocalContent():Boolean
		{
			if(isLocalContentValue == "yes")
				return true;
			if(isLocalContentValue == "?")
				return checkIfLocalContent();
			else
				return false;
			
		}

		/**
		 * Check if the vo is valid
		 */
		override public function get isValidVo():Boolean
		{
			return (name != null);
		}
		
		/**
		 * url to the thumb 
		 */
		override public function get thumbUrl():String{ 
			
			// online
			if(!Infos.IS_DESKTOP) return Infos.config.serverNormalUrl + thumbImagePath;
			// offline
			else if(temp) return thumbImagePath;
			else return Infos.currentProjectPath + "/" + thumbImagePath; // thumbs are stored in project folder
		};
		
		/**
		 * url to the normal version, used to work with
		 */
		public function get normalUrl():String{ 
			// online
			if(!Infos.IS_DESKTOP) return Infos.config.serverNormalUrl + workingImagePath;
			// offline
			else return highUrl; // there is no normal url for offline
		};
		
		
		/**
		 * url to the fullsize photo version, 
		 * > used to precise photo manipulation. 
		 * > This manipulations should save a new version of the file.
		 */
		public function get highUrl():String
		{
			// ONLINE
			CONFIG::online
			{
				return Infos.config.serverNormalUrl + fullsizeImagePath;
			}
			
			// OFFLINE
			CONFIG::offline
			{
				// photo is still a temp photo, in left photo tab
				if(temp) return fullsizeImagePath; 
				// photo is imported and project is using copy system OR image is an edited image (relative path to folder)
				else if(Infos.project.imageStorageSystem == ProjectVo.STORAGE_TYPE_COPY || edited)
					return Infos.currentProjectPath + "/" + fullsizeImagePath;
				// photo is using reference system and is not an edited image (absolute path to origin)
				else return new File(originUrl).url;
			}
		};
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * FIll Vo
		 */
		override public function fill(obj:Object):void
		{
			id = obj.id;
			//super.fill(obj);
			name = obj.name;
			
			// check if no id.. could be problematic so we try to find it back
			if(!id) {
				var ref:PhotoVo = SessionManager.instance.getPhotoVoByName(name);
				if(ref) id = ref.id;
				else {
					Debug.warn("PhotoVo with name : "+name+" has no id and doesn't exist anymore, we should destroy it");
				}
			}
			
			//thumbUrl = Infos.config.serverNormalUrl + "/flex/tn.php?img="+id; 
			//thumbUrl = obj.thumbUrl;
			
			
			
			// SECURITY and Retro compatibility with URL system
			// Actualy every photoVo should be refreshed/remaped with xml version, making them always up to date
			// but it happens that photo is not available in photo list anymore, we have then to try to fix this manually.. If this doesn't fix it, the user will have a red frame
			if( obj.thumbImagePath ) thumbImagePath = obj.thumbImagePath;
			else thumbImagePath = "/flex/tn.php?img=" +id;
			if( obj.fullsizeImagePath ) fullsizeImagePath = obj.fullsizeImagePath;
			else fullsizeImagePath = "/flex/img.php?img=" +id;
			if( obj.workingImagePath ) workingImagePath = obj.workingImagePath;
			else workingImagePath = "/flex/work.php?img=" +id;
			//else if(obj.thumbUrl && obj.thumbUrl.indexOf(".tictacphoto.com/") != -1) thumbImagePath = obj.thumbUrl.split(".tictacphoto.com")[1];
			//else Debug.warn("PhotoVo.fill : no thumbImagePath and thumbUrl is : "+obj.thumbUrl);
			
			if( obj.lastModified ) lastModified = obj.lastModified;
			else lastModified = 0;
			
			if( obj.originalDate ) originalDate = obj.originalDate;
			else originalDate = 0;
			
			// folder name			
			if(obj.albumName) folderName = obj.albumName;
			else folderName = (obj.folderName)? obj.folderName : "unknow" ;
			
			// origin url (offline only for now
			if(Infos.IS_DESKTOP && obj.originUrl) originUrl = obj.originUrl;
			
			width = obj.width;
			height = obj.height;
			suffix = obj.suffix;
			temp = obj.temp;
			tempID= obj.tempID;
			exifRot = (obj.exifRot)? obj.exifRot : 0;
			edited = obj.edited;
		}
		
		/**
		 * FIll Vo
		 */
		public function fillFromXML(node:XML):void
		{
			id = node.id[0].text();
			name = node.name[0].text();
			folderName = node.category[0].text();
			
			width = parseInt(node.width[0].text());
			height = parseInt(node.height[0].text());
			suffix = node.suffix[0].text();		
			
			lastModified = Number(node.last_modified);		
			originalDate = Number(node.original_date);		
			
			// old
			//thumbUrl = Infos.config.serverNormalUrl + "/flex/tn.php?img="+id;
			//thumbUrl = Infos.config.serverNormalUrl + node.tn[0].text();
			
			thumbImagePath = node.tn[0].text();
			if(thumbImagePath == "local")
				thumbImagePath = "/flex/tn.php?img=" +id; // TODO : this is a hack, server seems to send "local" for thumb url when it's not finished online... we need to have final thumb url, so we force it here
			fullsizeImagePath = node.fullsize[0].text();		

			workingImagePath = node.working[0].text();
			
			// offline origin url
			if(Infos.IS_DESKTOP)
			{
				if( node.hasOwnProperty("origin_url")) {
					originUrl = ""+node.origin_url[0].text();
					if(originUrl == "") originUrl = null;
				}
			}
		}
		
		/**
		 * Sets isLocalContent Value to "?"
		 */
		public function resetLocalContentValue():void
		{
			isLocalContentValue = "?";
		}
		
		/**
		 * Checks if all the need files are available offline, if not, return false.
		 * overriden by backgroundVo, clipartVo, overlayVo
		 */
		protected function checkIfLocalContent():Boolean
		{
			//overriden by assetVo -> backgroundVo, clipartVo, overlayVo
			isLocalContentValue = "no";
			return false; 
		}
		
		/**
		 * OFFLINE switch for url
		 * Define what url to return
		 */
		protected function urlToReturn(onlineURL:String, offlineUrl:String):String
		{
			if(isLocalContent)
				return offlineUrl;
			else
				return onlineURL;
		}
		
		
		/**
		 * create a copy of this framevo
		 */
		public function copy():PhotoVo
		{
			var p:String = new JSONEncoder(this).getString();
			var d:JSONDecoder = new JSONDecoder(p);
			var newPhotoVo : PhotoVo = new PhotoVo();
			newPhotoVo.fill(d.getValue());
			return newPhotoVo;
		}

	}
}

