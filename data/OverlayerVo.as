package data
{
	public class OverlayerVo extends AssetVo
	{
		public var bleft:Number; // Seems to be the border (the frame inside limits)
		public var btop:Number;
		public var bwidth:Number;
		public var bheight:Number;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function OverlayerVo(){ super(); }
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * Check if the vo is valid
		 */
		override public function get isValidVo():Boolean
		{
			return (id != null);
		}
		
		/**
		 * offline url to the thumb photo version
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		override public function get offlineThumbUrl():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsCommonPath + "/overlays"+"/tn_"+id+".png";	
			}
			return null;			
		}
		
		/**
		 * offline url to the fullsize photo version
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		override public function get offlineHighUrl():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsCommonPath + "/overlays"+"/"+id+".png";	
			}
			return null;			 
		}
		
		override public function get tnUrl():String{ 
			
			var onlinePath:String = Infos.config.serverNormalUrl + "/flex/loadoverlays.php?image=tn_"+id;
			return urlToReturn(onlinePath,offlineThumbUrl);
		};
		
		override public function get hdUrl():String{ 
			
			var onlinePath:String = Infos.config.serverNormalUrl + "/flex/loadoverlays.php?image="+id;
			return urlToReturn(onlinePath,offlineHighUrl);
		};
		
		
		/*override public function get thumbUrl():String
		{
			if(!Infos.IS_DESKTOP)
			return Infos.config.serverNormalUrl + "/flex/loadoverlays.php?image=tn_"+id;
			else
			{
				var path:String = Infos.offlineAssetsCommonPath + "/overlays"+"/"+id+".png"; //"app-storage:/"+
				return path;
			}
		};
		
		override public function get highUrl():String{ 
			if(!Infos.IS_DESKTOP)
				return Infos.config.serverNormalUrl + "/flex/loadoverlays.php?image="+id;
			else
			{
				var path:String = Infos.offlineAssetsCommonPath + "/overlays"+"/"+id+".png"; //"app-storage:/"+
				return path;
			}
		};*/
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * FIll vo from xml
		 */
		public override function fillFromXML(node:XML):void
		{
			/* EXAMPLE *
			<overlay 	
				left="0" top="0" 
				width="0" height="0" 
				bleft="0" btop="0" bwidth="0" bheight="0" 
				name="none" 
				type="overlay" 
				function="overlay_none" 
				luminance="0.8" 
				tnproxy="overlays/tn_blank.png" 
				proxy="overlays/blank.png"
				/>
			*/
			id = String(String(node.@proxy).split(".png")[0]).substring(9);  // there is no id for overlayers so we need to create it from url // TODO : ask Keith to provide ID, this would be less tricky and more robust
			name = node.@name;
			proxy = String(node.@proxy);
			width = parseInt(node.@width);
			height = parseInt(node.@height);
			
			// border frame
			bleft = parseInt(node.@bleft);
			btop = parseInt(node.@btop);
			bwidth = parseInt(node.@bwidth);
			bheight = parseInt(node.@bheight);
		}	
		
		
		
		/**
		 * FIll Vo from json or object
		 */
		override public function fill(obj:Object):void
		{
			super.fill(obj);
			
			id = obj.id;
			name = obj.name
			proxy = obj.proxy;
			width = obj.width;
			height = obj.height;
			
			// border frame
			bleft = obj.bleft;
			btop = obj.btop;
			bwidth = obj.bwidth;
			bheight = obj.bheight;
		}
	}
}