/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2012
 ****************************************************************/
package data 
{	
	import utils.Debug;

	public class ConfigVo 
	{		
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////
		
		private var configXML : XML;
		private var _languages : Vector.<LangVo>;
		private var _contactTopics : Vector.<ContactTopicVo>;
		private var _serverNormalUrl : String;
		private var _serverSecureUrl : String;
		private var _serverApiUrl : String;
		public var serverType : String;
		
		// rebranding
		private var rebrandingXML : XML;
		private var _mainColor : Number;
		private var _vendorId : String;
		private var _mainLogoUrl : String;
		private var _isRebranding :Boolean;
		private var _termAndConditions : String;
		private var _rebrandingForcedLanguage : String;
		private var _rebrandingInstallerUrl:String;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR
		////////////////////////////////////////////////////////////////
		
		public function ConfigVo() 
		{}
		
		
		
		////////////////////////////////////////////////////////////////
		// 	GETTER/SETTER 
		////////////////////////////////////////////////////////////////
		
		public function get contactTopics():Vector.<ContactTopicVo>
		{
			return _contactTopics;
		}

		public function set contactTopics(value:Vector.<ContactTopicVo>):void
		{
			_contactTopics = value;
		}

		public function get languages():Vector.<LangVo>
		{
			return _languages;
		}

		public function set languages(value:Vector.<LangVo>):void
		{
			_languages = value;
		}

		public function get serverApiUrl():String
		{
			if(!_serverApiUrl) _serverApiUrl = "" + getSettingsValue("EditorProtocol") + "://" + getSettingsValue("ApiHost"); 
			return _serverApiUrl;
		}

		public function get serverNormalUrl():String
		{
			if(!_serverNormalUrl) _serverNormalUrl = "" + getSettingsValue("EditorProtocol") + "://" + getSettingsValue("EditorHost"); 
			return _serverNormalUrl;
		}
		
		public function get serverSecureUrl():String
		{
			if(!_serverSecureUrl) _serverSecureUrl = "" + getSettingsValue("SecureProtocol") + "://" + getSettingsValue("OrderHost"); 
			return _serverSecureUrl;
		}
		
		public function get isRebranding():Boolean
		{
			return _isRebranding;
		}
		
		public function get termAndConditionsPath():String
		{
			return _termAndConditions;
		}
		
		public function get rebrandingForcedLanguage():String
		{
			return _rebrandingForcedLanguage;
		}
		
		public function get rebrandingMainColor():Number
		{
			return _mainColor;
		}
		
		public function get rebrandingLogoUrl():String
		{
			return _mainLogoUrl;
		}
		
		public function get rebrandingInstallerUrl():String
		{
			return _rebrandingInstallerUrl;
		}
		
		public function get vendorId():String
		{
			if(_isRebranding)
				return _vendorId;
			else
				return getSettingsValue("VendorCode");
		}
		
		public function get newsXMLUrl():String
		{
			return getSettingsValue("News");
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		public function fill(content:XML):void 
		{
			configXML = XML(content.config);
			serverType = serverNormalUrl.slice(7).split(".tictacphoto.com")[0] + ".tictacphoto.com";
			
			// rebranding values
			//Overriding value from the config itself
			if(Infos.REBRANDING_ID != "")
			{
				rebrandingXML = XML(content.rebranding.brand.(@id == Infos.REBRANDING_ID));
				if( rebrandingXML && rebrandingXML.logo != undefined)
				{
					
					_mainColor = parseInt( rebrandingXML.mainColor.@data, 16 );
					_mainLogoUrl = ""+rebrandingXML.logo.@data;
					_vendorId = rebrandingXML.vendorCode.@data;
					_termAndConditions = ""+rebrandingXML.termAndConditions.@data;
					
					_rebrandingForcedLanguage = ""+rebrandingXML.forcedLanguage.@data;
					if(!_rebrandingForcedLanguage && _rebrandingForcedLanguage == "") _rebrandingForcedLanguage = null;
					
					_rebrandingInstallerUrl = ""+rebrandingXML.installer.@data;
					
					_serverNormalUrl = "" + getSettingsValue("EditorProtocol") + "://" + rebrandingXML.editorHost.@data;
					_isRebranding = true;
					
				}
				else
				{
					throw new Error("REBRANDING ERROR: Infos.REBRANDING_ID is set to \""+Infos.REBRANDING_ID+"\" but configXML node associated has not been found. Check Infos.as for REBRANDING 'how to'");
				}
			}
			
			//Languages
			Debug.log("ConfigVo.fill > Languages");
			languages = new Vector.<LangVo>();
			var languagesList:XMLList = configXML.languages.lang;
			for (var i:int = 0; i < languagesList.length(); i++) 
			{
				var langVo:LangVo = new LangVo();
				langVo.fill(languagesList[i]);
				languages.push(langVo);
			}
			
			//Contact topic
			contactTopics = new Vector.<ContactTopicVo>();
			var topicList:XMLList = configXML.contactTopics.topic;
			for (var i:int = 0; i < topicList.length(); i++) 
			{
				var contactTopic:ContactTopicVo = new ContactTopicVo();
				contactTopic.fill(topicList[i]);
				contactTopics.push(contactTopic);
			}
			
		}
		
		
		/**
		 *
		 */
		public function getValue(value:String):String 
		{
			if(!configXML) return null;
			return configXML[value].@data;
		}
		
		/**
		 *
		 */
		public function getSettingsValue(value : String):String
		{
			return getValue("settings"+value);
		}
		
	}
}