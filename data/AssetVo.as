package data
{
	CONFIG::offline
		{
			import offline.manager.OfflineAssetsManager;
		}
	

	public class AssetVo extends PhotoVo
	{
		
		public var id_tn:String; //Thumbnail ID
		public var id_hd:String; //High res ID
		public var category:String; //christmas, amour, ...
		public var proxy:String; //online location used as uniq ID
		
		
		public function AssetVo()
		{
			super();
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		/**
		 * Check if the vo is valid
		 */
		override public function get isValidVo():Boolean
		{
			return (id_tn != null);
		}
		
		override public function get thumbUrl():String
		{
			return tnUrl;
		};
		
		override public function get normalUrl():String{ 
			return hdUrl;
		};
		
		override public function get highUrl():String{ 
			return hdUrl;
		};
		
		
		/**
		 * offline url to the thumb photo version
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		public function get offlineThumbUrl():String
		{
			return "this must be overriden";
		}
		
		/**
		 * offline url to the fullsize photo version
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		public function get offlineHighUrl():String
		{
			return "this must be overriden";
		}
		
		/**
		 * Url to load thumb
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		public function get tnUrl():String
		{ 
			return "this must be overriden";
		};
		
		/**
		 * Url to load hight res
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		public function get hdUrl():String
		{ 
			return "this must be overriden";
		};
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * FIll Vo
		 */
		override public function fill(obj:Object):void
		{
			super.fill(obj);
			
			id_tn = obj.id_tn;
			id_hd = obj.id_hd;
			category = obj.category;
			proxy = obj.proxy;
			
			//override thumbURL
			thumbUrl = tnUrl; 
		}
		
		/**
		 * Checks if all the need files are available offline, if not, return false.
		 */
		override protected function checkIfLocalContent():Boolean
		{
			CONFIG::offline
			{
				if(OfflineAssetsManager.instance.exist(offlineThumbUrl) && OfflineAssetsManager.instance.exist(offlineHighUrl))
				{
					isLocalContentValue = "yes";
					return true;
				}
			}
			
			isLocalContentValue = "no";
			return false; 
		}
	}
}