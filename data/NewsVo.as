package data
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	import manager.TutorialManager;

	public class NewsVo 
	{
		
		public var title : String;
		public var description : String;
		public var imageUrl : String;
		
		
		public function NewsVo($title:String, $description:String, $imageUrl:String):void
		{
			title = $title;
			description = $description;
			imageUrl = $imageUrl;
		}
		
		
		
	}
}