package data
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import be.antho.data.DateUtil;
	import be.antho.data.ResourcesManager;
	
	import comp.DefaultTooltip;
	
	import library.checkbox.selected.icon;
	
	import manager.FolderManager;
	import manager.ProjectManager;
	import manager.TutorialManager;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.popup.PopupContact;
	import view.popup.PopupProjectExplorer;

	public class TooltipVo
	{
		public static const TOOLTIP_SETTINGS_UID:String = "tootltipSettingsuid";
		
		public static const TYPE_SIMPLE : String = "TYPE_SIMPLE";
		public static const TYPE_HELP : String = "TYPE_HELP";
		public static const TYPE_IMAGE_PREVIEW : String = "TYPE_IMAGE_PREVIEW";
		public static const TYPE_BUTTON : String = "TYPE_BUTTON";
		public static const TYPE_BUTTON_DELETE : String = "TYPE_BUTTON_DELETE";
		
		public static const ARROW_TYPE_VERTICAL : String = "ARROW_TYPE_VERTICAL";
		public static const ARROW_TYPE_HORIZONTAL : String = "ARROW_TYPE_HORIZONTAL";
		
		// infos
		public var uid : String; // uniq id for the tooltip
		public var type : String;
		public var margin:Number = 10;
		public var bgColor:Number ; 
		public var bgAlpha:Number = 1; 
		public var textColor:Number ;
		//public var label:String;
		public var labelKey:String;
		public var labelIsNotKey:Boolean;
		public var arrowType : String
		public var persistOnKill : Boolean = false;
		public var interactive : Boolean = false; // can receive mouse actions
		public var autoHide : Number = -1; // time in second before auto hide (-1 if no autoHide)
		
		// custom content
		public var contentOrientation:String = "vertical"; // vertical or horizontal
		public var content : Array;// Example : [{type:text, value:"coucou je suis un texte"}, {type:image, value:"http://www.coucou.be/img.jpg"}]
		public var isLocal : Boolean = false;
		public var maxWidth : Number = NaN;
		private var _additionalUntranslatedContent:String = "";
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		public function TooltipVo($labelKey:String = "tooltip.example.key" , $type:String = TYPE_SIMPLE, $customContent:Array =null, $bgColor:Number =0x62a639, $textColor:Number = 0xffffff, $arrowType:String = ARROW_TYPE_VERTICAL ):void
		{
			uid = ""+new Date().time + "_" + Math.round(Math.random()*100);
			labelKey = $labelKey;
			type = $type;
			content = $customContent;
			bgColor = $bgColor;
			textColor = $textColor;
			arrowType = $arrowType;
		};
		
		/////////////////////////////////////////////////////////////////
		//	Shortcuts
		////////////////////////////////////////////////////////////////
		
		//Not used so far
		
		/*public static function TOOLTIP_LAYOUT_CUSTOM_THUMB(handler:Function):TooltipVo
		{
			var tooltip : TooltipVo = new TooltipVo();
			tooltip.type = TYPE_BUTTON_DELETE;
			tooltip.arrowType = ARROW_TYPE_HORIZONTAL;
			tooltip.bgColor = Colors.BLUE;
			tooltip.autoHide = 3;
			tooltip.interactive = true;
			tooltip.persistOnKill = true;
			
			var content : Array = new Array();
			content.push({type:"text", value:ResourcesManager.getString("tooltip.customlayout.text1")});
			content.push({type:"buttonDelete", value:"common.delete", handler:handler});
			content.push({type:"buttonDelete", value:"common.delete", handler:handler});
			content.push({type:"buttonDelete", value:"common.delete", handler:handler});
			tooltip.content = content;
			
			return tooltip;
			
			
		}*/
		
		public function get additionalUntranslatedContent():String
		{
			return _additionalUntranslatedContent;
		}

		public function set additionalUntranslatedContent(value:String):void
		{
			_additionalUntranslatedContent = value;
		}

		public static function TOOLTIP_QR_CODE():TooltipVo
		{
			var tooltip : TooltipVo = new TooltipVo();
			tooltip.type = TYPE_IMAGE_PREVIEW;
			tooltip.arrowType = ARROW_TYPE_VERTICAL;
			tooltip.bgColor = Colors.GREEN;
			//tooltip.label = ""+photoVo.name;
			var content : Array = new Array();
			content.push({type:"text", value:"tooltip.qr.code.title"});
			content.push({type:"text", value:"tooltip.qr.code.desc"});
			content.push({type:"image", value:"assets/images/common/qr_code_example.png", width:100, height:100});
			tooltip.content = content;
			
			return tooltip;
		}
		
		public static function TOOLTIP_IMAGE_PREVIEW(photoVo:PhotoVo, inToolbar:Boolean = false ):TooltipVo
		{
			var tooltip : TooltipVo = new TooltipVo();
			tooltip.type = TYPE_IMAGE_PREVIEW;
			tooltip.maxWidth = 300;
			tooltip.arrowType = ARROW_TYPE_HORIZONTAL;
			tooltip.bgColor = Colors.BLUE;
			tooltip.bgAlpha = .95;
			tooltip.isLocal = Infos.IS_DESKTOP;
			
			var date:Date = new Date(photoVo.originalDate);
			var dateStr:String = DateUtil.toPreciseDateString(date);
			
			var content : Array = new Array();
			
			if(!inToolbar)
				content.push({type:"text", value:"tooltip.leftTab.photoItem.doubleclick.toAdd"});
			content.push({type:"image", value:photoVo.normalUrl, width:photoVo.width, height:photoVo.height, rotation:photoVo.exifRot});
			content.push({type:"text", value:["tooltip.image.info.name"," :\n"+photoVo.name+" "]});
			
			// display photomanager folder info only for online version
			if(!Infos.IS_DESKTOP) content.push({type:"text", value:["tooltip.image.info.folder"," :\n"+ FolderManager.instance.getFolderDisplayName( photoVo.folderName )]});
			
			content.push({type:"text", value:["tooltip.image.info.size"," :\n"+ photoVo.width+"px X "+photoVo.height + "px "]});
			content.push({type:"text", value:["tooltip.photo.info.creationDate"," :\n"+ dateStr]});
			if(photoVo.originUrl) content.push({type:"text", value:["tooltip.photo.info.origin"," :\n"+ photoVo.originUrl]});
			tooltip.content = content;
						
			return tooltip;
		}
		
		
		public static function TOOLTIP_UPLOAD_COMPLETE(photoVo : PhotoVo, handler:Function):TooltipVo
		{
			var tooltip : TooltipVo = new TooltipVo();
			tooltip.type = TYPE_BUTTON;
			tooltip.autoHide = 3;
			tooltip.interactive = true;
			tooltip.persistOnKill = false; // avoid being killed when killalltooltips is called
			tooltip.arrowType = ARROW_TYPE_VERTICAL;
			tooltip.bgColor = Colors.BLUE;
			
			//tooltip.label = ""+photoVo.name;
			var content : Array = new Array();
			content.push({type:"text", value:["tooltip.upload.notify.complete.text","Photo '"+photoVo.name+"'"]});
			content.push({type:"button", value:"tooltip.upload.notify.complete.button", handler:handler});
			tooltip.content = content;
			
			return tooltip;
		}
		
		public static function TOOLTIP_SETTINGS():TooltipVo
		{
			var tooltip : TooltipVo = new TooltipVo();
			var btnWidth: int = 240;
			tooltip.type = TYPE_BUTTON;
			tooltip.margin = 20;
			tooltip.autoHide = 5;
			tooltip.interactive = true;
			tooltip.persistOnKill = true; // avoid being killed when killalltooltips is called
			tooltip.arrowType = ARROW_TYPE_VERTICAL;
			tooltip.bgColor = Colors.BLUE_LIGHT;
			tooltip.textColor = Colors.BLUE;
			tooltip.uid = TOOLTIP_SETTINGS_UID;
			var content : Array = new Array();
			content.push({type:"button", value:"btn.contact.us", fixWidth:btnWidth, handler:function():void
			{
				DefaultTooltip.killTooltipById(TOOLTIP_SETTINGS_UID);
				PopupContact.OPEN();
				
			}, iconIndex:8});
			
			//Tutorial btn
			if(Infos.currentLangVo.videoTutorialUrl != "")
			{
				content.push({type:"spacer", spacerHeight:10});
				content.push({type:"button", value:"settings.btn.go.to.tutorial.label", fixWidth:btnWidth, handler:function():void
				{
					var url:String = Infos.currentLangVo.videoTutorialUrl;
					navigateToURL(new URLRequest(url),"_blank");
					
				}, iconIndex:10});
			}
			
			//FAQ btn
			if(Infos.currentLangVo.faqUrl != "")
			{
				content.push({type:"spacer", spacerHeight:10});
				content.push({type:"button", value:"settings.btn.go.to.faq.label", fixWidth:btnWidth, handler:function():void
				{
					var url:String = Infos.currentLangVo.faqUrl;
					navigateToURL(new URLRequest(url),"_blank");
					
				}, iconIndex:11});
			}
			

			//Language btns
			content.push({type:"spacer", spacerHeight:10});
			content.push({type:"text", value:["tooltip.settings.label.change.language"]});
			content.push({type:"spacer", spacerHeight:5});
			for (var i:int = 0; i < Infos.config.languages.length; i++) 
			{
				var langVo:LangVo = Infos.config.languages[i];
				var id:String = langVo.id;
				content.push({type:"button", value:id, fixWidth:btnWidth, handler:ResourcesManager.changeLang,handlerParam:id});
			}
			
			
			
			//set content			
			tooltip.content = content;
			return tooltip;
		}
		
		
		public static function TOOLTIP_TEXT_LIMIT():TooltipVo
		{
			var tooltip : TooltipVo = new TooltipVo();
			tooltip.type = TYPE_SIMPLE;
			tooltip.interactive = false;
			tooltip.persistOnKill = true; // avoid being killed when killalltooltips is called
			tooltip.arrowType = ARROW_TYPE_VERTICAL;
			tooltip.bgColor = Colors.RED;
			tooltip.labelKey = "tooltip.text.limit";
			return tooltip;
		}
		
		public function toString():String
		{
			var t : String = "TooltipVo type:"+type ;
			if(type != TYPE_IMAGE_PREVIEW) t += " labelKey:"+labelKey;
			return t;
		}
		

		
	}
}