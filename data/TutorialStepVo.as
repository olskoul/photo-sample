package data
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	import manager.TutorialManager;

	public class TutorialStepVo 
	{
		public static const EDITOR_BOTH:String = "both";
		public static const EDITOR_ONLINE:String = "online";
		public static const EDITOR_OFFLINE:String = "offline";
		
		public var titleKey : String;
		public var descriptionKey : String;
		public var target : DisplayObject;
		public var arrow : String = TutorialManager.ARROW_UP;
		public var key : String;
		public var pointToAim : Point;
		public var editorType:String;
		
		
		public function TutorialStepVo($target:DisplayObject, $key:String, $arrow:String, $pointToAim:Point, $editorType:String = EDITOR_BOTH):void
		{
			titleKey = "tutorial.step."+$key+".title";
			descriptionKey = "tutorial.step."+$key+".description";
			target = $target;	
			key = $key;	
			arrow = $arrow;	
			pointToAim = $pointToAim;	
			editorType = $editorType;
		}
		
		
		
	}
}