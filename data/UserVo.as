package data
{
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	import utils.Debug;

	public class UserVo
	{
		public static const GENDER_MAN : String = "man";
		public static const GENDER_WOMAN : String = "woman";
		
		// user info
		public var id:String = "";
		public var gender:String = GENDER_MAN ;
		public var firstname:String = "";
		public var lastname:String = "";
		public var email:String = "";
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		public function UserVo():void{};
		
		/////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		

		
	}
}