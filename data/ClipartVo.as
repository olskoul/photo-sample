package data
{

	public class ClipartVo extends AssetVo
	{
		
		public function ClipartVo()
		{
			super();
			
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		/**
		 * offline url to the thumb photo version
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		override public function get offlineThumbUrl():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsCommonPath + "/clipart"+"/"+id_tn+".png";	
			}
			return null;
		}
		
		/**
		 * offline url to the fullsize photo version
		 * > overriden by background Vo, clipart vo, overlay vo 
		 */
		override public function get offlineHighUrl():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsCommonPath + "/clipart"+"/"+id_hd+".png";	
			}
			return null;
		 
		}
		
		override public function get tnUrl():String{ 
			
			var onlinePath:String = Infos.config.serverNormalUrl + "/flex/loadcliparts.php?image="+id_tn;
			return urlToReturn(onlinePath,offlineThumbUrl);
		};
		
		override public function get hdUrl():String{ 
			
			var onlinePath:String = Infos.config.serverNormalUrl + "/flex/loadcliparts.php?image="+id_hd;
			return urlToReturn(onlinePath,offlineHighUrl);
		};
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * FIll Vo
		 */
		override public function fill(obj:Object):void
		{
			super.fill(obj);
			
			/*
			Retro compatibility
			id_tn_clipart was replaced by id_tn (since new assets system)
			id_tn could end up null
			if id_tn is null after the fill function, try to fill it with id_tn_clipart
			**/
			if(id_tn == null)
				id_tn = obj.id_tn_clipart;
		}
	}
}