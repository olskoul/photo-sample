/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen
 YEAR			: 2013
 ****************************************************************/
package manager
{

	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import be.antho.data.ResourcesManager;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	import data.FrameVo;
	import data.Infos;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ProjectVo;
	
	
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Debug;
	
	import view.edition.EditionArea;
	import view.edition.FrameBackground;
	import view.edition.PageArea;
	import view.popup.PopupAbstract;
	
	CONFIG::offline{
		import offline.manager.OfflineAssetsManager;
		import offline.manager.ZIPManager;
		import be.antho.air.FileUtils;
		import flash.filesystem.File;
	}
	
	
	/**
	 * Backgrounds manager
	 */
	public class BackgroundsManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		public static const BACKGROUNDS_REFRESH_COMPLETE:Signal = new Signal();
		
		public var availableBkgIds:Vector.<String>;	// list of bkg Ids available for this project
		public var availableCoverBkgIds:Vector.<String>;	// list of bkg Ids available for this project
		public var availableCategoriesIds:Vector.<String>;	// list of categories Ids available for this project
		public var availableCoverCategoriesIds:Vector.<String>;	// list of categories Ids available for this project covers
		
		public var customBackgroundsList:Vector.<BackgroundCustomVo>;	// list of custom backgrounds for the current type of project
		public var customBackgroundsCoverList:Vector.<BackgroundCustomVo>;	// list of custom cover backgrounds for the current type of project
		
		private var initiated:Boolean = false;
		private var projectPrefix:String;
		private var thumbBMD:BitmapData;
		
		//Offline
		private var _showDownloadMoreBkgBtn:Boolean = false;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function BackgroundsManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("LayoutManager is a singleton, use instance"); 
			/*if(!initiated)
				reset();*/
		}
		
		/**
		 * instance
		 */
		private static var _instance:BackgroundsManager;


		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		public static function get instance():BackgroundsManager
		{
			if (!_instance) _instance = new BackgroundsManager(new SingletonEnforcer())
			return _instance;
		}
		
		public function get showDownloadMoreBtn():Boolean
		{
			//not used anymore
			return false; 
			
			
			/*CONFIG::online
			{
				return false;
			}
			
			CONFIG::offline
			{
				if(Infos.isCanvas)
					return false;
				
				return !OfflineAssetsManager.instance.checkIfDownloaded(OfflineAssetsManager.BACKGROUNDS);
			}*/
		}
		
		/**
		 * Get local xml path
		 */
		public function get xmlPath():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsPath + Infos.session.classname + "/backgrounds.xml";
			}
			return null;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Reset manager
		 * Set initiated to false to force init
		 */
		public function reset():void
		{
		
			if(Infos.IS_DESKTOP)
				refresh();
			else			
				init();
		}
		
		/**
		 * Refresh offline only
		 * Reload XML and reset Manager
		 */
		public function refresh():void
		{
			CONFIG::offline
				{
					
					Debug.log("BackgroundManager.refresh");
					var file:File = new File(xmlPath); 
					if(file.exists)
					{
						Debug.log("BackgroundManager.refresh > new xml exist");
						
						OfflineAssetsManager.instance.getXMLLocally(xmlPath, 
							function(result:Object):void
							{
								Debug.log("BackgroundManager.refresh > new xml exist");
								var freshXML:XML = new XML(result.toString());
								ProjectManager.instance.backgroundsXML = freshXML;
								init();
							}, 
							function(e:*):void
							{
								Debug.log("BackgroundManager.refresh failed");
							});
					}
				}
		}
		
		/**
		 * Update XML with freshly downloaded one
		 * For offline : Replace local XML and reset Manager
		 */
		public function replaceLocalXML(freshXML:XML, onComplete:Function):void
		{
			Debug.log("BackgroundsManager.replaceLocalXML");
			CONFIG::offline
			{
				OfflineAssetsManager.instance.saveXMLLocally(freshXML,xmlPath, 
					// ON success
					function(xmlString:String):void
					{
						BackgroundsManager.instance.reset();
						onComplete.call();
					});
			}
			
			CONFIG::online
			{
				ProjectManager.instance.backgroundsXML = freshXML;
				BackgroundsManager.instance.reset();
				onComplete.call();
			}
		}
		
		
		public function init():void
		{
			if(ProjectManager.instance.backgroundsXML == null || ProjectManager.instance.project == null)
				return;
			
			availableBkgIds = new Vector.<String>();	
			availableCoverBkgIds = new Vector.<String>();	
			availableCategoriesIds = new Vector.<String>();	
			availableCoverCategoriesIds = new Vector.<String>();				
			customBackgroundsList = new Vector.<BackgroundCustomVo>();	
			customBackgroundsCoverList = new Vector.<BackgroundCustomVo>();
			
			projectPrefix = ProjectManager.instance.project.docPrefix; // S, M, L, QA etc...
			
			// background documents are set with XX parameters for some project
			if(Infos.isAlbum || Infos.isCards || Infos.isCanvas ) projectPrefix = projectPrefix+"XX";
			
			//Available Bkg
			var bkgXML : XML = ProjectManager.instance.backgroundsXML;
			var node : XML = bkgXML.document.(@name == projectPrefix)[0];
			if(node){
				var availableBkgList:XMLList = node.background.@name;
				for (var j:int = 0; j < availableBkgList.length(); j++) 
					availableBkgIds.push(availableBkgList[j]);	
			}
			
			
			//Covers Bkg
			if(Infos.isAlbum)
			{
				var availableCoverBkgList:XMLList = bkgXML.document.(@name == "C"+projectPrefix)[0].background.@name;
				for (j = 0; j < availableCoverBkgList.length(); j++) 
				{
					availableCoverBkgIds.push(availableCoverBkgList[j]);
				}
			}
			
			//Categories 
			var seen:Object={}
			if(node)
			{
				var availableCategoriesList:XMLList = node..@category.(!seen[valueOf()]&&(seen[valueOf()]=true));
				for (j = 0; j < availableCategoriesList.length(); j++) 
				{
					availableCategoriesIds.push(availableCategoriesList[j]);
				}	
			}
			
			//Categories  overs
			seen ={}
			node = bkgXML.document.(@name == "C"+projectPrefix)[0];
			if(node)
			{
				availableCategoriesList = node..@category.(!seen[valueOf()]&&(seen[valueOf()]=true));
				for (j = 0; j < availableCategoriesList.length(); j++) 
				{
					availableCoverCategoriesIds.push(availableCategoriesList[j]);
				}
			}
			
			//custom Backgrounds
			refreshCustomBackgroundList();
						
			//flag
			initiated = true;
						
			if(Infos.IS_DESKTOP)
			{
				Infos.INTERNET_CHANGED.remove(reset);
				Infos.INTERNET_CHANGED.add(reset);
			}
		}
		
		
		/**
		 *  creates default background frame vo
		 */
		public function createBackgroundFrameVo(isCover:Boolean, isPostCardBack:Boolean = false):FrameVo
		{
			var frameVo:FrameVo = new FrameVo();
			frameVo.type = FrameVo.TYPE_BKG;
			
			var backgroundBounds:Rectangle = (!isCover)? getPageBackgroundBounds() : getCoverBackgroundBounds();
			frameVo.width = backgroundBounds.width;
			frameVo.height = backgroundBounds.height;
			frameVo.x = backgroundBounds.x;
			frameVo.y = backgroundBounds.y;
			frameVo.editable = true;
			
			return frameVo;
		}
		
		/**
		 * retrieve the page background bounds (it has specific bleed of 10mm) and take the spine into calculation
		 */
		public function getPageBackgroundBounds():Rectangle
		{
			var bounds : Rectangle = new Rectangle;
			var pageBleed : Number = (Infos.isCanvas)? CanvasManager.instance.edgeSize : MeasureManager.millimeterToPixel(4);
			bounds.width =  MeasureManager.PFLToPixel(Infos.project.width) + pageBleed *2; // * 2 because bleed is on left and right
			bounds.height =  MeasureManager.PFLToPixel(Infos.project.height) + pageBleed *2; // * 2 because bleed is on left and right
			bounds.x =  MeasureManager.PFLToPixel(Infos.project.width *.5);
			bounds.y = MeasureManager.PFLToPixel(Infos.project.height *.5);
			return bounds;
		}
		
		/**
		 * retrieve the cover background bounds (it has specific bleed of 10mm) and take the spine into calculation
		 */
		public function getCoverBackgroundBounds():Rectangle
		{
			var bounds : Rectangle = new Rectangle;
			var coverBounds:Rectangle = Infos.project.getCoverBounds();
			var coverBleedMM : Number = (Infos.project.type == ProductsCatalogue.ALBUM_CASUAL || Infos.project.type == ProductsCatalogue.ALBUM_TRENDY )? 4 : 10;
			bounds.width = coverBounds.width + MeasureManager.millimeterToPixel(coverBleedMM)*2; // * 2 because bleed is on left and right
			bounds.height = coverBounds.height + MeasureManager.millimeterToPixel(coverBleedMM)*2; // * 2 because bleed is on left and right
			bounds.x = coverBounds.width*.5;//frameVo.width/2;
			bounds.y = coverBounds.height*.5;//frameVo.height/2;
			return bounds;
		}
		
		/**
		 * Delete custom background
		 */
		public function deleteCustomBackground(bkgId:String):void
		{
			Debug.log("BackgroundManager > delete custom bkg");
			
			if(!Infos.IS_DESKTOP)
			{
				ServiceManager.instance.deleteCustomBackground(bkgId,
					function(result:String):void
					{
						Debug.log("BackgroundManager > delete custom bkg > Success");
						Debug.log("Result: "+result);
						
						var xml:XML = XML(result);
						if(xml.result.status == "success")
						{
							ProjectManager.instance.customBackgroundsXML = xml;
							refreshCustomBackgroundList();
							PopupAbstract.Alert(ResourcesManager.getString("popup.custombackground.title"), ResourcesManager.getString("loading.custombackground.deleted"),false,null,null);
						}
						else
						{
							PopupAbstract.Alert(ResourcesManager.getString("popup.custombackground.title"), ResourcesManager.getString("popup.custombackground.abort.deletion"),false,null,null);	
						}
						//Success function
						
					},function(msg:String):void
					{
						Debug.log("BackgroundManager > delete custom bkg > failed: "+msg);
						//failed function
						PopupAbstract.Alert(ResourcesManager.getString("popup.custombackground.title"), ResourcesManager.getString("popup.custombackground.abort.deletion"),false,null,null);	
					});
			}
			else
			{
				CONFIG::offline{
					var xml:XML = ProjectManager.instance.customBackgroundsXML;
					delete xml.customBackground.(@id == bkgId)[0];
					var custombkgFile:File = new File(Infos.offlineCustomBackgroundsXML);
					FileUtils.writeTextFile(ProjectManager.instance.customBackgroundsXML.toString(), custombkgFile);
					refreshCustomBackgroundList();
					
				}
				
			}
			
		}
		
		/**
		 * retrieve the cover background bounds (it has specific bleed of 10mm) and take the spine into calculation
		 */
		public function saveCurrentBackground():void
		{
			Debug.log("BackgroundManager > saveCurrentBackground");
			
			var pageArea: PageArea = EditionArea.getCurrentPageAreaFromPageVo(PagesManager.instance.currentEditedPage);
			if(pageArea)
			{
				//get the background of this page
				var background:FrameBackground = pageArea.getBackgroundFrame();
				if(background)
				{
					//if there is a background and it has a photoVo, cool let's save it
					if(background.frameVo.photoVo != null)
					{
						startSavingBackgroundProcess(background,PagesManager.instance.currentEditedPage.isCover);
					}
					else
					abortSavingCurrentBackground("BackgroundManager > saveCurrentBkg: No frameVo found.");	
				}
				else
				abortSavingCurrentBackground("BackgroundManager > saveCurrentBkg: No background frame found.");
			}
			else
			abortSavingCurrentBackground("BackgroundManager > saveCurrentBkg: No page Area found.");
			
		}
		
		/**
		 * Start process of saving current background as custom background
		 */
		private function startSavingBackgroundProcess(frameBackground:FrameBackground, isCover:Boolean):void
		{
			Debug.log("BackgroundManager > startSavingBackgroundProcess");
			var frameVoStr:String = JSON.stringify(frameBackground.frameVo);
			Debug.log(frameVoStr);
			thumbBMD = frameBackground.createThumbBitmapData();
			ServiceManager.instance.uploadCustomBackground(frameVoStr,thumbBMD,isCover,backgroundCustomUploadSuccess, backgroundCustomUploadFailed);
		}		
		
		/**
		 * Uplaod background success handler
		 */
		private function backgroundCustomUploadFailed(errorMessage:String):void
		{
			Debug.log("BackgroundManager.backgroundCustom UploadFailed");
			abortSavingCurrentBackground(errorMessage);		
		}
		
		/**
		 *Uplaod background error handler
		 */
		private function backgroundCustomUploadSuccess(result:String):void
		{
			Debug.log("BackgroundManager.backgroundCustomUploadSuccess");
			ProjectManager.instance.customBackgroundsXML = new XML(result);
			refreshCustomBackgroundList();
		}
		
		/**
		 * retrieve the cover background bounds (it has specific bleed of 10mm) and take the spine into calculation
		 */
		public function downloadMore(popup:PopupAbstract = null):void
		{
			CONFIG::offline{

				Debug.log("BackgroundManager > downloadMore");
				
				if(Infos.INTERNET)
				{
					//Call
					
					var folderName:String = OfflineAssetsManager.instance.getBkgFolderByDocPrefix(Infos.project.docPrefix);
					var url:String = Infos.config.getSettingsValue("OfflineAssetsToDownloadUrl") + "backgrounds_"+Infos.session.classname+"_"+folderName+".zip";
					//var url:String = "http://www.smileasyoudoit.com/customers/tictac/servicetemp/backgrounds_"+Infos.session.classname+"_"+folderName+".zip";	 //TODO: comment this line and use tictac servers
					var destination:String = Infos.offlineAssetsPath + Infos.session.classname + "/backgrounds";
					
					ZIPManager.instance.downloadAndUnzip(url, destination, downloadMoreSuccess, downloadMoreError, false, true);
				}
				else
					PopupAbstract.Alert(ResourcesManager.getString("popup.no.internet.title"), ResourcesManager.getString("popup.no.internet.desc"), false, null );

			}

			
		}
		/**
		 * Download more bkg SUCCESS
		 */
		private function downloadMoreSuccess():void
		{
			CONFIG::offline
			{
				Debug.log("BackgroundManager.downloadMoreSuccess");
				OfflineAssetsManager.instance.markAsDownloaded(OfflineAssetsManager.BACKGROUNDS);
				refresh();
				PopupAbstract.Alert(ResourcesManager.getString("popup.download.ready.title"), ResourcesManager.getString("popup.new.bkg.ready.desc"),false,null,null,null);
			}
		}
		/**
		 * Download more bkg ERROR
		 */
		private function downloadMoreError(msg:String):void
		{
			Debug.log("BackgroundManager.downloadMoreError > Error");
			Debug.log("Abort message: "+msg);
			Debug.warnAndSendMail(msg);
			
			//todo: clean everything
			
			//Show popup
			PopupAbstract.Alert(ResourcesManager.getString("popup.download.failed.title"), ResourcesManager.getString("popup.download.failed.desc"),false,null,null);
		}
		
		
		public function getBackgroundVoById(id:String, isCover:Boolean = false):BackgroundVo
		{
			return createVo(id, isCover);
		}
		
		public function getPostCardBackgroundVo():BackgroundVo
		{
			var vo:BackgroundVo;
			
			vo = new BackgroundVo();
			vo.id = CardsManager.POSTCARD_BKG_ID;
			vo.category = "postcardback";
			vo.id_tn = CardsManager.POSTCARD_BKG_ID;
			vo.proxy = "noproxy";
			vo.id_hd = CardsManager.POSTCARD_BKG_ID;
			vo.width = 876;
			vo.height = 630;
			vo.isCover = false;
			
			return vo;
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * If Saving background fails for any reason
		 * calling this function handles it smoothly
		 * */
		private function abortSavingCurrentBackground($message:String):void
		{
			Debug.log("BackgroundsTab > Aborting saveCurrentBkg");
			Debug.log("Abort message: "+$message);
			Debug.warnAndSendMail($message);
			//todo: clean everything
			
			if(thumbBMD)
				thumbBMD.dispose();
			thumbBMD = null;
			
			PopupAbstract.Alert(ResourcesManager.getString("popup.custombackground.title"), ResourcesManager.getString("popup.custombackground.abort"),false,null,null);	
		}
		
		/**
		 *  Refresh CustomBackground list
		 */
		private function refreshCustomBackgroundList():void
		{
			var i:int;
			var node:XML;
			var vo:BackgroundCustomVo;
			var list:XMLList
			
			//Custom backgroud for regular pages
			customBackgroundsList = new Vector.<BackgroundCustomVo>();
			list = (ProjectManager.instance.customBackgroundsXML != null)?ProjectManager.instance.customBackgroundsXML..customBackground.(@docType == Infos.project.docPrefix && @isCover == "false"):null;
			if(list != null)
			{
				for (i = 0; i < list.length(); i++) 
				{
					node = list[i];
					vo = new BackgroundCustomVo();
					vo.fillFromXML(node);
					customBackgroundsList.push(vo);
				}
				
			}
			
			//Custom backgroud for regular covers
			customBackgroundsCoverList = new Vector.<BackgroundCustomVo>();
			list = (ProjectManager.instance.customBackgroundsXML) ? ProjectManager.instance.customBackgroundsXML..customBackground.(@docType == Infos.project.docPrefix && @isCover == "true") : null;
			if(list != null)
			{
				for (i = 0; i < list.length(); i++) 
				{
					node = list[i];
					vo = new BackgroundCustomVo();
					vo.fillFromXML(node);
					customBackgroundsCoverList.push(vo);
				}
				
			}
			
			//remapBackgroundCustomList();
			BACKGROUNDS_REFRESH_COMPLETE.dispatch();
			
		}
		
		/**
		 * checks backgroundCustom Vo list 
		 * -> map current project backgriund custom vo with loaded one
		 * -> check photos in album but not existing in image list
		 */
		public function remapBackgroundCustomList():void
		{
			var pageVo : PageVo;
			var frameVo : FrameVo;
			var vo : BackgroundVo;
			var matchingVo : BackgroundVo;
			var project:ProjectVo = ProjectManager.instance.project;
			
			// check photos used in album
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				//ref pageVo
				pageVo = project.pageList[i];
				
				//security
				if(pageVo.layoutVo.frameList.length < 1)
					return;
				
				//Ref Background
				frameVo = pageVo.layoutVo.frameList[0]; //check only backgrounds
				
				if( (frameVo.type == FrameVo.TYPE_BKG && frameVo.photoVo != null && (frameVo.photoVo is BackgroundVo)) )
				{
					matchingVo = findMatchingPhotoVo( frameVo.photoVo as BackgroundVo);
					if(!matchingVo) 
					{
						if(frameVo.type == FrameVo.TYPE_PHOTO) 
						{
							 // there is a background in this project which is not in the background custom list anymore
								Debug.warn("Background.remapBackgroundCustomList : Background Custom with id : "+frameVo.photoVo.id+ " doesn't exist anymore in background Custom list");
								// check if photo is on the right server.. otherwise change 
								ProjectManager.instance.normalizePhotoUrl(frameVo.photoVo);							
						}
					}
					else
					{
						// TODO CHECK IF IT WORKS? WE REPLACE FRAME PHOTOVO BY THE ONE JUST DOWNLOADED
						frameVo.photoVo = matchingVo;
					}
				}	
			}
		}
		
		/**
		 * find matching frame vo in image list
		 */
		private function findMatchingPhotoVo( photoVo :BackgroundVo ):BackgroundVo
		{
			/*var tempPhotoVo : BackgroundCustomVo;
			for (var i:int = 0; i < customBackgroundsList.length; i++) 
			{
				tempPhotoVo = customBackgroundsList[i];
				if(tempPhotoVo.id == photoVo.id) 
				{
					return tempPhotoVo;
				}
				
			}*/
			return null;
		}
		
		/**
		 * Create a Bkg Vo based on a ID
		 * Look for it in the XML 
		 * */
		private function createVo(id:String, isCover:Boolean = false):BackgroundVo
		{
			//var projectPrefix:String = ProjectManager.instance.project.docPrefix + "XX";
			var ids:Vector.<String> = (isCover)?availableCoverBkgIds:availableBkgIds;
			var xml:XML = ProjectManager.instance.backgroundsXML.document.(@name == projectPrefix)[0];
			var node:XML;
			var vo:BackgroundVo;
			
			if(ids.lastIndexOf(id) != -1)
			{
				//Xml
				xml = (!isCover)?xml:ProjectManager.instance.backgroundsXML.document.(@name == "C"+projectPrefix)[0];
				//Get layout node
				node = xml.background.(@name == id)[0];
				//vo
				vo = new BackgroundVo();
				vo.id = node.@name;
				vo.category = node.@category;
				vo.id_tn = String(String(node.@tnproxy).split(".jpg")[0]).substring(12);
				vo.proxy = String(node.@proxy);
				vo.id_hd = String(String(node.@proxy).split(".jpg")[0]).substring(12);
				vo.width = MeasureManager.PFLToPixel(Number(node.@width));
				vo.height = MeasureManager.PFLToPixel(Number(node.@height));
				vo.isCover = isCover;
				
				return vo;
			}
			
			return null;
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
class SingletonEnforcer{}