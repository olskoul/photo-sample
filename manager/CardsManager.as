/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import data.Infos;
	
	import utils.Debug;

	
	
	/**
	 * Cards manager
	 */
	public class CardsManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		public static const POSTCARD_BKG_ID:String = "pcardt1/online/backpostcard";
			
		private var initiated:Boolean = false;
		
		/**
		 * instance
		 */
		private static var _instance:CardsManager;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function CardsManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("CardsManager is a singleton, use instance"); 
			if(!initiated)
				init();
		}
		
		public static function get instance():CardsManager
		{
			if (!_instance) _instance = new CardsManager(new SingletonEnforcer())
			return _instance;
		}
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////

		
		/**
		 * Return the amount of card repetition for this project
		 * > 1x 10 = 1 repetition
		 * > 5x 2 = 5 repetitions 
		 * etc...
		 */
		public function get cardsRepeatValue():int
		{
			/* EXAMPLE
			<models>
			<node id="01" label="10 x 1" />
			<node id="02" label="5 x 2"/>
			<node id="05" label="2 x 5" />
			<node id="10" label="1 x 10"/>
			<node id="00" label="1 x 1"/>
			</models>
			*/
			var repetitionId : String = "" + Infos.project.docCode.slice(Infos.project.docCode.length-2, Infos.project.docCode.length); 
			switch (repetitionId){
				case "01" :
					return 10;
					break;
				case "02" :
					return 5;
					break;
				case "05" :
					return 2;
					break;
				case "10" :
					return 1;
					break;
				case "00" :
					return 1;
					break;
			}
			return 1;
		}
		
		
		/**
		 * CARDS
		 * Get number of page to draw
		 **/
		public function getNumCardsPagePerDraw():int
		{
			switch (Infos.project.docPrefix)
			{
				case "PC":
				case "PX":
				case "ACP":
				case "ACL":
				case "AXP":
				case "AXL":
				case "AUP":
				case "AUL":
				case "AQP":
					return 2;
				case "FCP":
				case "FCL":
				case "GCP":
				case "GCL":
				case "FUP":
				case "FUL":
				case "FQP":
					return 4;
					
			}
			return 0;
		}
		
		
		/**
		 * returns the amount of pages per unit
		 * by multiplying this by num repeat we got the amount of pages for this project
		 */
		public function get pagesPerUnit():int
		{
			/*
			<code2 docidnum="3002" docidstr="PC02">
				<docPrefixStr value="PC"/>
				<docPages value="4"/>
				<docMeasure value="2.834691"/>
				<docResolution value="144.0"/>
				<docCutBorder value="12.5"/>
				<docEdgeOffset value="-3"/>
				<docClass value="cards"/>
				<docMenuName value="types_postcard"/>
				<pagesPerUnit value="2"/>
				<pagesPerJump value="2"/>
				<docOrientation value="landscape"/>
				<coverManifestName value="0"/>
			  </code2>
			*/
			var projectNode : XML = ProjectManager.instance.projectsXML.projects.children().(@docidstr==Infos.project.docCode)[0];
			var _pagesPerUnit : int = parseInt(projectNode.pagesPerUnit.@value);
			if(isNaN(_pagesPerUnit)) {
				Debug.warn("CardsManager > pagesPerUnit not found... put it back to 1 to avoid crash");
				_pagesPerUnit = 1;
			}
			return _pagesPerUnit;
		}
		
		/**
		 * retrieve project orientation
		 */
		public function get docOrientation():String
		{
			/*
			<code2 docidnum="3002" docidstr="PC02">
				<docPrefixStr value="PC"/>
				<docPages value="4"/>
				<docMeasure value="2.834691"/>
				<docResolution value="144.0"/>
				<docCutBorder value="12.5"/>
				<docEdgeOffset value="-3"/>
				<docClass value="cards"/>
				<docMenuName value="types_postcard"/>
				<pagesPerUnit value="2"/>
				<pagesPerJump value="2"/>
				<docOrientation value="landscape"/>
				<coverManifestName value="0"/>
			</code2>
			*/
			var projectNode : XML = ProjectManager.instance.projectsXML.projects.children().(@docidstr==Infos.project.docCode)[0];
			var orientation : String = projectNode.docOrientation.@value;
			return ""+ orientation;
		}
		
		
		/**
		 * retrieve cards model by doccode
		 */
		public function get cardsModel():String
		{
			/* EXAMPLE
			<models>
			<node id="01" label="10 x 1" />
			<node id="02" label="5 x 2"/>
			<node id="05" label="2 x 5" />
			<node id="10" label="1 x 10"/>
			<node id="00" label="1 x 1"/>
			</models>
			*/
			var repetitionId : String = "" + Infos.project.docCode.slice(Infos.project.docCode.length-2, Infos.project.docCode.length); 
			switch (repetitionId){
				case "01" :
					return "10 x 1";
					break;
				case "02" :
					return "5 x 2";
					break;
				case "05" :
					return "2 x 5";
					break;
				case "10" :
					return "1 x 10";
					break;
				case "00" :
					return "1 x 1";
					break;
			}
			return "1 x 1";
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		public function init():void
		{
			
			//flag
			initiated = true;
		}
		
		
		
		
	}
}
class SingletonEnforcer{}