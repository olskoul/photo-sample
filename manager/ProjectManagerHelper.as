package manager
{
	import be.antho.data.ResourcesManager;
	
	import data.FrameVo;
	import data.Infos;
	import data.PageVo;
	import data.ProjectVo;
	
	import utils.Debug;
	import utils.VectorUtils;
	
	import view.edition.Frame;
	import view.edition.FrameCalendar;

	public class ProjectManagerHelper
	{
		public var project:ProjectVo;
		private var linkages:Object;
		
		public function ProjectManagerHelper()
		{
			project = ProjectManager.instance.project;
		}
		
		

		
		/**
		 * Generate a uniq ID to link frames together
		 * */
		public function getUniqFrameLinkageID():String
		{
			return PagesManager.instance.getUniqFrameLinkageID();
		}
		
		/**
		 * Generate a uniq ID to link frames together
		 * */
		/*
		public function linkCalendarFrame(frame:Frame):void
		{
			if(linkages == null)
				linkages = {};
			
			var uniqId:String = frame.frameVo.uniqFrameLinkageID;
			if(!linkages[uniqId])
				linkages[uniqId] = frame;
			else
			{
				if(frame is FrameCalendar)
					FrameCalendar(frame).behindFrame = linkages[uniqId];
				else if(linkages[uniqId] is FrameCalendar)
					FrameCalendar(linkages[uniqId]).behindFrame = frame;
				else
					Debug.warn("Project manager Helper > linkFrame : At least one of the two frames must be a FrameCalendar");
					
			}
		}
		*/
		
		/**
		 * Move pages at other position
		 * -> return true if update was done correctly
		 **/
		public function movePages(pageIndexToMove:int, numPages:int, newPageIndex:int):Boolean
		{
			Debug.log("ProjectManagerHelper.movePages : Move pages with index : " + pageIndexToMove + " >> to >> "+newPageIndex )
			var str:String = "Indexes : ";
			for (var j:int = 0; j < Infos.project.pageList.length; j++) 
			{
				str += project.pageList[j].index + " - "
			}
			//trace(str);
			//trace("///////////////////////////////////");
			
			
			// be sure current page list is sorted on indexes
			sortPagesOnIndexes();
			
			// modify page list elements
			var moveUp : Boolean = (pageIndexToMove < newPageIndex)? true : false;
			
			// remove elements to move
			var moved:Vector.<PageVo> = project.pageList.splice(pageIndexToMove, numPages);
			
			// reinject elements at correct place
			if(moveUp) VectorUtils.addVectorElements(project.pageList, moved,newPageIndex-numPages);
			else VectorUtils.addVectorElements(project.pageList, moved,newPageIndex);
			
			// array has now elements at correct position, update their index in vo's
			var pageVo : PageVo;
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				pageVo = project.pageList[i];
				pageVo.index = i;
			}
			
			// sort pagelist (not necessary, this should already be done by last lines)
			//sortPagesOnIndexes();
			
			return true;
		}
		
		
		/**
		 * sort pages on indexes
		 **/
		public function sortPagesOnIndexes():void
		{
			VectorUtils.sortOn(project.pageList,"index",Array.NUMERIC);
		}
		
		
		
		/**
		 * CALENDARS
		 * Get number of page to draw
		 **/
		public function getNumCalendarsPagePerDraw():int
		{
			switch (Infos.project.docCode)
			{
				case "WCAL3":
				case "WCAL2":
					return 2;					
			}
			return 1;
		}
		
		
		
		
		public function getProjectDetailByDocCode(classname:String,docCode:String):String
		{
			// ALBUM DETAIL
			var detailString:String = "";
			if( Infos.isAlbum ){
				detailString += ResourcesManager.getString("class."+Infos.project.classname);
				detailString += " - " + ResourcesManager.getString("album.prefix."+Infos.project.docPrefix);
				detailString += " " + (Infos.project.pageList.length-1) +"p";
			}
				// CALENDAR
			else if( Infos.isCalendar ){
				detailString += ResourcesManager.getString("class."+Infos.project.classname);
				//detailString += ResourcesManager.getString("calendar.prefix."+Infos.project.docPrefix);
				//detailString += "\n" +  (Infos.project.pageList.length-1) +" "+ ResourcesManager.getString("common.pages");
			}
				// CANVAS
			else if( Infos.isCanvas ){
				detailString += ResourcesManager.getString("class."+Infos.project.classname);
				//detailString += ResourcesManager.getString("canvas.type."+Infos.project.canvasType);
				//detailString += "\n" +  ResourcesManager.getString("order.panel.detail.format") + ": " + ResourcesManager.getString("canvas.orientation."+Infos.project.type) ;
				//var w : Number = Math.round(MeasureManager.PFLToCM(Infos.project.width));
				//var h : Number = Math.round(MeasureManager.PFLToCM(Infos.project.height));
				//detailString += "\n" + w + "cm X " + h + "cm";
				//if(CanvasManager.instance.isMultiple) detailString += "\n(" + Infos.project.canvasMLayout + ")";
			}
				
				// CARDS
			else if( Infos.isCards ){
				detailString += ResourcesManager.getString("class."+Infos.project.classname);
				//var orientation : String = (Infos.project.type != "types_fc_port_sq" && Infos.project.type != "types_announcecard_sq")? "\n"+ResourcesManager.getString("cards.orientation."+CardsManager.instance.docOrientation) : "";
				//var model : String = CardsManager.instance.cardsModel;
				//detailString += ResourcesManager.getString("cards.type."+Infos.project.type) + " " + orientation;
				//detailString += "\n" + ResourcesManager.getString("cards.configurator.model.label") + " : " + model;
			}
			return detailString;
		}
	}
}