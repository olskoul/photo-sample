/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2014
 ****************************************************************/
package manager
{
	import data.FrameVo;
	import data.Infos;
	import data.PhotoVo;
	
	import flash.geom.Point;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FrameBackground;
	import view.edition.FrameMultiple;
	import view.edition.FramePhoto;

	/**
	 * Helper for canvas content
	 */
	public class CanvasManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		/**
		 * CANVAS TYPES = Material
		 */
		public static const TYPE_CANVAS : String = "canvas_types";
		public static const TYPE_WOOD : String = "wood_types";
		public static const TYPE_POSTER : String = "poster_types";
		public static const TYPE_PLEXI : String = "plexi_types";
		public static const TYPE_KADAPAK : String = "kadapak_types";
		public static const TYPE_ALUMINIUM : String = "aluminium_types";
		public static const TYPE_FOREX : String = "forex_types";
		public static const TYPE_REAL_ALUMINIUM : String = "real_aluminium_types";
		
		private static var modifierMap : Array = [
			{name:TYPE_CANVAS, value:0},
			{name:TYPE_WOOD, value:6900},
			{name:TYPE_PLEXI, value:7000},
			{name:TYPE_KADAPAK, value:7100},
			{name:TYPE_ALUMINIUM, value:7200},
			{name:TYPE_FOREX, value:7300},
			{name:TYPE_POSTER, value:7400},
			{name:TYPE_REAL_ALUMINIUM, value:7500}
		]; 
		
		/* INFOS :
		TYPES
		<key id="canvas.type.canvas_types"><![CDATA[Canvas]]></key>
		<key id="canvas.type.wood_types"><![CDATA[45 mm Canvas]]></key>
		<key id="canvas.type.poster_types"><![CDATA[Poster]]></key>
		<key id="canvas.type.plexi_types"><![CDATA[Plexi]]></key>
		<key id="canvas.type.kadapak_types"><![CDATA[Kadapak]]></key>
		<key id="canvas.type.aluminium_types"><![CDATA[Dibond® (Aluminium)]]></key>
		<key id="canvas.type.forex_types"><![CDATA[Forex®]]></key>
		
		ORIENTATIONS
		<key id="canvas.orientation.types_canvas_port"><![CDATA[Portait]]></key>
		<key id="canvas.orientation.types_canvas_land"><![CDATA[Landscape]]></key>
		<key id="canvas.orientation.types_canvas_square"><![CDATA[Square]]></key>
		<key id="canvas.orientation.types_canvas_freeformat"><![CDATA[Custom Made]]></key>
		<key id="canvas.orientation.types_canvas_multiple"><![CDATA[Multiple]]></key>
		*/
		
		public var multipleFrame : FrameMultiple;
		public var isMultipleFirstAdd:Boolean = true; // is it the first time we inject a frame as multiple or not (used for to display or not the multiple canvas update popup on drag/drop)
		
				
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////

		public function CanvasManager(sf:SingletonEnforcer) 
		{
			if(!sf) {
				throw new Error("CanvasManager is a singleton, use instance");
				return;
			}
			init();
		}
		
		private static var _instance:CanvasManager;

		

		public static function get instance():CanvasManager
		{
			if (!_instance) _instance = new CanvasManager(new SingletonEnforcer())
			return _instance;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////

		/**
		 * get format type (also named orientation in some cases.. but it's not just an orientation, was bad naming)
		 */
		public function get format():String
		{
			if(!Infos.project) return null; 
			return getFormatByPrefix( Infos.project.docPrefix );
		}

		/**
		 * shortcut to know if the current canvas project is a multiple one
		 * if projectVo has a canvasMLayout (multiple layout) we are in a multiple canvas project type
		 */
		public function get isMultiple():Boolean
		{
			return (Infos.project.canvasMLayout != null)? true : false;
		}
		
		public function get isPopart():Boolean
		{
			return (Infos.project.canvasStyle == "styles_style5")? true : false;
		}
		
		public function get isPelemele():Boolean
		{
			return (Infos.project.canvasStyle == "styles_style2")? true : false;
		}
		
		
		
		
		/**
		 * KADAPAK FRAME COLOR
		 */
		public function getKadapakFrameColor(str:String):uint {
			var color:uint = 0;
			switch(str.toLowerCase()) {
				case "black":
					color = 0x000000;
					break;
				case "silver":
					color = 0xA6A6A6;
					break;
				case "gold":
					color = 0xD1B181;
					break;
				case "red":
					color = 0x8B0000;
					break;
				case "blue":
					color = 0x123572;
					break;
			}
			return color | 0xFF000000;
		}
		
		
		/**
		 * retrieve canvas edge in pixel
		 */	
		public function get edgeSize():int
		{
			switch (Infos.project.canvasType) {
				case TYPE_CANVAS : 
					return MeasureManager.millimeterToPixel( 20 ); // 20 mm side
					break;
				case TYPE_WOOD : 
					return MeasureManager.millimeterToPixel( 50 ); // 45 mm side + 5 mm for bend
					break;
					/* TODO : get correct edge for other project types
				case TYPE_POSTER : 
					return MeasureManager.millimeterToPixel( 20 );
				case TYPE_PLEXI : 
					return MeasureManager.millimeterToPixel( 20 );
					*/
				default : 
					return MeasureManager.millimeterToPixel( 1 ); // 5 mm margin
			}
		} 
		
		/**
		 * OLD EDITOR CANVAS MODIFIER
		 * looks like the old way of doing so I just copy this from old editor
		 */	
		public function get CanvasModifier():int
		{
			return getCanvasModifierByCanvasType( Infos.project.canvasType );
			/*
			switch (Infos.project.canvasType) {
				case TYPE_WOOD :
					return 6900 ;
				case TYPE_PLEXI :
					return 7000;
				case TYPE_KADAPAK :
					return 7100;
				case TYPE_ALUMINIUM :
					return 7200;
				case TYPE_FOREX :
					return 7300;
				case TYPE_POSTER :
					return 7400;
				default : 
					return 0;
			}
			*/
		}
		
		/**
		 * OLD EDITOR CANVAS MODIFIER
		 * looks like the old way of doing so I just copy this from old editor
		 */	
		public function getCanvasModifierFromDash(canvasType:String):int
		{
			return getCanvasModifierByCanvasType( canvasType );
		}
		
		/**
		 * get canvas modifier by canvas type
		 */
		public function getCanvasModifierByCanvasType( canvasType : String ):int
		{
			for (var i:int = 0; i < modifierMap.length; i++) 
			{
				if(modifierMap[i].name == canvasType) return modifierMap[i].value;
			}
			return 0;	
		}
		
		/**
		 * get canvas modifier by canvas type
		 */
		public function getCanvasTypeByModifier( modifier : int ):String
		{
			for (var i:int = 0; i < modifierMap.length; i++) 
			{
				if(modifierMap[i].value == modifier) return modifierMap[i].name;
			}
			return TYPE_CANVAS;	
		}
		
		/**
		 * Return the canvas type by modifier product id
		 */
		public function getCanvasTypeByModifiedProductId( productId : Number ):String
		{
			var canvasModifier : int = 0;
			if( productId > 1000 ) // 599 is max product id for canvas right now, be careful if this limit changes
			{
				canvasModifier = (Math.floor(productId/100) - 5) * 100;
			}
			
			return getCanvasTypeByModifier( canvasModifier );
		}
		
		
		/**
		 * Overriding canvas cut border as values from xml are not correct
		 * > return value is in mm
		 */	
		public function get canvasCutBorder():int
		{
			switch (Infos.project.canvasType) {
				case TYPE_CANVAS :
					return 40; 
				case TYPE_WOOD :
					return 70;
				default : 
					return 25;
			}
		}
		
		
		/**
		 * OLD EDITOR CANVAS COLOR SYSTEM
		 THIS NEEDS TO BE CLEAR AS IT'S IMPORTANT FOR canvas
		 */	
		public function get CanvasColor():String
		{
			if(isKadapak)
			{
				return Infos.project.canvasFrameColor;
			}
			return "none";
		}
		
		/**
		 * check if we are in kadapak
		 */	
		public function get isKadapak():Boolean
		{
			if(Infos.isCanvas && Infos.project && Infos.project.canvasType == TYPE_KADAPAK) return true;
			return false;
		}
		
		
		/**
		 * retrieve current gap between pages
		 */	
		public function get gapBetweenPages():int
		{
			if(Infos.project.docPrefix == "CPM")
			{
				var gap : Number = MeasureManager.millimeterToPixel(Infos.project.canvasMargin);
				if(gap == 0) gap = 2; // a thin line to separate canvas is the minimum
				return gap;
			}
			return 0;
		}
		
		/**
		 * return amount of rows for the multiple canvas type
		 */
		public function get multipleRowsCount():int
		{
			if(!Infos.project || !Infos.project.canvasMLayout) return 1; 
			return parseInt(Infos.project.canvasMLayout.split("x")[0]);
		}
		
		/**
		 * return amount of cols for the multiple canvas type
		 */
		public function get multipleColsCount():int
		{
			if(!Infos.project || !Infos.project.canvasMLayout) return 1; 
			return parseInt(Infos.project.canvasMLayout.split("x")[1]);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * initialize canvas manager
		 */
		public function init():void
		{
			
		}
		
		
		/**
		 * retrieve a format by canvas prefix
		 */
		public function getFormatByPrefix( prefix:String ):String
		{
			switch (prefix)
			{
				case "CPP" :
					return "types_canvas_port";
					break;
				case "CPL" :
					return "types_canvas_land";
					break;
				case "CPF" :
					return "types_canvas_freeformat";
					break;
				case "CPS" :
					return "types_canvas_square";
					break;
				case "CPM" :
					return "types_canvas_multiple";
					break;
			}
			
			return null;
		}
		
		
		/**
		 * get amount of pages for a multiple canvas 
		 */
		public function getMultiplePageAmount():Number
		{
			if(!Infos.project || !Infos.project.canvasMLayout) return -1; 
			var numRow : int = parseInt(Infos.project.canvasMLayout.split("x")[0]);
			var numCols : int = parseInt(Infos.project.canvasMLayout.split("x")[1]);
			return numRow * numCols;
		}
		
		
		
		
		
		/**
		 * get page position by index
		 * as canvas are only one page for non "CPM" 
		 */
		public function getPagePosition( pageIndex : int, isPreview : Boolean = false):Point
		{
			if(!Infos.project || !Infos.project.canvasMLayout) return new Point(); 
			var numRow : int = parseInt(Infos.project.canvasMLayout.split("x")[0]);
			var numCols : int = parseInt(Infos.project.canvasMLayout.split("x")[1]);
			
			var margin : Number = gapBetweenPages;
			var edgeValue : Number = (isPreview)? 1 : edgeSize;
			var posX : Number = ((pageIndex+1) % numCols) * (Infos.project.getPageBounds().width + edgeValue*2 + margin) ;
			var posY : Number = Math.floor((pageIndex+1)/ numCols) * (Infos.project.getPageBounds().height + edgeValue*2 +  margin) ;
			return new Point(posX, posY);
		}
		
		/**
		 * Layouts for canvas are weird as they must fit to canvas edge but the info is not well documented in xml
		 * So we change it at source to make it fit current project properties
		 * Frames having 20 as offset should be updated to fit current project edge/bleed
		 */
		public function modifyCanvasLayoutXMLNode( _layoutNode : XML, layoutScaleX : Number, layoutScaleY : Number ):XML
		{
			// make a copy to avoid modify original xml ! (it was an issue for upgrades)
			var layoutNode : XML = new XML(_layoutNode.toString());
			
			var layoutWidth : Number = parseFloat(layoutNode.@pagewidth);
			var layoutHeight : Number = parseFloat(layoutNode.@pageheight);
			var frameNode : XML ;
			var w:Number,h:Number,top:Number, left:Number;
			var edgeX:Number = MeasureManager.pixelToPFL(edgeSize)/layoutScaleX;
			var edgeY:Number = MeasureManager.pixelToPFL(edgeSize)/layoutScaleY;
			
			// check frames and apply scale factor
			for (var j:int = 0; j < layoutNode.frame.length(); j++) 
			{
				frameNode = layoutNode.frame[j];
				w = Number(frameNode.@width);
				h = Number(frameNode.@height);
				left = Number(frameNode.@left);
				top = Number(frameNode.@top);
				
				if(left + w == layoutWidth + 20)
				{ 
					w = w -20 + edgeX;
				}
				if(left == -20)
				{ 
					left = -edgeX;
					w = w -20 + edgeX;
				}
				
				if(top + h == layoutHeight + 20)
				{ 
					h = h -20 + edgeY;
				}
				if(top == -20)
				{ 
					top = -edgeY;
					h = h -20 + edgeY;
				}
				
				
				frameNode.@width = w
				frameNode.@height = h
				frameNode.@left = left
				frameNode.@top = top 
			}
			
			
			return layoutNode;				
		}
		
		
		public function getFrame( layoutNode : XML, layoutScaleX : Number, layoutScaleY : Number ):XML
		{
			var layoutWidth : Number = parseFloat(layoutNode.@pagewidth);
			var layoutHeight : Number = parseFloat(layoutNode.@pageheight);
			var frameNode : XML ;
			var w:Number,h:Number,top:Number, left:Number;
			var edgeX:Number = MeasureManager.pixelToPFL(edgeSize)/layoutScaleX;
			var edgeY:Number = MeasureManager.pixelToPFL(edgeSize)/layoutScaleY;
			
			// check frames and apply scale factor
			for (var j:int = 0; j < layoutNode.frame.length(); j++) 
			{
				frameNode = layoutNode.frame[j];
				w = Number(frameNode.@width);
				h = Number(frameNode.@height);
				left = Number(frameNode.@left);
				top = Number(frameNode.@top);
				
				if(left + w == layoutWidth + 20)
				{ 
					w = w -20 + edgeX;
				}
				if(left == -20)
				{ 
					left = -edgeX;
					w = w -20 + edgeX;
				}
				
				if(top + h == layoutHeight + 20)
				{ 
					h = h -20 + edgeY;
				}
				if(top == -20)
				{ 
					top = -edgeY;
					h = h -20 + edgeY;
				}
				
				
				frameNode.@width = w
				frameNode.@height = h
				frameNode.@left = left
				frameNode.@top = top 
			}
			
			return layoutNode;				
		}
		
		
		
		
		/**
		 * > init multiple frame 
		 * > this is done when project is ready (after project load or a new project creation) 
		 * > if there is a photo vo, it's frame creation 
		 * > if not, it means it's a save recover or an upgrade
		 */
		public function initMultipleFrame( isUpdate : Boolean = false):void
		{
			// if not multipleFrameVo
			isMultipleFirstAdd = (!Infos.project.multipleFrameVo)? true : false;
			
			
			// create multiple frame vo
			var	frameVo:FrameVo = (Infos.project.multipleFrameVo)? Infos.project.multipleFrameVo : new FrameVo();
			frameVo.type = FrameVo.TYPE_PHOTO;
			frameVo.width = 	( multipleColsCount * Infos.project.getPageBounds().width )  +  ( 2 * edgeSize )  +  ((multipleColsCount-1) * gapBetweenPages); // num cols * page width + 2*edge (left and right) + gaps between
			frameVo.height = 	( multipleRowsCount * Infos.project.getPageBounds().height )  +  ( 2 * edgeSize )  +  ((multipleRowsCount-1) * gapBetweenPages);
			
			/*
			if(!Infos.project.multipleFrameVo || isUpdate){
				frameVo.type = FrameVo.TYPE_PHOTO;
				frameVo.width = 	( multipleColsCount * Infos.project.getPageBounds().width )  +  ( 2 * edgeSize )  +  ((multipleColsCount-1) * gapBetweenPages); // num cols * page width + 2*edge (left and right) + gaps between
				frameVo.height = 	( multipleRowsCount * Infos.project.getPageBounds().height )  +  ( 2 * edgeSize )  +  ((multipleRowsCount-1) * gapBetweenPages);
					
			}
			*/
			
			// check if there is already a photo in a possible updated multiple frame
			var tempPhotoVo : PhotoVo = (Infos.project.multipleFrameVo)?Infos.project.multipleFrameVo.photoVo:null
			
			// save it
			Infos.project.multipleFrameVo = frameVo;	
			
			// create virtual frame for calculation
			if(!multipleFrame) {
				if(tempPhotoVo)frameVo.photoVo = tempPhotoVo;
				multipleFrame = new FrameMultiple(Infos.project.multipleFrameVo)
				multipleFrame.init();
			} else {
				multipleFrame.frameVo = frameVo;
				if(tempPhotoVo)updateMultipleFramePhoto(tempPhotoVo, true);
				else multipleFrame.update(null);
			}
		}
		
		
		/**
		 * clear a multiple frame content
		 */
		public function clearMultipleFrame():void
		{
			// reset multipleFrame vo
			Infos.project.multipleFrameVo.photoVo = null;
			Infos.project.multipleFrameVo.cLeft = 0;
			Infos.project.multipleFrameVo.cTop = 0;
			Infos.project.multipleFrameVo.zoom = 1;
			
			// clear sub frames
			var subFrame : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				// find background of all pages
				subFrame = Infos.project.pageList[i].layoutVo.frameList[0];
				if(subFrame.isMultiple) { // only update photo if it's still a multiple frame
					var f : FramePhoto = EditionArea.getVisibleFrameByFrameVo( subFrame ) as FrameBackground;
					f.clearMe();
				}
			}
		}
		
		
		
		/**
		 * Inject a photo on all background frames of project (multiple canvas layout)
		 * > this action is only done when drag and drop of a photo on the background of a multiple canvas page
		 */
		public function updateMultipleFramePhoto( photoVo : PhotoVo , forceAllFrames : Boolean = true):void
		{
			isMultipleFirstAdd = false;
			
			// update multiple frame
			multipleFrame.update( photoVo );
			
			// update all visible subFrames (which compose all together the big frame)
			var subFrame : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				// find background of all pages
				subFrame = Infos.project.pageList[i].layoutVo.frameList[0];
				if(forceAllFrames) subFrame.isMultiple = true; // put the frame as multiple frame
				
				if(subFrame.isMultiple) { // only update photo if it's still a multiple frame
					subFrame.photoVo = photoVo;
					var f : FramePhoto = EditionArea.getVisibleFrameByFrameVo( subFrame ) as FrameBackground;
					f.update(photoVo);
				}
			}
			
			// update subFrames relative to main multiple frame
			updateMultipleFramesPosition();
		}
		
		
		/**
		 * Update multiple frames position relative to main virtual frame
		 */
		public function updateMultipleFramesPosition():void
		{
			var subFrame : FrameVo;
			var numCols : Number = multipleColsCount;
			var numRow : Number = multipleRowsCount;
			var margin : Number = gapBetweenPages;
			var pageW : Number = Infos.project.getPageBounds().width;
			var pageH : Number = Infos.project.getPageBounds().height;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				subFrame = Infos.project.pageList[i].layoutVo.frameList[0];
				if(subFrame.isMultiple){
					subFrame.zoom = multipleFrame.frameVo.zoom;
					//subFrame.cLeft = multipleFrame.frameVo.cLeft  +  ((i) % numCols) * (pageW + edgeSize*2 + margin)   - ;
					//subFrame.cTop = multipleFrame.frameVo.cTop  +  Math.floor((i)/ numCols) * (pageH + edgeSize*2 +  margin)  -
					subFrame.cLeft = multipleFrame.frameVo.cLeft   +  ((i) % numCols) * (pageW + margin)  ;
					//if( i % numCols == 0 ) subFrame.cLeft -= edgeSize;
					subFrame.cTop = multipleFrame.frameVo.cTop  + Math.floor((i)/ numCols) * (pageH +  margin) 
					//if( Math.floor( i/numCols ) == 0 ) subFrame.cTop -= edgeSize;
					
					// find visible frame
					var f : FramePhoto = EditionArea.getVisibleFrameByFrameVo( subFrame ) as FrameBackground;
					f.update(null);
				}
			}
			
			// store changes
			UserActionManager.instance.addAction(UserActionManager.TYPE_MULTIPLE_FRAME_CHANGE);
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
	
	}
}
class SingletonEnforcer{}