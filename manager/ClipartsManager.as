/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen
 YEAR			: 2013
 ****************************************************************/
package manager 
{
	import be.antho.data.ResourcesManager;
	
	import data.ClipartVo;
	import data.Infos;
	
	CONFIG::offline{
		import offline.manager.OfflineAssetsManager;
		import offline.manager.ZIPManager;
		import flash.filesystem.File;
		import be.antho.air.FileUtils;
	}
	
	import org.osflash.signals.Signal;
	
	import utils.Debug;
	
	import view.popup.PopupAbstract;
	
	

	/**
	 * Cliparts manager
	 */
	public class ClipartsManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		public static const CLIPARTS_REFRESH_COMPLETE:Signal = new Signal();
		
		public var availableIds:Vector.<String>;	// list of bkg Ids available for this project
		public var availableCategoriesIds:Vector.<String>;	// list of categories Ids available for this project
		private var initiated:Boolean = false;

		//offline
		private var offline_Process_Has_begun:Boolean;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function ClipartsManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("ClipartsManager is a singleton, use instance"); 
			/*if(!initiated)
				reset();*/
		}
		
		/**
		 * instance
		 */
		private static var _instance:ClipartsManager;
		public static function get instance():ClipartsManager
		{
			if (!_instance) _instance = new ClipartsManager(new SingletonEnforcer())
			return _instance;
		}
		
		public function get showDownloadMoreBtn():Boolean
		{
			return false;
			//not used anymore
			/*CONFIG::online
			{
				return false;
			}
			
			CONFIG::offline
			{
				return !OfflineAssetsManager.instance.checkIfDownloaded(OfflineAssetsManager.CLIPART);
			}*/
		}
		
		/**
		 * Get local xml path
		 */
		public function get xmlPath():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsCommonPath + "/cliparts.xml";		
			}
			return null;
		}
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Reset manager
		 * Set initiated to false to force init
		 */
		public function reset():void
		{
			if(Infos.IS_DESKTOP)
				refresh();
			else			
				init();
		}
		
		/**
		 * Refresh offline only
		 * Reload XML and reset Manager
		 */
		public function refresh():void
		{
			CONFIG::offline
				{
					Debug.log("ClipartManager.refresh");
					var file:File = new File(xmlPath); 
					if(file.exists)
					{
						Debug.log("ClipartManager.refresh > new xml exist");
						
						OfflineAssetsManager.instance.getXMLLocally(xmlPath, 
						function(result:Object):void
						{
							var freshXML:XML = new XML(result.toString());
							Debug.log("FRESH XML CLIPART: "+freshXML);
							ProjectManager.instance.clipartsXML = freshXML;
							init();
							CLIPARTS_REFRESH_COMPLETE.dispatch();
						}, 
						function(e:*):void
						{
							Debug.log("ClipartManager.refresh failed");
						});
					}
				}
		}
		
		/**
		 * Update offline XML with freshly downloaded one
		 * Replace local XML and reset Manager
		 */
		public function replaceLocalXML(freshXML:XML, onComplete:Function):void
		{
			Debug.log("ClipartsManager.replaceLocalXML");
			CONFIG::offline
			{
				OfflineAssetsManager.instance.saveXMLLocally(freshXML,xmlPath, 
					function(xmlString:String):void
					{
						ClipartsManager.instance.reset();
						onComplete.call();
					});
			}
			
			CONFIG::online
			{
				ProjectManager.instance.clipartsXML = freshXML;
				ClipartsManager.instance.reset();
				onComplete.call();
			}
		}
		
		
		public function init():void
		{
			if(ProjectManager.instance.clipartsXML == null) return;
			
			availableIds = new Vector.<String>();	
			availableCategoriesIds = new Vector.<String>();
			
			//Available cliparts
			var availableList:XMLList = ProjectManager.instance.clipartsXML.document..clipart.@name;
			for (var j:int = 0; j < availableList.length(); j++) 
				availableIds.push(availableList[j]);

			//Categories
			var seen:Object={}
			var node:XML = XML(ProjectManager.instance.clipartsXML.document[0]);
			var availableCategoriesList:XMLList = node.clipart.@catagory.(!seen[valueOf()]&&(seen[valueOf()]=valueOf()));
			for (j = 0; j < availableCategoriesList.length(); j++) 
				availableCategoriesIds.push(availableCategoriesList[j]);
					
			//flag
			initiated = true;
			
			if(Infos.IS_DESKTOP)
			{
				Infos.INTERNET_CHANGED.remove(reset);
				Infos.INTERNET_CHANGED.add(reset);
			}
		}
		
		
		public function getVoById(id:String):ClipartVo
		{
			return createVo(id);
		}
		
		/**
		 * retrieve the cover background bounds (it has specific bleed of 10mm) and take the spine into calculation
		 */
		public function downloadMore(popup:PopupAbstract = null):void
		{
			CONFIG::offline{
				Debug.log("ClipartManager > downloadMore");
				
				if(Infos.INTERNET)
				{
					//Call
					//var url:String = "http://www.smileasyoudoit.com/customers/tictac/servicetemp/clipart_all.zip";
					var url:String = Infos.config.getSettingsValue("OfflineAssetsToDownloadUrl") +"clipart_all.zip";
					var destination:String = Infos.offlineAssetsCommonPath//Infos.offlineAssetsPath + Infos.session.classname;
					
					ZIPManager.instance.downloadAndUnzip(url, destination,downloadMoreSuccess, downloadMoreError, false, true);
				
				}
				else
				PopupAbstract.Alert(ResourcesManager.getString("popup.no.internet.title"), ResourcesManager.getString("popup.no.internet.desc"), false, null );
				
			}			
		}
		
		/**
		 * Download more bkg SUCCESS
		 */
		private function downloadMoreSuccess():void
		{
			CONFIG::offline
			{
				Debug.log("ClipartManager.downloadMoreSuccess");
				OfflineAssetsManager.instance.markAsDownloaded(OfflineAssetsManager.CLIPART);
				refresh();
				PopupAbstract.Alert(ResourcesManager.getString("popup.download.ready.title"), ResourcesManager.getString("popup.new.clipart.ready.desc"),false,null,null,null);
			}
		}
		/**
		 * Download more bkg ERROR
		 */
		private function downloadMoreError(msg:String):void
		{
			Debug.log("ClipartManager.downloadMoreError > Error");
			Debug.log("Abort message: "+msg);
			Debug.warnAndSendMail(msg);
			
			//todo: clean everything
			
			//Show popup
			PopupAbstract.Alert(ResourcesManager.getString("popup.download.failed.title"), ResourcesManager.getString("popup.download.failed.desc"),false,null,null);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Create a Bkg Vo based on a ID
		 * Look for it in the XML 
		 * */
		private function createVo(id:String):ClipartVo
		{
			var ids:Vector.<String> = availableIds;
			var rawXML:XML = ProjectManager.instance.clipartsXML;
			var xml:XML = XML(ProjectManager.instance.clipartsXML.document[0]);
			var node:XML;
			var vo:ClipartVo;
			
			if(ids.lastIndexOf(id) != -1)
			{
				//Get layout node
				node = xml.clipart.(@name == id)[0];
				
				if(!node)
					return null;
				
				//vo
				vo = new ClipartVo();
				vo.id = node.@name;
				vo.name = node.@name;
				vo.category = node.@catagory;
				vo.id_tn = String(String(node.@tnproxy).split(".png")[0]).substring(8);
				vo.proxy = String(node.@proxy);
				vo.id_hd = String(String(node.@proxy).split(".png")[0]).substring(8);
				vo.width = Number(node.@width);
				vo.height = Number(node.@height);
				
				return vo;
			}
			
			return null;
		}
	}
}
class SingletonEnforcer{}