/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import com.bit101.charts.LineChart;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.engine.LineJustification;
	
	import be.antho.bitmap.snapshot.SnapShot;
	import be.antho.data.XmlManager;
	
	import comp.LayoutItem;
	
	import data.FrameVo;
	import data.Infos;
	import data.LayoutVo;
	import data.PageVo;
	import data.ProductsCatalogue;
	import data.ThumbVo;
	
	import flashx.textLayout.formats.VerticalAlign;
	
	import org.osmf.layout.HorizontalAlign;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.EditionArea;
	import view.edition.PageArea;
	
	
	
	/**
	 * Layout manager
	 */
	public class LayoutManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		private var availableLayoutIds:Vector.<String>;	// list of Layout Ids available for the app including hidden layouts
		private var availableCoverLayoutIds:Vector.<String>;	// list of Layout Ids available app including hidden layouts
		
		public var publicAvailableLayoutIds:Vector.<String>;	// list of Layout Ids available for the app
		public var publicAvailableCoverLayoutIds:Vector.<String>;	// list of Cover Layout Ids available for the app
		public var publicAvailableCustomLayoutIds:Vector.<String>;	// list of Custom Layout Ids available for the app
		public var publicAvailableCustomCoverLayoutIds:Vector.<String>;	// list of Custom Cover Layout Ids available for the app
		public var totalDaysToDisplay:int = 0;
		public var forceRedrawLayoutsAfterUpgrade : Boolean = false; 
		
		//
		private var initiated:Boolean = false;
		//private var textBitmapData : BitmapData;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function LayoutManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("LayoutManager is a singleton, use instance"); 
			if(!initiated)
				init();
		}
		
		/**
		 * instance
		 */
		private static var _instance:LayoutManager;

	
		public static function get instance():LayoutManager
		{
			if (!_instance) _instance = new LayoutManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Reset manager
		 * Set initiated to false to force init
		 */
		public function reset():void
		{
			init();
		}
		
		/**
		 * Init layout manager
		 * > retrieve available layout for current doc code (Q20, M30, etc..) from documents.xml 
		 * > put layout ids in availableLayoutList (and in public if type is not hidden) EDIT : TODO : what is a layout with hide type
		 * > retrieve same info for cover layouts
		 * > creates default text bitmap data which will be use to generate layout preview thumbs
		 */
		public function init():void
		{
			
			Debug.log("LayoutManager > init"); 
			
			var projectCode:String = ProjectManager.instance.project.docCode; // Q20, Q30 etc...
			var node:XML;
			var id:String
			
			
			availableLayoutIds = new Vector.<String>();
			availableCoverLayoutIds = new Vector.<String>();
			publicAvailableLayoutIds = new Vector.<String>();
			publicAvailableCoverLayoutIds = new Vector.<String>();
			publicAvailableCustomLayoutIds = new Vector.<String>();
			publicAvailableCustomCoverLayoutIds = new Vector.<String>();
				
			
			// Retrieave Available layouts from XML
			var availableLayoutList:XMLList = ProjectManager.instance.documentsXML.document.(@name == projectCode)[0].layout;//.@name;
			for (var j:int = 0; j < availableLayoutList.length(); j++) 
			{
				node = availableLayoutList[j];
				id = availableLayoutList[j].@name;
				
				//add id to list
				availableLayoutIds.push(id);
				 //if not hidden add it to plublic
				if(node.@type != "hide") 
					publicAvailableLayoutIds.push(id);
				
			}
			
			
			// Retrieave custom layouts from XML
			if(ProjectManager.instance.customLayoutXML != null)
			{
				//Custom layouts
				var availableCustomLayoutList:XMLList = ProjectManager.instance.customLayoutXML.layout;
				for (j = 0; j < availableCustomLayoutList.length(); j++) 
				{
					id = availableCustomLayoutList[j].@name;	
					node = availableCustomLayoutList[j];
					//add id to list if belong to same project type
					if(node.@doctype == Infos.project.docPrefix)
					publicAvailableCustomLayoutIds.push(id);
				}
				
				//Custom Cover layouts
				var availableCustomCoverLayoutList:XMLList = ProjectManager.instance.customLayoutXML.layout;
				for (j = 0; j < availableCustomCoverLayoutList.length(); j++) 
				{
					id = availableCustomCoverLayoutList[j].@name;	
					node = availableCustomCoverLayoutList[j];
					//add id to list if belong to same project type
					if(node.@doctype == "C"+Infos.project.docPrefix)
						publicAvailableCustomCoverLayoutIds.push(id);
				}
			}
			
			// Retrieve covers Layouts
			if(ProjectManager.instance.coversDocumentXML != "")
			{
				var availableCoverLayoutList:XMLList = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+projectCode)[0].layout;//.@name;
				for (j = 0; j < availableCoverLayoutList.length(); j++) 
				{
					node = availableCoverLayoutList[j];
					id = availableCoverLayoutList[j].@name;
					
					//add id to list
					availableCoverLayoutIds.push(id);
					if(node.@type != "hide") //if not hidden add it to plublic
					publicAvailableCoverLayoutIds.push(id);
					
				}
			}
			
			//Calendars infos
			if(Infos.isCalendar)
			{
				var xml:XML = ProjectManager.instance.layoutsXML.layout.(@name == availableLayoutIds[0])[0];
				var list:XMLList = xml..frame.(attribute("dateaction") == CalendarManager.DATEACTION_DAYDATE);
				totalDaysToDisplay = list.length();
			}
			
			// create reusable textBitmapData
			/*
			var textExample : Shape = new Shape();
			var boxW : Number = 20;
			var lineThickness : Number = 1;
			var lineSpace : Number = 2;
			textExample.graphics.beginFill(0xdddddd,.5);
			textExample.graphics.drawRect(0,0,boxW, boxW);
			var posY: Number = 0;
			for (var i:int = 0; i < boxW/lineSpace; i++) 
			{
				//textExample.graphics.lineStyle(lineThickness,0xff0000,1);
				//textExample.graphics.moveTo(0,posY);
				//textExample.graphics.lineTo(1,posY);
				textExample.graphics.lineStyle(lineThickness,0xddddff,.6);
				textExample.graphics.moveTo(0,posY);
				textExample.graphics.lineTo(boxW,posY);
				//textExample.graphics.lineStyle(lineThickness,0xff0000,1);
				//textExample.graphics.moveTo(boxW-1,posY);
				//textExample.graphics.lineTo(boxW,posY);
				posY = posY + lineSpace+lineThickness;
			}
			
			textBitmapData = new BitmapData(boxW,boxW,true, 0x000000);
			textBitmapData.draw(textExample);
			*/
			
			
			//flag
			initiated = true;
		}
		
		
		/**
		 * a fixed layout indicates a page that can be updated (background/photo/layout update)
		 */
		public function isLayoutFixed( layoutId : String ):Boolean
		{
			if(Infos.isCards)
			{
				 // for now use this only for cards, remove the condition above for all projects
				var layoutXML:XML = ProjectManager.instance.layoutsXML;
				var layoutNode:XML = layoutXML.layout.(@name == layoutId)[0];
				if( layoutNode && layoutNode.@editable == "false" ) return true;
			}
			
			return false;
		}
		
		
		/**
		 * Retrieve a layoutvo by it's id
		 * > Actualy it seems to create a new layout vo each time this function is requested TODO : ask why we do not store the layouts already created
		 */
		public function getLayoutById(id:String, isCoverLayout:Boolean = false, isCustom:Boolean = false):LayoutVo
		{
			return createLayoutVo(id, isCoverLayout, isCustom);
		}
		
		
		/**
		 * create a layout item for left tab
		 */
		public function createLayoutItem( layoutVo:LayoutVo, isCover:Boolean = false ):LayoutItem
		{
			var thumbVo:ThumbVo = new ThumbVo();
			thumbVo.id = layoutVo.id;
			thumbVo.label = layoutVo.id;
			thumbVo.bounds = new Rectangle(0,0,125,102);
			
			var layoutItem :LayoutItem = new LayoutItem( thumbVo );
			layoutItem.isCalendar = layoutVo.isCalendar;
			layoutItem.isCustomLayout = layoutVo.isCustom;
			layoutItem.layoutId = layoutVo.id;
			layoutItem.draggable = true;
			
			var pageBounds : Rectangle = (isCover) ? Infos.project.getCoverBounds() : Infos.project.getPageBounds();
			var pageRatio : Number = pageBounds.width / pageBounds.height;
			var thumbRatio : Number = thumbVo.bounds.width / thumbVo.bounds.height;
			var layoutScale : Number = (pageRatio > thumbRatio) ? thumbVo.bounds.width / pageBounds.width : thumbVo.bounds.height / pageBounds.height; 
			layoutItem.dropType = DragDropManager.instance.getDropTypeFromLayoutVo(layoutVo);//DragDropManager.TYPE_MENU_LAYOUT;
			
			var imageBounds : Rectangle = new Rectangle(0,0,pageBounds.width*layoutScale, pageBounds.height*layoutScale);
			layoutItem.updateThumbData(LayoutManager.instance.createBitmapThumbFromLayoutVo(layoutVo, layoutScale, imageBounds).bitmapData);
			layoutItem.listenSelection();
			
			return layoutItem;
		}
		
		
		
		/**
		 * Create a thumb for a generated layout vo
		 */
		private function createBitmapThumbFromLayoutVo(layoutVo:LayoutVo, frameScale : Number, thumbBounds:Rectangle):Bitmap
		{
			var s:Sprite = new Sprite();
			var frameVo : FrameVo;
			var isBkg : Boolean;
			var f : Sprite;
			var frameWrapper : Sprite;
			var lineThickness : Number = 1;
			
			for (var i:int = 0; i < layoutVo.frameList.length; i++) 
			{
				frameVo = layoutVo.frameList[i];
				isBkg = (i==0)?true:false;
				
				//var color:Number = Math.random()*0xffffff;
				f = new Sprite();
				frameWrapper = new Sprite();
				lineThickness = 1;
				
				if( frameVo.type == FrameVo.TYPE_TEXT ){
					f.graphics.lineStyle(lineThickness,Colors.BLUE,1,false, LineScaleMode.NONE,CapsStyle.NONE, JointStyle.BEVEL);
					f.graphics.beginFill(Colors.BLUE_LIGHT,0.6);
				}
				else if( frameVo.type == FrameVo.TYPE_BKG ){
					//f.graphics.lineStyle(1,Colors.GREY_LIGHT,1);
					f.graphics.beginFill(Colors.GREY_LIGHT,0.4);
				}
				else {
					f.graphics.lineStyle(lineThickness,Colors.GREY_DARK,1,false, LineScaleMode.NONE,CapsStyle.NONE, JointStyle.BEVEL);
					f.graphics.beginFill(Colors.GREY,0.4);
				}
			
			
				if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
				{
					var safetyZone:Number = MeasureManager.millimeterToPixel(20)
					f.graphics.drawRect(Math.round(-Infos.project.getCoverBounds().height+safetyZone),Math.round(frameVo.x-frameVo.height/2),Math.round(frameVo.width),Math.round(frameVo.height));
					f.graphics.endFill();
					f.rotation = frameVo.rotation;
					s.addChild(f);
				}
				else
				{
					//f.graphics.drawRect(Math.round(frameVo.x-frameVo.width/2),Math.round(frameVo.y-frameVo.height/2),Math.round(frameVo.width),Math.round(frameVo.height));
					f.graphics.drawRect(0,0,Math.round(frameVo.width),Math.round(frameVo.height));
					f.graphics.endFill();
					f.x = -frameVo.width/2;
					f.y = -frameVo.height/2;
					frameWrapper.addChild(f);
					frameWrapper.rotation = frameVo.rotation;
					frameWrapper.x = Math.round(frameVo.x);
					frameWrapper.y = Math.round(frameVo.y);
					s.addChild(frameWrapper);
				}
				
				/*
				if( frameVo.type == FrameVo.TYPE_TEXT && frameVo.grouptype != FrameVo.GROUP_TYPE_SPINE && frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
				{
					var textVisu : Bitmap = new Bitmap(textBitmapData);
					textVisu.x = frameVo.x-frameVo.width/2 + 20;
					textVisu.y = frameVo.y-frameVo.height/2 + 20;
					textVisu.scaleX = (frameVo.width-40)/textBitmapData.width;
					textVisu.scaleY = (frameVo.height-40)/textBitmapData.height;
					s.addChild(textVisu);
				}
				*/
				
			}
			
			s.scaleX = s.scaleY = frameScale;
			var snapShotContent : Sprite = new Sprite(); // we do not use snapshot scale property to scale this down as it make stroke look awful
			snapShotContent.addChild(s);
			var bitmapData:BitmapData = new BitmapData(thumbBounds.width,thumbBounds.height, true, 0x000000);
			bitmapData.draw(snapShotContent);
			return new Bitmap(bitmapData) ;//return new Bitmap(SnapShot.snap(snapShotContent,1)); //return new Bitmap(SnapShot.snap(s,ratio));
		}
		
		
		/**
		 * HELPER to verify layout visibility in LayoutTab
		 * */
		public function isHidden(id:String, isCover:Boolean):Boolean
		{
			var type:String;
			if(!isCover)
				type = ProjectManager.instance.documentsXML.document.(@name == Infos.project.docCode)[0].layout.(@name == id).@type;
			else
				type = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+Infos.project.docCode)[0].layout.(@name == id).@type;
			
			if(type == "hide")
				return true
			
			return false;
		}
		
		/**
		 * save current page layout (make it a custom layout)
		 */
		public function saveCurrentPageLayout():void
		{
			Debug.log("LayoutManager.saveCurrentPageLayout");
			
			var xml:XML = ProjectManager.instance.customLayoutXML;
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			var newCustomLayoutName:String = getNewLayoutname();
			var pageBounds:Rectangle = (PagesManager.instance.currentEditedPage.isCover)?Infos.project.getCoverBounds():Infos.project.getPageBounds();
			var docTypeStr:String = (PagesManager.instance.currentEditedPage.isCover)?"C"+Infos.project.docPrefix:Infos.project.docPrefix;
			var layoutNode:String = "<layout pagewidth=\""+Math.round(MeasureManager.pixelToPFL(pageBounds.width))+"\" pageheight=\""+Math.round(MeasureManager.pixelToPFL(pageBounds.height))+"\" name=\""+newCustomLayoutName+"\" doctype=\""+docTypeStr+"\" proxy=\"layouts/L1.bmp\" custom=\"true\" editable=\"true\">";
			//<layout pagewidth="138" pageheight="100" name="100002" doctype="S" proxy="layouts/L1.bmp" custom="true" editable="true">
			
			for (var i:int = 0; i < currentPage.layoutVo.frameList.length; i++) 
			{
				var frameVo:FrameVo = currentPage.layoutVo.frameList[i];
				if(frameVo.type == FrameVo.TYPE_TEXT && !frameVo.isPageNumber || frameVo.type == FrameVo.TYPE_PHOTO)
				{
					//<frame left="-1.988384" top="-1.988384" width="141.999480" height="103.998897" type="picture" custom="true" groupid="0" grouptype="0" fill="false" stroke="false" editable="true" bleed="0" move="false" rotation="0" orientate="0"/>
					var left:Number = Math.round(MeasureManager.pixelToPFL(frameVo.x - frameVo.width/2));
					var top:Number = Math.round(MeasureManager.pixelToPFL(frameVo.y - frameVo.height/2));
					var width:Number = MeasureManager.pixelToPFL(frameVo.width);
					var height:Number = MeasureManager.pixelToPFL(frameVo.height);
					var type:String = frameVo.type;
					var grouptype:int = frameVo.grouptype;
					var rotation:Number = frameVo.rotation;
					
					layoutNode += "<frame left=\""+left+"\" top=\""+top+"\" width=\""+width+"\" height=\""+height+"\" type=\""+type+"\" custom=\"true\" groupid=\"0\" grouptype=\""+grouptype+"\" fill=\"false\" stroke=\"false\" editable=\"true\" bleed=\"0\" move=\"false\" rotation=\""+rotation+"\" orientate=\"0\"/>";

				}
			}
			layoutNode += "</layout>";
			
			//add new node to XML
			var newNode:XML = new XML(layoutNode);
			xml.appendChild(newNode);
			refreshCustomLayoutIdList();
		}
		
		
		
		/**
		 * Delete custom page layout by id
		 */
		public function deleteCustomLayoutById(id:String):void
		{
			//Update XML
			var xml:XML = ProjectManager.instance.customLayoutXML;
			delete xml.layout.(@name == id)[0];
			
			//Refresh available ids list
			refreshCustomLayoutIdList();
			
			Debug.log(xml);
		}
		
		/**
		 * Helper taht return the correct fontSize for album frame text depending on the album size
		 */
		public function getFontSizeForAlbum():Number
		{
			if( Infos.project.docPrefix == "S" ) return 18;
			else if( Infos.project.docPrefix == "M" ) return 36;
			else if( Infos.project.docPrefix == "L" ) return 40;
			
			return 18;
		}
		
		/**
		 * Helper taht return the correct fontSize for album page number
		 */
		public function getFontSizeForAlbumPageNumber():Number
		{
			if( Infos.project.docPrefix == "S" ) return 11;
			else if( Infos.project.docPrefix == "M" ) return 15;
			else if( Infos.project.docPrefix == "L" ) return 20;
			
			return 11;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Refresh custom layout id list
		 */
		private function refreshCustomLayoutIdList():void
		{
			publicAvailableCustomLayoutIds = new Vector.<String>();
			var availableCustomLayoutList:XMLList = ProjectManager.instance.customLayoutXML.layout;
			for (var j:int = 0; j < availableCustomLayoutList.length(); j++) 
			{
				var id:String = availableCustomLayoutList[j].@name;	
				var node:XML = availableCustomLayoutList[j];
				//add id to list if belong to same project type
				if(node.@doctype == Infos.project.docPrefix)
					publicAvailableCustomLayoutIds.push(id);
			}
			
			//Custom Cover layouts
			publicAvailableCustomCoverLayoutIds = new Vector.<String>();
			var availableCustomCoverLayoutList:XMLList = ProjectManager.instance.customLayoutXML.layout;
			for (j = 0; j < availableCustomCoverLayoutList.length(); j++) 
			{
				id = availableCustomCoverLayoutList[j].@name;	
				node = availableCustomCoverLayoutList[j];
				//add id to list if belong to same project type
				if(node.@doctype == "C"+Infos.project.docPrefix)
					publicAvailableCustomCoverLayoutIds.push(id);
			}
		}
		
		/**
		 * Get name for new layout
		 */
		private function getNewLayoutname():String
		{
			var newName:int = 0;
			var availableCustomLayoutList:XMLList = ProjectManager.instance.customLayoutXML.layout;
			for (var j:int = 0; j < availableCustomLayoutList.length(); j++) 
			{
				var id:int = int(availableCustomLayoutList[j].@name);
				if(id>newName)
					newName = id;
			}
			return String(newName+1);
		}
		
		
		/**
		 * Create a layout Vo based on a ID
		 * Look for it in the XML 
		 * */
		private function createLayoutVo(id:String, isCoverLayout:Boolean = false, isCustomLayout:Boolean = false):LayoutVo
		{
			
			//Switch layout ids repository if isCover or if isCustom
			var ids:Vector.<String> = (isCoverLayout) ? availableCoverLayoutIds : availableLayoutIds;
			
			// switch to custom layouts
			if(isCustomLayout) ids = (!isCoverLayout)? publicAvailableCustomLayoutIds : publicAvailableCustomCoverLayoutIds;
			
			// check if layout id exists
			if(ids.lastIndexOf(id) != -1)
			{
				//Switch layout XML if is Custom Layout
				var layoutXML:XML = ProjectManager.instance.layoutsXML;
				if(isCustomLayout)
				{
					layoutXML = ProjectManager.instance.customLayoutXML;
				}
				
				var layoutNode:XML;
				var frameNode:XML;
				var frameList:XMLList;
				var frameVo:FrameVo;
				
				//Get layout xml node
				layoutNode = layoutXML.layout.(@name == id)[0];
				frameList = layoutNode.frame;
				
				//if there is frame in it (old XML containes layout without frames, not sure why) we create a layoutVo
				if(frameList.length() > 0) 
				{
					//Create vo
					var layoutVo:LayoutVo = new LayoutVo();
					layoutVo.id = layoutNode.@name;					
					
					//is custom
					layoutVo.isCustom = isCustomLayout;
					
					//is Calendar or not
					var dateActionList:XMLList = XMLList(layoutNode..@dateaction);
					layoutVo.isCalendar = checkIsCalendar(dateActionList);
					
					//Flag for only on background
					var bkgFound:Boolean = false;
					
					// first of all we create a frame background for each possible layout
					// we will not add the one from xml
					// EDIT : only if we are not in canvas (canvas background, frame calculation is a bit different)
					layoutVo.frameList.push(BackgroundsManager.instance.createBackgroundFrameVo(isCoverLayout));
					
					
					// Check if the layout width and height is different from the project width and height
					// if so, we need to modify the layout using a scale value
					// TODO : we should in best world use a scaleX and scaleY property, for now we just test the width
					//Casual albums have spine in their layout, but its not used.
					var withSpine:Boolean = (Infos.project.type == ProductsCatalogue.ALBUM_CASUAL)?true:false;
					var layoutWidth : Number = (isCoverLayout)?getRawCoverWidth(layoutNode,false):MeasureManager.PFLToPixel(parseFloat(layoutNode.@pagewidth));
					var layoutHeight : Number = MeasureManager.PFLToPixel(parseFloat(layoutNode.@pageheight),isCoverLayout);
					var projectWidth : Number = (isCoverLayout)? MeasureManager.PFLToPixel(Infos.project.coverWidth*2,true) : MeasureManager.PFLToPixel(Infos.project.width);
					var projectHeight : Number = (isCoverLayout)? MeasureManager.PFLToPixel(Infos.project.coverHeight,true) : MeasureManager.PFLToPixel(Infos.project.height);
					var layoutScaleX : Number = projectWidth/layoutWidth;
					var layoutScaleY : Number = projectHeight/layoutHeight;
					
					if(layoutScaleX != 1)
						Debug.log("");
					
					// for canvas and albums we use layout scale system, for others, we just add a warning in log
					if(!Infos.isCanvas && !Infos.isAlbum && Infos.project.docPrefix != "MCAL1" && Infos.project.docPrefix != "MCAL2"){
						if(layoutScaleX != 1) Debug.warn("Layout with id "+ layoutVo.id +" (isCover : "+isCoverLayout+") has a width of "+layoutWidth + "px but project has a width of "+projectWidth+"px");
						if(layoutScaleY != 1) Debug.warn("Layout with id "+ layoutVo.id +" (isCover : "+isCoverLayout+") has a height of "+layoutHeight + "px but project has a height of "+projectHeight+"px");
						
						// reset scalex and scaley fo
						layoutScaleY = 1;
						layoutScaleX = 1;
					}
					
					// for canvas we need to modify xml node to fit project edge 
					if(Infos.isCanvas) layoutNode = CanvasManager.instance.modifyCanvasLayoutXMLNode(layoutNode, layoutScaleX, layoutScaleY); 
					
					// get frame node list
					frameList = layoutNode.frame;
					
					//create frameVO
					for (var j:int = 0; j < frameList.length(); j++) 
					{
						frameNode = frameList[j];
						
						frameVo = new FrameVo();
						//frameVo.allowMove = frameNode.@move;
						
						// we directly scale frames at creation here but we could have used the layoutVo.scaleLayout system also
						frameVo.x = (MeasureManager.PFLToPixel(frameNode.@left,isCoverLayout) + (MeasureManager.PFLToPixel(frameNode.@width,isCoverLayout))/2) * layoutScaleX;
						frameVo.y = (MeasureManager.PFLToPixel(frameNode.@top,isCoverLayout) + (MeasureManager.PFLToPixel(frameNode.@height,isCoverLayout))/2) * layoutScaleY;
						frameVo.width = MeasureManager.PFLToPixel(frameNode.@width,isCoverLayout) * layoutScaleX;
						frameVo.height = MeasureManager.PFLToPixel(frameNode.@height,isCoverLayout) * layoutScaleY;
						
						frameVo.grouptype = frameNode.@grouptype;
						frameVo.dateAction = frameNode.@dateaction;
						frameVo.stroke = (frameNode.@stroke == "true")?true:false;
						
						// setup fixed frames // only for canvas right now // EDIT : not for canvas only
						//if( Infos.isCanvas && frameNode.@move == "false") frameVo.fixed = true;
						if( frameNode.@move == "false") frameVo.fixed = true;
						
						frameVo.editable = (frameNode.@editable == "true")?true:false;
						frameVo.calBold = (frameNode.@bold == "true")?true:false;
						frameVo.calItalic = (frameNode.@italic == "true")?true:false;
						frameVo.calFontName = frameNode.@font;
						frameVo.dateIndex = frameNode.@dateindex;
						frameVo.hAlign = frameNode.@halign;
						if( frameNode.@showborder == "false") frameVo.showBorder = false;
							
						frameVo.color = RGBtoHEX(String(frameNode.@color).split(" ")[0],String(frameNode.@color).split(" ")[1],String(frameNode.@color).split(" ")[2]);
						frameVo.fillColor = RGBtoHEX(String(frameNode.@fillcolor).split(" ")[0],String(frameNode.@fillcolor).split(" ")[1],String(frameNode.@fillcolor).split(" ")[2]);
						frameVo.calFontSizeRaw = String(Number(frameNode.@size*layoutScaleX));
						frameVo.isDeletable = !isLayoutFixed(layoutVo.id);
						if(frameVo.grouptype != FrameVo.GROUP_TYPE_SPINE)
							frameVo.rotation = frameNode.@rotation;
						
						
						
												
						//CHECK and SET TYPE FRAME
						//if(frameVo.dateAction != "")
						if(isCalendarFrame(frameVo.dateAction))
						{
							frameVo.type = FrameVo.TYPE_CALENDAR;
							
						}
						else if(frameNode.@type == FrameVo.TYPE_PHOTO) // Type: if its the first frame, it should be a background, let make some checks as we're using the old XML and it is kinda formatted weirdly 
						{
							// ToBe a background, the frame must be bigger than the pagewidth and pageheight 
							// if true, that can only be a background
							//if(Number(frameNode.@left) < 0 && Number(frameNode.@top) <0) // old way to find background not working on every case
							if(Number(frameNode.@width) > Number(layoutNode.@pagewidth) && Number(frameNode.@height) > Number(layoutNode.@pageheight)) // background is a frame bigger than project
							{
								if(!bkgFound)
								{
									frameVo.type = FrameVo.TYPE_BKG;
									bkgFound = true;
								}
								else
									frameVo.type = FrameVo.TYPE_PHOTO;
							}
							else
							{
								frameVo.type = FrameVo.TYPE_PHOTO;
							}
						}
						else
							frameVo.type = frameNode.@type;
						
						
						//Calculate frame foto number (layout tab classification)
						if(frameVo.type == FrameVo.TYPE_PHOTO)
							layoutVo.photoNumber++;
						
						//////
						// TODO : HACK : Fontsize was a real issue, the fonts in xml do not seem to have correct information
						// to avoid mini size or other problems with fonts, we just apply a theheight rule
						// for covers only
						/*
						if(frameVo.type == FrameVo.TYPE_TEXT && isCoverLayout) {
							frameVo.fontSize = Math.floor(frameVo.height*.4);
							if(frameVo.fontSize > 25) frameVo.fontSize = 25; // 30
						}
						else 
						{
							if(Infos.isAlbum)
							{
								frameVo.fontSize = getFontSizeForAlbum();
							}
							else
							frameVo.fontSize = MeasureManager.getFontSize(frameNode.@size,isCoverLayout);// / 2.8346 * Infos.project.PFLMultiplier; 
						}
						*/
						var optimalHeight : Number = frameVo.width /4 ;
						if(frameVo.height*.8 < optimalHeight) optimalHeight = frameVo.height * .8;
						//var preferedFontSize : Number = (frameVo.height > frameVo.width)? Math.floor(frameVo.width*.4) : Math.floor(frameVo.height*.4); // security to be sure it fits in the frame
						var preferedFontSize : Number = Math.floor(optimalHeight); // security to be sure it fits in the frame
						
						// use 12,18,36,40 as default font size
						if(preferedFontSize > 40 ) preferedFontSize = 40 ;
						else if(preferedFontSize > 36 ) preferedFontSize = 36 ;
						else if(preferedFontSize > 18 ) preferedFontSize = 18 ;
						else if(preferedFontSize > 12 ) preferedFontSize = 12 ;
						//frameVo.fontSize = MeasureManager.getFontSize(frameNode.@size,isCoverLayout);;
						frameVo.fontSize = preferedFontSize;
						//if(Infos.isAlbum) frameVo.fontSize = getFontSizeForAlbum();
						
						//// END HACK /////
						
						
						
						//handle Cover frame positions
						if(isCoverLayout && frameVo.grouptype == FrameVo.GROUP_TYPE_NONE)
							applyCoverPositionModifications(frameVo,frameNode, layoutNode, layoutScaleX)
						
						//handle Spine
						if(frameVo.grouptype == FrameVo.GROUP_TYPE_SPINE)
							applySpineModification(frameVo);
						
						//handle Edition
						if(frameVo.grouptype == FrameVo.GROUP_TYPE_EDITION)
							applyEditionModification(frameVo);
						
						
						//override vAlign if frame Calendar
						if(frameVo.type == FrameVo.TYPE_CALENDAR)
						{
							frameVo.vAlign = (String(frameNode.@valign) != "")?String(frameNode.@valign):"top";
							if(frameVo.vAlign == "center") frameVo.vAlign = VerticalAlign.MIDDLE;							
						}
						
						//add this frame to frame list (we do not add background frames as we have created it outside for non canvas projects)
						if(frameVo.type != FrameVo.TYPE_BKG)
						{
							layoutVo.frameList.push(frameVo);
						}
					}
				}
				
				return layoutVo;
			}
			return null;
		}
		
		
		/**
		 * This function applies modification to match editorV1's layouts (which is shit, no offence)
		 * 
		 * Frame on cover are have spine dependencies. Mostly on their X position (for now)
		 * 
		 * Y stays the same
		 * X could change if the frame is positionned on the rigth of the cover, you then need to add the spine width to it...
		 * */
		private function applyCoverPositionModifications(frameVo:FrameVo, frameNode:XML, layoutNode:XML, layoutScaleX:Number):void
		{
			var rawCoverWidth:Number = getRawCoverWidth(layoutNode);
			var currentCoverWidth:Number = Infos.project.getCoverBounds().width;
			var spineDiff:Number =  (currentCoverWidth - rawCoverWidth);
			var coverWidth:Number = Infos.project.coverWidth;
			var frameLeftPosition:Number = frameNode.@left;
			frameVo.x = ( frameLeftPosition >= coverWidth)?frameVo.x+spineDiff:frameVo.x;
		}	
		
		private function getRawCoverWidth(layoutNode:XML, withSpine:Boolean = true):Number
		{
			var pageCoverWidth:Number = layoutNode.@pagewidth;
			var spineWidth:Number = layoutNode.frame.(attribute("grouptype") == FrameVo.GROUP_TYPE_SPINE).@width;
			if(!withSpine)
				pageCoverWidth = pageCoverWidth - spineWidth;
				
			return MeasureManager.PFLToPixel(pageCoverWidth,true);
		}
		
		/**
		 * This function applies modification to match editorV1's layouts (which is shit, no offence)
		 * 
		 * Spine are textFrame rotated 90°
		 * So the width become the height and vice versa
		 * Height is the coverHeight
		 * Width is related to the number of pages, and if paper is set to tru or false (ProjectVo helper getSpineWidth makes the math for us)
		 * Y is then the half of the coverHeight (since frames use centered content)
		 * X is the middle of the whole coverWidth
		 * */
		private function applySpineModification(frameVo:FrameVo):void
		{
			var safetyZone:Number = MeasureManager.millimeterToPixel(20)
			var tempWidth:Number = frameVo.width;
			frameVo.width = Infos.project.getCoverBounds().height - (safetyZone*2);
			frameVo.height = Infos.project.getSpineWidth();
			frameVo.y = frameVo.width/2 + safetyZone;
			frameVo.x = Infos.project.getCoverBounds().width/2;
			frameVo.rotation = -90;
			frameVo.hAlign = HorizontalAlign.CENTER;
			frameVo.vAlign = VerticalAlign.MIDDLE;
			
			// FORCE spine fontsize 
			//if(frameVo.fontSize > frameVo.height -2) frameVo.fontSize = Math.floor(frameVo.height)-2; // security to be sure the fontsize is not bigger than the height of the spine !
			frameVo.fontSize = Math.floor(frameVo.height*.6); // security to be sure the fontsize is not bigger than the height of the spine !
			//frameVo.fontSize = 99;
		}	
		
		/**
		 * This function applies modification to match editorV1's layouts (which is shit, no offence)
		 * 
		 * Edsition are textFrame rotated 90°
		 * So the width become the height and vice versa
		 * Width is the spineWidth
		 * Height is the width already set previously
		 * Y is the the bottom of the coverPageHeight minus the width (which the height... Hahaha so funny to read it again... hmmm sorry)
		 * X same as the Spine
		 * */
		private function applyEditionModification(frameVo:FrameVo):void
		{
			var tempWidth:Number = frameVo.width;
			frameVo.width = Infos.project.getSpineWidth();
			frameVo.height =  MeasureManager.millimeterToPixel(15);//frameVo.height;
			frameVo.y = Infos.project.getCoverBounds().height - frameVo.height/2 - MeasureManager.millimeterToPixel(5);
			frameVo.x = Infos.project.getCoverBounds().width/2;
			frameVo.hAlign = HorizontalAlign.CENTER;
			frameVo.vAlign = VerticalAlign.TOP;
			
			// force Edition fontsize
			frameVo.fontSize = Math.floor(frameVo.height*.5*.6); 
			
			//frameVo.rotation = 90;
			
			//toolBar allowance
			/*
			frameVo.allowBorderMove = false;
			frameVo.allowCrop = false;
			frameVo.allowInsideMove = false;
			frameVo.allowMove = false;
			frameVo.allowResize = false;
			frameVo.allowRotation = false;
			*/
			
			
			//font
			//frameVo.fontSize = MeasureManager.getFontSize(10,true);
		}	
		
		/**
		 * each layout has one background, created by the editor.
		 * We do not use the backgrounds created by the xml so we are sure to always have a background
		 */
		/*
		private function createFrameBackground():FrameVo
		{
			var frameVo:FrameVo = new FrameVo();
			frameVo.type = FrameVo.TYPE_BKG;
			frameVo.width = MeasureManager.PFLToPixel(Infos.project.width + 4);
			frameVo.height = MeasureManager.PFLToPixel(Infos.project.height + 4);
			frameVo.x = MeasureManager.PFLToPixel(Infos.project.width *.5);
			frameVo.x = MeasureManager.PFLToPixel(Infos.project.height *.5);
			frameVo.editable = true;
			//frameVo.fillColor = RGBtoHEX(String(frameNode.@fillcolor).split(" ")[0],String(frameNode.@fillcolor).split(" ")[1],String(frameNode.@fillcolor).split(" ")[2]);
			return frameVo;
		}
		*/
		
		
		
		/**
		 * Check if the the given node is  a calendar node or a simple one
		 * Base on the dateaction attribute, empty or not
		 * TODO: We need to make sure no other condition could return true
		 * */
		private function checkIsCalendar(dateActionList:XMLList):Boolean
		{
			for (var i:int = 0; i < dateActionList.length(); i++) 
			{
				if(isCalendarFrame( dateActionList[i] ))
				{
					if( dateActionList[i] != CalendarManager.DATEACTION_YEAR &&  dateActionList[i] != CalendarManager.DATEACTION_URL)
						return true;
				}                       
			}

			return false;
		}
		
		/**
		 * Check if the the given dateAction define a calendar frame or not
		 * */
		public function isCalendarFrame( dateAction : String ) : Boolean
		{
			if( dateAction &&  dateAction != "" )
				//&&    dateAction != CalendarManager.DATEACTION_YEAR 
				//&&  dateAction != CalendarManager.DATEACTION_URL)
				return true;
			
			return false;
		}
		
		/**
		 * Helper to convert RGB from XML to HEXA
		 * */
		private function RGBtoHEX(r:Number, g:Number, b:Number) :Number
		{
			
			return r*255 << 16 | g*255 << 8 | b*255;
			
		}
	}
}
class SingletonEnforcer{}