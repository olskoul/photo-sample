/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import be.antho.data.ResourcesManager;
	
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.ProductsCatalogue;
	
	import utils.Debug;
	

	/**
	 * CoverManager 
	 * Get cover infos
	 * Should include get bounds, spine bounds etc...
	 
	 * 
	 * */
	public class CoverManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		
		/**
		 * Constructor
		 */
		public function CoverManager(sf:SingletonEnforcer) { if(!sf) throw new Error("CoverManager is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:CoverManager;
		public static function get instance():CoverManager
		{
			if (!_instance) _instance = new CoverManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		
		/*public function get coverName():String
		{
			var coverVo:PageVo = Infos.project.coverVo;
			if(coverVo.coverType == ProductsCatalogue.COVER_CUSTOM)
				return ResourcesManager.getString("cover.custom.label");
			
			if(coverVo.coverType == ProductsCatalogue.COVER_LEATHER)
			{
				
			}
			
			return "?"
		}
		*/
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * returns: Cover Vo
		 * */
		public function getCoverVo():PageVo
		{
			return Infos.project.coverVo;
		}
		
		/**
		 * Id: leatherblack or linenblack
		 * returns: Leather Black or Linen Black Localized
		 * */
		public function getCoverLabelName(localizeIt:Boolean = true):String
		{
			var coverVo:PageVo = Infos.project.coverVo;
			if(!coverVo) return "?";
			if(coverVo.coverType == ProductsCatalogue.COVER_CUSTOM)
			{
				if(localizeIt)
					return ResourcesManager.getString("cover.custom.label");
				else
					return "Custom Cover";
			}
			
			if(coverVo.coverType == ProductsCatalogue.COVER_LEATHER)
			{
				return  getCoverLabelNameById(PageCoverClassicVo(coverVo).coverLabelName,PageCoverClassicVo(coverVo).coverFabric,localizeIt);
			}
			return "?";
		}
		
		/**
		 * Id: leatherblack or linenblack
		 * type: leather or linen
		 * returns: Leather Black or Linen Black
		 * 
		 * Helper to match old version of app structure and handlers
		 * */
		public function getCoverLabelNameById(id:String, type:String, localizeIt:Boolean = false):String
		{
			if(localizeIt)
				return ResourcesManager.getString("cover."+type+"."+id+".label");
			
			try{
				switch(type)
				{
					case ProductsCatalogue.COVER_FABRIC_LEATHER:
						return "Leather "+capitalize(String(id).split(ProductsCatalogue.COVER_FABRIC_LEATHER)[1]);
						break;
					case ProductsCatalogue.COVER_FABRIC_LINEN:
						return "Linen "+capitalize(String(id).split(ProductsCatalogue.COVER_FABRIC_LINEN)[1]);
						break;
				}	
			}
			catch(e:Error){
				Debug.warn("CoverManager.getCoverLabelById : error trying to find label name");	
			}
			return '';
		}
		
		/**
		 * Id: roundgold or roundsilver
		 * returns: Rounded gold (localized)
		 * 
		 * */
		public function getCornerLabel():String
		{
			var coverVo:PageVo = Infos.project.coverVo;
			return ResourcesManager.getString("cover.corner."+PageCoverClassicVo(coverVo).corners+".label");
		}
		
		/**
		 * Id: roundgold or roundsilver
		 * returns: Id
		 * 
		 * */
		public function getCorner():String
		{
			var coverVo:PageVo = Infos.project.coverVo;
			if(coverVo && coverVo is PageCoverClassicVo)
				return (coverVo as PageCoverClassicVo).corners;
			return "";
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Helper to capitalize first letter of the ids
		 * 
		 */
		private function capitalize(str:String):String {
			var capital:String = str.charAt(0);
			return capital.toUpperCase()+str.substr(1);
		}
		
	}
}
class SingletonEnforcer{}