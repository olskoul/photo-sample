/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	//import com.coreyoneil.collision.CollisionList;
	import com.google.analytics.debug.Info;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.AutoAlphaPlugin;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import comp.BackgroundItem;
	import comp.DraggableItem;
	import comp.DropTargetArea;
	import comp.Image;
	
	import data.Infos;
	import data.LayoutVo;
	import data.PageVo;
	
	import org.osflash.signals.Signal;
	
	import utils.AssetUtils;
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.Frame;
	import view.edition.FrameBackground;
	import view.edition.PageArea;
	import view.edition.pageNavigator.PageNavigatorItem;
	
	/**
	 * DragDrop manager
	 * The class is used to manager drag and drop of items from anywhere in the application
	 */
	public class DragDropManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// Drag&drop types 
		public static const TYPE_ALL : String = "TYPE_ALL";
		public static const TYPE_MENU_PHOTO : String = "TYPE_MENU_PHOTO";
		public static const TYPE_MENU_LAYOUT : String = "TYPE_MENU_LAYOUT";
		public static const TYPE_MENU_LAYOUT_CALENDAR : String = "TYPE_MENU_LAYOUT_CALENDAR";
		public static const TYPE_MENU_CLIPART : String = "TYPE_MENU_CLIPART";
		public static const TYPE_MENU_OVERLAYER : String = "TYPE_MENU_OVERLAYER";
		public static const TYPE_MENU_BKG : String = "TYPE_MENU_BKG";
		public static const TYPE_NAVIGATOR_SIMPLE : String = "TYPE_NAVIGATOR_SIMPLE";
		public static const TYPE_NAVIGATOR_SIMPLE_CALENDAR : String = "TYPE_NAVIGATOR_SIMPLE_CALENDAR";
		public static const TYPE_NAVIGATOR_CARDS_POSTCARD_BACK : String = "TYPE_NAVIGATOR_CARDS_POSTCARD_BACK";
		public static const TYPE_NAVIGATOR_LAYOUT_NOT_EDITABLE : String = "TYPE_NAVIGATOR_LAYOUT_NOT_EDITABLE";
		public static const TYPE_NAVIGATOR_DOUBLE : String = "TYPE_NAVIGATOR_DOUBLE";
		public static const TYPE_FOLDER : String = "TYPE_FOLDER";
		
		// signals
		public const ITEM_DROPPED : Signal = new Signal(DraggableItem, DropTargetArea);
		
		// vars
		private var stageRef : Stage;
		private var dragItem : DraggableItem;
		private var itemClone : Image;
		private var targets : Vector.<DropTargetArea> = new Vector.<DropTargetArea>(); // All targets registered in the manager
		private var itemTargets : Vector.<DropTargetArea> ;	// targets available for this item (its groupType)
		private var initPoint : Point; // initial point of drag
		
		private var curveContainer : Sprite = new Sprite();
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function DragDropManager(sf:SingletonEnforcer) { if(!sf) throw new Error("DragDropManager is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:DragDropManager;
		public static function get instance():DragDropManager
		{
			if (!_instance) _instance = new DragDropManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public function startDrag( item:DraggableItem ):void
		{
			Debug.log("DragDropManager.startDrag : " + item);
			
			// create stage ref if not existing
			if(!stageRef) stageRef = Infos.stage;
			if(itemClone) destroyClone();
			
			// store Item
			dragItem = item;
			
			// check current item target type possible
			itemTargets = new Vector.<DropTargetArea>();
			var target:DropTargetArea;
			for (var i:int = 0; i < targets.length; i++) 
			{
				target = targets[i];
				for (var j:int = 0; j < target.dropTypes.length; j++) 
				{
					if(target.dropTypes[j] == item.dropType) itemTargets.push(target);	
				}
			}
			//trace( "---> " + itemTargets.length + " DROP TARGET POSSIBLE on ("+targets.length+") ");
			
			
			// store initial position
			initPoint = new Point(dragItem.x+dragItem.width*.5, dragItem.y+dragItem.height*.5);
			initPoint = dragItem.parent.localToGlobal(initPoint);
			
			// create clone
			itemClone = AssetUtils.getImageClone(dragItem,true,false);
			if(!itemClone) return;
			
			itemClone.alpha = .8;
			itemClone.x = stageRef.mouseX;
			itemClone.y = stageRef.mouseY;

			// show after some delay
			TweenMax.delayedCall(.3,stageRef.addChild,[itemClone]); // trying this to allow double click event to be catch
			
			// listen to move event
			stageRef.addEventListener(MouseEvent.MOUSE_MOVE, moveItem);
			
			// listen to stop drag
			stageRef.addEventListener(MouseEvent.MOUSE_UP, dropItem);
			stageRef.addEventListener(Event.MOUSE_LEAVE, dropItem);
			
			// distance curve
			if(item.distanceCurve){
				stageRef.addChild(curveContainer);
				curveContainer.graphics.clear();
			}
		}
		
		
		/**
		 * Add a drop target to the drop target list
		 * The drop target list contains all the drop target on wich items can be placed
		 */
		public function addDropTarget( target : DropTargetArea, typesAllowed : Array):void
		{
			if(!target) {
				Debug.warn("DragDropManager.addDropTarget : must receive valid target");
				return;
			}
			target.dropTypes = typesAllowed;
			targets.push(target);
			// sort targets
		}
		
		/**
		 * Remove a drop target from the drop target list
		 * 
		 */
		public function removeDropTarget( target : DropTargetArea ):void
		{
			if(!target) return;
			target.highLight(false, dragItem);
			if(targets.lastIndexOf(target) != -1) targets.splice(targets.lastIndexOf(target),1);
		}
		
		
		/**
		 * 
		 * Get the dropType based on page vo
		 * 
		 */
		public function getDropTypeFromPageVo(pageVo:PageVo):String
		{
			//Calendar Pages
			if(pageVo.isCalendar)
				return TYPE_NAVIGATOR_SIMPLE_CALENDAR;
			
			if(pageVo.isPostCardBack)
				return TYPE_NAVIGATOR_CARDS_POSTCARD_BACK;
			
			if(LayoutManager.instance.isLayoutFixed(pageVo.layoutVo.id))
				return TYPE_NAVIGATOR_LAYOUT_NOT_EDITABLE;
			
			//Add other conditions here
			
			return TYPE_NAVIGATOR_SIMPLE;
		}
		
		/**
		 * 
		 * Get the dropType based on layout vo
		 * 
		 */
		public function getDropTypeFromLayoutVo(layoutVo:LayoutVo):String
		{
			//Calendar Pages
			if(layoutVo.isCalendar)
				return TYPE_MENU_LAYOUT_CALENDAR;
			
			//Add other conditions here
			
			return TYPE_MENU_LAYOUT;
		}
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		private function moveItem(e:MouseEvent):void
		{
			itemClone.x = stageRef.mouseX;
			itemClone.y = stageRef.mouseY;
			
			var targetFound:DropTargetArea = checkIfOnTargets();
			
			if(dragItem.distanceCurve){
				curveContainer.graphics.clear();
				var curveColor : Number = (targetFound)? Colors.GREEN : Colors.YELLOW;
				curveContainer.graphics.beginFill(curveColor, 1);
				curveContainer.graphics.drawCircle(initPoint.x, initPoint.y, 10);
				curveContainer.graphics.endFill();
				
				curveContainer.graphics.lineStyle(2, curveColor);
				curveContainer.graphics.moveTo(initPoint.x, initPoint.y);
				curveContainer.graphics.curveTo(initPoint.x - (initPoint.x-itemClone.x)*.5, initPoint.y-200, itemClone.x, itemClone.y);
				curveContainer.graphics.endFill();
			}
			
			if(itemClone) itemClone.alpha = (targetFound)? .2 : .8;
		}
		
		/**
		 *
		 */
		private function checkIfOnTargets( isDrop : Boolean = false ):DropTargetArea
		{
			//Reverse targets order to avoid depth issue
			/* OLD METHOD
			targets.reverse();
			var itemBounds:Rectangle = itemClone.getBounds(stageRef);
			var target:DropTargetArea, targetBounds:Rectangle;
			for (var i:int = 0; i < targets.length; i++) 
			{
				target = targets[i];
				targetBounds = target.getBounds(stageRef);
				if(itemBounds.intersects(targetBounds)){
					target.highLight(true);
					//Reverse targets order back to normal
					targets.reverse();
					return target;
				}
				else target.highLight(false);
			}
			//Reverse targets order back to normal
			targets.reverse();
			*/
			
			// OLD METHOD 2
			/*
			targets.reverse();
			var target:DisplayObject; 
			var targetFound : DisplayObject;
			for (var i:int = 0; i < targets.length; i++) 
			{
				target = targets[i];
				if(!targetFound && itemClone.hitTestObject(target)){
					//target.highLight(true);
					target.alpha = .2;
					targetFound = target;
				}
				else target.alpha = 1;//target.highLight(false);
			}
			
			targets.reverse();
			if(targetFound) return targetFound;
			*/
			
			// METHOD 3 
			// With mouse only and target reverse system
			/*
			itemTargets.reverse();
			var target:DropTargetArea; 
			var targetFound : DropTargetArea;
			var usePixelCheck : Boolean;
			for (var i:int = 0; i < itemTargets.length; i++) 
			{
				target = itemTargets[i];
				usePixelCheck = target.usePixelCheck;
				if(!targetFound && target.hitTestPoint(stageRef.mouseX, stageRef.mouseY,usePixelCheck))
				{
					if (target.isCustom)
					{
						if(target.checkCustomDrop()){
							targetFound = target;
						}
					} 
					else {
						target.highLight(true);
						targetFound = target;	
						Debug.log("Target FOUND : "+target);
					}
				}
				else {
					target.highLight(false);
				}
			}
			
			itemTargets.reverse();
			if(targetFound) return targetFound;
			*/
			
			
			// METHOD 4
			try{
				// With mouse only and no target reverse system but priorities
				var target:DropTargetArea; 
				var targetFound : DropTargetArea;
				var usePixelCheck : Boolean;
				for (var i:int = 0; i < itemTargets.length; i++) 
				{
					target = itemTargets[i];
					target.highLight(false, null);
					usePixelCheck = target.usePixelCheck;
					if(target.hitTestPoint(stageRef.mouseX, stageRef.mouseY,usePixelCheck))
					{
						if (target.isCustomDropArea)
						{
							if(target.checkCustomDrop(dragItem)){
								targetFound = checkTargetPriorities(target, targetFound);
							}
						} 
						else targetFound = checkTargetPriorities(target, targetFound);
						
						// Debug.log("DragDropManager >Target FOUND : "+target);
					}
				}
				
				if(targetFound){
					targetFound.highLight(true, dragItem);
					return targetFound;
				}
			}
			catch(e:Error)
			{
				// error in check
				Debug.warn("DragDropManager.checkIfOnTargets error : "+e.message);
				dropItem(null, true);
				return null;
				
			}
			
			return null;
		}
		
		
		/**
		 * check priority between multiple drop targets and returns the best one
		 */
		private function checkTargetPriorities( target1 : DropTargetArea, target2 : DropTargetArea ):DropTargetArea
		{
			if( target1 is PageArea && target2 is Frame) return target2; // Page area vs frame
			else if (target1 is FrameBackground && target2 is Frame) return target2; // frame background vs frame
			else if (target1 is Frame && target2 is Frame && target2.parent.getChildIndex(target2) > target1.parent.getChildIndex(target1) ) return target2; // frame depth
			//else if (target1 is PageNavigatorItem && !(target2 is PageNavigatorItem)) return target2;
			return target1;
		}
		
		
				
		/**
		 * Drop Target
		 */
		private function dropItem(e:Event, forceReturn:Boolean = false):void
		{
			Debug.log("DragDropManager.dropItem");
			
			//TweenMax.killDelayedCallsTo(stageRef.addChild,[itemClone]);
			TweenMax.killDelayedCallsTo(stageRef.addChild ); // TODO check if this works as we did change the Tweenmax version	
		
			
			// kill listeners
			stageRef.removeEventListener(MouseEvent.MOUSE_MOVE, moveItem);
			stageRef.removeEventListener(MouseEvent.MOUSE_UP, dropItem);
			stageRef.removeEventListener(Event.MOUSE_LEAVE, dropItem);
			
			if(forceReturn)
			{
				returnToInitPoint();
			}
			else // check if we are on target
			{
				var dropTarget: DropTargetArea = checkIfOnTargets( true );
				Debug.log("DragDropManager.dropItem > dropTarget = "+ dropTarget );
				
				if(dropTarget) {
					ITEM_DROPPED.dispatch(dragItem, dropTarget);
					// on drop force remove hightlight
					dropTarget.highLight(false, null);
					destroyClone(true);
				}
				else returnToInitPoint();
			}
			
			// destroy collision list
			/*
			if(collisionList){
				collisionList.dispose();
				collisionList = null;
			}
			*/
			
			dragItem.mouseUpHandler();
			itemTargets = null;
			if(curveContainer && curveContainer.parent) curveContainer.parent.removeChild(curveContainer);
		}
		
		/**
		 *
		 */
		private function returnToInitPoint():void
		{
			if(Math.abs(itemClone.x -initPoint.x) < 50 && Math.abs(itemClone.y -initPoint.y) < 50 ) destroyClone();
			else TweenMax.to(itemClone, .7, {x:initPoint.x, autoAlpha:0, y:initPoint.y, ease:Strong.easeOut, onComplete:destroyClone});
		}
		
		
		/**
		 *
		 */
		private function destroyClone( withAnim :Boolean = false ):void
		{
			if(!itemClone) return;
			var duration : Number = (withAnim)? .4 : 0;
			TweenMax.killTweensOf(itemClone);
			TweenMax.to(itemClone, duration, {scale:0, ease:Strong.easeOut, onCompleteParams:[itemClone], onComplete:function(target:DisplayObject):void{
				if(target.parent) target.parent.removeChild(target);
				target = null;
			}});
			
		}
		
		
	}
}
class SingletonEnforcer{}