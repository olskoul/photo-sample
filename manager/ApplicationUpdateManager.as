/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2014
 ****************************************************************/
package manager
{
	import com.bit101.components.ProgressBar;
	import com.riaspace.nativeApplicationUpdater.NativeApplicationUpdater;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.system.Capabilities;
	
	import air.update.events.DownloadErrorEvent;
	import air.update.events.StatusUpdateErrorEvent;
	import air.update.events.StatusUpdateEvent;
	import air.update.events.UpdateEvent;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.DataLoadingManager;
	
	import data.Infos;
	
	import offline.manager.OfflineAssetsManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Debug;
	
	import view.popup.PopupAbstract;


	/**
	 * Application update manager
	 * > check for app build update
	 * > check for app file content update
	 */
	public class ApplicationUpdateManager
	{
		/**
		 * ------------------------------------ POPERTIES -------------------------------------
		 */
		
		// signals
		public static const APP_UPDATE_COMPLETE : Signal = new Signal();
		public static const CONTENT_UPDATE_COMPLETE:Signal = new Signal();
		
		// props
		private var _buildUpdateUrl : String; // pour l'instant on garde mon serveur, comme ca on peut encore tester facilement.. //"http://www.tictacphoto.com/offline/appUpdate.xml";//  "http://www.antho.be/prod/tictac/desktop/appUpdate.xml";
		private var appUpdater : NativeApplicationUpdater;
		private var _checkForContentUpdateToo : Boolean = false;
		private var userAskedForUpload:Boolean = false;
		
		
		/**
		 * ------------------------------------ GETTERS -------------------------------------
		 */
		
		public function get buildUpdateUrl():String
		{
			if(Debug.USE_DEBUG_UPDATE)
			{
				if(Infos.REBRANDING_ID == "")
					return "http://www.antho.be/prod/tictac/dev/appUpdate.xml";
				else
					return "http://www.antho.be/prod/tictac/dev/updates/"+Infos.REBRANDING_ID+"/appUpdate.xml"
			}
			else
			{
				if(Infos.REBRANDING_ID == "")
					return "http://cdn-1.tictacphoto.com/offline2/updates/update.xml";
				else
					return "http://cdn-1.tictacphoto.com/offline2/updates/"+Infos.REBRANDING_ID+"/update.xml"
			}
		}
		
		public function getAlternateUpdateUrl():String
		{
			var os:String = Capabilities.os.toLowerCase();
			var rebrandingInstallerUrl : String = (!Infos.config.isRebranding)? "http://cdn-1.tictacphoto.com/download/TicTacPhoto_Install" : Infos.config.rebrandingInstallerUrl;
			
			if (os.indexOf("win") > -1)
			{
				return rebrandingInstallerUrl+".exe";
			}
			else if (os.indexOf("mac") > -1)
			{
				return rebrandingInstallerUrl+".dmg";
			}
			
			return rebrandingInstallerUrl+".exe";
		}
		
		/**
		 * ------------------------------------ SINGLETON -------------------------------------
		 */
		
		public function ApplicationUpdateManager(sf:SingletonEnforcer) { if(!sf) throw new Error("ApplicationUpdateManager is a singleton, use instance"); }
		private static var _instance:ApplicationUpdateManager;

		

		public static function get instance():ApplicationUpdateManager
		{
			if (!_instance)	_instance = new ApplicationUpdateManager(new SingletonEnforcer());
			return _instance;
		}
		
		/**
		 * ------------------------------------ CHECK APP BUILD UPDATE -------------------------------------
		 */
		
		/**
		 * check if there is an available build update
		 */ 
		public function checkForAppBuildUpdate( checkForContentUpdateToo:Boolean = false ):void 
		{
			_checkForContentUpdateToo = checkForContentUpdateToo;
			
			
			// first check for available update
			appUpdater = new NativeApplicationUpdater();
			appUpdater.isNewerVersionFunction = checkIfNewerVersion;
			appUpdater.updateURL = buildUpdateUrl + "?t="+new Date().time;
			
			// listeners
			appUpdater.addEventListener(UpdateEvent.INITIALIZED, onInitialize);
			appUpdater.addEventListener(UpdateEvent.CHECK_FOR_UPDATE, onCheckForUpdate);
			appUpdater.addEventListener(UpdateEvent.DOWNLOAD_COMPLETE, onDownloadCompleted);
			appUpdater.addEventListener(ProgressEvent.PROGRESS, onDownloadProgress);
			appUpdater.addEventListener(StatusUpdateEvent.UPDATE_STATUS, onUpdaterStatusChange);
			appUpdater.addEventListener(ErrorEvent.ERROR, onUpdaterError);
			appUpdater.addEventListener(StatusUpdateErrorEvent.UPDATE_ERROR, onUpdaterError);
			appUpdater.addEventListener(DownloadErrorEvent.DOWNLOAD_ERROR, onUpdaterError);
			
			// initialize
			appUpdater.initialize();
		}
		private function onInitialize(event:UpdateEvent):void 
		{
			Debug.log("ApplicationUpdateManager.onInitialize > start check ");
			appUpdater.checkNow();
		}
		private function onCheckForUpdate(e:UpdateEvent):void
		{
			e.preventDefault(); // prevent the default behavior of native application updater so we can insert our popup
			appUpdater.checkForUpdate();
		}
		private function onUpdaterStatusChange(e:StatusUpdateEvent):void
		{
			Debug.log("=> App Status update : "+e.toString());
			
			if (e.available){
				DataLoadingManager.instance.hideLoading(); // be sure to allow user to interact with popup
				//appUpdater.downloadUpdate();
				var description : String = ResourcesManager.getString("popup.newupdate.description");
				description = description.split("@@updateurl@@").join(getAlternateUpdateUrl());
				
				// non forced update
				if(!appUpdater.forced)
				{
					PopupAbstract.Alert(ResourcesManager.getString("popup.newupdate.title") + " ("+appUpdater.updateVersion+")",description ,true,
						// ON OK
						function(e:*):void{
							userAskedForUpload = true;
							startDownload();
						},
						// ON CANCEL
						function(e:*):void
						{
							noBuildUpdateAvailable();
						});
				}
				else
				{
					startDownload();
				}
			}
			else noBuildUpdateAvailable();
		}
		private function startDownload():void
		{
			DataLoadingManager.instance.updateText(ResourcesManager.getString("popup.newupdate.download") + " : 0%");
			DataLoadingManager.instance.showLoading();
			appUpdater.downloadUpdate();
		}
		private function onDownloadProgress(e:ProgressEvent):void
		{
			DataLoadingManager.instance.updateText(ResourcesManager.getString("popup.newupdate.download") +" : "+Math.round((e.bytesLoaded/e.bytesTotal)*100)+"%");
		}
		private function onDownloadCompleted(e:UpdateEvent):void
		{
			DataLoadingManager.instance.updateText(ResourcesManager.getString("popup.newupdate.install"));
			appUpdater.installUpdate();
		}
		private function onUpdaterError(e:Event):void
		{
			Debug.warn("ApplicationUpdateManager.onUpdaterError : "+e.toString());
			DataLoadingManager.instance.hideLoading(); // remove it in case of
			
			if(userAskedForUpload){
				var description : String = ResourcesManager.getString("popup.newupdate.error.description");
				description = description.split("@@updateurl@@").join(getAlternateUpdateUrl());
				PopupAbstract.Alert(ResourcesManager.getString("popup.newupdate.error.title"), description, false, noBuildUpdateAvailable );	
			}
			else
			{
				noBuildUpdateAvailable();
			}
			
		}
		
		
		
		/**
		 * no build update available
		 */
		private function noBuildUpdateAvailable(e:* = null):void
		{
			Debug.log("ApplicationUpdateManager.noBuildUpdateAvailable");
			DataLoadingManager.instance.hideLoading(); // remove it in case of
			if (_checkForContentUpdateToo) checkForAppContentUpdates();
			else APP_UPDATE_COMPLETE.dispatch();
		}
		
		// custom project version check
		private function checkIfNewerVersion(currentVersion:String, updateVersion:String):Boolean
		{
			Debug.log("ApplicationUpdaterManager comparing version : "+currentVersion + " with " + updateVersion);
			// remove dots
			currentVersion = currentVersion.split(".").join("");
			updateVersion = updateVersion.split(".").join("");
			// to int
			var curr : Number = parseInt(currentVersion);
			var upd : Number = parseInt(updateVersion);
			
			return upd > curr;
		}
		
		/**
		 * ------------------------------------ CHECK APP CONTENT UPDATE -------------------------------------
		 */
		
		/**
		 * check application content file update
		 * OFFLINE ONLY
		 */
		public function checkForAppContentUpdates():void
		{
			Debug.log("ApplicationUpdateManager.checkAppContentUpdates");
			
			if(Infos.INTERNET && !Debug.DO_NOT_DO_APP_CONTENT_UPDATE)
			{
				ServiceManager.instance.checkUpdateDetails(function(result:*):void
				{
					try
					{
						var updateDetails:Object = JSON.parse(result);			
						OfflineAssetsManager.UPDATE_FINISHED.addOnce(appUpdateComplete);
						OfflineAssetsManager.instance.update(updateDetails);	
					}
					catch(e:Error)
					{
						Debug.warn("ApplicationUpdateManager.checForAppContentUpdates error : "+e.message);
						appUpdateComplete();
					}
					
					
				},
					function(msg:String = null):void
					{
						Debug.warn("Error while checking update details : "+msg);
						appUpdateComplete();
					});	
			}
			else
			{
				if(Debug.DO_NOT_DO_APP_CONTENT_UPDATE)
				Debug.log("DO_NOT_DO_APP_CONTENT_UPDATE flag is set to: "+Debug.DO_NOT_DO_APP_CONTENT_UPDATE+", skip the updates");
				else
					Debug.warn("No INTERNET, skip the updates");
				appUpdateComplete();
			}
		}
		private function appUpdateComplete():void
		{
			CONTENT_UPDATE_COMPLETE.dispatch();
		}
		
		
		
	}
}
class SingletonEnforcer{}