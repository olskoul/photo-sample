/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import com.adobe.serialization.json.JSONDecoder;
	import com.greensock.TweenMax;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.geom.Rectangle;
	import flash.net.URLLoaderDataFormat;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	
	CONFIG::offline
	{
		import flash.filesystem.File;
		import be.antho.air.FileUtils;
	}
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.DataLoadingManager;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	import br.com.stimuli.loading.loadingtypes.LoadingItem;
	
	import data.AlbumVo;
	import data.BackgroundVo;
	import data.FrameVo;
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.PhotoVo;
	import data.ProductsCatalogue;
	import data.ProjectCreationParamsVo;
	import data.ProjectVo;
	
	import offline.manager.LocalStorageManager;
	import offline.manager.PriceManager;
	
	import org.osflash.signals.Signal;
	
	import ru.inspirit.net.MultipartURLLoader;
	
	import service.ServiceManager;
	
	import utils.Colors;
	import utils.Debug;
	import utils.ProjectXMLSizeExporter;
	import utils.TextFormats;
	import utils.debug.ProjectDebugString;
	
	import view.dashboard.Dashboard;
	import view.edition.EditionArea;
	import view.menu.topmenu.TopBar;
	import view.popup.PopupAbstract;
	import view.popup.PopupProjectExplorer;
	import view.uploadManager.PhotoUploadManager;
	import offline.data.OfflineProjectManager;
	import flash.events.IOErrorEvent;
	import flash.errors.IOError;
	import flash.profiler.showRedrawRegions;
	import flash.display.BitmapData;
	import offline.data.OfflinePhotoLoader;
	import utils.BitmapUtils;
	import view.menu.lefttabs.photos.PhotosTabOffline;
	import utils.debug.TimeLog;
	import offline.ProjectPatcher;

	/**
	 * Project manager
	 * The class is used to manage project vo (LOAD/SAVE/EDIT..)
	 */
	public class ProjectManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		//Public Static const
		public static const PHOTO_SORT_ON_DATE_ASC:String = "PHOTO_SORT_ON_DATE_ASC";
		public static const PHOTO_SORT_ON_DATE_DES:String = "PHOTO_SORT_ON_DATE_DES";
		public static const PHOTO_SORT_ON_NAME_ASC:String = "PHOTO_SORT_ON_NAME_ASC";
		public static const PHOTO_SORT_ON_NAME_DES:String = "PHOTO_SORT_ON_NAME_DES";
		public static const DOWNLOAD_FLAG_PRICE:String = "price";
		// signals
		public static const PROJECT_READY : Signal = new Signal();
		public static const PROJECT_UPDATED : Signal = new Signal();
		public static const PROJECT_SAVE_COMPLETE : Signal = new Signal();
		public static const PROJECT_SAVE_CANCEL : Signal = new Signal();
		public static const PROJECT_DO_NOT_SAVE : Signal = new Signal();
		public static const PRICE_UPDATED : Signal = new Signal();
		//public static const PROJECT_SELECTED : Signal = new Signal();
		public static const PROJECT_NO_PROJECT_SELECTED : Signal = new Signal();
		public static const COVER_CHANGED : Signal = new Signal();
		public static const PHOTO_VO_MATCH_COMPLETE : Signal = new Signal(); // when photoVo map (match between left list and frames is done )
		public static const UNLOAD_PROJECT : Signal = new Signal();
		
		public static const PROJECT_OPEN_CONTINUE : Signal = new Signal();
				
		// public
		public var helper:ProjectManagerHelper;
		public var documentsXML : XML;
		public var projectsXML : XML;
		public var layoutsXML : XML;
		public var menuXML : XML;
		public var coversDocumentXML:XML;		
		public var coversClassicOptionsXML:XML;
		public var backgroundsXML:XML;
		public var clipartsXML:XML;
		public var overlayersXML:XML;
		public var customBackgroundsXML:XML;
		public var customLayoutXML:XML;
		public var priceListXML:XML;
		public var optionPriceListXML:XML;
		public var canvasCustomPriceListXML:XML;
		public var newsXML:XML;
		public var ready:Boolean; //used if no project type is selected, then we lauch the wizard (project tab)
		public var forceNewProject:Boolean; //used when there is a conflict ex: Project classname missmatch, trying to load an exisiting project ID that belong to another classname etc...
		public var selectedAlbumVo : AlbumVo; // currently selected albumVo in photo manager (if there is one)
		
		// private
		private var loader:BulkLoader = new BulkLoader("ProjectManager");
		private var saveLoader : BulkLoader = new BulkLoader("SaveLoader");
		private var saveUploader : MultipartURLLoader;
		private var projectExplorer : PopupProjectExplorer;
		
		// datas
		private var _project:ProjectVo;
		private var _projectName : String; 	// project name retrieved from session or prefs
		private var _projectId : String; 	// project id retrieved from session or prefs
		
		// flag for save as
		private var isSaveAs : Boolean = false;
		private var _nameBeforeSave : String; // when saving project, store previous project name so we can know if user did rename project
		
		//Donwload asset flag by classpath
		private var downloadFlags:Object; //ex : downloadFlags["albums"] = true => Do not download assets xml again //// Same for each claspath
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function ProjectManager(sf:SingletonEnforcer) { if(!sf) throw new Error("ProjectManager is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:ProjectManager;
		public static function get instance():ProjectManager
		{
			if (!_instance)
			{
				_instance = new ProjectManager(new SingletonEnforcer())
			}
			return _instance;
		}
		
		public static function reset():void
		{
			/*BulkLoader.removeAllLoaders();
			PROJECT_READY.removeAll();
			PROJECT_UPDATED.removeAll();
			PROJECT_SAVE_COMPLETE.removeAll();
			PROJECT_DO_NOT_SAVE.removeAll();
			PRICE_UPDATED.removeAll();
			PROJECT_NO_PROJECT_SELECTED.removeAll();
			COVER_CHANGED.removeAll();
			PHOTO_VO_MATCH_COMPLETE.removeAll();
			UNLOAD_PROJECT.removeAll();
			_instance = null;*/
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER SETTER
		////////////////////////////////////////////////////////////////
		
		public function get project():ProjectVo{ return _project };
		public function get coverText():Boolean{ 
			var coverVo : PageVo = project.coverVo;
			if(coverVo && coverVo is PageCoverClassicVo){
				return (coverVo as PageCoverClassicVo).hasCoverText;
			}
			return false;
		}
		public function get cornersType():String{
			if(project && project.coverVo && project.coverVo is PageCoverClassicVo){
				return (project.coverVo as PageCoverClassicVo).corners;
			}
			return "None";
		}
		
		public function get needSave():Boolean{ 
			return ( Infos.project && UserActionManager.instance.actionsSinceLastSave > 0)? true : false;	
		}
		
		
		public function get hasPrintWarning():Boolean
		{
			var page : PageVo;
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				page = project.pageList[i];
				if ( page.hasPrintWarning ) {
					Debug.log("Project has print warning for : "+page.index);
					return true;
				}
			}
			
			return false;	
		}
		
		public function get hasEmptyPages():Boolean{ 
			var page : PageVo;
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				page = project.pageList[i];
				if (!(page is PageCoverClassicVo) && page.isEmpty ) {
					Debug.log("Project has emtpy page : "+page.index);
					return true;
				}
			}	
			return false;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * init project manager
		 * - load documents.xml
		 * - load projects.xml
		 * Check if saved version or create new project
		 */
		public function init():void
		{
			Debug.log("ProjectManager.init");
			
			// TODO : urls should be created with
			var classPath:String = Infos.session.classname+"/"; 
			var xmlPath : String = Infos.config.getValue("xmlPath");
			var timeStamp:String = "?t="+new Date().time;
			var documentsUrl : String = xmlPath + classPath + "documents.xml"+timeStamp;
			var projectsUrl : String = xmlPath + classPath + "projects.xml"+timeStamp;
			var layoutsUrl : String = xmlPath + classPath + "layouts.xml"+timeStamp;
			var menuUrl : String = xmlPath + classPath + "menu.xml"+timeStamp;
			var coversDocumentsUrl : String = xmlPath + classPath + "covers_documents.xml";
			var coversClassicOptionsUrl : String = xmlPath + classPath + "covers_classic_options.xml";
			var fixedPath:String = (classPath == "calendars/")?"calendar/":classPath;
			var backgroundsUrl : String = Infos.config.serverNormalUrl + "/" + fixedPath + "assets/xml/"+ classPath + "backgrounds.xml"+timeStamp;
			var clipartUrl : String = Infos.config.serverNormalUrl + "/flex/loadcliparts.php";
			var overLayerUrl : String = Infos.config.serverNormalUrl + "/flex/loadoverlays.php";
			var customLayoutUrl : String = Infos.config.serverNormalUrl + "/flex/custom.php?xml=custom.xml";
			var customBackgroundUrl : String = Infos.config.serverNormalUrl + "/flex/custombkg.php?action=load";
			
			var priceListUrl : String = Infos.config.serverApiUrl + "/pricing.php?vendor="+Infos.config.vendorId+"&build="+Infos.buildVersion;
			var optionsPriceList : String = Infos.config.serverApiUrl + "/options.php?vendor="+Infos.config.vendorId+"&build="+Infos.buildVersion;
			var canvasCustomPriceList : String = Infos.config.serverApiUrl + "/pricing.php?vendor="+Infos.config.vendorId+"&ccp=1"+"&build="+Infos.buildVersion;
			var newsUrl : String = Infos.config.newsXMLUrl+timeStamp;//"http://www.smileasyoudoit.com/customers/tictac/servicetemp/news.xml"+timeStamp;

			// Change XML's URL if is_DESKTOP 
			CONFIG::offline
			{
				//XMLs
				var offlineAppFilePath:String = Infos.offlineAppFilePath
				documentsUrl = offlineAppFilePath + classPath + "documents.xml";
				projectsUrl = offlineAppFilePath + classPath + "projects.xml";
				layoutsUrl = offlineAppFilePath + classPath + "layouts.xml";
				menuUrl = offlineAppFilePath + classPath + "menu.xml";
				customBackgroundUrl = Infos.offlineCustomBackgroundsXML;
				customLayoutUrl = Infos.offlineCustomLayoutsXML;
				coversDocumentsUrl = offlineAppFilePath + classPath + "covers_documents.xml";
				coversClassicOptionsUrl = offlineAppFilePath + classPath + "covers_classic_options.xml";

			}
			
			
			var skipAssetsXMLDownload:Boolean = false; // Skip assest download if already done
			var skipPricesXMLDownload:Boolean = false; // Skip price list download if already done
			
			if(Infos.IS_DESKTOP)
			{
				if(!downloadFlags) 
				{
					downloadFlags = {};
					downloadFlags[Infos.session.classname] = skipPricesXMLDownload = skipAssetsXMLDownload = false;
				}
				else
				{
					skipAssetsXMLDownload = downloadFlags[Infos.session.classname];
					skipPricesXMLDownload = downloadFlags[DOWNLOAD_FLAG_PRICE];
				}
			}
			
			// init loader
			loader.addEventListener(BulkProgressEvent.COMPLETE, onLoadComplete);
			loader.addEventListener(BulkProgressEvent.PROGRESS, onLoadProgress);
			loader.addEventListener(BulkLoader.ERROR, onLoadError);
			loader.add(documentsUrl, {id:"documents"});
			loader.add(projectsUrl, {id:"projects"});
			loader.add(layoutsUrl, {id:"layouts"});
			loader.add(menuUrl, {id:"menu"});
			loader.add(newsUrl, {id:"news",type:BulkLoader.TYPE_XML});
			
			if(!skipAssetsXMLDownload && Infos.INTERNET)
			{
				loader.add(backgroundsUrl, {id:"backgrounds"});
				loader.add(clipartUrl, {id:"cliparts",type:BulkLoader.TYPE_XML});
				loader.add(overLayerUrl, {id:"overlayers",type:BulkLoader.TYPE_XML});
			}
			
			if(!skipPricesXMLDownload && Infos.INTERNET || !Infos.IS_DESKTOP)
			{
				loader.add(priceListUrl, {id:"priceList",type:BulkLoader.TYPE_XML});
				loader.add(optionsPriceList, {id:"optionPriceList",type:BulkLoader.TYPE_XML});
				loader.add(canvasCustomPriceList, {id:"canvasCustomPriceList",type:BulkLoader.TYPE_XML});
			}
							
			//Load additional XML if product is an Album
			if(Infos.session.classname == ProductsCatalogue.CLASS_ALBUM)
			{
				loader.add(coversDocumentsUrl, {id:"coversDocument"});
				loader.add(coversClassicOptionsUrl, {id:"coverClassicOptions"});
				loader.add(customLayoutUrl, {id:"customLayout",type:BulkLoader.TYPE_XML});
				loader.add(customBackgroundUrl, {id:"customBackgrounds",type:BulkLoader.TYPE_XML}); // TODO : reset this !
			}
			
			loader.start();
			
			// show loading
			DataLoadingManager.instance.showLoading();
			
		}
		private function onLoadProgress(e:BulkProgressEvent):void
		{
			var pct:Number = Math.floor(e.ratioLoaded*100);
			DataLoadingManager.instance.updateText(ResourcesManager.getString("loading.project.resources") + " : " + pct + "%");
		}
		
		
		/**
		 * when project resources are loaded
		 */
		private function onLoadComplete(e:BulkProgressEvent):void
		{
			DataLoadingManager.instance.hideLoading();
			Debug.log("ProjectManager.onLoadComplete (resources)");
			
			// General XML
			documentsXML = loader.getXML("documents");
			projectsXML = loader.getXML("projects");
			layoutsXML = loader.getXML("layouts");
			menuXML = loader.getXML("menu");
			coversDocumentXML = loader.getXML("coversDocument");
			coversClassicOptionsXML = loader.getXML("coverClassicOptions");
			
			// Price XML 
			priceListXML = loader.getXML("priceList");
			optionPriceListXML = loader.getXML("optionPriceList");
			canvasCustomPriceListXML = loader.getXML("canvasCustomPriceList");
			//News XMl
			newsXML = loader.getXML("news");
			
			//Condition for offline: 
			if(Infos.IS_DESKTOP)
			{
				//Have the assets xml for the current classname been loaded in this session or not, if yes, skip it, if not do it, then process it. 
				if(Infos.INTERNET && downloadFlags && downloadFlags[Infos.session.classname] != true)
				{
					// Assets XML
					backgroundsXML = loader.getXML("backgrounds");
					clipartsXML = loader.getXML("cliparts");
					overlayersXML = loader.getXML("overlayers");
					
					//process Assets XML			
					processAssetsXML();
				}
				else
				{
					//go straight to custom XML processing
					processCustomXML();
				}
			}
			else
			{
				//we are on the web version, just load the assets xml as usual
				backgroundsXML = loader.getXML("backgrounds");
				clipartsXML = loader.getXML("cliparts");
				overlayersXML = loader.getXML("overlayers");
				processAssetsXML();
			}
			
			
			////////////////////////////////////////////////////////////////////////////////
			///////// DEBUG EXPORT PROJECT SIZE    ////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			if(Debug.EXPORT_PROJECT_SIZE)
			{
				var pe : ProjectXMLSizeExporter = new ProjectXMLSizeExporter( projectsXML, documentsXML, coversDocumentXML );
				pe.traceProjectsSize();
			}
			////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////
			
		} 
		
		
		/**
		 * Process assets xml (bkg, cliparts, overlayers)
		 * Start with bkg
		 * Async
		 */
		private function processAssetsXML():void
		{
			Debug.log("ProjectManager.processAssetsXML");
			processBKGXML();			
		}
		
		/**
		 * Process BKG
		 */
		private function processBKGXML():void
		{
			Debug.log("ProjectManager.processBKGXML");
			//backgrounds xml update
			if(isValidXML(backgroundsXML))
			{
				var newXML:XML = loader.getXML("backgrounds");
				BackgroundsManager.instance.replaceLocalXML(newXML,processClipartsXML);
			}
		}
		
		/**
		 * Process CLIPARTs
		 */
		private function processClipartsXML():void
		{
			Debug.log("ProjectManager.processClipartsXML");
			//cliparts xml update
			if(isValidXML(clipartsXML))
			{
				var newXML:XML = loader.getXML("cliparts");
				ClipartsManager.instance.replaceLocalXML(newXML,processOverlaysXML);
			}	
		}
		
		/**
		 * Process overlays
		 */
		private function processOverlaysXML():void
		{
			Debug.log("ProjectManager.processOverlaysXML");
			//overlayers xml update
			if(isValidXML(overlayersXML))
			{
				var newXML:XML = loader.getXML("overlayers");
				OverlayerManager.instance.replaceLocalXML(newXML,checkAssetsXMLValidity);
			}
		}
		
		/**
		 * Process check the assets XML validity
		 * If one of the xml is not valid, set the flag to false => It will reload the XML at next new ProjectType
		 */
		private function checkAssetsXMLValidity():void
		{
			Debug.log("ProjectManager.checkProcessXMLValidity");
			
			// Offline only
			CONFIG::offline
			{
				if(isValidXML(backgroundsXML) && isValidXML(clipartsXML) && isValidXML(overlayersXML))
				{
					Debug.log("ProjectManager.checkProcessXMLValidity: all valid");
					downloadFlags[Infos.session.classname] = true;
				}
				else
				{
					Debug.log("ProjectManager.checkProcessXMLValidity: not all valid");
					downloadFlags[Infos.session.classname] = false;
				}
			}
			
			//next step
			processCustomXML();
		}
		
		/**
		 *  process customs xml (background, layouts
		 */
		private function processCustomXML():void
		{
			Debug.log("ProjectManager.processCustomXML");
			// try to retrieve custom layout xml
			try{
				customLayoutXML = loader.getXML("customLayout");
				if(customLayoutXML != null && customLayoutXML != "" && customLayoutXML.toString().indexOf("*ERROR") == -1)
					customLayoutXML = XML(customLayoutXML.layouts);
				else customLayoutXML = new XML(<root><layouts></layouts></root>);
			} 
			catch(e:Error)
			{
				customLayoutXML = new XML(<root><layouts></layouts></root>); // reset content on error
				Debug.warn("ProjectManager.onLoadComplete error with custom layout xml : "+e.toString());
			}
			
			// try to retrieve custom background xml
			try{
				customBackgroundsXML = loader.getXML("customBackgrounds");
				if(customBackgroundsXML != null && customBackgroundsXML != "" && customBackgroundsXML.toString().indexOf("*ERROR") == -1)
					customBackgroundsXML = XML(customBackgroundsXML.customBackgrounds);
				else if(Infos.IS_DESKTOP)
				{
					customBackgroundsXML = new XML(<root><customBackgrounds></customBackgrounds></root>);
				}
			} 
			catch(e:Error)
			{
				Debug.warn("ProjectManager.onLoadComplete error with customBackground : "+e.toString());
				Debug.warn("customBackgroundsXML is : " + customBackgroundsXML);
				if(Infos.IS_DESKTOP)
				{
					customBackgroundsXML = new XML(<root><customBackgrounds></customBackgrounds></root>);
				}
			}
			

			//Process Price XMLs - OFFLINE ONLY
			if(Infos.IS_DESKTOP && Infos.INTERNET && downloadFlags[DOWNLOAD_FLAG_PRICE] != true)
			{
				processPriceListXML();
			}
			else
			{
				//directly process News XML
				processNewsXML();
			}
		}
		
		
		/**
		 *  process prices xml 
		 * then process option price xml
		 */
		private function processPriceListXML():void
		{
			Debug.log("ProjectManager.processPriceListXML");

			if(isValidXML(priceListXML))
			{
				var newXML:XML = loader.getXML("priceList");
				PriceManager.instance.replaceLocalFile(newXML,PriceManager.PRICELIST,processOptionPriceListXML);
			}
			else
				processOptionPriceListXML(); //If xml is invalid, skip the step and try the next	
		}
		
		/**
		 *  process options prices xml 
		 * then process canvas custom XML
		 */
		private function processOptionPriceListXML():void
		{
			Debug.log("ProjectManager.processOptionPriceListXML");

			if(isValidXML(priceListXML))
			{
				var newXML:XML = loader.getXML("optionPriceList");
				PriceManager.instance.replaceLocalFile(newXML,PriceManager.OPTION_PRICELIST,processCanvasCustomPriceListXML);
			}
			else
				processCanvasCustomPriceListXML(); //If xml is invalid, skip the step and try the next
		}
		
		/**
		 *  process canvas custom prices xml 
		 *  Then check project state
		 */
		private function processCanvasCustomPriceListXML():void
		{
			Debug.log("ProjectManager.processCanvasCustomPriceListXML");
			
			if(isValidXML(priceListXML))
			{
				var newXML:XML = loader.getXML("canvasCustomPriceList");
				PriceManager.instance.replaceLocalFile(newXML,PriceManager.CANVAS_CUSTOM_PRICELIST,checkPricesXMLValidity);
			}
			else
				checkPricesXMLValidity(); //If xml is invalid, skip the step and go to the next
			
			
		}
		
		/**
		 * Process check the Prices XML validity
		 * If one of the xml is not valid, set the flag to false => It will reload the XML at next new ProjectType
		 */
		private function checkPricesXMLValidity():void
		{
			Debug.log("ProjectManager.checkPricesXMLValidity");
			
			if(Infos.IS_DESKTOP)
			{
				if(isValidXML(priceListXML) && isValidXML(optionPriceListXML) && isValidXML(canvasCustomPriceListXML))
				{
					Debug.log("ProjectManager.checkPricesXMLValidity: all valid");
					downloadFlags[DOWNLOAD_FLAG_PRICE] = true;
				}
				else
				{
					Debug.log("ProjectManager.checkProcessXMLValidity: not all valid");
					downloadFlags[DOWNLOAD_FLAG_PRICE] = false;
				}
			}
			
			//next step
			processNewsXML();
		}
		
		/**
		 * Process news xml 
		 * then start project
		 */
		private function processNewsXML():void 
		{
			Debug.log("ProjectManager.processNewsXML");
			
			if(newsXML != null)
			{
				if(isValidXML(newsXML))
				{
					var newXML:XML = loader.getXML("news");
					if(Debug.CONSOLE)
						Debug.log(newXML);
					NewsManager.instance.parseXML(newXML);
					//checkProjectState();
				}
				//else
					//checkProjectState(); //If xml is invalid, skip step
			}
			//else
				//checkProjectState(); //XML has already been loaded, skip
			
			
			//OFFLINE : before checking project state, we verify we make a retro compatibilty check
			CONFIG::offline
			{
				ProjectPatcher.instance.patchOldProjects(checkProjectState)
			}
			
			// ONLINE:  continue by checking what to do with project (load, create new, or display configurator)
			CONFIG::online
			{
				checkProjectState();
			}
			
		}
		
		/**
		 * START UP PROJECT
		 * Project has several start up state
		 * - No project selected, means showing up the configurator for the current classname (albums, calendars..) - Default
		 * - Create an empty porject (project selected through wizard, online only)
		 * - Open an existing project (we need a project id)
		 * 
		 * Those choices are based on the checksession result and on the cookie flash (generalprefs)
		 */
		private function checkProjectState():void
		{
			Debug.log("ProjectManager.checkProjectState");
			// load cookie general prefs
			Infos.prefs.loadGeneralPrefs();
			Debug.log("\n:: GENERAL PREFS :: \n" + Infos.prefs.generalPrefToString());
			
			if(Infos.IS_DESKTOP && Debug.USE_DEBUG_PROJECT_STRING)
			{
				Debug.log("INIT CASE : Open Debug string project from file on Desktop");
				_projectId = "123456789";
				_projectName = "DebugProject";
				onProjectLoadSuccess(null);
			}
			// CASE OPEN PROJECT -> from flash cookie
			else if( Infos.prefs.projectToOpen )
			{
				Debug.log("INIT CASE : Open project project -> from flash cookie : "+Infos.prefs.projectToOpen);
				loadProject ( Infos.prefs.projectToOpen, Infos.prefs.projectToOpenName);
			}
				
				// CASE NEW PROJECT CONFIGURATOR -> from flash cookie
			else if( Infos.prefs.isNewProject  || forceNewProject)
			{
				Debug.log("INIT CASE : NEW PROJECT CONFIGURATOR -> from flash cookie");
				Debug.log(" Infos.prefs.isNewProject > "+Infos.prefs.isNewProject);
				Debug.log(" Infos.prefs.projectToOpen > "+Infos.prefs.projectToOpen);
				Infos.prefs.isNewProject = false;
				Infos.prefs.save();
				PROJECT_NO_PROJECT_SELECTED.dispatch();
			}
				
				// CASE OPEN PROJECT -> from check session
			else if( Infos.session.recentProjectId && !Debug.DO_NOT_LOAD_RECENT ) {
				Debug.log("INIT CASE : Open project project -> from check session : "+Infos.session.recentProjectId );
				loadProject ( Infos.session.recentProjectId, Infos.session.recentProjectName );
			} 
				
				// CASE CREATE NEW PROJECT -> with product id from session
			else if(Infos.session.product_id != "") {
				Debug.log("INIT CASE : Create project from check session product id : "+Infos.session.product_id);
				var initParams : ProjectCreationParamsVo = ProjectCreationParamsVo.createWithSessionClassname();
				createNewProject(Infos.session.product_id, initParams); // create a new project with session product id
			}
				
				// CASE SHOW CONFIGURATOR
			else {
				Debug.log("INIT CASE : DEFAULT NEW PROJECT (configurator)");
				PROJECT_NO_PROJECT_SELECTED.dispatch(); // start from beginning
			}
		}
		
		
		
		
		/**
		 * ----------------------------------- CREATE NEW PROJECT -------------------------------------
		 */
		
		
		/**
		 * create new project
		 * here we should be able to inject docCode, docType, width and height depending on left menu selection
		 */
		public function createNewProject( docTypeId : String, initParams:ProjectCreationParamsVo = null):void
		{
			Debug.log("ProjectManager.createNewProject type : " + docTypeId);

			var projectNode : XML = projectsXML.projects.children().(@docidnum==docTypeId)[0];
			
			//security when going back and forth from beta.tictacphoto.com and local
			if(projectNode == null)
			{
				Debug.log("WARNING! docTypeId couldn't be found for this classname: starting new project configurator");
				PROJECT_NO_PROJECT_SELECTED.dispatch(); // start from beginning
				return;
			}
			
			var docCode:String = projectNode.@docidstr;
			var documentNode : XML = documentsXML.document.(@name==docCode)[0];
			var coverNode: XML = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+docCode)[0];
			
			_project = new ProjectVo(initParams);
			helper = new ProjectManagerHelper();
			
			// offline create temporary project folder
			// we also give an definitive ID to this project right now
			CONFIG::offline
			{
				_project.id = "" + new Date().time;
				_project.creationDate = new Date().toString();
				OfflineProjectManager.instance.createTempProjectFolder();
				
				// TODO remove this later, this must be selected from save popup
				_project.imageStorageSystem = ProjectVo.STORAGE_TYPE_REFERENCE;
			}
			
			//cover type (albums only)
			var _coverType:String = (initParams && initParams.coverType) ? initParams.coverType:"";
			
			// create new
			_project.createNew(projectNode, documentNode, coverNode, _coverType);
			
			//canvas
			if(initParams && initParams.canvasPriceInfo)
				_project.canvasPriceInfo = initParams.canvasPriceInfo;
			
			// add page Number if needed
			if(_project.pageNumber)
				PagesManager.instance.showPageNumber(true);
			
			// clean project and verify datas
			cleanAndVerifyProject();
			
			// each time a project is created we remap it with current user photo list
			remapPhotoVoList( true );
			
			PRICE_UPDATED.addOnce(projectReady);
			updateProjecPrice();
		}
		
		
		
		
		/**
		 * ----------------------------------- UPDATE EXISTING PROJECT -------------------------------------
		 */
		
		
		
		/**
		 * update current ALBUM project
		 * 
		 */
		public function updateCurrentAlbumProject(newDocTypeId : String, newProjectType:String, coverType:String = ""):void
		{
			var projectNode : XML = projectsXML.projects.children().(@docidnum==newDocTypeId)[0];
			var docCode:String = projectNode.@docidstr;
			var documentNode : XML = documentsXML.document.(@name==docCode)[0];
			var coverNode: XML = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+docCode)[0];
			
			//set new product ID
			Infos.session.product_id = newDocTypeId;
			
			//update project
			Debug.log("ProjectManager > Update ALBUMS project type to -> " + newDocTypeId);
	
			//update project
			_project.update(projectNode,documentNode,coverNode,coverType);
			
			// clean project and verify datas
			cleanAndVerifyProject();
			
			//update price
			PRICE_UPDATED.addOnce(projectUpdated);
			updateProjecPrice();
			
			// force layout redraw after update (for canvas only right now but can maybe be used for all project, to be tested)
			LayoutManager.instance.forceRedrawLayoutsAfterUpgrade = true;
			
			// update bar
			TopBar.UPDATE_PROJECT.dispatch();
		}
		
		/**
		 * update current CALENDAR project
		 * 
		 */
		public function updateCurrentCalendarProject(docTypeId : String):void
		{
			Debug.log("ProjectManager > Update CALENDARS project type to -> " + docTypeId);
			
			var projectNode : XML = projectsXML.projects.children().(@docidnum==docTypeId)[0];
			var docCode:String = projectNode.@docidstr;
			var documentNode : XML = documentsXML.document.(@name==docCode)[0];
			var coverNode: XML = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+docCode)[0];
			
			_project.update(projectNode,documentNode,coverNode);
			
			// clean project and verify datas
			cleanAndVerifyProject();
			
			PRICE_UPDATED.addOnce(projectUpdated);
			updateProjecPrice();
			
			// update bar
			TopBar.UPDATE_PROJECT.dispatch();
		}
		
		/**
		 * update current CARDS project
		 * 
		 */
		public function updateCurrentCardsProject(docTypeId : String):void
		{
			Debug.log("ProjectManager > Update CARDS project type to -> " + docTypeId);
			
			var projectNode : XML = projectsXML.projects.children().(@docidnum==docTypeId)[0];
			var docCode:String = projectNode.@docidstr;
			var documentNode : XML = documentsXML.document.(@name==docCode)[0];
			var coverNode: XML = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+docCode)[0];
			
			_project.update(projectNode,documentNode,coverNode);
			
			PRICE_UPDATED.addOnce(projectUpdated);
			updateProjecPrice();
		}
		
		/**
		 * update current CANVAS project
		 * 
		 */
		public function updateCurrentCanvasProject(docTypeId : String, initParams:ProjectCreationParamsVo):void
		{
			Debug.log("ProjectManager > Update CANVAS project type to -> " + docTypeId);
			
			var projectNode : XML = projectsXML.projects.children().(@docidnum==docTypeId)[0];
			var docCode:String = projectNode.@docidstr;
			var documentNode : XML = documentsXML.document.(@name==docCode)[0];
			var coverNode: XML = ProjectManager.instance.coversDocumentXML.documents.document.(@name == "C"+docCode)[0];
			
			// TODO : here we should probably display a popup !
			
			// update with init params
			project.fillInitParams(initParams);
			
			// update nodes
			_project.update(projectNode,documentNode,coverNode);
			
			// clean project and verify datas
			cleanAndVerifyProject();
			
			PRICE_UPDATED.addOnce(projectUpdated);
			updateProjecPrice();
			
			// force layout redraw after update (for canvas only right now but can maybe be used for all project, to be tested)
			LayoutManager.instance.forceRedrawLayoutsAfterUpgrade = true;
			
			// update bar
			TopBar.UPDATE_PROJECT.dispatch();
			
			
			// For canvas only right now but should maybe done for all projects, clean history when project update is complete
			UserActionManager.instance.clearHistory();
		}
		
		/**
		 * Helper to check XML validity after bulkloading
		 */
		private function isValidXML(xml:XML):Boolean
		{
			return(xml != null && xml != "" && xml.toString().indexOf("*ERROR") == -1);				
		}
			
		/**
		 * Restore project from history
		 * 
		 */
		public function restoreProjectFromHistory(projectObj:Object):void
		{
			_project.fillFromHistory(projectObj);
			PRICE_UPDATED.addOnce(projectUpdated);
			updateProjecPrice();
		}
		
		
		/**
		 * allows to set the _project properties while we patch an old project list.
		 */
		public function setProjectToBePatched(projectVo : ProjectVo):void
		{
			_project = projectVo;
		}
		
		
			
		/**
		 * get DocType ID By Code
		 */
		public function getDocTypeIDByCode( code : String ):String
		{
			var projectNode : XML = projectsXML.projects.children().(@docidstr==code)[0];
			if(projectNode == null)
				return null;
			else
			return projectNode.@docidnum;
		}
		
		/**
		 * get current project type max pages
		 */
		public function getMaxPages():int
		{
			if(!Infos.isAlbum) return -1;
			if(!Infos.project) return -1;
			if(menuXML == null) return -1;
			
			var projectNode:XMLList = menuXML.menu.item.(@id == "project").content.types.node.(@type == Infos.project.type);
			if(projectNode.length() > 0)
			return projectNode.@pagesMax;
			else
			return -1;
			
		}
		
		
		
		/**
		 * ------------------------------------ OPEN PROJECT EXPLORER POPUP -------------------------------------
		 */
		
		/**
		 * open project explorer
		 */
		public function openProjectExplorer( skipSaveWarning:Boolean = false ):void
		{
			Debug.log("ProjectManager > openProjectExplorer");
			if(!skipSaveWarning && needSave){
				disposeSavePopupSignals();
				PROJECT_SAVE_CANCEL.addOnce(disposeSavePopupSignals);
				PROJECT_SAVE_COMPLETE.addOnce(openProjectExplorerAfterSaveWarning);
				PROJECT_DO_NOT_SAVE.addOnce(openProjectExplorerAfterSaveWarning);
				saveProject(true);
				return;
			} 
			
			if(!projectExplorer) {
				projectExplorer = new PopupProjectExplorer();
			}
			
			projectExplorer.PROJECT_DELETE.remove( onDeleteProjectHandler )
			projectExplorer.PROJECT_DELETE.add( onDeleteProjectHandler );
			projectExplorer.loadAndOpen();
			projectExplorer.OK.add(projectOpenContinue);
		}
		private function projectOpenContinue(p:PopupProjectExplorer):void
		{
			// notify project open event
			PROJECT_OPEN_CONTINUE.dispatch();
			
			// If there is an available project to open from project list  
			if(p && p.selectedItem)
			{
				_projectId = p.selectedItem.id;
				_projectName = p.selectedItem.name;
				var _className:String = p.selectedItem.classname;
			}
				
				// otherwise, security, open new empty session
			else
			{
				_projectId = null;
				_projectName = null;
				_className = Infos.classname;
			}
			
			Infos.prefs.projectToOpen = _projectId;
			Infos.prefs.projectToOpenName = _projectName;
			Infos.prefs.projectToOpenClassname = _className;
			Infos.prefs.save();
			
			// OFFLINE
			CONFIG::offline
			{
				desktopProjectChange(_className);
			}		
			
			// ONLINE
			CONFIG::online
			{
				ServiceManager.instance.createOpenProjectSession(_projectId, function(r:Object):void{
					App.instance.reload();
				});
			}		
		}
		
		
		
		private function openProjectExplorerAfterSaveWarning():void
		{
			disposeSavePopupSignals();
			TweenMax.delayedCall(.1, function():void{
				openProjectExplorer( true); // we add a delay here to be sure there is not overlap of add/remove signal which was causing some errors
			});
		}
		
		/**
		 * Re-open Project after lang changes
		 */
		public function openSameProject( skipSaveWarning:Boolean = false ):void
		{
			Debug.log("ProjectManager > openSameProject");
			if(!skipSaveWarning && needSave){
				disposeSavePopupSignals();
				PROJECT_SAVE_CANCEL.addOnce(disposeSavePopupSignals);
				PROJECT_SAVE_COMPLETE.addOnce(openSameProjectAfterSaveWarning);
				PROJECT_DO_NOT_SAVE.addOnce(openSameProjectAfterSaveWarning);
				saveProject(true);
				return;
			} 
			
			// if we have a project, open it. Otherwise, open nothing
			if(project && project.isSaved)
			{
				Infos.prefs.projectToOpen = project.id
				Infos.prefs.projectToOpenName = project.name
			}
			Infos.prefs.projectToOpenClassname = SessionManager.instance.session.classname;
			Infos.prefs.save();
			
			if( Infos.IS_DESKTOP )
			{
				_projectId = Infos.prefs.projectToOpen;
				_projectName = Infos.prefs.projectToOpenName;
				desktopProjectChange(Infos.prefs.projectToOpenClassname);	
			}
			else
			{
				App.instance.reload();
			}
		}
		
		
		// update current project in preferences
		private function updateProjectPreferences():void
		{
			// save project infos in preference for next editor open
			Infos.prefs.projectToOpen = Infos.project.id;
			Infos.prefs.projectToOpenName = Infos.project.name;
			Infos.prefs.projectToOpenClassname = Infos.project.classname;
			Infos.prefs.save();
			
			// store project id and name in private values
			_projectId = Infos.project.id;
			_projectName = Infos.project.name;
		}	
		private function resetProjectPreferences():void
		{
			Infos.prefs.projectToOpen = null;
			Infos.prefs.projectToOpenName = null;
			Infos.prefs.isNewProject = false;
			Infos.prefs.save();
		}
		
		
		private function openSameProjectAfterSaveWarning():void
		{
			disposeSavePopupSignals();
			TweenMax.delayedCall(.1, function():void{
				openSameProject( true); // we add a delay here to be sure there is not overlap of add/remove signal which was causing some errors
			});
			
		}
		
		
		
		
				
		/**
		 * Desktop only
		 * Do what need to be done when:
		 * -user opens another project 
		 * -user wants to create a new one
		 * -handles classname changes
		 */
		public function desktopProjectChange(newClassName:String):void
		{
			// Flag
			ready = false;
			
			//change classname
			SessionManager.instance.session.classname = newClassName;
				
			//dispose views
			App.instance.disposeViews();
			
			//reload XMLs
			loader.clear();
			loader = null;
			loader = new BulkLoader("ProjectManager");
			init();
			
		}
		
		
		
		/**
		 * RESET MANAGERS
		 * Desktop only
		 */
		public function resetManagers():void
		{
			Debug.log("ProjectManager.ResetManagers");
			
			//Reset Managers
			LayoutManager.instance.reset();
			CalendarManager.instance.reset();
			BackgroundsManager.instance.reset();
		}
		

		
		/**
		 * ------------------------------------ LOAD PROJECT -------------------------------------
		 */
		

		public function loadProject ( projID : String, projectName:String ):void
		{
			// update temp project values
			_projectId = projID;
			_projectName = projectName;
			
			// ONLINE
			CONFIG::online
			{
				ServiceManager.instance.loadProject( projID, onProjectLoadSuccess, onProjectLoadError ) ;
			}
			
			// OFFLINE
			CONFIG::offline
			{
				Infos.currentProjectPath = Infos.offlineProjectsPath  + projectName;
				var projFile : File = new File(Infos.currentProjectPath + "/project.tic");
				if(!projFile.exists){
					onProjectLoadError(new ErrorEvent("PROJECT_LOAD_ERROR"));
					return;
				}
				OfflineProjectManager.instance.loadProject(projFile, onProjectLoadSuccess, onProjectLoadError);
			}
		}
		private function onProjectLoadError(e:ErrorEvent):void
		{
			Debug.warn("ProjectManager.onProjectLoadError : "+e.toString());
			// display a popup
			PopupAbstract.Alert(ResourcesManager.getString("loading.project.error.title"), ResourcesManager.getString("loading.project.error.description"),false);
			
			// reset
			_projectId = null
			_projectName = null
			resetProjectPreferences();
			PROJECT_NO_PROJECT_SELECTED.dispatch();
		}
		private function onProjectLoadSuccess(b:ByteArray):void
		{
			Debug.log("ProjectManager > load project success : uncompressing byteArray.. ");// + savedDatas);
			DataLoadingManager.instance.showLoading(false, null, ResourcesManager.getString("loading.project.construct"),true);
			var projectString : String ;
			var proj:Object ;
			
			// decompress project error handler
			var loadOrDecompressError:Function = function( errorMsg : String ):void
			{
				try{ Debug.warn(">> "+ b.readUTFBytes(b.bytesAvailable) ); }
				catch(e:Error){Debug.warn(">> impossible to convert loaded bytes to string : "+e.message)}
				Debug.warnAndSendMail("Project load fail, uncompressing error for file : " + errorMsg);
				onProjectLoadError(new ErrorEvent("UNCOMPRESS_ERROR"));
			}
				
			// DEBUG PROJECT STRING
			if( Debug.USE_DEBUG_PROJECT_STRING )
			{
				CONFIG::online
				{
					// we use a project string stored in a class
					projectString = ProjectDebugString.value;
				}
				
				CONFIG::offline
				{
					// we load the project from desktop directory
					var file:File = File.desktopDirectory.resolvePath("proj.tic");
					b = FileUtils.getBytes(file);
				}
			}
			
			// normal decompression
			else
			{
				// first we try to read object with new system (read object)
				try
				{
					var tryOldSystem:Boolean = false;
					proj = b.readObject();
					// if we do not have a name and ID, it means there is an error with the save, we will try to load using previous system (JSON) also
					if(!proj.id && !proj.name) tryOldSystem = true;
				} 
				catch(e:Error) { tryOldSystem =true; }
				
				// there was an error with current uncompressing, we try with old system
				if(tryOldSystem)
				{
					try{
						// uncompressing bytes to string and convert to json
						b.uncompress();
						projectString = b.toString();
						proj = new JSONDecoder(projectString).getValue();	
					}
					catch(e:Error) { 
						// that didn't work neither, there is defintely a problem with this save file...
						loadOrDecompressError(e.message);
						return;
					}
				}
			}
				
			
			// uncompression complete, remove loading
			DataLoadingManager.instance.hideLoading(); 
			
			
			// DO NOT LOG PROJECT STRING ANYMORE...
			Debug.log(" ******************************************************* ");
			Debug.log("ProjectManager.onProjectLoadSuccess : ");// + savedDatas);
			//Debug.log("ProjectManager > loaded save string : "+projectString);// + savedDatas);
			
			// CREATE PROJECT VO
			_project = new ProjectVo();
			helper = new ProjectManagerHelper();
			
			_project.name = _projectName;
			_project.id = _projectId; // on load success, override project id ! (security)
			_project.fillFromSave(proj);
			
			// (security), if no modification date in the project, use the creation date
			if(!_project.modificationDate) _project.modificationDate = _project.creationDate;
			
			// LOG SOME PROJECT INFOS
			Debug.log("  id : "+_project.id);
			Debug.log("  name : "+_project.classname);
			Debug.log("  classname : "+_project.name);
			Debug.log("  product : "+_project.docType);
			Debug.log(" ******************************************************* ");
			
			
			// clean project and verify datas
			cleanAndVerifyProject();
			
			
			// clean unused photos (if project was not save but photos were imported, we need to remove photos to be sure we don't have override conflicts)
			CONFIG::offline
			{
				if( _project.lastOpenedOfflinePath )Infos.lastUserBrowsePath = _project.lastOpenedOfflinePath;
				OfflineProjectManager.instance.cleanUnusedPhotos();
			}
			
			// each time a project is created we remap it with current user photo list
			remapPhotoVoList( true );
			
			PRICE_UPDATED.addOnce(projectReady);
			updateProjecPrice();
		}
		
		
		/*
		 * completely unload current project and reset views
		 */
		public function unloadProject():void
		{
			// reset project
			_project = null;
			
			// reset history
			UserActionManager.instance.clearHistory();
			
			// notify
			//UNLOAD_PROJECT.dispatch();
		}
		
		
		
		
		
		
		/**
		 * ------------------------------------ SAVE PROJECT -------------------------------------
		 */
		
		/**
		 * normal save 
		 */
		public function saveProject( isWarning : Boolean = false, isBrowserLeave:Boolean = false, _isSaveAs:Boolean = false ):void
		{
			isSaveAs = _isSaveAs;
			if(Infos.project) _nameBeforeSave = Infos.project.name;
			
			// block save if not allowed (debug user session for example)
			if(Debug.DO_NOT_ALLOW_SAVE) return;
			
			Debug.log("ProjectManager > saveProject");
			if(!projectExplorer) projectExplorer = new PopupProjectExplorer();
			//if(isBrowserLeave) isWarning = true; // force warning when save for browser leave
			
			projectExplorer.isSaveAs = isSaveAs;
			if(isWarning) projectExplorer.loadAndOpen( PopupProjectExplorer.TYPE_SAVE_WARNING, isBrowserLeave ); // save with warning
			else projectExplorer.loadAndOpen( PopupProjectExplorer.TYPE_SAVE, isBrowserLeave  ); // normal save
			
			projectExplorer.OK.addOnce(startSaveProcess);
			
			// save preferences
			Infos.prefs.save();
		}
		/**
		 * save as project
		 */
		public function saveProjectAs():void
		{
			saveProject(false, false, true);
		}
		
		private function startSaveProcess(p:PopupAbstract = null):void
		{
			// reset save warning timer
			TopBar.resetAutoSaveTimer();
			
			// time log
			TimeLog.start("SaveProcess");
			
			// save as
			if(isSaveAs){
				if(!Infos.IS_DESKTOP) project.id = null; // ONLINE Project ID is given by service result
				else project.id = "" + new Date().time; // OFFLINE project ID for copy
			}
			
			TopBar.UPDATE_PROJECT.dispatch();
			Debug.log("ProjectManager > start save process with name : "+project.name + " and id : "+project.id);// + savedDatas);
			//var projectString:String = Infos.project.getSaveString();
			Debug.log("-----> get save string " + TimeLog.log("SaveProcess"));
			
			if(Debug.DO_NOT_ALLOW_SAVE) return; // DO NOT SAVE SECURITY
			
			Debug.log("ProjectManager.sendProjectDatas : ");
			Debug.log("  id : "+project.id);
			Debug.log("  classname : "+project.classname);
			Debug.log("  name : "+project.name);
			Debug.log("  product : "+project.docType);
			
			// data loading
			DataLoadingManager.instance.showLoading(false, null, ResourcesManager.getString("loading.project.save"));
			
			//
			// ONLINE SAVE
			//
			CONFIG::online
			{
				clearUploader();
				saveUploader = new MultipartURLLoader();
				saveUploader.dataFormat = URLLoaderDataFormat.TEXT;
				saveUploader.addEventListener(Event.COMPLETE, projectSaveComplete);
				saveUploader.addEventListener(IOErrorEvent.IO_ERROR, projectSaveError);
				
				var fileName : String = "0"; // don't know why but was previously like this
				
				//Add Variables
				var docTypeStr:String = (Infos.isCanvas)?String( parseInt(project.docType) + CanvasManager.instance.CanvasModifier ) : project.docType;
				saveUploader.addVariable("classname", project.classname);
				saveUploader.addVariable("build", project.build); 
				saveUploader.addVariable("product", docTypeStr);
				
				if(project.id) saveUploader.addVariable("id", project.id);
				else Debug.log("Save new project (no existing id)"); // save project with no ID = create a new project
				
				saveUploader.addVariable("name", project.name);
				saveUploader.addVariable("Filename", fileName);
				saveUploader.addFile(project.getSaveBytes(), fileName, "uploadfile");
				
				//send
				var url : String = Infos.config.serverNormalUrl + "/" + Infos.config.getSettingsValue("ProjectSavePage");
				Debug.log("  url : "+url);
				saveUploader.load(url, false);
			}
			
			//
			// OFFLINE SAVE
			//
			CONFIG::offline
			{
				OfflineProjectManager.instance.SaveProject( isSaveAs, projectSaveComplete,projectSaveError );
				//LocalStorageManager.instance.saveProject( project.id, project.name, project.classname, project.docType, project.build, saveBytes, handleSaveResult );
			}
		}
		private function projectSaveError(e:ErrorEvent):void
		{
			Debug.warnAndSendMail("ProjectManager.projectSaveError : "+ e.text);
			if(!Infos.IS_DESKTOP) clearUploader();
			DataLoadingManager.instance.hideLoading();
			
			// warn user
			PopupAbstract.Alert(ResourcesManager.getString("project.save.error.title"),ResourcesManager.getString("project.save.error.description"),false);
		}
		private function projectSaveComplete(e:Event = null):void
		{
			// reset actions since last save to 0
			UserActionManager.instance.actionsSinceLastSave = 0;
			
			// ONLINE
			CONFIG::online
				{
					var resultString : String = MultipartURLLoader(e.target).loader.data
					Debug.log("ProjectManager.projectSaveComplete : "+resultString);
					clearUploader();
					
					var result : XML = new XML(resultString);
					if(result){
						/**
						 * EXAMPLE
						 *
						 <?xml version="1.0" encoding="utf-8"?>
						 <project>
						 <name>New L80 project</name>
						 <id>160428</id>
						 <url>/flex/project.php?id=160428</url>
						 <autosave>0</autosave>
						 <last_modified>2013-10-12 08:57:05+00</last_modified>
						 </project> 
						 **/
						
						// if we do not have a project id, it means this is the first save.
						var isFirstProjectSave:Boolean = (!Infos.project.id)? true : false;
						
						// override project id (which is also overriden while project load)
						Infos.project.id = ""+result.id; 
						if(_nameBeforeSave != ""+result.name){
							Debug.log("Project renamed from : '" +_nameBeforeSave + "' to '"+ result.name +"'");
							Infos.project.name = result.name;
							if(!isFirstProjectSave) SessionManager.instance.refreshImageList();
						}
						_nameBeforeSave = null;
						
						// notify the world
						Debug.log("ProjectManager.saveComplete");
						PROJECT_SAVE_COMPLETE.dispatch();
						
						// check if we need a folder update
						if(isFirstProjectSave) FolderManager.instance.newProjectSaved();
						
						// save preferences for fresh saved project
						Infos.prefs.save();
					}
					else
					{
						Debug.error("Project Save Error : " + resultString);
					}
				}
				
				
				// OFFLINE
				CONFIG::offline
				{
					// save preferences for fresh saved project
					updateProjectPreferences();
					
					// notify the world
					Debug.log("ProjectManager.saveComplete");
					PROJECT_SAVE_COMPLETE.dispatch();
				}
				
				// hide loading
				DataLoadingManager.instance.hideLoading();
		}
		

		
		
		
		/**
		 * ------------------------------------ NEW PROJECT -------------------------------------
		 */
		
		public function startNewProject():void
		{
			disposeSavePopupSignals();

			if( Infos.IS_DESKTOP )
			{
				//Launch Dashboard
				Dashboard.instance.open();
				//remove before adding it
				Dashboard.DASHBOARD_CANCELED.remove(onDashBoardCancel);
				Dashboard.DASHBOARD_CHOICE_MADE.remove(onDashBoardChoiceMade);
				//add signals
				Dashboard.DASHBOARD_CANCELED.add(onDashBoardCancel);
				Dashboard.DASHBOARD_CHOICE_MADE.add(onDashBoardChoiceMade);
				
			}
			else
			{
				Infos.prefs.projectToOpen = null;
				Infos.prefs.projectToOpenName = null;
				Infos.prefs.projectToOpenClassname = Infos.session.classname;
				Infos.prefs.isNewProject = true;
				Infos.prefs.save();
				
				ServiceManager.instance.createNewProjectSession(function(r:Object):void{
					App.instance.reload();
				});
			}
		}
		
		/**
		 * reload with new project with a popup for saving current one
		 */
		public function reloadWithNewProject():void
		{	
			Debug.log("ProjectManager > reloadWithNewProject");
			
			// first ask for save 
			if(needSave) {
				disposeSavePopupSignals();
				PROJECT_SAVE_CANCEL.add(disposeSavePopupSignals);
				PROJECT_SAVE_COMPLETE.add(startNewProject);
				PROJECT_DO_NOT_SAVE.add(startNewProject);
				saveProject(true);
			} else
			{
				startNewProject();
			}
		}
		
		private function disposeSavePopupSignals():void
		{
			PROJECT_SAVE_CANCEL.removeAll();
			PROJECT_SAVE_COMPLETE.removeAll();
			PROJECT_DO_NOT_SAVE.removeAll();
		}
		
		
		
		private function onDashBoardChoiceMade(newClassname:String):void
		{
			//Dashboard.DASHBOARD_CANCELED.removeAll();
			//Dashboard.DASHBOARD_CHOICE_MADE.removeAll();
			
			//Dashboard.instance.close();
			
			//Save prefs
			Infos.prefs.projectToOpen = null;
			Infos.prefs.projectToOpenName = null;
			Infos.prefs.projectToOpenClassname = newClassname;
			Infos.prefs.isNewProject = true;
			Infos.prefs.save();
			
			//unload current project
			unloadProject();
			//Change project
			desktopProjectChange(newClassname);
			
		}
		
		private function onDashBoardCancel():void
		{
			//Dashboard.DASHBOARD_CANCELED.removeAll();
			//Dashboard.DASHBOARD_CHOICE_MADE.removeAll();
		}		
		
		

		/**
		 * ----------------------------------- DELETE PROJECT (OFFLINE) -------------------------------------
		 */
		private function onDeleteProjectHandler ( projectName : String ):void
		{
			// OFFLINE
			CONFIG::offline
			{
				var onDeleteSuccess:Function = function():void
				{
					Debug.log("ProjectManager.onDeleteProject success : "+projectName);
					
					// close project if this is the running project 
					if(Infos.project && projectName == Infos.project.name){
						// clear history to allow close
						UserActionManager.instance.actionsSinceLastSave = 0;
						_project = null;
						reloadWithNewProject();
					}
					
					PopupAbstract.Alert(ResourcesManager.getString("project.delete.success.title"),ResourcesManager.getString("project.delete.success.description") + "\n'"+projectName+"'",false,openProjectExplorer);
				}
				var onDeleteError:Function = function(e:ErrorEvent):void
				{
					Debug.warn("ProjectManager.onDeleteProject error : "+e.toString());
					PopupAbstract.Alert(ResourcesManager.getString("project.delete.error.title"),ResourcesManager.getString("project.delete.error.description") + "\n'"+projectName+"'",false,openProjectExplorer);
				}
				OfflineProjectManager.instance.deleteProject(projectName, onDeleteSuccess, onDeleteError );
			}
		}
		
		
		
		
		/**
		 * ------------------------------------ LOGOUT -------------------------------------
		 */
		
		public function logout():void
		{	
			Debug.log("ProjectManager > logout");
			// check first if we can logout directly or we need a save
			if(needSave){
				disposeSavePopupSignals();
				PROJECT_SAVE_CANCEL.add(disposeSavePopupSignals);
				PROJECT_SAVE_COMPLETE.add(logoutStep2);
				PROJECT_DO_NOT_SAVE.add(logoutStep2);
				saveProject(true);
			} 
			else logoutStep2();
		}
		private function logoutStep2():void
		{
			disposeSavePopupSignals();
			ServiceManager.instance.logOut(function():void{App.instance.reload()});
		}
		

		
		
		
		
		/**
		 * Update current project price
		 */
		public function updateProjecPrice():void
		{
			Debug.log("ProjectManager.updateProjecPrice");
			if(!Infos.IS_DESKTOP)
				ServiceManager.instance.getProjectPrice(false, onProjectPriceResult);
			else
			{
				project.price = PriceManager.instance.getPrice();
				Debug.log("ProjectManager > new price is : "+ project.fullPrice);
				Debug.log("ProjectManager > p : "+ project.price);
				PRICE_UPDATED.dispatch();
			}
		}
		private function onProjectPriceResult(result:Object):void
		{
			if(result == "-1") Debug.warn("Wrong price result.., check query string");
			else {
				var priceString : String = result as String;
				priceString = priceString.substr(priceString.indexOf("EUR") +4); 
				project.price = parseFloat(priceString);
				Debug.log("ProjectManager > new price is : "+ project.fullPrice);
				Debug.log("ProjectManager > p : "+ project.price);
				PRICE_UPDATED.dispatch();
			}
		}
		
		/**
		 * make auto fill
		 * -> find all empty frames
		 * -> inject photoVo in it by checking everytime that photo vo was not already used
		 * -> TODO : amélioration, do not add frames that will be in poor quality depending on frame size
		 */
		public function makeAutoFill( onlyAlbumVo : AlbumVo = null):void
		{
			// Show auto fill waiting screen
			DataLoadingManager.instance.updateText("loading.autofill.progress");
			DataLoadingManager.instance.showLoading();
			
			
			// FIND ALL empty frames in current project
			var emptyFrames : Vector.<FrameVo> = new Vector.<FrameVo>();
			var frame : FrameVo;
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				for (var j:int = 0; j < project.pageList[i].layoutVo.frameList.length; j++) 
				{
					frame = project.pageList[i].layoutVo.frameList[j];
					if(frame.type == FrameVo.TYPE_PHOTO && frame.isEmpty){
						emptyFrames.push(frame);
					}
				}
			}
			
			// FIND ALL unused photo in image list
			var photoVo : PhotoVo;
			var albumVo : AlbumVo;
			var availablePhotos : Vector.<PhotoVo> = new Vector.<PhotoVo>();
			for (i = 0; i < SessionManager.instance.albumList.length; i++) 
			{
				albumVo = SessionManager.instance.albumList[i];
				// Only use folder open for autofill.(ONline only)
				if(!Infos.IS_DESKTOP && !FolderManager.instance.checkIfFolderIsUsed(albumVo.name)) continue;
				
				if(!onlyAlbumVo || onlyAlbumVo == albumVo){
					for (j = 0; j < albumVo.photoList.length; j++) 
					{
						photoVo = SessionManager.instance.albumList[i].photoList[j];
						if(!photoVo.used && photoVo.id){
							availablePhotos.push(photoVo);
						}
					}
				}
			}
			
			// MAP available images to empty frames
			var frameIndex : int = 0;
			var photoIndex : int = 0;
			var allPhotoUsed:Boolean = (availablePhotos.length>0)?false:true;
			var fillMap : Array = new Array();
			for (i = 0; i < emptyFrames.length && !allPhotoUsed; i++) 
			{
				// add in fill map the corresponding frame and photovo
				fillMap.push( { f:emptyFrames[i], p:availablePhotos[photoIndex] } );
				photoIndex += 1;
				if(photoIndex>availablePhotos.length-1)
					allPhotoUsed = true;
			}
			
			
			// INJECT in frames
			var processQueue:Function = function(index:int):void
			{
				// check end of queue
				if(index == fillMap.length){
					autoFillComplete(allPhotoUsed);
					return;
				}
				
				var photoVo:PhotoVo = fillMap[index].p as PhotoVo;
				var frameVo:FrameVo = fillMap[index].f as FrameVo;
				
				////////////////////
				// ONLINE : image import for online is direct
				////////////////////
				CONFIG::online
				{
					FrameVo.injectPhotoVoInFrameVo(frameVo, photoVo);
					// process next item
					processQueue(index+1);
				}
				
				////////////////////
				// OFFLINE : images must be copied first in folder
				////////////////////
				CONFIG::offline
				{
					// listeners
					var onSaveError : Function = function(e:ErrorEvent):void
					{
						Debug.warnAndSendMail("ProjectManager.autoFillError : onSave error :"+e.toString());
						processQueue(index+1);
					}
					var onSaveSuccess : Function = function():void
					{
						FrameVo.injectPhotoVoInFrameVo(frameVo,photoVo);
						// process next item
						processQueue(index+1);
					}
						
					// find thumb data
					var thumbData:BitmapData = PhotosTabOffline.getThumbDataForPhotoVo( photoVo );
					
					// save image in project folder
					if(photoVo.temp) OfflineProjectManager.instance.SaveImage(photoVo, thumbData, onSaveSuccess, onSaveError);
					else onSaveSuccess();
				}
			}
				
			// Start inject in frames
			processQueue(0);
		}
		private function autoFillComplete(allPhotoUsed:Boolean):void
		{
			//Alert popup when all photo are used
			if(allPhotoUsed)
				PopupAbstract.Alert(ResourcesManager.getString("popup.autoFill.warning.allphotoused.title"),ResourcesManager.getString("popup.autoFill.warning.allphotoused.description"),false);
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// update used photos
			remapPhotoVoList();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			// Remove data loading
			DataLoadingManager.instance.hideLoading();
		}
		
		
		/**
		 * checks photovo list 
		 * -> map current project photo vo with loaded one
		 * -> check already used photos and update 
		 * -> check photos in album but not existing in image list
		 */
		public function remapPhotoVoList( atStart : Boolean = false):void
		{
			if(!project) return; // project not loaded
			
			var pageVo : PageVo;
			var frameVo : FrameVo;
			var photoVo : PhotoVo;
			var matchingPhotoVo : PhotoVo;
			
			// reset all used to false
			resetPhotoVoUsed();
			
			// temp photo not uploaded
			var tempPhotoWarning : Boolean = false;
			
			// check photos used in album
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				pageVo = project.pageList[i];
				for (var j:int = 0; j < pageVo.layoutVo.frameList.length; j++) 
				{
					frameVo = pageVo.layoutVo.frameList[j];
					
					//Handle null frame
					if(frameVo == null)
					{
						pageVo.layoutVo.frameList.splice(j,1);
						j--;
						continue;
					}
					
					// if this frame use a user photo, check if we can remap it with existing photovo
					if( frameVo != null && frameVo.hasUserPhoto() )
					{
						
						//////////////////////////
						// in ONLINE editor
						// we override photovo in frames by the one from the server photo list
						//////////////////////////
						CONFIG::online
						{
							matchingPhotoVo = findMatchingPhotoVoByID( frameVo.photoVo );
							// IF we didn't find a match, we will need to check the cause
							if(!matchingPhotoVo)  
							{
								// IF the photo vo is a temp photovo still uploading
								if(frameVo.photoVo.tempID || frameVo.photoVo.temp)
								{
									// if there are no more uploading items, there is no more chances to recover it, so we reset the frame
									if( atStart && PhotoUploadManager.instance.numUploadingItems == 0 ) 
									{
										Debug.warn("ProjectManager.remapPhotoVoList : temp photo : "+frameVo.photoVo.tempID+ " was not succesfully uploaded, clean frame");
										frameVo.reset();
										tempPhotoWarning = true;
									}
								}
								// the photo has not match in photo list, this could be a lost photo
								else
								{
									Debug.warn("ProjectManager.remapPhotoVoList : Photo with id : "+frameVo.photoVo.id+ " doesn't exist anymore in photo list");
									// check if photo is on the right server.. otherwise change 
									ProjectManager.instance.normalizePhotoUrl(frameVo.photoVo);
								}
							}
							// ELSE we found a match
							else{
								// WE REPLACE FRAME PHOTOVO BY THE ONE JUST DOWNLOADED
								frameVo.photoVo = matchingPhotoVo;
								frameVo.photoVo.used = true;
								
								// check if folder is in folder prefs, otherwhise add it !
								if(atStart && !FolderManager.instance.checkIfFolderIsUsed(matchingPhotoVo.folderName))
								{
									if(!Infos.prefs.foldersUsedList) Infos.prefs.foldersUsedList = new Array();
									Infos.prefs.foldersUsedList.push(matchingPhotoVo.folderName);
								}
							}
						}
							
						
						//////////////////////////
						// in OFFLINE editor
						// we override photovo in the left photo tab area by the one already in the project
						//////////////////////////
						CONFIG::offline
						{
							matchingPhotoVo = findMatchingPhotoVoByOriginUrl( frameVo.photoVo );
							// we did find a match
							if(matchingPhotoVo) 
							{
								// replace values in it 
								/*
								matchingPhotoVo.id = frameVo.photoVo.id;
								matchingPhotoVo.temp = false;
								matchingPhotoVo.thumbImagePath = frameVo.photoVo.thumbImagePath;
								matchingPhotoVo.workingImagePath = frameVo.photoVo.workingImagePath;
								matchingPhotoVo.fullsizeImagePath = frameVo.photoVo.fullsizeImagePath;
								matchingPhotoVo.width = frameVo.photoVo.width;
								matchingPhotoVo.height = frameVo.photoVo.height;
								*/
								matchingPhotoVo.used = true;
							}
							
							// reset used flag
							frameVo.photoVo.used = true;
						}
					}
				}
			}
			
			// has temp photos
			if(tempPhotoWarning) PopupAbstract.Alert(ResourcesManager.getString("popup.remap.notUploadedPhoto.error.title"),ResourcesManager.getString("popup.remap.notUploadedPhoto.error.desc"),false,null,null,true);
			
			PHOTO_VO_MATCH_COMPLETE.dispatch();
		}
		
		
		
		/**
		 * find matching frame vo in image list
		 */
		public function findMatchingPhotoVoByID( photoVo :PhotoVo ):PhotoVo
		{
			var albumList : Vector.<AlbumVo> = SessionManager.instance.albumList;
			var albumVo : AlbumVo;
			var tempPhotoVo : PhotoVo;
			for (var i:int = 0; i < albumList.length; i++) 
			{
				albumVo = albumList[i];
				for (var j:int = 0; j < albumVo.photoList.length; j++) 
				{
					tempPhotoVo = albumVo.photoList[j];
					if(tempPhotoVo.id == photoVo.id) {
						return albumVo.photoList[j];
					}
				}
			}
			return null;
		}
		
		/**
		 * Find a matching photoVo by its origin url, which must be different by it's ID
		 */
		public function findMatchingPhotoVoByOriginUrl( photoVo :PhotoVo ):PhotoVo
		{
			// if no origin url, return null
			if(!photoVo || !photoVo.originUrl) return null;
			
			var albumList : Vector.<AlbumVo> = SessionManager.instance.albumList;
			var albumVo : AlbumVo;
			var tempPhotoVo : PhotoVo;
			for (var i:int = 0; i < albumList.length; i++) 
			{
				albumVo = albumList[i];
				for (var j:int = 0; j < albumVo.photoList.length; j++) 
				{
					tempPhotoVo = albumVo.photoList[j];
					
					// Check if we have a match but that is not the same photovo ID
					if(tempPhotoVo.originUrl && tempPhotoVo.originUrl == photoVo.originUrl && tempPhotoVo.id != photoVo.id ) {
						return albumVo.photoList[j];
					}
				}
			}
			return null;
		}
		
		
		/**
		 * Find a matching photoVo by its name
		 */
		public function findMatchingPhotoVoByName( photoVo :PhotoVo ):PhotoVo
		{
			// if no origin url, return null
			if(!photoVo || !photoVo.name) return null;
			
			var albumList : Vector.<AlbumVo> = SessionManager.instance.albumList;
			var albumVo : AlbumVo;
			var tempPhotoVo : PhotoVo;
			for (var i:int = 0; i < albumList.length; i++) 
			{
				albumVo = albumList[i];
				for (var j:int = 0; j < albumVo.photoList.length; j++) 
				{
					tempPhotoVo = albumVo.photoList[j];
					
					// Check if we have a match but that is not the same photovo ID
					if(tempPhotoVo.name && tempPhotoVo.name == photoVo.name && tempPhotoVo.id != photoVo.id ) {
						return albumVo.photoList[j];
					}
				}
			}
			return null;
		}
		
		
		
		/**
		 * reset all photovo list used to false
		 */
		private function resetPhotoVoUsed():void
		{
			var albumList : Vector.<AlbumVo> = SessionManager.instance.albumList;
			var albumVo : AlbumVo;
			var tempPhotoVo : PhotoVo;
			for (var i:int = 0; i < albumList.length; i++) 
			{
				albumVo = albumList[i];
				for (var j:int = 0; j < albumVo.photoList.length; j++) 
				{
					tempPhotoVo = albumVo.photoList[j];
					tempPhotoVo.used = false;
				}
			}
		}
		
		
		
		/**
		 * FIND/GET PHOTO VO BY ID
		 */
		public function getPhotoVoById( photoId : String ):PhotoVo
		{
			var albumList : Vector.<AlbumVo> = SessionManager.instance.albumList;
			var albumVo : AlbumVo;
			var tempPhotoVo : PhotoVo;
			for (var i:int = 0; i < albumList.length; i++) 
			{
				albumVo = albumList[i];
				for (var j:int = 0; j < albumVo.photoList.length; j++) 
				{
					tempPhotoVo = albumVo.photoList[j];
					if(tempPhotoVo.id == photoId) {
						return albumVo.photoList[j];
					}
				}
			}
			return null;
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * when project is ready
		 */
		private function projectUpdated():void
		{
			// say it to the world
			PROJECT_UPDATED.dispatch();
			
			if(Infos.isCanvas) {
				CanvasManager.instance.initMultipleFrame( true );
			}
		}
		
		/**
		 * when project is ready
		 */
		private function projectReady():void
		{
			//Flag it : 
			ready = true;
			
			// load specific project prefs and reset general setup prefs
			Infos.prefs.loadProjectPrefs();
			
			// say it to the world
			PROJECT_READY.dispatch();
			
			
			// CANVAS MULTIPLE SPECIFIC INIT
			if(Infos.isCanvas && CanvasManager.instance.isMultiple) CanvasManager.instance.initMultipleFrame();
			
		}
		
		
		
		
		
		
		
		/**
		 * clear multipart loader for save
		 */
		private function clearUploader():void
		{
			// clean image sender
			if(saveUploader){
				saveUploader.removeEventListener(Event.COMPLETE, projectSaveComplete);
				saveUploader.removeEventListener(IOErrorEvent.IO_ERROR, projectSaveError);
				saveUploader.close();
				saveUploader = null;
			}
		}
		
		
		
		
		
				
		
		
		
		/**
		 * START UP XML AND OTHER STUFF ERROR HANDLING
		 * Some of the loaded items are mandatory for the app
		 * but some of them are not really (custom backgrounds or custom layouts)
		 * If one the non-mandatory item fail, let's just remove it and continue
		 */
		private function onLoadError(e:ErrorEvent):void
		{
			DataLoadingManager.instance.hideLoading();
			Debug.warn("ProjectManager.onLoadError : Initial content load error : " + e.toString());
			
			//handling error
			var nonMandatoryItemList:Array = ["customLayout","customBackgrounds", "news"];
			
			//add assets xml ad non mandatory for offline
			if(Infos.IS_DESKTOP)
			{
				nonMandatoryItemList.push("backgrounds");
				nonMandatoryItemList.push("cliparts");
				nonMandatoryItemList.push("overlayers");
			}
			
			for (var i:int = 0; i < loader.items.length; i++) 
			{
				var item:LoadingItem = loader.items[i] as LoadingItem;
				var id:String = item.id;

				Debug.log("=> ITEM '"+ item.id + "' has status : " + item.status);
				Debug.log("=> ITEM URL: '"+ item.url.url);
				if(item.status != null && item.status == "error")
				{
					for (var j:int = 0; j < nonMandatoryItemList.length; j++) 
					{
						if(id == nonMandatoryItemList[j])
						{
							//this is a non mandatory item, let's remove it and continue
							loader.remove(id);
							Debug.warnAndSendMail("ERROR AT START UP: Trying to load '"+id+"'");
							return;
						}
					}
				}
			}

			PopupAbstract.Alert(ResourcesManager.getString("connectioncheck.down.title"), ResourcesManager.getString("connectionCheck.down.desc"), false, App.instance.reload );
			Debug.warnAndSendMail("BULKLOADER ERROR AT STARTUP -> CHECK LOGS"); 
			//Debug.error("Initial content load error, no solution found, Application cannot continue..");
		}
		
		
		/**
		 * CLEAN & VERIFY PROJECT has multiple purpose
		 * > check project validity ( verify possible leaks, build incompatiblities, etc..) 
		 * > check all pages have one background only at position 0 (as it's not always the case in layout xmls)
		 * > check that pages width do fit current project width (otherwise display error) 
		 * > check if there are photos not available anymore 
		 * > probably update layouts width/height after upgrade also 
		 */
		private function cleanAndVerifyProject():void
		{
			Debug.log("ProjectManager > cleanAndVerifyProject");
			
			// check if project as a build (otherwise, for security, add the current build)
			if( !project.build ){
				Debug.warn( "Project with id : "+_projectId + " doesn't have a build version, we are adding current build version to it : "+Infos.buildVersion );
				project.build = Infos.buildVersion;
			}
			
			
			// always re-update project properties with the one from xml
			var docType : String = project.docType;
			var projectNode : XML = projectsXML.projects.children().(@docidnum==docType)[0];
			var docCode:String = projectNode.@docidstr;
			var documentNode : XML = documentsXML.document.(@name==docCode)[0];
			var coverNode: XML = coversDocumentXML.documents.document.(@name == "C"+docCode)[0];
			var needPageUpdate : Boolean = project.fillFromXML(projectNode,documentNode, true);
			if(!Debug.USE_OLD_ALBUM_SIZE_SYSTEM && needPageUpdate){
				Debug.warn("ProjectManager > cleanAndVerifyProject : Scale project to new project size");
				project.updatePagesVo(documentsXML, coverNode, 0);
				PopupAbstract.Alert(ResourcesManager.getString("popup.sizeUpdate.warning.title"), ResourcesManager.getString("popup.sizeUpdate.warning.description"),false);
			}
			
			
			
			// Check album cover validity
			var coverWidth : Number = project.getCoverBounds().width;
			if( project.classname == ProductsCatalogue.CLASS_ALBUM  && !project.coverVo ) Debug.warn( "ProjectManager > clean project > project is album but do not have cover.." );
			else if (project.classname == ProductsCatalogue.CLASS_ALBUM  && project.coverVo){
				var coverVo : PageVo = project.coverVo;
				if( project.hasCustomCover && coverVo is PageCoverClassicVo ) Debug.warn( "ProjectManager > clean project > project coverType is :"+project.coverType + " but covervo is Classic vo" );
				else if( !project.hasCustomCover && !(coverVo is PageCoverClassicVo)) Debug.warn( "ProjectManager > clean project > project coverType is :"+project.coverType + " but covervo is not classic cover vo" );
				if (project.coverType != ProductsCatalogue.COVER_LEATHER && coverVo.bounds.width != coverWidth) Debug.warn("ProjectManager > clean project > Cover bounds doesn't fit project Cover bounds : covervo width : "+project.coverVo.bounds.width + " but project has width of " + coverWidth);
				
				// FIX for cover type
				project.coverType = ( project.coverVo is PageCoverClassicVo )? ProductsCatalogue.COVER_LEATHER : ProductsCatalogue.COVER_CUSTOM;
			}
			
			/*
			if(project.classname == ProductsCatalogue.CLASS_ALBUM && project.coverType == ProductsCatalogue.COVER_CUSTOM
				Debug.warn( "Project with id : "+projectId + " doesn't have a build version, we are adding current build version to it : "+Infos.buildVersion );
				project.build = Infos.buildVersion;
			}
			*/
			
			// check photos used in album
			var pageVo : PageVo;
			var frameVo : FrameVo;
			var bkgFound : Boolean;
			var projectWidth : Number = Math.round(Infos.project.width*Infos.project.PFLMultiplier);
			var backgroundBounds : Rectangle = BackgroundsManager.instance.getPageBackgroundBounds(); // page background bounds, used to check that all backgrounds are ok
			var tempTf:TextField = new TextField();
			var tempTextFormat:TextFormat = (TextFormats.LAST_PAGE_NUMBER_USED)?TextFormats.LAST_PAGE_NUMBER_USED:TextFormats.DEFAULT_FRAME_TEXT(LayoutManager.instance.getFontSizeForAlbumPageNumber(),Colors.BLACK);
			
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				
				bkgFound = false;
				pageVo = project.pageList[i];
				
				if(i>0 && pageVo.bounds.width != projectWidth){
					Debug.warn("ProjectManager > clean project > Page bounds doesn't fit project bounds : pageVo at index : "+i+ " has width of "+pageVo.bounds.width + " but project has width of " + projectWidth);
				}
				var numFrames : int = pageVo.layoutVo.frameList.length;
				if(!(pageVo is PageCoverClassicVo) && numFrames == 0 ){
					Debug.warn("ProjectManager > clean project > pageVo at index : "+i+ " has no frames, strange, we create at least a background");
					pageVo.layoutVo.frameList.unshift(BackgroundsManager.instance.createBackgroundFrameVo(pageVo.isCover));
				}
				
				// CHECK COVER BACKGROUND SIZE
				if( pageVo.isCover && !(pageVo is PageCoverClassicVo) && numFrames >0 && pageVo.layoutVo.frameList[0].type == FrameVo.TYPE_BKG )
				{
					var bkg : FrameVo = pageVo.layoutVo.frameList[0];
					var neededBounds : Rectangle = BackgroundsManager.instance.getCoverBackgroundBounds();
					if( bkg.width != neededBounds.width ||
							bkg.height != neededBounds.height ){
						Debug.warn("Cover background should be : "+neededBounds.toString() + " but cover background has bounds of " + bkg.boundRect + "\n WE update background bounds");
						bkg.width = neededBounds.width;
						bkg.height = neededBounds.height;
						if(bkg.photoVo && bkg.photoVo is BackgroundVo) FrameVo.injectPhotoVoInFrameVo(bkg, bkg.photoVo);
					}
				}
				
				//Fix for calendar bug (apply to all layout)
				/*if(project.docCode == "WCAL2")
				{
					if(pageVo.isCalendar && pageVo.layoutVo.id != "700")
					{
						var newLayoutVo:LayoutVo = LayoutManager.instance.getLayoutById("700",false,false);
						pageVo.layoutId = newLayoutVo.id;
						pageVo.layoutVo = newLayoutVo;
					}
							
				}*/
				
					
				// check frames
				var frameURLIndex:int = -1;
				var pageNumberTextFormat:TextFormat;
				var hasPageNumber:Boolean;
				
				for (var j:int = 0; j < numFrames; j++) 
				{
					frameVo = pageVo.layoutVo.frameList[j];
					
					
					//Calendar frames - VAlign fixes
					if(frameVo.vAlign == "center") frameVo.vAlign = "middle";
					
					//Calendar frames -VAlign fix for specific frame in specific type of project
					if(project.docCode == "WCAL2" && frameVo.dateAction == CalendarManager.DATEACTION_WEEKDAY)
					{
						frameVo.vAlign = "middle"
					}
					
					//Linked Frames check
					if(frameVo.uniqFrameLinkageID != null 
						&& frameVo.type != FrameVo.TYPE_CALENDAR 
						&& frameVo.type != FrameVo.TYPE_OVERLAYER
						&& frameVo.dateLinkageID == null)
					{
						Debug.warn("ProjectManager > clean project > pagevo ("+i+") has uniqFrameLinkageID but dateLinkageID is null");
						var linkedFrames:Vector.<FrameVo> = PagesManager.instance.findFramesVoByLinkage(frameVo.uniqFrameLinkageID,frameVo, pageVo);

						if(linkedFrames)
						{
							var linkedFrameVo:FrameVo = linkedFrames[0];
							if(linkedFrameVo.type == FrameVo.TYPE_CALENDAR)
							{
								frameVo.dateLinkageID = CalendarManager.instance.getDateLinkageId(linkedFrameVo);
							}
						}
						
					}
					
					//PAGE NUMBER CHECK
					hasPageNumber = false;
					if(frameVo.isPageNumber)
					hasPageNumber = true;
										
					// CHECK IF FRAME HAS TYPE !
					if(!frameVo.type ) Debug.warn("Frame with index : "+j + " in page " + i + " has NO TYPE !?"); 
					
					// BE SURE THAT ALL TEXT FRAMES ARE EDITABLE & non fixed
					//if(frameVo.type == FrameVo.TYPE_TEXT && frameVo.editable == false) frameVo.editable = true;
					if(frameVo.type == FrameVo.TYPE_TEXT)
					{
						frameVo.editable = true;
						frameVo.fixed = false;
					}
					
					// CHECK IF FIRST FRAME IS BACKGROUND otherwise inject a new one
					if(j == 0)
					{
						if( frameVo.type != FrameVo.TYPE_BKG ) // inject background at start
						{
							Debug.warn("ProjectManager > clean project > pagevo ("+i+") has no background at index 0, we will add one");
							pageVo.layoutVo.frameList.unshift(BackgroundsManager.instance.createBackgroundFrameVo(pageVo.isCover));
							numFrames ++;
							j++;
						}
						else if( !(pageVo.isCover) )// page has background, quick check if size seems correct based on project info
						{
							if(frameVo.x != backgroundBounds.x) frameVo.x = backgroundBounds.x;
							if(frameVo.y != backgroundBounds.y) frameVo.y = backgroundBounds.y;
						}
					}
					
					// MARK FRAME WITH URL TICTAC
					// For now we remove this frame if it exists for postcards and postcards XL (user complain))
					if(frameVo.dateAction == CalendarManager.DATEACTION_URL)
					{
						// For postcards and postcards xl we remove it
						if( project.docPrefix == "PC" || project.docPrefix == "PX" )
						{
							pageVo.layoutVo.frameList.splice(j,1);
							numFrames --;
							j--;
						}
						else
						{
							frameURLIndex = j; // store position so we can put on on top depth later
						}
					}
					
					//AVOID FRAME with height 0 and width 0
					if(frameVo.width <= 0 || frameVo.height <= 0)
					{
						pageVo.layoutVo.frameList.splice(j,1);
						numFrames --;
						j--;
						Debug.warn("ProjectManager > clean project > FrameVo has a width of ("+frameVo.width+") and a height of ("+frameVo.height+") so we removed it");
					}
					
					
					/*
					else (j != 0 && frameVo.type == FrameVo.TYPE_BKG)
					{
						Debug.warn("ProjectManager > clean project > pagevo ("+i+") has a background at index "+j+", we will delete it");
						pageVo.layoutVo.frameList.splice(j,1);
						numFrames --;
						j--;
					}
					*/
					
				}
				
				//PAGE NUMBER CHECK
				if(project.pageNumber && !hasPageNumber && !pageVo.isCover)
				{
					var framePageNumberVo:FrameVo = PagesManager.instance.getPageNumberFrameVo(pageVo,i,tempTf, tempTextFormat);
					if(framePageNumberVo != null)
					pageVo.layoutVo.frameList.push(framePageNumberVo);
				}
				
				// IF FRAME WITH URL TICTAC IS MARKED ( != -1) then put it on top
				if( frameURLIndex != -1 ) {
					var frameURLVo:FrameVo = pageVo.layoutVo.frameList.splice(frameURLIndex,1)[0];
					pageVo.layoutVo.frameList.push(frameURLVo);
				}				
			}
			
			// recover possible voucher from session
			if(Infos.session.voucher && !project.promoCode){
				project.promoCode = Infos.session.voucher;
				project.promoReduction = parseFloat(Infos.session.voucher_value);
				project.promoIsPercent = (Infos.session.voucher_type == "p")?true : false;
			}
			
			//Clean 
			if(tempTf)
				tempTf = null;
			
			if(tempTextFormat)
				tempTextFormat = null;
			
			Debug.log("ProjectManger > end of cleanAndVerifyProject");
			
		}
		
		
		/**
		 * This is a rare case,
		 * if a photo doesn't exist anymore in user photo list
		 * And this photo was created on another server (beta or dev)
		 * The url in the project save could be wrong (editor, beta, dev)
		 * We then try to normalize this url to fit the current server
		 */
		public function normalizePhotoUrl( photoVo : PhotoVo ):void
		{
			if(photoVo.thumbUrl.indexOf(Infos.config.serverType) == -1)
			{
				Debug.log("ProjectManager.normalizeUrl : serverType = "+Infos.config.serverType + " & normalUrl = "+photoVo.normalUrl);
				photoVo.normalUrl.split("beta.tictacphoto.com").join(Infos.config.serverType);
				photoVo.normalUrl.split("dev.tictacphoto.com").join(Infos.config.serverType);
				photoVo.normalUrl.split("editor.tictacphoto.com").join(Infos.config.serverType);
				photoVo.highUrl.split("beta.tictacphoto.com").join(Infos.config.serverType);
				photoVo.highUrl.split("dev.tictacphoto.com").join(Infos.config.serverType);
				photoVo.highUrl.split("editor.tictacphoto.com").join(Infos.config.serverType);
				photoVo.thumbUrl.split("beta.tictacphoto.com").join(Infos.config.serverType);
				photoVo.thumbUrl.split("dev.tictacphoto.com").join(Infos.config.serverType);
				photoVo.thumbUrl.split("editor.tictacphoto.com").join(Infos.config.serverType);
			}
		}
	}
}
class SingletonEnforcer{}