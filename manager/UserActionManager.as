/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import flash.utils.ByteArray;
	
	import data.Infos;
	import data.PageVo;
	
	import utils.Debug;
	import utils.KeyboardManager;
	
	import view.edition.EditionArea;
	import view.edition.pageNavigator.PageNavigator;
	import view.edition.toolbar.EditionToolBar;
	

	/**
	 * Print manager
	 */
	public class UserActionManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// action type
		public static const TYPE_FRAME_CREATE : String = "TYPE_FRAME_CREATE";
		public static const TYPE_FRAME_DELETE : String = "TYPE_FRAME_DELETE";
		public static const TYPE_FRAME_UPDATE : String = "TYPE_FRAME_UPDATE";
		public static const TYPE_FRAME_TEXT_UPDATE : String = "TYPE_FRAME_TEXT_UPDATE";
		public static const TYPE_FRAME_COPY : String = "TYPE_FRAME_COPY";
		public static const TYPE_FRAME_CUT : String = "TYPE_FRAME_CUT";
		public static const TYPE_FRAME_PASTE : String = "TYPE_FRAME_PASTE";
		public static const TYPE_COLOR_CHANGE : String = "TYPE_COLOR_CHANGE";
		
		public static const TYPE_PAGE_LAYOUT_UPDATE : String = "TYPE_PAGE_LAYOUT_UPDATE"; 
		public static const TYPE_PAGE_SWITCH : String = "TYPE_PAGE_SWITCH"; // TODO : needs to be done as otherwise page index stored will not be always correct
		public static const TYPE_BACKGROUND_UPDATE : String = "TYPE_BACKGROUND_UPDATE"; 
		
		public static const TYPE_AUTO_FILL : String = "TYPE_AUTO_FILL"; 
		public static const TYPE_ALL_BACKGROUND_CHANGED : String = "TYPE_ALL_BACKGROUND_CHANGED"; 
		public static const TYPE_ALL_LAYOUTS_CHANGED : String = "TYPE_ALL_LAYOUTS_CHANGED"; 
		public static const TYPE_ALL_FRAME_BORDER_UPDATE : String = "TYPE_ALL_FRAME_BORDER_UPDATE";
		public static const TYPE_ALL_CALENDAR_FRAME_UPDATE : String = "TYPE_ALL_CALENDAR_FRAME_UPDATE";

		public static const TYPE_PROJECT_UPGRADE : String = "TYPE_PROJECT_UPGRADE"; 
		public static const TYPE_PROJECT_UPDATE : String = "TYPE_PROJECT_UPDATE"; 
		
		public static const TYPE_PHOTO_SWAP : String = "TYPE_PHOTO_SWAP";
		public static const TYPE_MULTIPLE_FRAME_CHANGE : String = "TYPE_MULTIPLE_FRAME_CHANGE";
		
		
		
		
		public var actionsSinceLastSave : Number = 0;
		
		// private
		private const MAX_HISTORY_ITEMS : int = 20;
		
		// ui
		private var editionArea : EditionArea;
		private var editionToolbar : EditionToolBar;
		
		// data
		private var initialProjectState : ByteArray;
		private var actionsHistory : Vector.<Action>;
		private var actionIndex : int = -1;
		
		// Cover save
		private var covers : Object = {};
		
		
		/**
		 * ------------------------------------ SINGLETON-------------------------------------
		 */
		
		public function UserActionManager(sf:SingletonEnforcer) { if(!sf) throw new Error("UserActionManager is a singleton, use instance"); }
		private static var _instance:UserActionManager;
		public static function get instance():UserActionManager
		{
			if (!_instance) _instance = new UserActionManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		/**
		 * ------------------------------------ GETTER/SETTER -------------------------------------
		 */
		
		public function get hasPreviousAction():Boolean
		{
			if(actionsHistory && actionIndex>-1) return true;
			return false;
		}
		
		public function get hasRedoAction():Boolean
		{
			if(actionsHistory && actionIndex < actionsHistory.length-1) return true;
			return false;
		}
		

		/**
		 * ------------------------------------ INITIALIZE -------------------------------------
		 */
		
		public function init( $editionArea : EditionArea, $editionToolbar : EditionToolBar ):void
		{
			editionArea = $editionArea;
			editionToolbar = $editionToolbar;
			editionToolbar.init(editionArea);
			editionToolbar.update();
			
			// get current project state
			initialProjectState = getCurrentProjectState();
			
			// create action history list
			actionsHistory = new Vector.<Action>();
			
			KeyboardManager.CTRL_Z.add(ctrlZHandler);
		}
		
		/**
		 * save the current cover vo (PageVo)
		 * this function has no virification on the pageVo really beeing a cover because it must be done before calling this function
		 */
		public function savedCurrentCoverPageVo(pageVo:PageVo):void
		{

			if(pageVo.coverType)
			covers[pageVo.coverType] = pageVo;
			else
				Debug.warn("UserActionManager > savedCurrentCoverPageVo : pageVo.coverType is: "+pageVo.coverType + "(this properties has been added in build 91007)");
		}
		
		/**
		 * look for a saved version of the given cover type
		 * if none, create a blank one
		 */
		public function getSavedCoverPageVo(coverType:String):PageVo
		{
			//if one of this type, return it
			if(covers[coverType])return covers[coverType];
			
			return null;
			
		}
		
		
		/**
		 * just update toolbar when some actions are done
		 */
		public function updateToolbar():void
		{
			if(editionToolbar) editionToolbar.update();
		}
		
		/**
		 *
		 */
		public function addAction(type : String, data : * = null):void
		{
			Debug.log("Add action : "+type);
			if(!actionsHistory) return;	// if no history, we do nothing
			if(!PagesManager.instance.currentEditedPage){
				Debug.warn("UserActionManager.addAction WARNING : CurrentEditedPage is null");
				return;
			}
			
			try{
				// be sure to erase actions already undone
				actionsHistory = actionsHistory.slice(0, actionIndex+1);
				
				// check max items
				if(actionsHistory.length > MAX_HISTORY_ITEMS-1) {
					var firstAction : Action = actionsHistory.shift(); // remove first element
					initialProjectState = firstAction.projectState;
				}
				
				// For each new action done, we store a complete version of project.
				var projectState:ByteArray = getCurrentProjectState();
				actionsHistory.push(new Action(type, data, PagesManager.instance.currentEditedPage.index, projectState));
				
				//
				actionIndex = actionsHistory.length-1;
				actionsSinceLastSave ++;
			}
			catch(e:Error)
			{
				Debug.warn("UserActionManager.addAction error :"+e.message + " (actionIndex is : "+actionIndex + " & actionHistory.length = "+actionsHistory.length + ")");
			}
			
			
			updateToolbar();
		}
		
		/**
		 * ------------------------------------ UNDO ACTION -------------------------------------
		 */
		
		public function undoAction():void
		{
			try{
				actionIndex --;
				if(actionIndex+1 < actionsHistory.length) 
					updateProjectState(actionsHistory[actionIndex+1]);	
			}
			catch(e:Error){} // fail silentely, this could be user going too quick here
		}
		private function ctrlZHandler():void
		{
			if(hasPreviousAction) undoAction();
		}
		
		
		
		/**
		 * ------------------------------------ REDO ACTION -------------------------------------
		 */
		public function redoAction():void
		{
			try{
				actionIndex ++;
				updateProjectState(actionsHistory[actionIndex]);
			}
			catch(e:Error){} // fail silentely, this could be user going too quick here
		}
		
		

		/**
		 * ------------------------------------ CLEAR HISTORY -------------------------------------
		 */
		public function clearHistory():void
		{
			if(actionsHistory)
			{
				actionsSinceLastSave  = 0;
				actionIndex = -1;
				if(initialProjectState) initialProjectState.clear();
				
				for (var i:int = 0; i < actionsHistory.length; i++) 
				{
					actionsHistory[i].projectState.clear();
					actionsHistory[i].projectState = null;
					actionsHistory[i].datas = null;
				}
				
				covers  = {};
				
				// get current project state
				initialProjectState = getCurrentProjectState();
				
				// create action history list
				actionsHistory = new Vector.<Action>();	
				updateToolbar();
			}
		}

		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////

		/**
		 *
		 */
		private function updateProjectState( actionRef : Action ):void
		{
			Debug.log("UNDO/REDO > Reset project state to index : " + actionIndex + " from total actions : "+ actionsHistory.length);
			// retrieve previousProjectState
			var projectByte : ByteArray;
			if(actionIndex == -1 ) projectByte = initialProjectState;
			else projectByte = actionsHistory[actionIndex].projectState;
			//projectByte.uncompress();
			projectByte.position = 0;
			var project : Object = projectByte.readObject();
			
			switch ( actionRef.type ){
				
				case TYPE_PAGE_LAYOUT_UPDATE : // TODO : not sure this one will work, to be tested
				case TYPE_FRAME_PASTE :
				case TYPE_FRAME_CREATE :
				case TYPE_FRAME_DELETE :
				case TYPE_FRAME_UPDATE : 
				case TYPE_FRAME_TEXT_UPDATE : 
					// get page object
					var pageVo:PageVo = updatePageVo(project, actionRef.pageIndex);
					goToPage(pageVo);					
					break;
				
				case TYPE_FRAME_CUT : 
					// get page object
					pageVo = updatePageVo(project, actionRef.pageIndex);
					var cutPageVo:PageVo = updatePageVo(project, actionRef.datas as Number);
					goToPage(cutPageVo);
					break;
				case TYPE_PHOTO_SWAP : 
					// get page object
					pageVo = updatePageVo(project, actionRef.datas.page1 as Number);
					if(actionRef.datas.page2) updatePageVo(project, actionRef.datas.page2 as Number);
					goToPage(pageVo);
					break;
				
				
				// TODO handle this
				case TYPE_MULTIPLE_FRAME_CHANGE : 
					/*
					// get page object
					pageVo = updatePageVo(project, actionRef.data.page1 as Number);
					if(actionRef.data.page2) updatePageVo(project, actionRef.data.page2 as Number);
					goToPage(pageVo);
					*/
					break;
					
				
				case TYPE_COLOR_CHANGE :
					//Re-inject globalCalendarColors
					/*Infos.project.globalCalendarColors = new CalendarColors();
					for (var props:String in project.globalCalendarColors) {
						Infos.project.globalCalendarColors[props] = project.globalCalendarColors[props];
					}	*/					
					pageVo = updatePageVo(project, actionRef.pageIndex);
					goToPage(pageVo);
					break;
				
				case TYPE_AUTO_FILL : 
				case TYPE_ALL_CALENDAR_FRAME_UPDATE:
				case TYPE_ALL_FRAME_BORDER_UPDATE:
				case TYPE_ALL_BACKGROUND_CHANGED:
				case TYPE_ALL_LAYOUTS_CHANGED:
				case TYPE_PROJECT_UPGRADE :
				case TYPE_PROJECT_UPDATE :
					ProjectManager.instance.restoreProjectFromHistory(project);
					pageVo = updatePageVo(project, actionRef.pageIndex);
					goToPage(pageVo);
					break;
			}
			
			
			//
			updateToolbar();
			
			//update used photo
			Debug.log("UserActionManager > REMAPPHOTOLIST After UNDO or REDO");
			ProjectManager.instance.remapPhotoVoList();
		}
		
		/**
		 * recreate page vo
		 */
		private function updatePageVo( project:Object, pageIndex : Number ):PageVo
		{
			var pageVo:PageVo = Infos.project.pageList[pageIndex];
			var pageObject : Object = project.pageList[pageVo.index];
			pageVo.fillFromSave(pageObject);
			return pageVo;
		}
		
		/**
		 * recreate page vo
		 */
		private function goToPage( pageVo : PageVo ):void
		{
			// GO TO PAGE
			if(PagesManager.instance.currentEditedPage != pageVo ){
				PageNavigator.PAGE_CLICKED.dispatch(pageVo, true);				
			} 
			// UPDATE PAGE
			else {
				PageVo.PAGEVO_UPDATED.dispatch(pageVo);
			}
		}
		
		/**
		 * retrieve project state on bytearray
		 */
		private function getCurrentProjectState():ByteArray
		{
			var projectState:ByteArray = new ByteArray();
			projectState.writeObject(Infos.project);
			projectState.position = 0;
			return projectState;
		}
		
	}
}
import data.PageVo;

import flash.utils.ByteArray;

internal class Action
{
	public var type : String;
	public var datas : *;
	public var pageIndex :int;
	public var projectState : ByteArray;
	
	public function Action(_type:String, _datas:*, _pageIndex:int, _projectState : ByteArray):void
	{
		pageIndex = _pageIndex;
		type = _type;
		datas = _datas;
		projectState = _projectState;
	}
	
}

class SingletonEnforcer{}