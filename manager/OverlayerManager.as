/***************************************************************
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import be.antho.data.ResourcesManager;
	
	import data.Infos;
	import data.OverlayerVo;
	
	import org.osflash.signals.Signal;
	
	import utils.Debug;
	
	import view.popup.PopupAbstract;
	
	CONFIG::offline{
		import offline.manager.OfflineAssetsManager;
		import offline.manager.ZIPManager;
		import flash.filesystem.File;
		import be.antho.air.FileUtils;
	}
		
		
	
	/**
	 * Overlayer manager
	 */
	public class OverlayerManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		public static const OVERLAYS_REFRESH_COMPLETE:Signal = new Signal();
		private var overlayerXMLList : XMLList;
		public var overlayerList : Vector.<OverlayerVo>;
		private var initiated:Boolean = false;
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function OverlayerManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("OverlayerManager is a singleton, use instance"); 
			/*if(!initiated)
				reset();*/
		}
		
		/**
		 * instance
		 */
		private static var _instance:OverlayerManager;
		public static function get instance():OverlayerManager
		{
			if (!_instance) _instance = new OverlayerManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		public function get showDownloadMoreBtn():Boolean
		{
			//not used anymore, return false // could be deleted at some point
			return false;
			
			//download more btn visibility
			CONFIG::online
			{
				return false;
			}
				
			CONFIG::offline
			{
				return !OfflineAssetsManager.instance.checkIfDownloaded(OfflineAssetsManager.OVERLAYS);
			}
		}
		
		/**
		 * Get local xml path
		 */
		public function get xmlPath():String
		{
			CONFIG::offline
			{
				return Infos.offlineAssetsCommonPath + "/overlays.xml";
			}
			return null;
		}
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Reset manager
		 * Set initiated to false to force init
		 */
		public function reset():void
		{
			if(Infos.IS_DESKTOP)
				refresh();
			else			
				init();
		}
		
		/**
		 * Refresh offline only
		 * Reload XML and reset Manager
		 */
		public function refresh():void
		{
			CONFIG::offline
				{
					
					Debug.log("OverlaysManager.refresh");
					var file:File = new File(xmlPath); 
					if(file.exists)
					{
						Debug.log("OverlaysManager.refresh > new xml exist");
						
						OfflineAssetsManager.instance.getXMLLocally(xmlPath, 
							function(result:Object):void
							{
								var freshXML:XML = new XML(result.toString());
								ProjectManager.instance.overlayersXML = freshXML;
								init();
								OVERLAYS_REFRESH_COMPLETE.dispatch();
							}, 
							function(e:*):void
							{
								Debug.log("OverlaysManager.refresh failed");
							});
					}
					else
						Debug.log("OverlaysManager.refresh failed > file does not exist");
				}
		}
		
		/**
		 * Update offline XML with freshly downloaded one
		 * Replace local XML and reset Manager
		 */
		public function replaceLocalXML(freshXML:XML, onComplete:Function):void
		{
			Debug.log("OverlayersManager.replaceLocalXML");
			
			CONFIG::offline
			{
				OfflineAssetsManager.instance.saveXMLLocally(freshXML,xmlPath, 
					// on success	
					function(xmlString:String):void
					{
						OverlayerManager.instance.reset();
						onComplete.call();
					});
			}
			
			CONFIG::online
			{
				ProjectManager.instance.overlayersXML = freshXML;
				OverlayerManager.instance.reset();
				onComplete.call();
			}
		}
		
		
		/**
		 * init manager
		 * > create overlayer list
		 */
		public function init():void
		{
			overlayerXMLList = ProjectManager.instance.overlayersXML.document.overlay;
			overlayerList = new Vector.<OverlayerVo>();
			
			var vo : OverlayerVo;
			for (var i:int = 0; i < overlayerXMLList.length(); i++) 
			{
				vo = new OverlayerVo();
				vo.fillFromXML(overlayerXMLList[i]);
				overlayerList.push(vo);
			}
			
			// flag
			initiated = true;
			
			if(Infos.IS_DESKTOP)
			{
				Infos.INTERNET_CHANGED.remove(internetChangedHandler);
				Infos.INTERNET_CHANGED.add(internetChangedHandler);
			}
		}
		
		public function internetChangedHandler():void
		{
			Debug.log("OverlayerManager > InternetChanged");
			reset();
		}
		
		/**
		 * get overlayer vo by id
		 */
		public function getVoById(id:String):OverlayerVo
		{
			for (var i:int = 0; i < overlayerList.length; i++) 
			{
				if(overlayerList[i].id == id) return overlayerList[i];
			}
			
			Debug.warn("Overlayer with id : "+ id+ " doesn't exits in overlayerList");
			return null;
		}
		
		
		
		/**
		 * ------------------------------------ OFFLINE DOWNLOAD MORE -------------------------------------
		 */
		
		
		/**
		 * retrieve the cover background bounds (it has specific bleed of 10mm) and take the spine into calculation
		 */
		public function downloadMore(popup:PopupAbstract = null):void
		{
			CONFIG::offline
			{
				Debug.log("OverlaysManager > downloadMore");
				
				if(Infos.INTERNET)
				{
					//Call
					//var url:String = "http://www.smileasyoudoit.com/customers/tictac/servicetemp/overlays_all.zip";
					var url:String = Infos.config.getSettingsValue("OfflineAssetsToDownloadUrl") + "overlays_all.zip";
					var destination:String = Infos.offlineAssetsCommonPath;
					
					ZIPManager.instance.downloadAndUnzip(url, destination,downloadMoreSuccess, downloadMoreError, false, true);
				}
				else
					PopupAbstract.Alert(ResourcesManager.getString("popup.no.internet.title"), ResourcesManager.getString("popup.no.internet.desc"), false, null );
			}
		}
		/**
		 * Download more bkg SUCCESS
		 */
		private function downloadMoreSuccess():void
		{
			CONFIG::offline
			{
				Debug.log("OverlaysManager.downloadMoreSuccess");
				OfflineAssetsManager.instance.markAsDownloaded(OfflineAssetsManager.OVERLAYS);
		
				refresh();
				PopupAbstract.Alert(ResourcesManager.getString("popup.download.ready.title"), ResourcesManager.getString("popup.new.overlayer.ready.desc"),false,null,null,null);
			}
		}
		/**
		 * Download more bkg ERROR
		 */
		private function downloadMoreError(msg:String):void
		{
			Debug.log("OverlaysManager.downloadMoreError > Error");
			Debug.log("Abort message: "+msg);
			Debug.warnAndSendMail(msg);
			
			//todo: clean everything
			
			//Show popup
			PopupAbstract.Alert(ResourcesManager.getString("popup.download.failed.title"), ResourcesManager.getString("popup.download.failed.desc"),false,null,null);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		

	}
}
class SingletonEnforcer{}