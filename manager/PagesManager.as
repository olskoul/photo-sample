/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import flash.utils.flash_proxy;
	
	import be.antho.bitmap.snapshot.SnapShot;
	import be.antho.data.ResourcesManager;
	import be.antho.utils.DataLoadingManager;
	import be.antho.utils.MathUtils2;
	
	import data.BackgroundCustomVo;
	import data.BackgroundVo;
	import data.CalendarColors;
	import data.ClipartVo;
	import data.DateLinkageVo;
	import data.FrameVo;
	import data.Infos;
	import data.PageCoverClassicVo;
	import data.PageVo;
	import data.PhotoVo;
	import data.ProductsCatalogue;
	import data.ProjectVo;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	import utils.TextFormats;
	import utils.VectorUtils;
	
	import view.edition.EditionArea;
	import view.edition.Frame;
	import view.edition.FrameText;
	import view.edition.PageArea;
	import view.edition.pageNavigator.PageNavigator;
	import view.edition.toolbar.EditionToolBar;
	import view.popup.PopupAbstract;
	

	/**
	 * Pages manager should allow frame manipulation and frames group modifications
	 * > it works almost only on the datas and is used mostly as an helper
	 */
	public class PagesManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public static const CURRENT_EDITED_PAGE_CHANGED:Signal = new Signal();
		public static const BACKGROUND_UPDATED:Signal = new Signal();
		
		// object containing all linkages
		private var linkageList : Object = new Object();
		
		// the page currently edited
		private var _currentEditedPage:PageVo;
		
		//Album flyleaf colors 
		private var _flyleafColorsAvailable:Array;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		
		/**
		 * Constructor
		 */
		public function PagesManager(sf:SingletonEnforcer) { if(!sf) throw new Error("FramesManager is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:PagesManager;

		
		
		
		////////////////////////////////////////////////////////////////
		//	GETTER/SETTER
		////////////////////////////////////////////////////////////////
		public function get flyleafColorsAvailable():Array
		{
			if(_flyleafColorsAvailable)
				return _flyleafColorsAvailable;
			else
			{
				//album specifics
				if(Infos.isAlbum)
				{
					//fyleaf colors
					if(ProjectManager.instance.menuXML)
					{
						var colorList:XMLList = ProjectManager.instance.menuXML.menu.item.content.flyleaf_colors.color;
						flyleafColorsAvailable = [];
						for (var i:int = 0; i < colorList.length(); i++) 
						{
							flyleafColorsAvailable.push({color:colorList[i].@color,id:colorList[i].@id});
						}
						
					}
				}
			}
			return _flyleafColorsAvailable;
		}

		public function set flyleafColorsAvailable(value:Array):void
		{
			_flyleafColorsAvailable = value;
		}

		public static function get instance():PagesManager
		{
			if (!_instance) _instance = new PagesManager(new SingletonEnforcer())
			return _instance;
		}
		
		public function get currentEditedPage():PageVo
		{
			return _currentEditedPage;
		}
		
		public function set currentEditedPage(value:PageVo):void
		{
			_currentEditedPage = value;
			CURRENT_EDITED_PAGE_CHANGED.dispatch();
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Get FlyLeaf color by id
		 * Colors dictionary is based on the Menu.xml for albums only
		 */
		public function getFlyLeafColorByID(id:String):Number
		{
			if(flyleafColorsAvailable)
			{
				for (var i:int = 0; i < flyleafColorsAvailable.length; i++) 
				{
					if(flyleafColorsAvailable[i].id == id)
						return flyleafColorsAvailable[i].color;
				}
				return 0;
			}
			else
				return 0;
		}
		
		/**
		 * draw the postcard back (replacing old system that was loading a image and scaling it down)
		 * -> old image width was 876 pixels
		 * -> Line for adresses
		 * -> stamp area
		 * -> separation line
		 */ 
		public function drawPostCardBack( forPrint:Boolean = false ):*
		{
			var	postCardBackground:Sprite = new Sprite();
			
			var backgroundBounds : Rectangle = BackgroundsManager.instance.getPageBackgroundBounds(); // postcard background has same bounds than project background bounds
			var ratio:Number = backgroundBounds.width / 876; // ratio from old image width
			var g:Graphics = postCardBackground.graphics;
			
			g.clear();			
			
			//bg
			g.lineStyle(0,0,0,true,LineScaleMode.NORMAL);
			g.beginFill(Colors.RED_FLASH, 0);
			g.drawRect(0,0,backgroundBounds.width,backgroundBounds.height);//g.drawRect(0,0,pageVo.bounds.width,pageVo.bounds.height);
			g.endFill();
			
			//stamp
			g.lineStyle(1,0,1,true,LineScaleMode.NORMAL);
			g.drawRect(Math.round(706*ratio),Math.round(56*ratio),Math.round(119*ratio),Math.round(136*ratio));
			
			//separator
			g.moveTo(Math.round(473*ratio),Math.round(156*ratio));
			g.lineTo(Math.round(473*ratio),Math.round((156+380)*ratio));
			
			//Address lines
			g.moveTo(Math.round(500*ratio),Math.round(350*ratio));
			g.lineTo(Math.round(823*ratio),Math.round(350*ratio));
			
			g.moveTo(Math.round(500*ratio),Math.round(400*ratio));
			g.lineTo(Math.round(823*ratio),Math.round(400*ratio));
			
			g.moveTo(Math.round(500*ratio),Math.round(450*ratio));
			g.lineTo(Math.round(823*ratio),Math.round(450*ratio));
			
			g.moveTo(Math.round(500*ratio),Math.round(500*ratio));
			g.lineTo(Math.round(823*ratio),Math.round(500*ratio));
			
		/*	//Center it
			postCardBackground.x = -postCardBackground.width/2;
			postCardBackground.y = -postCardBackground.height/2;
			
			//add child it
			if(!postCardBackground.parent) addChild(postCardBackground);*/
			
			if(forPrint)
			{
				var qualityScale : Number = 3; // for best performancess
				var MAX_SIZE : Number = 2000; // with a max width of 2000 to avoid problems with upload
				if(backgroundBounds.width *qualityScale > MAX_SIZE) qualityScale = MAX_SIZE/backgroundBounds.width;
				if(backgroundBounds.height *qualityScale > MAX_SIZE) qualityScale = MAX_SIZE/backgroundBounds.height;
				/*
				var highBmd : BitmapData = new BitmapData(bmd.width*qualityScale, bmd.height*qualityScale,true, 0x000000);
				var m:Matrix = new Matrix();
				m.translate(highBmd.width*.5, highBmd.height*.5);
				m.scale(qualityScale,qualityScale);
				highBmd.draw(bmd, m, null,null, null, true);
				*/
				var bmd:BitmapData = SnapShot.snap(postCardBackground,qualityScale);// two times bigger for best print 
				return bmd
			}
			else
				return postCardBackground;
			
		}	
		
		public function centerPageNumber(flag:Boolean):void
		{
			Debug.log("PagesManager > centerPageNumber");
			
			if(!Infos.project.pageNumber)
				return;
			
			
			var frameVo : FrameVo;
			var pageVo:PageVo
			
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				pageVo = Infos.project.pageList[i];
				for (var j:int = 0; j < pageVo.layoutVo.frameList.length; j++) 
				{
					frameVo = pageVo.layoutVo.frameList[j];
					if(frameVo.isPageNumber)
					{
						var pageIndex:int = i;
						positionPageNumber(pageVo,frameVo,pageIndex);
					}
				}			
			}
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			
			Debug.log("PagesManager > centerPageNumber > end");
		}
		
		public function showPageNumber(atStart:Boolean = false):void
		{
			Debug.log("PagesManager > ShowpageNumber");
			
			if(!Infos.project.pageNumber)
				return;
			
			if(!atStart)
				DataLoadingManager.instance.showLoading(false, null, "");
			
			var frameVo : FrameVo;	
			var tempTf:TextField = new TextField();
			var tempTextFormat:TextFormat = (TextFormats.LAST_PAGE_NUMBER_USED)?TextFormats.LAST_PAGE_NUMBER_USED:TextFormats.DEFAULT_FRAME_TEXT(LayoutManager.instance.getFontSizeForAlbumPageNumber(),Colors.BLACK);
			// make list of all not empty frames
			
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				var pageVo:PageVo = Infos.project.pageList[i];
				
				if(pageVo.isCover)
					continue;
				
				frameVo = getPageNumberFrameVo(pageVo,i,tempTf,tempTextFormat);
				if(frameVo != null)
				pageVo.layoutVo.frameList.push(frameVo);
			}
			
			
			if(!atStart)
			{
				// update view
				PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
				
				// update page navigator
				EditionArea.updatePageNavigator();
				
				// add user action for undo process
				UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
				
				DataLoadingManager.instance.hideLoading();
			}
			
			Debug.log("PagesManager > ShowpageNumber > End");
		}
		
		
		public function getPageNumberFrameVo(pageVo:PageVo, index:int, tempTf:TextField, tempTextFormat:TextFormat):FrameVo
		{
			var frameVo : FrameVo;	
			
			//not on cover
			if(pageVo.isCover)
				return null;
			
			//security, do not add it twice
			if(getPageNumberFrameFromPageVo(pageVo) != null)
				return null;
			
			tempTf.text = String(index);
			tempTf.setTextFormat(tempTextFormat);
			frameVo = new FrameVo();
			frameVo.type = FrameVo.TYPE_TEXT;
			frameVo.vAlign = "bottom";
			frameVo.fixed = true;
			frameVo.isPageNumber = true;
			frameVo.fontSize = LayoutManager.instance.getFontSizeForAlbumPageNumber();
			frameVo.text = tempTf.htmlText;
			frameVo.width = pageVo.bounds.width/15;
			frameVo.height = frameVo.width;
			
			//frameVo.x = Math.round(pageVo.bounds.width/2);
			positionPageNumber(pageVo,frameVo,index);
			frameVo.y =  Math.round(pageVo.bounds.height - frameVo.height/1.8);
			
			return frameVo;
		}
		
		private function getPageNumberFrameFromPageVo(pageVo:PageVo):FrameVo
		{
			for (var i:int = 0; i < pageVo.layoutVo.frameList.length; i++) 
			{
				var frameVo:FrameVo = pageVo.layoutVo.frameList[i];
				if(frameVo.isPageNumber)
				{
					return frameVo;
				}
			}
			
			return null;
		}
		
		public function positionPageNumber(pageVo:PageVo,frameVo:FrameVo,index:int):void
		{
			if(Infos.project.pageNumberCentered)
			{
				frameVo.x = Math.round(pageVo.bounds.width/2);
			}
			else
			{
				if(MathUtils2.isEven(index))
				{
					frameVo.x = frameVo.width/2;
				}
				else
				{
					frameVo.x = Math.round(pageVo.bounds.width - frameVo.width/2);
					
					
				}
			}
		}
		
		public function hidePageNumber(forceHide:Boolean = false):void
		{
			Debug.log("PagesManager > hidePageNumber");
			
			if(Infos.project.pageNumber && !forceHide)
				return;
			
			DataLoadingManager.instance.showLoading(false, null, "");
			
			var frameVo : FrameVo;
			var pageVo:PageVo
						
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				pageVo = Infos.project.pageList[i];
				for (var j:int = 0; j < pageVo.layoutVo.frameList.length; j++) 
				{
					frameVo = pageVo.layoutVo.frameList[j];
					if(frameVo.isPageNumber)
					{
						pageVo.layoutVo.frameList.splice(j,1);
					}
				}			
			}
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			DataLoadingManager.instance.hideLoading();
			
			Debug.log("PagesManager > hidePageNumber > end");
		}
		
		
		public function applyTextFormatToAllText($textFormat:TextFormat, $vAlign:String, isPageNumber:Boolean = false):void
		{
			//temporary frame text			
			var tempFT:FrameText;
			var tempTextFormat:TextFormat;
			var fixedState:Boolean;
						
			// make list of all not empty frames
			var frameVo : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				for (var j:int = 0; j < Infos.project.pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = Infos.project.pageList[i].layoutVo.frameList[j];
					if(frameVo.type == FrameVo.TYPE_TEXT && !frameVo.isEmpty && frameVo.isPageNumber == isPageNumber)
					{
						tempFT = new FrameText(frameVo); // create temporzry frame text
						tempFT.init(); // init frame
						fixedState = frameVo.fixed; //save fixed state
						frameVo.fixed = true; //set fixed state to true
						
						// alignment
						frameVo.vAlign = $vAlign;
						
						if(isPageNumber)
						{
							tempFT.applyTextFormat($textFormat,-1,-1);
							frameVo.fixed = fixedState;
							tempFT.destroy();
							continue;
						}
						
						tempFT.setColor(Number($textFormat.color));
						tempFT.setFont($textFormat.font);
						tempFT.boldText(null,true, $textFormat.bold);
						tempFT.italicText(null,true, $textFormat.italic);
						tempFT.underlineText(null,true, $textFormat.underline);
						
						frameVo.fixed = fixedState;
						tempFT.destroy();
					}
				}
			}
			
			// delete ref
			tempFT = null;
			tempTextFormat = null;
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator(); 
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc, false);				

		}
		
		
		/**
		 * Apply border to all photo 
		 */
		public function applyCalendarColorToAllPages(colors:CalendarColors):void
		{
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				var pageVo:PageVo = Infos.project.pageList[i];
				if(pageVo.isCalendar)
				{
					if(!pageVo.customColors)
						pageVo.customColors = new CalendarColors();
					
					pageVo.customColors.month = colors.month;
					pageVo.customColors.year = colors.year;
					pageVo.customColors.border = colors.border;
					pageVo.customColors.weekDay = colors.weekDay;
					pageVo.customColors.weekEndDay = colors.weekEndDay;
					pageVo.customColors.offSetDay = colors.offSetDay;
					pageVo.customColors.dayBackground = colors.dayBackground;
					
					/*
					private var _month:Number = Colors.BLACK; // color of the month's textfield (january, february...)
					private var _year:Number = Colors.BLACK; // color of the year's textfield (2013,2014 etc..)
					private var _border:Number = Colors.BLACK; // color of the borders (when showBorder is set to true)
					private var _weekDay:Number = Colors.BLACK; // color of the week day numbers
					private var _weekEndDay:Number = Colors.RED_FLASH; // color of the weekEnd day numbers
					private var _offSetDay:Number = Colors.GREY; // color of the previous and next month day numbers
					private var _dayBackground:Number = -1; // color of days background (-1) = transparent 
					*/
				}
					
			}
			
			//undo redo
			UserActionManager.instance.addAction(UserActionManager.TYPE_COLOR_CHANGE);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");

			PopupAbstract.Alert(title, desc,false);
		}
		
		/**
		 * Apply border to all photo 
		 */
		public function applyBorderToAllPhoto(size:int, shadow:Boolean, borderColor : Number):void
		{
			// make list of all not empty frames
			var frameVo : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				for (var j:int = 0; j < Infos.project.pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = Infos.project.pageList[i].layoutVo.frameList[j];
					if(frameVo.type == FrameVo.TYPE_PHOTO && !frameVo.isEmpty && frameVo.editable && !frameVo.fixed ){
						{
							frameVo.border = size;
							frameVo.fillColor = borderColor;
							frameVo.shadow = shadow;
						}
					}
				}
			}
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc, false);	
		}
		
		
		/**
		 * Apply border to all photo 
		 */
		public function applyBackgroundToAllTexts( useBackground:Boolean, fillColor:Number = 0xffffff, fillAlpha : Number = 1):void
		{
			// make list of all not empty frames
			var frameVo : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				for (var j:int = 0; j < Infos.project.pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = Infos.project.pageList[i].layoutVo.frameList[j];
					if(frameVo.type == FrameVo.TYPE_TEXT && !frameVo.isEmpty){
						{
							frameVo.tb = useBackground;
							frameVo.fillColor = fillColor;
							frameVo.fillAlpha = fillAlpha;
						}
					}
				}
			}
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
		}
		
		
		
		/**
		 * Apply border to all photo 
		 */
		public function applyShadowToAll( useShadow:Boolean, color:Number = 0xffffff):void
		{
			// make list of all not empty frames
			var frameVo : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				for (var j:int = 0; j < Infos.project.pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = Infos.project.pageList[i].layoutVo.frameList[j];
					if( frameVo.type == FrameVo.TYPE_PHOTO && !frameVo.isEmpty && !frameVo.uniqFrameLinkageID ){
						{
							frameVo.shadow = useShadow;
							frameVo.sCol = color;
						}
					}
				}
			}
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc, false);	
		}
		
		
		
		/**
		 * Apply a mask type and corner radius to all photos
		 */
		public function applyMaskToAllPhotos( maskType : int, cornerRadius:int ):void
		{
			// make list of all not empty frames
			var frameVo : FrameVo;
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				for (var j:int = 0; j < Infos.project.pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = Infos.project.pageList[i].layoutVo.frameList[j];
					if(frameVo.type == FrameVo.TYPE_PHOTO && !frameVo.isEmpty){
						{
							frameVo.mask = maskType;
							frameVo.cr = cornerRadius;
						}
					}
				}
			}
			
			// update view
			PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			// update page navigator
			EditionArea.updatePageNavigator();
			
			// add user action for undo process
			UserActionManager.instance.addAction(UserActionManager.TYPE_AUTO_FILL);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc, false);	
		}
		
		
		
		/**
		 * add a photo centered on the current selected page !
		 */
		public function addClipartFrameToCurrentPage( clipartVo : ClipartVo):void
		{
			Debug.log("PagesManager.addClipartFrameToCurrentPage");
			
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			if(currentPage is PageCoverClassicVo ) return;
			var pageArea : PageArea = EditionArea.getCurrentPageAreaFromPageVo( currentPage );
			pageArea.createFrameFromPhotoVo( clipartVo , false );
			
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			
			// update used/not used items WHY ??
				//ProjectManager.instance.remapPhotoVoList();
			/*
			// update navigator
			PageArea.REDRAW_PAGE_IN_NAVIGATOR.dispatch( currentPage );
			*/
		}
		
		/**
		 * add a photo centered on the current selected page !
		 */
		public function addPhotoFrameToCurrentPage( photoVo : PhotoVo):void
		{
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			if(currentPage is PageCoverClassicVo ) return;
			var pageArea : PageArea = EditionArea.getCurrentPageAreaFromPageVo( currentPage );
			pageArea.createFrameFromPhotoVo( photoVo , false );
			
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			
			// update used/not used items
			ProjectManager.instance.remapPhotoVoList();
			/*
			// update navigator
			PageArea.REDRAW_PAGE_IN_NAVIGATOR.dispatch( currentPage );
			*/
		}
		
		
		/**
		 * add a photo centered on the current selected page !
		 */
		public function addPhotoFramePage( photoVo : PhotoVo, page:PageVo):void
		{
			var pageArea : PageArea = EditionArea.getCurrentPageAreaFromPageVo( page );
			pageArea.createFrameFromPhotoVo( photoVo , false );
			
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_CREATE);
			
			// update used/not used items
			ProjectManager.instance.remapPhotoVoList();
			/*
			// update navigator
			PageArea.REDRAW_PAGE_IN_NAVIGATOR.dispatch( currentPage );
			*/
		}

		
		/**
		 * update page background
		 */
		public function updateCurrentPageBackground( backgroundVo : BackgroundVo, fillColor:int = -1 ):void
		{
			var currentPage : PageVo = PagesManager.instance.currentEditedPage;
			
			if(!currentPage) return;
			
			//security postcards
			if(currentPage.isPostCardBack) return;
			
			updatePageBackground( currentPage, backgroundVo, fillColor);
			
		}
		
		/**
		 * update page background
		 */
		public function updatePageBackground( pageVo : PageVo, backgroundVo : BackgroundVo, fillColor:int = -1 ):void
		{
			var currentPage : PageVo = pageVo;
			
			if(!currentPage) return;
			if(currentPage is PageCoverClassicVo ) return;
			
			//Get current edited pageArea
			var pageArea : PageArea = EditionArea.getCurrentPageAreaFromPageVo( currentPage );
			
			//Update with backgroundVo
			if(backgroundVo)
			{
				pageArea.updateBackgroundWithBackgroundVo( backgroundVo );
				//UNDO REDO
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			}
				//Or update with a fill color
			else if(fillColor != -1)
			{
				pageArea.updateBackgroundWithColor(fillColor);
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			}	
			
			// update used/not used items (in case there was in image in the background)
			ProjectManager.instance.remapPhotoVoList();
			
			// redraw page
			PageNavigator.RedrawPage(pageVo);
			
			//dispatch Signal
			BACKGROUND_UPDATED.dispatch();
		}
		
		/**
		 * update page background
		 */
		public function updateCurrentPageBackgroundWithCustom( backgroundCustomVo : BackgroundCustomVo, pageVo:PageVo):void
		{
			var currentPage : PageVo = pageVo;
			//updatePageBackgroundWithCustom( currentPage, backgroundCustomVo);
			
			if(currentPage is PageCoverClassicVo ) return;
			
			if(currentPage.isPostCardBack) return;
			
			//Get current edited pageArea
			var pageArea : PageArea = EditionArea.getCurrentPageAreaFromPageVo( currentPage );
			
			//Update with backgroundVo
			if(backgroundCustomVo)
			{
				pageArea.updateBackgroundWithCustom( backgroundCustomVo );
				//UNDO REDO
				UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_UPDATE);
			}
			
			// update used/not used items (in case there was in image in the background)
			ProjectManager.instance.remapPhotoVoList();
			
			// redraw page in navigator
			PageNavigator.RedrawPage(currentPage);	
			
			//redraw page 
			PageVo.PAGEVO_UPDATED.dispatch(currentPage);
			
			//dispatch Signal
			BACKGROUND_UPDATED.dispatch();
		}
		
			
		/**
		 * Check if the swap photo button should be active or not in current pages
		 * > to be active it must have at least two frame photo not empty in current drawn pages
		 */
		public function checkSwapPhotoState():Boolean
		{
			if(EditionArea.numPhotoOnScreen > 0) return true; // > 1
			return false;
		}
			
		
		
		
		
		/**
		 * update all background
		 */
		public function updateAllPageBackground( backgroundVo : BackgroundVo, fillColor:int = -1 ):void
		{
			
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				var pageVo : PageVo = Infos.project.pageList[i];
				if(	!(pageVo is PageCoverClassicVo ) 
					&& !pageVo.isPostCardBack 
					&& (!backgroundVo || (backgroundVo.isCover == pageVo.isCover))) // && !pageVo.isCover)
				{
					if(pageVo.layoutVo.frameList.length>0)
					{
						var backgroundFrameVo:FrameVo = pageVo.layoutVo.frameList[0];
						
						//Update with backgroundVo
						if(backgroundVo)
						{
							backgroundFrameVo.fillColor = -1;
							//backgroundFrameVo.photoVo = backgroundVo;
							FrameVo.injectPhotoVoInFrameVo(backgroundFrameVo, backgroundVo);
							
						}
						//Or update with a fill color
						else if(fillColor != -1)
						{
							backgroundFrameVo.photoVo = null;
							backgroundFrameVo.fillColor = fillColor;
						}
					}
					else
						Debug.warn("FramesManager > updateAllPageBackground > The pageVo do not have FrameVo in it FrameList.");
				}				
			}
			
			// update used/not used items (in case there was in image in the background)
			ProjectManager.instance.remapPhotoVoList();
			
			//Redraw everything
			ProjectManager.PROJECT_UPDATED.dispatch();
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_ALL_BACKGROUND_CHANGED);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc,false);
		}
		/*
		 * *
		 * update all background with custom one
		 */
		public function updateAllPageBackgroundWithCustom( backgroundCustomVo : BackgroundCustomVo):void
		{
			
			for (var i:int = 0; i < Infos.project.pageList.length; i++) 
			{
				var pageVo : PageVo = Infos.project.pageList[i];
				if(!(pageVo is PageCoverClassicVo ) && !pageVo.isPostCardBack && (backgroundCustomVo.isCover == pageVo.isCover)) // && !pageVo.isCover)
				{
					if(pageVo.layoutVo.frameList.length>0)
					{
						var backgroundFrameVo:FrameVo = pageVo.layoutVo.frameList[0];
						
						if(backgroundFrameVo)
						{
							backgroundFrameVo.fill(JSON.parse(backgroundCustomVo.frameVoStr));						
						}
						
					}
					else
						Debug.warn("FramesManager > updateAllPageBackground > The pageVo do not have FrameVo in its FrameList.");
				}				
			}
			//Redraw everything
			ProjectManager.PROJECT_UPDATED.dispatch();
			//UNDO REDO
			UserActionManager.instance.addAction(UserActionManager.TYPE_ALL_BACKGROUND_CHANGED);
			
			//Show popup confirmation
			var title:String = ResourcesManager.getString("popup.apply.to.all.confirmation.title");
			var desc:String = ResourcesManager.getString("popup.apply.to.all.confirmation.desc");
			PopupAbstract.Alert(title, desc,false);
		}
		
		
		/**
		 *  HACK : TODO : ask keith for best solution for this
		 *  > check if loaded content ratio is same as photoVo ratio
		 *  > do this only for background items for now
		 *  > if ratio differ, update background vo width and height
		 *  > return true if modification was made, return false if ratio is good
		 */
		public function verifyBackgroundVoRatio( loadedBitmapData :BitmapData, backgroundVo: BackgroundVo ):Boolean
		{
			var explicitRatio : Number = Math.round(backgroundVo.width / backgroundVo.height*100)/100;
			var loadedRatio : Number = Math.round(loadedBitmapData.width / loadedBitmapData.height*100)/100;
			if(explicitRatio != loadedRatio){
				// TODO reset to warn?
				Debug.log("PagesManager > BackgroundVo ratio ("+explicitRatio+") differ from loaded content ("+loadedRatio+"). Update of background vo will be done"); 
				backgroundVo.height = backgroundVo.width / loadedRatio;
				return true;
			}
			return false;
		}
		
		
		
		/**
		 *  retrieve page vo from frame vo
		 */
		public function getPageVoByFrameVo( frameVo:FrameVo ):PageVo
		{
			var project : ProjectVo = Infos.project;
			var pageVo : PageVo;
			var f : FrameVo;
			for (var i:int = 0; i < project.pageList.length; i++) 
			{
				pageVo = project.pageList[i];
				for (var j:int = 0; j < pageVo.layoutVo.frameList.length; j++) 
				{
					f = pageVo.layoutVo.frameList[j];
					if(frameVo == f) return pageVo;
				}
			}
			return null;
		}
		
		/**
		 *  retrieve page area from frame vo
		 */
		public function getPageAreaByFrameVo( frameVo:FrameVo ):PageArea
		{
			return EditionArea.getPageAreaForFrameVo( frameVo );
		}
		
		
		

		/**
		 * Switch a frame from a page to another
		 * This is done after a drag and drop on another page
		 */
		public function switchFrameToPage( frame : Frame, newPage : PageArea, newPos:Point ):void
		{
			Debug.log("PagesManager.switchFrameToPage : "+ newPage + " > " + frame);
			
			// copy frame and linked frames
			var copiedFrames:Vector.<FrameVo> = new Vector.<FrameVo>;
			copiedFrames.push ( frame.frameVo.copy() ) ; // copy frame
			
			// check if there are some linked frames
			if( frame.frameVo.uniqFrameLinkageID )
			{
				var linkedFrames : Vector.<Frame> = getFramesLinked( frame );
				
				// first we copy all framevo
				for (var i:int = 0; i < linkedFrames.length; i++) 
				{
					copiedFrames.push( linkedFrames[i].frameVo.copy() );
				}
				
				// Then we delete linked frames !
				for (i = 0; i < linkedFrames.length; i++) 
				{
					linkedFrames[i].deleteMe(false); // TODO : beware this could remove linkage id, but as we work now with only two frames linked max together, this should not have any impact
				}
			}
			
			// delete from inital page
			frame.deleteMe(false);
			
			// add it to new page
			if( newPage )
			{
				copiedFrames.reverse(); // frame copied front to back, should be pasted back to front
				if( copiedFrames.length > 1 ) { // if multiple frames, generate new uniqID to link them together
					linkFramesVoTogether(copiedFrames);
				}
				var offsetX : Number = newPos.x - copiedFrames[0].x;
				var offsetY : Number = newPos.y - copiedFrames[0].y;  
				for (i = 0; i < copiedFrames.length; i++) 
				{
					var currentFrame:FrameVo = copiedFrames[i];
					currentFrame.x += offsetX
					currentFrame.y += offsetY;
					// add framevo to new page
					newPage.pageVo.layoutVo.frameList.push(currentFrame);
				}
			}
			
			// notify update on page
			PageVo.PAGEVO_UPDATED.dispatch(newPage.pageVo);
			
			// save action in history (which updates toolbar also)
			UserActionManager.instance.addAction(UserActionManager.TYPE_FRAME_PASTE)//, currentFrame); // add a create frame action here
		}
		
		
		
		
		
		/**
		 *  creates default background frame vo
		 */
		/*public function createBackgroundFrameVo(isCover:Boolean, isPostCardBack:Boolean = false):FrameVo
		{
			var frameVo:FrameVo = new FrameVo();
			frameVo.type = FrameVo.TYPE_BKG;
			
			var backgroundBounds:Rectangle = (!isCover)? getPageBackgroundBounds() : getCoverBackgroundBounds();
			frameVo.width = backgroundBounds.width;
			frameVo.height = backgroundBounds.height;
			frameVo.x = backgroundBounds.x;
			frameVo.y = backgroundBounds.y;
			frameVo.editable = true;
			
			return frameVo;
		}*/

		/**
		 * returns uniq linkage id
		 */
		public function getUniqFrameLinkageID():String
		{
			return "" + new Date().time; // + "_" + Math.random()*1000;
		}
		
		
		
		/**
		 * links multiple frames together
		 * -> returns name of new linkage id
		 */
		public function linkFramesVoTogether( framesVoToLink : Vector.<FrameVo>, linkageId : String = null ):String
		{
			// if no linkage specified, create a new one
			if(!linkageId) linkageId = getUniqFrameLinkageID();
			
			// There must be at lease two frames to link together
			if(framesVoToLink.length < 2) {
				Debug.warn("FramesManager.linkFramesTogether > we need at least two frames");
			}
			
			var frameVo : FrameVo;
			for (var i:int = 0; i < framesVoToLink.length; i++) 
			{
				frameVo = framesVoToLink[i] as FrameVo;
				if(!frameVo){
					Debug.warn( "FramesManager.linkFramesTogether > no frameVo found to link" );
					return null;
				}
				
				// override warning
				if(frameVo.uniqFrameLinkageID && frameVo.uniqFrameLinkageID != linkageId) Debug.warn("FramesManager.linkFramesTogether > frame has already a linkage : '"+ frameVo.uniqFrameLinkageID + "' but we will apply a new one");
				
				// set new linkage
				frameVo.uniqFrameLinkageID = linkageId;
			}
			
			return linkageId;
		}
		
		
		/**
		 * remove linkage 
		 * -> returns the amount of frames where linkage was found
		 */
		public function removeFramesLinkage( linkageId : String, removeDateLinkage:Boolean = true ):int
		{
			var linkageFound : int = 0;
			
			var pageList : Vector.<PageVo> = Infos.project.pageList;
			var frameVo : FrameVo;
			for (var i:int = 0; i <pageList.length; i++) 
			{
				for (var j:int = 0; j <pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = pageList[i].layoutVo.frameList[j];
					if( frameVo.uniqFrameLinkageID == linkageId ){
						linkageFound ++;
						frameVo.uniqFrameLinkageID = null;
						if(removeDateLinkage)
						frameVo.dateLinkageID = null;
					}
				}
			}
			return linkageFound;
		}
		
		
		/**
		 * retrieve linked frames with current frame
		 * returns a frame , not a frame vo !
		 */
		public function getFramesLinked ( refFrame:Frame ): Vector.<Frame>
		{
			if(! refFrame.frameVo.uniqFrameLinkageID ){
				Debug.warn( "FramesManager.linkFramesTogether > frame has no linkage");
				return null;
			}
			
			var pageList : Vector.<PageVo> = Infos.project.pageList;
			var frameVo : FrameVo;
			var frame : Frame;
			var frames : Vector.<Frame> = new Vector.<Frame>;
			for (var i:int = 0; i <pageList.length; i++) 
			{
				for (var j:int = 0; j <pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = pageList[i].layoutVo.frameList[j];
					if( frameVo.uniqFrameLinkageID == refFrame.frameVo.uniqFrameLinkageID ){
						frame = EditionArea.getVisibleFrameByFrameVo(frameVo);
						if(frame && frame != refFrame) frames.push( frame );
					}
				}
			}
			
			if(frames.length > 0) return frames;
			return null;
		}
		
		
		/**
		 * retrieve a visible (on screen) frame by it's linkage
		 */
		public function findFramesByLinkage ( linkageId : String ): Vector.<Frame>
		{
			var pageList : Vector.<PageVo> = Infos.project.pageList;
			var frameVo : FrameVo;
			var frame : Frame;
			var frames : Vector.<Frame> = new Vector.<Frame>;
			for (var i:int = 0; i <pageList.length; i++) 
			{
				for (var j:int = 0; j <pageList[i].layoutVo.frameList.length; j++) 
				{
					frameVo = pageList[i].layoutVo.frameList[j];
					if( frameVo.uniqFrameLinkageID == linkageId){
						frame = EditionArea.getVisibleFrameByFrameVo(frameVo);
						if(frame) frames.push( frame );
					}
				}
			}
			
			if(frames.length > 0) return frames;
			return null;
		}
		
		
		/**
		 * retrieve a frameVo by it's linkage
		 */
		public function findFramesVoByLinkage ( linkageId : String, frameVoRef : FrameVo , pageVo:PageVo) : Vector.<FrameVo>
		{
			var pageList : Vector.<PageVo> = Infos.project.pageList;
			var frames : Vector.<FrameVo> = new Vector.<FrameVo>;
			//for (var i:int = 0; i <pageList.length; i++) 
			//{
				for (var j:int = 0; j <pageVo.layoutVo.frameList.length; j++) 
				{
					if( pageVo.layoutVo.frameList[j].uniqFrameLinkageID == linkageId && pageVo.layoutVo.frameList[j] != frameVoRef){
						frames.push( pageVo.layoutVo.frameList[j] );
					}
				}
			//}
			
			if(frames.length > 0) return frames;
			return null;
		}
		
		/**
		 * Update all linked frame of the project
		 * Happens when starting day, starting month, or year is changed
		 * Make the linked frame follow the date
		 * Calendars only
		 */
		public function updateAllLinkedFrames ( onComplete:Function ) : void
		{
			Debug.log("PagesManager.updateAllLinkedFrames");
			
			var pageList : Vector.<PageVo> = Infos.project.pageList;
			var linkedFrameToUpdate:Vector.<FrameVo> = new Vector.<FrameVo>();
			
			//first get all frameVo to update
			//Remove them from their pageVo
			//Gather them in a vector
			for (var i:int = 0; i <pageList.length; i++) 
			{
				var page:PageVo = pageList[i];
				getAllLinkedFrameVoToUpdate(page,linkedFrameToUpdate);
				
				for (var j:int = 0; j < linkedFrameToUpdate.length; j++) 
				{
					var frameVo:FrameVo = linkedFrameToUpdate[j];
					removeFramesLinkage(frameVo.uniqFrameLinkageID,false);
					removeFrameVoFromPage(page,frameVo);
				}
				
			}
			
			//Then find the correct frame calendar
			//Could be in another pageVo
			for (var i:int = 0; i <pageList.length; i++) 
			{
				var page:PageVo = pageList[i];
				if(linkedFrameToUpdate.length>0)
				reInjectLinkedFrameVoAtCorrectDate(page,linkedFrameToUpdate);
			}
			
			Debug.log("PagesManager.updateAllLinkedFrames: All frames have been re-injected");
			if(PagesManager.instance.currentEditedPage)
				PageVo.PAGEVO_UPDATED.dispatch(PagesManager.instance.currentEditedPage);
			
			EditionArea.updatePageNavigator();
			PageNavigator.RedrawLabels();
			TweenMax.delayedCall(1,onComplete);
		}
		
		/**
		 * Get Linked frame to update in the given pageVo
		 * Calendars frames only
		 */
		public function getAllLinkedFrameVoToUpdate ( pageVo:PageVo, linkedFrameToUpdate:Vector.<FrameVo> ) : void
		{
			Debug.log("PagesManager.getAllLinkedFrameVoToUpdate");
			if(pageVo && pageVo.isCalendar)
			{
				for (var i:int = 0; i < pageVo.layoutVo.frameList.length; i++) 
				{
					var frameVo:FrameVo = pageVo.layoutVo.frameList[i];
					if(frameVo.uniqFrameLinkageID != null && frameVo.type != FrameVo.TYPE_CALENDAR)
					{
						var linkedFrames:Vector.<FrameVo> = findFramesVoByLinkage(frameVo.uniqFrameLinkageID,frameVo, pageVo);
						if(linkedFrames != null)
						{
							for (var j:int = 0; j < linkedFrames.length; j++) 
							{
								var linkedFrameVo:FrameVo = linkedFrames[j];
								if(linkedFrameVo.type == FrameVo.TYPE_CALENDAR && !matchDateLinkage(frameVo,linkedFrameVo, pageVo.monthIndex))
								{
									linkedFrameToUpdate.push(frameVo);
									//removeFramesLinkage(frameVo.uniqFrameLinkageID,false);
									//removeFrameVoFromPage(pageVo,frameVo);
								}
							}
						}
						
					}
				}
			}
			else
			{
				Debug.log("PagesManager.updateLinkedFrames : PageVo is null");
			}
		}
		
		/**
		 * Replace Linked frame to update in the given pageVo
		 * Calendars frames only
		 */
		public function reInjectLinkedFrameVoAtCorrectDate ( pageVo:PageVo, linkedFrameToUpdate:Vector.<FrameVo> ) : void
		{
			Debug.log("PagesManager.reInjectLinkedFrameVoAtCorrectDate");
			if(pageVo && pageVo.isCalendar)
			{
				for (var y:int = 0; y < pageVo.layoutVo.frameList.length; y++) 
				{
					var frameVo:FrameVo = pageVo.layoutVo.frameList[y];
					if(frameVo.type == FrameVo.TYPE_CALENDAR && frameVo.dateAction == CalendarManager.DATEACTION_DAYDATE)
					{
						var frameToLink:Vector.<FrameVo> = new Vector.<FrameVo>();
						for (var i:int = 0; i < linkedFrameToUpdate.length; i++)
						{
							var frameToReInject:FrameVo = linkedFrameToUpdate[i];
							if(matchDateLinkage(frameToReInject,frameVo, pageVo.monthIndex))
							{
								//linkFramesVoTogether([frameToReInject,frameVo]);
								updateLinkedFramesParams(frameToReInject,frameVo);
								frameToLink.push(frameToReInject);
								linkedFrameToUpdate.splice(i,1);
								i--;
							}
						}
						if(frameToLink.length >0)
						{
							addLinkedFrameVoToPage(pageVo,y,frameToLink);//inject framevo In pageVO
							frameToLink.push(frameVo);//Add the calendar frame
							linkFramesVoTogether(frameToLink);//then linke them together
						}
						
					}
				}
			}
			else
			{
				Debug.log("PagesManager.updateLinkedFrames : PageVo is null");
			}
		}

		/**
		 * Match linked frame on the DateLinkage
		 * Make sure user has not changed the starting month or day
		 * 
		 * Calendars only
		 */
		public function matchDateLinkage( frameVo:FrameVo, calendarFrameVo:FrameVo, monthIndex:int ) : Boolean
		{
			var dli:DateLinkageVo = frameVo.dateLinkageID;
			//var currentStartingDay:int = ProjectManager.instance.project.calendarStartDayIndex;
			var currentDayDate:String = CalendarManager.instance.getFrameText(calendarFrameVo);
			var currentDayKind:String = CalendarManager.instance.whatKindofDayDateAmI(calendarFrameVo,monthIndex,currentDayDate);
			var currentMonthIndexWithOffset:int = CalendarManager.instance.getMonthIndexWithOffSet(monthIndex);
			
			if(dli.montIndexWithOffset == currentMonthIndexWithOffset)
			{
				if(dli.dayDate == int(currentDayDate) && currentDayKind != CalendarManager.DAYDATE_KIND_OFFSET)
				{
					//if(dli.dayKind == currentDayKind)
						return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Update linked frame position and size
		 * 
		 * Calendars only
		 */
		public function updateLinkedFramesParams( frameVo:FrameVo, calendarFrameVo:FrameVo ) : void
		{
			if(frameVo.type == FrameVo.TYPE_PHOTO )
			{
				frameVo.x = calendarFrameVo.x;
				frameVo.y = calendarFrameVo.y;
				frameVo.width = calendarFrameVo.width;
				frameVo.height = calendarFrameVo.height;
			}
			else if(frameVo.type == FrameVo.TYPE_TEXT )
			{
				frameVo.x = calendarFrameVo.x;
				frameVo.y = calendarFrameVo.y;
				frameVo.width = calendarFrameVo.width;
				frameVo.height = calendarFrameVo.height;
			}
			
		}
		
		/**
		 * Add frameVo to page
		 */
		public function addFrameVoToPage(pageVo:PageVo, index:int, frameVo:FrameVo):void
		{
			pageVo.layoutVo.frameList.splice(index,0,frameVo);
		}
		
		/**
		 * Add linked frameVos to page
		 */
		public function addLinkedFrameVoToPage(pageVo:PageVo, index:int, frameVoList:Vector.<FrameVo>):void
		{
			//sort array on type (pictures come before text) alphabetically logic :) RISKY... ?
			var frameVoArray:Array = VectorUtils.toArray(frameVoList);
			frameVoArray.sortOn("type",Array.DESCENDING);
			for (var i:int = 0; i < frameVoArray.length; i++) 
			{
				var frameVo:FrameVo = frameVoArray[i];
				pageVo.layoutVo.frameList.splice(index,0,frameVo);
			}
			
			
		}
		
		/**
		 * Remove frameVo from page
		 */
		public function removeFrameVoFromPage(pageVo:PageVo, frameVoToRemove:FrameVo):void
		{
			for (var i:int = 0; i < pageVo.layoutVo.frameList.length; i++) 
			{
				var frame:FrameVo = pageVo.layoutVo.frameList[i];
				if(frame == frameVoToRemove)
				{
					pageVo.layoutVo.frameList.splice(i,1);
					return;
				}
			}
			
			//pageVo.layoutVo.frameList.splice(index,0,frameVo);
		}
		
		
		
	}
}
class SingletonEnforcer{}