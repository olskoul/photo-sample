/***************************************************************
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2014
 ****************************************************************/
package manager
{
	import com.bit101.components.HBox;
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import be.antho.bitmap.snapshot.SnapShot;
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.data.XmlManager;
	import be.antho.utils.tabs.TabManager;
	
	import comp.DefaultTooltip;
	import comp.button.SimpleButton;
	
	import data.Infos;
	import data.OverlayerVo;
	import data.TutorialStepVo;
	
	import library.tutorial.step.view;
	
	import utils.ButtonUtils;
	import utils.Debug;
	
	import view.menu.lefttabs.LeftTabs;
	
	
	/**
	 * Tutorial manager
	 */
	public class TutorialManager
	{
		//youtube links
		public static var youtubeURL:Object;
		
		//Arrows
		public static const ARROW_UP:String = "up";
		public static const ARROW_DOWN:String = "down";
		public static const ARROW_LEFT:String = "left";
		public static const ARROW_RIGHT:String = "right";
		
		
		//KEYS
		public static const KEY_TOP_BAR:String = "KEY_TOP_BAR";
		public static const KEY_EDITION_AREA:String = "KEY_EDITION_AREA";
		public static const KEY_EDITION_TOOLBAR:String = "KEY_EDITION_TOOLBAR";
		public static const KEY_FULLSCREEN_BTN:String = "KEY_FULLSCREEN_BTN";
		public static const KEY_PAGE_TOOLBAR_BTN:String = "KEY_PAGE_TOOLBAR_BTN";
		public static const KEY_TUTORIAL_BTN:String = "KEY_TUTORIAL_BTN";
		public static const KEY_LEFT_MENU:String = "KEY_LEFT_MENU";
		public static const KEY_BOTTOM_PANEL:String = "KEY_BOTTOM_PANEL";
		public static const KEY_CHECKOUT_BTN:String = "KEY_CHECKOUT_BTN";
		public static const KEY_SETTINGS_BTN:String = "KEY_SETTINGS_BTN";
		public static const KEY_PHOTOMANAGER_BTN:String = "KEY_PHOTOMANAGER_BTN";
		public static const KEY_LOGO:String = "KEY_LOGO";
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		

		private var currentStepIndex:int = 0;
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function TutorialManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("TutorialManager is a singleton, use instance"); 
		}
		
		/**
		 * instance
		 */
		private static var _instance:TutorialManager;
		private var tooltip:Sprite;
		private var steps:Object;
		private var stepsOrder:Array;
		private var btnFinish:SimpleButton;
		private var btnNext:SimpleButton;
		private var btnSkip:SimpleButton;
		private var btnPrev:Sprite;
		private var stageRef:Stage;
		private var darkness:Sprite;
		private var btnWrapper:HBox;
		private var fakeHero:Bitmap;
		private var fakeHeroWrapper:Sprite;
		private var fakeStage:Bitmap;
		private var tooltipArrow:MovieClip;
		private var tooltipBg:Sprite;
		private var tooltipContent:MovieClip;
		private var fakeHeroPos:Point;

		private var pointToAim:Point;
		private var tabOpened:int;
		
		
		public var isRunning:Boolean = false;
		
		
		public static function get instance():TutorialManager
		{
			if (!_instance) _instance = new TutorialManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * init manager
		 * 
		 */
		public function init($stageRef:Stage):void
		{
			//URLS youtube videos
			youtubeURL = {};
			youtubeURL["FR"] = "https://www.youtube.com/playlist?list=PLqeFOBkCDc1ihMCX214bDsGntp6FXUAjt";
			youtubeURL["NL"] = "https://www.youtube.com/playlist?list=PLqeFOBkCDc1hbSDOCypXlD-Byu_bwBjCC";
			
			//stageRef
			stageRef = $stageRef;
			
			//Setup Steps Order
			stepsOrder = [];
			stepsOrder.push(KEY_LEFT_MENU);
			
			//edition AREA
			stepsOrder.push(KEY_EDITION_AREA);
			stepsOrder.push(KEY_EDITION_TOOLBAR);
			
			//Bottom nav and preview
			
			stepsOrder.push(KEY_BOTTOM_PANEL);
			//stepsOrder.push(KEY_PAGE_TOOLBAR_BTN); //Nico does not want it
			stepsOrder.push(KEY_FULLSCREEN_BTN);

			//Top bar and btns
			stepsOrder.push(KEY_TOP_BAR);
			stepsOrder.push(KEY_SETTINGS_BTN);
			stepsOrder.push(KEY_CHECKOUT_BTN);
			stepsOrder.push(KEY_PHOTOMANAGER_BTN);
			
						
			//always at the end
			stepsOrder.push(KEY_TUTORIAL_BTN);
			
			//Setup view
			tooltip = new library.tutorial.step.view();
			tooltipContent = tooltip["content"];
			tooltipContent["desc"].autoSize = "left";
			tooltipContent["title"].autoSize = "left";
			tooltipContent["back"]["txt"].autoSize = "left";
			tooltipContent["back"]["txt"].text = ResourcesManager.getString("common.prev");
			tooltipArrow = tooltip["arrow"];
			tooltipBg = tooltipContent["bg"];
			
			
			

		}
		
		/**
		 * destroy manager
		 * 
		 */
		public function destroy():void
		{
			dispose();
			
			
			stageRef = null;
			_instance = null;
		}
		
		/**
		 * Add a step tothe tutorial flow
		 * */
		public function addStep(vo:TutorialStepVo):void
		{
			if(!steps)
				steps = {};
				
			steps[vo.key] = vo;
		}
		
		/**
		 * Start Tutorial
		 * */
		public function start():void
		{
			verifySteps();
			dispose();
			tabOpened = (TabManager.instance.isOpen)?TabManager.instance.selectedIndex:-1;
			currentStepIndex = 0;
			isRunning = true;
			buildStep();
			
			//mark as seen
			SharedObjectManager.instance.write(SharedObjectManager.KEY_TUTORIAL,"seen");
		}
		
		/**
		 * Check if tutorial has been shown
		 * 
		 */
		public function hasNeverSeenTutorial():Boolean
		{
			var cookieValue:String = SharedObjectManager.instance.read(SharedObjectManager.KEY_TUTORIAL);
			return (cookieValue == null);
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Verify steps 
		 * Check if vo exists and target is not null
		 * if true, remove it from the steps
		 * */
		private function verifySteps():void
		{
			var runAgain:Boolean = false;
			
			for (var i:int = 0; i < stepsOrder.length; i++) 
			{
				var key:String = stepsOrder[i];
				if(steps[key] != null)
				{
					var vo:TutorialStepVo = steps[key]
					if( vo.target == null && vo.target.parent != null)
					{
						stepsOrder.splice(i,1);
						runAgain = true;
						continue;
					}
					
					//CANVAS Exceptions
					if(		Infos.isCanvas && key == KEY_BOTTOM_PANEL 
						|| 	Infos.isCanvas && key == KEY_FULLSCREEN_BTN)
					{
						stepsOrder.splice(i,1);
						runAgain = true;
						continue;
					}
					
					if(Infos.IS_DESKTOP && vo.editorType != TutorialStepVo.EDITOR_OFFLINE && vo.editorType != TutorialStepVo.EDITOR_BOTH)
					{
						stepsOrder.splice(i,1);
						runAgain = true;
						continue;
					}
					
					if(!Infos.IS_DESKTOP && vo.editorType != TutorialStepVo.EDITOR_ONLINE && vo.editorType != TutorialStepVo.EDITOR_BOTH)
					{
						stepsOrder.splice(i,1);
						runAgain = true;
						continue;
					}
					
				}
				else
				{
					stepsOrder.splice(i,1);
					runAgain = true;
					continue;
				}
			}
			
			if(runAgain)
				verifySteps();
			
		}
		
		/**
		 * Show currentStepIndex 
		 * */
		private function nextStep():void
		{
			hideStep(buildStep);
		}
		
		/**
		 * show current step
		 * */
		private function showStep(vo:TutorialStepVo):void
		{
			
			stageRef.addChild(darkness);
			stageRef.addChild(fakeHeroWrapper);
			fakeHeroWrapper.x = fakeHeroPos.x;
			fakeHeroWrapper.y = fakeHeroPos.y;
			
			if(tooltip.parent == null)
				stageRef.addChild(tooltip);
			
			//tooltip.x = tooltip.y = 200;
			
			tooltip.alpha = 0;
			tooltip.visible = false;
			fakeHeroWrapper.alpha = 0;
			fakeHeroWrapper.visible = false;
			
			var timeline:TimelineMax = new TimelineMax();
			timeline.stop();
			
			timeline.append( TweenMax.to(darkness, 1, {colorTransform:{brightness:.2}}) );
			timeline.append( TweenMax.to(fakeHeroWrapper, 1, {autoAlpha:1, glowFilter:{color:0xFFFFFF, blurX:20, blurY:20, strength:1, alpha:1}}),-1 );
			
			switch(vo.arrow)
			{
				case ARROW_UP:
					tooltip.x = pointToAim.x;
					tooltip.y = pointToAim.y + 20;
					checkIfInBounds(vo.arrow);
					timeline.append( TweenMax.to(tooltip, 1, {autoAlpha:1, y:pointToAim.y, ease:Strong.easeOut}),-0.5 );
					break;
				case ARROW_DOWN:
					tooltip.x = pointToAim.x;
					tooltip.y = pointToAim.y - 20;
					checkIfInBounds(vo.arrow);
					timeline.append( TweenMax.to(tooltip, 1, {autoAlpha:1, y:pointToAim.y, ease:Strong.easeOut}),-.5 );
					break;
				case ARROW_LEFT:
					tooltip.x = pointToAim.x + 20;
					tooltip.y = pointToAim.y;
					checkIfInBounds(vo.arrow);
					timeline.append( TweenMax.to(tooltip, 1, {autoAlpha:1, x:pointToAim.x, ease:Strong.easeOut}),-.5 );
					break;
				case ARROW_RIGHT:
					tooltip.x = pointToAim.x - 20;
					tooltip.y = pointToAim.y;
					checkIfInBounds(vo.arrow);
					timeline.append( TweenMax.to(tooltip, 1, {autoAlpha:1, x:pointToAim.x, ease:Strong.easeOut}),-.5 );
					break;
			}
			
			timeline.play();
		}
		
		/**
		 * Check if into bounds
		 * */
		private function checkIfInBounds(arrow:String):void
		{
			var offset:Number;
			
			switch(arrow)
			{
				case ARROW_UP:
				case ARROW_DOWN:
					//Right hand side of screen
					offset = (tooltip.x + tooltipContent.x + tooltipContent.width) - stageRef.stageWidth;
					if(offset > 0)
						tooltipContent.x -= offset+10;
					
					//Left hand side of screen
					offset = (tooltip.x + tooltipContent.x);
					if(offset < 0)
						tooltipContent.x -= offset-10;
					break;
				case ARROW_LEFT:
				case ARROW_RIGHT:
					//top of screen
					offset = (tooltip.y + tooltipContent.y);
					if(offset < 0)
						tooltipContent.y -= offset-10;
					
					//bottom of screen
					offset = (tooltip.y + tooltipContent.y + tooltipContent.height) - stageRef.stageHeight;
					if(offset > 0)
						tooltipContent.y -= offset+10;
					break;
			}
		}
		
		/**
		 * hide current step
		 * */
		private function hideStep($onComplete:Function = null, isFinish:Boolean = false):void
		{
			var timeline:TimelineMax = new TimelineMax();
			timeline.stop();
			
			if(isFinish)
			timeline.append( TweenMax.to(darkness, .5, {autoAlpha:0}) );
			if(fakeHeroWrapper)
			timeline.append( TweenMax.to(fakeHeroWrapper, .3, {autoAlpha:0}) , -.3);
			if(tooltip)
			timeline.append( TweenMax.to(tooltip, .3,{autoAlpha:0, onComplete:$onComplete}),-.3 );
			
			timeline.play();
			
		}
		
		/**
		 * Build current step
		 * */
		private function buildStep():void
		{
			dispose(true);
			
			if(currentStepIndex > stepsOrder.length-1)
				currentStepIndex = 0;
			if(currentStepIndex < 0)
				currentStepIndex = stepsOrder.length-1;
			
			
			var vo:TutorialStepVo = steps[stepsOrder[currentStepIndex]];
			
			if(!vo || !vo.target || !vo.target.parent)
			{
				nextBtnHandler();return;
			}
			
			//Setup Btn
			btnPrev = ButtonUtils.makeButton(tooltipContent["back"],prevBtnHandler);
			btnNext = SimpleButton.createBlueButton("common.next", nextBtnHandler);
			btnSkip = SimpleButton.createGreyButton("common.skip", skipBtnHandler);
			btnFinish = SimpleButton.createBlueButton("common.close", finishBtnHandler);
			
			btnWrapper = new HBox(tooltipContent,0,0);		
			
			//Close tabs for step KEY_LEFT_MENU
			TabManager.instance.open(!(vo.key == KEY_LEFT_MENU));
			
			//txt
			tooltipContent["index"].autoSize = "left";
			tooltipContent["index"].text = ""+  int(currentStepIndex+1) + "/" + stepsOrder.length;
			tooltipContent["title"].htmlText = ResourcesManager.getString(vo.titleKey);
			tooltipContent["desc"].htmlText = ResourcesManager.getString(vo.descriptionKey);
			
			//link to tutorial
			tooltipContent["youtubeLink"].addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				navigateToURL(new URLRequest(youtubeURL[Infos.lang.toUpperCase()]),"_blank");
			});
			
			tooltipContent["youtubeLink"].addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
			{
				TweenMax.killTweensOf(tooltipContent["youtubeLink"]);
				TweenMax.to(tooltipContent["youtubeLink"],.3,{glowFilter:{color:0xFFFFFF, blurX:20, blurY:20, strength:1, alpha:1}});
			});
				
			tooltipContent["youtubeLink"].addEventListener(MouseEvent.MOUSE_OUT, function(e:MouseEvent):void
			{
				TweenMax.killTweensOf(tooltipContent["youtubeLink"]);
				TweenMax.to(tooltipContent["youtubeLink"],.3,{glowFilter:{color:0xFFFFFF, blurX:20, blurY:20, strength:1, alpha:0}});
			});
			
			tooltipContent["youtubeLink"].buttonMode = true;
			tooltipContent["youtubeLink"].mouseChildren=false;
			tooltipContent["youtubeLink"]["tutoLink"].autoSize = "left";
			
			tooltipContent["youtubeLink"].visible = (Infos.currentLangVo.videoTutorialUrl != "");
				
			tooltipContent["youtubeLink"]["tutoLink"].htmlText = ResourcesManager.getString("tutorial.step.link");
			
			btnWrapper.removeChildren();
			//btn
			btnPrev.visible = (currentStepIndex > 0);
			
			if(currentStepIndex == stepsOrder.length-1) btnWrapper.addChild(btnFinish);
			if(!(currentStepIndex == stepsOrder.length-1)) btnWrapper.addChild(btnSkip);
			if(currentStepIndex < stepsOrder.length-1)btnWrapper.addChild(btnNext);

			
			//positions
			tooltipContent["title"].x = tooltipContent["index"].x + tooltipContent["index"].width + 2;
			tooltipContent["desc"].y = tooltipContent["title"].y + tooltipContent["title"].height + 10;
			if(tooltipContent["youtubeLink"].visible == false)
			{
				btnWrapper.y = tooltipContent["desc"].y + tooltipContent["desc"].height + 10;
			}
			else
			{
				tooltipContent["youtubeLink"].y = tooltipContent["desc"].y + tooltipContent["desc"].height + 10;
				btnWrapper.y = tooltipContent["youtubeLink"].y + tooltipContent["youtubeLink"].height + 30;
			}
			
			btnWrapper.x = tooltipBg.width - btnWrapper.width - 20;
			btnPrev.y = btnWrapper.y + 8;
			tooltipBg.height = btnWrapper.y + btnWrapper.height + 20;
			
			//Create darkness (Sounds good huh!? :) )
			if(!darkness)
				darkness = new Sprite();
			
			fakeStage = new Bitmap(SnapShot.snap(stageRef,1,false));
			darkness.addChild(fakeStage);
			
			//Create fake hero clip
			if(!fakeHeroWrapper)
				fakeHeroWrapper = new Sprite();
			fakeHero = new Bitmap(SnapShot.take(vo.target,1));
			fakeHeroWrapper.addChild(fakeHero);
			
			//Position
			fakeHeroPos = new Point(vo.target.x, vo.target.y);
			fakeHeroPos = vo.target.parent.localToGlobal(fakeHeroPos);
			fakeHeroPos = stageRef.globalToLocal(fakeHeroPos);
			
			//calculate TooltipPosition
			calculateToolTipPosition(vo);
			
			//Change arrow position if needed
			positionArrow(vo);

			//anim in
			showStep(vo);
		}
		
		/**
		 * Calcultate tooltip position
		 * Also arrow position and direction
		 * */
		private function calculateToolTipPosition(vo:TutorialStepVo):void
		{
			//change tooltip arrow first
			tooltipArrow.gotoAndStop(vo.arrow);
			
			//calculate the point to aim
			pointToAim = vo.pointToAim.clone();
			pointToAim.x += fakeHeroPos.x; 
			pointToAim.y += fakeHeroPos.y; 
			
			
			//position the tooltip
			switch(vo.arrow)
			{
				case ARROW_UP:
					tooltipContent.y = tooltipArrow.height;
					tooltipContent.x = -tooltipContent.width/2;
					break;
				case ARROW_DOWN:
					tooltipContent.y = -tooltipArrow.height - tooltipContent.height;
					tooltipContent.x = -tooltipContent.width/2;
					break;
				case ARROW_LEFT:
					tooltipContent.y = -tooltipContent.height/2;
					tooltipContent.x = tooltipArrow.width;
					break;
				case ARROW_RIGHT:
					tooltipContent.y = -tooltipContent.height/2;
					tooltipContent.x = -tooltipArrow.width - tooltipContent.width;
					break;
			}

		}
		
		/**
		 * Change Arrow position based on the vo
		 * */
		private function positionArrow(vo:TutorialStepVo):void
		{
			
		}
		
		/**
		 * destroy function
		 * */
		private function dispose(isTransition:Boolean = false):void
		{
			if(tooltip.parent)
				tooltip.parent.removeChild(tooltip);
			
			if(fakeStage && fakeStage.bitmapData)
				fakeStage.bitmapData.dispose();
			
			if(fakeStage && fakeStage.parent)
				fakeStage.parent.removeChild(fakeStage);
			
			if(fakeStage)
				fakeStage = null;
			
			if(darkness && darkness.parent && !isTransition)
				darkness.parent.removeChild(darkness);
			
			if(darkness && !isTransition)
				darkness = null;
			
			if(fakeHero && fakeHero.bitmapData)
				fakeHero.bitmapData.dispose();
			if(fakeHero && fakeHero.parent)
				fakeHero.parent.removeChild(fakeHero);
			if(fakeHero)
				fakeHero = null;
			
			if(fakeHeroWrapper && fakeHeroWrapper.parent)
				fakeHeroWrapper.parent.removeChild(fakeHeroWrapper);
			
			if(fakeHeroWrapper)
				fakeHeroWrapper = null;
			
			if(btnWrapper)
			{
				btnWrapper.removeChildren();
				btnWrapper = null;
			}
			
			
		}
		
		/**
		 * SKIP Btn click handler
		 * */
		private function skipBtnHandler():void
		{
			finishMe();
		}
		
		/**
		 * NEXT  Btn click handler
		 * */
		private function nextBtnHandler():void
		{
			currentStepIndex++;
			nextStep();
		}
		
		/**
		 * PREV  Btn click handler
		 * */
		private function prevBtnHandler(e:MouseEvent):void
		{
			currentStepIndex--;
			nextStep();
		}	
		
		/**
		 * FINISH Btn click handler
		 * */
		private function finishBtnHandler():void
		{
			
			finishMe();
		}
		
		private function finishMe():void
		{
			isRunning =false;
			hideStep(dispose, true);	
		}
		

	}
}

class SingletonEnforcer{}