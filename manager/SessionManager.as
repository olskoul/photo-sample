/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import com.google.analytics.debug.FailureAlert;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.utils.Dictionary;
	
	import be.antho.data.ResourcesManager;
	import be.antho.utils.DataLoadingManager;
	
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	import br.com.stimuli.loading.loadingtypes.LoadingItem;
	
	import data.AlbumVo;
	import data.Infos;
	import data.LangVo;
	import data.PhotoVo;
	import data.ProductsCatalogue;
	import data.SessionVo;
	
	import offline.data.OfflineProjectManager;
	import offline.manager.OfflineAssetsManager;
	
	import org.osflash.signals.Signal;
	
	import service.ServiceManager;
	
	import utils.Debug;
	import utils.VectorUtils;
	
	import view.popup.PopupAbstract;
	import view.popup.PopupLogin;
	import view.uploadManager.PhotoUploadItem;
	import view.uploadManager.PhotoUploadManager;

	/**
	 * Project manager
	 * The class is used to manage project vo (LOAD/SAVE/EDIT..)
	 */
	public class SessionManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// signals
		public static const IMAGE_LIST_UPDATE : Signal = new Signal();
		public static const SESSION_READY : Signal = new Signal();
		public static const SESSION_ERROR : Signal = new Signal( String );
		
		// vars
		public var session : SessionVo;
		
		//Resources
		private var langProcessIndex:int = 0;
		private var resourcesLoader:BulkLoader;
		private var resourcesPath:String;
		private var resourceFailed:Array = [];

		
		//Menu Preload
		private var menuLoader:BulkLoader;
		public var menusXML : Object;

		
		// image list in xml format
		private var imageListXML : XML;
		private var _projectListXML : XML; 			// first time we load app, we retrieve full project list to be used later (in save, in folder management) 
		private var _isConnectionCheck : Boolean; 	// flag to indiquate if the check session is done inside a global connection check, if so, login popup is different
		private var _originalPathRefs : Dictionary ; // dictionary to store all images original path (for offline only right now) which is used to check doublons
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function SessionManager(sf:SingletonEnforcer) { if(!sf) throw new Error("ProjectManager is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:SessionManager;
		public static function get instance():SessionManager
		{
			if (!_instance) _instance = new SessionManager(new SingletonEnforcer())
			return _instance;
		}
		
		/**
		 * ------------------------------------ GETTER / SETTER -------------------------------------
		 */
		
		// album list (put it in getter/setter) for better control on set
		private var _albumList : Vector.<AlbumVo> = new Vector.<AlbumVo>;

		public function get albumList():Vector.<AlbumVo> 
		{
			return _albumList;
		}
		public function setImageList( value : Vector.<AlbumVo>):void
		{
			_albumList = value;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/*
		  STEPS ARE :
		  	> check the session file to setup editor class, recent project, user infos and lang
			> once we have session setup, we load lang resources
			> then we check if user is logged (session is still valid), otherwise we open login popup
			> when user is loggedin, we retrieve and update his projects list
			> then we update his images list
		 */
		
		/**
		 * ------------------------------------ CHECK SESSION -------------------------------------
		 */
		
		/**
		 * check session (online for webversion and hardcoded for offline )
		 */
		public function checkSession( isConnectionCheck : Boolean = false ):void
		{
			//Call webservice
			_isConnectionCheck = isConnectionCheck;
			ServiceManager.instance.checkSession(onSessionResult,onSessionError);
		}
		
		private function onSessionResult( result:Object ):void
		{
			Debug.log("SessionManager.onSessionResult");
			var sessionXML:XML = new XML(result);
			
			if(!Infos.IS_DESKTOP && !_isConnectionCheck && !sessionXML) {
				Debug.error("Problem with chksess : result is "+result.toString());
				return;
			}
			
			if(!_isConnectionCheck)
			{
				//Fill SessionVo
				session.fill(new XML(sessionXML));
				
				// session object is created, load resources if needed
				loadLangResources();
			}
			
			
		}
		private function onSessionError( msg : String ):void
		{
			Debug.log("SessionManager.onSessionError with msg:" + msg);
			SESSION_ERROR.dispatch(msg);
		}
		
		/**
		 * ------------------------------------ LOAD CORRECT LANGUAGE RESOURCE -------------------------------------
		 */
		
		/**
		 * Load lang Resources
		 */
		public function loadLangResources():void
		{
			Debug.log("SessionManager.loadResources");
			
			
			//Check if not already done when the first check sess has been done
			if(ResourcesManager.ready) 
			{
				//continue flow
				resourcesAreLoadedThen();
				return;
			}
			
			if(resourcesLoader != null)
			{
				//ResourceManager is not ready, but the bulk instance is not null... That's where the problem is.
				//Could not find where was the double call coming from.
				//Added logs
				//stop it.
				return;
			}
			
			//Check general pref for manguage
			Infos.prefs.loadGeneralPrefs();
			if(Infos.prefs.lang)
				session.lang = Infos.prefs.lang;
	
			//Define which path to use
			CONFIG::online
			{
				resourcesPath = Infos.config.getValue("assetPath") + Infos.config.getValue("resourcePath"); 
			}
			CONFIG::offline
			{
				if(Debug.USE_LOCAL_RESSOURCE || !Infos.INTERNET )
					resourcesPath = Infos.offlineAppFilePath + Infos.config.getValue("resourcePath");
				else
					resourcesPath =  Infos.config.serverNormalUrl + "/albums/" + Infos.config.getValue("assetPath") + Infos.config.getValue("resourcePath");
			}
			//start load
			startLoadResources();
			
		}
		
		/**
		 * Start load resource 
		 */
		private function startLoadResources():void
		{
			//show Loading
			DataLoadingManager.instance.updateText(Infos.getLangVoById(session.lang).loadingLabel);
			
			var timeStamp:String = (!Infos.IS_DESKTOP)?"?t="+new Date().time:"";
			
			if(!resourcesLoader)
			{
				resourcesLoader = new BulkLoader("resources");
				resourcesLoader.addEventListener(BulkProgressEvent.COMPLETE, resourcesLoadedHandler);
				resourcesLoader.addEventListener(BulkLoader.ERROR, resourcesLoadError);
			}
			
			for (var i:int = 0; i < Infos.config.languages.length; i++) 
			{
				var langVo:LangVo = Infos.config.languages[i];
				
				//Adding  item
				resourcesLoader.add( resourcesPath + langVo.id +".xml"+timeStamp , { id:langVo.id });
			}
			
			resourcesLoader.start();
		}
		
		/**
		 * Load resource Success Function
		 */
		private function resourcesLoadedHandler(e:Event):void
		{
			resourcesLoader.removeEventListener(BulkProgressEvent.COMPLETE, resourcesLoadedHandler);
			resourcesLoader.removeEventListener(BulkLoader.ERROR, resourcesLoadError);
			
			//Check if there is failed resources
			if(resourceFailed.length > 0) 
			{
				// if desktop, we directly try to reload all resources locally
				/*if(Infos.IS_DESKTOP)
				{
					CONFIG::offline
						{
							// If desktop and current path is online path, try it with local
							var localPath:String = Infos.offlineAppFilePath + Infos.config.getValue("resourcePath");
							if(resourcesPath != localPath)
							{
								resourceFailed = [];
								resourcesLoader.removeAll();
								resourcesPath = localPath;
								startLoadResources();
								return;
							}								
						}
				}*/
				
				//if there is at least one lang available let's go
				if(resourcesLoader.items.length > 0)
				{
					//clean failed Lang
					for (var i:int = 0; i < resourceFailed.length; i++) 
					{
						removeFailedLang(resourceFailed[i]);
					}
					
					//then process those OK
					processLanguages();
					
				}
				else
					Debug.error("SessionManager.resourcesLoadError : "+e.toString());	
				
			
			}
			else
				processLanguages();
			
			
		}
		
		/**
		 * Online and offline:
		 * Add lang to ResourcesManager
		 * 
		 * Offline:
		 * Copy XML locally
		 */
		private function processLanguages():void
		{
			Debug.log("SessionManager.processLanguages " + langProcessIndex);
			var langId:String = Infos.config.languages[langProcessIndex].id;
			var langXML:XML = resourcesLoader.getXML(langId);
			
			ResourcesManager.addLang(langXML,langId);
			
			//if desktop and we've loaded resources online, then save them locally
			if(Infos.IS_DESKTOP)
			{	
				CONFIG::offline
					{
						var localPath:String = Infos.offlineAppFilePath + Infos.config.getValue("resourcePath");
						if(resourcesPath != localPath)
							writeResourceXmlLocally(langXML,Infos.offlineAppFilePath +  Infos.config.getValue("resourcePath") +langId+".xml", processNextLanguage);
						else
							processNextLanguage();
						
					}
			}
			else
				processNextLanguage();
		}
		
		/**
		 * Process next language XML if needed
		 * or continue flow to initResources
		 */
		private function processNextLanguage():void
		{
			if(langProcessIndex < Infos.config.languages.length-1)
			{
				langProcessIndex ++;
				processLanguages();
			}
			else
				initResources();
		}
		
		/**
		 * Update offline XML with freshly downloaded one
		 * Replace local XML and reset Manager
		 */
		public function writeResourceXmlLocally(freshXML:XML, xmlPath:String , onComplete:Function):void
		{
			Debug.log("SessionManager.writeResourceXmlLocally: "+xmlPath);
			CONFIG::offline
				{
					OfflineAssetsManager.instance.saveXMLLocally(freshXML,xmlPath, 
						function(xmlString:String):void
						{
							ClipartsManager.instance.reset();
							onComplete.call();
						});
				}
				
				CONFIG::online
				{
					ProjectManager.instance.clipartsXML = freshXML;
					ClipartsManager.instance.reset();
					onComplete.call();
				}
		}
		
		/**
		 * Process ressources
		 * Set current lang
		 * Init resources manager
		 * And continue flox
		 */
		private function initResources():void
		{
			
			
			//Init resource manager
			ResourcesManager.init(session.lang.toUpperCase());
			
			//Init ContextMenu
			Debug.createContextMenu();
			
			//continue flow
			resourcesAreLoadedThen();
			
		}
		
		/**
		 * Function that follows load success of the resources
		 */
		private function resourcesAreLoadedThen():void
		{
			if(!Infos.IS_DESKTOP)
			{
				//check login state
				checkLoginState();
			}
			else
			{
				//Loading //todo: is this blocing interactivity with online when login?
				DataLoadingManager.instance.showLoading(false, null, ResourcesManager.getString("loading.sessionCheck"),true);
				
				//pre-load menus XMLs
				loadMenuXML();
			}
		}
		
		
		private function resourcesLoadError(e:ErrorEvent):void
		{

			for (var i:int = 0; i < resourcesLoader.items.length; i++) 
			{
				var item:LoadingItem = resourcesLoader.items[i] as LoadingItem;
				var id:String = item.id;
				if(item.status != null && item.status == "error")
				{
					Debug.warn("ERROR ressource load failed: '"+id+"'");
					resourceFailed.push(id);
					resourcesLoader.remove(id);
					
				}
			}
			
			
		}
		
		/**
		 * When a lang failed to load, remove it 
		 */
		private function removeFailedLang(id:String):void
		{
			for (var i:int = 0; i < Infos.config.languages.length; i++) 
			{
				var langVo:LangVo = Infos.config.languages[i];
				if(langVo.id == id)
				{
					Infos.config.languages.splice(i,1);
					return;
				}
			}
			
		}
		
		/**
		 * ------------------------------------ LOAD MENU XML FOR DASHBOARD -------------------------------------
		 */
		
		/**
		 * Load Menu XMl for Dashboard
		 * Load menu.xml for each product, Albums, Calendars, Card, Canvas
		 */
		private function loadMenuXML():void
		{
			Debug.log("SessionManager.loadMenuXML");
			
			CONFIG::offline
			{
				if(menusXML != null)
				{
					Debug.log("SessionManager.loadMenuXML : menus have already been loaded");
					//check login state
					checkLoginState();
					return;
				}
				
				var path:String;
				path = Infos.offlineAppFilePath;
				
				menuLoader = new BulkLoader("menus");
				menuLoader.addEventListener(BulkProgressEvent.COMPLETE, menusLoadedHandler);
				menuLoader.addEventListener(BulkLoader.ERROR, menusLoadError);
				menuLoader.add( path + ProductsCatalogue.CLASS_ALBUM + "/menu.xml" , { id:"menuAlbums" });
				menuLoader.add( path + ProductsCatalogue.CLASS_CALENDARS + "/menu.xml" , { id:"menuCalendars" });
				menuLoader.add( path + ProductsCatalogue.CLASS_CARDS + "/menu.xml" , { id:"menuCards" });
				menuLoader.add( path + ProductsCatalogue.CLASS_CANVAS + "/menu.xml" , { id:"menuCanvas" });
				menuLoader.start();
			}
		}
		
		private function menusLoadedHandler(e:Event):void
		{
			menusXML = {};
			menusXML[ProductsCatalogue.CLASS_ALBUM] 	= menuLoader.getXML("menuAlbums");
			menusXML[ProductsCatalogue.CLASS_CALENDARS] = menuLoader.getXML("menuCalendars");
			menusXML[ProductsCatalogue.CLASS_CARDS] 	= menuLoader.getXML("menuCards");
			menusXML[ProductsCatalogue.CLASS_CANVAS] 	= menuLoader.getXML("menuCanvas");
			
			//check login state
			checkLoginState();
		}
		private function menusLoadError(e:IOErrorEvent):void
		{
			Debug.error("SessionManager.menusLoadError : "+e.toString());
			// do nothing...
		}
		
		
		/**
		 * ------------------------------------ CHECK LOGIN STATE -------------------------------------
		 */
		
		private function checkLoginState(p:PopupAbstract = null):void
		{
			//Check session result
			if(session.userId != "0") // offline user id is never "0", it's created at first use
			{
				if(!_projectListXML) loadProjectList();
				else
				{
					IMAGE_LIST_UPDATE.addOnce(SESSION_READY.dispatch);
					updateImageList();	
				}
			}
			else 
			{
				showLoginPopup();
			}
		}
		private function showLoginPopup():void
		{
			var loginPopup:PopupLogin = new PopupLogin( _isConnectionCheck );
			loginPopup.open();
			loginPopup.OK.addOnce(loginPpopupOkHandler);
		}
		private function loginPpopupOkHandler(p:PopupAbstract):void
		{
			// check session again
			checkSession();
		}
		
		
		
		
		/**
		 * ------------------------------------ LOAD AND UPDATE PROJECT LIST -------------------------------------
		 */
		
		/**
		 * retrieve user project list
		 */
		private function loadProjectList():void
		{
			// load full project list (without classname) to retrieve all possible project names from user.
			CONFIG::online
			{
				ServiceManager.instance.getProjectList("", false, onProjectListLoadSuccess);
			}
			
			CONFIG::offline
			{
				OfflineProjectManager.instance.getProjectList(onProjectListLoadSuccess);
			}
		}
		private function onProjectListLoadSuccess( result:Object ):void
		{
			_projectListXML = new XML(result.toString());
			checkLoginState();
		}
		
		
		/**
		 * ------------------------------------ LOAD AND UPDATE IMAGE LIST -------------------------------------
		 */
		
		/**
		 * update image list
		 * with server call
		 */
		public function updateImageList():void
		{
			ServiceManager.instance.getImageList(onImageListResult);	
		}
		private function onImageListResult(result:Object):void
		{
			// image list retrived for webservice
			imageListXML = new XML(result.toString());
			
			// 
			PhotoUploadManager.instance.tempCompletedItemList = new Vector.<PhotoUploadItem>; // reset list of temp completed items
			refreshImageList();
		}
		
		
		/**
		 * ------------------------------------ REFRESH IMAGELIST BASED on imageListXML -------------------------------------
		 */
		
		/**
		 * create album vo structure from imageListXml
		 * no server call
		 */
		public function refreshImageList( tempFolderNameReplacement : String = null):void
		{
			// check xml
			if(!Infos.IS_DESKTOP && imageListXML)// && imageListXML.image.length() >0)
			{
				// generate album photo list base on image list result
				var numImages : uint = imageListXML.image.length();
				var albumVo : AlbumVo;
				var photoVo :PhotoVo;
				var serverUrl : String = "" + Infos.config.getSettingsValue("EditorProtocol") + "://" + Infos.config.getSettingsValue("EditorHost");
				var photoList : Array = new Array();
				_originalPathRefs = new Dictionary();
				for (var i:int = 0; i <numImages; i++) 
				{
					photoVo = new PhotoVo();


					
					// replace temp folder name with new folder name (after a rename)
					if(imageListXML.image[i].category.text() == FolderManager.instance.TempFolderName && tempFolderNameReplacement != null){
						imageListXML.image[i].category = tempFolderNameReplacement;
					}
					
					// add original path to dictionary for later doublons check 
					if( imageListXML.image[i].hasOwnProperty("origin_url")) {
						_originalPathRefs[""+imageListXML.image[i].origin_url.text()] = ""+imageListXML.image[i].id[0].text();
					}
					
					
					photoVo.fillFromXML(imageListXML.image[i]);
					photoList.push(photoVo);
				}
				
				// add temp photo items (uploading items)
				var tempImageCount : int = 0;
				for (i = 0; i < PhotoUploadManager.instance.uploadItemsList.length ; i++) 
				{
					photoVo = PhotoUploadManager.instance.uploadItemsList[i].photoVo;
					if(photoVo.tempID) {
						photoList.push(photoVo);
						tempImageCount ++;
					}
				}
				// add temp but completed
				for (i = 0; i < PhotoUploadManager.instance.tempCompletedItemList.length ; i++) 
				{
					photoVo = PhotoUploadManager.instance.tempCompletedItemList[i].photoVo;
					photoList.push(photoVo);
					tempImageCount ++;
				}
				Debug.log("SessionManager.refreshImageList > tempImage count : "+tempImageCount);
				
				// sort photo list on foldername
				photoList.sortOn("folderName");
				
				
				// check if there is a temp folder !
				
				// create albums
				_albumList = new Vector.<AlbumVo>
				var lastAlbumVo : AlbumVo;
				for (var j:int = 0; j <photoList.length; j++) 
				{
					photoVo = photoList[j];
					if( !lastAlbumVo || photoVo.folderName != lastAlbumVo.name){
						albumVo = new AlbumVo();
						albumVo.name = photoVo.folderName;
						albumList.push(albumVo);
						albumVo.photoList = new Vector.<PhotoVo>;
						lastAlbumVo = albumVo;
					}
					if(photoVo.tempID) {
						lastAlbumVo.mostRecentId = int.MAX_VALUE; // if album has temp photos, put it on top of album list, so bigger recent ID
					}
					else if(lastAlbumVo.mostRecentId < parseInt(photoVo.id)) lastAlbumVo.mostRecentId = parseInt(photoVo.id); // This is a quick hack as we do not receive the last modified date, we use photo id (which seems incredental) to determine most recent album
					
					lastAlbumVo.photoList.push(photoVo);
				}
				
				// sort photos inside albums
				changePhotoSortingInsideAlbum();
				
				
			}
			
			// sort album list on most recent id
			//VectorUtils.sortOn(albumList, "mostRecentId", Array.DESCENDING);
			albumList.sort(albumSortFunction);
			
			
			// case new project and no temp folder, create one virtual temp folder to display in photo tabs and photo manager
			if( ( !Infos.project || !Infos.project.id ) && !FolderManager.instance.TempFolder ){
				var tempAlbumVo : AlbumVo = new AlbumVo();
				tempAlbumVo.name = FolderManager.instance.TempFolderName;
				tempAlbumVo.mostRecentId = int.MAX_VALUE;
				tempAlbumVo.photoList = new Vector.<PhotoVo>;
				albumList.unshift(tempAlbumVo);
			}
			
			
			
			// each time we update image list we update map it with current project images
			ProjectManager.instance.remapPhotoVoList();
			
			// image list is updated
			IMAGE_LIST_UPDATE.dispatch();
		}
		
		
		/**
		 * ------------------------------------ HELPERS -------------------------------------
		 */
		
		
		/**
		 * Compare function to srot albums by most recent id and folder type
		 * DESCENDING
		 * return 1 if b is higher
		 * return -1 if a is higher
		 * */
		private function albumSortFunction(a:AlbumVo,b:AlbumVo):int 
		{
			var pa : int = FolderManager.instance.getFolderOrderPriority(a);
			var pb : int = FolderManager.instance.getFolderOrderPriority(b);
			
			if(pa != pb) return (pa < pb )?-1:1;
			
			// default on most recent ID
			return a.mostRecentId < b.mostRecentId ? 1 : -1;
		}
		
		
		/**
		 * Change photo sorting
		 */
		public function changePhotoSortingInsideAlbum():void
		{
			// sort photos inside albums
			for (var i2:int = 0; i2 < albumList.length; i2++) 
			{
				//Ref album
				var albumToSort:AlbumVo = albumList[i2];
								
				//sort his photo
				switch(Infos.prefs.photoSortingType)
				{
					case ProjectManager.PHOTO_SORT_ON_NAME_DES:
						albumToSort.photoList.sort(sortOnNameDES);
						break;
					
					case ProjectManager.PHOTO_SORT_ON_NAME_ASC:
						albumToSort.photoList.sort(sortOnNameASC);
						break;
					
					case ProjectManager.PHOTO_SORT_ON_DATE_DES:
						albumToSort.photoList = VectorUtils.sortOn(albumToSort.photoList,"originalDate",Array.DESCENDING | Array.NUMERIC);
						break;
					
					case ProjectManager.PHOTO_SORT_ON_DATE_ASC:
						albumToSort.photoList = VectorUtils.sortOn(albumToSort.photoList,"originalDate",Array.NUMERIC);
						break;
					
					default:
						Infos.prefs.photoSortingType = ProjectManager.PHOTO_SORT_ON_DATE_DES;
						albumToSort.photoList = VectorUtils.sortOn(albumToSort.photoList,"originalDate",Array.DESCENDING | Array.NUMERIC);
						
				}
				
				// log album sorting
				//logAlbumSorting(albumToSort);
				
				//trace out to verify
				/*
				trace("--------------------------"+albumToSort.name);
				if(albumToSort.name == "@@temp@@")
				{
					for (var k:int = 0; k < albumToSort.photoList.length; k++) 
					{
						trace(albumToSort.photoList[k].name);
						trace(albumToSort.photoList[k].originalDate);
						var date:Date = new Date(albumToSort.photoList[k].originalDate);
						trace(date.date +" / "+date.month +" / "+date.fullYear + " - "+date.hours+":"+date.minutes+":"+date.seconds);
					}
				}
				*/
				
			}
		}
		private function logAlbumSorting( albumSorted : AlbumVo ) : void 
		{
			// sort then items
			var maxSort : int = 10;
			if(maxSort > albumSorted.photoList.length) maxSort = albumSorted.photoList.length;
			Debug.log("SORT START -----------");
			for (var i:int = 0; i < maxSort; i++) 
			{
				Debug.log("["+i+"]" + albumSorted.photoList[i].name);
			}
			Debug.log("SORT END -----------");
			
		}
		
		
		/**
		 *  Compare function to sort alphanumerical value of object based on their "name" property
		 *  ASCENDING
		 * */
		private function sortOnNameASC(a:*,b:*):int 
		{
			var reA:RegExp = /[^a-zA-Z]/g;
			var reN:RegExp = /[^0-9]/g;
			/*
			var aA:String = a.name.replace(reA, "");
			var bA:String = b.name.replace(reA, "");
			if(aA === bA) {
				var aN:Number = parseInt(a.name.replace(reN, ""), 10);
				var bN:Number = parseInt(b.name.replace(reN, ""), 10);
				return aN === bN ? 0 : aN > bN ? 1 : -1;
			} else {
				return aA < bA ? -1 : 1;
			}
			*/
			
			return (a.name as String).toLowerCase() < (b.name as String).toLowerCase() ? -1 : 1;
		}
		
		/**
		 *  Compare function to sort alphanumerical value of object based on their "name" property
		 *  DESCENDING
		 * */
		private function sortOnNameDES(a:*,b:*):int 
		{
			/*----------------------------------------------------
			this method produce (ASC) --> not good
				hello 1
				hello 2 
				hello 10
				1942 hello
				hello 1950
				1939 hello 1
				1920 hello 22
				hello 2014-2-10
				hello 2014-10-10
				
			*/
			
			/*
			var reA:RegExp = /[^a-zA-Z]/g;
			var reN:RegExp = /[^0-9]/g;
			
			var aA:String = a.name.replace(reA, "");
			var bA:String = b.name.replace(reA, "");
			if(aA === bA) {
				var aN:Number = parseInt(a.name.replace(reN, ""), 10);
				var bN:Number = parseInt(b.name.replace(reN, ""), 10);
				return aN === bN ? 0 : aN < bN ? 1 : -1;
			} else {
				return aA < bA ? 1 : -1;
			}
			*/
			
			
			/*----------------------------------------------------
			 this method produce (ASC)
				1920 hello 22	
				1940 hello
				1939 hello 1
				hello 1
				hello 10
				hello 2
				hello 1920
				hello 2014-10-10
				hello 2014-2-10
			*/
			
			return (a.name as String).toLowerCase() < (b.name as String).toLowerCase() ? 1 : -1;
		}
		
		
		/**
		 * Helpers for photo
		 */
		public function getAlbumVoByPhotoId( photoId : String ):AlbumVo
		{
			for (var i:int = 0; i < albumList.length; i++) 
			{
				var photoList : Vector.<PhotoVo> = albumList[i].photoList;
				for (var j:int = 0; j < photoList.length; j++) 
				{
					if(photoList[j].id == photoId) return albumList[i];
				}
			}
			
			return null;
		}
		public function getPhotoVoByName( photoName : String ):PhotoVo
		{
			for (var i:int = 0; i < albumList.length; i++) 
			{
				var photoList : Vector.<PhotoVo> = albumList[i].photoList;
				for (var j:int = 0; j < photoList.length; j++) 
				{
					if(photoList[j].name == photoName) return photoList[j];
				}
			}
			
			return null;
		}
		public function getPhotoVoById( photoId : String ):PhotoVo
		{
			for (var i:int = 0; i < albumList.length; i++) 
			{
				var photoList : Vector.<PhotoVo> = albumList[i].photoList;
				for (var j:int = 0; j < photoList.length; j++) 
				{
					if(photoList[j].id == photoId) return photoList[j];
				}
			}
			
			return null;
		}
		


		
		/**
		 * retrieve a project name by it's ID
		 */
		public function getProjectNameById( projectId : String ):String
		{
			try
			{
				for (var i:int = 0; i < _projectListXML.project.length(); i++) 
				{
					if(_projectListXML.project[i].id.text() == projectId) return _projectListXML.project[i].name.text()
				}	
			}
			catch (e:Error)
			{
				Debug.warn("SessionManager.getProjectNameById : projectid = "+projectId + " & projectLIstXMl = "+_projectListXML);
				return null;
			}
			
			
			return null;
		}
		
		
		
		/**
		 * ------------------------------------ CHECK IF IMAGE ALREADY EXISTS -------------------------------------	
		 */
		
		public function imageAlreadyExists( nativePath:String):Boolean
		{
			// do we have a match in the current image list
			if( _originalPathRefs[nativePath] ) 
			{
				Debug.log("SessionManager.imageAlreadyExists : "+nativePath);
				return true;
			}
			
			// do we have a match in the current uploading list
			
			return false;
		}
		
		
		
		/**
		 * Get an existing photovo by origin path (OFFLINE)
		 */
		public function getExistingPhotoVoByOriginUrl( originPath : String ):PhotoVo
		{
			var photoId : String =  _originalPathRefs[originPath];
			var photoVo : PhotoVo = getPhotoVoById( photoId );
			return photoVo;
		}
		
		
		
		
	}
}
class SingletonEnforcer{}