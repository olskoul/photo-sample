/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import be.antho.data.ResourcesManager;
	
	import org.osflash.signals.Signal;
	
	import utils.Debug;
	
	import view.popup.PopupAbstract;
	

	/**
	 * Connection manager
	 * This utils is used to check connection and session and see if user can continue to work
	 */
	public class ConnectionManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		// signals
		public static const CONNECTION_CHECK_COMPLETE : Signal = new Signal(Boolean);	// return true if internet connection is on
		public static const SESSION_CHECK_COMPLETE : Signal = new Signal(Boolean);		// return true if user is logged in
		
		// private
		private var isChecking : Boolean = false;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function ConnectionManager(sf:SingletonEnforcer) { if(!sf) throw new Error("ConnectionManager is a singleton, use instance"); }
		
		/**
		 * instance
		 */
		private static var _instance:ConnectionManager;
		public static function get instance():ConnectionManager
		{
			if (!_instance) _instance = new ConnectionManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * check if able to connect to server
		 */
		public function checkConnection():void
		{
			Debug.log("ConnectionManager.checkConnection");
			if(isChecking) {
				Debug.log("Already checking connection...");
				return;
			}
			isChecking = true;
			checkSession();
		}
		
		/**
		 * check if user is logged in
		 */
		private function checkSession():void
		{
			Debug.log("ConnectionManager.checkConnection by calling checkSession");
			SessionManager.SESSION_READY.remove(sessionOk);
			SessionManager.SESSION_ERROR.remove(sessionNotOk);
			SessionManager.SESSION_READY.addOnce(sessionOk);
			SessionManager.SESSION_ERROR.addOnce(sessionNotOk);
			SessionManager.instance.checkSession( true );
		}

		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * session is ok, user is logged in and connection is ok
		 */
		private function sessionOk():void
		{
			Debug.log("ConnectionManager.sessionNotOk");
			SessionManager.SESSION_READY.remove(sessionOk);
			SessionManager.SESSION_ERROR.remove(sessionNotOk);
			isChecking = false;
		}
		private function sessionNotOk( msg : String):void
		{
			Debug.log("ConnectionManager.sessionNotOk");
			// error while connecting to check session webservice.. connection seem to be down, display popup
			PopupAbstract.Alert(ResourcesManager.getString("connectioncheck.down.title"), ResourcesManager.getString("connectionCheck.down.desc"), false, onRetryHandler );
		}
		
		/**
		 *
		 */
		private function onRetryHandler( p : PopupAbstract ):void
		{
			isChecking = false;
			checkConnection();
		}
		
	}
}
class SingletonEnforcer{}