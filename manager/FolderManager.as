package manager
{
	import be.antho.data.ResourcesManager;
	
	import data.AlbumVo;
	import data.Infos;
	
	import flash.events.MouseEvent;
	import flash.globalization.DateTimeFormatter;
	import flash.globalization.DateTimeStyle;
	import flash.globalization.LocaleID;
	
	import org.osflash.signals.natives.INativeDispatcher;
	
	import service.ServiceManager;
	
	import utils.Debug;
	
	import view.popup.PopupAbstract;
	import view.uploadManager.PhotoUploadManager;
	
	/**
	 * FolderManager manage the folder system
	 */
	public class FolderManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////
		
		// const
		
		public const TempFolderName : String = "@@temp@@";
		
		public static const TYPE_FOLDER_CURRENT_PROJECT : int = 0;
		public static const TYPE_FOLDER_OTHER_PROJECT : int = 1;
		public static const TYPE_FOLDER_CUSTOM : int = 2;
		
		// the folder name linked to this project 
		private var projectFolderName : String;
		private var _updatedTempFolderName : String ; // temp value to store the new temporary folder name
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function FolderManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("FolderManager is a singleton, use instance"); 
		}
		
		/**
		 * instance
		 */
		private static var _instance:FolderManager;
		public static function get instance():FolderManager
		{
			if (!_instance) _instance = new FolderManager(new SingletonEnforcer())
			return _instance;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		
		/**
		 * get current project folder name
		 * > if there is a project ID return real folder name
		 * > if not, return temp folder name 
		 */
		public function get currentProjectFolderName() : String
		{
			// first check if there is a folder already created for this project
			/*
			for (var i:int = 0; i < Infos.project.albumList.length; i++) 
			{
				var folderName:String = Infos.project.albumList[i].name;
				if(isProjectFolder(folderName)) _currentFolderName = folderName;
			}*/
			
			// if not, we create one
			if( Infos.project && Infos.project.id ) return FinalProjectFolderName;
			else return TempFolderName;
			
		}
		
		
		/**
		 * Check if there is a temp folder in the current photo list
		 */
		public function get TempFolder() : AlbumVo
		{
			// first check if there is a folder already created for this project
			var album : AlbumVo ;
			for (var i:int = 0; i < SessionManager.instance.albumList.length; i++) 
			{
				album = SessionManager.instance.albumList[i];
				if( album.name == TempFolderName ) return album;
			}
			return null;
		}
		
		/**
		 * A custom folder name for today, used when user don't want to merge temp folder with current project folder
		 */
		private var _todayFolderName:String;
		public function get TodayFolderName() : String
		{
			if(_todayFolderName) return _todayFolderName
			var df:DateTimeFormatter = new DateTimeFormatter(LocaleID.DEFAULT, DateTimeStyle.SHORT, DateTimeStyle.NONE);
			var currentDate:Date = new Date();
			_todayFolderName = df.format(currentDate); 
			return _todayFolderName;
		}
		
		
		/**
		 * retrieve the update folder name
		 * the folder name that should be (but that might not be)
		 */
		public function get FinalProjectFolderName() : String
		{
			return "@@"+ Infos.project.id +"@@";
		}
		
		
		

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * retrieve corresponding folder in the folder list
		 */
		public function init():void
		{}
		
		
		/**
		 * Check folder status
		 * > verify if there is a temp folder in photo list
		 * > if yes and there is also a project ID, user must make a choice : merge temp folder with current project or use custom folder name.
		 */
		public function checkFolderStatus() : void
		{
			Debug.log("FolderManager.checkFolderStatus");
			if(Infos.project && Infos.project.id ){
				var tempFolder : AlbumVo = FolderManager.instance.TempFolder;
				if( tempFolder && tempFolder.photoList && tempFolder.photoList.length >0 ){
					var customFolderName : String = FolderManager.instance.TodayFolderName;
					var popupDesc:String = ResourcesManager.getString("popup.photo.not.linked.desc") + customFolderName;
					PopupAbstract.Alert(ResourcesManager.getString("popup.photo.not.linked.title"), popupDesc, false, null, null, false, ["common.yes", "common.no"],[mergeTempFolderWithProjectFolder, transformTempFolderToCustomFolder]); 
				}
			}
		}
		
		
		/**
		 * return a folder priority depending on it's type
		 */
		public function getFolderOrderPriority( a : AlbumVo ) : int
		{
			if( a.name == currentProjectFolderName ) return TYPE_FOLDER_CURRENT_PROJECT;
			if( isSpecialFolder( a.name )) return TYPE_FOLDER_OTHER_PROJECT;
			return TYPE_FOLDER_CUSTOM;
		}
		
		
		/**
		 * check if a folder is used in this project
		 */
		public function checkIfFolderIsUsed( folderName : String ):Boolean
		{
			// project default folder and temp folder are always activated
			if( isProjectFolder( folderName ) || isTempFolder( folderName ) ) return true;
			
			var foldersUsedList : Array = Infos.prefs.foldersUsedList; 
			if(!foldersUsedList) return false;
			for (var i:int = 0; i < foldersUsedList.length; i++) 
			{
				if(folderName == foldersUsedList[i]) return true;
			}
			return false;
		}
	
		
		/**
		 * return real display name of a folder to show to user
		 */
		public function getFolderDisplayName( folderName : String ) :String
		{
			if(Debug.SHOW_REAL_FOLDER_NAME) return folderName;
			
			// case current project folder name
			if( isProjectFolder(folderName) ) return Infos.project.name;// ResourcesManager.getString("lefttab.photos.projectFolderName");//Infos.project.name;
			
			// case temp folder name
			else if ( isTempFolder(folderName) ) return ResourcesManager.getString("lefttab.photos.projectFolderName");//ResourcesManager.getString("lefttab.photos.recentUploadFolderName");
			
			// case another project folder name
			else if ( isSpecialFolder(folderName) ){
				var projectId:String = folderName.split("@@")[1];
				var newName: String = SessionManager.instance.getProjectNameById( projectId );
				if(newName) return newName;
				else return folderName;
			}
			
			// case normal folder
			return folderName;
		}
	
		
		
		/**
		 * is this folder allowed to be edited
		 * > false if folder is a project related folder
		 * > false if folder is the temp folder
		 */
		public function canFolderBeEdited( folderName : String ):Boolean
		{
			if( isSpecialFolder(folderName) ) return false;
			else return true;
		}
		
		/**
		 * is this folder allowed to desactivated in the project manager
		 * > 
		 */
		public function canFolderBeDesactivated( folderName : String ):Boolean
		{
			if( isProjectFolder( folderName )) return false;
			else if( isTempFolder( folderName )) return false;
			
			return true;
		}
		
		
		
		/**
		 * New project was saved !
		 * -> we need to create a project with current projet ID
		 * -> IF there is a temp folder, we need to convert it to current project ID 
		 */
		public function newProjectSaved():void
		{
			mergeTempFolderWithProjectFolder();
		}
		
		
		/**
		 * Merge temp folder with current project folder
		 * > find name of current project folder
		 * > rename temp folder server side
		 * > refresh list
		 */
		private function mergeTempFolderWithProjectFolder(e:MouseEvent = null ):void
		{
			renameTempFolder( FinalProjectFolderName );
		}
		
		/**
		 * New project was saved !
		 * -> we need to create a project with current projet ID
		 * -> IF there is a temp folder, we need to convert it to current project ID 
		 */
		private function transformTempFolderToCustomFolder(e:MouseEvent = null):void
		{
			renameTempFolder( TodayFolderName );
		}
		
		/**
		 * Serverside rename temp folder
		 */
		private function renameTempFolder( newFolderName : String ):void
		{
			// first we be sure to update all uploading items to this new name !
			PhotoUploadManager.instance.updateTempFolderUploadName( newFolderName );
			
			// then we tell the server the new name selected
			_updatedTempFolderName = newFolderName;
			ServiceManager.instance.renameFolder( TempFolderName, newFolderName , onFolderUpdateSuccess );
		}
		
		
		
		////////////////////////////////////////////////////////////////
		//	private METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * is this folder the project folder
		 */
		private function isProjectFolder(folderName : String):Boolean
		{
			if( Infos.project && Infos.project.id)
			{
				if(folderName.indexOf("@@"+ Infos.project.id +"@@") > -1) return true;
			}
			return false;
		}
		
		/**
		 * is this folder special?
		 */
		public function isSpecialFolder(folderName : String):Boolean
		{
			if( folderName.indexOf("@@") > -1 ) return true;
			return false;
		}
		
		/**
		 * is this folder the temp folder
		 */
		public function isTempFolder(folderName : String):Boolean
		{
			if(folderName == TempFolderName) return true;
			return false;
		}
		
		/**
		 * update foldername serverside
		 */
		private function onFolderUpdateSuccess( result:Object ):void
		{
			Debug.log("FolderManager.onFolderUpdateSuccess");
			SessionManager.instance.refreshImageList( _updatedTempFolderName );
			_updatedTempFolderName = null;
		}
	}	
}
class SingletonEnforcer{}