/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: Anthony De Brackeleire (http://www.antho.be)
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import data.Infos;

	/**
	 * Print manager
	 */
	public class MeasureManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		/* 
		NOTE : inch > cm > point (PostScript point) > pixel
		
		1 inch = 96 pixel -- 72 pixel
		1 inch = 72 point
		1 inch = 2.54 cm
		1 pixel = 0.75 point
		1 cm = 28.3464567 points
		*/
		
		public static const inchToPixel : Number = 72; // TODO : check if Server side use same ratio and values.. otherwise it will not work correctly. EDIT : seems that they work with 72 and not 96.. EDIT : they work with 96 but don't want to change all the system..
		public static const inchToPoint : Number = 72;
		public static const pixelToPoint : Number = inchToPoint/inchToPixel;
		public static const inchToCm : Number = 2.54;
		public static const cmToPoint : Number = 28.3464567;
		public static const mmToPoint : Number = cmToPoint/10; // TODO : check if Server side use same ratio and values.. otherwise it will not work correctly. EDIT : seems that they work with 72 and not 96
		
		
		/*
		private static const minImageDpi : Number = 150 ; // 300
		private static const koImageDpi : Number = 75 ; // 300
		private static const imageLowScale:Number = inchToPixel/minImageDpi; // this scale is the scale corresponding to an image injected in 72 dpi to equal 300 dpi
		private static const imageKOScale:Number = inchToPixel/koImageDpi; // this scale is the scale corresponding to an image injected in 72 dpi to equal 300 dpi
		*/
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public static function PFLToPixel( PFLValue:Number, isCover : Boolean = false ):Number
		{
			var postScriptValue : Number;
			if(isCover)
				postScriptValue = PFLValue * Infos.project.PFLCoverMultiplier;
			else 
				postScriptValue = PFLValue * Infos.project.PFLMultiplier;
			return postScriptValue / pixelToPoint;
		}
		
		/**
		 *
		 */
		public static function CMToPixel( CMValue:Number ):Number
		{
			return CMValue * cmToPoint;
		}
		
		/**
		 *
		 */
		public static function PixelToCM( pixelValue:Number, isCover : Boolean = false ):Number
		{
			var pfl : Number = pixelToPFL(pixelValue);
			return PFLToCM(pfl,isCover);
		}
		
		/**
		 *
		 */
		public static function PFLToCM( PFLValue:Number, isCover : Boolean = false ):Number
		{
			var pixel : Number = PFLToPixel(PFLValue, isCover);
			return pixel/cmToPoint;
		}
		
		/**
		 *
		 */
		public static function pixelToPFL( pixelValue:Number ):Number
		{
			return pixelValue / Infos.project.PFLMultiplier * pixelToPoint;
		}
		
		
		
		/**
		 *
		 */
		public static function pixelToPostScript( pixel:Number ):Number
		{
			return pixel*pixelToPoint;
		}
		
		/**
		 * TODO : if tomorrow point != pixel, we need to change this function
		 */
		public static function millimeterToPixel( mill:Number ):Number
		{
			return mill*mmToPoint;
		}
		
		
		
		/**
		 *
		 */
		public static function pixelToInch( pixel:Number ):Number
		{
			return pixel/inchToPixel;
		}
		
		/**
		 *
		 */
		/*
		public static function pixelToDpi( value:Number ):Number
		{
			var points : Number = pixelToPostScript(value);
			
			return value/pointToPixel;
		}
		*/
		
		
		/**
		 * return true if image quality is enough
		 * return false if image quality is not enough
		 * Images injected are always in 72dpi
		 * Document resolution is the minimum resolution required for this document to be ok
		 * Returns = 
		 * 1 : quality is enough
		 * 0 : quality is low, should display yello warning
		 * -1 : quality is really poor, should display red warning 
		 */
		public static function checkImageResolution(imageScale:Number):Number
		{
			var okImageDpi : Number = Infos.project.resolution ;
			var lowImageDpi : Number = Infos.project.resolution *0.75; //*0.85;
			//var badImageDpi : Number = Infos.project.resolution *0.70;
			
			var imageLowScale:Number = inchToPixel/okImageDpi; // this scale is the scale corresponding to an image injected in 72 dpi to equal 300 dpi
			var imageBadScale:Number  = inchToPixel/lowImageDpi; // this scale is the scale corresponding to an image injected in 72 dpi to equal 300 dpi
			
			if(imageScale > imageBadScale) return -1;
			if(imageScale > imageLowScale) return 0;
			return 1;
		}
		
		
		/**
		 * OLD EDITOR calculation code...
		 */
		/*
		public function PFLToPixelBadMultiplier():Number
		{
			//normal: 140 DPI
			var multiplier:Number = 0.7;
			//docResolution = "144.0"
			return (PFLToPoints / 72) * (projectXML.DocResolution * multiplier);
		}
		
		public function PFLToPixelModerateMultiplier():Number
		{
			//normal: 170 DPI
			var multiplier:Number = 0.85;
			//docResolution = "144.0"
			return (PFLToPoints / 72) * (projectXML.DocResolution * multiplier);
		}
		
		public function PFLToPixelGoodMultiplier():Number
		{
			// actual resolution 200 DPI
			//docResolution = "144.0"
			return (PFLToPoints / 72) * projectXML.DocResolution;
		}
		*/

		
		
		
		/**
		 * FONT size converter
		 */
		public static function getFontSize( fontSize:Number = 15, isCover:Boolean = false ):Number
		{
			var multiplier:Number = (isCover)?Infos.project.PFLCoverMultiplier:Infos.project.PFLMultiplier
			var newFontSize : Number;
			newFontSize = fontSize / (mmToPoint) * multiplier;
			return newFontSize;
		}
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		
	}
}
class SingletonEnforcer{}