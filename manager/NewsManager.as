/***************************************************************
 COPYRIGHT 		: jonathan@nguyen.eu
 YEAR			: 2014
 ****************************************************************/
package manager
{
	import com.bit101.components.HBox;
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.ResizeEvent;
	
	
	import be.antho.data.ResourcesManager;
	import be.antho.data.SharedObjectManager;
	import be.antho.utils.SimpleImageGetter;
	
	import comp.button.SimpleButton;
	import comp.checkbox.CheckboxTitac;
	
	import data.Infos;
	import data.NewsVo;
	
	import library.news.view;
	
	import utils.ButtonUtils;
	import utils.Colors;
	import utils.Debug;
	
	
	/**
	 * News manager
	 */
	public class NewsManager
	{
		
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		

		private var currentStepIndex:int = 0;
		private var onComplete:Function = null;
		
		
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function NewsManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("NewsManager is a singleton, use instance"); 
		}
		
		/**
		 * instance
		 */
		private static var _instance:NewsManager;
		private var steps:Array;
		private var currentNewsGroupId:String;
		private var btnFinish:SimpleButton;
		private var btnNext:SimpleButton;
		private var btnSkip:SimpleButton;
		private var btnPrev:Sprite;
		private var stageRef:Stage;
		private var darkness:Sprite;
		private var btnWrapper:HBox;
		private var view:Sprite;
		private var viewBg:Sprite;
		private var viewContent:MovieClip;
		private var initiated:Boolean;
		private var shown:Boolean;
		private var doNotShowAnymore:CheckboxTitac;

		private var image:Sprite;
		
		public static function get instance():NewsManager
		{
			if (!_instance) _instance = new NewsManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * Create Vo's based on 
		 * */
		public function parseXML(xml:XML):void
		{
			Debug.log("NewsManager.parseXML");
			Debug.log("NEWS: XML : "+xml);
			//Get only the first newsgroup and make sure its the online/offline version
			var version:String = (Infos.IS_DESKTOP)?"offline":"online";
			var newsGroupList:XMLList = xml.newsGroup.(@version == version);
			if(newsGroupList.length() > 0)
			{
				//Save newsgroupID for capping
				currentNewsGroupId = newsGroupList[0].@id;
				//create newsVo
				var newsList:XMLList = newsGroupList[0].news;
				for (var i:int = 0; i < newsList.length(); i++) 
				{
					var node:XML = newsList[i];
					var vo:NewsVo = new NewsVo(node.title[Infos.lang.toUpperCase()],node.description[Infos.lang.toUpperCase()], node.image.@url);
					addNews(vo);
					
					
						Debug.log("NEWS: ");
						Debug.log("NEWS - title: "+vo.title);
						Debug.log("NEWS - description: "+vo.description);
					
				}
				
				
			}
		}
		
		/**
		 * init manager
		 * 
		 */
		public function init($stageRef:Stage):void
		{
			if(initiated)
				return;
			
			//set flag
			initiated = true;
			
			//stageRef
			stageRef = $stageRef;
			 
			//Setup view
			view = new library.news.view();
			viewContent = view["content"];
			viewContent["desc"].autoSize = "left";
			viewContent["title"].autoSize = "left";
			viewContent["back"]["txt"].autoSize = "left";
			viewContent["back"]["txt"].text = ResourcesManager.getString("common.prev");
			viewBg = viewContent["bg"];
			
			
			//Setup Btn
			btnPrev = ButtonUtils.makeButton(viewContent["back"],prevBtnHandler);
			btnNext = SimpleButton.createBlueButton("common.next", nextBtnHandler);
			btnSkip = SimpleButton.createGreyButton("common.skip", skipBtnHandler);
			btnFinish = SimpleButton.createBlueButton("common.close", finishBtnHandler);
			doNotShowAnymore = new CheckboxTitac(null,10,0,ResourcesManager.getString("news.checkbox.do.not.show.anymore.label"),null,true,Colors.WHITE);
			
			btnWrapper = new HBox(viewContent,0,0);		
			btnWrapper.alignment = HBox.MIDDLE;
			

			stageRef.addEventListener(ResizeEvent.RESIZE, resizeHandler);
		}
		
		/**
		 * destroy manager
		 * 
		 */
		public function destroy():void
		{
			dispose();
			
			if(btnWrapper)
			{
				btnWrapper.removeChildren();
				btnWrapper = null;
			}
			stageRef = null;
			_instance = null;
		}
		
		/**
		 * Add a step tothe news flow
		 * */
		public function addNews(vo:NewsVo):void
		{
			if(!steps)
				steps = [];
				
			steps.push(vo);
		}
		
		
		/**
		 * Start News show
		 * */
		public function open(onCompleteFunction:Function = null):void
		{
			onComplete = onCompleteFunction;
			dispose();
			currentStepIndex = 0;
			buildStep();
			//anim in
			showStep();
			shown = true;
		}
		
		/**
		 * Check if new availalbe
		 * 
		 */
		public function hasNews():Boolean
		{
			var cookieValue:String = SharedObjectManager.instance.read(SharedObjectManager.KEY_NEWS);
			return (!shown && steps != null && steps.length>0 && cookieValue != currentNewsGroupId);
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		
		/**
		 * Show currentStepIndex 
		 * */
		private function nextStep():void
		{
			hideStep(buildStep);
		}
		
		/**
		 * show current step
		 * */
		private function showStep():void
		{		
			Debug.log("News Manager: showStep");
			if(view.parent == null)
				stageRef.addChild(view);
			
			view.x = (stageRef.stageWidth - view.width) *.5;
			view.y = (stageRef.stageHeight - view.height) *.5;
			
			view.alpha = 0;
			view.visible = false;
			
			var timeline:TimelineMax = new TimelineMax();
			timeline.stop();
			if(darkness.alpha == 0)
			timeline.append( TweenMax.to(darkness, .6, {autoAlpha:1}) );
			timeline.append( TweenMax.to(view, .3, {autoAlpha:1, ease:Strong.easeOut}) );
			timeline.play();
		}
		
		
		
		/**
		 * hide current step
		 * */
		private function hideStep($onComplete:Function = null, isFinish:Boolean = false):void
		{
			Debug.log("News Manager: hideStep");
			var timeline:TimelineMax = new TimelineMax();
			timeline.stop();
			
			var timming:Number = (isFinish)?.3:0;
			
			if(isFinish)
			{
				timeline.append( TweenMax.to(darkness, .5, {autoAlpha:0}) );
				timeline.append( TweenMax.to(view, timming,{autoAlpha:0, onComplete:$onComplete}),-timming );
				timeline.play();
			}
			else
				$onComplete.call();
			
			
			
			
		}
		
		/**
		 * Build current step
		 * */
		private function buildStep():void
		{	
			Debug.log("News Manager: BuildStep");
			if(currentStepIndex > steps.length-1)
				currentStepIndex = 0;
			if(currentStepIndex < 0)
				currentStepIndex = steps.length-1;
			
			var vo:NewsVo = steps[currentStepIndex];
			
			//image
			if(image && image.parent)
				image.parent.removeChild(image);
			var sig:SimpleImageGetter = new SimpleImageGetter();
			image = sig.getImage(vo.imageUrl,true);
			viewContent["image"].addChildAt(image,0);
			
			//txt
			viewContent["index"].text = ""+  int(currentStepIndex+1) + "/" + steps.length;
			viewContent["title"].text = vo.title;
			viewContent["desc"].text = vo.description;
			
			
				Debug.log("NEWS VIEW: ");
				Debug.log("NEWS - title: "+vo.title);
				Debug.log("NEWS - description: "+vo.description);
			
			
			//btn
			btnWrapper.removeChildren();
			btnPrev.visible = (currentStepIndex > 0);
			
			if(currentStepIndex == steps.length-1) 
			{
				btnWrapper.addChild(btnFinish);
				btnWrapper.addChild(doNotShowAnymore);
			}
			if(!(currentStepIndex == steps.length-1)) btnWrapper.addChild(btnSkip);
			if(currentStepIndex < steps.length-1)btnWrapper.addChild(btnNext);

			
			//positions
			viewContent["desc"].y = viewContent["title"].y + viewContent["title"].height + 10;
			btnWrapper.y = viewContent["desc"].y + viewContent["desc"].height + 15;
			if(btnWrapper.y < viewContent["image"].y + viewContent["image"].height)
				btnWrapper.y = viewContent["image"].y + viewContent["image"].height + 20;
			btnWrapper.x = viewContent["title"].x; //viewContent["index"].x - btnWrapper.width - 20;
			btnPrev.y = btnWrapper.y + 8;
			
			viewBg.height = btnWrapper.y + btnWrapper.height + 40;
			viewContent["index"].y = viewBg.height - viewContent["index"].height - 10;
			viewContent["index"].x = viewBg.width - viewContent["index"].width - 10;
			
			//Create darkness (Sounds good huh!? :) )
			if(!darkness)
			{
				darkness = new Sprite();
				darkness.graphics.beginFill(0,.7);
				darkness.graphics.drawRect(0,0,stageRef.stageWidth, stageRef.stageHeight);
				darkness.graphics.endFill();
				darkness.alpha = 0;
				stageRef.addChild(darkness);
				
			}
		}
		
		
		
		
		/**
		 * destroy function
		 * */
		private function dispose():void
		{
			if(view && view.parent)
				view.parent.removeChild(view);
						
			if(darkness && darkness.parent)
				darkness.parent.removeChild(darkness);
			
			if(darkness)
				darkness = null;
			
		}
		
		/**
		 * SKIP Btn click handler
		 * */
		private function skipBtnHandler():void
		{
			hideStep(dispose);
		}
		
		/**
		 * NEXT  Btn click handler
		 * */
		private function nextBtnHandler():void
		{
			currentStepIndex++;
			nextStep();
		}
		
		/**
		 * PREV  Btn click handler
		 * */
		private function prevBtnHandler(e:MouseEvent):void
		{
			currentStepIndex--;
			nextStep();
		}	
		
		/**
		 * FINISH Btn click handler
		 * */
		private function finishBtnHandler():void
		{
			if(doNotShowAnymore.selected)
			{
				//save news Id to cookies, so the user don't see it anymore
				SharedObjectManager.instance.write(SharedObjectManager.KEY_NEWS,currentNewsGroupId);
			}
			hideStep(dispose, true);
			
			if(onComplete)
				TweenMax.delayedCall(2,onComplete)
		}
		
		
		/**
		 * Resize handler
		 * */
		protected function resizeHandler(event:Event):void
		{
			if(darkness)
			{
				darkness.graphics.clear();
				darkness.graphics.beginFill(0,.7);
				darkness.graphics.drawRect(0,0,stageRef.stageWidth, stageRef.stageHeight);
				darkness.graphics.endFill();
			}
			
			if(view && stageRef)
			{
				view.x = (stageRef.stageWidth - view.width) *.5;
				view.y = (stageRef.stageHeight - view.height) *.5;
			}
			
		}
		
		

	}
}

class SingletonEnforcer{}