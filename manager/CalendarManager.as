/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import flash.geom.Point;
	import flash.text.TextFormatAlign;
	
	import be.antho.data.ResourcesManager;
	
	import data.CalendarColors;
	import data.DateLinkageVo;
	import data.FrameVo;
	import data.Infos;
	import data.PageVo;
	
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.VerticalAlign;
	
	import utils.Debug;
	
	
	
	/**
	 * Calendar manager
	 */
	public class CalendarManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public static const DATEACTION_NOTES:String 							= "[notes]";
		public static const DATEACTION_MINICALENDAR_MONTH_PREVIOUS:String 		= "[mcmpre]";
		public static const DATEACTION_MINICALENDAR_MONTH_NEXT:String 			= "[mcmpost]";
		public static const DATEACTION_MINICALENDAR_WEEK_PREVIOUS:String 		= "[mcwpre]";
		public static const DATEACTION_MINICALENDAR_WEEK_NEXT:String 			= "[mcwpost]";
		public static const DATEACTION_MINICALENDAR_PREVIOUS:String 			= "[mcpre]";
		public static const DATEACTION_MINICALENDAR_NEXT:String 				= "[mcpost]";
		public static const DATEACTION_MINICALENDAR_MONTH:String 				= "[mcmvar]";
		public static const DATEACTION_MINICALENDAR_WEEK:String 				= "[mcwvar]";
		public static const DATEACTION_URL:String 								= "[url]";
		public static const DATEACTION_YEAR:String 								= "[year]";
		public static const DATEACTION_MONTH:String 							= "[month]";
		public static const DATEACTION_MONTHYEAR:String 						= "[monthyear]";
		public static const DATEACTION_WEEKOFYEAR:String 						= "[weekofyear]";
		public static const DATEACTION_WEEKDAY:String 							= "[weekday]";
		public static const DATEACTION_WEEKDAYFULL:String 						= "[weekdayfull]";
		public static const DATEACTION_DATE:String 								= "[date]";
		public static const DATEACTION_DATENAME:String 							= "[datename]";
		public static const DATEACTION_DAYDATE:String 							= "[daydate]";
		public static const DAYDATE_KIND_WEEKDAY:String 						= "daydateWeekDay";
		public static const DAYDATE_KIND_WEEKEND:String 						= "daydateWeekEnd";
		public static const DAYDATE_KIND_OFFSET:String 							= "daydateOffset";
			
		private var initiated:Boolean = false;
		public var weekDayFullIds:Vector.<String>; 
		public var monthIds:Vector.<String>;
		private var splittedMonth:Object = {};
		private var _year:int;
		/**
		 * instance
		 */
		private static var _instance:CalendarManager;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function CalendarManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("CalendarsManager is a singleton, use instance"); 
			if(!initiated)
				init();
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTERS SETTERS
		////////////////////////////////////////////////////////////////

		
		public function get year():int
		{
			return ProjectManager.instance.project.calendarYear;
		}

		public function set year(value:int):void
		{
			_year = value;
		}

		public static function get instance():CalendarManager
		{
			if (!_instance) _instance = new CalendarManager(new SingletonEnforcer())
			return _instance;
		}
		
		
		
		
		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Reset manager
		 * Set initiated to false to force init
		 */
		public function reset():void
		{
			init();
		}
		
		public function init():void
		{
			
			//DAYS ids list
			weekDayFullIds = new Vector.<String>();
			var list:XMLList = ProjectManager.instance.menuXML..calendarWeekday.node.@id;
			for (var i:int = 0; i < list.length(); i++) 
			{
				weekDayFullIds.push(list[i]);
			}
			
			//MONTH ids list
			monthIds = new Vector.<String>();
			list = ProjectManager.instance.menuXML..calendarMonth.node;
			for (i = 0; i < list.length(); i++) 
			{
				monthIds.push(list[i].@id);
			}
			
			
			//flag
			initiated = true;
		}
		
		/**
		 * get month index withOffset
		 * return 0-11
		 */
		public function getMonthIndexWithOffSet(monthIndex:int):int
		{
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int = wantedStartMonthIndex + monthIndex;
			var yearToUse:int = year;
			if(newmonthIndex>11)
			{
				newmonthIndex= Math.abs(12-newmonthIndex);
			}
			return newmonthIndex;
		}
		
		/**
		 * get year to use if month offset
		 * return correct year to use
		 */
		public function getYearToUseWithOffSet(monthIndex:int):int
		{
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int = wantedStartMonthIndex + monthIndex;
			var yearToUse:int = year;
			if(newmonthIndex>11)
			{
				newmonthIndex= Math.abs(12-newmonthIndex);
				yearToUse = year +1;
			}
			return yearToUse;
		}
		
		/**
		 * get number of day in month
		 * return 29 or 30 or 31
		 */
		public function getDaysInMonth(monthIndex:int):int
		{
			return daysInMonth(getYearToUseWithOffSet(monthIndex),getMonthIndexWithOffSet(monthIndex));
		}
		
		/**
		 * Get Text by dateAction and dateIndex
		 * returns dd-mm
		 */
		public function getDateLinkageId(frameCalendarVo:FrameVo):DateLinkageVo
		{
			var obj:DateLinkageVo = new DateLinkageVo();
			var monthIndex:int = getFrameMonthIndex( frameCalendarVo );
			var index:int = frameCalendarVo.dateIndex;
			//obj.startingDay = ProjectManager.instance.project.calendarStartDayIndex;
			//obj.startingMonth = ProjectManager.instance.project.calendarStartMonthIndex;
			obj.dayDate = int(getDayDate(index,monthIndex,frameCalendarVo));
			obj.dayKind = whatKindofDayDateAmI(frameCalendarVo,monthIndex,String(obj.dayDate));
			obj.montIndexWithOffset = getMonthIndexWithOffSet(monthIndex);
			return obj;
		}
		
		/**
		 * Get Text by dateAction and dateIndex
		 * 
		 */
		public function getFrameText(frameVo:FrameVo):String
		{
			var monthIndex : Number = getFrameMonthIndex( frameVo );
			var dateAction:String = frameVo.dateAction;
			var index:int = frameVo.dateIndex;
			switch(dateAction)
			{
				case DATEACTION_WEEKDAYFULL:
					return getWeekDayFull(index-1,monthIndex, false, true);
					break;
				
				case DATEACTION_WEEKDAY:
					return getWeekDayFull(index-1,monthIndex, true, true);
					break;
				
				case DATEACTION_MONTHYEAR:
					return getMonthWithOffSetYear(monthIndex,true);//getMonthYear(monthIndex);
					break;
				
				case DATEACTION_DAYDATE:
					return getDayDate(index,monthIndex,frameVo);
					break;
				
				case DATEACTION_WEEKOFYEAR:
					return getWeekOfYear(frameVo,monthIndex);
					break;
				
				case DATEACTION_MINICALENDAR_MONTH_PREVIOUS:
					return "";//getMonthYear(monthIndex-1);
					break;
				
				case DATEACTION_MINICALENDAR_MONTH_NEXT:
					return "";//getMonthYear(monthIndex+1);
					break;
				
				case DATEACTION_MINICALENDAR_WEEK_PREVIOUS:
					return "";// todo: How to align the data in the textfield to match weeks rows and day colums?
					break;
				
				case DATEACTION_MINICALENDAR_WEEK_NEXT:
					return "";// todo: How to align the data in the textfield to match weeks rows and day colums?
					break;
				
				case DATEACTION_MINICALENDAR_PREVIOUS:
					return "";// todo: How to align the data in the textfield to match weeks rows and day colums?
					break;
				
				case DATEACTION_MINICALENDAR_NEXT:
					return "";// todo: How to align the data in the textfield to match weeks rows and day colums?
					break;
				
				case DATEACTION_NOTES:
					return getNotesContent();//
					break;
				
				case DATEACTION_YEAR:
					if(Infos.project.docCode == "DCAL")
					return getyearLabel(monthIndex);
					else
						return getyearLabel(monthIndex, true);
					break;
				
				case DATEACTION_DATENAME:
					return getDateName(index,monthIndex,true);//
					break;
				
				case DATEACTION_MONTH:
					return getMonthWithOffSetYear(monthIndex, false);//getMonthFull(monthIndex);
					break;
				
				case DATEACTION_MINICALENDAR_MONTH:
					return getMonthWithOffSetYear(index-1, true);//YeargetMonthYear(index-1);
					break;
				
				case DATEACTION_MINICALENDAR_WEEK:
					return getVerticalWeek(index,0,3,true);
					break;
				
				case DATEACTION_URL:
					return "www.tictacphoto.com";
					break;
			}
			return "?";
		}
		
		/**
		 * Helper
		 * Gives index of the given day index of the month (0->6) (Sunday:0 -> Saturday:6)
		 * */
		public function whatKindofMcwvarAmI(dateIndex:int):String
		{
			var str:String = String(dateIndex);
			var dayIndex:int = int(str.substr(str.length-1))-1;
			var wantedStartDayIndex:int = ProjectManager.instance.project.calendarStartDayIndex;
			var offSetIndex:int = dayIndex + wantedStartDayIndex;
			offSetIndex = (offSetIndex < weekDayFullIds.length)?offSetIndex:offSetIndex - weekDayFullIds.length;
			
			if(offSetIndex==6 || offSetIndex==0)
				return DAYDATE_KIND_WEEKEND;
			return DAYDATE_KIND_WEEKDAY;
		}
		
		/**
		 * Helper
		 * Gives index of the given day index of the month (0->6) (Sunday:0 -> Saturday:6)
		 * */
		public function whatKindofDayNameAmI(dateIndex:int, monthIndex:int, yearToUse:int):String
		{
			var dayIndex:int = getDayOfMonthIndex(dateIndex,monthIndex, yearToUse);
			if(dayIndex==6 || dayIndex==0)
				return DAYDATE_KIND_WEEKEND;
			return DAYDATE_KIND_WEEKDAY;
		}
		
		/**
		 * Helper
		 * Gives index of the given day index of the month (0->6) (Sunday:0 -> Saturday:6)
		 * */
		public function whatKindofWeekDayAmI(dateIndex:int, monthIndex:int):String
		{
			var dayIndex:String = getWeekDayFull(dateIndex-1,monthIndex, false);
			if(dayIndex== "saturday" || dayIndex== "sunday")
				return DAYDATE_KIND_WEEKEND;
			return DAYDATE_KIND_WEEKDAY;
		}
		
		/**
		 * Helper
		 * Gives index of the given day index of the month (0->6) (Sunday:0 -> Saturday:6)
		 * */
		public function whatKindofDayDateAmI(frameVo:FrameVo, monthIndex:int, displayIndex:String):String
		{
			var yearToUse:int = year;
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int =  wantedStartMonthIndex+monthIndex;
			if(newmonthIndex>11)
			{
				newmonthIndex = Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			
			
			
			var dateIndex:int = frameVo.dateIndex;
			var displayOffSet:int;
			var endOffset:int;
			var wantedStartDayIndex:int = ProjectManager.instance.project.calendarStartDayIndex;
			var firstDayOfMonthIndex:int = getFirstDayOfMonthIndex(newmonthIndex, yearToUse);
			var totalDaysinMonth:int = daysInMonth(yearToUse,newmonthIndex);
			var startOffset:int = Math.abs(wantedStartDayIndex -firstDayOfMonthIndex);
			
			
			
			displayOffSet = daysToDisplay(frameVo) - totalDaysinMonth;
			startOffset = (firstDayOfMonthIndex<wantedStartDayIndex)?7-startOffset:startOffset;
			endOffset = displayOffSet - startOffset;
			
			//small hack for caledar WCAL4 (birthday)
			if(ProjectManager.instance.project.docCode == "WCAL4")
				startOffset = 0;
			
			if(dateIndex<=startOffset)
				return DAYDATE_KIND_OFFSET;
			if(dateIndex>startOffset+totalDaysinMonth)
				return DAYDATE_KIND_OFFSET;
			var dayIndex:int = getDayOfMonthIndex(int(displayIndex),newmonthIndex,yearToUse);
			if(dayIndex==6 || dayIndex==0)
				return DAYDATE_KIND_WEEKEND;
			
			return DAYDATE_KIND_WEEKDAY;
		}
		
		
		
		/**
		 * retrieve calendar month index
		 */
		public function getFrameMonthIndex( frameVo:FrameVo ):int
		{
			if( frameVo.type != FrameVo.TYPE_CALENDAR ) return -1;
			return PagesManager.instance.getPageVoByFrameVo(frameVo).monthIndex;
		}
		
		
		/**
		 * get horizontal align
		 */
		public function getFrameTextHAlign( frameVo:FrameVo ):String
		{
			if( frameVo.type != FrameVo.TYPE_CALENDAR ) return TextFormatAlign.LEFT;
			switch(frameVo.dateAction)
			{
				case CalendarManager.DATEACTION_NOTES:
					return TextFormatAlign.LEFT;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_WEEK:
					return TextFormatAlign.CENTER;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_MONTH:
					return TextFormatAlign.CENTER;
					break;
				
			}
			return frameVo.hAlign;
		}
		
		/**
		 * get horizontal align
		 */
		public function getFrameTextVAlign( frameVo:FrameVo ):String
		{
			if( frameVo.type != FrameVo.TYPE_CALENDAR ) return VerticalAlign.TOP
			switch(frameVo.dateAction)
			{
				case CalendarManager.DATEACTION_NOTES:
					return VerticalAlign.TOP
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_WEEK:
					return VerticalAlign.TOP;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_MONTH:
					return VerticalAlign.TOP
					break;
				
			}
			return frameVo.vAlign
		}
		
		public function getFrameTextOffset( frameVo : FrameVo ):Point
		{
			var mmToPoint : Number = MeasureManager.mmToPoint;
			var offsetPoint : Point = new Point(0,0);
			if(frameVo.stroke){
				switch(frameVo.dateAction)
				{
					case CalendarManager.DATEACTION_DAYDATE:
						offsetPoint.x = ( frameVo.vAlign == VerticalAlign.TOP )? mmToPoint*2 : 0;
						offsetPoint.y = ( frameVo.hAlign == TextAlign.LEFT )? mmToPoint*2 : 0;
						break;
					
				}
			}
			
			return offsetPoint;
		}
		
		public function isFrameMultiline( frameVo : FrameVo ):Boolean
		{
			switch(frameVo.dateAction)
			{
				case CalendarManager.DATEACTION_NOTES:
				case CalendarManager.DATEACTION_MINICALENDAR_WEEK:
				case CalendarManager.DATEACTION_DATENAME: // new add 08/04/2014 > keith remark
					return true;
					break;
				
			}
			return false;
		}
		
		
		/**
		 * retrieve calendar text color
		 */
		public function getFrameTextColor( frameVo : FrameVo ):Number
		{
			if( frameVo.type != FrameVo.TYPE_CALENDAR ) return 0;
			
			var monthIndex : Number = getFrameMonthIndex( frameVo );
			var yearToUse:int = getYearToUseWithOffSet(monthIndex);
			monthIndex = getMonthIndexWithOffSet(monthIndex);
			
			var text : String = getFrameText(frameVo);
			var dayType:String;
			var color:Number;
			if(frameVo.dateAction == CalendarManager.DATEACTION_DAYDATE)
			{
				dayType = whatKindofDayDateAmI(frameVo,monthIndex, text);
			}
			else if(frameVo.dateAction == CalendarManager.DATEACTION_DATENAME)
			{
				dayType = CalendarManager.instance.whatKindofDayNameAmI(frameVo.dateIndex,monthIndex,yearToUse);
			}
			else if(frameVo.dateAction == CalendarManager.DATEACTION_MINICALENDAR_WEEK)
			{
				dayType = CalendarManager.instance.whatKindofMcwvarAmI(frameVo.dateIndex);
			}
			else if(frameVo.dateAction == CalendarManager.DATEACTION_WEEKDAYFULL || frameVo.dateAction == CalendarManager.DATEACTION_WEEKDAY)
			{
				dayType = CalendarManager.instance.whatKindofWeekDayAmI(frameVo.dateIndex,monthIndex);
			}
			else
			{
				dayType = frameVo.dateAction;
			}
			
			return getColor(frameVo, dayType);
		}
		
		/**
		 * Get color byt type of dayDate
		 */
		public function getColor(frameVo:FrameVo, dayType:String):Number
		{
			var pageVo : PageVo = PagesManager.instance.getPageVoByFrameVo(frameVo);
			var colors:CalendarColors = (pageVo.customColors)? pageVo.customColors : new CalendarColors();
			
			switch(dayType)
			{
				case CalendarManager.DAYDATE_KIND_OFFSET:
					return colors.offSetDay;
					break;
				
				case CalendarManager.DAYDATE_KIND_WEEKDAY:
					return colors.weekDay;
					break;
				
				case CalendarManager.DAYDATE_KIND_WEEKEND:
					if(Infos.project.docCode != "WCAL4")
					return colors.weekEndDay;
					else
					return colors.weekDay;
					break;
				
				case CalendarManager.DATEACTION_MONTH:
					return colors.month;
					break;
				
				case CalendarManager.DATEACTION_YEAR:
					return colors.year;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_MONTH:
				case CalendarManager.DATEACTION_MONTHYEAR:
					return colors.month;
					break;
				
				case CalendarManager.DATEACTION_MINICALENDAR_NEXT:
				case CalendarManager.DATEACTION_MINICALENDAR_PREVIOUS:
					return colors.offSetDay;
					break;
			}
			
			return 0;
		}
		
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		/**
		 * Return string with: Mon 1 7 14 21
		 * index is 11, 12, 13, 14, 15, 16, 17, 21, 22, 23 etc...
		 * 21 = month 2 Sunday (depending on the start day of week)
		 */
		public function getVerticalWeek(index:int, monthIndexOffSet:int = 0, dayNameMaxChars:int = 3, localized:Boolean = false):String
		{
			var str:String = String(index);
			var dayIndex:int = int(str.substr(str.length-1))-1;
			var monthIndex:int = int(str.substr(0,str.length-1))-1;
			var yearToUse:int = year;
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int =  wantedStartMonthIndex+monthIndex+monthIndexOffSet;
			if(newmonthIndex>11)
			{
				newmonthIndex = Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			
			//only for mini calendars
			if(newmonthIndex<0)
			{
				newmonthIndex = 11;
				yearToUse = year-1;
			}
			
			
			var endOffset:int;
			var wantedStartDayIndex:int = ProjectManager.instance.project.calendarStartDayIndex;
			var firstDayOfMonthIndex:int = getFirstDayOfMonthIndex(newmonthIndex, yearToUse);
			var totalDaysinMonth:int = daysInMonth(yearToUse,newmonthIndex);
			var startOffset:int = Math.abs(wantedStartDayIndex - firstDayOfMonthIndex);
			startOffset = (firstDayOfMonthIndex<wantedStartDayIndex)?7-startOffset:startOffset;
			
			//Parse all month
			//Group mondays with in monday's Array, Tuesdays in tuesday's array etc...
			// {"1": [1,8,15,22,29]} , {"2": [2,9,16,23,30]} etc...
			var repository:Object = {};
			for (var i:int = 0; i < totalDaysinMonth; i++) 
			{
				var dayArrName:String = String(getDayOfMonthIndex(i+1,newmonthIndex,yearToUse))
				if(repository["y"+yearToUse] == null)
					repository["y"+yearToUse] = {};
				
				if(repository["y"+yearToUse]["d"+dayArrName] == null)
					repository["y"+yearToUse]["d"+dayArrName] = [];
				
				repository["y"+yearToUse]["d"+dayArrName].push(i+1);
			}
			var offSetIndex:int = dayIndex + wantedStartDayIndex;
			offSetIndex = (offSetIndex < weekDayFullIds.length)?offSetIndex:offSetIndex - weekDayFullIds.length;
			
			if(dayIndex<startOffset)
				repository["y"+yearToUse]["d"+offSetIndex].unshift(" ");
			
			var dayName:String = (!localized)?weekDayFullIds[offSetIndex].substr(0,dayNameMaxChars):ResourcesManager.getString("calendar.weekday."+weekDayFullIds[offSetIndex]).substr(0,dayNameMaxChars);
			dayName = (dayNameMaxChars == 1)?dayName.toUpperCase():dayName;
			var string:String = dayName+"\n"+repository["y"+yearToUse]["d"+offSetIndex].join("\n");
						
			return string;
		}
		
		/**
		 * Return notes:.........
		 */
		private function getNotesContent():String
		{
			var dots:String = ".";
			for (var i:int = 0; i < 1000; i++) 
			{
				dots = dots + " .";
			}
			
			return ResourcesManager.getString("calendar.text.notes")+": "+ dots;
		}
		
		
		/**
		 * get the date name
		 * return We 1, Tu 2, Fr 3...
		 */
		private function getDateName(index:int, monthIndex:int, localized:Boolean = false):String
		{
			var yearToUse:int = year;
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int =  wantedStartMonthIndex+monthIndex;
			if(newmonthIndex>11)
			{
				newmonthIndex = Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			
			
			var dayIndex:int = getDayOfMonthIndex(index,newmonthIndex, yearToUse);
			var dayName:String = (!localized)?weekDayFullIds[dayIndex].substr(0,2):ResourcesManager.getString("calendar.weekday."+weekDayFullIds[dayIndex]).substr(0,2);
			return dayName+"\n"+index;
		}		
		
		/**
		 * get the day Number
		 * return the correct year depending on monthIndex
		 */
		private function getyearLabel(monthIndex:int, withNextYear:Boolean = false):String
		{
			var yearToUse:int = year;
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int =  wantedStartMonthIndex+monthIndex;
			if(newmonthIndex>11)
			{
				newmonthIndex = Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			if(withNextYear)
				return (wantedStartMonthIndex>0)?String(year) +"-"+ String(year+1):String(year);
			else 
				return String(yearToUse);
		}
		
		
		/**
		 * get the day Number
		 * return 1->31
		 */
		private function getDayDate(index:int, monthIndex:int, frameVo:FrameVo, returnFullDate:Boolean = false):String
		{
			var yearToUse:int = year;
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int =  wantedStartMonthIndex+monthIndex;
			
			if(newmonthIndex>11)
			{
				newmonthIndex = Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			
			
			
			var dayToReturn:int;
			var displayOffSet:int;
			var endOffset:int;
			var wantedStartDayIndex:int = ProjectManager.instance.project.calendarStartDayIndex;
			var firstDayOfMonthIndex:int = getFirstDayOfMonthIndex(newmonthIndex, yearToUse);
			var totalDaysinMonth:int = daysInMonth(yearToUse,newmonthIndex);
			var startOffset:int = Math.abs(wantedStartDayIndex -firstDayOfMonthIndex);
			startOffset = (firstDayOfMonthIndex<wantedStartDayIndex)?7-startOffset:startOffset;
			var totalDaysinPrevMonth:int = daysInMonth(yearToUse,newmonthIndex-1);
			var dayNumberToReturn:String;
			
			//small hack for caledar WCAL4 (birthday)
			if(ProjectManager.instance.project.docCode == "WCAL4")
				startOffset = 0;
			
			//Create the array that contains all the days to display
			//Can be 30, 31, 28, 29 or even 37 when offset the previous month or the next
			var dayDateArray:Array = [];//(splittedMonth[monthIndex])?splittedMonth[monthIndex]:[];
			if(dayDateArray.length == 0)
			{
				for (var i:int = 1; i <= totalDaysinMonth; i++) 
				{
					dayDateArray.push(i);
				}
				//Calculate the difference between total day in the given month and total day to display
				// check if offset the previous month or the next
				displayOffSet = daysToDisplay(frameVo) - totalDaysinMonth;
				if(displayOffSet == 0)
				{
					dayNumberToReturn = dayDateArray[index-1];
					if(!returnFullDate)
					return dayNumberToReturn;
					else
					return dayNumberToReturn+"/"+newmonthIndex+"/"+yearToUse;
						
				}
				else
				{
					//start offset
					for (var j:int = 0; j < startOffset; j++) 
					{
						dayDateArray.unshift(totalDaysinPrevMonth-j);
					}
					//end offset
					endOffset = displayOffSet - startOffset;
					for (j = 1; j <= endOffset; j++) 
					{
						dayDateArray.push(j);
					}
				}
				//splittedMonth[monthIndex] = dayDateArray;
			}
			dayNumberToReturn = dayDateArray[index-1];
			if(!returnFullDate)
			{
			return dayNumberToReturn;
			}
			else
			{
				if(index < 7 && int(dayNumberToReturn) > 7)
					newmonthIndex --;
				else if (int(dayNumberToReturn) < 7 && (index-startOffset) > 7)
					newmonthIndex ++;
				
				if(newmonthIndex == -1)//december
				{
					newmonthIndex =  11;
					yearToUse--;
				}
				else if(newmonthIndex == 12)
				{
					newmonthIndex =  0;
					yearToUse++;
				}
				
				return dayNumberToReturn+"/"+newmonthIndex+"/"+yearToUse;
			}
		}
		
		/**
		 * get the difference of day between the month and what is displayed
		 * 
		 */
		private function getOffSetDays(monthIndex:int, frameVo:FrameVo):int
		{
			var totalDaysInMonth:int = daysInMonth(year,monthIndex);
			var daysToDisplay:int = daysToDisplay(frameVo);
			return daysToDisplay - totalDaysInMonth;
		}
		
		/**
		 * get the number of day to display 
		 */
		private function daysToDisplay(frameVo:FrameVo):int
		{
			var pageVo:PageVo = PagesManager.instance.getPageVoByFrameVo(frameVo);
			var xml:XML = ProjectManager.instance.layoutsXML.layout.(@name == pageVo.layoutId)[0];
			var list:XMLList = xml..frame.(attribute("dateaction") == CalendarManager.DATEACTION_DAYDATE);
			
			//return LayoutManager.instance.totalDaysToDisplay;
			return list.length();
		}
		
		/**
		 * get weekDay by index
		 * return monday, tuesday etc...
		 */
		private function getWeekDayFull(index:int, monthIndex:int, shortVersion:Boolean = false, localized:Boolean = false):String
		{
			var maxChars:int = 3;
			//small hack for caledar DCAL2 
			if(ProjectManager.instance.project.docCode == "DCAL2")
				maxChars = 2;
			var wantedStartIndex:int = index + ProjectManager.instance.project.calendarStartDayIndex;
			//var firstDayOfMonthOffset:int = getFirstDayOfMonthOffset(monthIndex, year);
			var dayIndex:int = wantedStartIndex;
			if(dayIndex>weekDayFullIds.length-1)
			{
				dayIndex = dayIndex-weekDayFullIds.length;
			}
			
			if(shortVersion)
			{
				if(!localized)
				return weekDayFullIds[dayIndex].substr(0,maxChars);
				else
				return ResourcesManager.getString("calendar.weekday."+weekDayFullIds[dayIndex]).substr(0,maxChars);
			}
			
			if(!localized)
				return weekDayFullIds[dayIndex];
			else
				return ResourcesManager.getString("calendar.weekday."+weekDayFullIds[dayIndex]);
		}
		
		/**
		 * get week of year
		 * return 1->52
		 */
		private function getWeekOfYear(frameVo:FrameVo, monthIndex:int):String
		{
			var index:int = frameVo.dateIndex;
			var day:Date;
			var dateStringOfFirstFrameAfterWeekDayFrame:String = getDayDate(index,monthIndex,frameVo,true);
			var splittedDate:Array = dateStringOfFirstFrameAfterWeekDayFrame.split("/");
			var monthfordate:int = splittedDate[1];
			var yearfordate:int = splittedDate[2];
			var dayfordate:int = splittedDate[0];
			var dateObj:Date = new Date(yearfordate,monthfordate,dayfordate);
			var dayOfYear:int = getDayOfYear(dateObj); //1-365
			var weekDay:int =  dateObj.getUTCDay()+1;// (Monday:1 -> Sunday:7)
			var weekNumber:int = Math.floor((dayOfYear - weekDay + 10)/7);
			//hack for january first week
			if(monthfordate == 11 && index == 1)
			{
				if(weekNumber > 51)
					weekNumber = 1;
			}
			return ResourcesManager.getString("calendar.text.week.initital")+String(weekNumber);
		}
		
		/**
		 * get month text
		 * return january, february etc..
		 */
		private function getMonthFull(monthIndex:int, localized:Boolean = false):String
		{
			if(!localized)
			return monthIds[monthIndex];
			else
			return ResourcesManager.getString("calendar.month."+monthIds[monthIndex]);
		}
		
		/**
		 * get month with offset
		 * return january 2014, february 2014 etc..
		 */
		public function getMonthWithOffSetYear(monthIndex:int, withYear:Boolean = false):String
		{
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int = wantedStartMonthIndex + monthIndex;
			var yearToUse:int = year;
			if(newmonthIndex>11)
			{
				newmonthIndex= Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			if(withYear)return getMonthFull(newmonthIndex, true) +" "+ Number(yearToUse);
			return getMonthFull(newmonthIndex, true);
		}
		
		
		
		/**
		 * get month with offset
		 * return january 2014, february 2014 etc..
		 */
		public function getMiniCalendarMonthWithOffSetYear(monthIndex:int, monthIndexOffset:int, withYear:Boolean = false):String
		{
			var wantedStartMonthIndex:int = ProjectManager.instance.project.calendarStartMonthIndex;
			var newmonthIndex:int = wantedStartMonthIndex + monthIndex + monthIndexOffset;
			var yearToUse:int = year;
			if(newmonthIndex>11)
			{
				newmonthIndex = Math.abs(12-newmonthIndex);
				yearToUse = year+1;
			}
			if(newmonthIndex<0)
			{
				newmonthIndex= 11;
				yearToUse = year-1;
			}
			
			if(withYear)return getMonthFull(newmonthIndex, true) +" "+ Number(yearToUse);
			return getMonthFull(newmonthIndex, true);
		}
		
		/**
		 * get month text
		 * return january 2014, february 2014 etc..
		 */
		private function getMonthYear(monthIndex:int):String
		{
			var yearOffSet:int = 0;
			if(monthIndex <0)
			{
				monthIndex = 11;
				yearOffSet--;
			}
			else if(monthIndex >11)
			{
				monthIndex = 0;
				yearOffSet++;
			}
			return getMonthFull(monthIndex, true) +" "+ Number(ProjectManager.instance.project.calendarYear+yearOffSet); //return getMonthFull(monthIndex, true) +" "+ Number(year+yearOffSet);
		}
		
		/**
		 * Helper
		 * Gives the amount of day in a given month
		 * */
		private function daysInMonth(year:int, month:int):Number 
		{
			month = (month<0)?12-Math.abs(month):month;
			var totalDays:Number = 0;
			// create an array of month durations
			var monthLength:Array = new Array (31,28,31,30,31,30,31,31,30,31,30,31);
			// check for leap year
			var isLeap:Boolean = (year % 4 == 0) && (year % 100 != 0) || (year % 100 == 0) && (year % 400 == 0);

			if(month == 1) 
			{
				if(ProjectManager.instance.project.docCode == "WCAL4" || isLeap)
					totalDays = 29;
				else
					totalDays = monthLength[month];
			}
			else {
				totalDays = monthLength[month];
			}
			return totalDays;
		}
		
		/**
		 * Helper
		 * Gives index of the given day in a year (365)
		 * */
		private function getDayOfYear(date:Date):int 
		{	
			var monthLengths:Array = new Array (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
			
			// A leap year is divisable by 4, but not by 100 unless divisable by 400.
			if (((date.getFullYear() % 4 == 0) && (date.getFullYear() % 100 != 0)) || (date.getFullYear() % 400 == 0)) {
				monthLengths[1] = 29;
				trace ("leap year");
			}
			
			var dayInYear:int = 0;
			
			// get day of year up to month
			for (var i:Number = 0; i < date.getMonth(); i++) {
				dayInYear += monthLengths[i];
			}
			
			// add day inside month
			dayInYear += date.getDate();
			
			// Start counting on 0 (optional)
			// dayInYear--;
			
			return dayInYear;
		}

		
		/**
		 * Helper
		 * Gives index of the first day of the month (0->6) (Sunday:0 -> Saturday:6)
		 * */
		private function getFirstDayOfMonthIndex(month:int, year:int):Number
		{
			var d:Date = new Date(year, month, 1);
			return d.getDay();
		}
		
		/**
		 * Helper
		 * Gives index of the given day index of the month (0->6) (Sunday:0 -> Saturday:6)
		 * */
		private function getDayOfMonthIndex(day:int, month:int, year:int):Number
		{
			var d:Date = new Date(year, month, day);
			return d.getDay();
		}
	}
}
class SingletonEnforcer{}