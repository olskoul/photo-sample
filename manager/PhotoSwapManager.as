/***************************************************************
 PROJECT 		: TicTac Photo
 COPYRIGHT 		: jonathan@nguyen
 YEAR			: 2013
 ****************************************************************/
package manager
{
	import be.antho.utils.tabs.TabManager;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	
	import data.FrameVo;
	import data.PhotoVo;
	
	import org.osflash.signals.Signal;
	
	import utils.Colors;
	import utils.Debug;
	
	import view.edition.Frame;
	import view.edition.FramePhoto;
	import view.edition.pageNavigator.PageNavigator;
	
	/**
	 * Photo swap manager
	 */
	public class PhotoSwapManager
	{
		////////////////////////////////////////////////////////////////
		//	PROPERTIES
		////////////////////////////////////////////////////////////////		
		
		public static const SWAP_IMAGE_MODE:Signal = new Signal(Boolean); // activate <> deasactivate swap image mode
		
		// private
		private var _swapActive : Boolean = false;
		private var frame1:FramePhoto;
		private var frame2:FramePhoto;
		
		////////////////////////////////////////////////////////////////
		//	CONSTRUCTOR / INSTANCE
		////////////////////////////////////////////////////////////////
		/**
		 * Constructor
		 */
		public function PhotoSwapManager(sf:SingletonEnforcer) 
		{
			if(!sf) throw new Error("LayoutManager is a singleton, use instance"); 
		}
		
		/**
		 * instance
		 */
		private static var _instance:PhotoSwapManager;
		public static function get instance():PhotoSwapManager
		{
			if (!_instance) _instance = new PhotoSwapManager(new SingletonEnforcer())
			return _instance;
		}
		
		////////////////////////////////////////////////////////////////
		//	GETTER / SETTER
		////////////////////////////////////////////////////////////////
		/**
		 * check if swap is active right now
		 */
		public function get swapActive():Boolean
		{
			return _swapActive;
		}

		////////////////////////////////////////////////////////////////
		//	PUBLIC METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 *
		 */
		public function toggleSwapMode():void
		{
			if(_swapActive) stopSwap();
			else startSwap();
		}
		
		/**
		 *
		 */
		public function startSwap():void
		{
			Debug.log("PhotoSwap > startSwap");
			_swapActive = true;
			TabManager.instance.open(false);
			SWAP_IMAGE_MODE.dispatch(true);
		}
		
		
		/**
		 *
		 */
		public function stopSwap():void
		{
			Debug.log("PhotoSwap > stopSwap");
			_swapActive = false;
			if(frame1) desactivateFrame(frame1)
			if(frame2) desactivateFrame(frame2)
			frame1 = null;
			frame2 = null;
			SWAP_IMAGE_MODE.dispatch(false);
		}
		
		/**
		 * add frame to swap
		 */
		public function addFrame( frame : FramePhoto ):void
		{
			if(!frame) {
				Debug.warn("PhotoSwap > addFrame > frame is null...");
				return;
			}
			
			if( !frame1 ) {
				frame1 = frame;
				activateFrame (frame);
			}
			else frame2 = frame;
			
			if(frame2) makeSwapAndClose(); 
		}
		
		////////////////////////////////////////////////////////////////
		//	PRIVATE METHODS
		////////////////////////////////////////////////////////////////
		
		/**
		 * activate frame visualy
		 */
		private function activateFrame( frame : FramePhoto ):void
		{
			frame.mouseChildren = frame.mouseEnabled = false;
			frame.swapHighlight(true);
			/*
			TweenMax.delayedCall(.05, function():void{ // put it in delay call to do it after the rollout event on the frame
				TweenMax.killTweensOf(frame);
				//TweenMax.to(frame, .2, { tint:Colors.YELLOW, alpha:1, ease : Strong.easeOut});
				TweenMax.to(frame, .3, {colorMatrixFilter:{colorize:Colors.YELLOW, amount:1.5, brightness:1.5}});
			});
			*/
		}
		
		/**
		 * activate frame visualy
		 */
		private function desactivateFrame( frame : FramePhoto ):void
		{
			frame.mouseChildren = frame.mouseEnabled = true;
			//TweenMax.to(frame, 0, { tint:null, alpha:1});
			//TweenMax.to(frame, 0, {colorMatrixFilter:{}});
			frame.swapHighlight(false);
		}
		
		
		/**
		 * make swap action
		 * + remove indicators
		 * + return in normal mode !
		 */
		private function makeSwapAndClose():void
		{
			var photoVo1 : PhotoVo = frame1.frameVo.photoVo; 
			var photoVo2: PhotoVo = frame2.frameVo.photoVo;
			
			if(!photoVo2) frame1.clearMe(); 
			else frame1.update(photoVo2);
			
			if(!photoVo1) frame2.clearMe(); 
			else frame2.update(photoVo1);
			
			// redraw
			PageNavigator.RedrawPageByFrame(frame1.frameVo);
			PageNavigator.RedrawPageByFrame(frame2.frameVo);
			
			// add action in history
			var page1:Number = PagesManager.instance.getPageVoByFrameVo( frame1.frameVo).index;
			var page2 : Number = PagesManager.instance.getPageVoByFrameVo( frame2.frameVo).index;
			if(page2 != page1 ) UserActionManager.instance.addAction(UserActionManager.TYPE_PHOTO_SWAP, {page1:page1, page2:page2});
			else UserActionManager.instance.addAction(UserActionManager.TYPE_PHOTO_SWAP, {page1:page1});
			
			// stop
			stopSwap();
		}
		
		
	}
		
}
class SingletonEnforcer{}